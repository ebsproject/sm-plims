FROM yiisoftware/yii2-php:8.1-apache

WORKDIR /var/www/html

COPY . .

#Copy setting files
COPY public/000-default.conf /etc/apache2/sites-enabled/
COPY config.sh config.sh

#Contains all the containerization steps
RUN apt-get update -y && apt-get install -y zip unzip nano && sh config.sh

#Expose port for service
EXPOSE 80
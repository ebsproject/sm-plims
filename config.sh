#!/bin/bash

#Setup PLIMS project
cp -a sm-plims/. ./
composer update
php init --env=Production --overwrite=No
chmod 777 -R frontend/web/documents

#Main-local variables
#sed -i "s/db_user/$db_user/g; s/db_pass/$db_pass/g" common/config/main-local.php

#/bin/bash

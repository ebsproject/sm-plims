<?php

namespace frontend\modules\intelligence;

/**
 * intelligence module definition class
 */
class Intelligence extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\intelligence\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

<?php

namespace frontend\modules\functional;

class Functional extends \yii\base\Module
{
    // assays
    const ESSAY_ELISA = 5;
    const ESSAY_BLOTTER = 3;
    const ESSAY_GERMINATION = 6;
    const ESSAY_PCR = 8;
    const ESSAY_VISUAL = 1;
    const ESSAY_WASHING = 2;
    const ESSAY_WASH = 7;
    const ESSAY_ANILINE = 4;
    const ESSAY_GLOEOTINIA = 9;
    const ESSAY_FILTRATION_CENTRIFUGATION = 11;
    const ESSAY_ENDOSPERM_HYDROLYSIS = 12;
    const ESSAY_STAINING_TEST = 13;
    // order in other entities
    const NUM_ORDER_INI = 1;
    // order in request
    const NUM_ORDER_ID_PROCESS = 1;
    const NUM_ORDER_ID_REPROCESS = 2;
    const NUM_ORDER_ID_DOUBTFUL = 3;

    const NEGATIVE_RESULT = 'negative';
    const POSITIVE_RESULT = 'positive';
    const DOUBT_RESULT = 'doubt';

    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';
    const TEXT_DISCARD = 'discard';
    const TEXT_EMPTY = 'empty';
    const READING_DATA_ENTRY = 67;
    const READING_DATA_POSITIVE = 68;
    const READING_DATA_NEGATIVE = 70;
    const READING_DATA_BUFFER = 71;
    const COMPOSITE_SAMPLE_TYPE_HEADER = 32;
    const COMPOSITE_SAMPLE_GENERAL = 30;
    const COMPOSITE_SAMPLE_SIMPLE = 31;
    const COMPOSITE_SAMPLE_INDIVIDUAL = 29;
    const REQUEST_PROCESS_PENDING = 16;
    const REQUEST_PROCESS_FINISHED = 18;
    const REQUEST_PROCESS_IN_PROCESS = 17;
    const REQUEST_PROCESS_ESSAY_PENDING = 41;
    const REQUEST_PROCESS_ESSAY_FINISHED = 43;
    const REQUEST_PROCESS_ESSAY_IN_PROCESS = 42;
    const REQUEST_PROCESS_ESSAY_ACTIVITY_PENDING = 141;
    const REQUEST_PROCESS_ESSAY_ACTIVITY_FINISHED = 142;
    const REQUEST_PROCESS_ESSAY_ACTIVITY_IN_PROCESS = 143;
    const REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_PENDING = 144;
    const REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_FINISHED = 145;
    const REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_IN_PROCESS = 146;
    const REQUEST_PROCESS_STATUS_PENDING = 16;
    const REQUEST_PROCESS_MATERIAL_EMPTY = 20;
    const REQUEST_PROCESS_PAYMENT_INCOMPLETE = 21;
    const REQUEST_PROCESS_ESSAY_STATUS_PENDING = 41; //pending
    const REQUEST_PROCESS_ESSAY_STATUS_IN_PROCESS = 42; //in process
    const REQUEST_PROCESS_ESSAY_STATUS_FINISHED = 43; //finished

    const REQUEST_CREATED = 12;
    const VERIFIED_ENTRIES = 28;
    const COMPOSITE_SAMPLE_GENERATED = 39;
    const ASSAY_GENERATED = 140;
    const REQUEST_IN_PROCESS = 46;
    const REQUEST_REPROCESS_GENERATED = 45;
    const REQUEST_READY_FOR_RELEASE = 44;
    const REQUEST_FINISHED = 192;
    const REQUEST_IN_STOCK = 47;

    const MATERIAL_CONTROL = 136;
    const MATERIAL_DELETED = 137;
    const MATERIAL_IN_STOCK = 138;
    const MATERIAL_NORMAL = 139;
    const INIT_CODE = 0;
    const REQUEST_TYPE_SHIPMENT_DISTRIBUTION = 11; //shipment distribution
    const REQUEST_TYPE_INTRODUCTION = 40; //introduction
    const MATERIAL_TYPE = 72;
    const MATERIAL_UNIT = 9;
    const MATERIAL_HISTORICAL_DATA_VERSION_INIT = 1;
    const NUM_AGENT = 66;
    const REQUEST_PAYMENT_ID = 14;
    const ZERO = 0;
    const COMPLETE_ANALYSIS = 1;
    const ESSAY_TYPE_MOB_APP = 26; //mobile app
    const ESSAY_TYPE_WEB_APP = 27; //web app
    const AGENT_TOKEN = 1;
    const ACTIVITY_PROPERTY_GET = 61;
    const ACTIVITY_PROPERTY_SET = 62;
    const READING_DATA_SYMPTOM = 1; //SYMPTOM TOKEN UNDETERMINED
    const READING_DATA_SUPPORT_ORDER_INI = 1; //SUPPORT TOKEN UNQUE
    const READING_DATA_SUPPORT_CELL_POSITION = 0; // IT IS NOT NECESSARY TO SPECIFY
    const READING_DATA_GRAFTING_NUMBER = 0; // ALWAYS BEGINS WITH AN GRAFT
    const READING_DATA_GRAFTING_NUMBER_AUX = 1; // SECOND GRAFT
    const READING_DATA_HISTORICAL_DATA_VERSION_INIT = 1; // INITIAL HISTORY
    const ACTIVITY_SAMPLE_HISTORICAL_DATA_VERSION_INIT = 0; // INITIAL HISTORY
    const PARAMETER_UNDETERMINED = 1;
    const AVAILABLE_TRUE = true;

    const ELISA_REPETITION = 2;

    const RELEASE_STATUS_PENDING = 188;
    const RELEASE_STATUS_IN_PROCESS = 189;
    const RELEASE_STATUS_FINISHED = 190;

    const ACTIVITY_CONSOLIDATE = 29;


    // ROLES
    const SUPER_ADMIN_ROLE = 1;
    const INSTITUTION_ADMIN_ROLE = 2;
    const LABORATORY_ADMIN_ROLE = 3;
    const NORMAL_USER_ROLE = 4;

    const INSTITUTION_TOKEN = 1;
    const LABORATORY_TOKEN = 1;


    public $controllerNamespace = 'frontend\modules\functional\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}

<?php

namespace frontend\modules\functional\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
// use app\models\UploadFile;
use app\models\UploadForm;
use frontend\modules\functional\models\RequestDocumentation;
use yii\web\UploadedFile;
// use frontend\modules\configuration\models\Parameter;
use frontend\modules\functional\models\CompositeSample;
use frontend\modules\functional\models\RequestSearch;
use frontend\modules\functional\models\RequestProcessEssay;
use frontend\modules\functional\models\RequestProcessEssayActivity;
use frontend\modules\functional\models\RequestProcessEssayActivitySample;
use frontend\modules\functional\models\RequestProcessEssayAgent;
use frontend\modules\functional\models\Status;
use frontend\modules\functional\models\Support;
use yii\data\ArrayDataProvider;
use frontend\modules\functional\Functional;
use frontend\models\Script;
use yii\helpers\Url;
use yii\filters\AccessControl;

class ManageRequestController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger', \Yii::t('app', 'You do not have sufficient permissions to access this website'));
          return $this->goHome();
        },
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  // Index
  public function actionIndex()
  {
    $params = Yii::$app->request->queryParams;
    $num_order_id = Functional::NUM_ORDER_ID_PROCESS;
    $order_list = ['registered_at' => SORT_DESC];
    $filterModel = new RequestSearch();
    //  TENGO QUE VER LA FORMA DE CREAR COLUMAS DE FILTROS
    $data_provider = $filterModel->search($params, $order_list);

    $_user_institution  = Yii::$app->user->Identity->institution;
    $_user_laboratory  = Yii::$app->user->Identity->laboratory;
    $_user_role  = Yii::$app->user->Identity->role;

    $_query = "SELECT request_id,
            request_code,
            bar_id,
            crop,
            material_qty,
            sample_qty,
            seed_distribution,
            recipient,
            sender,
            request_user,
            request_status,
            request_class,
            status_code,
            date_created,
            datetime_created
        FROM   (SELECT t1.request_id,
                    t1.request_code,
                    t2.bar_id          AS bar_id,
                    c1.short_name      AS crop,
                    t1.material_qty,
                    t1.sample_qty,
                    p2.short_name		  AS seed_distribution,
                    t2.recipient_name  AS recipient,
                    t2.sender_name     AS sender,
                    a1.username        AS request_user,
                    p1.short_name      AS request_status,
                    p1.class_details   AS request_class,
                    p1.code            AS status_code,
                    t1.registered_at::date   AS date_created,
                    t1.registered_at::time   AS datetime_created
            FROM plims.bsns_request t1
                    INNER JOIN plims.bsns_request_detail t2	 
                            ON t1.request_id = t2.request_id
                    INNER JOIN plims.bsns_parameter p1
                            ON t1.request_status_id = p1.parameter_id
                    INNER JOIN plims.bsns_parameter p2
                            ON t1.request_type_id = p2.parameter_id
                    INNER JOIN plims.auth_user a1
                            ON t1.requested_user_id = a1.user_id
                    INNER JOIN plims.bsns_request_process t3
                            ON t3.request_id = t1.request_id
                              AND t3.num_order_id = :num_order_id
                    INNER JOIN plims.bsns_crop c1
                            ON t3.crop_id = c1.crop_id
            WHERE t1.status = :status_active ";

    $_params = [
      ':num_order_id' => $num_order_id,
      ':status_active' => Functional::STATUS_ACTIVE
    ];


    if ($_user_institution) {
      $_query = $_query . "AND t1.institution_id = :_institution_id ";
      $_params[':_institution_id'] = $_user_institution;
    }

    if ($_user_laboratory) {
      $_query = $_query . "AND t1.laboratory_id = :_laboratory_id ";
      $_params[':_laboratory_id'] = $_user_laboratory;
    }

    $_query = $_query . "ORDER BY t1.registered_at DESC) AS tab";

    $data = Yii::$app->db->createCommand($_query, $_params)->queryAll();

    $newDataProvider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => [
          'request_id',
          'request_code',
          'bar_id',
          'crop',
          'material_qty',
          'sample_qty',
          'seed_distribution',
          'recipient',
          'sender',
          'request_user',
          'request_status',
          'request_class',
          'date_created'
        ],
      ],
      'key' => 'request_id',
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    $is_creator =  true;

    if ($_user_institution == 1 || $_user_laboratory ==  1 || $_user_role < 3) {
      $is_creator =  false;
    }


    return $this->render('index', [
      'filterModel' => $filterModel,
      'dataProvider' => $data_provider,
      'num_order_id' => $num_order_id,
      'newDataProvider' => $newDataProvider,
      'is_creator' => $is_creator,
    ]);
  }

  // Management
  public function actionManagement($request_id)
  {
    $num_order_id = Functional::NUM_ORDER_ID_PROCESS;

    $r_data = Yii::$app->db->createCommand(
      "SELECT t1.request_id,
            t4.short_name                AS crop,
            t5.short_name                AS seed_distribution,
            t5.code                      AS code_distribution,
            t6.username                  AS requested_user,
            t3.sender_country            AS sender_country,
            t3.recipient_country         AS recipient_country,
            t3.recipient_name            AS recipient,
            t3.sender_name               AS sender,
            t1.material_qty              AS material_qty,
            t1.sample_qty                AS sample_qty,
            t7.class_details             AS r_status_class,
            t7.short_name                AS r_status_name,
            t1.request_code              AS r_code,
            t7.code                      AS r_status_code,
            t8.class_details             AS rp_status_class,
            t8.short_name                AS rp_status_name,
            t2.sample_detail             AS sample_detail,
            t2.composite_sample_group_id AS composite_sample_group_id,
            t1.request_status_id         AS request_status_id,
            t1.registered_at::date       AS date_created,
            t2.crop_id,
            t1.request_type_id,      
            CASE
              WHEN t2.material_detail = '0' THEN 'Simple composite sample (' ||(365 - (now()::date - t1.registered_at::date)) || ' days)'
              WHEN t2.material_detail = '1' THEN 'Individual sample (' ||(365 - (now()::date - t1.registered_at::date)) || ' days)'
            ELSE 'No reservation' END AS material_reserve
      FROM   plims.bsns_request t1
            LEFT JOIN plims.bsns_request_process t2
                  ON t1.request_id = t2.request_id
                      AND t2.num_order_id = :num_order_id
            LEFT JOIN plims.bsns_request_detail t3
                  ON t1.request_id = t3.request_id
            LEFT JOIN plims.bsns_crop t4
                  ON t2.crop_id = t4.crop_id
            LEFT JOIN plims.bsns_parameter t5
                  ON t1.request_type_id = t5.parameter_id
            LEFT JOIN plims.auth_user t6
                  ON t3.requested_user = t6.user_id
            LEFT JOIN plims.bsns_parameter t7
                  ON t1.request_status_id = t7.parameter_id
            LEFT JOIN plims.bsns_parameter t8
                  ON t2.request_process_status_id = t8.parameter_id
      WHERE  t1.request_id = :request_id;",
      [
        ':num_order_id' => $num_order_id,
        ':request_id' => $request_id
      ]
    )->queryOne();

    $crop_id = $r_data['crop_id'];
    $request_type_id = $r_data['request_type_id'];

    $request_code = $r_data['r_code'];
    $request_status_code = $r_data['r_status_code'];
    $request_status_name =  $r_data['r_status_name'];
    $request_status_class =  $r_data['r_status_class'];
    $composite_sample_types =  $r_data['sample_detail'];
    $composite_sample_group_id = $r_data['composite_sample_group_id'];
    $request_status_id = $r_data['request_status_id'];

    $date_verified_entries = Yii::$app->db->createCommand(
      'SELECT status_date
      FROM   plims.bsns_status
      WHERE  request_id = :request_id
             AND request_status_id = :request_status_id;',
      [':request_id' => $request_id, ':request_status_id' => Functional::VERIFIED_ENTRIES,]
    )->queryScalar();

    $model_upload_documentation = new UploadForm();
    $data_documentation = Yii::$app->db->createCommand(
      'SELECT t1.*, t2.class_details
        FROM   plims.bsns_request_documentation t1 
        LEFT JOIN plims.bsns_parameter t2 
          ON t1.file_type_id = t2.parameter_id
        WHERE  t1.request_id = :request_id
              AND t1.status = :status
              AND t1.essay_id isnull;',
      [
        ':request_id' => $request_id,
        ':status' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    $data_request_process_assay = Yii::$app->db->createCommand(
      "SELECT 
        t1.request_id,                     
        t1.num_order_id,                   
        t1.essay_id,                       
        t1.composite_sample_type_id,       
        t1.crop_id,                        
        t1.workflow_id,                    
        t1.start_date,                     
        t1.finish_date,                    
        t1.check_date,                     
        t1.agent_qty,                      
        t1.agent_detail,                   
        t1.sub_total_cost_essay,           
        t1.registered_by,                  
        t1.registered_at,                  
        t1.deleted_by,                     
        t1.deleted_at,                     
        t1.status,                         
        t1.num_order,                      
        t1.sample_qty,                     
        ' ['||t1.sample_detail||']' as sample_detail,                  
        t1.request_process_essay_status_id,
        t1.request_process_essay_type_id,  
        t1.activity_qty,                   
        t1.activity_detail,                
        t1.observation,                    
        t1.request_process_essay_root_id,  
        t1.support_master_id,              
        t1.request_process_essay_id,       
        t1.start_by,                       
        t1.finish_by,                      
        t1.check_by,                       
        t1.is_deleted,                     
        t1.code,                           
        t1.code_root, 
        p1.class_details as essay_type_class, 
        p1.description as assay_type_description, 
        t2.short_name as assay_name,
        p2.class_details as request_process_essay_status_class,
        p2.short_name as request_process_essay_status_name,
        t3.short_name as crop_name,
        a0.username as username0,
        a1.username as username1,
        t2.style_class as style_class
      FROM   plims.bsns_request_process_essay t1
        LEFT JOIN plims.bsns_essay t2 on t1.essay_id = t2.essay_id
        LEFT JOIN plims.bsns_parameter p1 on t2.essay_type_id = p1.parameter_id
        LEFT JOIN plims.bsns_parameter p2 on t1.request_process_essay_status_id = p2.parameter_id
        LEFT JOIN plims.bsns_crop t3 on t1.crop_id = t3.crop_id
        LEFT JOIN plims.auth_user a0 on t1.start_by = a0.user_id
        LEFT JOIN plims.auth_user a1 on t1.finish_by = a1.user_id
      WHERE  t1.request_id = :request_id
             AND t1.num_order_id = :num_order_id
             AND t1.composite_sample_type_id = :composite_sample_type_id
             AND t1.status = :status
      ORDER  BY t1.num_order;",
      [
        ':request_id' => $request_id,
        ':num_order_id' => $num_order_id,
        ':composite_sample_type_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
        ':status' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();


    $data_request_process_assay_finished = Yii::$app->db->createCommand(
      "SELECT 
        t1.request_id,                     
        t1.num_order_id,                   
        t1.essay_id,                       
        t1.composite_sample_type_id,       
        t1.crop_id,                        
        t1.workflow_id,                    
        t1.start_date,                     
        t1.finish_date,                    
        t1.check_date,                     
        t1.agent_qty,                      
        t1.agent_detail,                   
        t1.sub_total_cost_essay,           
        t1.registered_by,                  
        t1.registered_at,                  
        t1.deleted_by,                     
        t1.deleted_at,                     
        t1.status,                         
        t1.num_order,                      
        t1.sample_qty,                     
        ' ['||t1.sample_detail||']' as sample_detail,                  
        t1.request_process_essay_status_id,
        t1.request_process_essay_type_id,  
        t1.activity_qty,                   
        t1.activity_detail,                
        t1.observation,                    
        t1.request_process_essay_root_id,  
        t1.support_master_id,              
        t1.request_process_essay_id,       
        t1.start_by,                       
        t1.finish_by,                      
        t1.check_by,                       
        t1.is_deleted,                     
        t1.code,                           
        t1.code_root, 
        p1.class_details as essay_type_class, 
        p1.description as assay_type_description, 
        t2.short_name as assay_name,
        p2.class_details as request_process_essay_status_class,
        p2.short_name as request_process_essay_status_name,
        t3.short_name as crop_name,
        a0.username as username0,
        a1.username as username1,
        t2.style_class as style_class
      FROM   plims.bsns_request_process_essay t1
        LEFT JOIN plims.bsns_essay t2 on t1.essay_id = t2.essay_id
        LEFT JOIN plims.bsns_parameter p1 on t2.essay_type_id = p1.parameter_id
        LEFT JOIN plims.bsns_parameter p2 on t1.request_process_essay_status_id = p2.parameter_id
        LEFT JOIN plims.bsns_crop t3 on t1.crop_id = t3.crop_id
        LEFT JOIN plims.auth_user a0 on t1.start_by = a0.user_id
        LEFT JOIN plims.auth_user a1 on t1.finish_by = a1.user_id
      WHERE  t1.request_id = :request_id
             AND t1.num_order_id = :num_order_id
             AND t1.composite_sample_type_id = :composite_sample_type_id
             AND t1.status = :status
             and t1.request_process_essay_status_id = :_rpa_finished
      ORDER  BY t1.num_order;",
      [
        ':request_id' => $request_id,
        ':num_order_id' => $num_order_id,
        ':composite_sample_type_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
        ':status' => Functional::STATUS_ACTIVE,
        ':_rpa_finished' => Functional::REQUEST_PROCESS_ESSAY_FINISHED,
      ]
    )->queryAll();


    $_user_laboratory = Yii::$app->user->Identity->laboratory;
    $_query = "SELECT DISTINCT e1.essay_id,
                  e1.short_name AS assay_name
              FROM   plims.bsns_agent_condition t1
              INNER JOIN plims.bsns_essay e1
                ON t1.essay_id = e1.essay_id
              WHERE  t1.status = :_status_active
              AND t1.crop_id = :_crop_id
              AND t1.request_type_id = :_request_type_id
              AND t1.essay_id NOT IN (SELECT t2.essay_id
                                  FROM   plims.bsns_request_process_essay t2
                                  WHERE  status = :_status_active
                                          AND t2.request_id = :_request_id
                                          AND t2.num_order_id = :_num_order_id
                                          AND t2.composite_sample_type_id =
                                              :_cst_header_id)
              AND t1.essay_id NOT IN ( :_es_id_1, :_es_id_2 ) ";

    $_params = [
      ':_request_id' => $request_id,
      ':_num_order_id' => $num_order_id,
      ':_cst_header_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
      ':_status_active' => Functional::STATUS_ACTIVE,
      ':_crop_id' => $crop_id,
      ':_request_type_id' => $request_type_id,
      ':_es_id_1' => Functional::ESSAY_FILTRATION_CENTRIFUGATION,
      ':_es_id_2' => Functional::ESSAY_ENDOSPERM_HYDROLYSIS,
    ];


    if ($_user_laboratory) {
      $_query = $_query . "AND t1.laboratory_id = :_laboratory_id ";
      $_params[':_laboratory_id'] = $_user_laboratory;
    }
    $_query = $_query . "ORDER BY assay_name;";
    $data_assay = Yii::$app->db->createCommand($_query, $_params)->queryAll();
    $select_data_1 = ArrayHelper::map($data_assay, 'essay_id', 'assay_name');


    $is_remove_all = false;
    $status_list = array_column($data_request_process_assay, 'request_process_essay_status_id');
    if (in_array(Functional::REQUEST_PROCESS_ESSAY_STATUS_PENDING, $status_list)) {
      $is_remove_all = true;
    }

    $assay_tree_data = Yii::$app->db->createCommand(
      "SELECT t1.request_process_essay_id                      AS id,
          CASE
            WHEN t1.request_process_essay_root_id IS NULL THEN 0
            ELSE t1.request_process_essay_root_id
          END                                              AS id_root,
          t1.code,
          t1.status,
          t1.essay_id                                      AS assay_id,
          e1.short_name                                    AS assay_name,
          t1.composite_sample_type_id                      AS sample_type_id,
          p2.long_name                                    AS sample_type,
          t1.request_process_essay_status_id               AS status_id,
          p1.short_name                                    AS status_name,
          Replace(p1.class_details, 'label label-', 'bg-') AS status_class,
          t1.num_order_id,
          p2.code,
          e1.style_class,
          case 
            when t1.start_date is null and t1.finish_date is null then 'no dates'
            when t1.finish_date is null then '<i class=''fa fa-calendar-o''></i> ' || cast(t1.start_date as text) || ' | no date'
            else '<i class=''fa fa-calendar-o''></i> ' || cast(t1.start_date as text) || ' | ' || '<i class=''fa fa-calendar-check-o''></i> ' || cast(t1.finish_date as text)
          end as dates

      FROM   plims.bsns_request_process_essay t1
            INNER JOIN plims.bsns_essay e1
                    ON t1.essay_id = e1.essay_id
            INNER JOIN plims.bsns_parameter p1
                    ON t1.request_process_essay_status_id = p1.parameter_id
            INNER JOIN plims.bsns_parameter p2
                    ON t1.composite_sample_type_id = p2.parameter_id
      WHERE  t1.request_id = :_request_id
            AND ( ( t1.composite_sample_type_id = :_composite_sample_type_header AND t1.num_order_id = :_num_order_ini )
                  OR ( t1.composite_sample_type_id <> :_composite_sample_type_header ) )
            AND t1.status = :_status_active
      ORDER  BY t1.num_order, t1.num_order_id, p2.code;",
      [
        ':_request_id' => $request_id,
        ':_composite_sample_type_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
        ':_num_order_ini' => functional::NUM_ORDER_ID_PROCESS,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    $assay_tree_data_lvl = [];

    foreach ($assay_tree_data as $key => $value) {

      $assay_id = $value['assay_id'];
      $id_main  = $value['id'];
      $sample_type_id = $value['sample_type_id'];
      $id_root = $value['id_root'];
      $style_class = $value['style_class'];
      $status_class = $value['status_class'];
      $status_name = $value['status_name'];

      $dates =  $value['dates'];

      if ($sample_type_id == Functional::COMPOSITE_SAMPLE_TYPE_HEADER) {
        // assay information
        $assay_tree_data_lvl[$assay_id] = [
          'body' => '<span class="' . $style_class . '" style="font-weight: bold; font-size: 1.2em;">' . $value['assay_name'] . '</span>
          <a href="' . Url::toRoute(
            [
              'manage-assay/management',
              'request_id' => $request_id,
              'essay_id' => $assay_id
            ]
          ) . '" > go to assay
          </a>
            <label title="Status of the assay" class="label ' . $status_class . '">' . $status_name . '</label>

            <small style="font-size: 0.75em; color: #aaa">' . $dates . '</small>',
          'items' => [],
        ];
      } else if ($id_root == 0) {
        // NORMAL-PROCESS from assay and a sample type information
        $assay_tree_data_lvl[$assay_id]['items'][] =   [
          'body' => '<span class="' . $style_class . '">' . $value['sample_type'] . '</span>
            <a href="' . Url::toRoute(
            [
              'manage-sample-results/management',
              'request_id' => $request_id,
              'num_order_id' => $num_order_id,
              'essay_id' => $assay_id,
              'composite_sample_type_id' => $sample_type_id,
              'request_process_essay_id' => $id_main,
            ]
          ) . '"   target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i>
          </a>
            <label title="Status of the assay" class="label ' . $status_class . '">' . $status_name . '</label>

            <small style="font-size: 0.75em; color: #aaa">' . $dates . '</small>',
          // RE-PROCESS
          'items' => $this->getItems($assay_tree_data, $id_main, $request_id),
        ];
      }
    }

    $is_ready_to_finish = Yii::$app->db->createCommand(
      "SELECT CASE
            WHEN rpa_status = :_rpa_finished THEN true
            ELSE false
          end AS is_ready_to_finish
      FROM   (SELECT DISTINCT request_process_essay_status_id AS rpa_status
          FROM   plims.bsns_request_process_essay
          WHERE  request_id = :_request_id
                  AND status = :_status_active
          ORDER  BY 1 ASC
          LIMIT  1) tab ",
      [
        ':_rpa_finished' => Functional::REQUEST_PROCESS_ESSAY_FINISHED,
        ':_request_id' => $request_id,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryScalar();


    return $this->render(
      'management',
      [
        'request_id' => $request_id,
        'r_data' => $r_data,
        'request_code' => $request_code,
        'request_status_code' => $request_status_code,
        'request_status_name' => $request_status_name,
        'request_status_class' => $request_status_class,
        'composite_sample_types' => $composite_sample_types,
        'composite_sample_group_id' => $composite_sample_group_id,
        'model_upload_documentation' => $model_upload_documentation,
        'data_documentation' => $data_documentation,
        'data_request_process_assay' => $data_request_process_assay,
        'data_request_process_assay_finished' => $data_request_process_assay_finished,
        'select_data_1' => $select_data_1,
        'date_verified_entries' => $date_verified_entries,
        'request_status_id' => $request_status_id,
        'is_remove_all' => $is_remove_all,
        'num_order_id' => $num_order_id,
        'assay_tree_data' => $assay_tree_data,
        'assay_tree_data_lvl' => $assay_tree_data_lvl,
        'is_ready_to_finish' => $is_ready_to_finish,
      ]
    );
  }


  private function getItems($array_data, $id_main, $request_id)
  {
    $result = [];
    $array_data_filtered = $this->getFilteredItems($array_data, $id_main);
    foreach ($array_data_filtered as $key => $value) {

      $num_order_id  = $value['num_order_id'];
      $assay_id = $value['assay_id'];
      $id_main  = $value['id'];
      $sample_type_id = $value['sample_type_id'];
      $style_class = $value['style_class'];
      $status_class = $value['status_class'];
      $status_name = $value['status_name'];

      $dates =  $value['dates'];

      $result[] = [
        'body' => '<span class="' . $style_class . '">' . $value['sample_type'] . '</span>
          
          <a href="' . Url::toRoute(
          [
            'manage-sample-results/management',
            'request_id' => $request_id,
            'num_order_id' => $num_order_id,
            'essay_id' => $assay_id,
            'composite_sample_type_id' => $sample_type_id,
            'request_process_essay_id' => $id_main,
          ]
        ) . '"   target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a>

          re-process
          <label title="Status of the assay" class="label ' . $status_class . '">' . $status_name . '</label>

          <small style="font-size: 0.75em; color: #aaa">' . $dates . '</small>',
        'items' => $this->getItems($array_data, $id_main, $request_id),
      ];
    }
    return $result;
  }

  private function getFilteredItems($array_data, $rpe_id_root)
  {
    $result = [];
    foreach ($array_data as $key => $value) {
      if ($value['id_root'] ==  $rpe_id_root) {
        $result[] = $value;
      }
    }
    return $result;
  }

  public function actionRemove($request_id)
  {
    Script::instance()->webRequestRemove($request_id);

    return $this->redirect(
      [
        'index'
      ]
    );
  }

  // Request Detail
  public function actionShowStatus($request_id, $request_status_id)
  {
    $model = Status::findOne(['request_id' => $request_id, 'request_status_id' => $request_status_id]);
    if ($this->request->isPost && $model->load($this->request->post())) {
      $model->save();
      sleep(3);
      return $this->redirect(
        [
          'management',
          'request_id' => $request_id,
        ]
      );
    }

    $s_data = Yii::$app->db->createCommand(
      'SELECT r1.request_code        AS "s_request_code",
            r1.request_type_id     AS "s_request_type",
            c1.short_name          AS "s_crop_name",
            t1.number_index        AS "s_number_index",
            t1.request_status_id   AS "s_request_status_id",
            t1.status_date :: DATE AS "s_date",
            t1.status_date :: TIME AS "s_time",
            t3.username            AS "s_username",
            t1.observation         AS "s_observation",
            t2.code                AS "s_code",
            t2.short_name          AS "s_name",
            t2.long_name           AS "s_name_title",
            t2.description         AS "s_description",
            t2.class_details       AS "s_class_details"
      FROM   plims.bsns_request r1
            left join plims.bsns_request_detail r2
                  ON r1.request_id = r2.request_id
            left join plims.bsns_crop c1
                  ON r2.crop_id = c1.crop_id
            left join plims.bsns_status t1
                  ON r1.request_id = t1.request_id
            left join plims.bsns_parameter t2
                  ON t1.request_status_id = t2.parameter_id
            left join plims.auth_user t3
                  ON t1.status_by = t3.user_id
      WHERE  t1.request_id = :request_id
            AND t2.status = :status
      ORDER  BY t1.number_index DESC;',
      [':request_id' => $request_id, ':status' => Functional::STATUS_ACTIVE,]
    )->queryAll();

    $s_data_last = $s_data[0];
    $s_request_type = $s_data_last['s_request_type'];

    if ($s_request_type == Functional::REQUEST_TYPE_SHIPMENT_DISTRIBUTION) {
      $s_colums_recipient = [
        'recipient_name',
        'recipient_institution',
        'recipient_address',
        'recipient_country',
      ];
      $s_colums_sender = [
        'sender_name',
        'sender_program',
        'sender_account',
      ];
    } else if ($s_request_type == Functional::REQUEST_TYPE_INTRODUCTION) {
      $s_colums_recipient = [
        'recipient_name',
        'recipient_program',
        'recipient_account',
      ];
      $s_colums_sender = [
        'sender_name',
        'sender_institution',
        'sender_address',
        'sender_country',
        // ----------
        'international_phytosanitary_certificate',
        'quarantine_custody',
        'requirements_sheet_folio',
        'total_shipping_weight',
      ];
    }

    $s_data_aux = Yii::$app->db->createCommand(
      'SELECT r2.recipient_name,
            r2.recipient_program,
            r2.recipient_account,
            r2.recipient_institution,
            r2.recipient_address,
            r2.recipient_country,
            ----------------------------
            r2.sender_name,
            r2.sender_program,
            r2.sender_account,
            r2.sender_institution,
            r2.sender_address,
            r2.sender_country,
            ----------------------------
            r2.international_phytosanitary_certificate,
            r2.quarantine_custody,
            r2.requirements_sheet_folio,
            r2.total_shipping_weight
      FROM   plims.bsns_request r1
            INNER JOIN plims.bsns_request_detail r2
                    ON r1.request_id = r2.request_id
      WHERE  r2.request_id = :request_id
            AND r1.status = :status; ',
      [':request_id' => $request_id, ':status' => Functional::STATUS_ACTIVE,]
    )->queryOne();

    return $this->renderAjax('_modal-request-status-log', [
      'request_id' => $request_id,
      's_data' => $s_data,
      's_data_last' => $s_data_last,
      's_data_aux' => $s_data_aux,
      's_colums_recipient' => $s_colums_recipient,
      's_colums_sender' => $s_colums_sender,
      'model' => $model
    ]);
  }

  public function actionFinish($request_id)
  {

    $user = Yii::$app->user->identity->id;
    $date = date("Y-m-d H:i:s", time());

    Yii::$app->db->createCommand(
      "UPDATE plims.bsns_request
        SET    request_status_id = :_r_finished
        WHERE  request_id = :_request_id
               AND status = :_status_active
               AND request_status_id = :_rs_request_in_process; ",
      [
        ':_request_id' => $request_id,
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_r_finished' => Functional::REQUEST_FINISHED,
        ':_rs_request_in_process' => Functional::REQUEST_IN_PROCESS,
      ]
    )->execute();

    Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_status
              (request_id,
              request_status_id,
              number_index,
              status_date,
              status_by,
              observation)
        SELECT :_request_id                         AS request_id,
          :_r_finished                       AS request_status_id,
          number_index + 1                     AS number_index,
          :_start_date                          AS status_date,
          :_start_by                            AS status_by,
          'Completion of test analysis processes' AS observation
        FROM   plims.bsns_status
        WHERE  request_id = :_request_id
        AND request_status_id = :_r_in_progress
        ON CONFLICT (request_id, request_status_id) DO NOTHING;",
      [
        ':_request_id' => $request_id,

        ':_r_finished' => Functional::REQUEST_FINISHED,
        ':_start_by' => $user,
        ':_start_date' => $date,
        ':_r_in_progress' => Functional::REQUEST_IN_PROCESS,
      ]
    )->execute();

    $this->generateDetail($request_id);

    $this->generateResult($request_id);

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }

  // Request Documentation
  public function actionUploadDocument($request_id, $num_order_id)
  {
    $model_upload_documentation = new UploadForm();

    if (Yii::$app->request->isPost) {
      $serial =  "R" . $request_id . ".O" . $num_order_id;
      $model_upload_documentation->mixFiles = UploadedFile::getInstances($model_upload_documentation, 'mixFiles');
      if ($model_upload_documentation->upload("_" . $serial, $request_id)) {
      }
    }

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }
  public function actionRemoveDocument($request_id, $request_documentation_id)
  {
    try {
      if (($model = RequestDocumentation::findOne($request_documentation_id)) !== null) {
        $document_name = $model->path_documentation . $model->document_name . '.' . $model->document_extension;
        unlink($document_name);
        $model->status = 'disable';
        $model->save();
      }
    } catch (\Throwable $th) {
      echo "<pre>";
      print_r($th);
      echo "</pre>";
      die();
    }

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }


  // Assay Management
  public function actionAddEssay($request_id)
  {
    $num_order_id = Functional::NUM_ORDER_ID_PROCESS;

    $rpv_array = Yii::$app->db->createCommand(
      "SELECT t2.crop_id,
            t2.workflow_id,
            t2.composite_sample_group_id,
            t2.sample_qty,
            t1.request_type_id
      FROM   plims.bsns_request t1
            INNER JOIN plims.bsns_request_process t2
                    ON t1.request_id = t2.request_id
      WHERE  t1.request_id = :request_id
            AND t2.num_order_id = :num_order_id;",
      [
        ':request_id' => $request_id,
        ':num_order_id' => $num_order_id
      ]
    )->queryOne();

    $crop_id = $rpv_array['crop_id'];
    $workflow_id = $rpv_array['workflow_id'];
    $sample_qty = $rpv_array['sample_qty'];
    $request_type_id = $rpv_array['request_type_id'];

    if ($sample_qty > 0) {
      $select_name_1 = Yii::$app->request->post('select_name_1');
      if (!empty($select_name_1)) {
        foreach ($select_name_1 as $key => $essay_id) {
          if (isset($essay_id)) {
            $this->setAssay($request_id, $num_order_id, $crop_id, $workflow_id, $essay_id);
            if ($essay_id == Functional::ESSAY_WASHING) {

              $column_assay = Yii::$app->db->createCommand(
                "SELECT DISTINCT e1.essay_id
                FROM   plims.bsns_agent_condition t1
                INNER JOIN plims.bsns_essay e1
                    ON t1.essay_id = e1.essay_id
                WHERE  t1.status = :_status_active
                AND t1.crop_id = :_crop_id
                AND t1.request_type_id = :_request_type_id;",
                [
                  ':_status_active' => Functional::STATUS_ACTIVE,
                  ':_crop_id' => $crop_id,
                  ':_request_type_id' => $request_type_id,
                ]
              )->queryColumn();

              $assay_id = Functional::ESSAY_FILTRATION_CENTRIFUGATION;
              if (in_array($assay_id, $column_assay))
                $this->setAssay($request_id, $num_order_id, $crop_id, $workflow_id, Functional::ESSAY_FILTRATION_CENTRIFUGATION);
              $assay_id = Functional::ESSAY_ENDOSPERM_HYDROLYSIS;
              if (in_array($assay_id, $column_assay))
                $this->setAssay($request_id, $num_order_id, $crop_id, $workflow_id, Functional::ESSAY_ENDOSPERM_HYDROLYSIS);
            }
          }
        }
      } else {
        Yii::$app->session->setFlash('info', \Yii::t(
          'app',
          'At least one essay must be chosen'
        ));
      }


      $request_status_id = Functional::ASSAY_GENERATED;
      $observation = 'Request generada desde la plataforma PLIMS WEB';
      $this->setStatusRequest($request_id, $request_status_id, $observation);
    }

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }

  private function setAssay($request_id, $num_order_id, $crop_id, $workflow_id, $essay_id)
  {
    $user = Yii::$app->user->identity->id;
    $date =  date("Y-m-d H:i:s", time());

    $essay_type_id = Yii::$app->db->createCommand(
      'SELECT essay_type_id
      FROM   plims.bsns_essay
      WHERE  essay_id = :essay_id;',
      [':essay_id' => $essay_id]
    )->queryScalar();

    $num_order = Yii::$app->db->createCommand(
      'SELECT COALESCE(Max(num_order), 0) + 1 AS num_order
      FROM   plims.bsns_request_process_essay
      WHERE  request_id = :request_id;',
      [':request_id' => $request_id]
    )->queryScalar();

    $code = Yii::$app->db->createCommand(
      "SELECT 'SH'
            || Substring(Cast(r1.year_code AS TEXT), 3, 2)
            || RIGHT('000'
                    || Cast(r1.sequential_code AS TEXT), 3)
            || Cast(:_num_order_id AS TEXT) AS code
      FROM   plims.bsns_request r1
      WHERE  request_id = :_request_id;",
      [
        ':_num_order_id' => $num_order_id,
        ':_request_id' => $request_id,
      ]
    )->queryScalar();

    $assay_abb = Yii::$app->db->createCommand(
      "SELECT abbreviation as assay_abb
        FROM   plims.bsns_essay
        WHERE  essay_id = :_assay_id;",
      [
        ':_assay_id' => $essay_id,
      ]
    )->queryScalar();

    $code = $code . $assay_abb;

    Yii::$app->db->createCommand()->insert(
      'plims.bsns_request_process_essay',
      [
        'request_id' => $request_id,
        'num_order_id' => $num_order_id,
        'crop_id' => $crop_id,
        'workflow_id' => $workflow_id,
        'composite_sample_type_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
        'essay_id' => $essay_id,
        'num_order' => $num_order,
        'request_process_essay_status_id' => Functional::REQUEST_PROCESS_ESSAY_STATUS_PENDING,
        'registered_by' => $user,
        'registered_at' => $date,
        'status' => Functional::STATUS_ACTIVE,
        'code' => $code,
      ]
    )->execute();

    if ($essay_type_id == Functional::ESSAY_TYPE_MOB_APP) {

      Script::instance()->mobTemplateSettings($user, $date, $request_id, $num_order_id, $essay_id, $num_order);

      $sample_detail = Yii::$app->db->createCommand(
        "SELECT String_agg(distinct sample_detail, ' | ')
        FROM   plims.bsns_request_process_essay
        WHERE  request_id = :_request_id
                AND essay_id = :_assay_id
                AND num_order_id = :_num_order_id;",
        [
          ':_request_id' => $request_id,
          ':_assay_id' => $essay_id,
          ':_num_order_id' => $num_order_id,
        ]
      )->queryScalar();

      Yii::$app->db->createCommand(
        "UPDATE plims.bsns_request_process_essay
        SET    sample_detail = :_sample_detail
        WHERE  request_id = :_request_id
               AND essay_id = :_assay_id
               AND num_order_id = :_num_order_id
               AND composite_sample_type_id = :_cst_header; ",
        [
          ':_sample_detail' => $sample_detail,
          ':_request_id' => $request_id,
          ':_assay_id' => $essay_id,
          ':_num_order_id' => $num_order_id,
          ':_cst_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
        ]
      )->execute();
    }
  }

  public function actionRemoveAssay($request_id, $essay_id)
  {
    $num_order_id = Functional::NUM_ORDER_ID_PROCESS;

    $request_process_essay_head_model =  RequestProcessEssay::find()->where(
      [
        'request_id' => $request_id,
        'num_order_id' => $num_order_id,
        'essay_id' => $essay_id,
        'composite_sample_type_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
        'request_process_essay_status_id' => Functional::REQUEST_PROCESS_ESSAY_STATUS_PENDING,
      ]
    )->exists();

    if ($request_process_essay_head_model) {

      RequestProcessEssayActivitySample::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
          'essay_id' => $essay_id,
        ]
      );

      Support::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
          'essay_id' => $essay_id,
        ]
      );

      RequestProcessEssayActivity::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
          'essay_id' => $essay_id,
        ]
      );

      RequestProcessEssayAgent::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
          'essay_id' => $essay_id,
        ]
      );

      RequestProcessEssay::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
          'essay_id' => $essay_id,
        ]
      );
    }

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }
  public function actionRemoveAllAssay($request_id, $composite_sample_group_id)
  {
    $num_order_id = Functional::NUM_ORDER_ID_PROCESS;
    $is_not_all_pending = RequestProcessEssay::find()
      ->where(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
          'composite_sample_type_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
        ]
      )->andWhere(
        [
          '<>', 'request_process_essay_status_id', Functional::REQUEST_PROCESS_ESSAY_STATUS_PENDING
        ]
      )->exists();

    if ($is_not_all_pending) {
      $request_process_essay_list = RequestProcessEssay::find()
        ->where(
          [
            'request_id' => $request_id,
            'num_order_id' => $num_order_id,
            'composite_sample_type_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
            'request_process_essay_status_id' => Functional::REQUEST_PROCESS_ESSAY_STATUS_PENDING
          ]
        )->asArray()->all();
      foreach ($request_process_essay_list as $key => $request_process_essay) {

        RequestProcessEssayActivitySample::deleteAll(
          [
            'request_id' => $request_id,
            'num_order_id' => $num_order_id,
            'essay_id' => $request_process_essay['essay_id'],
          ]
        );

        Support::deleteAll(
          [
            'request_id' => $request_id,
            'num_order_id' => $num_order_id,
            'essay_id' => $request_process_essay['essay_id'],
          ]
        );

        RequestProcessEssayActivity::deleteAll(
          [
            'request_id' => $request_id,
            'num_order_id' => $num_order_id,
            'essay_id' => $request_process_essay['essay_id'],
          ]
        );

        RequestProcessEssayAgent::deleteAll(
          [
            'request_id' => $request_id,
            'num_order_id' => $num_order_id,
            'essay_id' => $request_process_essay['essay_id'],
          ]
        );

        RequestProcessEssay::deleteAll(
          [
            'request_id' => $request_id,
            'num_order_id' => $num_order_id,
            'essay_id' => $request_process_essay['essay_id'],
          ]
        );
      }
    } else {
      CompositeSample::deleteAll(
        [
          'composite_sample_group_id' => $composite_sample_group_id,
          'num_order' => null,
        ]
      );

      RequestProcessEssayActivitySample::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
        ]
      );

      Support::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
        ]
      );

      RequestProcessEssayActivity::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
        ]
      );

      RequestProcessEssayAgent::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
        ]
      );

      RequestProcessEssay::deleteAll(
        [
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
        ]
      );
    }

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }


  // Util Status
  public function setStatusRequest($request_id, $request_status_id, $observation)
  {
    $number_index = Yii::$app->db->createCommand(
      "SELECT COALESCE(Max(number_index) + 1, 1) AS number_index
    FROM   plims.bsns_status
    WHERE  request_id = :request_id;",
      [':request_id' => $request_id]
    )->queryScalar();

    try {
      Yii::$app->db->createCommand()->insert(
        'plims.bsns_status',
        [
          'request_id' => $request_id,
          'request_status_id' => $request_status_id,
          'number_index' => $number_index,
          'status_date' => date("Y-m-d H:i:s", time()),
          'status_by' => Yii::$app->user->identity->id,
          'observation' => $observation,
        ]
      )->execute();

      Yii::$app->db->createCommand()->update(
        'plims.bsns_request',
        [
          'request_status_id' => $request_status_id,
        ],
        'request_id = :request_id',
        [':request_id' => $request_id]
      )->execute();
    } catch (\Throwable $th) {
    }
  }



  private function generateDetail($request_id)
  {
    $_cs_group_id = Yii::$app->db->createCommand(
      "SELECT DISTINCT composite_sample_group_id
      FROM   plims.bsns_request_process
      WHERE  request_id = :_request_id; ",
      [
        ':_request_id' => $request_id,
      ]
    )->queryScalar();

    Yii::$app->db->createCommand(
      "DELETE FROM plims.bsns_consolidated_information
      WHERE  request_id = :_request_id; ",
      [
        ':_request_id' => $request_id
      ]
    )->execute();


    Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_consolidated_information
      (
            request_id,
            num_order,
            general_sample_id,
            general_sample_name,
            simple_sample_id,
            simple_sample_name,
            indiviual_sample_id,
            indiviual_sample_name,
            final_decision,
            automatic_decision,
            final_result,
            release_status_id
      )
      
      SELECT :_request_id           AS request_id,
             t1.num_order,
             t3.composite_sample_id AS general_sample_id,
             t3.short_name          AS general_sample_name,
             t2.composite_sample_id AS simple_sample_id,
             t2.short_name          AS simple_sample_name,
             t1.composite_sample_id AS indiviual_sample_id,
             t1.short_name          AS indiviual_sample_name,
             true                   AS final_decision,
             true                   AS automatic_decision,
             true                   AS final_result,
             :_rs_pending       AS release_status_id
      FROM   plims.bsns_composite_sample t1
             INNER JOIN plims.bsns_material_group m1
                     ON t1.material_group_id = m1.material_group_id
                        AND t1.composite_sample_group_id = m1.composite_sample_group_id
             INNER JOIN plims.bsns_composite_sample t2
                     ON t1.composite_sample_group_id = t2.composite_sample_group_id
                        AND m1.number_index = t2.num_order
             INNER JOIN plims.bsns_material_group m2
                     ON t2.material_group_id = m2.material_group_id
                        AND t1.composite_sample_group_id = m1.composite_sample_group_id
             INNER JOIN plims.bsns_composite_sample t3
                     ON t1.composite_sample_group_id = t3.composite_sample_group_id
                        AND m2.number_index = t3.num_order
      WHERE  t1.composite_sample_group_id = :_cs_group_id
             AND t1.composite_sample_type_id = :_cst_individual
             AND t1.reading_data_type_id = :_rdt_entry
             AND t2.composite_sample_type_id = :_cst_simple
             AND t2.reading_data_type_id = :_rdt_entry
             AND t3.composite_sample_type_id = :_cst_general
             AND t3.reading_data_type_id = :_rdt_entry
      ORDER  BY t1.num_order; ",
      [
        ':_request_id' => $request_id,
        ':_rs_pending' => Functional::RELEASE_STATUS_PENDING,
        ':_cs_group_id' => $_cs_group_id,
        ':_cst_individual' =>  Functional::COMPOSITE_SAMPLE_INDIVIDUAL,
        ':_rdt_entry' =>  Functional::READING_DATA_ENTRY,
        ':_cst_simple' => Functional::COMPOSITE_SAMPLE_SIMPLE,
        ':_cst_general' => Functional::COMPOSITE_SAMPLE_GENERAL,
      ]
    )->execute();
  }

  private function generateResult($request_id)
  {
    Yii::$app->db->createCommand(
      "DELETE FROM plims.bsns_consolidated_result
      WHERE  request_id = :_request_id; ",
      [
        ':_request_id' => $request_id
      ]
    )->execute();


    Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_consolidated_result
      (
            request_id,
            composite_sample_id,
            essay_id,
            agent_id,
            text_result,
            text_sample,
            sample_order,
            assay_class,
            assay_name,
            agent_name
      )
      SELECT tab.request_id,
             tab.composite_sample_id,
             tab.essay_id,
             tab.agent_id,
             tab.text_result,
             tab.text_sample,
             tab.sample_order,
             tab.assay_class,
             tab.assay_name,
             tab.agent_name
      FROM   (SELECT DISTINCT t1.request_id,
                              t1.essay_id,
                              e1.short_name as assay_name,
                              e1.style_class as assay_class,
                              t1.agent_id,
                              a1.short_name as agent_name,
                              t1.composite_sample_id,
                              p1.short_name as  text_sample,
                              p1.code as sample_order,
                              t1.auxiliary_result         AS text_result,
                              t1.num_order_id,
                              t1.primary_order_num        AS sample_index,
                              t1.secondary_order_num      AS agent_index,
                              t1.request_process_essay_id AS creation_index
              FROM   plims.bsns_reading_data t1
                     INNER JOIN plims.bsns_activity_by_essay t2
                             ON t1.activity_id = t2.activity_id
                                AND t2.is_template
                     inner join plims.bsns_parameter p1
                              on t1.composite_sample_type_id = p1.parameter_id 
                              inner join plims.bsns_essay e1 on t1.essay_id = e1.essay_id 
                              inner join plims.bsns_agent a1 on t1.agent_id = a1.agent_id 
              WHERE  t1.request_id = :_request_id
                     AND t1.status = :_status_active
                     AND t1.reading_data_type_id = :_rdt_entry
              ORDER  BY t1.essay_id ASC,
                        t1.request_process_essay_id ASC,
                        t1.primary_order_num ASC,
                        t1.secondary_order_num ASC) TAB;",
      [
        ':_request_id' => $request_id,
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_rdt_entry' => Functional::READING_DATA_ENTRY,
      ]
    )->execute();
  }
}

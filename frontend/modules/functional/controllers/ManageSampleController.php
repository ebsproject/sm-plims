<?php

namespace frontend\modules\functional\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use frontend\modules\functional\models\MaterialGroup;
use frontend\modules\functional\models\Material;
use frontend\modules\functional\models\MaterialSearch;
use frontend\modules\functional\models\Request;
use frontend\modules\functional\models\RequestProcess;
use frontend\modules\functional\Functional;
use yii\filters\AccessControl;

class ManageSampleController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger', \Yii::t('app', 'You do not have sufficient permissions to access this website'));
          return $this->goHome();
        },
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }



  public function actionManagement($request_id)
  {
    $params = Yii::$app->request->queryParams;
    $num_order_id = Functional::NUM_ORDER_ID_PROCESS;


    $r_model = Request::findOne($request_id);
    $request_code = $r_model->request_code;
    $request_status_code = $r_model->requestStatus->code;


    $rp_model = RequestProcess::findOne(['request_id' => $request_id, 'num_order_id' => $num_order_id]);
    $composite_sample_group_id = $rp_model->composite_sample_group_id;


    $order_list = ['num_order' => SORT_ASC];
    $filterModel = new MaterialSearch();
    $dataProvider = $filterModel->search($params, $composite_sample_group_id, $order_list, Functional::STATUS_ACTIVE);


    $order_list = ['registered_at' => SORT_DESC, 'num_order' => SORT_ASC];
    $filterModelH = new MaterialSearch();
    $dataProviderH = $filterModelH->search($params, $composite_sample_group_id, $order_list);

    $data_material = Yii::$app->db->createCommand(
      "SELECT material_group_id,
          number_index,
          group_name,
          material_qty,
          material_unit_qty,
          sample_group_name,
          num_order_ini,
          material_name_ini,
          num_order_end,
          material_name_end,
          composite_sample_group_id
        FROM   plims.bsns_material_group
          WHERE  composite_sample_group_id = :_composite_sample_group_id
            AND group_name <> 'G0'
        ORDER  BY number_index ASC;",
      [
        ':_composite_sample_group_id' => $composite_sample_group_id
      ]
    )->queryAll();

    $material_reserve_value = is_null($rp_model->material_detail) ? 2 : $rp_model->material_detail;
    // guardar este dato en la tabla de request detail
    $session = Yii::$app->session;
    $var1 = Yii::$app->session->get('var.v1');
    $var2 = Yii::$app->session->get('var.v2');
    $var3 = Yii::$app->session->get('var.v3');

    unset($session['var.v1']);
    unset($session['var.v2']);
    unset($session['var.v3']);

    return $this->render(
      'management',
      [
        'request_id' => $request_id,
        'request_code' => $request_code,
        'request_status_code' => $request_status_code,

        'filterModel' => $filterModel,
        'dataProvider' => $dataProvider,

        'filterModelH' => $filterModelH,
        'dataProviderH' => $dataProviderH,

        'data_material' => $data_material,
        'composite_sample_group_id' => $composite_sample_group_id,

        'material_reserve_value' => $material_reserve_value,

        'var1' => $var1,
        'var2' => $var2,
        'var3' => $var3,
      ]
    );
  }



  // Material Check-in
  public function actionDelete($request_id, $material_id)
  {
    $new_material_id = $this->getNewVersion($material_id);

    Yii::$app->db->createCommand()->update(
      'plims.bsns_material',
      [
        'material_status_id' => Functional::MATERIAL_DELETED,
        'details' => 'Update: material removed',
      ],
      'material_id = :material_id'
    )->bindParam(':material_id', $new_material_id)
      ->execute();

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }
  public function actionControl($request_id, $material_id)
  {
    $new_material_id = $this->getNewVersion($material_id);

    Yii::$app->db->createCommand()->update(
      'plims.bsns_material',
      [
        'material_status_id' => Functional::MATERIAL_CONTROL,
        'details' => 'Update: control material',
      ],
      'material_id = :material_id'
    )->bindParam(':material_id', $new_material_id)
      ->execute();

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }
  public function actionUndo($request_id, $material_id)
  {
    $new_material_id = $this->getNewVersion($material_id);

    Yii::$app->db->createCommand()->update(
      'plims.bsns_material',
      [
        'material_status_id' => Functional::MATERIAL_NORMAL,
        'details' => 'Update: data reversion',
      ],
      'material_id = :material_id'
    )->bindParam(':material_id', $new_material_id)
      ->execute();

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }
  public function actionUpdate($request_id, $material_id)
  {
    $m_model = Material::findOne($material_id);
    if ($m_model->load(Yii::$app->request->post())) {
      $new_material_id = $this->getNewVersion($material_id);
      Yii::$app->db->createCommand()->update(
        'plims.bsns_material',
        [
          'material_unit_qty' => $m_model->material_unit_qty,
          'material_status_id' => $m_model->material_status_id,
          'observation' => $m_model->observation,
          'details' => $m_model->details,
        ],
        'material_id = :material_id'
      )->bindParam(':material_id', $new_material_id)
        ->execute();

      return $this->redirect(
        [
          'management',
          'request_id' => $request_id,
        ]
      );
    }

    return $this->renderAjax('_modal-material-edit', [
      'material_model' => $m_model,
    ]);
  }
  public function actionConfirmedMaterial($request_id, $composite_sample_group_id)
  {

    // confirmacion de entrada de material
    $num_order_id = Functional::NUM_ORDER_ID_PROCESS;
    $cbx_quarantine = Yii::$app->request->post('cbx_quarantine');
    $year_code = (int) date("Y");
    $status_active =  Functional::STATUS_ACTIVE;

    // doy de baja a todos los que estan en estado eliminado o control en materiales
    Yii::$app->db->createCommand()->update(
      'plims.bsns_material',
      ['status' => Functional::STATUS_DISABLED],
      [
        'in', 'material_status_id', [Functional::MATERIAL_DELETED, Functional::MATERIAL_CONTROL],
        'composite_sample_group_id' => $composite_sample_group_id,
      ]
    )->execute();

    // obtener las cantidades de unidades
    $mq_array = Yii::$app->db->createCommand(
      'SELECT Count(material_unit_qty) AS material_qty,
                Sum(material_unit_qty)   AS material_unit_qty
        FROM   plims.bsns_material
        WHERE  composite_sample_group_id = :composite_sample_group_id
                AND status = :status;'
    )->bindParam(':composite_sample_group_id', $composite_sample_group_id)
      ->bindParam(':status', $status_active)
      ->queryOne();

    $material_qty = $mq_array['material_qty'];
    $material_unit_qty = $mq_array['material_unit_qty'];

    $request_process_model = RequestProcess::findOne(['request_id' => $request_id, 'num_order_id' => $num_order_id,]);
    $request_process_model->material_qty =  $material_qty;
    $request_process_model->material_unit_qty = $material_unit_qty;
    $request_process_model->save();

    $request_model = Request::findOne($request_id);
    $request_model->material_qty = $material_qty;
    $request_model->material_unit_qty = $material_unit_qty;
    $request_model->year_code = $year_code;

    if (isset($cbx_quarantine)) {

      // actualiza el status del request en stock por quarentena
      $sequential_code = Yii::$app->db->createCommand(
        "SELECT RIGHT('000' || Cast(COALESCE(Max(sequential_code) + 1, 1) AS TEXT), 3) AS sequential_code
          FROM   plims.bsns_request
          WHERE  status = :status
            AND year_code = :year_code
            AND request_status_id = :request_status_id;",
        [
          ':status' => $status_active,
          ':year_code' => $year_code,
          ':request_status_id' => Functional::REQUEST_IN_STOCK,
        ]
      )->queryScalar();

      $request_model->sequential_code  = (int) $sequential_code;
      $request_model->request_code = 'RF' . substr($year_code, -2) . '-' . $sequential_code;

      $request_status_id = Functional::REQUEST_IN_STOCK;
      $observation = 'The entry into stock of ' . $material_qty . ' materials with a total of ' . $material_unit_qty . ' units was verified.';
      $this->setStatusRequest($request_id, $request_status_id, $observation);
    } else {

      $_user_laboratory = Yii::$app->user->identity->laboratory;

      $request_prefix = Yii::$app->db->createCommand(
        "SELECT request_prefix
        FROM   plims.bsns_laboratory
        WHERE  laboratory_id = :_laboratory_id;",
        [
          ':_laboratory_id' => $_user_laboratory,
        ]
      )->queryScalar();

      // actualiza el status del request en proceso
      $sequential_code = Yii::$app->db->createCommand(
        "SELECT RIGHT('000' || Cast(COALESCE(Max(sequential_code) + 1, 1) AS TEXT), 3) AS sequential_code
                  FROM   plims.bsns_request
                  WHERE  status = :status 
                  AND year_code = :year_code
                  AND request_status_id <> :request_status_id;",
        [
          ':status' => $status_active,
          ':year_code' => $year_code,
          ':request_status_id' => Functional::REQUEST_IN_STOCK,
        ]
      )->queryScalar();

      $request_model->sequential_code  = (int) $sequential_code;
      $request_model->request_code = $request_prefix . substr($year_code, -2) . '-' . $sequential_code;

      $request_status_id = Functional::VERIFIED_ENTRIES;
      $observation = 'The entry of ' . $material_qty . ' materials with a total of ' . $material_unit_qty . ' units was verified.';
      $this->setStatusRequest($request_id, $request_status_id, $observation);
      // actualiza los indices de orden de la tabla material
      $material_status_id = Functional::MATERIAL_NORMAL;
      Yii::$app->db->createCommand(
        'UPDATE plims.bsns_material
          SET    "num_order" = t.rn, "material_status_id" = :material_status_id
          FROM   (SELECT material_id, Row_number() OVER ( ORDER BY num_order) AS rn
                  FROM   plims.bsns_material
                  WHERE  composite_sample_group_id = :composite_sample_group_id 
                  AND status = :status
                  AND material_status_id = :material_status_id)t
          WHERE  plims.bsns_material.material_id = t.material_id
          AND status = :status'
      )->bindParam(':material_status_id', $material_status_id)
        ->bindParam(':composite_sample_group_id', $composite_sample_group_id)
        ->bindParam(':status', $status_active)
        ->execute();
    }

    $request_model->save();

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }
  public function actionExportReport($composite_sample_group_id, $extension)
  {
    $status =  Functional::STATUS_ACTIVE;
    $array = Yii::$app->db->createCommand(
      'SELECT t1.num_order          AS "NUM ORDER",
            t1.seed_name          AS "MATERIAL NAME",
            t2.short_name         AS "CROP",
            t3.short_name         AS "MATERIAL TYPE",
            t4.short_name         AS "MATERIAL UNIT",
            t1. material_unit_qty AS "MATERIAL UNIT QTY",
            t1.origin             AS "ORIGIN",
            t5.short_name         AS "MATERIAL STATUS"
      FROM   plims.bsns_material t1
            LEFT JOIN plims.bsns_crop t2
                  ON t1.crop_id = t2.crop_id
            LEFT JOIN plims.bsns_parameter t3
                  ON t1.material_type_id = t3.parameter_id
            LEFT JOIN plims.bsns_parameter t4
                  ON t1.material_unit_id = t4.parameter_id
            LEFT JOIN plims.bsns_parameter t5
                  ON t1.material_status_id = t5.parameter_id
      WHERE  composite_sample_group_id = :composite_sample_group_id
            AND t1.status = :status
      ORDER  BY 1;',
      [':composite_sample_group_id' => $composite_sample_group_id, ':status' => $status]
    )->queryAll();

    switch ($extension) {
      case 'CSV':
        $this->exportFileCSV($array, 'export-report');
        break;
      case 'XLS':
        $this->exportFileXLS($array, 'export-report');
        break;
      case 'PDF':
        $this->exportFilePDF($array, 'export-report');
        break;
      default:
        break;
    }
  }



  // Sample view / Sample Generator
  public function actionGenerate($request_id, $mode, $composite_sample_group_id, $request_code)
  {
    Yii::$app->db->createCommand()
      ->delete(
        'plims.bsns_material_group',
        'composite_sample_group_id = :composite_sample_group_id',
        [
          ':composite_sample_group_id' => $composite_sample_group_id
        ]
      )->execute();

    $material_status_id = Functional::MATERIAL_NORMAL;
    $status =  Functional::STATUS_ACTIVE;
    $count = Yii::$app->db->createCommand(
      'SELECT COUNT(*) 
        FROM plims.bsns_material 
        WHERE composite_sample_group_id = :composite_sample_group_id 
        AND material_status_id = :material_status_id 
        AND status = :status;',
      [
        ':composite_sample_group_id' => $composite_sample_group_id,
        ':material_status_id' => $material_status_id,
        ':status' => $status,
      ]
    )->queryScalar();

    $session = Yii::$app->session;

    unset($session['var.v1']);
    unset($session['var.v2']);
    unset($session['var.v3']);

    $var1 = null;
    $var2 = null;
    $var3 = null;

    switch ($mode) {
      case 'group':
        // ------------ //
        try {
          $count_variable = Yii::$app->request->post('txtQtyGroup');
          $count_variable = (int) str_replace("_", "", $count_variable);
          $var1 = $count_variable;
          $count_variable = $count_variable > $count ? $count : $count_variable;
          $groups_of = floor($count / $count_variable);

          for ($i = 0; $i <  $count_variable; $i++) {

            $index = $i + 1;
            $num_order_ini = $i * $groups_of + 1;

            if ($index == $count_variable) { //ultimo grupo
              $num_order_end = $count;
              $num_order_ini = ($num_order_ini < $count ? $num_order_ini : $count);
              $var3 =  $var3 . (string) $num_order_ini . '-' . (string) $num_order_end;
              $material_qty =  $num_order_end == $num_order_ini ?  0 : $num_order_end - $num_order_ini + 1;
            } else {
              $num_order_end = $index * $groups_of;
              $var3 =  $var3 .  (string) $num_order_ini . '-' . (string) $num_order_end . "\r\n";
              $material_qty =  $num_order_end - $num_order_ini + 1;
            }

            $data_material_group[] = [
              $index,
              'G' . (string) $index,
              $material_qty,
              $request_code . '.0.' . (string) $index . '.1',
              $num_order_ini,
              $num_order_end,
              $composite_sample_group_id,
              null
            ];
          }
          Yii::$app->db->createCommand()->batchInsert(
            'plims.bsns_material_group',
            [
              'number_index',
              'group_name',
              'material_qty',
              'sample_group_name',
              'num_order_ini',
              'num_order_end',
              'composite_sample_group_id',
              'material_details',
            ],
            $data_material_group
          )->execute();
        } catch (\DivisionByZeroError $th) {
        } catch (\Throwable $th) {
        }
        // ------------ //
        break;

      case 'groups':
        // ------------ //
        try {
          $count_variable = Yii::$app->request->post('txtQtyGroup');
          $count_variable = (int) str_replace("_", "", $count_variable);
          $var1 = $count_variable;
          $count_variable = $count_variable > $count ? $count : $count_variable;
          $groups_of = round($count / $count_variable);

          for ($i = 0; $i <  $count_variable; $i++) {

            $index = $i + 1;
            $num_order_ini = $i * $groups_of + 1;

            if ($index == $count_variable) { //ultimo grupo
              $num_order_end = $count;
              $num_order_ini = ($num_order_ini < $count ? $num_order_ini : $count);
              $var3 =  $var3 . (string) $num_order_ini . '-' . (string) $num_order_end;
              $material_qty =  $num_order_end == $num_order_ini ?  0 : $num_order_end - $num_order_ini + 1;
            } else {
              $num_order_end = $index * $groups_of;
              $var3 =  $var3 .  (string) $num_order_ini . '-' . (string) $num_order_end . "\r\n";
              $material_qty =  $num_order_end - $num_order_ini + 1;
            }

            $data_material_group[] = [
              $index,
              'G' . (string) $index,
              $material_qty,
              $request_code . '.0.' . (string) $index . '.1',
              $num_order_ini,
              $num_order_end,
              $composite_sample_group_id,
              null
            ];
          }
          // insercion en db
          Yii::$app->db->createCommand()->batchInsert(
            'plims.bsns_material_group',
            [
              'number_index',
              'group_name',
              'material_qty',
              'sample_group_name',
              'num_order_ini',
              'num_order_end',
              'composite_sample_group_id',
              'material_details',
            ],
            $data_material_group
          )->execute();
        } catch (\DivisionByZeroError $th) {
        } catch (\Throwable $th) {
        }
        // ------------ //
        break;

      case 'material':
        // ------------ //
        try {
          $groups_of = Yii::$app->request->post('txtQtyMaterial');
          $groups_of = (int) str_replace("_", "", $groups_of);
          $var2 = $groups_of;
          $count_variable = floor($count / $groups_of);
          $count_variable =  $count >  $count_variable * $groups_of ? $count_variable + 1 : $count_variable;

          for ($i = 0; $i <  $count_variable; $i++) {
            $index = $i + 1;
            $num_order_ini = $i * $groups_of + 1;
            if ($index == $count_variable) { //ultima fila
              $num_order_end = $count;
              $var3 =  $var3 . (string) $num_order_ini . '-' . (string) $num_order_end;
            } else {
              $num_order_end = $index * $groups_of;
              $var3 =  $var3 .  (string) $num_order_ini . '-' . (string) $num_order_end . "\r\n";
            }

            $material_qty =  $num_order_end - $num_order_ini + 1;
            $data_material_group[] = [
              $index,
              'G' . (string) $index,
              $material_qty,
              $request_code . '.0.' . (string) $index . '.1',
              $num_order_ini,
              $num_order_end,
              $composite_sample_group_id,
              null
            ];
          }
          Yii::$app->db->createCommand()->batchInsert(
            'plims.bsns_material_group',
            [
              'number_index',
              'group_name',
              'material_qty',
              'sample_group_name',
              'num_order_ini',
              'num_order_end',
              'composite_sample_group_id',
              'material_details',
            ],
            $data_material_group
          )->execute();
        } catch (\DivisionByZeroError $th) {
        } catch (\Throwable $th) {
        }
        // ------------ //
        break;

      case 'interval':
        // ------------ //
        if (!empty(Yii::$app->request->post('txtQtyIntervale'))) {
          $txt_qty_intervale = Yii::$app->request->post('txtQtyIntervale');
          $var3 =  $txt_qty_intervale;
          $data_rows = preg_split('/\r\n?/', $txt_qty_intervale, -1, PREG_SPLIT_NO_EMPTY);
          $all_elements = [];
          foreach ($data_rows as $key_row => $value_row) {
            // groups
            $index = $key_row + 1;
            $current_elements = [];
            $data_parts  = array_map('trim', explode(',', $value_row));
            foreach ($data_parts as $key_part => $value_part) {
              $data_elements  = array_map('trim', explode('-', $value_part));
              if (count($data_elements) == 1) {
                $current_elements[] = $data_elements[0];
              } else if (count($data_elements) == 2) {
                for ($i = min($data_elements); $i <= max($data_elements); $i++) {
                  $current_elements[] = $i;
                }
              }
            }
            $elements = array_diff($current_elements, $all_elements);
            $all_elements = array_merge($all_elements, $elements);

            $material_qty = count($elements);
            $num_order_ini = min($elements);
            $num_order_end = max($elements);
            $material_details = implode(", ", $elements);
            $data_material_group[] = [
              $index,
              'G' . (string) $index,
              $material_qty,
              $request_code . '.0.' . (string) $index . '.1',
              $num_order_ini,
              $num_order_end,
              $composite_sample_group_id,
              $material_details,
            ];
          }
          Yii::$app->db->createCommand()->batchInsert(
            'plims.bsns_material_group',
            [
              'number_index',
              'group_name',
              'material_qty',
              'sample_group_name',
              'num_order_ini',
              'num_order_end',
              'composite_sample_group_id',
              'material_details',
            ],
            $data_material_group
          )->execute();
        }
        // ------------ //
        break;

      default:
        break;
    }

    Yii::$app->session->set('var.v1', $var1);
    Yii::$app->session->set('var.v2', $var2);
    Yii::$app->session->set('var.v3', $var3);

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
      ]
    );
  }
  public function actionSave($request_id, $composite_sample_group_id, $request_code)
  {
    $status =  Functional::STATUS_ACTIVE;
    $material_status_id = Functional::MATERIAL_NORMAL;

    $count = Yii::$app->db->createCommand(
      'SELECT Count(1)
      FROM   plims.bsns_material_group
      WHERE  composite_sample_group_id = :composite_sample_group_id
            AND material_details IS NOT NULL;',
      [
        ':composite_sample_group_id' => $composite_sample_group_id,
      ]
    )->queryScalar();

    if ($count > 0) {
      $query =
        "UPDATE plims.bsns_material
        SET composite_sample_order = t.number_index,
            material_group_id = t.material_group_id
        FROM ( SELECT num_order_ini,
                    num_order_end,
                    number_index,
                    material_group_id,
                    ('{'||material_details||'}')::int[] AS material_details
              FROM   plims.bsns_material_group
              WHERE  composite_sample_group_id = :composite_sample_group_id )t
        WHERE   status = :status
        AND  plims.bsns_material.composite_sample_group_id = :composite_sample_group_id
        AND  plims.bsns_material.material_status_id = :material_status_id 
        AND plims.bsns_material.num_order = ANY(t.material_details);";
    } else {

      $query =
        "UPDATE plims.bsns_material
        SET composite_sample_order = t.number_index,
            material_group_id = t.material_group_id
        FROM ( SELECT num_order_ini,
                    num_order_end,
                    number_index,
                    material_group_id
              FROM   plims.bsns_material_group
              WHERE  composite_sample_group_id = :composite_sample_group_id )t
        WHERE   status = :status
        AND  plims.bsns_material.composite_sample_group_id = :composite_sample_group_id
        AND  plims.bsns_material.material_status_id = :material_status_id
        AND (plims.bsns_material.num_order BETWEEN t.num_order_ini AND t.num_order_end);";
    }

    // actualiza el foreign key del material con el grupo al que pertenece
    Yii::$app->db->createCommand(
      $query,
      [
        ':composite_sample_group_id' => $composite_sample_group_id,
        ':status' => $status,
        ':material_status_id' => $material_status_id,
      ]
    )->execute();

    // actualizacion inicio en grupo de materiales
    Yii::$app->db->createCommand(
      'UPDATE plims.bsns_material_group
        SET    "material_name_ini" = t.seed_name
        FROM   (SELECT num_order,
                      seed_name
                FROM   plims.bsns_material
                WHERE  composite_sample_group_id = :composite_sample_group_id
                      AND status = :status
                      AND material_status_id = :material_status_id)t
        WHERE  plims.bsns_material_group.composite_sample_group_id = :composite_sample_group_id
              AND plims.bsns_material_group.num_order_ini = t.num_order;',
      [
        ':composite_sample_group_id' => $composite_sample_group_id,
        ':status' => $status,
        ':material_status_id' => $material_status_id,
      ]
    )->execute();

    // actualizacion fin en grupo de materiales
    Yii::$app->db->createCommand(
      'UPDATE plims.bsns_material_group
        SET    "material_name_end" = t.seed_name
        FROM   (SELECT num_order,
                        seed_name
                FROM   plims.bsns_material
                WHERE  composite_sample_group_id = :composite_sample_group_id
                AND status = :status
                      AND material_status_id = :material_status_id)t
        WHERE  plims.bsns_material_group.composite_sample_group_id = :composite_sample_group_id
              AND plims.bsns_material_group.num_order_end = t.num_order;',
      [
        ':composite_sample_group_id' => $composite_sample_group_id,
        ':status' => $status,
        ':material_status_id' => $material_status_id,
      ]
    )->execute();

    // actualizacion cantidad de materiales en grupo de materiales
    Yii::$app->db->createCommand(
      'UPDATE plims.bsns_material_group
          SET    "material_unit_qty" = t.material_unit_qty
          FROM   (SELECT composite_sample_order,
                        Sum(material_unit_qty) AS material_unit_qty
                  FROM   plims.bsns_material
                  WHERE  composite_sample_group_id = :composite_sample_group_id
                  AND status = :status
                        AND material_status_id = :material_status_id
                  GROUP  BY composite_sample_order) t
          WHERE  plims.bsns_material_group.composite_sample_group_id = :composite_sample_group_id
                AND plims.bsns_material_group.number_index = t.composite_sample_order;',
      [
        ':composite_sample_group_id' => $composite_sample_group_id,
        ':status' => $status,
        ':material_status_id' => $material_status_id,
      ]
    )->execute();

    // obtencion de parametros cultivo, cantidad de materiales y cantidad de unidades en materiales
    $sample_qty = Yii::$app->db->createCommand(
      'SELECT COUNT(*) 
      FROM plims.bsns_material_group 
      WHERE composite_sample_group_id = :composite_sample_group_id;',
      [':composite_sample_group_id' => $composite_sample_group_id]
    )->queryScalar();

    $mq_array = Yii::$app->db->createCommand(
      'SELECT crop_id, material_qty, material_unit_qty
      FROM   plims.bsns_request_process brp
      WHERE  request_id = :request_id
      AND num_order_id = :num_order_id',
      [
        ':request_id' => $request_id,
        ':num_order_id' => Functional::NUM_ORDER_ID_PROCESS
      ]
    )->queryOne();
    $crop_id = $mq_array['crop_id'];
    $material_qty = $mq_array['material_qty'];
    $material_unit_qty = $mq_array['material_unit_qty'];


    // COMPOSITE_SAMPLE_INDIVIDUAL
    Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_composite_sample
        (composite_sample_type_id,
        composite_sample_group_id,
        crop_id,
        num_order,
        short_name,
        material_qty,
        material_unit_qty,
        registered_by,
        registered_at,
        reading_data_type_id,
        status,
        material_group_id)
      SELECT :composite_sample_type_id,
             a.composite_sample_group_id,
             :crop_id,
             a.num_order,
             :request_code
             || '.'
             || a.num_order
             || '.'
             || a.composite_sample_order
             || '.1',
             1,
             a.material_unit_qty,
             :registered_by,
             :registered_at,
             :reading_data_type_id,
             status,
             b.material_group_id
      FROM   plims.bsns_material a 
        INNER JOIN plims.bsns_material_group b 
        ON a.composite_sample_order = b.number_index
        AND a.composite_sample_group_id = b.composite_sample_group_id
      WHERE  a.composite_sample_group_id = :composite_sample_group_id AND a.status = :status;",
      [
        ':composite_sample_type_id' => Functional::COMPOSITE_SAMPLE_INDIVIDUAL,
        ':crop_id' => $crop_id,
        ':request_code' => $request_code,
        ':registered_by' => Yii::$app->user->identity->id,
        ':registered_at' => date("Y-m-d H:i:s", time()),
        ':reading_data_type_id' => Functional::READING_DATA_ENTRY,
        ':composite_sample_group_id' => $composite_sample_group_id,
        ':status' => Functional::STATUS_ACTIVE,
      ]
    )->execute();

    $new_material_group_id =   Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_material_group (
        number_index, 
        group_name, 
        material_qty, 
        material_unit_qty, 
        sample_group_name, 
        composite_sample_group_id
        )
        VALUES (
          :number_index,
          :group_name, 
          :material_qty, 
          :material_unit_qty, 
          :sample_group_name, 
          :composite_sample_group_id
          )
          RETURNING material_group_id;",
      [
        ':number_index' => 1,
        ':group_name' => 'G0',
        ':material_qty' => $material_qty,
        ':material_unit_qty' => $material_unit_qty,
        ':sample_group_name' =>  $request_code . '.0.0.1',
        ':composite_sample_group_id' => $composite_sample_group_id,
      ]
    )->queryScalar();

    // COMPOSITE_SAMPLE_SIMPLE
    Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_composite_sample
        (composite_sample_type_id,
        composite_sample_group_id,
        crop_id,
        num_order,
        short_name,
        material_qty,
        material_unit_qty,
        registered_by,
        registered_at,
        reading_data_type_id,
        status,
        material_group_id)
      SELECT 
        :composite_sample_type_id,
        :composite_sample_group_id,
        :crop_id,
        number_index,
        sample_group_name,
        material_qty,
        material_unit_qty,
        :registered_by,
        :registered_at,
        :reading_data_type_id,
        :status,
        :material_group_id
      FROM   plims.bsns_material_group
      WHERE  composite_sample_group_id = :composite_sample_group_id
      AND group_name <> 'G0';",
      [
        ':composite_sample_type_id' => Functional::COMPOSITE_SAMPLE_SIMPLE,
        ':composite_sample_group_id' => $composite_sample_group_id,
        ':crop_id' => $crop_id,
        ':registered_by' => Yii::$app->user->identity->id,
        ':registered_at' => date("Y-m-d H:i:s", time()),
        ':reading_data_type_id' => Functional::READING_DATA_ENTRY,
        ':status' => Functional::STATUS_ACTIVE,
        ':material_group_id' => $new_material_group_id,
      ]
    )->execute();

    // COMPOSITE_SAMPLE_GENERAL MODEL
    $csgm_array = [
      'composite_sample_type_id' =>  Functional::COMPOSITE_SAMPLE_GENERAL,
      'composite_sample_group_id' => $composite_sample_group_id,
      'crop_id' => $crop_id,
      'num_order' => Functional::NUM_ORDER_INI,
      'short_name' =>  $request_code . '.0.0.1',
      'material_qty' => $material_qty,
      'material_unit_qty' => $material_unit_qty,
      'reading_data_type_id' => Functional::READING_DATA_ENTRY,
      'registered_by' => Yii::$app->user->identity->id,
      'registered_at' => date("Y-m-d H:i:s", time()),
      'status' => Functional::STATUS_ACTIVE,
    ];
    Yii::$app->db->createCommand()->insert('plims.bsns_composite_sample', $csgm_array)->execute();

    // actualizacion de sample_qty del request
    Yii::$app->db->createCommand()
      ->update(
        'plims.bsns_request',
        [
          'sample_qty' =>  $sample_qty,
        ],
        'request_id = :request_id',
        [':request_id' => $request_id]
      )->execute();
    $request_status_id = Functional::COMPOSITE_SAMPLE_GENERATED;
    $observation = 'Request generada desde la plataforma PLIMS WEB';
    $this->setStatusRequest($request_id, $request_status_id, $observation);


    // actualizacion de sample_detail del request-preocess
    Yii::$app->db->createCommand()
      ->update(
        'plims.bsns_request_process',
        [
          'sample_detail' => 'Individual Sample | Simple Composite Sample | General Sample',
          'sample_qty' =>  $sample_qty,
          'material_detail' => Yii::$app->request->post('material_reserve'),
        ],
        'request_id = :request_id AND num_order_id = :num_order_id',
        [':request_id' => $request_id, ':num_order_id' => Functional::NUM_ORDER_ID_PROCESS]
      )->execute();



    return $this->redirect(
      [
        'manage-request/management',
        'request_id' => $request_id,
      ]
    );
  }
  public function actionMoreInfo($request_id, $material_group_id)
  {
    $data = Yii::$app->db->createCommand(
      "SELECT t1.composite_sample_id,
            t1.composite_sample_type_id,
            t1.composite_sample_group_id,
            t1.crop_id,
            t1.num_order,
            t1.short_name,
            t1.material_qty,
            t1.material_unit_qty,
            t1.registered_by,
            t1.registered_at,
            t1.updated_by,
            t1.updated_at,
            t1.deleted_by,
            t1.deleted_at,
            t1.status,
            t1.long_name,
            t1.reading_data_type_id,
            t1.material_group_id
      FROM   plims.bsns_composite_sample t1
            INNER JOIN plims.bsns_material_group t2
                    ON t1.material_group_id = t2.material_group_id
      WHERE  t1.composite_sample_type_id = :composite_sample_inividual
            AND t1.material_group_id = :material_group_id
            AND t1.reading_data_type_id = :_reading_data_entry
      ORDER  BY t1.num_order; ",
      [
        ':composite_sample_inividual' => Functional::COMPOSITE_SAMPLE_INDIVIDUAL,
        ':material_group_id' => $material_group_id,
        ':_reading_data_entry' => Functional::READING_DATA_ENTRY,
      ]
    )->queryAll();

    return $this->renderAjax('_modal-ind-sample-list', [
      'request_id' => $request_id,
      'material_group_id' => $material_group_id,
      'data' => $data,
    ]);
  }



  // Real Time Report
  public function actionExportHistorical($composite_sample_group_id, $extension)
  {
    $array = Yii::$app->db->createCommand(
      'SELECT t1.num_order                AS "NUM ORDER",
            t1.seed_name                  AS "MATERIAL NAME",
            t2.short_name                 AS "CROP",
            t3.short_name                 AS "MATERIAL TYPE",
            t4.short_name                 AS "MATERIAL UNIT",
            t1. material_unit_qty         AS "MATERIAL UNIT QTY",
            t1.origin                     AS "ORIGIN",
            t6.username                   AS "REGISTERED BY",
            t1.registered_at              AS "REGISTERED AT",
            t5.short_name                 AS "MATERIAL STATUS",
            t1.observation                AS "OBSERVATION",
            t1.historical_data_version    AS "HISTORICAL DATA VERSION",
            t1.details                    AS "DETAILS",
            t1.status                     AS "STATUS"
      FROM   plims.bsns_material t1
            LEFT JOIN plims.bsns_crop t2
                  ON t1.crop_id = t2.crop_id
            LEFT JOIN plims.bsns_parameter t3
                  ON t1.material_type_id = t3.parameter_id
            LEFT JOIN plims.bsns_parameter t4
                  ON t1.material_unit_id = t4.parameter_id
            LEFT JOIN plims.bsns_parameter t5
                  ON t1.material_status_id = t5.parameter_id
            LEFT JOIN plims.auth_user t6
                  ON t1.registered_by = t6.user_id
      WHERE  composite_sample_group_id = :composite_sample_group_id
      ORDER  BY 1;',
      [':composite_sample_group_id' => $composite_sample_group_id]
    )->queryAll();

    switch ($extension) {
      case 'CSV':
        $this->exportFileCSV($array, 'report');
        break;
      case 'XLS':
        $this->exportFileXLS($array, 'report');
        break;
      case 'PDF':
        $this->exportFilePDF($array, 'report');
        break;
      default:
        break;
    }
  }



  // Util Functions
  public function getNewVersion($material_id)
  {
    // da de baja la version actual
    Yii::$app->db->createCommand()->update('plims.bsns_material', ['status' => Functional::STATUS_DISABLED], 'material_id = :material_id')->bindParam(':material_id', $material_id)->execute();
    // crea el nuevo id del material
    $registered_by = Yii::$app->user->identity->id;
    $registered_at = date("Y-m-d H:i:s", time());
    $status = Functional::STATUS_ACTIVE;
    $new_material_id =   Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_material
          ( material_id,
            composite_sample_id,
            composite_sample_type_id,
            composite_sample_group_id,
            crop_id,
            material_type_id,
            material_unit_id,
            num_order,
            composite_sample_order,
            material_unit_qty,
            -- short_name,
            -- long_name,

            seed_name,     
            seed_code,     
            package_label, 
            package_code,  
            germplasm_name,

            observation,
            details,
            origin,
            grafting_order,
            material_code_a,
            material_code_b,
            material_code_c,
            registered_by,
            registered_at,
            updated_by,
            updated_at,
            deleted_by,
            deleted_at,
            status,
            material_group_id,
            material_status_id,
            historical_data_version)
      SELECT Nextval ('plims.material_id_seq'),
        composite_sample_id,
        composite_sample_type_id,
        composite_sample_group_id,
        crop_id,
        material_type_id,
        material_unit_id,
        num_order,
        composite_sample_order,
        material_unit_qty,
        -- short_name,
        -- long_name,

        seed_name,     
        seed_code,     
        package_label, 
        package_code,  
        germplasm_name,

        observation,
        details,
        origin,
        grafting_order,
        material_code_a,
        material_code_b,
        material_code_c,
        :registered_by,
        :registered_at,
        updated_by,
        updated_at,
        deleted_by,
        deleted_at,
        :status,
        material_group_id,
        material_status_id,
        historical_data_version + 1
      FROM   plims.bsns_material
      WHERE  material_id = :material_id
      RETURNING material_id;"
    )->bindParam(':registered_by', $registered_by)
      ->bindParam(':registered_at', $registered_at)
      ->bindParam(':status', $status)
      ->bindParam(':material_id', $material_id)
      ->queryScalar();
    return $new_material_id;
  }
  public function exportFileCSV($records, $fileName)
  {
    $fileNameExtension = $fileName . "_" . date("Y-m-d H:i:s", time()) . ".csv";
    $out = fopen('/tmp/' . $fileNameExtension, 'w');

    $heading = false;
    if (!empty($records))
      foreach ($records as $data) {
        if (!$heading) {
          fputcsv($out, array_keys($data));
          $heading = true;
        }
        fputcsv($out, $data);
      }
    fclose($out);

    header('Content-type: text/csv');
    header('Content-disposition: attachment; filename="' . $fileNameExtension . '"');
    readfile('/tmp/' . $fileNameExtension);
    exit();
  }
  public function exportFileXLS($records, $fileName)
  {
    $fileNameExtension = $fileName . "_" . date("Y-m-d H:i:s", time()) . ".xls";
    $headers = array_keys($records[0]);
    $heading = false;
    if (!empty($records))
      foreach ($records as $data) {
        if (!$heading) {
          $excelData = implode("\t", array_values($headers)) . "\n";
          $heading = true;
        }
        $excelData .= implode("\t", array_values($data)) . "\n";
      }
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=\"$fileNameExtension\"");
    echo $excelData;
    exit;
  }
  public function exportFilePDF($records, $fileName)
  {
    $longFileName = $fileName . "_" . date("Y-m-d H:i:s", time());
    $headers = array_keys($records[0]);
    $output = $this->renderPartial(
      '_view-report',
      [
        'headers' => $headers,
        'records' => $records,
        'longFileName' => $longFileName,
      ]
    );
    print($output);
    exit;
  }

  public function setStatusRequest($request_id, $request_status_id, $observation)
  {
    $number_index = Yii::$app->db->createCommand(
      "SELECT COALESCE(Max(number_index) + 1, 1) AS number_index
    FROM   plims.bsns_status
    WHERE  request_id = :request_id;",
      [':request_id' => $request_id]
    )->queryScalar();

    try {
      Yii::$app->db->createCommand()->insert(
        'plims.bsns_status',
        [
          'request_id' => $request_id,
          'request_status_id' => $request_status_id,
          'number_index' => $number_index,
          'status_date' => date("Y-m-d H:i:s", time()),
          'status_by' => Yii::$app->user->identity->id,
          'observation' => $observation,
        ]
      )->execute();

      Yii::$app->db->createCommand()->update(
        'plims.bsns_request',
        [
          'request_status_id' => $request_status_id,
        ],
        'request_id = :request_id',
        [':request_id' => $request_id]
      )->execute();
    } catch (\Throwable $th) {
    }
  }
}

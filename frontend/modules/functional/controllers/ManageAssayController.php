<?php

namespace frontend\modules\functional\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use frontend\modules\functional\models\RequestProcess;
use frontend\modules\functional\models\Request;
use frontend\modules\functional\models\RequestProcessEssay;
use app\models\UploadFile;
use app\models\UploadForm;
use yii\web\UploadedFile;
use frontend\modules\functional\models\RequestDocumentation;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\AgentCondition;
use frontend\modules\functional\Functional;
use frontend\models\Script;
use yii\helpers\Url;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\filters\AccessControl;

class ManageAssayController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger', \Yii::t('app', 'You do not have sufficient permissions to access this website'));
          return $this->goHome();
        },
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  public function actionManagement($request_id, $essay_id)
  {
    $r_data = Yii::$app->db->createCommand(
      'SELECT t1.request_id,
            t1.request_type_id           AS "r_type",
            t4.short_name                AS "crop",
            t5.short_name                AS "seed_distribution",
            t5.code                      AS "code_distribution",
            t6.username                  AS "requested_user",
            t3.sender_country            AS "sender_country",
            t3.recipient_country         AS "recipient_country",
            t3.recipient_name            AS "recipient",
            t3.sender_name               AS "sender",
            t1.material_qty              AS "material_qty",
            t1.sample_qty                AS "sample_qty",
            t7.class_details             AS "r_status_class",
            t7.short_name                AS "r_status_class_name",
            t1.request_code              AS "r_code",
            t7.code                      AS "r_status_code",
            t8.class_details             AS "rp_status_class",
            t8.short_name                AS "rp_status_class_name",
            t2.sample_detail             AS "sample_detail",
            t2.composite_sample_group_id AS "composite_sample_group_id",
            t1.request_status_id         AS "request_status_id",
            t1.registered_at::date       AS "date_created"
      FROM   plims.bsns_request t1
            LEFT JOIN plims.bsns_request_process t2
                  ON t1.request_id = t2.request_id
                      AND t2.num_order_id = :num_order_id
            LEFT JOIN plims.bsns_request_detail t3
                  ON t1.request_id = t3.request_id
            LEFT JOIN plims.bsns_crop t4
                  ON t2.crop_id = t4.crop_id
            LEFT JOIN plims.bsns_parameter t5
                  ON t1.request_type_id = t5.parameter_id
            LEFT JOIN plims.auth_user t6
                  ON t3.requested_user = t6.user_id
            LEFT JOIN plims.bsns_parameter t7
                  ON t1.request_status_id = t7.parameter_id
            LEFT JOIN plims.bsns_parameter t8
                  ON t2.request_process_status_id = t8.parameter_id
      WHERE  t1.request_id = :request_id;',
      [
        ':num_order_id' => functional::NUM_ORDER_ID_PROCESS,
        ':request_id' => $request_id
      ]
    )->queryOne();

    $request_code =  $r_data['r_code'];
    $request_status_code = $r_data['r_status_code'];
    $request_type_id = $r_data['r_type'];
    $rp_model = RequestProcess::findOne($request_id, functional::NUM_ORDER_ID_PROCESS);
    $assay_management_status_class =  $rp_model->requestProcessStatus->class_details;
    $assay_management_status_name =  $rp_model->requestProcessStatus->short_name;
    $composite_sample_types =  $rp_model->sample_detail;
    $composite_sample_group_id = $rp_model->composite_sample_group_id;
    $crop_id =  $rp_model->crop_id;

    $rpa_header_data = Yii::$app->db->createCommand(
      'SELECT t2.short_name                      AS assay_name,
            t2.essay_type_id                   AS assay_type,
            CASE
              WHEN t2.essay_type_id = :essay_type_web THEN true
              ELSE false
            END                                AS is_web_app,
            p2.short_name 										  AS assay_type_name,
            p2.class_details 									AS assay_type_class,
            CASE
              WHEN t1.request_process_essay_status_id =
                  :rpa_status_finished
            THEN true
              ELSE false
            END                                AS is_finished_rpa,
            t1.start_date,
            u1.username      AS start_by,
            t1.finish_date,
            u2.username      AS finish_by,
            t1.request_process_essay_status_id AS rpa_status_id,
            p1.short_name                      AS rpa_status_name,
            p1.class_details                   AS rpa_status_class,
            p1.code                            AS rpa_status_code,
            t1.sample_detail,
            t1.observation
      FROM   plims.bsns_request_process_essay t1
            LEFT JOIN plims.bsns_essay t2
                  ON t1.essay_id = t2.essay_id
            LEFT JOIN plims.bsns_parameter p1
                  ON t1.request_process_essay_status_id = p1.parameter_id
            LEFT JOIN plims.bsns_parameter p2
                  ON t2.essay_type_id = p2.parameter_id
            LEFT JOIN plims.auth_user u1
                  ON u1.user_id = t1.start_by
            LEFT JOIN plims.auth_user u2
                  ON u2.user_id = t1.finish_by  
      WHERE  t1.request_id = :request_id
            AND t1.num_order_id = :num_order_process
            AND t1.essay_id = :essay_id
            AND t1.composite_sample_type_id = :composite_sample_type_header;',
      [
        ':essay_type_web' => Functional::ESSAY_TYPE_WEB_APP,
        ':rpa_status_finished' => Functional::REQUEST_PROCESS_ESSAY_STATUS_FINISHED,
        ':request_id' => $request_id,
        ':num_order_process' => functional::NUM_ORDER_ID_PROCESS,
        ':essay_id' => $essay_id,
        ':composite_sample_type_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
      ]
    )->queryOne();

    $assay_name = $rpa_header_data['assay_name'];
    $is_web_app = $rpa_header_data['is_web_app']; //  $rpe_model->essay->essay_type_id == Functional::ESSAY_TYPE_WEB_APP ? true : false;
    $is_finished_rpa = $rpa_header_data['is_finished_rpa'];  // $rpe_model->request_process_essay_status_id == Functional::REQUEST_PROCESS_ESSAY_STATUS_FINISHED ? true : false;
    $rpa_status_code = $rpa_header_data['rpa_status_code'];
    $min_status =   $is_web_app ? 3.1 : 3.2;

    // request process assays in normal and reprocess
    $rpa_data_samples = Yii::$app->db->createCommand(
      "SELECT 
            t1.request_id,
            t1.num_order_id,
            t1.essay_id,
            t1.crop_id,
            t1.workflow_id,
            t1.start_date,
            t1.finish_date,
            t1.request_process_essay_id,
            t1.request_process_essay_status_id,
            t1.request_process_essay_root_id,
            u1.username      AS start_by,
            u2.username      AS finish_by,
            t1.agent_qty,
            t1.status,
            t1.agent_detail,
            t1.sub_total_cost_essay,
            t1.composite_sample_type_id,
            p1.code          AS rpa_status_code,
            p1.class_details AS rpa_status_class,
            p1.short_name    AS rpa_status_name,
            p2.class_details AS rpa_sample_type_class,
            p2.long_name || CASE WHEN p3.parameter_id = 1 THEN '' ELSE ' [' || p3.short_name || ']' END AS rpa_sample_type_name,
            p2.code || '_' || p3.code          AS rpa_sample_code,
            c1.short_name    AS crop_name,
            t1.code
      FROM   plims.bsns_request_process_essay t1
                LEFT JOIN plims.bsns_parameter p1
                    ON t1.request_process_essay_status_id = p1.parameter_id
                LEFT JOIN plims.bsns_parameter p2
                    ON t1.composite_sample_type_id = p2.parameter_id
                LEFT JOIN plims.bsns_parameter p3
                    ON t1.request_process_essay_type_id = p3.parameter_id
                LEFT JOIN plims.bsns_crop c1
                    ON t1.crop_id = c1.crop_id
                LEFT JOIN plims.auth_user u1
                    ON u1.user_id = t1.start_by
                LEFT JOIN plims.auth_user u2
                    ON u2.user_id = t1.finish_by                    
      WHERE  t1.request_id = :request_id
            AND t1.num_order_id = :num_order_id
            AND t1.essay_id = :assay_id
            AND t1.status = :status_active

            AND (t1.request_process_essay_status_id <> :request_process_essay_status_pending OR :request_process_essay_status_pending is NULL)
            AND t1.composite_sample_type_id <> :composite_sample_type_header
      ORDER BY p2.code;",
      [
        ':request_id' => $request_id,
        ':num_order_id' => functional::NUM_ORDER_ID_PROCESS,
        ':assay_id' => $essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
        ':request_process_essay_status_pending' => $is_web_app ? null : Functional::REQUEST_PROCESS_ESSAY_STATUS_PENDING, // is web -> all / is mobile -> no pendings
        ':composite_sample_type_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
      ]
    )->queryAll();

    $rpa_data_samples_rep = Yii::$app->db->createCommand(
      "SELECT 
            t1.request_id,
            t1.num_order_id,
            t1.essay_id,
            t1.crop_id,
            t1.workflow_id,
            t1.start_date,
            t1.finish_date,
            t1.request_process_essay_id,
            t1.request_process_essay_status_id,
            t1.request_process_essay_root_id,
            u1.username      AS start_by,
            u2.username      AS finish_by,
            t1.agent_qty,
            t1.status,
            t1.agent_detail,
            t1.sub_total_cost_essay,
            t1.composite_sample_type_id,
            p1.code          AS rpa_status_code,
            p1.class_details AS rpa_status_class,
            p1.short_name    AS rpa_status_name,
            p2.class_details AS rpa_sample_type_class,
            p2.long_name || CASE WHEN p3.parameter_id = 1 THEN '' ELSE ' [' || p3.short_name || ']' END AS rpa_sample_type_name,
            p2.code || '_' || p3.code          AS rpa_sample_code,
            c1.short_name    AS crop_name,
            t1.code
      FROM   plims.bsns_request_process_essay t1
                LEFT JOIN plims.bsns_parameter p1
                    ON t1.request_process_essay_status_id = p1.parameter_id
                LEFT JOIN plims.bsns_parameter p2
                    ON t1.composite_sample_type_id = p2.parameter_id
                LEFT JOIN plims.bsns_parameter p3
                    ON t1.request_process_essay_type_id = p3.parameter_id
                LEFT JOIN plims.bsns_crop c1
                    ON t1.crop_id = c1.crop_id
                LEFT JOIN plims.auth_user u1
                    ON u1.user_id = t1.start_by
                LEFT JOIN plims.auth_user u2
                    ON u2.user_id = t1.finish_by                    
      WHERE  t1.request_id = :request_id
            AND t1.num_order_id > :num_order_id
            AND t1.essay_id = :assay_id
            AND t1.status = :status_active
            AND (t1.request_process_essay_status_id <> :request_process_essay_status_pending OR :request_process_essay_status_pending is NULL)
            AND t1.composite_sample_type_id <> :composite_sample_type_header
      ORDER BY p2.code;",
      [
        ':request_id' => $request_id,
        ':num_order_id' => functional::NUM_ORDER_ID_PROCESS,
        ':assay_id' => $essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
        ':request_process_essay_status_pending' => $is_web_app ? null : Functional::REQUEST_PROCESS_ESSAY_STATUS_PENDING,
        ':composite_sample_type_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
      ]
    )->queryAll();

    // Documentation
    $model_upload_documentation = new UploadForm();

    $data_documentation = Yii::$app->db->createCommand(
      'SELECT t1.*, t2.class_details
        FROM   plims.bsns_request_documentation t1 
        LEFT JOIN plims.bsns_parameter t2 
          ON t1.file_type_id = t2.parameter_id
        WHERE  t1.request_id = :request_id
              AND t1.status = :status_active
              AND t1.essay_id =:essay_id;',
      [
        ':request_id' => $request_id,
        ':essay_id' => $essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    // Assay Management
    $available_samples_list = Yii::$app->db->createCommand(
      "SELECT tab1.code,
            CASE
              WHEN tab1.rpe_type_id = :_undetermined THEN tab1.sample_type_name
              ELSE tab1.sample_type_name || ' [' || tab1.rpe_type_name || ']'
            END AS sample_type_name
      FROM   (SELECT DISTINCT t1.composite_sample_type_id         AS cs_type_id,
                            t1.request_process_essay_type_id    AS rpe_type_id,
                            p1.long_name                       AS sample_type_name,
                            p2.short_name                       AS rpe_type_name,
                            t1.composite_sample_type_id
                            || '.'
                            || t1.request_process_essay_type_id AS code
            FROM   plims.bsns_agent_condition t1
                    INNER JOIN plims.bsns_parameter p1
                            ON t1.composite_sample_type_id = p1.parameter_id
                    INNER JOIN plims.bsns_parameter p2
                            ON t1.request_process_essay_type_id = p2.parameter_id
            WHERE  t1.status = :_status_active
                    AND t1.essay_id = :_assay_id
                    AND t1.crop_id = :_crop_id
                    AND t1.request_type_id = :_request_type_id)tab1
      WHERE  tab1.code NOT IN (SELECT composite_sample_type_id
                                    || '.'
                                    || request_process_essay_type_id AS code
                              FROM   plims.bsns_request_process_essay
                              WHERE  request_id = :_request_id
                                    AND num_order_id = :_process_id
                                    AND essay_id = :_assay_id
                                    AND status = :_status_active
                                    AND composite_sample_type_id <> :_cst_header);",
      [
        ':_undetermined' => Functional::PARAMETER_UNDETERMINED,
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_assay_id' => $essay_id,
        ':_crop_id' => $crop_id,
        ':_request_type_id' => $request_type_id,
        ':_request_id' => $request_id,
        ':_process_id' => functional::NUM_ORDER_ID_PROCESS,
        ':_cst_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
      ]
    )->queryAll();

    $available_samples_list = ArrayHelper::map($available_samples_list, 'code', 'sample_type_name');

    // is remove all / is finished
    $is_remove_all = false;
    $is_finished_all =  false;

    $status_list = array_column($rpa_data_samples, 'request_process_essay_status_id');

    $status_list_rep = array_column($rpa_data_samples_rep, 'request_process_essay_status_id');

    $status_list_merge = array_merge($status_list, $status_list_rep);


    if (in_array(Functional::REQUEST_PROCESS_ESSAY_STATUS_PENDING, $status_list_merge)) {
      $is_remove_all = true;
    } else if (!in_array(Functional::REQUEST_PROCESS_ESSAY_STATUS_IN_PROCESS, $status_list_merge)) {
      $is_finished_all = true;
    }

    // Render
    return $this->render(
      'management',
      [
        'request_id' => $request_id,
        'num_order_id' => functional::NUM_ORDER_ID_PROCESS,
        'request_code' => $request_code,
        'request_status_code' => $request_status_code,
        'assay_management_status_class' => $assay_management_status_class,
        'assay_management_status_name' => $assay_management_status_name,
        'composite_sample_types' => $composite_sample_types,
        'composite_sample_group_id' => $composite_sample_group_id,
        'essay_id' => $essay_id,
        'assay_name' => $assay_name,
        'model_upload_documentation' => $model_upload_documentation,
        'data_documentation' => $data_documentation,
        'available_samples_list' => $available_samples_list,

        'is_finished_rpa' => $is_finished_rpa,
        'is_finished_all' => $is_finished_all,
        'is_web_app' => $is_web_app,
        'rpa_status_code' => $rpa_status_code,
        'r_data' => $r_data,
        'rpa_header_data' => $rpa_header_data,
        'rpa_data_samples' => $rpa_data_samples,
        'rpa_data_samples_rep' => $rpa_data_samples_rep,
        'min_status' => $min_status,
        'is_remove_all' => $is_remove_all,
      ]
    );
  }

  public function actionUploadDocument($request_id, $essay_id)
  {
    $model_upload_documentation = new UploadForm();

    if (Yii::$app->request->isPost) {
      $serial =  "R" . $request_id;
      $model_upload_documentation->mixFiles = UploadedFile::getInstances($model_upload_documentation, 'mixFiles');
      if ($model_upload_documentation->upload("_" . $serial, $request_id, $essay_id)) {
      }
    }

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
        'essay_id' => $essay_id,
      ]
    );
  }
  public function actionRemoveDocument($request_id, $request_documentation_id, $essay_id)
  {
    try {
      if (($model = RequestDocumentation::findOne($request_documentation_id)) !== null) {
        $document_name = $model->path_documentation . $model->document_name . '.' . $model->document_extension;
        unlink($document_name);
        $model->status = 'disable';
        $model->save();
      }
    } catch (\Throwable $th) {
      echo "<pre>";
      print_r($th);
      echo "</pre>";
      die();
    }

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
        'essay_id' => $essay_id,
      ]
    );
  }

  // add sample type in web process assay
  public function actionAddSample($request_id, $essay_id, $num_order_id)
  {
    $user =  Yii::$app->user->identity->id;
    $date = date("Y-m-d H:i:s", time());

    $composite_sample_type_list = $this->request->post('select_name_1');

    $num_order = Yii::$app->db->createCommand(
      "SELECT num_order
      FROM   plims.bsns_request_process_essay
      WHERE  request_id = :_request_id
             AND essay_id = :_assay_id
             AND num_order_id = :_num_order_id
             AND composite_sample_type_id = :_cst_header;",
      [
        ':_request_id' => $request_id,
        ':_assay_id' => $essay_id,
        ':_num_order_id' => $num_order_id,
        ':_cst_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
      ]
    )->queryScalar();

    if (isset($composite_sample_type_list)) {
      foreach ($composite_sample_type_list as $key => $value) {

        $composite_sample_type_id = explode('.', $value)[0];
        $request_process_essay_type_id = explode('.', $value)[1];
        Script::instance()->webTemplateSettings($user, $date, $request_id, $composite_sample_type_id, $essay_id, $num_order_id, $request_process_essay_type_id, $num_order);
      }
    }

    $sample_detail = Yii::$app->db->createCommand(
      "SELECT   string_agg(DISTINCT sample_detail, ' | ' order BY sample_detail)
      FROM     plims.bsns_request_process_essay
      WHERE    request_id = :_request_id
      AND      essay_id = :_assay_id
      AND      num_order_id = :_num_order_id;",
      [
        ':_request_id' => $request_id,
        ':_assay_id' => $essay_id,
        ':_num_order_id' => $num_order_id,
      ]
    )->queryScalar();

    Yii::$app->db->createCommand(
      "UPDATE plims.bsns_request_process_essay
      SET    sample_detail = :_sample_detail
      WHERE  request_id = :_request_id
             AND essay_id = :_assay_id
             AND num_order_id = :_num_order_id
             AND composite_sample_type_id = :_cst_header; ",
      [
        ':_sample_detail' => $sample_detail,
        ':_request_id' => $request_id,
        ':_assay_id' => $essay_id,
        ':_num_order_id' => $num_order_id,
        ':_cst_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
      ]
    )->execute();

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
        'essay_id' => $essay_id,
        'num_order_id' => $num_order_id
      ]
    );
  }

  public function actionRemoveSample($request_id, $num_order_id, $essay_id, $composite_sample_type_id = null)
  {
    Script::instance()->webSampleRemove($request_id, $num_order_id, $essay_id, $composite_sample_type_id = null);

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
        'essay_id' => $essay_id,
        'num_order_id' => $num_order_id
      ]
    );
  }

  public function actionFinish($request_id, $essay_id)
  {
    $date = date("Y-m-d H:i:s", time());
    $user = Yii::$app->user->identity->id;

    Yii::$app->db->createCommand(
      "UPDATE plims.bsns_request_process_essay
      SET    request_process_essay_status_id = :_rpa_finished,
      finish_date = :_date,
      finish_by = :_user
      WHERE  request_id = :_request_id
            AND essay_id = :_assay_id
            AND composite_sample_type_id = :_cst_header
            AND status = :_status_active;",
      [
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_rpa_finished' => Functional::REQUEST_PROCESS_ESSAY_STATUS_FINISHED,
        ':_request_id' => $request_id,
        ':_assay_id' => $essay_id,
        ':_cst_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
        ':_date' => $date,
        ':_user' => $user,
      ]
    )->execute();

    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
        'essay_id' => $essay_id,
      ]
    );
  }

  public function actionRelease($request_id, $essay_id)
  {
    $rpa_id_list = Yii::$app->db->createCommand(
      "SELECT t1.request_process_essay_id,
            p1.long_name AS sample_name
      FROM   plims.bsns_request_process_essay t1
            LEFT JOIN plims.bsns_parameter p1
                  ON t1.composite_sample_type_id = p1.parameter_id
      WHERE  t1.request_id = :_request_id
            AND t1.essay_id = :_assay_id
            -- AND t1.composite_sample_type_id = :_cst_individual
            AND t1.composite_sample_type_id <> :_cst_header
      ORDER  BY 1; ",
      [
        ':_request_id' => $request_id,
        ':_assay_id' => $essay_id,
        // ':_cst_individual' => Functional::COMPOSITE_SAMPLE_INDIVIDUAL,
        ':_cst_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
      ]
    )->queryAll();

    $rpa_list = ArrayHelper::map($rpa_id_list, 'request_process_essay_id', 'sample_name');

    $data = [];

    foreach ($rpa_list as $request_process_essay_id => $sample_name) {
      $data[] = $this->getData($request_process_essay_id, $sample_name);
    }

    return $this->render(
      'release',
      [
        'request_id' => $request_id,
        'data' => $data,
      ]
    );
  }


  public function getData($request_process_essay_id, $sample_name)
  {
    $columns = [['class' => 'yii\grid\SerialColumn']];

    $columns[] = [
      'attribute' => 'sample_order',
    ];

    $columns[] = [
      'attribute' => 'sample',
      'format' => 'raw',
    ];


    $columns[] =  [
      'class' => 'yii\grid\ActionColumn',
      'header'        => 'Final Decision',
      'template'      => '{control}',
      'headerOptions' => ['min-width' => '80'],
      'buttons'       => [
        'control' => function ($url, $model, $key) {
          return
            '<input type="checkbox" name="cbx_reprocess" checked="">';
        },
      ],
    ];

    $columns[] =  [
      'class' => 'yii\grid\ActionColumn',
      'header'        => 'Automatic Decision',
      'template'      => '{control}',
      'headerOptions' => ['min-width' => '80'],
      'buttons'       => [
        'control' => function ($url, $model, $key) {
          return
            'Do not release';
        },
      ],
    ];


    $data_agents = Yii::$app->db->createCommand(
      'SELECT t1.agent_id,
          t2.short_name
    FROM   plims.bsns_request_process_essay_agent t1
          INNER JOIN plims.bsns_agent t2
                  ON t1.agent_id = t2.agent_id
    WHERE  request_process_essay_id = :request_process_essay_id
          AND t1.status = :status_active
    ORDER  BY t1.num_order;',
      [
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    $max_agents = count($data_agents);

    if ($max_agents > 0) {
      $query = "SELECT num_order as sample_order, sample,";


      foreach ($data_agents as $key => $value) {
        $query = $query
          . "'<span class=st-'|| Max(text_result) filter (WHERE agent_id = '"
          . $value['agent_id'] . "') ||'>'|| Max(text_result) filter (WHERE agent_id = '"
          . $value['agent_id'] . "') ||'</span>' AS " . '"' . $value['short_name'] . '"';
        if ($key + 1 < $max_agents) $query = $query . ", ";
        $columns[] = [
          'attribute' => $value['short_name'],
          'format' => 'raw',
        ];
      }




      $query =
        $query . ' FROM     (
        SELECT     t2.num_order     AS num_order,
                    t2.short_name    AS sample,
                    t1.agent_id     AS agent_id,
                    t1.number_result AS number_result,
                    t1.text_result   AS text_result
        FROM       plims.bsns_reading_data t1
        INNER JOIN plims.bsns_composite_sample t2
        ON         t1.composite_sample_id = t2.composite_sample_id
        WHERE      t1.request_process_essay_id = :request_process_essay_id
        AND        t1.status = :status
        ORDER BY   
            t1.secondary_order_num) AS ntb
        GROUP BY 
            num_order,
            sample 
        ORDER BY 1;';

      // echo "<pre>";
      // print_r($query);
      // echo "</pre>";
      // die();

      $data = Yii::$app->db->createCommand(
        $query,
        [
          ':request_process_essay_id' => $request_process_essay_id,
          ':status' => Functional::STATUS_ACTIVE
        ]
      )->queryAll();
    } else {
      $data = null;
      $columns = [];
    }


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['sample_order', 'sample'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns,
        'sample_name' => $sample_name,
      ];
  }
}

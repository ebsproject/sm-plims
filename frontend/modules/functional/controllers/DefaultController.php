<?php

namespace frontend\modules\functional\controllers;

use yii\web\Controller;

/**
 * Default controller for the `functional` module
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}

<?php

namespace frontend\modules\functional\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ArrayDataProvider;
use frontend\modules\functional\Functional;
use yii\helpers\Html;
use yii\filters\AccessControl;

class ManageResultController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger', \Yii::t('app', 'You do not have sufficient permissions to access this website'));
          return $this->goHome();
        },
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  public function actionManagement($request_id, $request_code)
  {
    $data_columns = Yii::$app->db->createCommand(
      "SELECT DISTINCT essay_id,
            agent_id,
            assay_class,
            assay_name,
            agent_name
      FROM   plims.bsns_consolidated_result
      WHERE  request_id = :_request_id
      ORDER  BY assay_name,
      agent_name;",
      [
        ':_request_id' => $request_id
      ]
    )->queryAll();
    $query_1 = "SELECT 
    t1.consolidate_id,
    t1.num_order,
    t1.general_sample_name,
    t1.simple_sample_name,
    t1.indiviual_sample_name,
    t1.final_decision,  
    case when Max(rn.text_sample || '.' || rn.text_result)  like '%positive' then 'do not release' else 'RELEASE' end as automatic_decision,
    case when Max(rn.text_sample || '.' || rn.text_result)  like '%positive' then '<span class=st-positive>POSITIVE</span>' else '<span class=st-negative>NEGATIVE</span>' end as final_result, ";
    $query_3 = ' t1.num_order FROM   plims.bsns_consolidated_information t1 ';
    $query_5 = "INNER JOIN plims.bsns_consolidated_result rn
                  ON t1.request_id = rn.request_id AND rn.composite_sample_id IN ( t1.general_sample_id, t1.simple_sample_id, t1.indiviual_sample_id )
                WHERE  t1.request_id = :_request_id
                GROUP  BY 
                t1.consolidate_id,
                t1.num_order,
                t1.general_sample_name,
                t1.simple_sample_name, 
                t1.indiviual_sample_name, 
                t1.final_decision,
                t1.automatic_decision, 
                t1.final_result; ";
    $query_2 = '';
    $query_4 = '';
    $attributes = [
      'consolidate_id',
      'num_order',
      'general_sample_name',
      'simple_sample_name',
      'indiviual_sample_name',
      'final_decision',
      'automatic_decision',
      'final_result',
    ];
    $columns = [
      [
        'attribute' => 'num_order',
        'format' => 'raw',
      ],
      [
        'attribute' => 'general_sample_name',
        'format' => 'raw',
      ],
      [
        'attribute' => 'simple_sample_name',
        'format' => 'raw',
      ],
      [
        'attribute' => 'indiviual_sample_name',
        'format' => 'raw',
      ],
      [
        'format' => 'raw',
        'attribute' => 'final_decision',
        'value' => function ($data, $key) use ($request_id, $request_code) {
          return Html::a(
            $data['final_decision']  ? '<i class="fa fa-toggle-on" ></i> RELEASED' : '<i class="fa fa-toggle-off" ></i> NOT RELEASED',
            [
              'update',
              'request_id' => $request_id,
              'request_code' => $request_code,
              'consolidate_id' => $key,
            ]
          );
        },
      ],
      [
        'attribute' => 'automatic_decision',
        'format' => 'raw',
      ],
      [
        'attribute' => 'final_result',
        'format' => 'raw',
      ],
    ];
    foreach ($data_columns as $key => $value) {
      $colum_alias =  '"' . $value['assay_name'] . ' | ' . $value['agent_name'] . '"';
      $attribute =    $value['assay_name'] . ' | ' . $value['agent_name'];
      $query_2 = $query_2 . "String_agg(DISTINCT '<span title='||r"
        . $key . ".text_sample||' class=st-'|| r"
        . $key . ".text_result|| '>'|| r"
        . $key . ".text_sample || '.' || r"
        . $key . ".text_result || '</span>', ' -> ') AS " . $colum_alias . ", ";
      $attributes[] = $attribute;
      $columns[] = [
        'attribute' => $attribute,
        'format' => 'raw',
      ];
      $query_4 = $query_4 . " INNER JOIN plims.bsns_consolidated_result r" . $key . "
      ON t1.request_id = r" . $key . ".request_id
          AND r" . $key . ".composite_sample_id IN ( t1.general_sample_id, t1.simple_sample_id, t1.indiviual_sample_id )
          AND r" . $key . ".essay_id = " . $value['essay_id'] . "
          AND r" . $key . ".agent_id = " . $value['agent_id'];
    }
    $query = $query_1 . $query_2 . $query_3 . $query_4 . $query_5;
    $data = Yii::$app->db->createCommand(
      $query,
      [
        ':_request_id' => $request_id
      ]
    )->queryAll();



    $_limit = Yii::$app->request->post('_limit');
    $_limit = $_limit == null ? '10' : $_limit;


    $data_provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => $attributes,
      ],
      'key' => 'consolidate_id',
      'pagination' => [
        'pageSize' => $_limit,
      ],
    ]);


    return $this->render(
      'management',
      [
        'request_id' => $request_id,
        'data_provider' => $data_provider,
        'columns' => $columns,
        'attributes' => $attributes,
        'request_code' => $request_code,
        '_limit' => $_limit,
      ]
    );
  }

  public function actionUpdate($request_id, $request_code, $consolidate_id)
  {

    Yii::$app->db->createCommand(
      "UPDATE plims.bsns_consolidated_information
      SET    final_decision =
             CASE
                    WHEN final_decision THEN false
                    ELSE true
             END
      WHERE  consolidate_id = :_consolidate_id;",
      [

        ':_consolidate_id' => $consolidate_id,
      ]
    )->execute();
    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
        'request_code' => $request_code,
      ]
    );
  }

  public function actionExportReport($request_id)
  {
    $data_columns = Yii::$app->db->createCommand(
      "SELECT DISTINCT essay_id,
            agent_id,
            assay_class,
            assay_name,
            agent_name
      FROM   plims.bsns_consolidated_result
      WHERE  request_id = :_request_id
      ORDER  BY assay_name,
      agent_name;",
      [
        ':_request_id' => $request_id
      ]
    )->queryAll();
    $query_1 = "SELECT 
    t1.consolidate_id,
    t1.num_order,
    t1.general_sample_name,
    t1.simple_sample_name,
    t1.indiviual_sample_name,
    t1.final_decision,  
    case when Max(rn.text_sample || '.' || rn.text_result)  like '%positive' then 'do not release' else 'RELEASE' end as automatic_decision,
    case when Max(rn.text_sample || '.' || rn.text_result)  like '%positive' then 'POSITIVE' else 'NEGATIVE' end as final_result, ";
    $query_3 = ' t1.num_order FROM   plims.bsns_consolidated_information t1 ';
    $query_5 = "INNER JOIN plims.bsns_consolidated_result rn
                  ON t1.request_id = rn.request_id AND rn.composite_sample_id IN ( t1.general_sample_id, t1.simple_sample_id, t1.indiviual_sample_id )
                WHERE  t1.request_id = :_request_id
                GROUP  BY 
                t1.consolidate_id,
                t1.num_order,
                t1.general_sample_name,
                t1.simple_sample_name, 
                t1.indiviual_sample_name, 
                t1.final_decision,
                t1.automatic_decision, 
                t1.final_result; ";
    $query_2 = '';
    $query_4 = '';
    foreach ($data_columns as $key => $value) {
      $colum_alias =  '"' . $value['assay_name'] . ' | ' . $value['agent_name'] . '"';
      $query_2 = $query_2 . "String_agg(DISTINCT r"
        . $key . ".text_sample || '.' || r"
        . $key . ".text_result, ' -> ') AS " . $colum_alias . ", ";
      $query_4 = $query_4 . " INNER JOIN plims.bsns_consolidated_result r" . $key . "
      ON t1.request_id = r" . $key . ".request_id
          AND r" . $key . ".composite_sample_id IN ( t1.general_sample_id, t1.simple_sample_id, t1.indiviual_sample_id )
          AND r" . $key . ".essay_id = " . $value['essay_id'] . "
          AND r" . $key . ".agent_id = " . $value['agent_id'];
    }
    $query = $query_1 . $query_2 . $query_3 . $query_4 . $query_5;
    $data = Yii::$app->db->createCommand(
      $query,
      [
        ':_request_id' => $request_id
      ]
    )->queryAll();

    $this->exportFileCSV($data, 'export-report-results');
  }

  public function exportFileCSV($records, $fileName)
  {
    $fileNameExtension = $fileName . "_" . date("Y-m-d H:i:s", time()) . ".csv";
    $out = fopen('/tmp/' . $fileNameExtension, 'w');

    $heading = false;
    if (!empty($records))
      foreach ($records as $data) {
        if (!$heading) {
          fputcsv($out, array_keys($data));
          $heading = true;
        }
        fputcsv($out, $data);
      }
    fclose($out);

    header('Content-type: text/csv');
    header('Content-disposition: attachment; filename="' . $fileNameExtension . '"');
    readfile('/tmp/' . $fileNameExtension);
    exit();
  }
}

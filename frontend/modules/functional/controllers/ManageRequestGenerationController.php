<?php

namespace frontend\modules\functional\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\functional\models\Request;
use frontend\modules\functional\models\RequestProcess;
use frontend\modules\functional\models\Material;
use frontend\modules\functional\Functional;
use app\models\UploadForm;
use yii\filters\AccessControl;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class ManageRequestGenerationController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger', \Yii::t('app', 'You do not have sufficient permissions to access this website'));
          return $this->goHome();
        },
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  public function actionManagement()
  {
    try {
      // $name_data = $this->getNameList();
      $name_data = [];
      $data = Yii::$app->request->post();

      $request_model = new Request();
      $request_type_data = $request_model->getParameter('request', 'type');
      $request_type_value =   Functional::REQUEST_TYPE_SHIPMENT_DISTRIBUTION;

      $request_process_model = new RequestProcess();
      $crop_data =  $request_process_model->getCropId();
      $crop_value =  array_key_first($crop_data);
      $small_grain_cereal_data = $request_process_model->getParameterName('crop', 'small_grain_cereal');

      $material_model = new Material();
      $material_type_data =  $material_model->getParameter('material', 'type');
      $material_type_value = Functional::MATERIAL_TYPE;


      $material_unit_data =  $material_model->getParameter('material', 'unit');
      $material_unit_value = Functional::MATERIAL_UNIT;

      $b_recipient_name_value = Yii::$app->request->post('b_recipient_name');
      $c_sender_name_value = Yii::$app->request->post('c_sender_name');

      $model_upload_material_list = new UploadForm();
    } catch (\Throwable $th) {
      Yii::$app->session->setFlash('danger', \Yii::t('app', $th->getMessage()));
    }

    return $this->render(
      'management',
      [
        'request_type_data' => $request_type_data,
        'request_type_value' => $request_type_value,
        'crop_data' => $crop_data,
        'crop_value' => $crop_value,
        'small_grain_cereal_data' => $small_grain_cereal_data,

        'material_type_data' => $material_type_data,
        'material_type_value' => $material_type_value,

        'material_unit_data' => $material_unit_data,
        'material_unit_value' => $material_unit_value,

        'name_data' =>   $name_data,
        'b_recipient_name_value' =>   $b_recipient_name_value,
        'c_sender_name_value' =>   $c_sender_name_value,

        'data' => $data,

        'model_upload_material_list' => $model_upload_material_list,
      ]
    );
  }

  public function actionSave()
  {
    $request_type_id =  Yii::$app->request->post('request_type');
    $crop_id =  Yii::$app->request->post('crop');
    $small_grain_cereal =   empty(Yii::$app->request->post('small_grain_cereal')) ? null : implode('|', Yii::$app->request->post('small_grain_cereal'));
    $small_grain_cereal_other =   empty(Yii::$app->request->post('small_grain_cereal_other')) ? null : Yii::$app->request->post('small_grain_cereal_other');
    $request_process_other =  empty(Yii::$app->request->post('request_process_other')) ? null : Yii::$app->request->post('request_process_other');
    $registration_out_of_the_system =  empty(Yii::$app->request->post('registration_out_of_the_system')) ? null : Yii::$app->request->post('registration_out_of_the_system');
    $material_type =  Yii::$app->request->post('material_type');
    $material_unit =  Yii::$app->request->post('material_unit');
    $source_database = Yii::$app->request->post('source_database');

    $_user_institution = Yii::$app->user->Identity->institution;
    $_user_laboratory = Yii::$app->user->Identity->laboratory;


    # Recipient data (shipment)
    $b_options = Yii::$app->request->post('b_options');
    if ($b_options == 'option1') {
      $b_recipient_name =  empty(Yii::$app->request->post('b_recipient_name')) ? null : Yii::$app->request->post('b_recipient_name');
    } else {
      $b_recipient_name =  empty(Yii::$app->request->post('b_recipient_name_aux')) ? null : Yii::$app->request->post('b_recipient_name_aux');
    }

    $b_institution =  empty(Yii::$app->request->post('b_institution')) ? null : Yii::$app->request->post('b_institution');
    $b_address =  empty(Yii::$app->request->post('b_address')) ? null : Yii::$app->request->post('b_address');
    $b_country =  empty(Yii::$app->request->post('b_country')) ? null : Yii::$app->request->post('b_country');

    # Sender details (shipment)
    $b_sender_name =  empty(Yii::$app->request->post('b_sender_name')) ? null : Yii::$app->request->post('b_sender_name');
    $b_program =  empty(Yii::$app->request->post('b_program')) ? null : Yii::$app->request->post('b_program');
    $b_account_number =  empty(Yii::$app->request->post('b_account_number')) ? null : Yii::$app->request->post('b_account_number');


    # Recipient data (introduction)
    $c_recipient_name =  empty(Yii::$app->request->post('c_recipient_name')) ? null : Yii::$app->request->post('c_recipient_name');
    $c_program =  empty(Yii::$app->request->post('c_program')) ? null : Yii::$app->request->post('c_program');
    $c_account_number =   empty(Yii::$app->request->post('c_account_number')) ? null : Yii::$app->request->post('c_account_number');
    #Sender details (introduction)
    $c_options = Yii::$app->request->post('c_options');
    if ($c_options == 'option1') {
      $c_sender_name =  empty(Yii::$app->request->post('c_sender_name')) ? null : Yii::$app->request->post('c_sender_name');
    } else {
      $c_sender_name =  empty(Yii::$app->request->post('c_sender_name_aux')) ? null : Yii::$app->request->post('c_sender_name_aux');
    }

    $c_sender_name = empty(Yii::$app->request->post('c_sender_name')) ? null : Yii::$app->request->post('c_sender_name');
    $c_institution = empty(Yii::$app->request->post('c_institution')) ? null : Yii::$app->request->post('c_institution');
    $c_address =  empty(Yii::$app->request->post('c_address')) ? null : Yii::$app->request->post('c_address');
    $c_country =  empty(Yii::$app->request->post('c_country')) ? null : Yii::$app->request->post('c_country');


    $c_international_phytosanitary_certificate =  empty(Yii::$app->request->post('c_international_phytosanitary_certificate')) ? null : Yii::$app->request->post('c_international_phytosanitary_certificate');
    $c_quarantine_custody_number =  empty(Yii::$app->request->post('c_quarantine_custody_number')) ? null : Yii::$app->request->post('c_quarantine_custody_number');
    $c_requirements_sheet_folio_number =  empty(Yii::$app->request->post('c_requirements_sheet_folio_number')) ? null : Yii::$app->request->post('c_requirements_sheet_folio_number');
    $c_total_shipping_weight = empty(Yii::$app->request->post('c_total_shipping_weight')) ? null : Yii::$app->request->post('c_total_shipping_weight');

    $year_code  = (int) date("Y");
    $sequential_code_group_material = Yii::$app->db->createCommand('SELECT COALESCE(Max(sequential_code), 0) + 1 AS sequential_code FROM plims.bsns_composite_sample_group;')->queryScalar();
    $composite_sample_group_code = 'CSG' . substr($year_code, -2) . '-' . substr('00' . $sequential_code_group_material, -3);
    $current_user =  Yii::$app->user->identity->id;
    $current_date = date("Y-m-d H:i:s", time());
    $request_status_id = Functional::REQUEST_CREATED;
    $observation = 'Request generated from the PLIMS WEB platform';

    $request_id = Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_request
        ( year_code,
          sequential_code,
          request_code,
          details,
          diagnostic,
          agent_qty,
          material_qty,
          sample_qty,
          total_cost,
          request_type_id,
          request_status_id,
          request_payment_id,
          pay_number,
          requested_user_id,
          owner_user_id,
          registered_by,
          registered_at,
          status,
          laboratory_id,
          institution_id
        )VALUES(
          :year_code,
          :sequential_code,
          :request_code,
          :details,
          :diagnostic,
          :agent_qty,
          :material_qty,
          :sample_qty,
          :total_cost,
          :request_type_id,
          :request_status_id,
          :request_payment_id,
          :pay_number,
          :requested_user_id,
          :owner_user_id,
          :registered_by,
          :registered_at,
          :status_active,
          :_laboratory_id,
          :_institution_id
        )RETURNING request_id;",
      [
        ':year_code' => null,
        ':sequential_code' => 0,
        ':request_code' => null,
        ':details' => 'Test request, detail value',
        ':diagnostic' => 'Pending evaluations',
        ':agent_qty' => Functional::ZERO,
        ':material_qty' => Functional::ZERO,
        ':sample_qty' => Functional::ZERO,
        ':total_cost' => Functional::ZERO,
        ':request_type_id' => $request_type_id,
        ':request_status_id' => null,
        ':request_payment_id' => Functional::REQUEST_PAYMENT_ID,
        ':pay_number' => '###-###########',
        ':requested_user_id' => $current_user,
        ':owner_user_id' => $current_user,
        ':registered_by' => $current_user,
        ':registered_at' => $current_date,
        ':status_active' => Functional::STATUS_ACTIVE,
        ':_laboratory_id' =>  $_user_laboratory,
        ':_institution_id' => $_user_institution
      ]
    )->queryScalar();

    $this->setStatusRequest($request_id, $request_status_id, $observation);

    if ($request_type_id == Functional::REQUEST_TYPE_SHIPMENT_DISTRIBUTION) {
      $recipient_name = $b_recipient_name;
      $recipient_institution = $b_institution;
      $recipient_address = $b_address;
      $recipient_country = $b_country;
      $recipient_program = null;
      $recipient_account = null;

      $sender_name = $b_sender_name;
      $sender_institution = null;
      $sender_address = null;
      $sender_country = null;
      $sender_program = $b_program;
      $sender_account = $b_account_number;
    } else if ($request_type_id == Functional::REQUEST_TYPE_INTRODUCTION) {
      $recipient_name = $c_recipient_name;
      $recipient_institution = null;
      $recipient_address = null;
      $recipient_country = null;
      $recipient_program = $c_program;
      $recipient_account = $c_account_number;

      $sender_name =  $c_sender_name;
      $sender_institution = $c_institution;
      $sender_address = $c_address;
      $sender_country = $c_country;
      $sender_program = null;
      $sender_account = null;
    }

    Yii::$app->db->createCommand()->insert('plims.bsns_request_detail', [
      'request_id' => $request_id,
      'bar_id' => 'XCF' . (string) rand(100, 1000),
      'st_id' => 'KJH' . (string) rand(100, 1000),
      'plims_id' => 'SH20-' . (string) rand(100, 1000),
      'request_program' => '...',
      'request_sub_program' => '...',
      'identification_material' => '...',
      'seed_distribution' => Parameter::findOne($request_type_id)->short_name,
      'seed_sent_by' => '...',
      'icc_seed_health_testing' => '...',
      'certification' => '...',
      'cost_shipment_paid_by' => '$ 0.00',
      'seed_purpose' => '...',
      'shipping_date_desired' => 'YYYY-MM-DD',
      'consignee_name' =>  '...',
      'request_position' => '...',
      'request_department' => '...',
      'request_institution' => '...',
      'request_street' => '...',
      'request_city' => '...',
      'request_country' =>  '...',
      'request_phone' => '###-###-######',
      'request_email' => '...',
      'requested_user' => $current_user,
      'consignee_user' => $current_user,

      'recipient_name' => $recipient_name,
      'recipient_institution' => $recipient_institution,
      'recipient_address' => $recipient_address,
      'recipient_country' => $recipient_country,
      'recipient_program' => $recipient_program,
      'recipient_account' => $recipient_account,

      'sender_name' => $sender_name,
      'sender_institution' => $sender_institution,
      'sender_address' => $sender_address,
      'sender_country' => $sender_country,
      'sender_program' => $sender_program,
      'sender_account' => $sender_account,

      'international_phytosanitary_certificate' => $c_international_phytosanitary_certificate,
      'quarantine_custody' => $c_quarantine_custody_number,
      'requirements_sheet_folio' => $c_requirements_sheet_folio_number,
      'total_shipping_weight' => $c_total_shipping_weight,
      'crop_id' => $crop_id,
    ])->execute();

    $composite_sample_group_id = Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_composite_sample_group
      (
        year_code,
        sequential_code,
        composite_sample_group_code,
        registered_by,
        registered_at,
       status
       )VALUES(
        :year_code,
        :sequential_code,
        :composite_sample_group_code,
        :registered_by,
        :registered_at,
        :status_active
      )RETURNING composite_sample_group_id;",
      [
        ':year_code' => $year_code,
        ':sequential_code' => $sequential_code_group_material,
        ':composite_sample_group_code' => $composite_sample_group_code,
        ':registered_by' => $current_user,
        ':registered_at' => $current_date,
        ':status_active' => Functional::STATUS_ACTIVE
      ]
    )->queryScalar();

    Yii::$app->db->createCommand()->insert('plims.bsns_request_process', [
      'request_id' => $request_id,
      'num_order_id' =>  Functional::NUM_ORDER_ID_PROCESS,
      'child_crop' => $small_grain_cereal,
      'child_crop_other' => $small_grain_cereal_other,
      'composite_sample_group_id' => $composite_sample_group_id,
      'crop_id' => $crop_id,
      'workflow_id' => Functional::COMPLETE_ANALYSIS,
      'essay_qty' => Functional::ZERO,
      'agent_qty' => Functional::ZERO,
      'material_qty' => Functional::ZERO,
      'sample_qty' => Functional::ZERO,
      'sub_total_cost' => Functional::ZERO,
      'request_process_status_id' => Functional::REQUEST_PROCESS_STATUS_PENDING,
      'request_process_material_id' => Functional::REQUEST_PROCESS_MATERIAL_EMPTY,
      'request_process_payment_id' => Functional::REQUEST_PROCESS_PAYMENT_INCOMPLETE,
      'payment_percentage' => Functional::ZERO,
      'request_process_other' => $request_process_other,
      'registration_out_of_the_system' => $registration_out_of_the_system,
    ])->execute();

    $sheet = 0;
    $model_upload_material_list = new UploadForm();

    if (Yii::$app->request->isPost) {
      try {
        $serial =  "-" . $composite_sample_group_code;
        $model_upload_material_list->mixFiles = UploadedFile::getInstances($model_upload_material_list, 'mixFiles');
        if ($model_upload_material_list->uploadMaterial($serial)) {
        }
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $file_name =   $model_upload_material_list->path . $model_upload_material_list->filename;
        $spreadsheet = $reader->load($file_name);
        $sheet_data = $spreadsheet->getSheet($sheet)->toArray();

        array_walk($sheet_data, function (&$item, $key) use (
          $crop_id,
          $material_type,
          $material_unit,
          $source_database,
          $composite_sample_group_id,
          $current_user,
          $current_date
        ) {
          $item[] = $crop_id; //crop_id
          $item[] = $material_type; //material_type_id  
          $item[] = $material_unit; //material_unit_id
          $item[] = 'Initial: original version'; //details
          $item[] = $source_database; //source_database_id
          $item[] = $key; //num_order
          $item[] = Functional::MATERIAL_NORMAL; //material_status_id
          $item[] = $composite_sample_group_id; //composite_sample_group_id
          $item[] = Functional::MATERIAL_HISTORICAL_DATA_VERSION_INIT; //historical_data_version
          $item[] = $current_user; //registered_by
          $item[] = $current_date; //registered_at
          $item[] = Functional::STATUS_ACTIVE; //status
        });

        $sheet_data = array_slice($sheet_data, 1);

        Yii::$app->db->createCommand()->batchInsert(
          'plims.bsns_material',
          [
            'seed_name',
            'seed_code',
            'package_label',
            'package_code',
            'germplasm_name',
            'material_unit_qty',
            'crop_id',
            'material_type_id',
            'material_unit_id',
            'details',
            'origin',
            'num_order',
            'material_status_id',
            'composite_sample_group_id',
            'historical_data_version',
            'registered_by',
            'registered_at',
            'status',
          ],
          $sheet_data
        )->execute();
      } catch (\Throwable $th) {
      }
    }
    return $this->redirect(['manage-request/index']);
  }

  public function getNameList()
  {
    $this->getCURL();
    $data = Yii::$app->session->get('user.contact_data');
    try {
      $data_provider_1 = array_map(
        function ($value) {

          if (isset($value['person']['givenName'])) {
            $result['name'] = $value['person']['givenName'] . ' ' . $value['person']['familyName'];
          } else {
            $result['name'] = '';
          }

          if (isset($value['person']['institutions']) and count($value['person']['institutions']) > 0) {
            $result['institutions'] = $value['person']['institutions'][0]['legalName'];
          } else {
            $result['institutions'] = '';
          }

          if (isset($value['addresses'][0]) and count($value['addresses']) > 0) {
            $result['streetAddress'] = $value['addresses'][0]['streetAddress'];
            $result['region'] = $value['addresses'][0]['region'];
            $result['country'] = $value['addresses'][0]['country']['name'];
          } else {
            $result['streetAddress'] = '';
            $result['region'] = '';
            $result['country'] = '';
          }
          return $result;
        },
        $data
      );
      $data_name_name = ArrayHelper::map($data_provider_1, 'name', 'name');
    } catch (\Throwable $th) {
      return [];
    }
    return $data_name_name;
  }

  public function getCURL()
  {
    // $authToken = Yii::$app->session->get('user.id_token');
    // uso temporal de este token ERNESTO
    $authToken = 'eyJ4NXQiOiJNRE00TnpZM05HWTBOV0ppTkdNMk1qSmtNams0TlRNME1EUmtNelkwT1RVeU56aGlNakl5WkRKaVpUQm1NbVJpTTJGbFlqSTFZbVl6WW1JelpEVmhNdyIsImtpZCI6Ik1ETTROelkzTkdZME5XSmlOR00yTWpKa01qazROVE0wTURSa016WTBPVFV5TnpoaU1qSXlaREppWlRCbU1tUmlNMkZsWWpJMVltWXpZbUl6WkRWaE13X1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJhdF9oYXNoIjoiTDc1MmwwZE5PVF9TdkpZMFg5LTZPZyIsImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL3VzZXJuYW1lIjoieEs1bTh1TGNta2xHc2pvcnVpZHN6WHM2ekxocURfVjJNMXVJS3I2X1V3cyIsInN1YiI6InhLNW04dUxjbWtsR3Nqb3J1aWRzelhzNnpMaHFEX1YyTTF1SUtyNl9Vd3NAY2FyYm9uLnN1cGVyIiwiaHR0cDpcL1wvd3NvMi5vcmdcL2NsYWltc1wvcm9sZSI6WyI0MGQxNzZjNy05YWZlLTQ2MWEtYTViMC0wN2U0YjVkZDIxYTciLCIyMzdjMGM3YS1lYmU0LTRlYjYtYWM1OC1kNTc2ZmQ2MmM3YTkiLCIwYmRhNTczYi1lMTc4LTQ4MjQtOTVkMi03NDcwYjFiMzRlYjQiLCI2NWEyYzMxNC05Nzk3LTRkOGUtYjNiNi1mZDYyNjUzYjhhOGQiLCI3ZDFhODczOS1jOWE5LTQ2MDYtODg3Ny1kYzkyZWYwNmVhNjgiLCJkYmRiNTg2ZS1hMzQ5LTQ3YWUtYTdkZC1hMzRmYzI0OGQwZGYiLCIyNjllZDlkNi1kMzEyLTRkMGEtYTk0ZS0wNDdjYzk2YWE0MWYiLCJlNWJkN2IzZS03NGJhLTQyNjctYWJmMy04MWI4MmUzMTBiZmUiLCIwNjY2MzYxYi1iMjk4LTRjMjMtYjBhMi00MGIyZDE1YWYyMjciLCIzZjYyMjI1ZS1jYzE3LTRjMjktODVkYi1lZjYwMjI2M2U0N2QiLCI2ZWQ3MWRhNS03MGVlLTQxYjItOWYzZS02NjVmZjIxMmQ0ZjIiLCI3NjIwYjI5OS03NWM4LTQwZmUtYTdhOS1mMDdmNDJiMDUxMzMiLCJlN2RkYmNlOC04OGNiLTRjODctYjc4ZS1iOThjM2JjMjNlNWUiLCI5NGQ0MWZlMi00ZTcwLTQ3OTAtYTIzOC01YzVjMTRiMjc4MWMiLCJlMzBkOWUxMS1kNWJiLTQ1YjMtYTExOC02Mjg2ZDQ4N2RmYmYiLCJjNTMyNTNiZS0xYjY4LTQ0MjEtYmMxYi1jM2M5YWNlYTdhN2MiLCIxMDgwNjE1My0zZWNhLTRjODgtYjU2Yi04YzdmYzMwZDZmMGMiLCJiMDE3ZDExYi1lNDI5LTRiOTMtOWY3NC1jZWQ1MTcwMGE5NWQiLCI2MWE1N2VkNi01MmFmLTQ5N2EtODU2ZS0wOWI4OWVhYTNjNGMiLCJiY2RmOWNjMC04MDA1LTQ3NmYtYmIwNS03MWM5NmYwMzgxYTYiLCI0ZjZiODcwZS0xYjY2LTQ0YzgtYWMxYy1mMGUzNWYyNGQyODAiXSwiYW1yIjpbIlNBTUxTU09BdXRoZW50aWNhdG9yIl0sImlzcyI6Imh0dHBzOlwvXC9zZy5lYnNwcm9qZWN0Lm9yZzo5NDQzXC9vYXV0aDJcL3Rva2VuIiwic2lkIjoiNWNjNWMzMjAtYzYyNC00ZjQ4LThiNDYtMzNhODAzZmE1ZWYwIiwiYXVkIjoiS0UySjR5cGk5NGlhYmJwVEVHWUVubDhLTjFRYSIsImNfaGFzaCI6IjRuX2R1VGRqaGFSeFJIUmkybGdtZEEiLCJuYmYiOjE2NDg2NDgyOTMsImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL2Z1bGxuYW1lIjpbIkJSSU9ORVMgUEVSRVlSQSIsIkVybmVzdG8gSm9zZSAoQ0lNTVlUKSJdLCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9kaXNwbGF5TmFtZSI6IkUuQlJJT05FU0BDSU1NWVQuT1JHIiwiYXpwIjoiS0UySjR5cGk5NGlhYmJwVEVHWUVubDhLTjFRYSIsImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL2VtYWlsYWRkcmVzcyI6IkUuQlJJT05FU0BDSU1NWVQuT1JHIiwiZXhwIjoxNjQ4NjUxODkzLCJpYXQiOjE2NDg2NDgyOTN9.c1qOynYfp0OQHbl4WHOKgUnGWY6uWh62KqIP9GXhsQxHXJpmeeT3O32dOcuBOSkzJFVvwkMYTT80vOijLOBBsNeFXpcO-At1BOVh91fKGhAkq0ESgzzS2_ebebt56l0CYUr50Y54SKMaEVR5_GnfBDwVjFdZlvzZ1aU_oD-6hAy95dXQlrAF-E44RBrR-x8mGHbj_SFCjvvAnoXzBbf0IO1M-VsqCzL-GiYk1gIkZaiTsXpdeS3-xxaFeVl5z1gkkoLl_clfXdUaX1iimm-uO3PjNtIQcc66NKPhVTnlLRI_46EIZLA8ZfEdWiPmrcIP0McX6yPt0rf8L8HKeka0hg';
    $graphqlParams = Yii::$app->params['graphql'];
    $result = [];

    try {
      $endpoint = $graphqlParams['endpoint'];
      $qry = $graphqlParams['qry'];
      $headers = array();
      $headers[] = $graphqlParams['header'];
      $headers[] = 'Authorization: Bearer ' . $authToken;
      $ch = curl_init();

      // Configure cURL options
      curl_setopt($ch, CURLOPT_URL, $endpoint);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $qry);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      // Make cURL request
      $response = curl_exec($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      if ($httpCode === 504) {
        curl_close($ch);
        echo 'Error:' . curl_error($ch);
        $result = [];
      } else if (curl_errno($ch)) {
        curl_close($ch);
        echo 'Error:' . curl_error($ch);
        $result = [];
      } else {
        curl_close($ch);
        $array_data = json_decode($response, true);
        $result = $array_data['data']['findContactList']['content'];
      }
    } catch (ClientException $e) {
      $result = [];
    } catch (ServerException $e) {
      $result = [];
    } catch (\Throwable $e) {
      $result = [];
    }

    Yii::$app->session->set('user.contact_data', $result);
  }

  public function actionNameAddress($id = null)
  {
    $data = Yii::$app->session->get('user.contact_data');
    $data_provider_1 = array_map(
      function ($value) {
        if (isset($value['person']['givenName'])) {
          $result['name'] = $value['person']['givenName'] . ' ' . $value['person']['familyName'];
        } else {
          $result['name'] = '';
        }

        if (isset($value['person']['institutions']) and count($value['person']['institutions']) > 0) {
          $result['institutions'] = $value['person']['institutions'][0]['legalName'];
        } else {
          $result['institutions'] = '';
        }

        if (isset($value['addresses'][0]) and count($value['addresses']) > 0) {
          $result['streetAddress'] = $value['addresses'][0]['streetAddress'];
          $result['region'] = $value['addresses'][0]['region'];
          $result['country'] = $value['addresses'][0]['country']['name'];
        } else {
          $result['streetAddress'] = '';
          $result['region'] = '';
          $result['country'] = '';
        }
        return $result;
      },
      $data
    );
    $data_provider_2 = ArrayHelper::map($data_provider_1, 'name', 'streetAddress');


    if (is_null($id) or !isset($data_provider_2[$id])) {
      return 'No registered address';
    } else  if (empty($data_provider_2[$id])) {
      return 'No data';
    } else {
      return $data_provider_2[$id];
    }
  }

  public function actionNameCountry($id = null)
  {
    $data = Yii::$app->session->get('user.contact_data');
    $data_provider_1 = array_map(
      function ($value) {
        if (isset($value['person']['givenName'])) {
          $result['name'] = $value['person']['givenName'] . ' ' . $value['person']['familyName'];
        } else {
          $result['name'] = '';
        }

        if (isset($value['person']['institutions']) and count($value['person']['institutions']) > 0) {
          $result['institutions'] = $value['person']['institutions'][0]['legalName'];
        } else {
          $result['institutions'] = '';
        }

        if (isset($value['addresses'][0]) and count($value['addresses']) > 0) {
          $result['streetAddress'] = $value['addresses'][0]['streetAddress'];
          $result['region'] = $value['addresses'][0]['region'];
          $result['country'] = $value['addresses'][0]['country']['name'];
        } else {
          $result['streetAddress'] = '';
          $result['region'] = '';
          $result['country'] = '';
        }
        return $result;
      },
      $data
    );
    $data_provider_2 = ArrayHelper::map($data_provider_1, 'name', 'country');
    if (is_null($id) or !isset($data_provider_2[$id])) {
      return 'No registered country';
    } else  if (empty($data_provider_2[$id])) {
      return 'No data';
    } else {
      return $data_provider_2[$id];
    }
  }

  public function actionNameInstitution($id = null)
  {
    $data = Yii::$app->session->get('user.contact_data');
    $data_provider_1 = array_map(
      function ($value) {
        if (isset($value['person']['givenName'])) {
          $result['name'] = $value['person']['givenName'] . ' ' . $value['person']['familyName'];
        } else {
          $result['name'] = '';
        }

        if (isset($value['person']['institutions']) and count($value['person']['institutions']) > 0) {
          $result['institutions'] = $value['person']['institutions'][0]['legalName'];
        } else {
          $result['institutions'] = '';
        }

        if (isset($value['addresses'][0]) and count($value['addresses']) > 0) {
          $result['streetAddress'] = $value['addresses'][0]['streetAddress'];
          $result['region'] = $value['addresses'][0]['region'];
          $result['country'] = $value['addresses'][0]['country']['name'];
        } else {
          $result['streetAddress'] = '';
          $result['region'] = '';
          $result['country'] = '';
        }
        return $result;
      },
      $data
    );
    $data_provider_2 = ArrayHelper::map($data_provider_1, 'name', 'institutions');
    if (is_null($id) or !isset($data_provider_2[$id])) {
      return 'No registered institution';
    } else  if (empty($data_provider_2[$id])) {
      return 'No data';
    } else {
      return $data_provider_2[$id];
    }
  }

  public function setStatusRequest($request_id, $request_status_id, $observation)
  {
    $number_index = Yii::$app->db->createCommand(
      "SELECT COALESCE(Max(number_index) + 1, 1) AS number_index
    FROM   plims.bsns_status
    WHERE  request_id = :request_id;",
      [':request_id' => $request_id]
    )->queryScalar();

    // condicional, si no no hacer la siguiente linea
    Yii::$app->db->createCommand()->insert(
      'plims.bsns_status',
      [
        'request_id' => $request_id,
        'request_status_id' => $request_status_id,
        'number_index' => $number_index,
        'status_date' => date("Y-m-d H:i:s", time()),
        'status_by' => Yii::$app->user->identity->id,
        'observation' => $observation,
      ]
    )->execute();

    // siguiente linea
    Yii::$app->db->createCommand()->update(
      'plims.bsns_request',
      [
        'request_status_id' => $request_status_id,
      ],
      'request_id = :request_id',
      [':request_id' => $request_id]
    )->execute();
  }
}

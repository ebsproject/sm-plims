<?php

namespace frontend\modules\functional\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use frontend\modules\functional\Functional;
use yii\data\SqlDataProvider;
use yii\filters\AccessControl;

class ManageSearchController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger', \Yii::t('app', 'You do not have sufficient permissions to access this website'));
          return $this->goHome();
        },
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  public function actionManagement()
  {
    $data_post = Yii::$app->request->post();
    $data_get = Yii::$app->request->get();

    $_rt_id = Yii::$app->request->post('rt_options');
    $_crop_id = Yii::$app->request->post('crop_options');
    $_process = Yii::$app->request->post('process_options');
    $_cst_id = Yii::$app->request->post('cst_options');
    $_result_id = Yii::$app->request->post('result_options');

    $_rt_id = $_rt_id != null && $_rt_id != 'null' ? $_rt_id : NULL;
    $_crop_id = $_crop_id != null && $_crop_id != 'null' ? $_crop_id : NULL;
    $_process = $_process != null && $_process != 'null' ? $_process : NULL;
    $_cst_id = $_cst_id != null && $_cst_id != 'null' ? $_cst_id : NULL;
    $_result_id = $_result_id != null && $_result_id != 'null' ? $_result_id : NULL;


    $_requested_id_list = Yii::$app->request->post('_requested_name');
    $_registrar_id_list = Yii::$app->request->post('_registrar_name');
    $_year_id_list = Yii::$app->request->post('_year_name');
    $_status_id_list = Yii::$app->request->post('_status_name');
    $_activity_id_list = Yii::$app->request->post('_activity_name');
    $_agent_id_list = Yii::$app->request->post('_agent_name');
    $_assay_id_list = Yii::$app->request->post('_assay_name');
    $_request_id_list = Yii::$app->request->post('_request_name');
    $_limit = Yii::$app->request->post('_limit');

    $_requested_id_list_text = $_requested_id_list != null && count($_requested_id_list) > 0 ? implode(",", $_requested_id_list)  : null;
    $_registrar_id_list_text = $_registrar_id_list != null && count($_registrar_id_list) > 0 ? implode(",", $_registrar_id_list)  : null;
    $_year_id_list_text = $_year_id_list != null && count($_year_id_list) > 0 ? implode(",", $_year_id_list)  : null;
    $_status_id_list_text = $_status_id_list != null && count($_status_id_list) > 0 ? implode(",", $_status_id_list)  : null;
    $_activity_id_list_text = $_activity_id_list != null && count($_activity_id_list) > 0 ? implode(",", $_activity_id_list)  : null;
    $_agent_id_list_text = $_agent_id_list != null && count($_agent_id_list) > 0 ? implode(",", $_agent_id_list)  : null;
    $_assay_id_list_text = $_assay_id_list != null && count($_assay_id_list) > 0 ? implode(",", $_assay_id_list)  : null;
    $_request_id_list_text = $_request_id_list != null && count($_request_id_list) > 0 ? implode(",", $_request_id_list)  : null;


    $session = Yii::$app->session;
    if ($data_post) {
      $session['post_data'] = [
        '_rt_id' => $_rt_id,
        '_crop_id' => $_crop_id,
        '_process' => $_process,
        '_cst_id' => $_cst_id,
        '_result_id' => $_result_id,
        '_requested_id_list' => $_requested_id_list,
        '_registrar_id_list' => $_registrar_id_list,
        '_year_id_list' => $_year_id_list,
        '_status_id_list' => $_status_id_list,
        '_activity_id_list' => $_activity_id_list,
        '_agent_id_list' => $_agent_id_list,
        '_assay_id_list' => $_assay_id_list,
        '_request_id_list' => $_request_id_list,
        '_limit' => $_limit,
        '_requested_id_list_text' => $_requested_id_list_text,
        '_registrar_id_list_text' => $_registrar_id_list_text,
        '_year_id_list_text' => $_year_id_list_text,
        '_status_id_list_text' => $_status_id_list_text,
        '_activity_id_list_text' => $_activity_id_list_text,
        '_agent_id_list_text' => $_agent_id_list_text,
        '_assay_id_list_text' => $_assay_id_list_text,
        '_request_id_list_text' => $_request_id_list_text,
      ];
    } else if ($data_get and isset($session['post_data'])) {
      // GET
      $_rt_id = $session['post_data']['_rt_id'];
      $_crop_id = $session['post_data']['_crop_id'];
      $_process = $session['post_data']['_process'];
      $_cst_id = $session['post_data']['_cst_id'];
      $_result_id = $session['post_data']['_result_id'];
      $_requested_id_list = $session['post_data']['_requested_id_list'];
      $_registrar_id_list = $session['post_data']['_registrar_id_list'];
      $_year_id_list = $session['post_data']['_year_id_list'];
      $_status_id_list = $session['post_data']['_status_id_list'];
      $_activity_id_list = $session['post_data']['_activity_id_list'];
      $_agent_id_list = $session['post_data']['_agent_id_list'];
      $_assay_id_list = $session['post_data']['_assay_id_list'];
      $_request_id_list = $session['post_data']['_request_id_list'];
      $_limit = $session['post_data']['_limit'];
      $_requested_id_list_text = $session['post_data']['_requested_id_list_text'];
      $_registrar_id_list_text = $session['post_data']['_registrar_id_list_text'];
      $_year_id_list_text = $session['post_data']['_year_id_list_text'];
      $_status_id_list_text = $session['post_data']['_status_id_list_text'];
      $_activity_id_list_text = $session['post_data']['_activity_id_list_text'];
      $_agent_id_list_text = $session['post_data']['_agent_id_list_text'];
      $_assay_id_list_text = $session['post_data']['_assay_id_list_text'];
      $_request_id_list_text = $session['post_data']['_request_id_list_text'];
    } else {
      $session->remove('post_data');
    }
    $msg = $session['post_data'];


    // ===============================================================
    $data = Yii::$app->db->createCommand(
      "SELECT DISTINCT t1.requested_user_id AS requested_id,
                    t2.username as requested_user
        FROM   plims.bsns_request t1
        INNER JOIN plims.auth_user t2
                ON t1.requested_user_id = t2.user_id
        ORDER  BY username ASC;"
    )->queryAll();
    $_requested_data = ArrayHelper::map($data, 'requested_id', 'requested_user');



    $data = Yii::$app->db->createCommand(
      "SELECT DISTINCT t1.registered_by AS registrar_id,
                    t2.username as registrar_user
        FROM   plims.bsns_reading_data  t1
        INNER JOIN plims.auth_user t2
                ON t1.registered_by  = t2.user_id
        ORDER  BY username ASC;"
    )->queryAll();
    $_registrar_data = ArrayHelper::map($data, 'registrar_id', 'registrar_user');

    $data = Yii::$app->db->createCommand(
      "SELECT DISTINCT year_code AS year_id,
            year_code AS year_name
      FROM   plims.bsns_request
      WHERE  status = :_status_active
      AND year_code IS NOT NULL
      ORDER  BY year_code;",
      ['_status_active' => Functional::STATUS_ACTIVE]
    )->queryAll();
    $_year_data = ArrayHelper::map($data, 'year_id', 'year_name');

    $data = Yii::$app->db->createCommand(
      "SELECT parameter_id AS status_id,
                short_name   AS status_name
        FROM   plims.bsns_parameter
        WHERE  status = :_status_active
                AND entity = 'request'
                AND singularity = 'status'
        ORDER  BY code; ",
      ['_status_active' => Functional::STATUS_ACTIVE]
    )->queryAll();
    $_status_data = ArrayHelper::map($data, 'status_id', 'status_name');



    $data = Yii::$app->db->createCommand(
      "SELECT activity_id,
                long_name AS activity_name
        FROM   plims.bsns_activity
        WHERE  status = :_status_active
        ORDER  BY activity_name;",
      ['_status_active' => Functional::STATUS_ACTIVE]
    )->queryAll();
    $_activity_data = ArrayHelper::map($data, 'activity_id', 'activity_name');



    $data = Yii::$app->db->createCommand(
      "SELECT agent_id,
                short_name AS agent_name
        FROM   plims.bsns_agent
        WHERE  status = :_status_active
        ORDER  BY agent_name; ",
      ['_status_active' => Functional::STATUS_ACTIVE]
    )->queryAll();
    $_agent_data = ArrayHelper::map($data, 'agent_id', 'agent_name');



    $data = Yii::$app->db->createCommand(
      "SELECT essay_id, short_name AS assay_name
        FROM   plims.bsns_essay
        ORDER  BY assay_name;"
    )->queryAll();
    $_assay_data = ArrayHelper::map($data, 'essay_id', 'assay_name');



    $data = Yii::$app->db->createCommand(
      "SELECT request_id,
              request_code AS request_name
        FROM   plims.bsns_request
        ORDER  BY request_code; "
    )->queryAll();
    $_request_data = ArrayHelper::map($data, 'request_id', 'request_name');
    // ===============================================================

    $_registered_at = null;
    $_at_ini = null;
    $_at_end = null;

    $_registered_by = null;
    $_status = null;
    $_limit = $_limit == null ? '10' : $_limit;

    $query = "SELECT 
            r1.request_id                                     AS request_id,
            r1.num_order_id                                   AS num_order_id,
            r1.essay_id                                       AS essay_id,
            r1.composite_sample_type_id                       AS composite_sample_type_id,
            r1.request_process_essay_id                       AS request_process_essay_id,

            t1.request_code,
            t1.sequential_code,
            p3.short_name                                     AS request_type,
            p2.short_name                                     AS request_status,
            CASE
                WHEN r1.num_order_id = :_ini_order THEN 'normal-process'
                ELSE 're-process'
            end                                               AS process_type,
            c1.short_name                                     AS crop_name,
            e1.short_name                                     AS assay_name,
            a1.long_name                                      AS activity_name,
            p1.long_name                                      AS sample_type,
            r1.tertiary_order_num,
            u1.username                                       AS requested_user,
            t1.registered_at                                  AS requesst_creation_date,
            -- String_agg(DISTINCT a2.short_name, ', ')                                AS agents,
            String_agg(DISTINCT '<span class=''label label-'||r1.text_result||'''>'||a2.short_name||'</span>', ' ')                            AS text_results,
            String_agg(DISTINCT '<span class=''label label-'||COALESCE(r1.auxiliary_result, 'info')||'''>' ||a2.short_name|| '</span>', ' ')   AS auxiliary_results,
            String_agg(DISTINCT u2.username, ', ')                                                                                              AS registrar_users

        FROM   plims.bsns_reading_data r1
            INNER JOIN plims.bsns_request t1
                    ON r1.request_id = t1.request_id
                        AND t1.status = :_status_active
            ------------------------------
            LEFT JOIN plims.bsns_crop c1
                    ON r1.crop_id = c1.crop_id
            LEFT JOIN plims.bsns_essay e1
                    ON r1.essay_id = e1.essay_id
            LEFT JOIN plims.bsns_activity a1
                    ON r1.activity_id = a1.activity_id
            LEFT JOIN plims.bsns_agent a2
                    ON r1.agent_id = a2.agent_id
            LEFT JOIN plims.bsns_parameter p1
                    ON r1.composite_sample_type_id = p1.parameter_id
            LEFT JOIN plims.bsns_parameter p2
                    ON t1.request_status_id = p2.parameter_id
            LEFT JOIN plims.bsns_parameter p3
                    ON p3.parameter_id = t1.request_type_id
            ------------------------------
            LEFT JOIN plims.auth_user u1
                        ON u1.user_id = t1.requested_user_id
            LEFT JOIN plims.auth_user u2
                    ON u2.user_id = r1.registered_by
        WHERE  ( r1.crop_id = :_crop_id OR :_crop_id IS NULL ) 

            AND ( r1.composite_sample_type_id = :_cst_id OR :_cst_id IS NULL )
            AND ( r1.num_order_id = :_process OR :_process IS NULL )
            AND ( r1.text_result = :_result_id OR r1.auxiliary_result = :_result_id OR :_result_id IS NULL )
            AND ( r1.registered_at = :_registered_at OR :_registered_at IS NULL )
            AND ( r1.registered_at >= :_at_ini OR :_at_ini IS NULL )
            AND ( r1.registered_at <= :_at_end OR :_at_end IS NULL )
            AND ( r1.registered_by = :_registered_by OR :_registered_by IS NULL )
            AND ( r1.status = :_status OR :_status IS NULL )
            AND ( t1.request_type_id = :_rt_id OR :_rt_id IS NULL )

            AND ( t1.requested_user_id in (" . (is_null($_requested_id_list_text) ? 'NULL' : $_requested_id_list_text) . ") OR :_requested_id_list_text::text IS NULL)

            AND ( r1.registered_by in (" . (is_null($_registrar_id_list_text) ? 'NULL' : $_registrar_id_list_text) . ") OR :_registrar_id_list_text::text IS NULL)

            AND ( t1.year_code in (" . (is_null($_year_id_list_text) ? 'NULL' : $_year_id_list_text) . ") OR :_year_id_list_text::text IS NULL)

            AND ( t1.request_status_id in (" . (is_null($_status_id_list_text) ? 'NULL' : $_status_id_list_text) . ") OR :_status_id_list_text::text IS NULL)

            AND ( r1.activity_id in (" . (is_null($_activity_id_list_text) ? 'NULL' : $_activity_id_list_text) . ") OR :_activity_id_list_text::text IS NULL)

            AND ( r1.agent_id in (" . (is_null($_agent_id_list_text) ? 'NULL' : $_agent_id_list_text) . ") OR :_agent_id_list_text::text IS NULL)

            AND ( r1.essay_id in (" . (is_null($_assay_id_list_text) ? 'NULL' : $_assay_id_list_text) . ") OR :_assay_id_list_text::text IS NULL)

            AND ( t1.request_id in (" . (is_null($_request_id_list_text) ? 'NULL' : $_request_id_list_text) . ") OR :_request_id_list_text::text IS NULL)

        GROUP  BY 
                r1.request_id,
                r1.num_order_id,
                r1.essay_id,
                r1.composite_sample_type_id,
                r1.request_process_essay_id,
                request_code,
                sequential_code,
                request_type,
                request_status,
                process_type,
                crop_name,
                assay_name,
                activity_name,
                sample_type,
                tertiary_order_num,
                requested_user,
                requesst_creation_date";

    $dataProvider = new SqlDataProvider([
      'sql' => $query,
      'params' => [
        ':_ini_order' => Functional::NUM_ORDER_ID_PROCESS,
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_crop_id' => $_crop_id,
        ':_cst_id' => $_cst_id,
        ':_process' => $_process,

        ':_requested_id_list_text' => $_requested_id_list_text,
        ':_registrar_id_list_text' => $_registrar_id_list_text,
        ':_year_id_list_text' => $_year_id_list_text,
        ':_status_id_list_text' => $_status_id_list_text,
        ':_activity_id_list_text' => $_activity_id_list_text,
        ':_agent_id_list_text' => $_agent_id_list_text,
        ':_assay_id_list_text' => $_assay_id_list_text,
        ':_request_id_list_text' => $_request_id_list_text,

        ':_registered_at' => $_registered_at,
        ':_at_ini' => $_at_ini,
        ':_at_end' => $_at_end,
        ':_registered_by' => $_registered_by,
        ':_status' => $_status,
        ':_rt_id' => $_rt_id,

        ':_result_id' => $_result_id,
      ],
      'pagination' => [
        'pageSize' => $_limit,
      ],
      'sort' => [
        'attributes' => [
          'request_code',
          'sequential_code',
          'request_type',
          'request_status',
          'process_type',
          'crop_name',
          'assay_name',
          'activity_name',
          'sample_type',
          'tertiary_order_num',
          'requested_user',
          'requesst_creation_date',

          //'agents',
          'text_results',
          'auxiliary_results',
          'registrar_users'
        ],
        'defaultOrder' => [
          'sequential_code' => SORT_DESC,
          'assay_name' => SORT_ASC,
          'process_type' => SORT_ASC,
          'tertiary_order_num' => SORT_ASC
        ],
      ],
    ]);

    return $this->render(
      'management',
      [
        'data_get' => $data_get,
        'data_post' => $data_post,
        'session' => $session,
        'msg' => $msg,



        '_rt_id' => $_rt_id,
        '_crop_id' => $_crop_id,
        '_process' => $_process,
        '_cst_id' => $_cst_id,
        '_result_id' => $_result_id,


        '_limit' => $_limit,
        'dataProvider' => $dataProvider,


        '_requested_data' => $_requested_data,
        '_requested_id_list' => $_requested_id_list,

        '_registrar_data' => $_registrar_data,
        '_registrar_id_list' => $_registrar_id_list,

        '_year_id_list' => $_year_id_list,
        '_year_data' => $_year_data,

        '_status_id_list' => $_status_id_list,
        '_status_data' => $_status_data,

        '_activity_id_list' => $_activity_id_list,
        '_activity_data' => $_activity_data,

        '_agent_id_list' => $_agent_id_list,
        '_agent_data' => $_agent_data,

        '_assay_id_list' => $_assay_id_list,
        '_assay_data' => $_assay_data,

        '_request_id_list' => $_request_id_list,
        '_request_data' => $_request_data,

      ]
    );
  }

  public function actionReset()
  {
    $this->redirect(
      [
        'management',

      ]
    );
  }
}

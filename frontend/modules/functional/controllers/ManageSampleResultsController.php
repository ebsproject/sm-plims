<?php

namespace frontend\modules\functional\controllers;

use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Parameter;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use frontend\modules\functional\models\Request;
use yii\data\ArrayDataProvider;
use frontend\modules\functional\Functional;
use frontend\models\Script;
use yii\filters\AccessControl;

class ManageSampleResultsController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger', \Yii::t('app', 'You do not have sufficient permissions to access this website'));
          return $this->goHome();
        },
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  // Management
  public function actionManagement(
    $request_id,
    $num_order_id,
    $essay_id,
    $composite_sample_type_id,
    $request_process_essay_id,
    $sample = null
  ) {

    $user = Yii::$app->user->identity->id;
    $date = date("Y-m-d H:i:s", time());

    $r_model = Request::findOne($request_id);
    $request_code = $r_model->request_code;

    $e_model = Essay::findOne($essay_id);
    $assay_name = $e_model->short_name;



    $p_model = Parameter::findOne($composite_sample_type_id);
    $sample_type_name = $p_model->long_name;



    $rpa_status_id = Yii::$app->db->createCommand(
      "SELECT request_process_essay_status_id
      FROM   plims.bsns_request_process_essay
      WHERE  request_process_essay_id = :request_process_essay_id;",
      [
        ':request_process_essay_id' => $request_process_essay_id
      ]
    )->queryScalar();



    $is_web_assay = Yii::$app->db->createCommand(
      "SELECT CASE
            WHEN t1.essay_type_id = :_web_type THEN true
            ELSE false
          END AS is_web_assay
      FROM   plims.bsns_essay t1
      WHERE  t1.essay_id = :_assay_id;",
      [
        ':_assay_id' => $essay_id,
        ':_web_type' => Functional::ESSAY_TYPE_WEB_APP
      ]
    )->queryScalar();

    // set template result settings
    if ($rpa_status_id == Functional::REQUEST_PROCESS_ESSAY_PENDING && $is_web_assay) {
      Script::instance()->checkStatusInProgressSubProcess($request_process_essay_id, $user, $date);
      return $this->redirect(
        [
          'management',
          'request_id' => $request_id,
          'num_order_id' => $num_order_id,
          'essay_id' => $essay_id,
          'composite_sample_type_id' => $composite_sample_type_id,
          'request_process_essay_id' => $request_process_essay_id,
          'sample' => $sample,
        ]
      );
    } else if ($rpa_status_id == Functional::REQUEST_PROCESS_ESSAY_PENDING) {
      Yii::$app->session->setFlash('danger', \Yii::t('app', 'You can start the process only through the mobile application'));

      return $this->redirect(
        [
          './manage-request/management',
          'request_id' => $request_id,
        ]
      );
    }

    // ======================================================================Set Results
    // get sample list
    $rpea_sample_list =  $this->getSampleList($request_process_essay_id);
    // get sample value
    $sample = empty(Yii::$app->request->post('rpe_activity_sample_id')) ?
      (is_null($sample) ?
        array_keys($rpea_sample_list)[0] : $sample) : Yii::$app->request->post('rpe_activity_sample_id');
    // get reading data list
    $rd_list = $this->getReadingDataList($sample);

    $tab_list_1 = [];
    $tab_content_list_1 = [];

    $tab_list_2 = [];
    $tab_content_list_2 = [];

    $tab_list_3 = [];
    $tab_content_list_3 = [];

    // valores booleanos de informacion 
    $is_set_bulk = ($composite_sample_type_id != Functional::COMPOSITE_SAMPLE_GENERAL ? true : false);
    $is_finalisable_in_mobile_essays = $essay_id == Functional::ESSAY_ELISA ? true : false;
    $is_in_process = $rpa_status_id == Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS ? true : false;
    $is_web_source = false;


    $is_ready_to_finish = Yii::$app->db->createCommand(
      "SELECT CASE
            WHEN count(is_complete) > 0 THEN false
            ELSE true
          END AS is_ready_to_finish
      FROM(
      SELECT DISTINCT
        CASE
            WHEN all_samples = samples
                AND all_agents = agents THEN true
            ELSE false
          END AS is_complete 
      FROM       (SELECT DISTINCT t2.composite_sample_id      AS all_samples,
                              t2.request_process_essay_id AS id
              FROM   plims.bsns_request_process_essay_activity_sample t2
                      INNER JOIN plims.bsns_composite_sample t3
                              ON t2.composite_sample_id = t3.composite_sample_id
              WHERE  t2.request_process_essay_id = :_rpa_id
              -- avalible (revisar si es necesario)
                      AND t3.reading_data_type_id = :_rdt_entry
                      AND t2.activity_id = :_activity_get ) t5
      INNER JOIN
              (
                SELECT   string_agg(Cast(t4.agent_id AS TEXT), '|' order BY t4.agent_id) AS all_agents,
                          t4.request_process_essay_id                                     AS id
                FROM     plims.bsns_request_process_essay_agent t4
                WHERE    t4.request_process_essay_id = :_rpa_id
                AND t4.status = :_status_active
                GROUP BY id) t6
      ON         t5.id = t6.id
      LEFT JOIN
              (
                SELECT   tab3.composite_sample_id                               AS samples,
                          string_agg( tab3.agent_id, '|' ORDER BY tab3.agent_id) AS agents,
                          tab3.request_process_essay_id                          AS id
                FROM     (
                              SELECT DISTINCT t1.composite_sample_id,
                                              cast(t1.agent_id AS text) AS agent_id,
                                              t1.request_process_essay_id
                              FROM            plims.bsns_reading_data t1 -- cambiar por la bolsa posteriormente
                              WHERE           t1.request_process_essay_id = :_rpa_id
                              AND             t1.reading_data_type_id = :_rdt_entry
                              AND             t1.activity_id = :_activity_get
                              AND             t1.status = :_status_active ) tab3
                GROUP BY samples, id ) t7
      ON         t5.all_samples = t7.samples
      ) t8 WHERE is_complete = false;",
      [
        ':_rpa_id' => $request_process_essay_id,
        ':_rdt_entry' => Functional::READING_DATA_ENTRY,
        ':_activity_get' => Functional::ACTIVITY_CONSOLIDATE,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryScalar();


    $assay_web_list = [Functional::ESSAY_VISUAL, Functional::ESSAY_WASH, Functional::ESSAY_WASHING, Functional::ESSAY_GLOEOTINIA, Functional::ESSAY_ANILINE, Functional::ESSAY_FILTRATION_CENTRIFUGATION, Functional::ESSAY_ENDOSPERM_HYDROLYSIS, Functional::ESSAY_STAINING_TEST];
    $assay_elisa_list = [Functional::ESSAY_ELISA];
    $assay_pcr_list = [Functional::ESSAY_PCR];
    $assay_blotter_list = [Functional::ESSAY_BLOTTER];
    $assay_germination_list = [Functional::ESSAY_GERMINATION];

    switch (true) {
      case in_array($essay_id, $assay_web_list):
        // ==============================================
        $data = $this->getDataTypeWebAssay($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = "active"',
          'href' => '#tab_1',
          'text' => 'Data Results'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane active"',
          'id' => 'tab_1',
          'title' => 'Data Results',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $is_web_source = true;
        $data = $this->getDataTypeHistoricalResultWeb($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = ""',
          'href' => '#tab_2',
          'text' => 'Historical results'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane"',
          'id' => 'tab_2',
          'title' => 'Historical results',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        break;

      case in_array($essay_id, $assay_elisa_list):
        // ==============================================
        $data = $this->getDataTypeByConsolidationElisa($request_process_essay_id, 29);
        $tab_list_1[] = [
          'class' => 'class = "active"',
          'href' => '#tab_3',
          'text' => 'Consolidate'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane active"',
          'id' => 'tab_3',
          'title' => 'Consolidate',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataTypeByActivityElisa($request_process_essay_id, 5);
        $tab_list_1[] = [
          'class' => 'class = ""',
          'href' => '#tab_1',
          'text' => 'Agent absorbance evaluation 1'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane "',
          'id' => 'tab_1',
          'title' => 'Agent absorbance evaluation 1',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataTypeByActivityElisa($request_process_essay_id, 6);
        $tab_list_1[] = [
          'class' => 'class = ""',
          'href' => '#tab_2',
          'text' => 'Agent absorbance evaluation 2'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane"',
          'id' => 'tab_2',
          'title' => 'Agent absorbance evaluation 2',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        break;

      case in_array($essay_id, $assay_pcr_list):
        // ==============================================
        $data = $this->getDataTypeWebAssay($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = "active"',
          'href' => '#tab_1',
          'text' => 'Data Results'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane active"',
          'id' => 'tab_1',
          'title' => 'Data Results',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataTypeHistoricalResultPcr($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = ""',
          'href' => '#tab_2',
          'text' => 'Historical results'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane"',
          'id' => 'tab_2',
          'title' => 'Historical results',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        break;

      case in_array($essay_id, $assay_blotter_list):
        // ==============================================
        $data = $this->getDataResultsBlotter($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = "active"',
          'href' => '#tab_1',
          'text' => 'Data Results'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane active"',
          'id' => 'tab_1',
          'title' => 'Data Results',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataAveragesBlotter($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = ""',
          'href' => '#tab_2',
          'text' => 'Averages'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane"',
          'id' => 'tab_2',
          'title' => 'Averages',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataHistoricalResultsBlotter($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = ""',
          'href' => '#tab_3',
          'text' => 'Historical results'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane"',
          'id' => 'tab_3',
          'title' => 'Historical results',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataSowingBlotter($request_process_essay_id);
        $tab_list_2[] = [
          'class' => 'class = "active"',
          'href' => '#tab_2_1',
          'text' => 'Sowing'
        ];
        $tab_content_list_2[] = [
          'class' => 'class = "tab-pane active"',
          'id' => 'tab_2_1',
          'title' => 'Sowing',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataHistoricalSowingBlotter($request_process_essay_id);
        $tab_list_2[] = [
          'class' => 'class = ""',
          'href' => '#tab_2_2',
          'text' => 'Historical sowing'
        ];
        $tab_content_list_2[] = [
          'class' => 'class = "tab-pane"',
          'id' => 'tab_2_2',
          'title' => 'Historical sowing',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        break;

      case in_array($essay_id, $assay_germination_list):
        // ==============================================
        $data = $this->getDataResultsGermination($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = "active"',
          'href' => '#tab_1',
          'text' => 'Data Results'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane active"',
          'id' => 'tab_1',
          'title' => 'Data Results',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataAveragesGermination($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = ""',
          'href' => '#tab_2',
          'text' => 'Averages'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane"',
          'id' => 'tab_2',
          'title' => 'Averages',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataHistoricalResultsGermination($request_process_essay_id);
        $tab_list_1[] = [
          'class' => 'class = ""',
          'href' => '#tab_3',
          'text' => 'Historical results'
        ];
        $tab_content_list_1[] = [
          'class' => 'class = "tab-pane"',
          'id' => 'tab_3',
          'title' => 'Historical results',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataSowingGermination($request_process_essay_id);
        $tab_list_2[] = [
          'class' => 'class = "active"',
          'href' => '#tab_2_1',
          'text' => 'Sowing'
        ];
        $tab_content_list_2[] = [
          'class' => 'class = "tab-pane active"',
          'id' => 'tab_2_1',
          'title' => 'Sowing',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        $data = $this->getDataHistoricalSowingGermination($request_process_essay_id);
        $tab_list_2[] = [
          'class' => 'class = ""',
          'href' => '#tab_2_2',
          'text' => 'Historical sowing'
        ];
        $tab_content_list_2[] = [
          'class' => 'class = "tab-pane"',
          'id' => 'tab_2_2',
          'title' => 'Historical sowing',
          'dataProvider' => $data['provider'],
          'dataColumns' => $data['columns'],
        ];
        // ==============================================
        break;
    }

    return $this->render(
      'management',
      [
        'request_id' => $request_id,
        'essay_id' => $essay_id,
        'num_order_id' => $num_order_id,
        'request_code' => $request_code,
        'assay_name' => $assay_name,
        'sample_type_name' => $sample_type_name,
        'rpea_sample_count' => count($rpea_sample_list),
        'is_web_source' => $is_web_source,
        'is_in_process' => $is_in_process,

        // solo en Elisa
        'is_ready_to_finish' => $is_ready_to_finish,
        'is_finalisable_in_mobile_essays' => $is_finalisable_in_mobile_essays,

        // Set Results
        'rpea_sample_list' => $rpea_sample_list,
        'sample' => $sample,
        'rd_list' => $rd_list,
        'is_set_bulk' => $is_set_bulk,

        // Results
        'tab_list_1' =>   $tab_list_1,
        'tab_content_list_1' =>   $tab_content_list_1,

        'tab_list_2' => $tab_list_2,
        'tab_content_list_2' => $tab_content_list_2,

        'tab_list_3' => $tab_list_3,
        'tab_content_list_3' => $tab_content_list_3,
      ]
    );
  }

  private function getSampleList($request_process_essay_id)
  {
    $rpea_sample_list = Yii::$app->db->createCommand(
      'SELECT request_process_essay_activity_sample_id as "id",
            t4.short_name as "name"
      FROM   plims.bsns_request_process_essay t1
            INNER JOIN plims.bsns_request_process_essay_activity t2
                    ON t1.request_process_essay_id = t2.request_process_essay_id
                      AND t1.request_process_essay_id = :request_process_essay_id
            INNER JOIN plims.bsns_request_process_essay_activity_sample t3
                    ON t2.request_process_essay_activity_id =
                      t3.request_process_essay_activity_id
            INNER JOIN plims.bsns_composite_sample t4
                    ON t3.composite_sample_id = t4.composite_sample_id
      ORDER  BY t4.num_order;',
      [
        ':request_process_essay_id' => $request_process_essay_id,
      ]
    )->queryAll();

    return ArrayHelper::map($rpea_sample_list, 'id', 'name');
  }

  private function getReadingDataList($sample)
  {
    $rd_list = Yii::$app->db->createCommand(
      "SELECT 
            CASE
              WHEN r1.reading_data_id IS NULL THEN 0
              ELSE r1.reading_data_id
            end                               AS rd_id,

            t2.request_process_essay_agent_id AS id,
            a1.short_name                     AS agent_name,
            CASE
              WHEN r1.text_result IS NULL THEN 'empty'
              ELSE r1.text_result
            end                               AS result,
            CASE
              WHEN r1.historical_data_version IS NULL THEN 0
              ELSE r1.historical_data_version
            end                               AS version
      FROM   plims.bsns_request_process_essay t1
            INNER JOIN plims.bsns_request_process_essay_agent t2
                    ON t1.request_process_essay_id = t2.request_process_essay_id
            INNER JOIN plims.bsns_request_process_essay_activity_sample t3
                    ON t1.request_process_essay_id = t3.request_process_essay_id
            INNER JOIN plims.bsns_agent a1
                    ON a1.agent_id = t2.agent_id
            LEFT JOIN plims.bsns_reading_data r1
                  ON t3.request_process_essay_activity_sample_id =
                                r1.request_process_essay_activity_sample_id
                      AND t2.request_process_essay_agent_id =
                          r1.request_process_essay_agent_id
                      AND r1.status = 'active'
      WHERE  t3.available
            AND ( t3.request_process_essay_activity_sample_id = :sample
                  OR :sample IS NULL )
      ORDER  BY t2.num_order;  ",
      [
        ':sample' => $sample,
      ]
    )->queryAll();

    return $rd_list;
  }

  public function actionAddSampleResult(
    $request_id,
    $num_order_id,
    $essay_id,
    $composite_sample_type_id,
    $request_process_essay_id,
    $sample
  ) {
    $date =  date("Y-m-d H:i:s", time());
    $user =  Yii::$app->user->identity->id;
    try {
      Script::instance()->checkRequestProcessAssayActivity($request_process_essay_id, $user, $date);
      Script::instance()->checkRequestProcessAssayActivitySample($request_process_essay_id, 0, 0, $user, $date);
      $rpe_agent_list = Yii::$app->request->post('rpe_agent_list_id');
      foreach ($rpe_agent_list as $rpe_agent_id => $text_result) {
        $request_process_essay_agent_id =  explode('|', $rpe_agent_id)[0];
        $reading_data_id =  explode('|', $rpe_agent_id)[1];
        $number_result = $text_result == 'positive' ? 1 : ($text_result == 'negative' ? 0 : null);
        Script::instance()->webSampleResultsAddResult($user, $date, $reading_data_id, $text_result, $number_result, $request_process_essay_agent_id, $sample);
      }
    } catch (\Throwable $th) {
      echo "<pre>";
      print_r('actionAddSampleResult:' . $th->getMessage());
      echo "</pre>";
      die();
    }
    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
        'num_order_id' => $num_order_id,
        'essay_id' => $essay_id,
        'composite_sample_type_id' => $composite_sample_type_id,
        'request_process_essay_id' => $request_process_essay_id,
        'sample' => $sample,
      ]
    );
  }

  public function actionAddSampleResultBulk(
    $request_id,
    $num_order_id,
    $essay_id,
    $composite_sample_type_id,
    $request_process_essay_id
  ) {
    $date =  date("Y-m-d H:i:s", time());
    $user =  Yii::$app->user->identity->id;
    try {
      if (!empty(Yii::$app->request->post('sliderOutputInit')) && !empty(Yii::$app->request->post('sliderOutputEnd'))) {
        Script::instance()->checkRequestProcessAssayActivity($request_process_essay_id, $user, $date);
        Script::instance()->checkRequestProcessAssayActivitySample($request_process_essay_id, 0, 0, $user, $date);
        $slider_init = Yii::$app->request->post('sliderOutputInit');
        $slider_end = Yii::$app->request->post('sliderOutputEnd');
        $text_result = empty(Yii::$app->request->post('cbx_interval')) ? 'empty' : Yii::$app->request->post('cbx_interval');
        $cbx_overwrite = Yii::$app->request->post('cbx_overwrite');
        $number_result = $text_result == 'positive' ? 1 : ($text_result == 'negative' ? 0 : null);
        $is_overwrite = $cbx_overwrite == 'on' ? true : false;
        Script::instance()->webSampleResultsAddResultBulk($user, $date, $slider_init, $slider_end, $request_process_essay_id, $is_overwrite, $text_result, $number_result);
      }
    } catch (\Throwable $th) {
      echo "<pre>";
      print_r('actionAddSampleResultBulk: ' . $th->getMessage());
      echo "</pre>";
      die();
    }
    return $this->redirect(
      [
        'management',
        'request_id' => $request_id,
        'num_order_id' => $num_order_id,
        'essay_id' => $essay_id,
        'composite_sample_type_id' => $composite_sample_type_id,
        'request_process_essay_id' => $request_process_essay_id,
      ]
    );
  }

  public function actionFinishSampleResults(
    $request_id,
    $num_order_id,
    $essay_id,
    $composite_sample_type_id,
    $request_process_essay_id
  ) {

    try {
      $date = date("Y-m-d H:i:s", time());
      $user = Yii::$app->user->identity->id;

      $rpa_activity_id_get = null;
      $activity_id_get = null;
      $rdt_result = NULL;

      // finaliza si el RPA esta en proceso
      $rpa_in_process = Yii::$app->db->createCommand(
        "SELECT CASE
                WHEN t1.request_process_essay_status_id = :_rpa_in_process THEN true
                ELSE false
              END AS is_processing
        FROM   plims.bsns_request_process_essay t1
        WHERE  t1.request_process_essay_id = :_rpa_id;",
        [
          ':_rpa_in_process' => Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
          ':_rpa_id' => $request_process_essay_id,
        ]
      )->queryScalar();

      if ($rpa_in_process) {

        // establece el estado finalizado a las entidades RPA, RPA activity y RPA activity-sample 
        Script::instance()->webFinishRpaas($user, $date, $request_process_essay_id);

        // estandariza los resultados (number, text, null) en la columna resultado auxiliar p/n para todos los ensayos en cada actividad
        Script::instance()->standardizeResults($request_process_essay_id);

        // revisar si tiene actividades get
        $has_property_get = Yii::$app->db->createCommand(
          "SELECT CASE
                  WHEN Count(1) > 0 THEN true
                  ELSE false
                END AS has_property_get
          FROM   plims.bsns_activity_by_essay t1
                INNER JOIN plims.bsns_activity t2
                        ON t1.activity_id = t2.activity_id
          WHERE  t1.essay_id = :_assay_id
                AND t1.status = :_status_active
                AND t2.activity_property_id = :_ap_get;",
          [
            ':_assay_id' => $essay_id,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_ap_get' => Functional::ACTIVITY_PROPERTY_GET,
          ]
        )->queryScalar();

        if ($has_property_get) {

          // obtencion de RPA-activitity_id y activity_id, ambos de tipo GET
          $data = Yii::$app->db->createCommand(
            "SELECT t1.request_process_essay_activity_id as rpa_activity_id_get, 
                    t1.activity_id as activity_id_get
            FROM   plims.bsns_request_process_essay_activity t1
                   INNER JOIN plims.bsns_activity t2
                           ON t1.activity_id = t2.activity_id
            WHERE  t1.request_process_essay_id = :_rpa_id
                   AND t2.activity_property_id = :_ap_get;",
            [
              ':_rpa_id' => $request_process_essay_id,
              ':_ap_get' => Functional::ACTIVITY_PROPERTY_GET,
            ]
          )->queryOne();

          $rpa_activity_id_get = $data['rpa_activity_id_get'];
          $activity_id_get = $data['activity_id_get'];

          // consolida los resultados en una sola actividad [get] para quienes manejan mas de una actividad [set]
          // P.E. como GEREMINATION Y BLOTTER
          Script::instance()->consolidateResults(
            $user,
            $date,
            $request_process_essay_id,
            $essay_id,
            $rpa_activity_id_get,
            $activity_id_get
          );

          //revision de resultados positivos true/false en actividad GET
          $has_positive_results = Yii::$app->db->createCommand(
            "SELECT CASE
                      WHEN 
                        STRING_AGG(DISTINCT Cast(t1.auxiliary_result AS TEXT), '|') 
                        LIKE '%'|| :_positive_result || '%' THEN true
                      ELSE false
                    END AS has_positive_results
            FROM   plims.bsns_reading_data t1
            WHERE  t1.request_process_essay_id = :_rpa_id
                    AND t1.request_process_essay_activity_id = :_rpa_activity_id_get
                    AND t1.status = :_status_active
                    AND reading_data_type_id = :_rdt_entry;",
            [
              ':_positive_result' => Functional::POSITIVE_RESULT,
              ':_rpa_id' => $request_process_essay_id,
              ':_rpa_activity_id_get' => $rpa_activity_id_get,
              ':_status_active' => Functional::STATUS_ACTIVE,
              ':_rdt_entry' => Functional::READING_DATA_ENTRY,
            ]
          )->queryScalar();
        } else {
          // revision de resultados positivos true/false
          $has_positive_results = Yii::$app->db->createCommand(
            "SELECT CASE
                      WHEN 
                        STRING_AGG(DISTINCT Cast(t1.auxiliary_result AS TEXT), '|') 
                        LIKE '%'|| :_positive_result || '%' THEN true
                      ELSE false
                    END AS has_positive_results
            FROM   plims.bsns_reading_data t1
            WHERE  t1.request_process_essay_id = :_rpa_id
                    AND t1.status = :_status_active;",
            [
              ':_positive_result' => Functional::POSITIVE_RESULT,
              ':_rpa_id' => $request_process_essay_id,
              ':_status_active' => Functional::STATUS_ACTIVE,
            ]
          )->queryScalar();
        }

        // si cumple condición, ejecutar re-proceso
        if (
          Yii::$app->request->post('cbx_reprocess') !== null
          && $composite_sample_type_id != Functional::COMPOSITE_SAMPLE_INDIVIDUAL
          && $has_positive_results
        ) {

          $assay_list_1 = [
            Functional::ESSAY_VISUAL,
            Functional::ESSAY_WASH,
            Functional::ESSAY_WASHING,
            Functional::ESSAY_GLOEOTINIA,
            Functional::ESSAY_ANILINE,
            Functional::ESSAY_FILTRATION_CENTRIFUGATION,
            Functional::ESSAY_ENDOSPERM_HYDROLYSIS,
            Functional::ESSAY_STAINING_TEST,
            Functional::ESSAY_PCR,
            Functional::ESSAY_BLOTTER,
            Functional::ESSAY_GERMINATION,
            Functional::ESSAY_STAINING_TEST
          ];

          $assay_list_2 = [Functional::ESSAY_ELISA];

          switch (true) {
            case in_array($essay_id, $assay_list_1):
              // ANOTHER ASSAYS
              $activity_id_get = NULL;
              $rdt_result = Functional::READING_DATA_ENTRY;
              break;
            case in_array($essay_id, $assay_list_2):
              // ELISA
              $rdt_result = NULL;
              break;
          }

          $data = $this->initReprocessNormalProcess(
            $request_id,
            $num_order_id,
            $request_process_essay_id,
            $essay_id,
            $composite_sample_type_id,
            $activity_id_get,
            $rdt_result,
            $user,
            $date
          );
        }
      } else {
        echo "<pre>";
        print_r("no esta in process");
        echo "</pre>";
        die();
      }
    } catch (\Throwable $th) {
      echo "<pre>";
      print_r($th->getMessage());
      echo "</pre>";
      die();
    }
    Yii::$app->session->setFlash('success', \Yii::t('app', 'Scientific analysis evaluation process completed satisfactorily '));
    return $this->redirect(
      [
        './manage-request/management',
        'request_id' => $request_id,
      ]
    );
  }

  private function initReprocessNormalProcess(
    $request_id,
    $num_order_id,
    $request_process_essay_id,
    $essay_id,
    $composite_sample_type_id,
    $activity_id_get,
    $rdt_result,
    $user,
    $date
  ) {
    $composite_sample_type_id_new = ($composite_sample_type_id == Functional::COMPOSITE_SAMPLE_GENERAL ?  Functional::COMPOSITE_SAMPLE_SIMPLE : Functional::COMPOSITE_SAMPLE_INDIVIDUAL);
    $num_order_id_new =  Functional::NUM_ORDER_ID_REPROCESS;
    // obtain first activity ID
    $activity_id_set_first = Yii::$app->db->createCommand(
      'SELECT t1.activity_id as activity_id_set_first
        FROM   plims.bsns_activity_by_essay t1
              INNER JOIN plims.bsns_activity t2
                        ON t1.activity_id = t2.activity_id
        WHERE  t1.essay_id = :_assay_id
                AND t1.num_order = :_init_num_order
                AND t2.activity_property_id = :_ap_set
                AND t1.status = :_status_active;',
      [
        ':_assay_id' => $essay_id,
        ':_init_num_order' => Functional::NUM_ORDER_INI,
        ':_ap_set' => Functional::ACTIVITY_PROPERTY_SET,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryScalar();

    Script::instance()->addRequestProcess(
      $user,
      $date,
      $request_id,
      $num_order_id,
      $num_order_id_new
    );

    $request_process_essay_id_new = Script::instance()->addRequestProcessAssay(
      $user,
      $date,
      $request_id,
      $essay_id,
      $num_order_id_new,
      $request_process_essay_id,
      $composite_sample_type_id_new
    );

    Script::instance()->addRequestProcessAssayAgentActive(
      $user,
      $date,
      $num_order_id_new,
      $request_process_essay_id,
      $request_process_essay_id_new,
      $composite_sample_type_id_new,
      $activity_id_get
    );

    // retorno de rpa_activity_id de SET y GET si este ultimo no es null
    $data = Script::instance()->addRequestProcessAssayActivity(
      $user,
      $date,
      $num_order_id_new,
      $request_process_essay_id,
      $request_process_essay_id_new,
      $composite_sample_type_id_new,
      $activity_id_set_first,
      $activity_id_get
    );
    // retorna el primero rpa-activity-id de tipo set
    $rpa_activity_id_get_new = $data['rpa_activity_id_get_new'];
    // null si no tiene actividad get
    $rpa_activity_id_set_new = $data['rpa_activity_id_set_new'];

    $data = Script::instance()->addRequestProcessAssayActivitySample(
      $user,
      $date,
      $num_order_id_new,
      $request_process_essay_id,
      $request_process_essay_id_new,
      $composite_sample_type_id_new,
      $activity_id_set_first,
      $activity_id_get,
      $rpa_activity_id_set_new,
      $rpa_activity_id_get_new,
      $rdt_result
    );

    if ($essay_id == Functional::ESSAY_ELISA) {
      Script::instance()->addSupport($request_id, $num_order_id_new, $essay_id);
    }

    return $data;
  }

  // WEB ASSAY
  private function getDataTypeWebAssay($request_process_essay_id)
  {
    $columns = [['class' => 'yii\grid\SerialColumn']];

    $columns[] = [
      'attribute' => 'sample_order',
    ];

    $columns[] = [
      'attribute' => 'sample',
      'format' => 'raw',
    ];

    $data_agents = Yii::$app->db->createCommand(
      'SELECT t1.agent_id,
          t2.short_name
    FROM   plims.bsns_request_process_essay_agent t1
          INNER JOIN plims.bsns_agent t2
                  ON t1.agent_id = t2.agent_id
    WHERE  request_process_essay_id = :request_process_essay_id
          AND t1.status = :status_active
    ORDER  BY t1.num_order;',
      [
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    $max_agents = count($data_agents);

    if ($max_agents > 0) {
      $query = "SELECT num_order as sample_order, sample,";
      foreach ($data_agents as $key => $value) {
        $query = $query
          . "'<span class=st-'|| Max(text_result) filter (WHERE agent_id = '"
          . $value['agent_id'] . "') ||'>'|| Max(text_result) filter (WHERE agent_id = '"
          . $value['agent_id'] . "') ||'</span>' AS " . '"' . $value['short_name'] . '"';
        if ($key + 1 < $max_agents) $query = $query . ", ";
        $columns[] = [
          'attribute' => $value['short_name'],
          'format' => 'raw',
        ];
      }
      $query =
        $query . ' FROM     (
        SELECT     t2.num_order     AS num_order,
                    t2.short_name    AS sample,
                    t1.agent_id     AS agent_id,
                    t1.number_result AS number_result,
                    t1.text_result   AS text_result
        FROM       plims.bsns_reading_data t1
        INNER JOIN plims.bsns_composite_sample t2
        ON         t1.composite_sample_id = t2.composite_sample_id
        WHERE      t1.request_process_essay_id = :request_process_essay_id
        AND        t1.status = :status
        ORDER BY   
            t1.secondary_order_num) AS ntb
        GROUP BY 
            num_order,
            sample 
        ORDER BY 1;';

      $data = Yii::$app->db->createCommand(
        $query,
        [
          ':request_process_essay_id' => $request_process_essay_id,
          ':status' => Functional::STATUS_ACTIVE
        ]
      )->queryAll();
    } else {
      $data = null;
      $columns = [];
    }


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['sample_order', 'sample'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns
      ];
  }
  private function getDataTypeHistoricalResultWeb($request_process_essay_id)
  {
    $columns =
      [
        [
          'class' => 'yii\grid\SerialColumn'
        ],
        [
          'attribute' => 'sample_order',
          'format' => 'raw',
        ],
        [
          'attribute' => 'sample_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'agent_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'result',
          'format' => 'raw',
        ],
        [
          'attribute' => 'version',
          'format' => 'raw',
        ],
        [
          'attribute' => 'status',
          'format' => 'raw',
        ],
        [
          'attribute' => 'date',
          'format' => 'raw',
        ],
        [
          'attribute' => 'user',
          'format' => 'raw',
        ]
      ];

    $data = Yii::$app->db->createCommand(
      "SELECT t2.num_order                                   AS sample_order,
            t1.request_process_essay_activity_sample_id    AS sample_id,
            t2.short_name                                  AS sample_name,
            t1.agent_id                                    AS agent_id,
            t3.short_name                                  AS agent_name,
            t1.number_result                               AS number_result,
            '<span class=st-'|| t1.text_result|| '> '|| t1.text_result|| '</span>'                                   AS result,
            t1.historical_data_version                     AS version,
            '<span title='''||t1.registered_at||'''>'||t1.registered_at :: timestamp :: DATE||'</span>' AS date,
            t1.registered_by                               AS user_id,
            a1.username                                    AS user,

            CASE
              WHEN t1.status = 'active' THEN '<span class=text-primary><b>' || t1.status || '</b></span>'
              ELSE '<span class=text-secondary>' ||t1.status || '</span>'
            END                                            AS status

      FROM   plims.bsns_reading_data t1
            inner join plims.bsns_composite_sample t2
                    ON t1.composite_sample_id = t2.composite_sample_id
            inner join plims.bsns_agent t3
                    ON t1.agent_id = t3.agent_id
            inner join plims.auth_user a1
                    ON t1.registered_by = a1.user_id
      WHERE  t1.request_process_essay_id = :request_process_essay_id
      ORDER  BY t2.num_order,
              t1.secondary_order_num,
              t1.status,
              t1.historical_data_version ASC;",
      [
        ':request_process_essay_id' => $request_process_essay_id,
      ]
    )->queryAll();


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['#', 'sample_order', 'sample_name', 'agent_name', 'result', 'version', 'date',  'user', 'status'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns
      ];
  }
  // PCR
  private function getDataTypeHistoricalResultPcr($request_process_essay_id)
  {
    $columns =
      [
        [
          'class' => 'yii\grid\SerialColumn'
        ],
        [
          'attribute' => 'sample_order',
          'format' => 'raw',
        ],
        [
          'attribute' => 'sample_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'agent_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'result',
          'format' => 'raw',
        ],
        [
          'attribute' => 'version',
          'format' => 'raw',
        ],
        [
          'attribute' => 'status',
          'format' => 'raw',
        ],
        [
          'attribute' => 'date',
          'format' => 'raw',
        ],
        [
          'attribute' => 'user',
          'format' => 'raw',
        ]
      ];

    $data = Yii::$app->db->createCommand(
      "SELECT t2.num_order                                   AS sample_order,
            t1.request_process_essay_activity_sample_id    AS sample_id,
            t2.short_name                                  AS sample_name,
            t1.agent_id                                    AS agent_id,
            t3.short_name                                  AS agent_name,
            t1.number_result                               AS number_result,
            '<span class=st-'|| t1.text_result|| '> '|| t1.text_result|| '</span>'                                   AS result,
            t1.historical_data_version                     AS version,
            '<span title='''||t1.registered_at||'''>'||t1.registered_at :: timestamp :: DATE||'</span>' AS date,
            t1.registered_by                               AS user_id,
            a1.username                                    AS user,

            CASE
              WHEN t1.status = 'active' THEN '<span class=text-primary><b>' || t1.status || '</b></span>'
              ELSE '<span class=text-secondary>' ||t1.status || '</span>'
            END                                            AS status

      FROM   plims.bsns_reading_data t1
            inner join plims.bsns_composite_sample t2
                    ON t1.composite_sample_id = t2.composite_sample_id
            inner join plims.bsns_agent t3
                    ON t1.agent_id = t3.agent_id
            inner join plims.auth_user a1
                    ON t1.registered_by = a1.user_id
      WHERE  t1.request_process_essay_id = :request_process_essay_id
      ORDER  BY t2.num_order,
              t1.secondary_order_num,
              t1.status,
              t1.historical_data_version ASC;",
      [
        ':request_process_essay_id' => $request_process_essay_id,
      ]
    )->queryAll();


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['#', 'sample_order', 'sample_name', 'agent_name', 'result', 'version', 'date',  'user', 'status'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns
      ];
  }
  // GERMINATION
  private function getDataResultsGermination($request_process_essay_id)
  {

    $columns = [['class' => 'yii\grid\SerialColumn']];

    $columns[] = [
      'attribute' => 'sample_order',
    ];

    $columns[] = [
      'attribute' => 'sample_name',
    ];

    $columns[] = [
      'attribute' => 'repetition',
    ];

    $columns[] = [
      'attribute' => 'number_of_seeds_tested',
    ];

    $data_agents = Yii::$app->db->createCommand(
      'SELECT t1.agent_id,
              t2.short_name
      FROM   plims.bsns_request_process_essay_agent t1
            INNER JOIN plims.bsns_agent t2
                    ON t1.agent_id = t2.agent_id
      WHERE  request_process_essay_id = :request_process_essay_id
            AND t1.status = :status_active
      ORDER  BY t1.num_order;',
      [
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();
    $max_agents = count($data_agents);
    if ($max_agents > 0) {
      $query = 'SELECT tab.sample_order,
                  tab.activity_order,
                  tab.act_smp_id,
                  tab.activity_id,
                  tab.repetition,
                  tab.number_of_seeds_tested,
                  tab.sample_id,
                  tab.sample_name,';

      foreach ($data_agents as $key => $value) {
        $query = $query . "'<span class='|| max(tab.class_result) filter (WHERE tab.agent=" . $value['agent_id'] . ") ||'>'|| Max (tab.result) filter (WHERE tab.agent=" . $value['agent_id'] . ") ||'</span>' AS " . '"' . $value['short_name'] . '"';

        if ($key + 1 < $max_agents) $query = $query . ", ";

        $columns[] = [
          'attribute' => $value['short_name'],
          'format' => 'raw',
        ];
      }
      $query = $query . "FROM
        (SELECT     r1.primary_order_num                                      AS sample_order,
                    r1.tertiary_order_num                                     AS activity_order,
                    r1.request_process_essay_activity_sample_id               AS act_smp_id,
                    t3.activity_id                                            AS activity_id,
                    t3.short_name                                             AS repetition,
                    r1.number_of_seeds_tested                                 AS number_of_seeds_tested,
                    t2.composite_sample_id                                    AS sample_id,
                    t2.short_name                                             AS sample_name,
                    r1.agent_id                                               AS agent,
                    CASE WHEN r1.number_result > 0 THEN 'st-positive' ELSE 'st-empty' END AS class_result,
                    COALESCE(Cast(r1.number_result AS TEXT), r1.text_result) AS result

          FROM       plims.bsns_reading_data r1
            INNER JOIN plims.bsns_agent t1
              ON         r1.agent_id = t1.agent_id
            INNER JOIN plims.bsns_request_process_essay_activity_sample t4
              ON         r1.request_process_essay_activity_sample_id = t4.request_process_essay_activity_sample_id
            INNER JOIN plims.bsns_composite_sample t2
              ON         r1.composite_sample_id = t2.composite_sample_id
            INNER JOIN plims.bsns_activity t3
              ON         r1.activity_id = t3.activity_id
            WHERE      r1.request_process_essay_id = :request_process_essay_id
              AND        r1.status = :status_active
              AND        t4.available) AS tab
      GROUP  BY tab.sample_order, tab.activity_order, tab.act_smp_id, tab.activity_id, tab.repetition, tab.number_of_seeds_tested, tab.sample_id, tab.sample_name 
      ORDER  BY 1,2; ";
      $data = Yii::$app->db->createCommand(
        $query,
        [
          ':request_process_essay_id' => $request_process_essay_id,
          ':status_active' => Functional::STATUS_ACTIVE
        ]
      )->queryAll();
    } else {
      $data = null;
      $columns = [];
    }

    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['sample_order', 'sample_name', 'repetition', 'number_of_seeds_tested'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns,
      ];
  }
  private function getDataAveragesGermination($request_process_essay_id)
  {

    $columns = [['class' => 'yii\grid\SerialColumn']];

    $columns[] = [
      'attribute' => 'sample_order',
    ];

    $columns[] = [
      'attribute' => 'sample_name',
    ];

    $data_agents = Yii::$app->db->createCommand(
      'SELECT t1.agent_id,
              t2.short_name
      FROM   plims.bsns_request_process_essay_agent t1
            INNER JOIN plims.bsns_agent t2
                    ON t1.agent_id = t2.agent_id
      WHERE  request_process_essay_id = :request_process_essay_id
            AND t1.status = :status_active
      ORDER  BY t1.num_order;',
      [
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    $max_agents = count($data_agents);
    if ($max_agents > 0) {
      $query =
        'SELECT 
        r1.primary_order_num                        AS sample_order,
        t2.composite_sample_id                      AS sample_id,
        t2.short_name                               AS sample_name,';
      foreach ($data_agents as $key => $value) {

        $query = $query
          . "'<span class=st-info>'|| "
          . "round((   sum(r1.number_result) filter (WHERE r1.agent_id = " . $value['agent_id'] . ")*100   /   sum(r1.number_of_seeds_tested) filter (WHERE r1.agent_id = " . $value['agent_id'] . ")   )::numeric,2) "
          . "||'%</span>' AS " . '"'
          . $value['short_name'] . '"';


        if ($key + 1 < $max_agents) $query = $query . ", ";
        $columns[] = [
          'attribute' => $value['short_name'],
          'format' => 'raw',
        ];
      }

      $query =
        $query .
        'FROM  plims.bsns_reading_data r1
        INNER JOIN plims.bsns_agent t1
        ON         r1.agent_id = t1.agent_id  AND r1.status = :status_active AND r1.request_process_essay_id = :request_process_essay_id
        INNER JOIN plims.bsns_request_process_essay_activity_sample t4
        ON         r1.request_process_essay_activity_sample_id = t4.request_process_essay_activity_sample_id AND t4.available
        INNER JOIN plims.bsns_composite_sample t2
        ON         r1.composite_sample_id = t2.composite_sample_id
        INNER JOIN plims.bsns_activity t3
        ON         r1.activity_id = t3.activity_id
  
        GROUP BY
              r1.primary_order_num,
              t2.composite_sample_id,
              t2.short_name
        ORDER BY   
              r1.primary_order_num;';


      $data = Yii::$app->db->createCommand(
        $query,
        [
          ':request_process_essay_id' => $request_process_essay_id,
          ':status_active' => Functional::STATUS_ACTIVE
        ]
      )->queryAll();
    } else {
      $data = null;
      $columns = [];
    }

    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['sample_order', 'sample_name'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns,
      ];
  }
  private function getDataHistoricalResultsGermination($request_process_essay_id)
  {
    $columns =
      [
        [
          'class' => 'yii\grid\SerialColumn'
        ],
        [
          'attribute' => 'sample_order',
          'format' => 'raw',
        ],
        [
          'attribute' => 'sample_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'activity_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'agent_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'result',
          'format' => 'raw',
        ],
        [
          'attribute' => 'version',
          'format' => 'raw',
        ],
        [
          'attribute' => 'status',
          'format' => 'raw',
        ],
        [
          'attribute' => 'date',
          'format' => 'raw',
        ],
        [
          'attribute' => 'user',
          'format' => 'raw',
        ]
      ];

    $data = Yii::$app->db->createCommand(
      "SELECT t2.num_order                                   AS sample_order,
            t1.request_process_essay_activity_sample_id    AS sample_id,
            t2.short_name                                  AS sample_name,
            t1.agent_id                                    AS agent_id,
            t3.short_name                                  AS agent_name,
            t4.long_name                                   AS activity_name,
            t1.number_result                               AS number_result,
            CASE
              WHEN t1.number_result > 0 THEN '<span class=st-positive>'
                                            || Cast(t1.number_result AS TEXT)
                                            || '</span>'
              ELSE '<span class=st-empty>'
                  || Cast(t1.number_result AS TEXT)
                  || '</span>'
            END                                            AS result,
            t1.historical_data_version                     AS version,
            '<span title='''
            ||t1.registered_at
            ||'''>'
            ||t1.registered_at :: timestamp :: DATE
                                              ||'</span>' AS date,
            t1.registered_by                               AS user_id,
            a1.username                                    AS USER,
            CASE
              WHEN t1.status = 'active' THEN '<span class=text-primary><b>' || t1.status || '</b></span>'
              ELSE '<span class=text-secondary>' ||t1.status || '</span>'
            END                                            AS status
      FROM   plims.bsns_reading_data t1
            inner join plims.bsns_composite_sample t2
                    ON t1.composite_sample_id = t2.composite_sample_id
            inner join plims.bsns_agent t3
                    ON t1.agent_id = t3.agent_id
            inner join plims.auth_user a1
                    ON t1.registered_by = a1.user_id
            inner join plims.bsns_activity t4
                    ON t1.activity_id = t4.activity_id
      WHERE  t1.request_process_essay_id = :request_process_essay_id
      ORDER  BY t2.num_order,
              t1.secondary_order_num,
              t1.tertiary_order_num,
              t1.status,
              t1.historical_data_version ASC;",
      [
        ':request_process_essay_id' => $request_process_essay_id,
      ]
    )->queryAll();


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['#', 'sample_order', 'sample_name', 'activity_name', 'agent_name', 'result', 'version', 'date',  'user', 'status'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns
      ];
  }
  private function getDataSowingGermination($request_process_essay_id)
  {
    $columns =
      [
        [
          'class' => 'yii\grid\SerialColumn'
        ],
        [
          'attribute' => 'sample_order',
          'format' => 'raw',
        ],
        [
          'attribute' => 'sample_name',
          'format' => 'raw',
        ],

        [
          'attribute' => 'number_of_seeds',
          'format' => 'raw',
        ],

        [
          'attribute' => 'version',
          'format' => 'raw',
        ],
        [
          'attribute' => 'activity_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'date',
          'format' => 'raw',
        ],
        [
          'attribute' => 'user',
          'format' => 'raw',
        ]
      ];

    $data = Yii::$app->db->createCommand(
      "SELECT t2.num_order     AS sample_order,
              t3.short_name    AS sample_name,
              t2.number_of_seeds,
              t2.historical_data_version AS version,
              a1.short_name    AS activity_name,
              t2.registered_at :: timestamp :: DATE AS date,
              u1.username      AS user
       FROM   plims.bsns_request_process_essay_activity t1
              INNER JOIN plims.bsns_activity a1
                      ON t1.request_process_essay_id = :request_process_essay_id
                         AND t1.activity_id = a1.activity_id
              INNER JOIN plims.bsns_request_process_essay_activity_sample t2
                      ON t1.request_process_essay_activity_id =
                         t2.request_process_essay_activity_id
                         AND t2.available
                         AND t2.historical_data_version > 0
                         AND t1.status = 'active'
              INNER JOIN plims.auth_user u1
                      ON t2.registered_by = u1.user_id
              INNER JOIN plims.bsns_composite_sample t3
                      ON t2.composite_sample_id = t3.composite_sample_id
       ORDER  BY t2.num_order,
                 t1.num_order,
                t2.historical_data_version ; 
       ",
      [
        ':request_process_essay_id' => $request_process_essay_id,
      ]
    )->queryAll();


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['#', 'sample_order', 'sample_name', 'number_of_seeds', 'activity_name', 'version', 'date',  'user'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns
      ];
  }
  private function getDataHistoricalSowingGermination($request_process_essay_id)
  {
    $columns =
      [
        [
          'class' => 'yii\grid\SerialColumn'
        ],
        [
          'attribute' => 'sample_order',
          'format' => 'raw',
        ],
        [
          'attribute' => 'sample_name',
          'format' => 'raw',
        ],

        [
          'attribute' => 'number_of_seeds',
          'format' => 'raw',
        ],
        [
          'attribute' => 'version',
          'format' => 'raw',
        ],
        [
          'attribute' => 'status',
          'format' => 'raw',
        ],
        [
          'attribute' => 'activity_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'date',
          'format' => 'raw',
        ],
        [
          'attribute' => 'user',
          'format' => 'raw',
        ]
      ];

    $data = Yii::$app->db->createCommand(
      "SELECT t2.num_order                                                         as sample_order,
            t3.short_name                                                        as sample_name,
            t2.number_of_seeds,
            t2.historical_data_version                                           as version,
            a1.short_name                                                        as activity_name,
            '<span title='''||t2.registered_at :: timestamp||'''>'||t2.registered_at :: timestamp :: DATE||'</span>' as date,
            u1.username                                                          as user,
            a1.activity_type_id,
            a1.activity_property_id,
            a1.activity_group_id,
            case when t2.available then '<span class=text-primary><b>active</b></span>' else '<span class=text-secondary>disabled</span>' end as status
      FROM   plims.bsns_request_process_essay_activity t1
            inner join plims.bsns_activity a1
                    ON t1.request_process_essay_id = :_request_process_essay_id
                      AND t1.activity_id = a1.activity_id
                      AND a1.activity_property_id = :_activity_property_set
            inner join plims.bsns_request_process_essay_activity_sample t2
                    ON t1.request_process_essay_activity_id =
                      t2.request_process_essay_activity_id
                      AND t1.status = :_status_active
            inner join plims.auth_user u1
                    ON t2.registered_by = u1.user_id
            inner join plims.bsns_composite_sample t3
                    ON t2.composite_sample_id = t3.composite_sample_id
      ORDER  BY t2.num_order,
              t1.num_order,
              t2.historical_data_version desc;",
      [
        ':_request_process_essay_id' => $request_process_essay_id,
        ':_activity_property_set' => Functional::ACTIVITY_PROPERTY_SET,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['#', 'sample_order', 'sample_name', 'number_of_seeds', 'activity_name', 'version', 'status', 'date',  'user'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns
      ];
  }
  // BLOTTER
  private function getDataResultsBlotter($request_process_essay_id)
  {

    $columns = [['class' => 'yii\grid\SerialColumn']];

    $columns[] = [
      'attribute' => 'sample_order',
    ];

    $columns[] = [
      'attribute' => 'sample_name',
    ];

    $columns[] = [
      'attribute' => 'repetition',
    ];

    $columns[] = [
      'attribute' => 'number_of_seeds_tested',
    ];

    $data_agents = Yii::$app->db->createCommand(
      'SELECT t1.agent_id,
              t2.short_name
      FROM   plims.bsns_request_process_essay_agent t1
            INNER JOIN plims.bsns_agent t2
                    ON t1.agent_id = t2.agent_id
      WHERE  request_process_essay_id = :request_process_essay_id
            AND t1.status = :status_active
      ORDER  BY t1.num_order;',
      [
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();
    $max_agents = count($data_agents);
    if ($max_agents > 0) {
      $query = 'SELECT tab.sample_order,
                  tab.activity_order,
                  tab.act_smp_id,
                  tab.activity_id,
                  tab.repetition,
                  tab.number_of_seeds_tested,
                  tab.sample_id,
                  tab.sample_name,';

      foreach ($data_agents as $key => $value) {
        $query = $query . "'<span class='|| max(tab.class_result) filter (WHERE tab.agent=" . $value['agent_id'] . ") ||'>'|| Max (tab.result) filter (WHERE tab.agent=" . $value['agent_id'] . ") ||'</span>' AS " . '"' . $value['short_name'] . '"';

        if ($key + 1 < $max_agents) $query = $query . ", ";

        $columns[] = [
          'attribute' => $value['short_name'],
          'format' => 'raw',
        ];
      }
      $query = $query . "FROM
        (SELECT     r1.primary_order_num                                      AS sample_order,
                    r1.tertiary_order_num                                     AS activity_order,
                    r1.request_process_essay_activity_sample_id               AS act_smp_id,
                    t3.activity_id                                            AS activity_id,
                    t3.short_name                                             AS repetition,
                    r1.number_of_seeds_tested                                 AS number_of_seeds_tested,
                    t2.composite_sample_id                                    AS sample_id,
                    t2.short_name                                             AS sample_name,
                    r1.agent_id                                               AS agent,
                    CASE WHEN r1.number_result > 0 THEN 'st-positive' ELSE 'st-empty' END AS class_result,
                    COALESCE(Cast(r1.number_result AS TEXT), r1.text_result) AS result

          FROM       plims.bsns_reading_data r1
            INNER JOIN plims.bsns_agent t1
              ON         r1.agent_id = t1.agent_id
            INNER JOIN plims.bsns_request_process_essay_activity_sample t4
              ON         r1.request_process_essay_activity_sample_id = t4.request_process_essay_activity_sample_id
            INNER JOIN plims.bsns_composite_sample t2
              ON         r1.composite_sample_id = t2.composite_sample_id
            INNER JOIN plims.bsns_activity t3
              ON         r1.activity_id = t3.activity_id
            WHERE      r1.request_process_essay_id = :request_process_essay_id
              AND        r1.status = :status_active
              AND        t4.available) AS tab
      GROUP  BY tab.sample_order, tab.activity_order, tab.act_smp_id, tab.activity_id, tab.repetition, tab.number_of_seeds_tested, tab.sample_id, tab.sample_name 
      ORDER  BY 1,2; ";
      $data = Yii::$app->db->createCommand(
        $query,
        [
          ':request_process_essay_id' => $request_process_essay_id,
          ':status_active' => Functional::STATUS_ACTIVE
        ]
      )->queryAll();
    } else {
      $data = null;
      $columns = [];
    }

    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['sample_order', 'sample_name', 'repetition', 'number_of_seeds_tested'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns,
      ];
  }
  private function getDataAveragesBlotter($request_process_essay_id)
  {

    $columns = [['class' => 'yii\grid\SerialColumn']];

    $columns[] = [
      'attribute' => 'sample_order',
    ];

    $columns[] = [
      'attribute' => 'sample_name',
    ];

    $data_agents = Yii::$app->db->createCommand(
      'SELECT t1.agent_id,
              t2.short_name
      FROM   plims.bsns_request_process_essay_agent t1
            INNER JOIN plims.bsns_agent t2
                    ON t1.agent_id = t2.agent_id
      WHERE  request_process_essay_id = :request_process_essay_id
            AND t1.status = :status_active
      ORDER  BY t1.num_order;',
      [
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    $max_agents = count($data_agents);
    if ($max_agents > 0) {
      $query =
        'SELECT 
        r1.primary_order_num                        AS sample_order,
        t2.composite_sample_id                      AS sample_id,
        t2.short_name                               AS sample_name,';
      foreach ($data_agents as $key => $value) {

        $query = $query
          . "'<span class=st-info>'|| "
          . "round((   sum(r1.number_result) filter (WHERE r1.agent_id = " . $value['agent_id'] . ")*100   /   sum(r1.number_of_seeds_tested) filter (WHERE r1.agent_id = " . $value['agent_id'] . ")   )::numeric,2) "
          . "||'%</span>' AS " . '"'
          . $value['short_name'] . '"';


        if ($key + 1 < $max_agents) $query = $query . ", ";
        $columns[] = [
          'attribute' => $value['short_name'],
          'format' => 'raw',
        ];
      }

      $query =
        $query .
        'FROM   plims.bsns_reading_data r1
                INNER JOIN plims.bsns_agent t1
                        ON r1.agent_id = t1.agent_id
                          AND r1.status = :status_active
                          AND r1.request_process_essay_id = :request_process_essay_id
                INNER JOIN plims.bsns_request_process_essay_activity_sample t4
                        ON r1.request_process_essay_activity_sample_id =
                                      t4.request_process_essay_activity_sample_id
                          AND t4.available
                INNER JOIN plims.bsns_composite_sample t2
                        ON r1.composite_sample_id = t2.composite_sample_id
                INNER JOIN plims.bsns_activity t3
                        ON r1.activity_id = t3.activity_id
        GROUP  BY r1.primary_order_num,
                  t2.composite_sample_id,
                  t2.short_name
        ORDER  BY r1.primary_order_num;';


      $data = Yii::$app->db->createCommand(
        $query,
        [
          ':request_process_essay_id' => $request_process_essay_id,
          ':status_active' => Functional::STATUS_ACTIVE
        ]
      )->queryAll();
    } else {
      $data = null;
      $columns = [];
    }

    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['sample_order', 'sample_name'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns,
      ];
  }
  private function getDataHistoricalResultsBlotter($request_process_essay_id)
  {
    $columns =
      [
        [
          'class' => 'yii\grid\SerialColumn'
        ],
        [
          'attribute' => 'sample_order',
          'format' => 'raw',
        ],
        [
          'attribute' => 'sample_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'activity_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'agent_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'result',
          'format' => 'raw',
        ],
        [
          'attribute' => 'version',
          'format' => 'raw',
        ],
        [
          'attribute' => 'status',
          'format' => 'raw',
        ],
        [
          'attribute' => 'date',
          'format' => 'raw',
        ],
        [
          'attribute' => 'user',
          'format' => 'raw',
        ]
      ];

    $data = Yii::$app->db->createCommand(
      "SELECT t2.num_order                                   AS sample_order,
            t1.request_process_essay_activity_sample_id    AS sample_id,
            t2.short_name                                  AS sample_name,
            t1.agent_id                                    AS agent_id,
            t3.short_name                                  AS agent_name,
            t4.long_name                                   AS activity_name,
            t1.number_result                               AS number_result,
            CASE
              WHEN t1.number_result > 0 THEN '<span class=st-positive>'
                                            || Cast(t1.number_result AS TEXT)
                                            || '</span>'
              ELSE '<span class=st-empty>'
                  || Cast(t1.number_result AS TEXT)
                  || '</span>'
            END                                            AS result,
            t1.historical_data_version                     AS version,
            '<span title='''
            ||t1.registered_at
            ||'''>'
            ||t1.registered_at :: timestamp :: DATE
                                              ||'</span>' AS date,
            t1.registered_by                               AS user_id,
            a1.username                                    AS USER,
            
            CASE
              WHEN t1.status = 'active' THEN '<span class=text-primary><b>' || t1.status || '</b></span>'
              ELSE '<span class=text-secondary>' ||t1.status || '</span>'
            END                                            AS status

      FROM   plims.bsns_reading_data t1
            inner join plims.bsns_composite_sample t2
                    ON t1.composite_sample_id = t2.composite_sample_id
            inner join plims.bsns_agent t3
                    ON t1.agent_id = t3.agent_id
            inner join plims.auth_user a1
                    ON t1.registered_by = a1.user_id
            inner join plims.bsns_activity t4
                    ON t1.activity_id = t4.activity_id
      WHERE  t1.request_process_essay_id = :request_process_essay_id
      ORDER  BY t2.num_order,
              t1.secondary_order_num,
              t1.tertiary_order_num,
              t1.status,
              t1.historical_data_version ASC;",
      [
        ':request_process_essay_id' => $request_process_essay_id,
      ]
    )->queryAll();


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['#', 'sample_order', 'sample_name', 'activity_name', 'agent_name', 'result', 'version', 'date',  'user', 'status'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns
      ];
  }
  private function getDataSowingBlotter($request_process_essay_id)
  {
    $columns =
      [
        [
          'class' => 'yii\grid\SerialColumn'
        ],
        [
          'attribute' => 'sample_order',
          'format' => 'raw',
        ],
        [
          'attribute' => 'sample_name',
          'format' => 'raw',
        ],

        [
          'attribute' => 'number_of_seeds',
          'format' => 'raw',
        ],

        [
          'attribute' => 'version',
          'format' => 'raw',
        ],
        [
          'attribute' => 'activity_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'date',
          'format' => 'raw',
        ],
        [
          'attribute' => 'user',
          'format' => 'raw',
        ]
      ];

    $data = Yii::$app->db->createCommand(
      "SELECT t2.num_order     AS sample_order,
              t3.short_name    AS sample_name,
              t2.number_of_seeds,
              t2.historical_data_version AS version,
              a1.short_name    AS activity_name,
              t2.registered_at :: timestamp :: DATE AS date,
              u1.username      AS user
       FROM   plims.bsns_request_process_essay_activity t1
              INNER JOIN plims.bsns_activity a1
                      ON t1.request_process_essay_id = :request_process_essay_id
                         AND t1.activity_id = a1.activity_id
              INNER JOIN plims.bsns_request_process_essay_activity_sample t2
                      ON t1.request_process_essay_activity_id =
                         t2.request_process_essay_activity_id
                         AND t2.available
                         AND t2.historical_data_version > 0
                         AND t1.status = 'active'
              INNER JOIN plims.auth_user u1
                      ON t2.registered_by = u1.user_id
              INNER JOIN plims.bsns_composite_sample t3
                      ON t2.composite_sample_id = t3.composite_sample_id
       ORDER  BY t2.num_order,
                 t1.num_order,
                t2.historical_data_version ; 
       ",
      [
        ':request_process_essay_id' => $request_process_essay_id,
      ]
    )->queryAll();


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['#', 'sample_order', 'sample_name', 'number_of_seeds', 'activity_name', 'version', 'date',  'user'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns
      ];
  }
  private function getDataHistoricalSowingBlotter($request_process_essay_id)
  {
    $columns =
      [
        [
          'class' => 'yii\grid\SerialColumn'
        ],
        [
          'attribute' => 'sample_order',
          'format' => 'raw',
        ],
        [
          'attribute' => 'sample_name',
          'format' => 'raw',
        ],

        [
          'attribute' => 'number_of_seeds',
          'format' => 'raw',
        ],
        [
          'attribute' => 'version',
          'format' => 'raw',
        ],
        [
          'attribute' => 'status',
          'format' => 'raw',
        ],
        [
          'attribute' => 'activity_name',
          'format' => 'raw',
        ],
        [
          'attribute' => 'date',
          'format' => 'raw',
        ],
        [
          'attribute' => 'user',
          'format' => 'raw',
        ]
      ];

    $data = Yii::$app->db->createCommand(
      "SELECT t2.num_order                                                         as sample_order,
            t3.short_name                                                        as sample_name,
            t2.number_of_seeds,
            t2.historical_data_version                                           as version,
            a1.short_name                                                        as activity_name,
            '<span title='''||t2.registered_at :: timestamp||'''>'||t2.registered_at :: timestamp :: DATE||'</span>' as date,
            u1.username                                                          as user,
            a1.activity_type_id,
            a1.activity_property_id,
            a1.activity_group_id,
            case 
              when t2.available then '<span class=text-primary><b>active</b></span>' 
              else '<span class=text-secondary>disabled</span>' end as status
      FROM   plims.bsns_request_process_essay_activity t1
            inner join plims.bsns_activity a1
                    ON t1.request_process_essay_id = :_request_process_essay_id
                      AND t1.activity_id = a1.activity_id
                      AND a1.activity_property_id = :_activity_property_set
            inner join plims.bsns_request_process_essay_activity_sample t2
                    ON t1.request_process_essay_activity_id =
                      t2.request_process_essay_activity_id
                      AND t1.status = :_status_active
            inner join plims.auth_user u1
                    ON t2.registered_by = u1.user_id
            inner join plims.bsns_composite_sample t3
                    ON t2.composite_sample_id = t3.composite_sample_id
      ORDER  BY t2.num_order,
              t1.num_order,
              t2.historical_data_version desc;",
      [
        ':_request_process_essay_id' => $request_process_essay_id,
        ':_activity_property_set' => Functional::ACTIVITY_PROPERTY_SET,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();


    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['#', 'sample_order', 'sample_name', 'number_of_seeds', 'activity_name', 'version', 'status', 'date',  'user'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns
      ];
  }
  // ELISA
  private function getDataTypeByActivityElisa($request_process_essay_id, $activity_id)
  {
    $columns = [['class' => 'yii\grid\SerialColumn']];
    $columns[] = [
      'attribute' => 'sample_order',
    ];
    $columns[] = [
      'attribute' => 'sample_name',
    ];
    //====================================
    $data_agents = Yii::$app->db->createCommand(
      'SELECT t1.agent_id,
          t2.short_name
      FROM   plims.bsns_request_process_essay_agent t1
            INNER JOIN plims.bsns_agent t2
                    ON t1.agent_id = t2.agent_id
      WHERE  request_process_essay_id = :request_process_essay_id
            AND t1.status = :status_active
      ORDER  BY t1.num_order;',
      [
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();
    $max_agents = count($data_agents);
    if ($max_agents > 0) {
      $query =
        'SELECT 
            r1.primary_order_num                        AS sample_order,
            r1.tertiary_order_num                       AS activity_order,
            r1.request_process_essay_activity_sample_id AS act_smp_id,
            t3.activity_id                              AS activity_id,
            t3.short_name                               AS repetition,
            r1.number_of_seeds_tested                   AS number_of_seeds_tested,
            t2.composite_sample_id                      AS sample_id,
            t2.short_name                               AS sample_name, ' . "\n";

      foreach ($data_agents as $key => $value) {
        $query =
          $query . "'<span class=''label label-'|| Max(r1.text_result) || '''>' || Max(r1.text_result) filter (WHERE r1.agent_id = " .
          $value['agent_id'] . ") ||'</span>' AS " . '"' . $value['short_name'] . '"';

        if ($key + 1 < $max_agents) $query = $query . ", " . "\n";
        $columns[] = [
          'attribute' => $value['short_name'],
          'format' => 'raw',
        ];
      }

      $query =
        $query . "\n" .
        'FROM  plims.bsns_reading_data r1
            INNER JOIN plims.bsns_agent t1
            ON         r1.agent_id = t1.agent_id and r1.activity_id = :activity_id
            INNER JOIN plims.bsns_request_process_essay_activity_sample t4
            ON         r1.request_process_essay_activity_sample_id = t4.request_process_essay_activity_sample_id
            INNER JOIN plims.bsns_composite_sample t2
            ON         r1.composite_sample_id = t2.composite_sample_id
            INNER JOIN plims.bsns_activity t3
            ON         r1.activity_id = t3.activity_id
            WHERE      r1.request_process_essay_id = :request_process_essay_id
            AND        r1.status = :status_active
            AND        t4.available
            GROUP BY
                  r1.primary_order_num,
                  r1.tertiary_order_num,
                  r1.request_process_essay_activity_sample_id ,
                  t3.activity_id ,
                  t3.short_name ,
                  r1.number_of_seeds_tested ,
                  t2.composite_sample_id ,
                  t2.short_name
            ORDER BY   
                  sample_name DESC;';
      $data = Yii::$app->db->createCommand(
        $query,
        [
          ':request_process_essay_id' => $request_process_essay_id,
          ':status_active' => Functional::STATUS_ACTIVE,
          ':activity_id' => $activity_id
        ]
      )->queryAll();
    } else {
      $data = null;
      $columns = [];
    }
    //====================================
    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['sample_order', 'sample_name', 'repetition', 'number_of_seeds_tested'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);
    return
      [
        'provider' => $provider,
        'columns' => $columns,
      ];
  }
  private function getDataTypeByConsolidationElisa($request_process_essay_id, $activity_id)
  {
    $columns = [['class' => 'yii\grid\SerialColumn']];
    $columns[] = [
      'attribute' => 'sample_order',
    ];
    $columns[] = [
      'attribute' => 'sample_name',
    ];
    //====================================
    $data_agents = Yii::$app->db->createCommand(
      'SELECT t1.agent_id,
          t2.short_name
      FROM   plims.bsns_request_process_essay_agent t1
            INNER JOIN plims.bsns_agent t2
                    ON t1.agent_id = t2.agent_id
      WHERE  request_process_essay_id = :request_process_essay_id
            AND t1.status = :status_active
      ORDER  BY t1.num_order;',
      [
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();
    $max_agents = count($data_agents);

    if ($max_agents > 0) {
      $query =
        'SELECT 
          tab.sample_order,                        	
          tab.sample_id,                     
          tab.sample_name,' . "\n";

      foreach ($data_agents as $key => $value) {
        $query = $query .
          "'<span class=''label label-'|| Max(tab.auxiliary_result) filter (WHERE tab.agent_id = "
          . $value['agent_id'] . ") || '''>' || Max(tab.auxiliary_result) filter (WHERE tab.agent_id = "
          . $value['agent_id'] . ") ||'</span>' AS " . '"' . $value['short_name'] . '",' . "\n"
          . "'<span>'" . '||Max( tab.observation ) filter (WHERE tab.agent_id = ' . $value['agent_id'] . ')|| ' . "'</span>'" . '  as observation_' . $value['short_name'];

        if ($key + 1 < $max_agents) $query = $query . ", " . "\n";
        $columns[] = [
          'attribute' => $value['short_name'],
          'format' => 'raw',
        ];
        $columns[] = [
          'attribute' => 'observation_' .  strtolower($value['short_name']),
          'format' => 'raw',
        ];
      }

      $query =
        $query . "\n" .
        'FROM                  
            (select distinct on (
            r1.request_process_essay_id,
            r1.primary_order_num,
            r1.composite_sample_id,
            r1.agent_id ,
            r1.reading_data_type_id 
                      ) 
                        r1.primary_order_num                        AS sample_order,
                        r1.composite_sample_id                      AS sample_id,
                        t1.short_name                               AS sample_name,
                        r1.auxiliary_result													AS auxiliary_result,
                        r1.observation															AS observation,
                        r1.agent_id																	AS agent_id
                        
            from plims.bsns_reading_data r1 inner join plims.bsns_composite_sample t1 on r1.composite_sample_id = t1.composite_sample_id 
            where r1.request_process_essay_id = :request_process_essay_id
            and r1.status = :status_active
            and r1.activity_id = :activity_id
            and r1.grafting_number_id = 0
            ORDER BY  r1.request_process_essay_id,
                      r1.primary_order_num,
                      r1.composite_sample_id,
                      r1.agent_id,
                      r1.reading_data_type_id ,
                      r1.historical_data_version desc
            ) as tab        
          group by  tab.sample_order, tab.sample_id, tab.sample_name       
          order by   tab.sample_order;';
      $data = Yii::$app->db->createCommand(
        $query,
        [
          ':request_process_essay_id' => $request_process_essay_id,
          ':status_active' => Functional::STATUS_ACTIVE,
          ':activity_id' => $activity_id
        ]
      )->queryAll();
    } else {
      $data = null;
      $columns = [];
    }
    //====================================
    $provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => ['sample_order', 'sample_name', 'repetition', 'number_of_seeds_tested'],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return
      [
        'provider' => $provider,
        'columns' => $columns,
      ];
  }
}

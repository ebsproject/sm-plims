<?php

namespace frontend\modules\functional\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use app\models\UploadForm;
use yii\web\UploadedFile;
use frontend\modules\functional\Functional;
use frontend\models\Script;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class ManageMultiRequestController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger', \Yii::t('app', 'You do not have sufficient permissions to access this website'));
          return $this->goHome();
        },
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  // -----------[index]-----------//
  public function actionIndex()
  {
    $_user_institution  = Yii::$app->user->Identity->institution;
    $_user_laboratory  = Yii::$app->user->Identity->laboratory;

    $_query = "SELECT t1.support_master_id                               AS event_id,
                  'MR'
                  || RIGHT('0000'
                          || Cast(t1.support_master_id AS TEXT), 5) AS event_code,
                  String_agg(DISTINCT CASE
                                        WHEN t3.num_order_id > 1 THEN t2.request_code
                                                                      || ' (R)'
                                        ELSE t2.request_code
                                      END, ', ')                     AS request_codes,
                  String_agg(DISTINCT c1.short_name, ', ')           AS crop_names,
                  String_agg(DISTINCT p1.short_name, ', ')           AS distribution_names
              FROM   plims.bsns_multi_request_event t1
                  INNER JOIN plims.bsns_request t2
                          ON t1.request_id = t2.request_id
                            AND t2.status = :_status_active ";

    $_params = [
      ':_status_active' => Functional::STATUS_ACTIVE
    ];


    if ($_user_institution) {
      $_query = $_query . "AND t2.institution_id = :_institution_id ";
      $_params[':_institution_id'] = $_user_institution;
    }

    if ($_user_laboratory) {
      $_query = $_query . "AND t2.laboratory_id = :_laboratory_id ";
      $_params[':_laboratory_id'] = $_user_laboratory;
    }

    $_query = $_query . "INNER JOIN plims.bsns_request_process t3
                            ON t2.request_id = t3.request_id
                              AND t3.num_order_id = t1.num_order_id
                        LEFT JOIN plims.bsns_parameter p1
                          ON t2.request_type_id = p1.parameter_id
                        LEFT JOIN plims.bsns_crop c1
                          ON t3.crop_id = c1.crop_id
                        GROUP  BY t1.support_master_id
                        ORDER  BY t1.support_master_id DESC; ";

    $data = Yii::$app->db->createCommand($_query, $_params)->queryAll();


    $data_provider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => [
          'event_id',
          'event_code',
          'request_codes',
          'crop_names',
          'distribution_names'
        ],
      ],
      'key' => 'event_id',
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return $this->render('index', [
      'dataProvider' => $data_provider
    ]);
  }

  // -----------[management]-----------//
  public function actionManagement($event_id = null, $agent_id = null)
  {

    $event_code = 'MR' . substr('00000' . $event_id, -5,);
    $agent_list = Yii::$app->db->createCommand(
      "SELECT DISTINCT t1.agent_id,
              short_name AS agent_name
      FROM   plims.bsns_multi_request_event t1
      INNER JOIN plims.bsns_agent t2
            ON t1.agent_id = t2.agent_id
      WHERE  support_master_id = :_event_id
      AND t1.agent_id IS NOT NULL;",
      [
        ':_event_id' => $event_id,
      ]
    )->queryAll();
    $agent_list = ArrayHelper::map($agent_list, 'agent_id', 'agent_name');

    $activity_set_first =   Yii::$app->db->createCommand(
      "SELECT t1.activity_id AS activity_set_first
      FROM   plims.bsns_activity t1
             INNER JOIN plims.bsns_activity_by_essay t2
                     ON t1.activity_id = t2.activity_id
                        AND t2.status = :_status_active
                        AND t2.essay_id = :_assay_elisa
                        AND t1.activity_property_id = :_property_set
      ORDER  BY t2.num_order
      LIMIT  1;",
      [
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_assay_elisa' => Functional::ESSAY_ELISA,
        ':_property_set' => Functional::ACTIVITY_PROPERTY_SET,
      ]
    )->queryScalar();

    // ------------------------------------------------------
    $reading_data_list = [];
    $activity_support_cell_position_reading_list = [];
    $activity_support_list = [];

    if (is_null($agent_id)) {
      if (null !== Yii::$app->request->post('agent_name')) {
        $agent_id = Yii::$app->request->post('agent_name');
      }
    }

    if (null !==  $agent_id) {
      $activity_get =   Yii::$app->db->createCommand(
        "SELECT t1.activity_id
        FROM   plims.bsns_activity t1
               INNER JOIN plims.bsns_activity_by_essay t2
                       ON t1.activity_id = t2.activity_id
                          AND t2.status = :_status_active
                          AND t2.essay_id = :_assay_elisa
                          AND t1.activity_property_id = :_property_get; ",
        [
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_assay_elisa' => Functional::ESSAY_ELISA,
          ':_property_get' => Functional::ACTIVITY_PROPERTY_GET,
        ]
      )->queryScalar();

      $reading_data = Yii::$app->db->createCommand(
        "SELECT r1.request_id,
                r1.agent_id,
                r1.support_order_id || '.' || r1.activity_id AS activity_support_id,
                'SUPP.' || r1.support_order_id || '-' || t3.short_name AS activity_support,
                r1.support_cell_position,
                CASE
                  WHEN t2.reading_data_type_id = :_rdt_entry THEN t1.request_code
                  ELSE 'Control'
                END AS request_code,
                CASE
                  WHEN t2.reading_data_type_id = :_rdt_entry THEN Right(t2.short_name, Length(t2.short_name) - Length(t1.request_code) - 1)
                  ELSE t2.short_name
                END AS sample_name,
                
                p1.short_name AS sample_type,
                r1.reading_data_type_id,
                r1.primary_order_num ||'.' ||( r1.grafting_number_id + 1 ) AS order,
                r1.support_master_id,
                r1.reading_data_id,
                
                CASE
                  WHEN r1.text_result IS NULL THEN 'empty'
                  ELSE r1.text_result
                END AS text_result,
                
                CASE
                  WHEN r1.text_result IS NULL THEN 'label label-empty'
                  ELSE 'label label-' ||r1.text_result
                END AS class_result,
                
                r1.auxiliary_result,
                CASE
                  WHEN r1.number_result IS NULL THEN '0.000'
                  ELSE Cast(r1.number_result AS TEXT)
                END AS number_result,
                case
                  WHEN t2.reading_data_type_id = :_rdt_positive THEN 'background-color:#ffc4d6'
                  WHEN t2.reading_data_type_id = :_rdt_negative THEN 'background-color:#d0f4de'
                  WHEN t2.reading_data_type_id = :_rdt_buffer THEN 'background-color:#adb5bd'
                  
                  WHEN Right(t1.request_code, 1) = '0' THEN 'background-color:#e4c1f9'
                  WHEN Right(t1.request_code, 1) = '1' THEN 'background-color:#a9def9'
                  WHEN Right(t1.request_code, 1) = '2' THEN 'background-color:#dee2ff'
                  WHEN Right(t1.request_code, 1) = '3' THEN 'background-color:#fcf6bd'
                  WHEN Right(t1.request_code, 1) = '4' THEN 'background-color:#f4a261'
                  WHEN Right(t1.request_code, 1) = '5' THEN 'background-color:#F48FB1'
                  WHEN Right(t1.request_code, 1) = '6' THEN 'background-color:#EDBB90'
                  WHEN Right(t1.request_code, 1) = '7' THEN 'background-color:#A3E4D7'
                  WHEN Right(t1.request_code, 1) = '8' THEN 'background-color:#CCD1D1'
                  WHEN Right(t1.request_code, 1) = '9' THEN 'background-color:#EFEF00'
                END AS bg_color
        FROM   plims.bsns_reading_data r1
                inner join plims.bsns_request t1
                        ON r1.request_id = t1.request_id
                inner join plims.bsns_composite_sample t2
                        ON r1.composite_sample_id = t2.composite_sample_id
                inner join plims.bsns_activity t3
                        ON r1.activity_id = t3.activity_id
                inner join plims.bsns_parameter p1
                        ON r1.composite_sample_type_id = p1.parameter_id
        WHERE  r1.support_master_id = :_event_id
                AND r1.agent_id = :_agent_id
                AND r1.status = :_status_active
                AND r1.activity_id <> :_activity_get
        ORDER  BY r1.support_order_id, r1.activity_id;",
        [
          ':_rdt_entry' => Functional::READING_DATA_ENTRY,
          ':_rdt_positive' => Functional::READING_DATA_POSITIVE,
          ':_rdt_negative' => Functional::READING_DATA_NEGATIVE,
          ':_rdt_buffer' => Functional::READING_DATA_BUFFER,
          ':_event_id' => $event_id,
          ':_agent_id' => $agent_id,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_activity_get' => $activity_get,
        ]
      )->queryAll();
      $reading_data_list = ArrayHelper::index($reading_data, 'reading_data_id');
      $activity_support_cell_position_reading_list =  ArrayHelper::map($reading_data, 'support_cell_position', 'reading_data_id', 'activity_support');
      $activity_support_list =  ArrayHelper::map($reading_data, 'activity_support_id', 'activity_support');
    }

    $is_documents_complete = Yii::$app->db->createCommand(
      "SELECT CASE
            WHEN Count(DISTINCT agent_id) = :_agent_count THEN true
            ELSE false
          END AS is_documents_complete
      FROM   plims.bsns_request_documentation
      WHERE  support_master_id = :_event_id
          AND activity_id = :_activity_set_first; ",
      [
        ':_event_id' => $event_id,
        ':_activity_set_first' => $activity_set_first,
        ':_agent_count' => count($agent_list),
      ]
    )->queryScalar();


    $model_upload_absorbance = new UploadForm();

    $absorbance_file_list = Yii::$app->db->createCommand(
      "SELECT 'SUPP.'
            || t1.support_order_id
            || '-'
            || t2.short_name         AS activity_support,
            t1.path_documentation,
            t1.document_name,
            t1.document_extension,
            t1.path_documentation
            || t1.document_name
            || '.'
            || t1.document_extension AS full_name,
            t1.document_name
            || '.'
            || t1.document_extension AS file_name
      FROM   plims.bsns_request_documentation t1
            INNER JOIN plims.bsns_activity t2
                    ON t1.activity_id = t2.activity_id
                      AND t1.support_master_id = :_event_id
                      AND t1.agent_id = :_agent_id
                      AND t1.status = :_status_active;",
      [
        ':_event_id' => $event_id,
        ':_agent_id' => $agent_id,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    $absorbance_file_list =  ArrayHelper::map($absorbance_file_list, 'activity_support', 'full_name');
    $rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];

    $rpa_status = Yii::$app->db->createCommand(
      "SELECT DISTINCT 
        CASE
          WHEN request_process_essay_status_id = :_rpe_pending THEN true
          ELSE false
        END AS is_pending,
        CASE
          WHEN request_process_essay_status_id = :_rpe_in_process THEN
          true
          ELSE false
        END AS is_in_process,
        CASE
          WHEN request_process_essay_status_id = :_rpe_finished THEN
          true
          ELSE false
        END AS is_finished
      FROM   plims.bsns_multi_request_event
      WHERE  support_master_id = :_event_id;",
      [
        ':_rpe_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
        ':_rpe_in_process' => Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
        ':_rpe_finished' => Functional::REQUEST_PROCESS_ESSAY_FINISHED,
        ':_event_id' => $event_id,
      ]
    )->queryOne();

    $is_pending =  $rpa_status['is_pending'];
    $is_in_process =  $rpa_status['is_in_process'];
    // $is_finished =  $rpa_status['is_finished'];


    return $this->render(
      'management',
      [
        'event_id' => $event_id,
        // ------------------------------------------------------
        'event_code' => $event_code,
        'agent_list' => $agent_list,
        'agent_id' => $agent_id,
        'reading_data_list' => $reading_data_list, // lista de reading_datas con key unico reading_data_id
        'activity_support_cell_position_reading_list' => $activity_support_cell_position_reading_list, //lista distribuida en SUPPORT y ACTIVITY con valores READING DATAS correspondientes
        'activity_support_list' => $activity_support_list,
        'absorbance_file_list' => $absorbance_file_list,
        // ------------------------------------------------------
        'model_upload_absorbance' => $model_upload_absorbance,
        'rows' =>  $rows,
        'is_in_process' => $is_in_process,
        'is_pending' => $is_pending,
        'is_documents_complete' => $is_documents_complete,
      ]
    );
  }

  // subir absorbancias con resultados por agente y evaluación
  public function actionUploadAbsorbance($event_id, $agent_id)
  {
    try {
      // actualiza el evento [in process]
      Yii::$app->db->createCommand(
        "UPDATE plims.bsns_multi_request_event
        SET request_process_essay_status_id = :_rpa_in_process
        WHERE support_master_id = :_event_id
        AND request_process_essay_status_id = :_rpa_pending;",
        [
          ':_rpa_pending' => functional::REQUEST_PROCESS_ESSAY_PENDING,
          ':_rpa_in_process' => functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
          ':_event_id' => $event_id,
        ]
      )->execute();

      // --------------------------------------------------------- //
      $data_configuration = Yii::$app->db->createCommand(
        "SELECT short_name,
          number_value
      FROM   plims.bsns_parameter
      WHERE  entity = 'absorbance'
          AND singularity = 'configuration';"
      )->queryAll();

      // --------------------------------------------------------- //
      $data_configuration = ArrayHelper::map($data_configuration, 'short_name', 'number_value');
      $sheet = (int) $data_configuration['sheet'];
      $initial_row =  (int) $data_configuration['initial_row'];
      $last_row =  (int) $data_configuration['last_row'];
      $initial_column = (int) $data_configuration['initial_column'];
      $last_column = (int) $data_configuration['last_column'];

      // --------------------------------------------------------- //
      $support_activity_name = yii::$app->request->post('support_activity_name');
      $support_order_id = explode('.', $support_activity_name)[0];
      $activity_id = explode('.', $support_activity_name)[1];

      // --------------------------------------------------------- //
      $data_readings = Yii::$app->db->createCommand(
        "SELECT reading_data_id,
              support_cell_position,
              number_result,
              reading_data_type_id,
              grafting_number_id
        FROM plims.bsns_reading_data
        WHERE support_master_id = :_support_master_id
              AND agent_id = :_agent_id
              AND support_order_id = :_support_order_id
              AND activity_id = :_activity_id
              AND status = :_status_active
        ORDER BY support_cell_position;",
        [
          ':_support_master_id' => $event_id,
          ':_agent_id' => $agent_id,
          ':_support_order_id' => $support_order_id,
          ':_activity_id' => $activity_id,
          ':_status_active' => Functional::STATUS_ACTIVE,
        ]
      )->queryAll();
      $data_readings_rdt_scp_rdi = ArrayHelper::map($data_readings, 'support_cell_position', 'reading_data_id', 'reading_data_type_id');
      $data_readings_rdi_scp_rdt = ArrayHelper::map($data_readings,  'support_cell_position', 'reading_data_type_id', 'reading_data_id');

      // --------------------------------------------------------- //
      $abs_data_results = [];
      $model_upload_absorbance = new UploadForm();
      if (Yii::$app->request->isPost) {
        $serial =  "E" . $event_id  . ".S" . $support_order_id . ".A" . $activity_id;
        $model_upload_absorbance->mixFiles = UploadedFile::getInstances($model_upload_absorbance, 'mixFiles');
        if ($model_upload_absorbance->uploadAbsorbances("_" . $serial, $event_id, $support_order_id, $activity_id, $agent_id)) {
          $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
          $file_name =   $model_upload_absorbance->path . $model_upload_absorbance->filename;
          $spreadsheet = $reader->load($file_name);
          $sheet_data = $spreadsheet->getSheet($sheet)->toArray();
          $row = 0;
          $col = 0;
          foreach ($sheet_data as $row_key => $row_column_data) {
            if ($row_key >= $initial_row and $row_key <= $last_row) {
              $row = $row + 1;
              foreach ($row_column_data as $columnKey => $cell_value) {
                if ($columnKey >= $initial_column and $columnKey <= $last_column) {
                  $position = $row + (8 * $col);
                  $abs_data_results[$position] = $cell_value;
                  $col = $col + 1;
                }
              }
              $col = 0;
            }
          }
        }
      }
      // --------------------------------------------------------- //
      // neg_avg_sup and neg_avg_inf
      $neg_acum = 0;
      foreach ($data_readings_rdt_scp_rdi[Functional::READING_DATA_NEGATIVE] as $support_cell_position => $reading_data_id) {
        $neg_acum = $neg_acum + $abs_data_results[$support_cell_position];
      }
      $neg_avg_sup = 0.9 * $neg_acum;
      $neg_avg_inf = 0.75 * $neg_acum;
      // --------------------------------------------------------- //
      foreach ($data_readings_rdi_scp_rdt as $reading_data_id => $scp_rdt) {
        $support_cell_position = key($scp_rdt);
        $reading_data_type = $scp_rdt[$support_cell_position];
        $number_result = $abs_data_results[$support_cell_position];
        // P|N|D and controls condition
        switch ($reading_data_type) {
          case Functional::READING_DATA_ENTRY:
            if ($number_result < $neg_avg_inf) {
              $text_result = 'negative';
            } else if ($number_result < $neg_avg_sup) {
              $text_result = 'doubt';
            } else {
              $text_result = 'positive';
            }
            break;
          case Functional::READING_DATA_POSITIVE:
            $text_result =  'positive';
            break;
          case Functional::READING_DATA_NEGATIVE:
            $text_result =  'negative';
            break;
          case Functional::READING_DATA_BUFFER:
            $text_result =  'buffer';
            break;
          default:
            break;
        }
        // update number and text result
        Yii::$app->db->createCommand(
          "UPDATE plims.bsns_reading_data 
          SET number_result = :_number_result, 
          text_result = :_text_result 
          WHERE reading_data_id = :_reading_data_id;",
          [
            ':_number_result' => $number_result,
            ':_text_result' => $text_result,
            ':_reading_data_id' => $reading_data_id,
          ]
        )->execute();
      }
      // --------------------------------------------------------- //
    } catch (\Throwable $th) {

      $code = $th->getCode();
      switch ($code) {
        case 2:
          Yii::$app->session->setFlash('info', 'Please select a Support-Evaluation(s) - ' . $th->getMessage());
          break;
        default:
          Yii::$app->session->setFlash('info', $th->getMessage());
          break;
      }
    }
    return $this->redirect(
      [
        'management',
        'event_id' => $event_id,
        'agent_id' => $agent_id
      ]
    );
  }

  // -----------[consolidation-preview]-----------//
  public function actionConsolidationPreview($event_id, $agents)
  {

    $first_activity = Yii::$app->db->createCommand(
      "SELECT activity_id
        FROM   plims.bsns_activity_by_essay
        WHERE  essay_id = :_assay_elisa
               AND num_order = :_num_order_ini;",
      [
        ':_assay_elisa' => Functional::ESSAY_ELISA,
        ':_num_order_ini' => Functional::NUM_ORDER_INI,
      ]
    )->queryScalar();

    $data = Yii::$app->db->createCommand(
      "SELECT t3.activity_id
              || '.'
              || t3.short_name AS activity,
              t2.short_name    AS agent,
              t1.document_name AS document
        FROM   plims.bsns_request_documentation t1
              INNER JOIN plims.bsns_agent t2
                      ON t1.agent_id = t2.agent_id
              INNER JOIN plims.bsns_activity t3
                      ON t1.activity_id = t3.activity_id
        WHERE  t1.support_master_id = :_event_id
              AND t1.status = :_status_active
        ORDER  BY t3.short_name,
                t2.short_name;",
      [
        ':_event_id' => $event_id,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    $data = ArrayHelper::map($data, 'agent', 'document', 'activity');

    return $this->renderAjax(
      'consolidation-preview',
      [
        'data' => $data,
        'agents' => $agents,
        'first_activity' => $first_activity,
      ]
    );
  }

  // consolida los resultados de las absorbancias en la actividad consolidada
  public function actionConsolidateAbsorbances($event_id)
  {
    // obtener datos parametrales (actividad consolidadora -> activity_get)
    $activity_get =   Yii::$app->db->createCommand(
      "SELECT t1.activity_id
      FROM   plims.bsns_activity t1
             INNER JOIN plims.bsns_activity_by_essay t2
                     ON t1.activity_id = t2.activity_id
                        AND t2.status = :_status_active
                        AND t2.essay_id = :_assay_elisa
                        AND t1.activity_property_id = :_property_get; ",
      [
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_assay_elisa' => Functional::ESSAY_ELISA,
        ':_property_get' => Functional::ACTIVITY_PROPERTY_GET,
      ]
    )->queryScalar();

    // actualizar el resultado en la actividad consolidadora
    $query = "UPDATE plims.bsns_reading_data AS tab1
              SET    text_result = tab2.consolidate_result, 
                     auxiliary_result = tab2.consolidate_result
              FROM   (

                            SELECT r2.support_order_id,
                                  r2.support_cell_position,
                                  r2.agent_id,
                                  r2.support_master_id,
                                  
                                  CASE
                                    WHEN 'positive' = any ( Array_agg(r2.text_result) ) THEN 'positive'
                                    WHEN 'doubt' = any ( Array_agg(r2.text_result) ) THEN 'doubt'
                                    WHEN 'buffer' = any ( Array_agg(r2.text_result) ) THEN 'buffer'
                                    ELSE 'negative'
                                  END AS consolidate_result

                            FROM   plims.bsns_reading_data r2
                            WHERE  r2.support_master_id = :_event_id ";
    $cbx_consolidate = Yii::$app->request->post('cbx_consolidate');
    $activity_list = array_keys($cbx_consolidate);
    $activity_list_text = implode(",", $activity_list);
    $query = $query
      . " AND r2.activity_id in (" . $activity_list_text . ") ";
    $query = $query
      . " AND r2.status = :_status_active
          AND r2.is_deleted IS FALSE
    GROUP  BY r2.support_order_id,
              r2.support_cell_position,
              r2.agent_id,
              r2.support_master_id 
              ) AS tab2
              WHERE tab1.support_master_id = :_event_id
              AND tab1.activity_id = :_activity_get
              AND tab1.support_order_id = tab2.support_order_id
              AND tab1.support_cell_position = tab2.support_cell_position
              AND tab1.agent_id = tab2.agent_id;";

    Yii::$app->db->createCommand(
      $query,
      [
        ':_event_id' => $event_id,
        ':_activity_get' => $activity_get,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->execute();

    return $this->redirect(
      [
        'consolidation',
        'event_id' => $event_id,
      ]
    );
  }

  // -----------[consolidation]-----------//
  public function actionConsolidation($event_id = null, $index_lvl_2 = null, $index_lvl_1 = null)
  {
    $event_code = 'MR' . substr('00000' . $event_id, -5,);

    $activity_get =   Yii::$app->db->createCommand(
      "SELECT t1.activity_id
      FROM   plims.bsns_activity t1
             INNER JOIN plims.bsns_activity_by_essay t2
                     ON t1.activity_id = t2.activity_id
                        AND t2.status = :_status_active
                        AND t2.essay_id = :_assay_elisa
                        AND t1.activity_property_id = :_property_get;",
      [
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_assay_elisa' => Functional::ESSAY_ELISA,
        ':_property_get' => Functional::ACTIVITY_PROPERTY_GET,
      ]
    )->queryScalar();

    $reading_data = Yii::$app->db->createCommand(
      "SELECT 
            r1.agent_id||'|'||'Plate-'||r1.support_order_id||'.'||Round(Avg(r1.support_cell_position), 0)  
              AS index_lvl_1,

            r1.agent_id||'|'||'Plate-'||r1.support_order_id  
              AS index_lvl_2,
            
            r1.agent_id|| '|'||a1.short_name
              AS index_lvl_3,
            -- ==================== --
            r1.agent_id||'|'||'Plate-'||r1.agent_id||r1.support_order_id
              AS activity_support,
            CASE
              WHEN r1.reading_data_type_id = :_rdt_entry THEN t1.request_code
              ELSE 'Control'
            END                                               AS request_code_title,
            CASE
              WHEN r1.reading_data_type_id = :_rdt_entry THEN RIGHT(t2.short_name, Length(t2.short_name) - Length(t1.request_code) - 1)
              ELSE t2.short_name
            END                                               AS sample_name,
            p1.short_name                                     AS short_sample_type,
            p1.long_name                                      AS long_sample_type,
            case
              when r1.num_order_id = 1 then 'standard process'
              else 'reprocess'
            end as process,
            String_agg(case when r1.observation is null then '...' else r1.observation end, '|') as observation,
            CASE
              WHEN r1.reading_data_type_id = :_rdt_positive THEN 'background-color:#ffc4d6'
              WHEN r1.reading_data_type_id = :_rdt_negative THEN 'background-color:#d0f4de'
              WHEN r1.reading_data_type_id = :_rdt_buffer THEN 'background-color:#adb5bd'
              WHEN RIGHT(t1.request_code, 1) = '0' THEN 'background-color:#e4c1f9'
              WHEN RIGHT(t1.request_code, 1) = '1' THEN 'background-color:#a9def9'
              WHEN RIGHT(t1.request_code, 1) = '2' THEN 'background-color:#dee2ff'
              WHEN RIGHT(t1.request_code, 1) = '3' THEN 'background-color:#fcf6bd'
              WHEN RIGHT(t1.request_code, 1) = '4' THEN 'background-color:#f4a261'
              WHEN RIGHT(t1.request_code, 1) = '5' THEN 'background-color:#F48FB1'
              WHEN RIGHT(t1.request_code, 1) = '6' THEN 'background-color:#EDBB90'
              WHEN RIGHT(t1.request_code, 1) = '7' THEN 'background-color:#A3E4D7'
              WHEN RIGHT(t1.request_code, 1) = '8' THEN 'background-color:#CCD1D1'
              WHEN RIGHT(t1.request_code, 1) = '9' THEN 'background-color:#EFEF00'
            END                                               AS bg_color,
            String_agg(Cast(r1.reading_data_id AS TEXT), '_') AS reading_data_ids,
            r1.support_order_id||'_'||Round(Avg(r1.support_cell_position), 0) AS index_rows_id,
            r1.agent_id AS agent_id_key,
            a1.short_name AS agent_name,
          
            CASE 
              WHEN r1.reading_data_type_id = :_rdt_entry then r1.text_result
              WHEN r1.reading_data_type_id = :_rdt_positive THEN 'positive'
              WHEN r1.reading_data_type_id = :_rdt_negative THEN 'negative'
              WHEN r1.reading_data_type_id = :_rdt_buffer THEN 'buffer'
              ELSE '-'				
            END AS original_result,
            
            CASE 
              WHEN r1.reading_data_type_id = :_rdt_entry then r1.auxiliary_result
              WHEN r1.reading_data_type_id = :_rdt_positive THEN 'positive'
              WHEN r1.reading_data_type_id = :_rdt_negative THEN 'negative'
              WHEN r1.reading_data_type_id = :_rdt_buffer then 'buffer'
              ELSE '-'			
            END AS auxiliary_result
            
      FROM   plims.bsns_reading_data r1
            INNER JOIN plims.bsns_request t1
                    ON r1.request_id = t1.request_id
            INNER JOIN plims.bsns_composite_sample t2
                    ON r1.composite_sample_id = t2.composite_sample_id
            INNER JOIN plims.bsns_parameter p1
                    ON r1.composite_sample_type_id = p1.parameter_id
            INNER JOIN plims.bsns_agent a1
                    ON r1.agent_id = a1.agent_id
      WHERE  r1.support_master_id = :_event_id
            AND r1.status = :_status_active
            AND r1.activity_id = :_activity_get
      GROUP  BY support_order_id,
              activity_support,
              request_code_title,
              sample_name,
              short_sample_type,
              long_sample_type,
              process,
              agent_id_key,
              agent_name,
              original_result,
              auxiliary_result,
              r1.reading_data_type_id,
              bg_color
      ORDER BY 2, 3;",
      [
        ':_rdt_entry' => Functional::READING_DATA_ENTRY,
        ':_rdt_positive' => Functional::READING_DATA_POSITIVE,
        ':_rdt_negative' => Functional::READING_DATA_NEGATIVE,
        ':_rdt_buffer' => Functional::READING_DATA_BUFFER,
        ':_event_id' => $event_id,
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_activity_get' => $activity_get,
      ]
    )->queryAll();

    $reading_data_list = ArrayHelper::index($reading_data, 'index_lvl_1');
    $rdl_001 = ArrayHelper::map($reading_data, 'index_lvl_2', 'index_lvl_2', 'index_lvl_3');
    $index_lvl_2 = is_null($index_lvl_2) ? ArrayHelper::getColumn($reading_data, 'index_lvl_2')[0] : $index_lvl_2;
    $rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];


    $is_in_process = Yii::$app->db->createCommand(
      "SELECT DISTINCT CASE
            WHEN request_process_essay_status_id = :_rpe_in_process THEN
            true
            ELSE false
          END AS is_in_process
      FROM   plims.bsns_multi_request_event
      WHERE  support_master_id = :_event_id;",
      [
        ':_rpe_in_process' => functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
        ':_event_id' => $event_id,
      ]
    )->queryScalar();


    return $this->render(
      'consolidation',
      [
        'event_id' => $event_id,
        'event_code' => $event_code,
        'reading_data_list' => $reading_data_list,
        'rdl_001' =>  $rdl_001,
        'index_lvl_1' =>  $index_lvl_1,
        'index_lvl_2' =>  $index_lvl_2,
        'rows' =>  $rows,
        'is_in_process' => $is_in_process,
      ]
    );
  }



  public function actionSave($event_id, $index_lvl_2, $index_lvl_1, $reading_data_ids)
  {
    $_user = Yii::$app->user->identity->id;
    $_date =  date("Y-m-d H:i:s", time());

    $auxiliary_result = Yii::$app->request->post('result_name');
    $observation = Yii::$app->request->post('observation');
    $readings_data_list = explode('_', $reading_data_ids);

    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      foreach ($readings_data_list as $key => $_reading_data_id) {

        $db->createCommand(
          "UPDATE plims.bsns_reading_data
          SET    status = :_status_disabled
          WHERE  reading_data_id = :_reading_data_id;",
          [
            ':_status_disabled' => Functional::STATUS_DISABLED,
            ':_reading_data_id' => $_reading_data_id,
          ]
        )->execute();

        $db->createCommand(
          "INSERT INTO plims.bsns_reading_data
                    (request_id,
                    num_order_id,
                    essay_id,
                    activity_id,
                    agent_id,
                    support_order_id,
                    support_cell_position,
                    grafting_number_id,
                    historical_data_version,
                    composite_sample_group_id,
                    composite_sample_id,
                    composite_sample_type_id,
                    crop_id,
                    workflow_id,
                    reading_data_type_id,
                    auxiliary_result,
                    text_result,
                    primary_order_num,
                    secondary_order_num,
                    tertiary_order_num,
                    registered_by,
                    registered_at,
                    status,
                    support_master_id,
                    request_process_essay_id,
                    request_process_essay_agent_id,
                    request_process_essay_activity_id,
                    request_process_essay_activity_sample_id,
                    support_id,
                    observation,
                    is_deleted)
          SELECT request_id,
              num_order_id,
              essay_id,
              activity_id,
              agent_id,
              support_order_id,
              support_cell_position,
              grafting_number_id,
              historical_data_version + 1 AS historical_data_version,
              composite_sample_group_id,
              composite_sample_id,
              composite_sample_type_id,
              crop_id,
              workflow_id,
              reading_data_type_id,
              :_auxiliary_result,
              :_auxiliary_result,
              primary_order_num,
              secondary_order_num,
              tertiary_order_num,
              :_user,
              :_date,
              :_status_active,
              support_master_id,
              request_process_essay_id,
              request_process_essay_agent_id,
              request_process_essay_activity_id,
              request_process_essay_activity_sample_id,
              support_id,
              :_observation,
              is_deleted
          FROM   plims.bsns_reading_data
          WHERE  reading_data_id = :_reading_data_id;",
          [
            ':_auxiliary_result' => $auxiliary_result,
            ':_user' => $_user,
            ':_date' => $_date,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_reading_data_id' => $_reading_data_id,
            ':_observation' => $observation,
          ]
        )->execute();
      }

      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }




    return $this->redirect(
      [
        'consolidation',
        'event_id' => $event_id,
        'index_lvl_2' => $index_lvl_2,
        'index_lvl_1' => $index_lvl_1,
      ]
    );
  }

  public function actionFinish($event_id)
  {
    Yii::$app->db->createCommand(
      "UPDATE plims.bsns_multi_request_event
      SET    request_process_essay_status_id = :_rpe_finished
      WHERE  support_master_id = :_event_id;",
      [
        ':_rpe_finished' => functional::REQUEST_PROCESS_ESSAY_FINISHED,
        ':_event_id' => $event_id,
      ]
    )->execute();

    return $this->redirect(
      [
        'consolidation',
        'event_id' => $event_id,
      ]
    );
  }
}

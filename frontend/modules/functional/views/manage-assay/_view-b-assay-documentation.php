<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
?>
<style>
  #w3 {
    height: 180px;
    overflow-y: scroll;
    overflow-x: hidden;
  }
</style>
<div class="card card-outline card-secondary">

  <div class="card-header">
    <h3 class="card-title text-blue-link">Documentation</h3>
  </div>

  <div class="card-body" style="height: 300px;">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <div class="row">
      <div class="col-lg-6">
        <div class="input-group" title="File extensions: xls, xlsx, pdf, doc, docx, ppt, pptx, jpg, png, txt, zip.">
          <?= $form->field($model_upload_documentation, 'mixFiles[]')->fileInput(['multiple' => true]) ?>
        </div>
      </div>
      <div class="col-lg-6">
        <?= Html::a(
          '<i class="fa fa-upload"></i> Upload file(s)',
          [
            'upload-document',
            'request_id' => $request_id,
            'essay_id' => $essay_id,
          ],
          [
            'id' => 'save_absorbance_btn_id',
            'name' => 'save_absorbance_btn_name',
            'title' => 'Upload file',
            'data-method' => 'post',
            'class' => 'btn btn-default pull-right',
          ]
        ); ?>
      </div>
    </div>
    <?php ActiveForm::end() ?>
    <div class="row">
      <div class="col-lg-12">
        <?php $data_documentation_provider = new ArrayDataProvider(
          [
            'allModels' => $data_documentation,
            'pagination' => [
              'pageSize' => 0,
            ],
            'sort' => [
              'attributes' => [
                'request_documentation_id',
                'path_documentation',
                'document_name',
                'document_extension',
                'request_id',
                'file_type_id',
              ],
            ],
          ]
        ); ?>
        <?= ListView::widget(
          [
            'dataProvider' => $data_documentation_provider,
            'itemView' => '_item-view-document',
            'layout' => '{items}{pager}{summary}',
            'pager' => [
              'firstPageLabel' => 'first',
              'lastPageLabel' => 'last',
              'prevPageLabel' => 'previous',
              'nextPageLabel' => 'next',
            ],
          ]
        ); ?>
      </div>
    </div>
  </div>


</div>
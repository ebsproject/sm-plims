<?php

use yii\helpers\Html;
// VARIABLES
$modal_title = 'Assay manage process';
$id_mod =  $model['rpa_sample_code'];

// URL
$url_1 = ['#'];
$url_2 = [
  'manage-sample-results/management',
  'request_id' =>    $model['request_id'],
  'num_order_id' => $model['num_order_id'],
  'essay_id' => $model['essay_id'],
  'composite_sample_type_id' => $model['composite_sample_type_id'],
  'request_process_essay_id' => $model['request_process_essay_id'],
];
$url =  $model['rpa_status_code'] == '3.1' ? $url_1 : $url_2;
// CLASS
$class_1 = 'modal-view-action';
$class_2 = null;
$class = $model['rpa_status_code'] == '3.1' ? $class_1 : $class_2;
// JS
$JsFunction1 = '';
$JsFunction2 = new \yii\web\JsExpression(
  "function popUpModel() {
    $('#" . $id_mod . "')
	  .modal('show')
	  .find('.modal-view-content')
  }; 
  popUpModel();"
);
$JsFunction = $model['rpa_status_code'] == '3.1' ? $JsFunction2 : $JsFunction1;
?>

<div class="pull-right" style="margin-top: 5px; margin-right: 10px;">
  <?php if (is_null($model['start_date'])) { ?>
    <?= Html::a(
      '<i class="glyphicon glyphicon-remove"></i>',
      [
        'remove-sample',
        'request_id' => $model['request_id'],
        'num_order_id' => $model['num_order_id'],
        'essay_id' => $model['essay_id'],
        'composite_sample_type_id' => $model['composite_sample_type_id'],
      ],
      [
        'title'        => 'Remove sample type',
        'data-method'  => 'post',
        'class' =>  'bg-gray-light'
      ]
    ) ?>
  <?php } ?>
</div>

<?= Html::a(
  '<div class="info-box bg-gray-light">

    <span class="info-box-icon"><i class="' . $model['rpa_sample_type_class'] . '"></i></span>

    <div class="info-box-content">

      <span class="info-box-number">' . $model['rpa_sample_type_name'] . ' <small>(' . $model['code'] . ')</small>  
        <small>
          <i title="' . ($model['status'] == 'active' ? 'This type of sample is in an active state, it has a distribution.' : 'This type of sample is disabled, it does not present a distribution.') . '" class="fa fa-circle text-' . ($model['status'] == 'active' ? 'green' : 'red') . '"></i>
          <span class="label label-' . $model['rpa_status_class'] . '" title="Rpe.id: [' . ($model['request_process_essay_id']) . ']">' . $model['rpa_status_name'] . '</span>
          <span class="label label-navy">' . ($model['composite_sample_type_id'] == $model['request_process_essay_root_id'] ? 'R' : '') . '</span> 
        </small>
      </span>

      <div class="progress">
        <div class="progress-bar" style="width: 0%"></div>
      </div>
      
      <span class="info-box-text pull-left">' . $model['crop_name'] . '</span>  
      
      <span class="pull-right">
        <i class="fa fa-user"></i> ' . (isset($model['start_by']) ? $model['start_by'] : '') . ' 
        <i class="fa fa-calendar-o"></i> ' .  $model['start_date']  . ' 
        <i class="fa fa-angle-double-right"></i> 
        <i class="fa fa-user"></i> ' . (isset($model['finish_by']) ? $model['finish_by'] : '') . '
        <i class="fa fa-calendar-check-o"></i> ' . $model['finish_date']  . '
      </span>
       
     
    </div>
  </div>',
  $url,
  [
    'class' => $class,
    'title' => 'Manage results for this sample',
    'value' => null,
    'onclick' => $JsFunction,
  ]
); ?>

<!-- MODAL-INI -->
<div class="modal" id=<?= $model['rpa_sample_code'] ?> tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">
          <?= Html::encode($modal_title) ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>

      </div>
      <div class="modal-body">
        <div class="modal-view-content">Are you sure to start recording the results in <b><?= $model['rpa_sample_type_name'] ?></b> process?</div>
      </div>
      <div class="modal-footer">
        <?= Html::a(
          'Cancel',
          ['#'],
          [
            'title' => 'Cancel operation',
            'class' => 'btn btn-default',
            'data-dismiss' => "modal",
          ]
        ) ?>
        <?= Html::a(
          'Continue',
          $url_2,
          [
            'title' => 'Manage results for this sample',
            'class' => 'btn btn-primary'
          ]
        ) ?>
      </div>
    </div>
  </div>
</div>
<!-- MODAL-END -->
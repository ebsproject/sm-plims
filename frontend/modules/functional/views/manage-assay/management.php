<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['manage-request/index']];
$this->params['breadcrumbs'][] = ['label' =>  'Manage Request' . ' (' . $request_code . ')', 'url' => ($request_status_code == '1.1' ? ['manage-request/index'] : ['manage-request/management', 'request_id' => $request_id])];
$this->title = 'Assay Manage';
$this->params['breadcrumbs'][] =  $this->title . ' (' . $assay_name . ')';
?>
<div class="management">
  <!-- #region BACK -->
  <div class="row">
    <div class="col-md-8">
      <h1><?= Html::encode($this->title) . ' (<span class="text-blue-link">' . $assay_name . '</span>)' ?></h1>
    </div>
    <div class="col-md-4 init-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back', ($request_status_code == '1.1' ? ['manage-request/index'] : ['manage-request/management', 'request_id' => $request_id]), ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6 col-md-6">
      <!-- Assay Detail -->
      <?= $this->render('_view-a-assay-detail', [
        'r_data' => $r_data,
        'rpa_header_data' => $rpa_header_data,
        'is_finished_rpa' => $is_finished_rpa,
        'is_finished_all' => $is_finished_all,
      ])  ?>
    </div>
    <div class="col-lg-6 col-md-6">
      <!-- Assay Documentation -->
      <?= $this->render('_view-b-assay-documentation', [
        'request_id' => $request_id,
        'num_order_id' => $num_order_id,
        'essay_id' => $essay_id,
        'model_upload_documentation' => $model_upload_documentation,
        'data_documentation' => $data_documentation,
      ])  ?>
    </div>
  </div>

  <?php if ($rpa_status_code >= $min_status) { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Standard processes</a></li>
            <li class=""><a href="#tab_1-1" data-toggle="tab" aria-expanded="false">Reprocesses</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab_2-2">
              <?= $this->render('_view-c-sample-results-management', [
                'request_id' => $request_id,
                'num_order_id' => $num_order_id,
                'is_remove_all' => $is_remove_all,
                'rpa_data_samples' => $rpa_data_samples,
                'essay_id' => $essay_id,
                'available_samples_list' => $available_samples_list,
                'is_finished_rpa' => $is_finished_rpa,
                'is_web_app' => $is_web_app,
              ]); ?>
            </div>
            <div class="tab-pane" id="tab_1-1">
              <?= $this->render('_view-c-sample-results-management-rep', [
                'request_id' => $request_id,
                'num_order_id' => $num_order_id,
                'is_remove_all' => $is_remove_all,
                'rpa_data_samples' => $rpa_data_samples_rep,
                'essay_id' => $essay_id,
                'available_samples_list' => $available_samples_list,
                'is_finished_rpa' => $is_finished_rpa,
                'is_web_app' => $is_web_app,
              ]); ?>
            </div>
          </div>
        </div>

      </div>
    </div>
  <?php } ?>

  <!-- #region BACK -->
  <div class="row ">
    <div class="col-md-12 final-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back', ($request_status_code == '1.1' ? ['manage-request/index'] : ['manage-request/management', 'request_id' => $request_id]), ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
</div>
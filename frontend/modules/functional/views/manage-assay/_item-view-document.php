<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="document_item">
  <div class="row">
    <div class="col-lg-6">
      <span class="text">
        <i class="fa fa-calendar-o"></i> <?= $model['registered_at'] ?> | <i class="<?= $model['class_details'] ?>"></i> <?= $model['document_name'] ?>
      </span>
    </div>

    <div class="col-lg-6">
      <div class="btn-group pull-right">
        <?= Html::a(
          '<i class="glyphicon glyphicon-trash"></i> Remove',
          [
            'remove-document',
            'request_id' => $model['request_id'],
            'num_order_id' => $model['num_order_id'],
            'request_documentation_id' => $model['request_documentation_id'],
            'essay_id' => $model['essay_id'],
          ],
          [
            'class' => 'text-red',
          ]
        ); ?>

        <a href="<?= Url::base() ?>/<?= $model['path_documentation'] ?><?= $model['document_name'] ?>.<?= $model['document_extension'] ?>">
          <i class="fa fa-download"></i> Download
        </a>
      </div>
    </div>
  </div>
</div>
<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;

?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Sample by Assay Reprocess</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-md-12">
        <?php
        $rpa_data_samples_provider = new ArrayDataProvider(
          [
            'allModels' => $rpa_data_samples,
            'pagination' => [
              'pageSize' => 0,
            ],
            'sort' => [
              'attributes' => [
                'request_id',
                'num_order_id',
                'essay_id',
                'crop_id',
                'workflow_id',
                'start_date',
                'finish_date',
                'check_date',
                'agent_qty',
                'agent_detail',
                'sub_total_cost_essay',
                'composite_sample_type_id'
              ],
            ],
          ]
        );
        ?>
        <?= ListView::widget(
          [
            'dataProvider' => $rpa_data_samples_provider,
            'itemView' => '_item-view-sample-results',
            'layout' => '{items}{pager}{summary}',
            'pager' => [
              'firstPageLabel' => 'first',
              'lastPageLabel' => 'last',
              'prevPageLabel' => 'previous',
              'nextPageLabel' => 'next',
            ],
          ]
        );
        ?>
      </div>
    </div>
  </div>
</div>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\DetailView;
?>

<div class="card card-outline card-secondary">

  <div class="card-header">
    <h3 class="card-title text-blue-link">Detail</h3>
  </div>

  <div class="card-body">

    <div class="row">
      <div class="col-md-6 col-lg-6">
        <?= DetailView::widget([
          'model' => $r_data,
          'attributes' => [
            [
              'label' => 'Request Code',
              'value' =>   $r_data['r_code']
            ],
            [
              'label' => 'Crop',
              'value' =>   $r_data['crop']
            ],
            [
              'label' =>  'Material Qty',
              'value' =>  $r_data['material_qty'],
            ],
            [
              'label' =>  'Sample Qty',
              'value' =>  $r_data['sample_qty'],
            ],
            [
              'label' => 'Request User',
              'value' =>   $r_data['requested_user'],
            ],
            [
              'label' => 'Date Created',
              'value' =>   $r_data['date_created'],
            ],
            [
              'label' => 'Seed distribution',
              'value' =>   $r_data['seed_distribution'],
            ],
          ],
          'options' => [
            'class' => 'table detail-view',
          ],
        ]) ?>
      </div>
      <div class="col-md-6 col-lg-6">
        <?= DetailView::widget([
          'model' => $rpa_header_data,
          'attributes' => [
            [
              'label' => 'Start date',
              'value' =>   $rpa_header_data['start_date']
            ],
            [
              'label' => 'Start by',
              'value' =>   $rpa_header_data['start_by']
            ],
            [
              'label' => 'Finish date',
              'value' =>   $rpa_header_data['finish_date']
            ],
            [
              'label' => 'Finish by',
              'value' =>   $rpa_header_data['finish_by']
            ],
            [
              'label' => 'Sample list',
              'value' =>   $rpa_header_data['sample_detail'],
              'format' => 'raw',
            ],
            [
              'label' => 'Source',
              'value' =>    '<i class="' . $rpa_header_data['assay_type_class'] . '"></i> ' . $rpa_header_data['assay_type_name'] . '',
              'format' => 'raw',
            ],
            [
              'label' => 'Status',
              'value' =>    '<span class="' . $rpa_header_data['rpa_status_class'] . '">' . $rpa_header_data['rpa_status_name'] . '</span>',
              'format' => 'raw',
            ],

          ],
          'options' => [
            'class' => 'table detail-view',
          ],
        ]) ?>
      </div>
    </div>
  </div>

  <?php if (!$is_finished_rpa && $is_finished_all) { ?>
    <div class="card-footer">
      <div class="row">
        <div class="col-lg-12">
          <div class="btn-group pull-right">
            <?= Html::a(
              'Finish',
              [
                'finish',
                'request_id' => Yii::$app->request->get('request_id'),
                // 'num_order_id' => Yii::$app->request->get('num_order_id'),
                'essay_id' => Yii::$app->request->get('essay_id'),
                // 'composite_sample_type_id' => Yii::$app->request->get('composite_sample_type_id'),
                // 'request_process_essay_id' => Yii::$app->request->get('request_process_essay_id'),
              ],
              [
                // 'id' => 'finish_btn_id',
                // 'name' => 'finish_btn_name',
                'class' => 'btn btn-success',
                'title'        => 'Finish assay process',
                // 'data' => [
                //   'confirm' => 'Are you completely sure about finishing the results record?',
                // ],
              ]
            );
            ?>
          </div>
        </div>
      </div>
    </div>

  <?php } ?>
</div>
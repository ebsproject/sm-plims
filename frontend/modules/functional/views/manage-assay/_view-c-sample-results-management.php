<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;
?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Sample by Assay</h3>
  </div>
  <div class="card-body">
    <?php if ($is_web_app) { ?>

      <?php if (!($is_finished_rpa)) { ?>
        <div class="row">
          <div class="col-md-12">
            <label class="control-label">Sample Type: </label>
            <?php $form = ActiveForm::begin(); ?>
            <div class="input-group">
              <?= Select2::widget(
                    [
                      'id' => 'select_id_1',
                      'name' => 'select_name_1',
                      'data' => $available_samples_list,
                      'value' => null,
                      'maintainOrder' => true,
                      'options' => [
                        'placeholder' => 'Select one o more samples types...',
                        'multiple' => true
                      ]
                    ]
                  );  ?>
              <span class="input-group-btn">
                <?= Html::a(
                      '<i class="fa fa-plus"></i> Add Sample(s)',
                      [
                        'add-sample',
                        'request_id' => $request_id,
                        'essay_id' => $essay_id,
                        'num_order_id' => $num_order_id,
                      ],
                      [
                        'class' => 'btn btn-default',
                        'data-method' => 'post',
                      ]
                    ) ?>
              </span>
            </div>
            <?php ActiveForm::end() ?>
          </div>
        </div>
      <?php } ?>

    <?php } ?>
    <br>
    <div class="row">
      <div class="col-md-12">


        <?php
        $rpa_data_samples_provider = new ArrayDataProvider(
          [
            'allModels' => $rpa_data_samples,
            'pagination' => [
              'pageSize' => 0,
            ],
            'sort' => [
              'attributes' => [
                'request_id',
                'num_order_id',
                'essay_id',
                'crop_id',
                'workflow_id',
                'start_date',
                'finish_date',
                'check_date',
                'agent_qty',
                'agent_detail',
                'sub_total_cost_essay',
                'composite_sample_type_id'
              ],
            ],
          ]
        );
        ?>
        <?= ListView::widget(
          [
            'dataProvider' => $rpa_data_samples_provider,
            'itemView' => '_item-view-sample-results',
            'layout' => '{items}{pager}{summary}',
            'pager' => [
              'firstPageLabel' => 'first',
              'lastPageLabel' => 'last',
              'prevPageLabel' => 'previous',
              'nextPageLabel' => 'next',
            ],
          ]
        );
        ?>
      </div>
    </div>

    <?php if ($is_remove_all) { ?>
      <div class="row">
        <div class="col-lg-12">
          <?= Html::a(
              'Remove all',
              [
                'remove-sample',
                'request_id' => $request_id,
                'num_order_id' => $num_order_id,
                'essay_id' => $essay_id,
                'composite_sample_type_id' => NULL,
              ],
              [
                'class' => 'pull-right',
                'title'        => 'Remove all sample type',
                'data-method'  => 'post',
              ]
            )
            ?>
        </div>
      </div>
    <?php } ?>

  </div>
</div>
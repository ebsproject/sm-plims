<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['manage-request/index']];
$this->title = 'Manage Request';
$this->params['breadcrumbs'][] = ['label' =>  $this->title . ' (' . $request_code . ')', 'url' => ([
  'manage-request/management', 'request_id' => $request_id
])];
$this->title = 'Assay Manage';
$this->params['breadcrumbs'][] =  ['label' =>  $this->title . ' (' . $assay_name . ')', 'url' => ([
  'manage-assay/management', 'request_id' => $request_id, 'essay_id' => $essay_id
])];
$this->title = 'Sample Results Management';
$this->params['breadcrumbs'][] =  $this->title . ' (' . $sample_type_name . ')';
?>

<div class="management">
  <!-- #region BACK -->
  <div class="row">
    <div class="col-md-8">
      <h1><?= Html::encode($this->title) . ' (<span class="text-blue-link">' . $sample_type_name . '</span>)' ?></h1>
    </div>
    <div class="col-md-4 init-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back',  [
        'manage-assay/management', 'request_id' => $request_id, 'essay_id' => $essay_id
      ], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->

  <?php if ($is_web_source and $is_in_process) { ?>

    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Set Results</a></li>
        <?php if ($is_set_bulk) { ?>
          <li class=""><a href="#tab_1-1" data-toggle="tab" aria-expanded="false">Set bulk results</a></li>
        <?php } ?>
      </ul>
      <div class="tab-content">
        <div class="tab-pane" id="tab_1-1">
          <!-- Set bulk results -->
          <?= $this->render('_view-b-set-sample-results-bulk', [
            'rpea_sample_count' => $rpea_sample_count,
          ])  ?>
        </div>
        <div class="tab-pane active" id="tab_2-2">
          <!-- Set Results -->
          <?= $this->render('_view-a-set-sample-results', [
            'rpea_sample_list' => $rpea_sample_list,
            'sample' => $sample,
            'rd_list' => $rd_list,
          ])  ?>
        </div>
      </div>
    </div>

  <?php } ?>


  <!-- ================================== -->
  <?php if (count($tab_list_2) > 0) { ?>
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <?php foreach ($tab_list_2 as $key => $value) { ?>
          <li <?= $value['class'] ?>><a href=<?= $value['href'] ?> data-toggle="tab" aria-expanded="false"><?= $value['text'] ?></a></li>
        <?php } ?>
      </ul>
      <div class="tab-content">
        <?php foreach ($tab_content_list_2 as $key => $value) { ?>
          <div <?= $value['class'] ?> id=<?= $value['id'] ?>>
            <?= $this->render('_view-c-data-results', [
              'dataProvider' => $value['dataProvider'],
              'dataColumns' => $value['dataColumns'],
              'title' =>  $value['title'],
              'is_in_process' => $is_in_process,
              'is_ready_to_finish' => $is_ready_to_finish,
              'is_finalisable_in_mobile_essays' => $is_finalisable_in_mobile_essays,
            ])  ?>
          </div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
  <!-- ================================== -->
  <?php if (count($tab_list_1) > 0) { ?>
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <?php foreach ($tab_list_1 as $key => $value) { ?>
          <li <?= $value['class'] ?>><a href=<?= $value['href'] ?> data-toggle="tab" aria-expanded="false"><?= $value['text'] ?></a></li>
        <?php } ?>
      </ul>
      <div class="tab-content">
        <?php foreach ($tab_content_list_1 as $key => $value) { ?>
          <div <?= $value['class'] ?> id=<?= $value['id'] ?>>
            <?= $this->render('_view-c-data-results', [
              'dataProvider' => $value['dataProvider'],
              'dataColumns' => $value['dataColumns'],
              'title' =>  $value['title'],
              'is_in_process' => $is_in_process,
              'is_ready_to_finish' => $is_ready_to_finish,
              'is_finalisable_in_mobile_essays' => $is_finalisable_in_mobile_essays,
            ])  ?>
          </div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
  <!-- ================================== -->
  <?php if (count($tab_list_3) > 0) { ?>
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <?php foreach ($tab_list_3 as $key => $value) { ?>
          <li <?= $value['class'] ?>><a href=<?= $value['href'] ?> data-toggle="tab" aria-expanded="false"><?= $value['text'] ?></a></li>
        <?php } ?>
      </ul>
      <div class="tab-content">
        <?php foreach ($tab_content_list_3 as $key => $value) { ?>
          <div <?= $value['class'] ?> id=<?= $value['id'] ?>>
            <?= $this->render('_view-c-data-results', [
              'dataProvider' => $value['dataProvider'],
              'dataColumns' => $value['dataColumns'],
              'title' =>  $value['title'],
              'is_in_process' => $is_in_process,
              'is_ready_to_finish' => $is_ready_to_finish,
              'is_finalisable_in_mobile_essays' => $is_finalisable_in_mobile_essays,
            ])  ?>
          </div>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
  <!-- ================================== -->


  <?php $form = ActiveForm::begin(
    [
      'options' => [
        'data-pjax' => true,
        'id' => 'manage-sample-results2'
      ]
    ]
  ); ?>

  <?php ActiveForm::end() ?>

  <!-- #region BACK -->
  <div class="row ">
    <div class="col-md-12 final-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back', ([
        'manage-assay/management', 'request_id' => $request_id, 'essay_id' => $essay_id
      ]), ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->
</div>
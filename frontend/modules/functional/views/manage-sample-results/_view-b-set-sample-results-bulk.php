<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;
use Yii;
?>

<style>
  .st-positive {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;
    background-color: #dd4b39 !important;
  }

  .st-negative {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;
    background-color: #00a65a !important;
  }

  .st-empty {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;
    background-color: #c1c1c1 !important;
  }

  .panel-result {
    padding: 10px;
    border: 1px solid #ccc !important;
    border-radius: 10px;
    height: 70px;
    margin: 0px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .panel-range {
    padding: 10px;
    border: 1px solid #ccc !important;
    border-radius: 10px;
    min-height: 60px;
    margin: 0px;
  }

  .box-body {
    min-height: 255px
  }
</style>

<div class="card card-outline card-secondary">


  <?php $form = ActiveForm::begin(
    [
      'options' => [
        'data-pjax' => true,
        'id' => 'manage-sample-results-bulk'
      ]
    ]
  );
  ?>

  <div class="card-header">
    <h3 class="card-title text-blue-link">Set bulk results</h3>
  </div>

  <div class="card-body">

    <br>
    <div class="row">

      <div class="col-sm-6 col-md-8 col-lg-10" style="margin-bottom: 1em;">
        <label class="control-label">Information:</label>
        Complete readings with empty value for results with <span class="label label-negative">NEGATIVE</span> <span class="label label-positive">POSITIVE</span> or keep on <span class="label label-empty">EMPTY</span> value in the sample range
        <br>
        <label>
          <input type="checkbox" name="cbx_overwrite">
          <span class="control-label">Overwrite any results</span>
        </label>
      </div>

      <div class="col-sm-6 col-md-4 col-lg-2">
        <label class="control-label">Result:</label>
        <div class="panel-result">
          <div class="wrapper_aux" style="margin: 0rem auto;">

            <label for='yes_radio' id='yes-lbl'>-</label>
            <input type="radio" value="negative" name='cbx_interval' id='yes_radio' class='yes_radio'>

            <label for='maybe_radio' id='maybe-lbl' style="left: 33.33333%; -webkit-animation-delay: 2s; -o-animation-delay: 2s; -moz-animation-delay: 2s; animation-delay: 2s;"></label>
            <input type="radio" value="empty" name='cbx_interval' id='maybe_radio' class='maybe_radio' style="left: 33.33333%;">

            <label for='no_radio' id='no-lbl' style="left: 66.66667%; -webkit-animation-delay: 4s; -o-animation-delay: 4s; -moz-animation-delay: 4s; animation-delay: 4s;">+</label>
            <input type="radio" value="positive" name='cbx_interval' id='no_radio' class='no_radio' style="left: 66.66667%;">

            <div class="toggle"></div>
          </div>
        </div>
      </div>
    </div>

    <br>
    <div class="row">

      <div class="col-lg-12">
        <label class="control-label">Sample range:</label>
        Values ​​within the range must be entered, the same minimum and maximum value is not allowed
        <div class="panel-range">

          <div class="col-6 col-sm-6 col-md-6 col-lg-8">
            <div class="slider" data-slider="data-slider" data-initial-start="1" data-start="1" data-initial-end=<?= $rpea_sample_count ?> data-end=<?= $rpea_sample_count ?> data-step="1">

              <span class="slider-handle" data-slider-handle="" role="slider" tabindex="1" aria-controls="sliderOutputInit" aria-valuemin="1" aria-valuemax=<?= $rpea_sample_count ?> aria-orientation="horizontal" aria-valuenow="1">
              </span>

              <span class="slider-fill" data-slider-fill="">
              </span>

              <span class="slider-handle" data-slider-handle="" role="slider" tabindex="1" aria-controls="sliderOutputEnd" aria-valuemin="1" aria-valuemax=<?= $rpea_sample_count ?> aria-orientation="horizontal" aria-valuenow=<?= $rpea_sample_count ?>>
              </span>

            </div>
          </div>

          <div class="col-3 col-sm-3 col-md-3 col-lg-2">
            <input type="number" id="sliderOutputInit" name="sliderOutputInit" max=<?= $rpea_sample_count ?> min="1" step="1" class='form-control'>
          </div>

          <div class="col-3 col-sm-3 col-md-3 col-lg-2">
            <input type="number" id="sliderOutputEnd" name="sliderOutputEnd" max=<?= $rpea_sample_count ?> min="1" step="1" class='form-control'>
          </div>

        </div>
      </div>
    </div>

  </div>


  <div class="card-footer">
    <div class="row">
      <div class="col-lg-6">
        <div class="text-left">
          <label>
            <input type="checkbox" name="cbx_reprocess" checked>
            <span class="control-label">Save to reprocess</span>
          </label>
        </div>
      </div>
      <div class="col-md-6">
        <div class="btn-group pull-right">
          <?= Html::a(
            'Finish',
            [
              'finish-sample-results',
              'request_id' => Yii::$app->request->get('request_id'),
              'num_order_id' => Yii::$app->request->get('num_order_id'),
              'essay_id' => Yii::$app->request->get('essay_id'),
              'composite_sample_type_id' => Yii::$app->request->get('composite_sample_type_id'),
              'request_process_essay_id' => Yii::$app->request->get('request_process_essay_id'),
            ],
            [
              'id' => 'finish_btn_id',
              'name' => 'finish_btn_name',
              'class' => 'btn btn-success',
              'title'        => 'Finish sample results',
              'data' => [
                'confirm' => 'Are you completely sure about finishing the results record?',
              ],
            ]
          );
          ?>
          <?= Html::a(
            'Save',
            [
              'add-sample-result-bulk',
              'request_id' => Yii::$app->request->get('request_id'),
              'num_order_id' => Yii::$app->request->get('num_order_id'),
              'essay_id' => Yii::$app->request->get('essay_id'),
              'composite_sample_type_id' => Yii::$app->request->get('composite_sample_type_id'),
              'request_process_essay_id' => Yii::$app->request->get('request_process_essay_id'),
            ],
            [
              'id' => 'save_btn_id',
              'name' => 'save_btn_name',
              'class' => 'btn btn-primary',
              'title'        => 'Save sample results',
              'data-method'  => 'post',
            ]
          ); ?>
        </div>
      </div>
    </div>
  </div>
  <?php ActiveForm::end() ?>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/foundation/6.2/foundation.min.js"></script>
<script id="rendered-js">
  $(document).foundation();
</script>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;
use Yii;
?>

<style>
  .st-info {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;

    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;

    color: #fff;
    background-color: #337ab7 !important;
  }

  .st-positive {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;

    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;

    color: #fff;
    background-color: #dd4b39 !important;
  }

  .st-negative {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;

    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;

    color: #fff;
    background-color: #00a65a !important;
  }

  .st-empty {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;
    background-color: #c1c1c1 !important;
  }

  .st-active {
    color: #000;
    font-weight: 700;
    font-size: 1.2em;
  }

  .table-bordered>tbody>tr>td {
    text-align: center;
  }

  .label-positive {
    background-color: #dd4b39 !important;
  }

  .label-negative {
    background-color: #008d4c !important;
  }

  .label-buffer {
    background-color: #686868 !important;
  }
</style>


<div class="card card-outline card-secondary data-results">
  <div class="card-header">
    <h3 class="card-title text-blue-link"><?= $title ?></h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12" style="margin-bottom: 1em;">

        <?php Pjax::begin();
        echo GridView::widget([
          'dataProvider' => $dataProvider,
          'layout' => "{items}{summary}{pager}",
          'columns' => $dataColumns,
        ]);
        Pjax::end(); ?>


      </div>
    </div>
  </div>
  <?php if ($is_finalisable_in_mobile_essays && $is_ready_to_finish) { ?>

    <?php $form = ActiveForm::begin(); ?>


    <div class="card-footer">
      <div class="row">
        <div class="col-lg-6">
          <div class="text-left">
            <label>
              <input type="checkbox" name="cbx_reprocess" checked>
              <span class="control-label">Save to reprocess</span>
            </label>
          </div>
        </div>
        <div class="col-md-6">
          <div class="pull-right">
            <?= Html::a(
              'Finish',
              [
                'finish-sample-results',
                'request_id' => Yii::$app->request->get('request_id'),
                'num_order_id' => Yii::$app->request->get('num_order_id'),
                'essay_id' => Yii::$app->request->get('essay_id'),
                'composite_sample_type_id' => Yii::$app->request->get('composite_sample_type_id'),
                'request_process_essay_id' => Yii::$app->request->get('request_process_essay_id'),
              ],
              [
                'id' => 'finish_btn_id',
                'name' => 'finish_btn_name',
                'class' => 'btn btn-success',
                'title'        => 'Finish sample results',
                'data' => [
                  'confirm' => 'Are you completely sure about finishing the results record?',
                ],
              ]
            );
            ?>
          </div>
        </div>
      </div>
    </div>

    <?php ActiveForm::end() ?>

  <?php } ?>
</div>
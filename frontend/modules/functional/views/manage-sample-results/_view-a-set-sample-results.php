<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use Yii;
?>
<style>
  .panel-agents {
    padding: 10px;
    border: 1px solid #ccc !important;
    border-radius: 10px;
    min-height: 210px;
    margin: 0px;
    overflow-y: scroll;
  }

  .content-result {
    padding: 4px;
    margin: 2px;
    border: 1px solid #ccc !important;
    border-radius: 10px;
  }

  .cut-text {
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }

  .text-results {
    font-size: clamp(0.95rem, 1vw, 1.25rem);
  }
</style>

<div class="card card-outline card-secondary">

  <?php Pjax::begin(); ?>
  <?php $form = ActiveForm::begin(
    [
      'options' => [
        'data-pjax' => true,
        'id' => 'manage-sample-results'
      ]
    ]
  );
  ?>
  <div class="card-header">
    <h3 class="card-title text-blue-link">Set Results</h3>
  </div>

  <div class="card-body">
    <div class="row">
      <div class="col-md-4 col-lg-3">
        <label class="control-label">Sample List:</label>
        <?= Select2::widget(
          [
            'id' => 'select_sample',
            'name' => 'rpe_activity_sample_id',
            'data' => $rpea_sample_list,
            'value' => $sample,
            'options' => [
              'placeholder' => 'Select...',
            ],
            'pluginEvents' => [
              'select2:select' => 'function() { 
                // var data = document.getElementById("select_sample").value;
                // console.log(data);
                document.getElementById("manage-sample-results").submit(); 
              }',
            ]
          ]
        ); ?>
      </div>

      <div class="col-md-8 col-lg-9">
        <label class="control-label">Agent List:</label>
        <div class="row panel-agents">
          <?php foreach ($rd_list as  $key => $value) {
            $id = $value['id'];
            $rd_id = $value['rd_id'];
            $agent_name = $value['agent_name'];
            $result = $value['result'];
            $version = $value['version'];
          ?>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
              <div class="row content-result">

                <?php
                $checked_negative = "";
                $checked_positive = "";
                $checked_empty = "";
                $switch_option = is_null($result) ? 'default'  : $result;

                switch ($switch_option) {
                  case 'negative':
                    $checked_negative = "checked";
                    break;
                  case 'positive':
                    $checked_positive = "checked";
                    break;
                  case 'empty':
                    $checked_empty = "checked";
                    break;
                  default:
                    break;
                } ?>

                <div class="text-center">
                  <span title="<?= $agent_name ?>" class="text-results cut-text">
                    <?= (strlen($agent_name) > 20) ? (substr($agent_name, 0, 17) . '...') : ($agent_name) ?>
                  </span>
                </div>
                <div class="wrapper_aux">

                  <label for=<?= 'yes_radio' . $id ?> id=<?= 'yes-lbl' . $id ?>>-</label>
                  <input type="radio" value="negative" name=<?= 'rpe_agent_list_id[' . $id . '|' . $rd_id . ']' ?> id=<?= 'yes_radio' . $id ?> class='yes_radio' <?= $checked_negative ?>>

                  <label for=<?= 'maybe_radio' . $id ?> id=<?= 'maybe-lbl' . $id ?> style="left: 33.33333%; -webkit-animation-delay: 2s; -o-animation-delay: 2s; -moz-animation-delay: 2s; animation-delay: 2s;"></label>
                  <input type="radio" value="empty" name=<?= 'rpe_agent_list_id[' . $id . '|' . $rd_id . ']' ?> id=<?= 'maybe_radio' . $id ?> class='maybe_radio' style="left: 33.33333%;" <?= $checked_empty ?>>

                  <label for=<?= 'no_radio' . $id ?> id=<?= 'no-lbl' . $id ?> style="left: 66.66667%; -webkit-animation-delay: 4s; -o-animation-delay: 4s; -moz-animation-delay: 4s; animation-delay: 4s;">+</label>
                  <input type="radio" value="positive" name=<?= 'rpe_agent_list_id[' . $id . '|' . $rd_id . ']' ?> id=<?= 'no_radio' . $id ?> class='no_radio' style="left: 66.66667%;" <?= $checked_positive ?>>

                  <div class="toggle"></div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>


  <div class="card-footer">
    <div class="row">
      <div class="col-lg-6">
        <div class="text-left">
          <label>
            <input type="checkbox" name="cbx_reprocess" checked>
            <span class="control-label" id="">Save to reprocess</span>
          </label>
        </div>
      </div>
      <div class="col-md-6">
        <div class="btn-group pull-right">
          <?= Html::a(
            'Finish',
            [
              'finish-sample-results',
              'request_id' => Yii::$app->request->get('request_id'),
              'num_order_id' => Yii::$app->request->get('num_order_id'),
              'essay_id' => Yii::$app->request->get('essay_id'),
              'composite_sample_type_id' => Yii::$app->request->get('composite_sample_type_id'),
              'request_process_essay_id' => Yii::$app->request->get('request_process_essay_id'),
            ],
            [
              'id' => 'finish_btn_id',
              'name' => 'finish_btn_name',
              'class' => 'btn btn-success',
              'title'        => 'Finish sample results',
              'data' => [
                'confirm' => 'Are you completely sure about finishing the results record?',
              ],
            ]
          );
          ?>
          <?= Html::a(
            'Save',
            [
              'add-sample-result',
              'request_id' => Yii::$app->request->get('request_id'),
              'num_order_id' => Yii::$app->request->get('num_order_id'),
              'essay_id' => Yii::$app->request->get('essay_id'),
              'composite_sample_type_id' => Yii::$app->request->get('composite_sample_type_id'),
              'request_process_essay_id' => Yii::$app->request->get('request_process_essay_id'),
              'sample' => $sample
            ],
            [
              'id' => 'save_btn_id',
              'name' => 'save_btn_name',
              'class' => 'btn btn-primary',
              'title'        => 'Save sample results',
              'data-method'  => 'post',
            ]
          ); ?>
        </div>
      </div>
    </div>
  </div>
  <?php ActiveForm::end() ?>
  <?php Pjax::end(); ?>
</div>
<?php

use yii\helpers\Html;
use kartik\select2\Select2;

$this->registerJS(
  "$('#c_options_radios2').on('click', 
    function() { 
      document.getElementById('c_sender_name_div').style.display = 'none';
      document.getElementById('c_sender_name_aux_div').style.display = 'initial';

      $('#c_sender_name_id').select2('val', ' ', true);
      document.getElementById('c_sender_name_aux_id').value = '';
      document.getElementById('c_institution_id').value = '';
      document.getElementById('c_address_id').value = '';
      document.getElementById('c_country_id').value = '';
    }
  );
  
  $('#c_options_radios1').on('click', 
    function() { 
      document.getElementById('c_sender_name_div').style.display = 'initial';
      document.getElementById('c_sender_name_aux_div').style.display = 'none';

      $('#c_sender_name_id').select2('val', ' ', true);
      document.getElementById('c_sender_name_aux_id').value = '';
      document.getElementById('c_institution_id').value = '';
      document.getElementById('c_address_id').value = '';
      document.getElementById('c_country_id').value = '';
    }
  );"

);
?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Sender details (introduction)</h3>
  </div>
  <div class="card-body" style="min-height: 615px">
    <div class="row">
      <div class="col-lg-12">

        <div class="radio">
          <label>
            <input type="radio" name="c_options" id="c_options_radios1" value="option1" checked="">
            Name from list
          </label>

          <label style="margin-left: 20px;">
            <input type="radio" name="c_options" id="c_options_radios2" value="option2">
            New name
          </label>
        </div>

        <!-- ------------------------------------------------------------ -->
        <div id="c_sender_name_div" style="display:initial">
          <label class="control-label">Name</label>
          <?= Select2::widget(
            [
              'id' => 'c_sender_name_id',
              'name' => 'c_sender_name',
              'data' => $name_data,
              'value' => $c_sender_name_value,
              'options' => ['placeholder' => 'Select name ...',],
              'pluginEvents' => [
                'select2:select' => 'function() {
                  $( "#c_institution_id" ).val("loading ...");
                  $( "#c_address_id" ).val("loading ...");
                  $( "#c_country_id" ).val("loading ...");

                  $.post("./name-institution?id="+$(this).val(),function( data ){$( "#c_institution_id" ).val(data);});
                  $.post("./name-address?id="+$(this).val(),function( data ){$( "#c_address_id" ).val(data);});
                  $.post("./name-country?id="+$(this).val(),function( data ){$( "#c_country_id" ).val(data);});
                }',
              ]
            ]
          );
          ?>
        </div>

        <div id="c_sender_name_aux_div" style="display:none">
          <label class="control-label">Name</label>
          <?= Html::input('text', 'c_sender_name_aux', '', $options = ['class' => 'form-control', 'id' => 'c_sender_name_aux_id']) ?>
        </div>

        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="c_institution_div" style="display:initial">
          <label class="control-label">Institution</label>
          <?= Html::input('text', 'c_institution', '', $options = ['class' => 'form-control', 'id' => 'c_institution_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="c_address_div" style="display:initial">
          <label class="control-label">Address</label>
          <?= Html::input('text', 'c_address', '', $options = ['class' => 'form-control', 'id' => 'c_address_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="c_country_div" style="display:initial">
          <label class="control-label">Country</label>
          <?= Html::input('text', 'c_country', '', $options = ['class' => 'form-control', 'id' => 'c_country_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="c_international_phytosanitary_certificate_div" style="display:initial">
          <label class="control-label">International phytosanitary certificate</label>
          <?= Html::input('text', 'c_international_phytosanitary_certificate', '', $options = ['class' => 'form-control', 'id' => 'c_international_phytosanitary_certificate_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="c_quarantine_custody_number_div" style="display:initial">
          <label class="control-label">Quarantine custody number</label>
          <?= Html::input('text', 'c_quarantine_custody_number', '', $options = ['class' => 'form-control', 'id' => 'c_quarantine_custody_number_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="c_requirements_sheet_folio_number_div" style="display:initial">
          <label class="control-label">Requirements Sheet folio number</label>
          <?= Html::input('text', 'c_requirements_sheet_folio_number', '', $options = ['class' => 'form-control', 'id' => 'c_requirements_sheet_folio_number_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="c_total_shipping_weight_div" style="display:initial">
          <label class="control-label">Total shipping weight (kg)</label>
          <?= Html::input('text', 'c_total_shipping_weight', '', $options = ['class' => 'form-control', 'id' => 'c_total_shipping_weight_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
      </div>
    </div>
  </div>
</div>
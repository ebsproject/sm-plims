<?php

use yii\helpers\Html;

?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Recipient data (introduction)</h3>
  </div>
  <div class="card-body" style="min-height: 615px">
    <div class="row">
      <div class="col-lg-12">
        <!-- ------------------------------------------------------------ -->
        <div id="c_recipient_name_div" style="display:initial">
          <label class="control-label">Name</label>
          <?= Html::input('text', 'c_recipient_name', '', $options = ['class' => 'form-control', 'id' => 'c_recipient_name_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="c_program_div" style="display:initial">
          <label class="control-label">Program</label>
          <?= Html::input('text', 'c_program', '', $options = ['class' => 'form-control', 'id' => 'c_program_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="c_account_number_div" style="display:initial">
          <label class="control-label">Account number</label>
          <?= Html::input('text', 'c_account_number', '', $options = ['class' => 'form-control', 'id' => 'c_account_number_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
      </div>
    </div>
  </div>
</div>
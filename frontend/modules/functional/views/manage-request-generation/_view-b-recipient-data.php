<?php

use yii\helpers\Html;
use kartik\select2\Select2;


$this->registerJS(
  "$('#b_options_radios2').on('click', 
    function() { 
      document.getElementById('b_recipient_name_div').style.display = 'none';
      document.getElementById('b_recipient_name_aux_div').style.display = 'initial';

      $('#b_recipient_name_id').select2('val', ' ', true);
      document.getElementById('b_recipient_name_aux_id').value = '';
      document.getElementById('b_institution_id').value = '';
      document.getElementById('b_address_id').value = '';
      document.getElementById('b_country_id').value = '';
    }
  );
  
  $('#b_options_radios1').on('click', 
    function() { 
      document.getElementById('b_recipient_name_div').style.display = 'initial';
      document.getElementById('b_recipient_name_aux_div').style.display = 'none';

      $('#b_recipient_name_id').select2('val', ' ', true);
      document.getElementById('b_recipient_name_aux_id').value = '';
      document.getElementById('b_institution_id').value = '';
      document.getElementById('b_address_id').value = '';
      document.getElementById('b_country_id').value = '';
    }
  );"
);
?>

<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Recipient data (shipment)</h3>
  </div>
  <div class="card-body" style="min-height: 615px">

    <div class="row">
      <div class="col-lg-12">

        <div class="radio">
          <label>
            <input type="radio" name="b_options" id="b_options_radios1" value="option1" checked="">
            Name from list
          </label>

          <label style="margin-left: 20px;">
            <input type="radio" name="b_options" id="b_options_radios2" value="option2">
            New name
          </label>
        </div>

        <!-- ------------------------------------------------------------ -->
        <div id="b_recipient_name_div" style="display:initial">
          <label class="control-label">Name</label>
          <?= Select2::widget(
            [
              'id' => 'b_recipient_name_id',
              'name' => 'b_recipient_name',
              'data' => $name_data,
              'value' => $b_recipient_name_value,
              'options' => ['placeholder' => 'Select name ...',],
              'pluginEvents' => [
                'select2:select' => 'function() {
                  $( "#b_institution_id" ).val("loading ...");
                  $( "#b_address_id" ).val("loading ...");
                  $( "#b_country_id" ).val("loading ...");

                  $.post("./name-institution?id="+$(this).val(),function( data ){$( "#b_institution_id" ).val(data);});
                  $.post("./name-address?id="+$(this).val(),function( data ){$( "#b_address_id" ).val(data);});
                  $.post("./name-country?id="+$(this).val(),function( data ){$( "#b_country_id" ).val(data);});
                }',
              ]
            ]
          );
          ?>
        </div>

        <div id="b_recipient_name_aux_div" style="display:none">
          <label class="control-label">Name</label>
          <?= Html::input('text', 'b_recipient_name_aux', '', $options = ['class' => 'form-control', 'id' => 'b_recipient_name_aux_id']) ?>
        </div>

        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="b_institution_div" style="display:initial">
          <label class="control-label">Institution</label>
          <?= Html::input('text', 'b_institution', '', $options = ['class' => 'form-control', 'id' => 'b_institution_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="b_address_div" style="display:initial">
          <label class="control-label">Address</label>
          <?= Html::input('text', 'b_address', '', $options = ['class' => 'form-control', 'id' => 'b_address_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="b_country_div" style="display:initial">
          <label class="control-label">Country</label>
          <?= Html::input('text', 'b_country', '', $options = ['class' => 'form-control', 'id' => 'b_country_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
      </div>
    </div>
  </div>
</div>
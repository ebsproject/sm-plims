<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Distribution</h3>
  </div>
  <div class="card-body" style="min-height: 615px">
    <div class="row">
      <div class="col-lg-12">
        <!-- ------------------------------------------------------------ -->
        <div id="crop_div" style="display:initial">
          <label class="control-label">Crop </label>
          <?= Select2::widget(
            [
              'id' => 'crop_id',
              'name' => 'crop',
              'data' => $crop_data,
              'value' => $crop_value,
              'options' => [
                'placeholder' => 'Select crop ...',
                'onChange' => '
                  if( $(this).val() == 4 ){
                    document.getElementById("small_grain_cereal_div").style.display = "initial";
                  }
                  else {
                    
                    $("#small_grain_cereal_id").select2("val", " ", true);
                    document.getElementById("small_grain_cereal_div").style.display = "none";
                    
                    document.getElementById("small_grain_cereal_other_id").value = "";
                    document.getElementById("child_crop_other_div").style.display = "none";

                  }
                ',
              ],
            ]
          );
          ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="request_type_div" style="display:initial">
          <label class="control-label">Request Type</label>
          <?= Select2::widget(
            [
              'id' => 'request_type_id',
              'name' => 'request_type',
              'data' => $request_type_data,
              'value' => $request_type_value,
              'options' => [
                'placeholder' => 'Select request type ...',
                'onChange' => '
                  if( $(this).val() == 11 ){
                    document.getElementById("distribution_id").style.display = "initial";
                    document.getElementById("introduction_id").style.display = "none";

                    $("#b_recipient_name_id").select2("val", " ", true);
                    document.getElementById("b_recipient_name_aux_id").value = "";
                    document.getElementById("b_institution_id").value = "";
                    document.getElementById("b_address_id").value = "";
                    document.getElementById("b_country_id").value = "";
                    document.getElementById("b_sender_name_id").value = "";
                    document.getElementById("b_program_id").value = "";
                    document.getElementById("b_account_number_id").value = "";
                    

                    $("#c_sender_name_id").select2("val", " ", true);
                    document.getElementById("c_sender_name_aux_id").value = "";
                    document.getElementById("c_institution_id").value = "";
                    document.getElementById("c_address_id").value = "";
                    document.getElementById("c_country_id").value = "";
                    document.getElementById("c_international_phytosanitary_certificate_id").value = "";
                    document.getElementById("c_quarantine_custody_number_id").value = "";
                    document.getElementById("c_requirements_sheet_folio_number_id").value = "";
                    document.getElementById("c_total_shipping_weight_id").value = "";
                    document.getElementById("c_recipient_name_id").value = "";
                    document.getElementById("c_program_id").value = "";
                    document.getElementById("c_account_number_id").value = "";
                  } else {
                    document.getElementById("distribution_id").style.display = "none";
                    document.getElementById("introduction_id").style.display = "initial";

                    $("#b_recipient_name_id").select2("val", " ", true);
                    document.getElementById("b_recipient_name_aux_id").value = "";
                    document.getElementById("b_institution_id").value = "";
                    document.getElementById("b_address_id").value = "";
                    document.getElementById("b_country_id").value = "";
                    document.getElementById("b_sender_name_id").value = "";
                    document.getElementById("b_program_id").value = "";
                    document.getElementById("b_account_number_id").value = "";


                    $("#c_sender_name_id").select2("val", " ", true);
                    document.getElementById("c_sender_name_aux_id").value = "";
                    document.getElementById("c_institution_id").value = "";
                    document.getElementById("c_address_id").value = "";
                    document.getElementById("c_country_id").value = "";
                    document.getElementById("c_international_phytosanitary_certificate_id").value = "";
                    document.getElementById("c_quarantine_custody_number_id").value = "";
                    document.getElementById("c_requirements_sheet_folio_number_id").value = "";
                    document.getElementById("c_total_shipping_weight_id").value = "";
                    document.getElementById("c_recipient_name_id").value = "";
                    document.getElementById("c_program_id").value = "";
                    document.getElementById("c_account_number_id").value = "";
                  }
                ',
              ],
            ]
          );
          ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="small_grain_cereal_div" style="display:none">
          <label class="control-label">Small grain cereals</label>
          <?= Select2::widget(
            [
              'id' => 'small_grain_cereal_id',
              'name' => 'small_grain_cereal',
              'data' => $small_grain_cereal_data,
              'size' => Select2::MEDIUM,
              'value' => null,
              'maintainOrder' => true,
              'options' => [
                'placeholder' => 'Select one o more cereals ...',
                'multiple' => true,
                'onChange' => ' 
                  if( $(this).val().includes("Other") ){
                    document.getElementById("child_crop_other_div").style.display = "initial";
                  }
                  else {
                    document.getElementById("child_crop_other_div").style.display = "none";
                  }
                ',
              ],
              // 'pluginOptions' => [
              //   'allowClear' => true
              // ],
            ]
          );
          ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="child_crop_other_div" style="display:none">
          <label class=" control-label">Child Crop Other</label>
          <?= Html::input('text', 'small_grain_cereal_other', '', $options = ['id' => 'small_grain_cereal_other_id', 'class' => 'form-control']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="request_process_other_div" style="display:initial">
          <label class="control-label">Request Process Other</label>
          <?= Select2::widget(
            [
              'id' => 'request_process_other_id',
              'name' => 'request_process_other',
              'data' => [
                'Monitoring of stations' => 'Monitoring of stations',
                'Germplasm Bank Conservation' => 'Germplasm Bank Conservation',
                'External services' => 'External services',
                'Interlaboratory compararisons' => 'Interlaboratory compararisons',
              ],
              'value' => null,
              'options' => [
                'placeholder' => 'Select request process ...',
              ],
            ]
          );
          ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="registration_out_of_the_system_div" style="display:initial">
          <label class="control-label">Registration Out Of The System</label>
          <?= Select2::widget(
            [
              'id' => 'registration_out_of_the_system_id',
              'name' => 'registration_out_of_the_system',
              'data' =>  [
                'Previously analyzed material' => 'Previously analyzed material',
                'Shipment for GMO analysis' => 'Shipment for GMO analysis',
                'National maize shipments' => 'National maize shipments',
                'Shipment for laboratory analysis' => 'Shipment for laboratory analysis',
              ],
              'value' => null,
              'options' => [
                'placeholder' => 'Select registration ...',
              ],
            ]
          );
          ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="material_type_div" style="display:initial">
          <label class="control-label">Material type</label>
          <?= Select2::widget(
            [
              'id' => 'material_type_id',
              'name' => 'material_type',
              'data' => $material_type_data,
              'value' => $material_type_value,
              'options' => [
                'placeholder' => 'Select material type ...',
              ],
            ]
          );
          ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="material_unit_div" style="display:initial">
          <label class="control-label">Material unit</label>
          <?= Select2::widget(
            [
              'id' => 'material_unit_id',
              'name' => 'material_unit',
              'data' => $material_unit_data,
              'value' => $material_unit_value,
              'options' => [
                'placeholder' => 'Select material unit ...',
              ],
            ]
          );
          ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->

        <!-- ------------------------------------------------------------ -->
        <div id="source_database_div" style="display:initial">
          <label class="control-label">Source database</label>
          <?= Select2::widget(
            [
              'id' => 'source_database_id',
              'name' => 'source_database',
              'data' => ['SD_1', 'SD_2', 'SD_3'],
              'value' => [0],
              'options' => [
                'placeholder' => 'Select source db ...',
              ],
            ]
          );
          ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
      </div>
    </div>
  </div>
</div>
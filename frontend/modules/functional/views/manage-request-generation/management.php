<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Manage Request Generation';
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
  .callout a {
    color: #28a745;
    text-decoration: none;
  }


  .callout a:hover {
    color: #218838;
    text-decoration: none;
  }
</style>
<div class="management">
  <!-- #region BACK -->
  <div class="row">
    <div class="col-lg-12">
      <h1><?= Html::encode($this->title) ?></h1>
    </div>
  </div>
  <!-- end region -->

  <?php $form = ActiveForm::begin(); ?>

  <div class="row">
    <div class="col-lg-4 col-md-4">
      <?= $this->render('_view-a-distribution', [
        'request_type_data' => $request_type_data,
        'request_type_value' => $request_type_value,
        'crop_data' => $crop_data,
        'crop_value' => $crop_value,
        'small_grain_cereal_data' => $small_grain_cereal_data,
        'material_type_data' => $material_type_data,
        'material_type_value' => $material_type_value,
        'material_unit_data' => $material_unit_data,
        'material_unit_value' => $material_unit_value,
      ])  ?>
    </div>

    <div class="col-lg-8 col-md-8 no-padding" id="distribution_id" style="display:initial">
      <div class="col-lg-6 col-md-6">
        <?= $this->render('_view-b-recipient-data', [
          'name_data' =>   $name_data,
          'b_recipient_name_value' =>   $b_recipient_name_value,
        ])  ?>
      </div>

      <div class="col-lg-6 col-md-6">
        <?= $this->render('_view-b-sender-details', [])  ?>
      </div>
    </div>

    <div class="col-lg-8 col-md-8 no-padding" id="introduction_id" style="display:none">
      <div class="col-lg-6 col-md-6">
        <?= $this->render('_view-c-recipient-data', [])  ?>
      </div>

      <div class="col-lg-6 col-md-6">
        <?= $this->render('_view-c-sender-details', [
          'name_data' =>   $name_data,
          'c_sender_name_value' =>   $c_sender_name_value,
        ])  ?>
      </div>

    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <?php //echo $this->render('_view-d-material', [])  
      ?>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="callout callout-secondary">
        <div class="row">
          <div class="col-lg-8">
            <span>
              <i class="fa fa-info"></i>
              Material template, the columns seed_name*, seed_code, package_label, package_code, germplasm_name and material_unit_qty* will be considered.
            </span>
            <a href="<?= Url::base() ?>/documents/templates/material_template.xlsx" class="pull-right">
              <i class="fa fa-file-excel-o"></i> Material template
            </a>
            <br /><br />
            <small style="font-style: italic;">
              * Required field value
            </small>
          </div>

          <div class="col-lg-4">
            <div class="input-group" title="File extensions: xls, xlsx">
              <?= $form->field($model_upload_material_list, 'mixFiles[]')->fileInput(['multiple' => true]) ?>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="col-lg-12">
      <div class="btn-group pull-right">
        <?= Html::a('Cancel', ['manage-request/index',], ['class' => 'btn btn-default']) ?>
        <?= Html::a(
          'Generate Request',
          [
            'save',
          ],
          [
            'class' => 'btn btn-primary',
            'title'        => 'Generate new request',
            'data-method'  => 'post',
          ]
        ); ?>
      </div>
    </div>
  </div>
  <br />
  <?php ActiveForm::end() ?>
</div>
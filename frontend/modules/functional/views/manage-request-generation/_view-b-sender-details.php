<?php

use yii\helpers\Html;

?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Sender details (shipment)</h3>
  </div>
  <div class="card-body" style="min-height: 615px">
    <div class="row">
      <div class="col-lg-12">
        <!-- ------------------------------------------------------------ -->
        <div id="b_sender_name_div" style="display:initial">
          <label class="control-label">Name</label>
          <?= Html::input('text', 'b_sender_name', '', $options = ['class' => 'form-control', 'id' => 'b_sender_name_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="b_program_div" style="display:initial">
          <label class="control-label">Program</label>
          <?= Html::input('text', 'b_program', '', $options = ['class' => 'form-control', 'id' => 'b_program_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
        <div id="b_account_number_div" style="display:initial">
          <label class="control-label">Account number</label>
          <?= Html::input('text', 'b_account_number', '', $options = ['class' => 'form-control', 'id' => 'b_account_number_id']) ?>
        </div>
        <p></p>
        <!-- ------------------------------------------------------------ -->
      </div>
    </div>
  </div>
</div>
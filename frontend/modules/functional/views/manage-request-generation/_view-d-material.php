<?php

use yii\helpers\Html;
?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Material</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class=" col-lg-12">
        <label class="control-label">Material</label>
        <?= Html::textArea(
          'consolidated_material',
          null,
          [
            'class' => 'form-control',
            'rows' => 20
          ]
        ) ?>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <div class="row">
      <div class="col-lg-12">
        <div class="btn-group pull-right">
          <?= Html::a('Cancel', ['manage-request/index',], ['class' => 'btn btn-default']) ?>
          <?= Html::a(
            'Generate Request',
            [
              'save',
            ],
            [
              'class' => 'btn btn-primary',
              'title'        => 'Generate new request',
              'data-method'  => 'post',
            ]
          ); ?>
        </div>
      </div>
    </div>
  </div>
</div>
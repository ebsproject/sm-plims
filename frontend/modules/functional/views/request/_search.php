<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\functional\models\RequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'request_id') ?>

    <?= $form->field($model, 'year_code') ?>

    <?= $form->field($model, 'sequential_code') ?>

    <?= $form->field($model, 'request_code') ?>

    <?= $form->field($model, 'details') ?>

    <?php // echo $form->field($model, 'diagnostic') ?>

    <?php // echo $form->field($model, 'agent_qty') ?>

    <?php // echo $form->field($model, 'material_qty') ?>

    <?php // echo $form->field($model, 'total_cost') ?>

    <?php // echo $form->field($model, 'request_type_id') ?>

    <?php // echo $form->field($model, 'request_status_id') ?>

    <?php // echo $form->field($model, 'request_payment_id') ?>

    <?php // echo $form->field($model, 'pay_number') ?>

    <?php // echo $form->field($model, 'requested_user_id') ?>

    <?php // echo $form->field($model, 'owner_user_id') ?>

    <?php // echo $form->field($model, 'registered_by') ?>

    <?php // echo $form->field($model, 'registered_at') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'sample_qty') ?>

    <?php // echo $form->field($model, 'material_unit_qty') ?>

    <?php // echo $form->field($model, 'is_deleted')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

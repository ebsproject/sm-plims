<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\functional\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Request', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'request_id',
            'year_code',
            'sequential_code',
            'request_code',
            'details',
            //'diagnostic',
            //'agent_qty',
            //'material_qty',
            //'total_cost',
            //'request_type_id',
            //'request_status_id',
            //'request_payment_id',
            //'pay_number',
            //'requested_user_id',
            //'owner_user_id',
            //'registered_by',
            //'registered_at',
            //'deleted_by',
            //'deleted_at',
            //'status',
            //'sample_qty',
            //'material_unit_qty',
            //'is_deleted:boolean',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Request $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'request_id' => $model->request_id]);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
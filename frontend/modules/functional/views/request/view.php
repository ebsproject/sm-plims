<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\functional\models\Request */

$this->title = $model->request_id;
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="request-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'request_id' => $model->request_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'request_id' => $model->request_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'request_id',
            'year_code',
            'sequential_code',
            'request_code',
            'details',
            'diagnostic',
            'agent_qty',
            'material_qty',
            'total_cost',
            'request_type_id',
            'request_status_id',
            'request_payment_id',
            'pay_number',
            'requested_user_id',
            'owner_user_id',
            'registered_by',
            'registered_at',
            'deleted_by',
            'deleted_at',
            'status',
            'sample_qty',
            'material_unit_qty',
            'is_deleted:boolean',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\functional\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'year_code')->textInput() ?>

    <?= $form->field($model, 'sequential_code')->textInput() ?>

    <?= $form->field($model, 'request_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'diagnostic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agent_qty')->textInput() ?>

    <?= $form->field($model, 'material_qty')->textInput() ?>

    <?= $form->field($model, 'total_cost')->textInput() ?>

    <?= $form->field($model, 'request_type_id')->textInput() ?>

    <?= $form->field($model, 'request_status_id')->textInput() ?>

    <?= $form->field($model, 'request_payment_id')->textInput() ?>

    <?= $form->field($model, 'pay_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'requested_user_id')->textInput() ?>

    <?= $form->field($model, 'owner_user_id')->textInput() ?>

    <?= $form->field($model, 'registered_by')->textInput() ?>

    <?= $form->field($model, 'registered_at')->textInput() ?>

    <?= $form->field($model, 'deleted_by')->textInput() ?>

    <?= $form->field($model, 'deleted_at')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sample_qty')->textInput() ?>

    <?= $form->field($model, 'material_unit_qty')->textInput() ?>

    <?= $form->field($model, 'is_deleted')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
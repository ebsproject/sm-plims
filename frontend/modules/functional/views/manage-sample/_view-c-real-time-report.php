<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

$this->title = 'Real Time Report';
?>
<style>
  .table-bordered {
    text-align: center;
  }
</style>

<div class="real-time-report">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-secondary">
        <div class="card-header">
          <h3 class="card-title text-blue-link"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <?php Pjax::begin(); ?>
              <?= GridView::widget([
                'dataProvider' => $dataProviderH,
                // 'filterModel' => $filterModelH,
                'rowOptions' => function ($model) {
                  return ['class' => $model->materialStatus->class_details];
                },
                'layout' => "{items}{summary}{pager}",
                'columns' => [
                  ['class' => 'yii\grid\SerialColumn'],
                  'num_order',
                  'short_name',
                  'material_code_a',
                  [
                    'attribute' => 'crop_id',
                    'value' =>  'crop.short_name',
                  ],
                  [
                    'attribute' => 'material_type_id',
                    'value' =>  'materialType.short_name',
                  ],
                  [
                    'attribute' => 'material_unit_id',
                    'value' =>  'materialUnit.short_name',
                  ],
                  'material_unit_qty',
                  'origin',
                  'historical_data_version',

                  [
                    'attribute' => 'registered_by',
                    'value' =>  'registeredBy.username',
                  ],
                  [
                    'attribute' => 'registered_at',
                    'format' => ['date', 'php:d/m/Y']
                  ],
                  [
                    'attribute' => 'material_status_id',
                    'value' =>  'materialStatus.short_name',
                  ],
                  'status',
                ],
              ]); ?>
              <?php Pjax::end(); ?>
            </div>
          </div>
        </div>

        <div class="card-footer text-left">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <?= Html::a(
                '<i class="fa fa-file-excel-o"></i> Download .csv',
                [
                  'export-historical',
                  'composite_sample_group_id' => $composite_sample_group_id,
                  'extension' => 'CSV'
                ],
                [
                  'class' => 'btn btn-default pull-right',
                  'data-method' => 'post',
                ]
              ) ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
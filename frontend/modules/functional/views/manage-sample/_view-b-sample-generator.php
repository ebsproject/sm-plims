<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
use yii\widgets\MaskedInput;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title =  'Sample Generator';
$info_1 = 'List of materials grouped by order number <b>[Num Order]</b>, each group represents a composite sample. Save the samples to the database if the pool is defined correctly.';
$link_1 = Html::a(
  'Confirmed Material Entry',
  [
    'confirmed-material',
    'request_id' => $request_id,
  ],
  [
    'class' => 'btn btn-success',
    'data-method' => 'post',
  ]
);

$data_material_provider = new ArrayDataProvider(
  [
    'allModels' => $data_material,
    'pagination' => [
      'pageSize' => 0,
    ],
  ]
);

$modal_title = 'List of individual samples';
?>

<div class="sample-generator">

  <?php $form_sample_generator = ActiveForm::begin([
    'id' => 'form-sample-generator',
    'options' => ['class' => 'class-sample-generator'],
  ]) ?>
  <div class="card card-outline card-secondary">
    <!-- HEADER -->
    <div class="card-header">
      <h3 class="card-title text-blue-link"><?= $this->title ?></h3>
    </div>


    <!-- BODY -->
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12">
          <div class="form-group"><i class="icon glyphicon glyphicon-info-sign"></i> <?= $info_1 ?> </div>
        </div>
      </div>



      <div class="row">
        <div class="col-lg-8">
          <div class="row">
            <div class="col-lg-6">
              <span style="font-size: 12px;"></i> Group qty.:</span>
              <div class="input-group">
                <?= MaskedInput::widget(
                  [
                    'id' => 'QtyGroup',
                    'name' => 'txtQtyGroup',
                    'mask' => '9999',
                    'value' =>  $var1
                  ]
                ); ?>
                <span class="input-group-btn">
                  <?= Html::a(
                    'Submit-',
                    [
                      'generate',
                      'request_id' => Yii::$app->request->get('request_id'),
                      'mode' => 'group',
                      'composite_sample_group_id' => $composite_sample_group_id,
                      'request_code' => $request_code,
                    ],
                    [
                      'class' => 'btn btn-default',
                      'data-method'  => 'post',
                    ]
                  ); ?>
                  <?= Html::a(
                    'Submit+',
                    [
                      'generate',
                      'request_id' => Yii::$app->request->get('request_id'),
                      'mode' => 'groups',
                      'composite_sample_group_id' => $composite_sample_group_id,
                      'request_code' => $request_code,
                    ],
                    [
                      'class' => 'btn btn-default',
                      'data-method'  => 'post',
                    ]
                  ); ?>
                </span>

              </div>
            </div>
            <div class="col-lg-6">
              <span style="font-size: 12px;"></i> Material qty. per group:</span>
              <div class="input-group">
                <?= MaskedInput::widget(
                  [
                    'id' => 'QtyMaterial',
                    'name' => 'txtQtyMaterial',
                    'mask' => '9999',
                    'value' => $var2
                  ]
                ); ?>
                <span class="input-group-btn">
                  <?= Html::a(
                    '<i class="glyphicon glyphicon-share-alt"></i> Submit',
                    [
                      'generate',
                      'request_id' => Yii::$app->request->get('request_id'),
                      'mode' => "material",
                      'composite_sample_group_id' => $composite_sample_group_id,
                      'request_code' => $request_code,
                      // '#' => 'grouped_material_id',
                    ],
                    [
                      'class' => 'btn btn-default',
                      'data-method'  => 'post',
                    ]
                  ); ?>
                </span>
              </div>
            </div>
          </div>
          <br>
          <div class="row no-margin">
            <div class="col-lg-12">
              <div class="scroll1">
                <?php Pjax::begin(); ?>
                <?= ListView::widget(
                  [
                    'dataProvider' => $data_material_provider,
                    'itemView' =>  '_item-view-material-group',
                    'layout' => '{items}{pager}{summary}',
                    'pager' => [
                      'firstPageLabel' => 'first',
                      'lastPageLabel' => 'last',
                      'prevPageLabel' => 'previous',
                      'nextPageLabel' => 'next',
                    ],
                  ]
                ); ?>
                <?php Pjax::end(); ?>
              </div>
            </div>

          </div>
        </div>
        <div class="col-lg-4">
          <div class="row">
            <div class="col-lg-12">
              <span style="font-size: 12px;"></i> Interval qty. by order material:</span>
              <div>
                <span>
                  <?= Html::a(
                    '<i class="glyphicon glyphicon-share-alt"></i> Submit',
                    [
                      'generate',
                      'request_id' => Yii::$app->request->get('request_id'),
                      'mode' => 'interval',
                      'composite_sample_group_id' => $composite_sample_group_id,
                      'request_code' => $request_code,
                      // '#' => 'grouped_material_id',
                    ],
                    [
                      'class' => 'btn btn-default',
                      'data-method'  => 'post',
                    ]
                  ); ?>
                </span>
                <br> <br>
                <?= Html::textArea(
                  'txtQtyIntervale',
                  $var3,
                  [
                    'class' => 'form-control',
                    'rows' => 15
                  ]
                ) ?>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-4">
          <br>
          <label class="control-label">Material Reserve: </label>
          <?= Select2::widget(
            [
              'id' => 'material_reserve_id',
              'name' => 'material_reserve',
              'data' => ['simple composite samples', 'individual sample', 'no reservation'],
              'value' => $material_reserve_value,
              'options' => [
                'placeholder' => 'Select reserve ...'
              ],
            ]
          );
          ?>
        </div>
      </div>

    </div>

    <!-- FOOTER -->
    <?php if ($data_material_provider->totalCount > 0) { ?>
      <div class="card-footer text-left">
        <div class="row">
          <div class="col-lg-12">
            <?= Html::a(
              'Save',
              [
                'save',
                'request_id' => Yii::$app->request->get('request_id'),
                'composite_sample_group_id' => $composite_sample_group_id,
                'request_code' => $request_code,
              ],
              [
                'class' => 'pull-right btn btn-primary',
                'data-method'  => 'post',
              ]
            ); ?>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
  <?php ActiveForm::end() ?>
</div>
<?php

use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;

$dataProvider = new ArrayDataProvider(
  [
    'allModels' => $data,
    'pagination' => [
      'pageSize' => 0,
    ],
  ]
);
?>

<div class="row no-margin">
  <div class="col-lg-12 scroll2">
    <?php Pjax::begin(); ?>
    <?= ListView::widget(
      [
        'dataProvider' => $dataProvider,
        'itemView' => '_item-view-ind-sample',
        'layout' => '{items}{pager}{summary}',
        'pager' => [
          'firstPageLabel' => 'first',
          'lastPageLabel' => 'last',
          'prevPageLabel' => 'previous',
          'nextPageLabel' => 'next',
        ],
      ]
    );
    ?>
    <?php Pjax::end(); ?>
  </div>
</div>
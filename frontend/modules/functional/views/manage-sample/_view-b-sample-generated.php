<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
use yii\widgets\MaskedInput;
use yii\widgets\ActiveForm;

$this->title = 'Sample view';
$info_1 = 'List of materials grouped by order number <b>[Num Order]</b>, each group represents a composite sample. Save the samples to the database if the pool is defined correctly.';
$link_1 = Html::a(
  'Confirmed Material Entry',
  [
    'confirmed-material',
    'request_id' => $request_id,
  ],
  [
    'class' => 'btn btn-success',
    'data-method' => 'post',
  ]
);

$data_material_provider = new ArrayDataProvider(
  [
    'allModels' => $data_material,
    'pagination' => [
      'pageSize' => 0,
    ],
  ]
);

$modal_title = 'List of individual samples';
?>

<div class="sample-generator">

  <!-- MODAL-INI -->
  <div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?= Html::encode($modal_title) ?></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="modal-view-content"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- MODAL-END -->


  <div class="card card-outline card-secondary">
    <!-- HEADER -->
    <div class="card-header">
      <h3 class="card-title text-blue-link"><?= $this->title ?></h3>
    </div>


    <!-- BODY -->
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12">

          <h4 class="modal-title">List of composite samples</h4>

        </div>
      </div>
      <?php $form_sample_generator = ActiveForm::begin([
        'id' => 'form-sample-generator',
        'options' => ['class' => 'class-sample-generator'],
      ]) ?>
      <div class="row">
        <div class="col-lg-12">
          <div class="scroll1">
            <?php Pjax::begin(); ?>
            <?= ListView::widget(
              [
                'dataProvider' => $data_material_provider,
                'itemView' =>  '_item-view-com-sample',
                'layout' => '{items}{pager}{summary}',
                'pager' => [
                  'firstPageLabel' => 'first',
                  'lastPageLabel' => 'last',
                  'prevPageLabel' => 'previous',
                  'nextPageLabel' => 'next',
                ],
              ]
            ); ?>
            <?php Pjax::end(); ?>
          </div>
        </div>
      </div>
      <?php ActiveForm::end() ?>
    </div>



  </div>

</div>
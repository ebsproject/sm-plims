<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Material Check-in';
$control = 136;
$deleted = 137;
$info = 'Confirmation of the material entering the laboratory.';
$modal_title = 'Update Material';

$gridColumns = [
  'material_code_a',
  'crop_id',
  'material_type_id',
  'material_unit_id',
  'material_status_id',
];
?>

<style>
  .table-bordered {
    text-align: center;
  }

  .box-default {
    margin-bottom: 0px;
    ;
  }

  .text-orange-link {
    font-size: 1.5rem;
    color: #d39e00;
    margin-bottom: 0.75rem;
    margin-top: 0.75rem;
  }
</style>

<div class="material-check-in">



  <!-- MODAL-INI -->
  <?php $form_sample_generator_modal = ActiveForm::begin(['id' => 'form-view-a-material-check-in-modal', 'options' => ['class' => 'class-view-a-material-check-in-modal'],]) ?>

  <div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?= Html::encode($modal_title) ?></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="modal-update-content"></div>
        </div>
      </div>
    </div>
  </div>

  <?php ActiveForm::end() ?>
  <!-- MODAL-END -->

  <?php $form_sample_generator = ActiveForm::begin([
    'id' => 'form-view-a-material-check-in',
    'options' => ['class' => 'class-view-a-material-check-in'],
  ]) ?>

  <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-secondary">
        <div class="card-header">
          <h3 class="card-title text-blue-link"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <?php Pjax::begin(); ?>
                  <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $filterModel,
                    'rowOptions' => function ($model) {
                      return ['class' => $model->materialStatus->class_details];
                    },
                    'layout' => "{items}{summary}{pager}",
                    'columns' => [
                      ['class' => 'yii\grid\SerialColumn'],
                      [
                        'attribute' => 'num_order',
                      ],

                      'seed_name',
                      'seed_code',
                      // 'package_label',
                      'package_code',
                      'germplasm_name',



                      // 'short_name',
                      // 'material_code_a',
                      [
                        'attribute' => 'crop_id',
                        'value' =>  'crop.short_name',
                      ],
                      [
                        'attribute' => 'material_type_id',
                        'value' =>  'materialType.short_name',
                      ],
                      [
                        'attribute' => 'material_unit_id',
                        'value' =>  'materialUnit.short_name',
                      ],
                      'material_unit_qty',
                      'observation',
                      // 'origin',
                      [
                        'attribute' => 'material_status_id',
                        'value' =>  'materialStatus.short_name',
                      ],
                      [
                        'class' => 'yii\grid\ActionColumn',
                        'header'        => 'Action',
                        'visible'   => $request_status_code == '1.1' ? true : false,
                        'template'      => '{control} {update} {delete}',
                        'headerOptions' => ['min-width' => '80'],
                        'buttons'       => [
                          'control' => function ($url, $model, $key) use ($request_id, $control) {
                            return Html::a(
                              $model->material_status_id == $control ? '<i class="fa fa-undo" title="Reverse action"></i>' : '<i class="fa fa-copyright" title="Convert to control"></i>',
                              [
                                $model->material_status_id == $control ? 'undo' : 'control',
                                'request_id' => $request_id,
                                'material_id' => $key,
                              ],
                              [
                                'class' => 'btn btn-warning btn-xs',
                              ]
                            );
                          },
                          'update' => function ($url, $model, $key) use ($request_id) {
                            return Html::a(
                              '<i class="fa fa-pencil" title="Edit"></i>',
                              ['#'],
                              [
                                'class' => 'btn btn-default btn-xs modal-update-action',
                                'value' => Url::to(
                                  [
                                    'update',
                                    'request_id' => $request_id,
                                    'material_id' => $key,
                                  ]
                                ),
                              ]
                            );
                          },
                          'delete' => function ($url, $model, $key) use ($request_id, $deleted) {
                            return Html::a(
                              $model->material_status_id == $deleted ? '<i class="fa fa-undo" title="Reverse action"></i>' : '<i class="glyphicon glyphicon-trash" title="Remove"></i>',
                              [
                                $model->material_status_id == $deleted ? 'undo' : 'delete',
                                'request_id' => $request_id,
                                'material_id' => $key,
                              ],
                              [
                                'class' => 'btn btn-danger btn-xs',
                              ]
                            );
                          },
                        ],
                      ],
                    ],
                  ]); ?>
                  <?php Pjax::end(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php if ($request_status_code == '1.1') { ?>
          <div class="card-footer text-left">



            <div class="row">
              <div class="col-md-12 col-lg-6">
                <div class="form-group"><i class="icon glyphicon glyphicon-info-sign"></i> <?= $info ?> </div>
              </div>

              <div class="col-md-6 col-lg-4">
                <label class="pull-right">
                  <input type="checkbox" name="cbx_quarantine">
                  <span class="control-label" id="">Save in Stock</span>
                </label>
              </div>

              <div class="col-md-6 col-lg-2">
                <div class="btn-group pull-right">
                  <?= Html::a(
                    'Confirmed Material Entry',
                    [
                      'confirmed-material',
                      'request_id' => $request_id,
                      'composite_sample_group_id' => $composite_sample_group_id,
                    ],
                    [
                      'class' => 'btn btn-success',
                      'data-method' => 'post',
                    ]
                  ) ?>
                </div>
              </div>
            </div>

          </div>
        <?php } else { ?>
          <div class="card-footer text-left">
            <div class="row">
              <div class="col-md-12 col-lg-12">
                <div class="btn-group pull-right">
                  <?= Html::a(
                    '<i class="fa fa-file-excel-o"></i> Download .csv',
                    [
                      'export-report',
                      'composite_sample_group_id' => $composite_sample_group_id,
                      'extension' => 'CSV'
                    ],
                    [
                      'class' => 'btn btn-default',
                      'data-method' => 'post',
                    ]
                  ) ?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>

  <?php ActiveForm::end() ?>
</div>
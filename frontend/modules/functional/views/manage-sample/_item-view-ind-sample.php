<div class="col-lg-2 col-xs-3">
  <div class="small-box bg-orange">
    <div class="inner">
      <b>Individual Sample</b><br>
      <small title="Sample name">Name:</small> <b><?= $model['short_name'] ?></b><br>

      <small>Quantities: <?= $model['material_unit_qty'] ?>units</small><br>

      <small>Order: <?= $model['num_order'] ?></small><br>
    </div>
    <div class="icon">
      <i class="fa fa-archive"></i>
    </div>
  </div>
</div>
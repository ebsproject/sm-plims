<div class="col-lg-3 col-md-4 col-xs-6">
  <div class="small-box bg-yellow">
    <div class="inner">
      <b title="Number of materials per group"><span style="font-size: 1.4em;"><?= $model['group_name'] ?> [<?= $model['material_qty'] ?> materials]</span></b><br>
      <b title="Proposed new name for the simple composite sample"><small>New C.S.Name: </small><?= $model['sample_group_name'] ?></b><br>
      <b><small>Order interval: </small>[<?= $model['num_order_ini'] ?> ... <?= $model['num_order_end'] ?>]</b>
    </div>
    <div class="icon">
      <i class="fa fa-archive"></i>
    </div>
  </div>
</div>
<?php

use yii\helpers\Html;

$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['manage-request/index']];
$this->params['breadcrumbs'][] = ['label' =>  'Manage Request', 'url' => ($request_status_code == '1.1' ? ['manage-request/index'] : ['manage-request/management', 'request_id' => $request_id])];
$this->title = 'Sample Manage';
$this->params['breadcrumbs'][] = empty($request_code) ? $this->title :  $this->title . ' (' . $request_code . ')';
$class_active_li_2_2 = ($request_status_code == 1.1 or $request_status_code == 1.9) ? 'class="active"' : '';
$class_active_2_2 = ($request_status_code == 1.1 or $request_status_code == 1.9) ? 'class="tab-pane active"' : 'class="tab-pane"';

$class_active_3_3 = $request_status_code == 1.2 ? 'class="tab-pane active"' : 'class="tab-pane"';
$class_active_4_4 = ($request_status_code >= 1.3 and $request_status_code != 1.9) ? 'class="tab-pane active"' : 'class="tab-pane"';
?>

<div class="management">
  <!-- #region BACK -->
  <div class="row">
    <div class="col-md-8">
      <h1><?= empty($request_code) ? Html::encode($this->title) : Html::encode($this->title) . ' (<span class="text-blue-link">' . $request_code . '</span>)' ?></h1>
    </div>
    <div class="col-md-4 init-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back', ($request_status_code == '1.1' ? ['manage-request/index'] : ['manage-request/management', 'request_id' => $request_id]), ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->


  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">

      <?php if ($request_status_code == 1.2) { ?>
        <li class="active"><a href="#tab_3-3" data-toggle="tab" aria-expanded="false">Sample Generator</a></li>
      <?php } ?>

      <?php if ($request_status_code >= 1.3 and $request_status_code != 1.9) { ?>
        <li class="active"><a href="#tab_4-4" data-toggle="tab" aria-expanded="false">Sample Generated</a></li>
      <?php } ?>

      <li <?= $class_active_li_2_2 ?>><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Material Check-in</a></li>

      <li class=""><a href="#tab_1-1" data-toggle="tab" aria-expanded="false">Real Time Report</a></li>

    </ul>
    <div class="tab-content">



      <div class="tab-pane" id="tab_1-1">
        <!-- Real Time Report -->
        <?= $this->render('_view-c-real-time-report', [
          'dataProviderH' => $dataProviderH,
          'filterModelH' => $filterModelH,
          'request_id' => $request_id,
          'composite_sample_group_id' => $composite_sample_group_id,
        ]) ?>
      </div>

      <div <?= $class_active_2_2 ?> id="tab_2-2">
        <!-- Material Check-in -->
        <?= $this->render('_view-a-material-check-in', [
          'dataProvider' => $dataProvider,
          'filterModel' => $filterModel,
          'request_status_code' => $request_status_code,
          'request_id' => $request_id,
          'composite_sample_group_id' => $composite_sample_group_id,
        ])  ?>
      </div>

      <div <?= $class_active_3_3 ?> id="tab_3-3">
        <!-- Sample Generator -->
        <?= $this->render('_view-b-sample-generator', [
          'request_status_code' => $request_status_code,
          'request_id' => $request_id,
          'data_material' => $data_material,
          'composite_sample_group_id' => $composite_sample_group_id,
          'request_code' => $request_code,
          'material_reserve_value' => $material_reserve_value,
          'var1' => $var1,
          'var2' => $var2,
          'var3' => $var3,
        ])  ?>
      </div>

      <div <?= $class_active_4_4 ?> id="tab_4-4">
        <!-- Sample Generated -->
        <?= $this->render('_view-b-sample-generated', [
          'request_status_code' => $request_status_code,
          'request_id' => $request_id,
          'data_material' => $data_material,
          'composite_sample_group_id' => $composite_sample_group_id,
          'request_code' => $request_code,
        ])  ?>
      </div>



    </div>
  </div>



  <!-- #region BACK -->
  <div class="row ">
    <div class="col-md-12 final-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back', ($request_status_code == '1.1' ? ['manage-request/index'] : ['manage-request/management', 'request_id' => $request_id]), ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->
</div>
<style>
  html {
    font-size: 12px;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  }

  body {
    font-family: "Lucida Console", "Courier New", monospace;
    font-size: 12px;
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
  }

  .table {
    border-collapse: collapse !important;
  }

  .table td,
  .table th {
    background-color: #fff !important;
  }

  .table thead tr th {
    font-weight: bold;
    color: #337ab7;
  }

  .table-bordered td,
  .table-bordered th {
    border: 1px solid #ddd !important;
  }


  .table {
    width: 95%;
    max-width: 95%;
    margin-top: 40px;
    margin-bottom: 20px;
    margin-left: 2.5%;
    margin-right: 2.5%;
  }

  .table tbody tr td,
  .table tbody tr th,
  .table tfoot tr td,
  .table tfoot tr th,
  .table thead tr td,
  .table thead tr th {
    padding: 4px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
    font-size: 12px;
  }

  .table thead tr th {
    vertical-align: bottom;
    border-bottom: 2px solid #ddd;
  }

  .table caption+thead tr:first-child td,
  .table caption+thead tr:first-child th,
  .table colgroup+thead tr:first-child td,
  .table colgroup+thead tr:first-child th,
  .table thead:first-child tr:first-child td,
  .table thead:first-child tr:first-child th {
    border-top: 0;
  }

  .table tbody+tbody {
    border-top: 2px solid #ddd;
  }

  .table .table {
    background-color: #fff;
  }

  .table-condensed tbody tr td,
  .table-condensed tbody tr th,
  .table-condensed tfoot tr td,
  .table-condensed tfoot tr th,
  .table-condensed thead tr td,
  .table-condensed thead tr th {
    padding: 5px;
  }

  .table-bordered,
  .table-bordered tbody tr td,
  .table-bordered tbody tr th,
  .table-bordered tfoot tr td,
  .table-bordered tfoot tr th,
  .table-bordered thead tr td,
  .table-bordered thead tr th {
    border: 1px solid #ddd;
  }

  .table-bordered thead tr td,
  .table-bordered thead tr th {
    border-bottom-width: 2px;
  }

  .table-striped tbody tr:nth-child(odd) {
    background-color: #f9f9f9;
  }

  .table-hover tbody tr:hover {
    background-color: #f5f5f5;
  }

  table col[class*="col-"] {
    position: static;
    display: table-column;
    float: none;
  }

  table td[class*="col-"],
  table th[class*="col-"] {
    position: static;
    display: table-cell;
    float: none;
  }

  .table-responsive {
    min-height: 0.01%;
    overflow-x: auto;
  }






  @media print {
    table {
      page-break-after: auto
    }

    tr {
      page-break-inside: avoid;
      page-break-after: auto
    }

    td {
      page-break-inside: avoid;
      page-break-after: auto
    }

    thead {
      display: table-header-group
    }

    tfoot {
      display: table-footer-group
    }
  }
</style>

<body onload="window.onafterprint = window.close; 
document.title =  '<?php echo ($longFileName)  ?>' ;
window.print();">
  <div>
    <table class="table table-bordered">
      <thead>
        <tr>
          <?php foreach ($headers as $head) { ?>
            <th><?= $head ?></th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($records as $row) { ?>
          <tr>
            <?php foreach ($row as $value) { ?>
              <td><?= $value ?></td>
            <?php } ?>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</body>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
  <div class="col-lg-6 col-md-12">
    <?php echo $form->field($material_model, 'seed_name')->textInput(['maxlength' => true, 'disabled' => true]) ?>
    <?php echo $form->field($material_model, 'seed_code')->textInput(['disabled' => true]) ?>
    <?php echo $form->field($material_model, 'package_label')->textInput(['disabled' => true]) ?>
    <?php echo $form->field($material_model, 'package_code')->textInput(['disabled' => true]) ?>
    <?php echo $form->field($material_model, 'germplasm_name')->textInput(['disabled' => true]) ?>
  </div>

  <div class="col-lg-6 col-md-12">
    <?php echo $form->field($material_model, 'material_unit_qty')->textInput() ?>
    <?php echo $form->field($material_model, 'material_status_id')->dropDownList($material_model->getParameter('material', 'status'), ['prompt' => 'Select...']) ?>
    <?php echo $form->field($material_model, 'observation')->textInput(['maxlength' => true]) ?>
    <?php echo $form->field($material_model, 'historical_data_version')->textInput(['disabled' => true]) ?>
    <?php echo $form->field($material_model, 'details')->textInput(['maxlength' => true]) ?>
  </div>
</div>

<div class="modal-footer">
  <div class="form-group">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
  </div>
</div>

<?php ActiveForm::end(); ?>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="col-lg-2 col-xs-3">
  <div class="small-box bg-yellow">
    <div class="inner">
      <b>Simple Composite Sample</b><br>
      <small title="Sample name">Name:</small> <b><?= $model['sample_group_name'] ?></b><br>
      <small title="Interval limits">Int. limits: [<?= $model['num_order_ini'] ?> ... <?= $model['num_order_end'] ?>]</small><br>
      <small title="Interval names">Int. names: [<?= $model['material_name_ini'] ?> ... <?= $model['material_name_end'] ?>]</small><br>
      <small>Group: <?= $model['group_name'] ?></small><br>
      <small>Quantities: <?= $model['material_qty'] ?>materials - <?= $model['material_unit_qty'] ?>units</small><br>

    </div>
    <div class="icon">
      <i class="fa fa-archive"></i>
    </div>
    <?= Html::a(
      'More info  <i class="fa fa-arrow-circle-right"></i>',
      '#',
      [
        'class' => 'small-box-footer modal-view-action',
        'value' => Url::to(
          [
            'more-info',
            'request_id' => Yii::$app->request->get('request_id'),
            'material_group_id' => $model['material_group_id'],
          ]
        )
      ]
    ); ?>
  </div>
</div>
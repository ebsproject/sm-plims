<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

$this->title = 'Search';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
  .card-title {
    font-size: 1.5rem;
  }

  .radio>label {
    font-size: 1.25rem;
    font-weight: 500 !important;
  }

  .form-group {
    padding: 0px 15px;
    margin-bottom: 5px;
  }
</style>


<div class="search">
  <h1><?= Html::encode($this->title) ?></h1>

  <?php $form = ActiveForm::begin(); ?>
  <div class="row">
    <div class="col-6">
      <div class="callout callout-secondary">
        <div class="row">
          <div class="col-4">
            <div class="form-group">
              <label>Request</label>
              <?= Select2::widget(
                [
                  'id' => 'select_request_id',
                  'name' => '_request_name',
                  'data' => $_request_data,
                  'size' => Select2::SMALL,
                  'value' => $_request_id_list,
                  'maintainOrder' => true,
                  'options' => [
                    'placeholder' => 'Select one o more options ...',
                    'allowClear' => true,
                    'multiple' => true
                  ],
                ]
              );
              ?>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label>Year</label>
              <?= Select2::widget(
                [
                  'id' => 'select_year_id',
                  'name' => '_year_name',
                  'data' => $_year_data,
                  'size' => Select2::SMALL,
                  'value' => $_year_id_list,
                  'maintainOrder' => true,
                  'options' => [
                    'placeholder' => 'Select one o more options ...',
                    'allowClear' => true,
                    'multiple' => true
                  ],
                ]
              );
              ?>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label>Status</label>
              <?= Select2::widget(
                [
                  'id' => 'select_status_id',
                  'name' => '_status_name',
                  'data' => $_status_data,
                  'size' => Select2::SMALL,
                  'value' => $_status_id_list,
                  'maintainOrder' => true,
                  'options' => [
                    'placeholder' => 'Select one o more options ...',
                    'allowClear' => true,
                    'multiple' => true
                  ],
                ]
              );
              ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-4">
            <div class="form-group">
              <label>Assay</label>
              <?= Select2::widget(
                [
                  'id' => 'select_assay_id',
                  'name' => '_assay_name',
                  'data' => $_assay_data,
                  'size' => Select2::SMALL,
                  'value' => $_assay_id_list,
                  'maintainOrder' => true,
                  'options' => [
                    'placeholder' => 'Select one o more options ...',
                    'allowClear' => true,
                    'multiple' => true
                  ],
                ]
              );
              ?>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label>Activity</label>
              <?= Select2::widget(
                [
                  'id' => 'select_activity_id',
                  'name' => '_activity_name',
                  'data' => $_activity_data,
                  'size' => Select2::SMALL,
                  'value' => $_activity_id_list,
                  'maintainOrder' => true,
                  'options' => [
                    'placeholder' => 'Select one o more options ...',
                    'allowClear' => true,
                    'multiple' => true
                  ],
                ]
              );
              ?>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label>Agents</label>
              <?= Select2::widget(
                [
                  'id' => 'select_agent_id',
                  'name' => '_agent_name',
                  'data' => $_agent_data,
                  'size' => Select2::SMALL,
                  'value' => $_agent_id_list,
                  'maintainOrder' => true,
                  'options' => [
                    'placeholder' => 'Select one o more options ...',
                    'allowClear' => true,
                    'multiple' => true
                  ],
                ]
              );
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="callout callout-secondary">
        <div class="row">
          <div class="col-4">
            <div class="form-group">
              <label>Requested User</label>
              <?= Select2::widget(
                [
                  'id' => 'select_requested_id',
                  'name' => '_requested_name',
                  'data' => $_requested_data,
                  'size' => Select2::SMALL,
                  'value' => $_requested_id_list,
                  'maintainOrder' => true,
                  'options' => [
                    'placeholder' => 'Select one o more options ...',
                    'allowClear' => true,
                    'multiple' => true
                  ],
                ]
              ); ?>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label>Registrar Users</label>
              <?= Select2::widget(
                [
                  'id' => 'select_registrar_id',
                  'name' => '_registrar_name',
                  'data' => $_registrar_data,
                  'size' => Select2::SMALL,
                  'value' => $_registrar_id_list,
                  'maintainOrder' => true,
                  'options' => [
                    'placeholder' => 'Select one o more options ...',
                    'allowClear' => true,
                    'multiple' => true
                  ],
                ]
              ); ?>
            </div>
          </div>
          <div class="col-4">
          </div>
        </div>
      </div>
    </div>
    <div class="col-6">
      <div class="callout callout-secondary">
        <div class="row">
          <div class="col-4">
            <div class="form-group">
              <label>Request type</label>
              <div class="radio">
                <label>
                  <input type="radio" name="rt_options" id="rt_options_radios1" value="null" <?= $_rt_id == null ? 'checked' : '' ?>>
                  All
                </label>
                <br>
                <label>
                  <input type="radio" name="rt_options" id="rt_options_radios2" value="40" <?= $_rt_id == '40' ? 'checked' : '' ?>>
                  Introduction
                </label>
                <br>
                <label>
                  <input type="radio" name="rt_options" id="rt_options_radios3" value="11" <?= $_rt_id == '11' ? 'checked' : '' ?>>
                  Shipment
                </label>
              </div>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label>Auxiliary Results</label>
              <div class="radio">
                <label>
                  <input type="radio" name="result_options" id="result_options_radios1" value="null" <?= $_result_id == null ? 'checked' : '' ?>>
                  All
                </label>
                <br>
                <label>
                  <input type="radio" name="result_options" id="result_options_radios2" value="negative" <?= $_result_id == 'negative' ? 'checked' : '' ?>>
                  Negative
                </label>
                <br>
                <label>
                  <input type="radio" name="result_options" id="result_options_radios3" value="positive" <?= $_result_id == 'positive' ? 'checked' : '' ?>>
                  Positive
                </label>
              </div>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label>Process type</label>
              <div class="radio">
                <label>
                  <input type="radio" name="process_options" id="process_options_radios1" value="null" <?= $_process == null ? 'checked' : '' ?>>
                  All
                </label>
                <br>
                <label>
                  <input type="radio" name="process_options" id="process_options_radios2" value="1" <?= $_process == '1' ? 'checked' : '' ?>>
                  Normal-process
                </label>
                <br>
                <label>
                  <input type="radio" name="process_options" id="process_options_radios3" value="2" <?= $_process == '2' ? 'checked' : '' ?>>
                  Re-process
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-4">
            <div class="form-group">
              <label>Crop</label>
              <div class="radio">
                <label>
                  <input type="radio" name="crop_options" id="crop_options_radios1" value="null" <?= $_crop_id == null ? 'checked' : '' ?>>
                  All
                </label>
                <br>
                <label>
                  <input type="radio" name="crop_options" id="crop_options_radios2" value="3" <?= $_crop_id == '3' ? 'checked' : '' ?>>
                  Maize
                </label>
                <br>
                <label>
                  <input type="radio" name="crop_options" id="crop_options_radios3" value="4" <?= $_crop_id == '4' ? 'checked' : '' ?>>
                  Small grain cereals
                </label>
              </div>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label>Sample type</label>
              <div class="radio">
                <label>
                  <input type="radio" name="cst_options" id="cst_options_radios1" value="null" <?= $_cst_id == null ? 'checked' : '' ?>>
                  All
                </label>
                <br>
                <label>
                  <input type="radio" name="cst_options" id="cst_options_radios2" value="30" <?= $_cst_id == '30' ? 'checked' : '' ?>>
                  General Composite Sample
                </label>
                <br>
                <label>
                  <input type="radio" name="cst_options" id="cst_options_radios3" value="31" <?= $_cst_id == '31' ? 'checked' : '' ?>>
                  Simple Composite Sample
                </label>
                <br>
                <label>
                  <input type="radio" name="cst_options" id="cst_options_radios3" value="29" <?= $_cst_id == '29' ? 'checked' : '' ?>>
                  Individual Sample
                </label>
              </div>
            </div>
          </div>
          <div class="col-4">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <div class="callout callout-secondary">
        <div class="row">
          <div class="col-lg-6">
            <div class="form-group">
              <label>Page size</label>
              <?= Html::dropDownList(
                '_limit',
                $_limit,
                [
                  10 => 10,
                  50 => 50,
                  100 => 100,
                  1000 => 1000
                ],
                ['id' => 'pagesize']
              ) ?>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="btn-group pull-right">

              <?= Html::a('Reset', ['reset',], ['class' => 'btn btn-default']) ?>

              <button type="submit" class="btn btn-primary">Search</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="card card-outline card-secondary">
    <div class="card-body" style="display: block;">
      <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}{summary}{pager}",
        'columns' => [
          [
            'format' => 'raw',
            'attribute' => 'request_code',
            'value' => function ($data) {
              return Html::a(
                $data['request_code'] . ' <i class="fa fa-external-link" aria-hidden="true"></i>',
                [
                  'manage-request/management',
                  'request_id' => $data['request_id']
                ],
                ['target' => '_blank']
              );
            },

          ],
          // 'sequential_code',
          'request_type',
          'request_status',
          'process_type',

          // 'consignee_user',
          'crop_name',
          // 'assay_name',

          [
            'format' => 'raw',
            'attribute' => 'assay_name',
            'value' => function ($data) {
              return Html::a(
                $data['assay_name'] . ' <i class="fa fa-external-link" aria-hidden="true"></i>',
                [
                  'manage-assay/management',
                  'request_id' => $data['request_id'],
                  'essay_id' => $data['essay_id'],
                ],
                ['target' => '_blank']
              );
            },
          ],


          'activity_name',
          [
            'format' => 'raw',
            'attribute' => 'sample_type',
            'value' => function ($data) {
              return Html::a(
                $data['sample_type'] . ' <i class="fa fa-external-link" aria-hidden="true"></i>',
                [
                  'manage-sample-results/management',
                  'request_id' => $data['request_id'],
                  'num_order_id' => $data['num_order_id'],
                  'essay_id' => $data['essay_id'],
                  'composite_sample_type_id' => $data['composite_sample_type_id'],
                  'request_process_essay_id' => $data['request_process_essay_id'],

                ],
                ['target' => '_blank']
              );
            },
          ],

          // 'tertiary_order_num',
          'requested_user',
          'requesst_creation_date',
          // 'agents',
          // 'text_results',
          [
            'attribute' => 'text_results',
            'format' => 'raw',
          ],
          // 'auxiliary_results',
          [
            'attribute' => 'auxiliary_results',
            'format' => 'raw',
          ],
          'registrar_users'
        ],
        'emptyText' => '-',
      ]); ?>
    </div>
  </div>

  <?php
  // echo "<pre><h3>MSG</h3>";
  // print_r($msg);
  // echo "</pre>";

  // echo "<pre><h3>GET</h3>";
  // print_r($data_get);
  // echo "</pre>";

  // echo "<pre><h3>POST</h3>";
  // print_r($data_post);
  // echo "</pre>";
  ?>

</div>

<?php ActiveForm::end(); ?>
</div>
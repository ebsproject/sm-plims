<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="document_item">
  <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12">
      <span class="text">
        <i class="fa fa-calendar-o"></i> <?= $model['registered_at'] ?> | <i class="<?= $model['class_details'] ?>"></i> <?= $model['document_name'] ?>
      </span>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
      <div class="btn-group pull-right">
        <?= Html::a(
          '<small>
            <i class="glyphicon glyphicon-trash"></i> Remove
          </small>',
          [
            'remove-document',
            'request_id' => $model['request_id'],
            'request_documentation_id' => $model['request_documentation_id'],
          ],
          [
            'class' => 'text-red',
          ]
        ); ?>

        <a href="<?= Url::base() ?>/<?= $model['path_documentation'] ?><?= $model['document_name'] ?>.<?= $model['document_extension'] ?>">
          <small>
            <i class="fa fa-download"></i> Download
          </small>
        </a>
      </div>
    </div>
  </div>
</div>
<?php

use yii\helpers\Html;

$this->title = 'Manage Request';
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] =  $this->title . ' (' . $request_code . ')';
?>

<div class="management">
  <!-- #region BACK -->
  <div class="row">
    <div class="col-md-8">
      <h1><?= Html::encode($this->title) . ' (<span class="text-blue-link">' . $request_code . '</span>)' ?></h1>
    </div>
    <div class="col-md-4 init-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back', ['index',], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->

  <div class="row">
    <div class="col-lg-6 col-md-6">
      <!-- Request Detail -->
      <?= $this->render('_view-a-request-detail', [
        'request_id' => $request_id,
        'request_code' => $request_code,
        'r_data' => $r_data,
        'request_status_id' => $request_status_id,
        'is_ready_to_finish' => $is_ready_to_finish,
        'request_status_code' => $request_status_code,
      ])  ?>
    </div>
    <div class="col-lg-6 col-md-6">
      <!-- Request Documentation -->
      <?= $this->render('_view-b-request-documentation', [
        'request_id' => $request_id,
        'model_upload_documentation' => $model_upload_documentation,
        'data_documentation' => $data_documentation,
        'num_order_id' => $num_order_id,
      ])  ?>
    </div>
  </div>

  <!-- Sample Management -->
  <?= $this->render('_view-c-sample-management', [
    'request_id' => $request_id,
    'composite_sample_types' => $composite_sample_types,
    'date_verified_entries' => $date_verified_entries,
  ]) ?>

  <!-- Assay Management -->
  <?php if ($request_status_code >= 1.3) { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-body">


            <?= $this->render('_view-f-assay-tree', [
              'request_code' => $request_code,
              'assay_tree_data_lvl' => $assay_tree_data_lvl,
              'request_status_name' => $request_status_name,
              'request_status_class' => $request_status_class,
              'request_id' => $request_id,
              'select_data_1' => $select_data_1,
              'request_status_code' => $request_status_code,
            ])  ?>




            <!-- <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">

                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false"><i class="fa fa-sitemap fa-rotate-270"></i> Assay Tree View</a></li>

                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-list"></i> Assay List View</a></li>

              </ul>
              <div class="tab-content">

                <div class="tab-pane active" id="tab_1">
                  <?php

                  // $this->render('_view-f-assay-tree', [
                  //   'request_code' => $request_code,
                  //   'assay_tree_data_lvl' => $assay_tree_data_lvl,
                  //   'request_status_name' => $request_status_name,
                  //   'request_status_class' => $request_status_class,
                  //   'request_id' => $request_id,
                  //   'select_data_1' => $select_data_1,
                  //   'request_status_code' => $request_status_code,
                  // ])  
                  ?>
                </div>

                <div class="tab-pane " id="tab_2">
                  <?php

                  // $this->render('_view-e-assay-management', [
                  //   'request_id' => $request_id,

                  //   'data_request_process_assay' => $data_request_process_assay,
                  //   'composite_sample_group_id' => $composite_sample_group_id,
                  //   'is_remove_all' => $is_remove_all,
                  // ])  

                  ?>
                </div>

              </div>
            </div> -->



          </div>
        </div>
      </div>
    </div>
  <?php } ?>

  <!-- #region BACK -->
  <div class="row ">
    <div class="col-md-12 final-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back', ['index',], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->
</div>
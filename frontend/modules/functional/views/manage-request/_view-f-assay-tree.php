<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;

?>
<style>
  .tree {
    min-height: 20px;
    padding: 19px;
    margin-bottom: 20px;
    /* background-color: #fbfbfb; */
    background-color: #fbfbfb40;
    border: 1px solid #999;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
    -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05)
  }

  .tree li {
    list-style-type: none;
    margin: 0;
    padding: 10px 5px 0 5px;
    position: relative
  }

  .tree li::before,
  .tree li::after {
    content: '';
    left: -20px;
    position: absolute;
    right: auto
  }

  .tree li::before {
    border-left: 1px solid #999;
    bottom: 50px;
    height: 100%;
    top: 0;
  }

  .tree li::after {
    box-sizing: border-box;
    border-left: 1px solid #999;
    border-bottom: 1px solid #999;
    border-bottom-left-radius: 10px;
    height: 25px;
    top: 0px;
    width: 25px;
  }

  .tree li span {
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border: 1px solid #999;
    border-radius: 5px;
    display: inline-block;
    padding: 3px 8px;
    text-decoration: none;
  }

  .tree li.parent_li>span {
    cursor: pointer;
  }

  .tree>ul>li::before,
  .tree>ul>li::after {
    border: 0;
  }

  .tree li:last-child::before {
    height: 14px;
  }

  .tree li:not(.parent_li) span {
    position: relative;
  }

  .tree li span:before {
    content: "";
    position: absolute;
    /* border-width - 1px */
    left: -4px;
    bottom: calc(50% - 4px);
    border-color: transparent;
    border-left-color: #999;
    border-style: inherit;
    border-width: 4px;
  }

  /* another styles for parent_li immediate span children */
  .tree li.parent_li>span:before {
    bottom: initial;
    left: 2px;
    top: 20.5px;
  }

  /* hide arrow for immediate children */
  .tree>ul>li>span:before {
    display: none;
  }

  .tree li.parent_li>span:hover,
  .tree li.parent_li>span:hover+ul li span {
    background: #eee;
    border: 1px solid #94a0b4;
    color: #000;
  }
</style>
<br />

<?php if ($request_status_code != 1.9) { ?>
  <div class="row">
    <div class="col-md-12">
      <?php $form = ActiveForm::begin(); ?>
      <div class="input-group">
        <?= Select2::widget(
          [
            'id' => 'select_id_1',
            'name' => 'select_name_1',
            'data' => $select_data_1,
            'value' => null,
            'maintainOrder' => true,
            'options' => [
              'placeholder' => 'Select one o more assays ...',
              'allowClear' => true,
              'multiple' => true
            ],
          ]
        );
        ?>
        <span class="input-group-btn">
          <?= Html::a(
            '<i class="fa fa-plus"></i> Add Assay(s)',
            [
              'add-essay',
              'request_id' => $request_id,
            ],
            [
              'class' => 'btn btn-default',
              'title'        => 'Add Assay',
              'data-method'  => 'post',
            ]
          ) ?>
        </span>
      </div>
      <?php ActiveForm::end() ?>
    </div>
  </div>
<?php  } ?>
<!-- seleccion de ensayo -->


<div class="row">
  <div class="col-md-12">
    <div class="tree well">
      <ul>
        <li>
          <span><?= $request_code ?></span>
          <label class="<?= $request_status_class ?>"><?= $request_status_name ?></label>
          <?php
          echo ('<ul>');
          foreach ($assay_tree_data_lvl as $key => $value1) {
            echo ('<li>');
            echo ($value1['body']);
            if (count($value1['items']) > 0) {
              echo ('<ul>');
              foreach ($value1['items'] as $key => $value2) {
                echo ('<li>');
                echo ($value2['body']);
                if (count($value2['items']) > 0) {
                  echo ('<ul>');
                  foreach ($value2['items'] as $key => $value3) {
                    echo ('<li>');
                    echo ($value3['body']);
                    if (count($value3['items']) > 0) {
                      echo ('<ul>');
                      foreach ($value3['items'] as $key => $value4) {
                        echo ('<li>');
                        echo ($value4['body']);
                        if (count($value4['items']) > 0) {
                          echo ('<ul>');
                          foreach ($value4['items'] as $key => $value5) {
                            echo ('<li>');
                            echo ($value5['body']);
                            echo ('</li>');
                          }
                          echo ('</ul>');
                        }
                        echo ('</li>');
                      }
                      echo ('</ul>');
                    }
                    echo ('</li>');
                  }
                  echo ('</ul>');
                }
                echo ('</li>');
              }
              echo ('</ul>');
            }
            echo ('</li>');
          }
          echo ('</li>');
          ?>
        </li>
      </ul>
    </div>
  </div>
</div>
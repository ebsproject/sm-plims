<?php

use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;

?>

<div class="card card-outline card-secondary">

  <div class="card-header">
    <h3 class="card-title text-blue-link">Management of reprocess assays</h3>
  </div>

  <div class="card-body">

    <!-- lista de ensayos -->
    <div class="row">
      <div class="col-lg-12">
        <?php
        $data_request_reprocess_essay_provider = new ArrayDataProvider(
          [
            'allModels' => $data_request_reprocess_assay,
            'pagination' => [
              'pageSize' => 0,
            ],
            'sort' => [
              'attributes' => [
                'request_id',
                'num_order_id',
                'essay_id',
                'crop_id',
                'workflow_id',
                'start_date',
                'finish_date',
                'check_date',
                'agent_qty',
                'agent_detail',
                'sub_total_cost_essay',
                'composite_sample_type_id'
              ],
            ],
          ]
        );
        ?>
        <?= ListView::widget(
          [
            'dataProvider' => $data_request_reprocess_essay_provider,
            'itemView' => '_item-view-assay',
            'layout' => '{items}{pager}{summary}',
            'pager' => [
              'firstPageLabel' => 'first',
              'lastPageLabel' => 'last',
              'prevPageLabel' => 'previous',
              'nextPageLabel' => 'next',
            ],
          ]
        );
        ?>
      </div>
    </div>

  </div>

</div>
<?php

use yii\helpers\Html;
use kartik\select2\Select2;
use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
use yii\widgets\ActiveForm;

?>
<br />
<!-- lista de ensayos -->
<div class="row">
  <div class="col-lg-12">
    <?php
    $data_request_process_essay_provider = new ArrayDataProvider(
      [
        'allModels' => $data_request_process_assay,
        'pagination' => [
          'pageSize' => 0,
        ],
        'sort' => [
          'attributes' => [
            'request_id',
            'num_order_id',
            'essay_id',
            'crop_id',
            'workflow_id',
            'start_date',
            'finish_date',
            'check_date',
            'agent_qty',
            'agent_detail',
            'sub_total_cost_essay',
            'composite_sample_type_id'
          ],
        ],
      ]
    );
    ?>
    <?= ListView::widget(
      [
        'dataProvider' => $data_request_process_essay_provider,
        'itemView' => '_item-view-assay',
        'layout' => '{items}{pager}{summary}',
        'pager' => [
          'firstPageLabel' => 'first',
          'lastPageLabel' => 'last',
          'prevPageLabel' => 'previous',
          'nextPageLabel' => 'next',
        ],
      ]
    );
    ?>
  </div>
</div>
<!-- remover todo -->
<?php if ($is_remove_all) { ?>
  <div class="row">
    <div class="col-lg-12">
      <?= Html::a(
        'Remove all',
        [
          'remove-all-assay',
          'request_id' => $request_id,
          'composite_sample_group_id' => $composite_sample_group_id,
        ],
        [
          'class' => 'pull-right',
          'title'        => 'Remove Assay',
          'data-method'  => 'post',
        ]
      )
      ?>
    </div>
  </div>
<?php } ?>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
?>
<div class="row">
  <div class="col-md-6">
    <h3>Current status</h3>
    <!-- ------------------ -->
    <?php Pjax::begin(['id' => 'detail_status']) ?>
    <?= DetailView::widget([
      'model' => $s_data_last,
      'attributes' => [
        [
          'label' => 'Status',
          'value' => '<span class="' . $s_data_last['s_class_details'] . '">' . $s_data_last['s_name'] . '</span>',
          'format' => 'raw',
        ],
        [
          'label' => 'Request Code',
          'attribute' => 's_request_code',
          'format' => 'raw',
        ],
        [
          'label' => 'Crop',
          'attribute' => 's_crop_name',
          'format' => 'raw',
        ],
        [
          'label' => 'Update Status User',
          'attribute' => 's_username',
          'format' => 'raw',
        ],
        [
          'label' => 'Date Status',
          'attribute' => 's_date',
          'format' => 'raw',
        ],
      ],
      'options' => [
        'class' => 'table table-striped table-bordered detail-view',
      ],
    ])
    ?>
    <?php Pjax::end() ?>
    <!-- ------------------ -->
    <?php $this->registerJs(
      '$("document").ready(function(){ 
        $("#update_observation").on("pjax:end", function() {
          $.pjax.reload({container:"#detail_status"});
        });
      });'
    ); ?>
    <!-- ------------------ -->
    <?php Pjax::begin(['id' => 'update_observation']) ?>
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>
    <?= $form->field($model, 'observation')->textarea(['rows' => 3, 'maxlength' => 500]) ?>
    <div class="form-group pull-right">
      <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
    <!-- ------------------ -->
    <h3>Recipient</h3>
    <?= DetailView::widget([
      'model' => $s_data_aux,
      'attributes' => $s_colums_recipient,
      'options' => [
        'class' => 'table table-striped table-bordered detail-view',
      ],
    ])
    ?>
    <h3>Sender</h3>
    <?= DetailView::widget([
      'model' => $s_data_aux,
      'attributes' => $s_colums_sender,
      'options' => [
        'class' => 'table table-striped table-bordered detail-view',
      ],
    ])
    ?>
    <!-- ------------------ -->
  </div>
  <div class="col-md-6">
    <h3>Status line</h3>
    <div class="timeline">
      <?php foreach ($s_data as $key => $value) { ?>
        <div class="time-label">
          <span class="bg-green">
            <?= $value['s_date'] ?>
          </span>
        </div>
        <div>
          <i class="fa fa-calendar-plus-o <?= str_replace('label label', 'bg', $value['s_class_details']) ?>"></i>
          <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> <?= $value['s_time'] ?></span>
            <h3 class="timeline-header text-blue"> <?= $value['s_name_title'] ?> by <a href="#"><?= $value['s_username'] ?></a></h3>
            <div class="timeline-body">
              <p>
                <?= $value['s_observation']
                ?>
              </p>
            </div>
          </div>
        </div>
      <?php } ?>
      <div>
        <i class="fa fa-clock-o bg-gray"></i>
      </div>
    </div>
  </div>
</div>
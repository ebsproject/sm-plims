<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use yii\widgets\DetailView;

$modal_title = 'Status log';
?>

<!-- MODAL-INI -->
<div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <!-- modal-sm modal-md modal-lg modal-xl -->
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?= Html::encode($modal_title) ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="modal-view-content"></div>
      </div>
    </div>
  </div>
</div>
<!-- MODAL-END -->

<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Detail</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-md-6 col-lg-6">
        <?= DetailView::widget([
          'model' => $r_data,
          'attributes' => [
            [
              'label' => 'Request Code',
              'value' =>   $r_data['r_code']
            ],
            [
              'label' => 'Crop',
              'value' =>   $r_data['crop']
            ],
            [
              'label' =>  'Material Qty',
              'value' =>  $r_data['material_qty'],
            ],
            [
              'label' =>  'Sample Qty',
              'value' =>  $r_data['sample_qty'],
            ],

            [
              'label' =>  'Material Reserve',
              'value' =>  $r_data['material_reserve'],
            ],


            [
              'label' => 'Request User',
              'value' =>   $r_data['requested_user'],
            ],
            [
              'label' => 'Date Created',
              'value' =>   $r_data['date_created'],
            ],
          ],
          'options' => [
            'class' => 'table detail-view',
          ],
        ]) ?>
      </div>
      <div class="col-md-6 col-lg-6">
        <?= DetailView::widget([
          'model' => $r_data,
          'attributes' => [
            [
              'label' => 'Seed distribution',
              'value' =>   $r_data['seed_distribution'],
            ],
            [
              'label' =>   'Recipient',
              'value' =>      $r_data['recipient'],
            ],
            [
              'label' => $r_data['code_distribution'] == 'RT1' ? 'Country [Recipient]' : 'Sender',
              'value' =>   $r_data['code_distribution'] == 'RT1' ?   $r_data['recipient_country'] : $r_data['sender'],
            ],
            [
              'label' => $r_data['code_distribution'] == 'RT1' ? 'Sender' : 'Country [Sender]',
              'value' =>   $r_data['code_distribution'] == 'RT1' ?   $r_data['sender'] : $r_data['sender_country'],
            ],
            [
              'label' => 'Status',
              'value' =>    '<span title="' . $r_data['r_status_code'] . ' - ' . $r_data['r_status_name'] . '" class="' . $r_data['r_status_class'] . '">' . $r_data['r_status_name'] . '</span>',
              'format' => 'raw',
            ],
          ],
          'options' => [
            'class' => 'table detail-view',
          ],
        ]) ?>

        <?php

        // Html::a(
        //   'More details',
        //   ['#'],
        //   [
        //     'class' => 'pull-right modal-view-action',
        //     'title'        => 'See more details',
        //     'value' => Url::to(
        //       [
        //         'show-status',
        //         'request_id' => $request_id,
        //         'request_status_id' => $request_status_id,
        //       ]
        //     ),
        //   ]
        // ) 
        ?>
      </div>
    </div>
  </div>
  <?php if ($is_ready_to_finish and $request_status_code != 1.9) { ?>
    <div class="card-footer">
      <div class="row">
        <div class="col-lg-12">
          <div class="btn-group pull-right">
            <?= Html::a(
              'Finish',
              [
                'finish',
                'request_id' => $request_id,
              ],
              [
                'class' => 'btn btn-success',
              ]
            );
            ?>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>

</div>
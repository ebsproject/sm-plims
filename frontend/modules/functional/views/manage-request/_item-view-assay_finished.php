<?php

use yii\helpers\Html;
?>





<div class="info-box <?= $model['style_class'] ?>">
  <span class="info-box-icon">
    <i class="<?= $model['essay_type_class'] ?>" title="<?= $model['assay_type_description'] ?>"></i>
  </span>


  <?= Html::a(
    '<div class="info-box-content">
      <span class="info-box-number">' . $model['assay_name']  . ' (' . $model['code'] . ')    
        <small>
          <span title="Status of the assay" class="label label-' . $model['request_process_essay_status_class'] . '">' . $model['request_process_essay_status_name']  . '</span>   
          <span class="label label-navy">' . ($model['request_process_essay_root_id'] > 0 ? 'R' : '') . '</span> 
        </small>
      </span>
    <div class="progress">
      <div class="progress-bar" style="width: 0%"></div>
    </div>
    <span class="progress-description pull-left">' . $model['crop_name'] . $model['sample_detail'] . '</span>
    <span class="pull-right">
      <i class="fa fa-user"></i> ' . (isset($model['username0']) ? $model['username0'] : '') . '
      <i class="fa fa-calendar"></i> ' . $model['start_date'] . '

      <i class="fa fa-long-arrow-right"></i>

      <i class="fa fa-user"></i> ' . (isset($model['username1']) ? $model['username1'] : '') . '
      <i class="fa fa-calendar"></i> ' . $model['finish_date'] . '
    </span>
  </div>',
    [
      'manage-assay/release',
      'request_id' => $model['request_id'],
      'essay_id' => $model['essay_id'],
    ],
    [
      'class' =>  $model['style_class'],
    ]
  ); ?>

</div>
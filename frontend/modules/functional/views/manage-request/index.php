<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Requests';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
  .table-bordered {
    text-align: center;
  }
</style>

<div class="index">
  <div class="row">
    <div class="col-md-8">
      <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?php if ($is_creator) { ?>
      <div class="col-md-4 init-row-margin">
        <?= Html::a('<i class="fa fa-plus"></i> New Request', ['/functional/manage-request-generation/management',], ['class' => 'btn btn-default pull-right']) ?>
      </div>
    <?php } ?>

  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-secondary">
        <div class="card-body">

          <?php Pjax::begin(); ?>
          <?= GridView::widget([
            'dataProvider' => $newDataProvider,
            'layout' => "{items}{summary}{pager}",
            'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              [
                'class' => 'yii\grid\ActionColumn',
                'header'        => 'Request Code',
                'template'      => '{link}',
                'buttons'       => [
                  'link' => function ($url, $model, $key) use ($num_order_id) {
                    return Html::a(
                      (empty($model['request_code']) ? 'in process' : "<b class='text-blue'>" . $model['request_code'] . "</b>"),
                      $model['request_status'] == 'request created' ?
                        [
                          'manage-sample/management',
                          'request_id' => $key,
                          'num_order_id' => $num_order_id,
                        ]
                        : [
                          'management',
                          'request_id' => $key,
                        ],
                      [
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    );
                  },
                ],
              ],
              [
                'label' => 'EBS',
                'attribute' => 'bar_id'
              ],
              'crop',
              'material_qty',
              'sample_qty',
              'seed_distribution',
              'recipient',
              'sender',
              'request_user',
              [
                'attribute' => 'request_status',
                'value' => function ($model) {
                  return "<span class='" . $model['request_class'] . "'>" .  $model['request_status'] . "</span>";
                },
                'format' => 'raw',
              ],
              [
                'attribute' => 'date_created',
                'value' => function ($model) {
                  return "<span title='" . $model['datetime_created'] . "'>" .  $model['date_created'] . "</span>";
                },
                'format' => 'raw',
              ],
              [
                'class' => 'yii\grid\ActionColumn',
                'header'        => 'Action',
                'template'      =>  Yii::$app->user->identity->id === 1 ? '{manage} {report} {delete}' : '{manage} {report}',
                'headerOptions' => ['width' => '80'],
                'buttons'       => [
                  'manage' => function ($url, $model, $key) use ($num_order_id) {
                    return Html::a(
                      '<span class="fa fa-th-list" title="Request manage"></span>',
                      $model['request_status'] == 'request created' ?
                        [
                          'manage-sample/management',
                          'request_id' => $key,
                          'num_order_id' => $num_order_id,
                        ]
                        : [
                          'management',
                          'request_id' => $key,
                        ],
                      [
                        'class' => 'btn btn-primary btn-xs',
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    );
                  },
                  'report' => function ($url, $model, $key) use ($num_order_id) {
                    return Html::a(
                      '<span class="fa fa-rocket" title="Release view"></span>',
                      $model['status_code'] == 1.9 ? [
                        'manage-result/management',
                        'request_id' => $key,
                        'request_code' => $model['request_code'],
                      ] : ['#'],

                      [
                        'class' => 'btn btn-default btn-xs',
                        'disabled' =>  $model['status_code'] < 1.9,
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    );
                  },
                  'delete' => function ($url, $model, $key) use ($num_order_id) {
                    return Html::a(
                      '<span class="fa fa-trash" title="Remove"></span>',
                      [
                        'remove',
                        'request_id' => $key,
                      ],
                      [
                        'class' => 'btn btn-danger btn-xs',
                        'data' => [
                          'confirm' => 'ARE YOU SURE YOU WANT TO DELETE THIS REQUEST: ' . $model['request_code'] . '?',
                          'method' => 'post',
                        ],
                      ]
                    );
                  },
                ],
              ],
            ],
          ]); ?>
          <?php Pjax::end(); ?>
        </div>
      </div>

    </div>
  </div>
</div>
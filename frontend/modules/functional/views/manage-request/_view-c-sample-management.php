<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
?>

<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Sample Management</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <?= Html::a(
          '<div class="info-box bg-yellow">
                  <span class="info-box-icon">
                    <i class="glyphicon glyphicon-grain"></i>
                  </span>
                  <div class="info-box-content">
                    <span class="info-box-number">' . $this->title . '</span>
                    <div class="progress">
                      <div class="progress-bar" style="width: 0%">
                      </div>
                    </div>
                    <div class="col-md-8 col-lg-8 no-padding">
                      <span class="info-box-text">Samples available: 
                        <i>' .  $composite_sample_types . '</i>
                      </span>
                      <!-- <span class="progress-description">Handling of composite samples</span> -->
                    </div>
                    <div class="col-md-4 col-lg-4 no-padding">
                      <span class="pull-right">
                        Confirmed Material Entry: <i class="fa fa-calendar-check-o"></i> ' . $date_verified_entries . '
                      </span>
                    </div>
                  </div>
                </div>',
          [
            'manage-sample/management',
            'request_id' => $request_id,
          ]
        ); ?>
      </div>
    </div>
  </div>
</div>
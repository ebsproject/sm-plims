<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Manage Request';
$this->params['breadcrumbs'][] = ['label' => 'Requests', 'url' => ['/functional/manage-request/index']];
$this->params['breadcrumbs'][] =  $this->title . ' (' . $request_code . ')';
?>

<style>
  .st-positive {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;
    background-color: #dd4b39 !important;
  }

  .st-negative {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;
    background-color: #00a65a !important;
  }

  .st-empty {
    display: inline;
    padding: 0.2em 0.6em 0.3em;
    font-size: 75%;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: 0.25em;
    background-color: #c1c1c1 !important;
  }
</style>

<div class="management">

  <!-- #region BACK -->
  <div class="row">
    <div class="col-md-8">
      <h1><?= 'Manage results (<span class="text-blue-link">' . $request_code . '</span>)' ?></h1>
    </div>
    <div class="col-md-4 init-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back', ['/functional/manage-request/index',], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->



  <div class="card card-outline card-secondary data-results">
    <div class="card-header">
      <h3 class="card-title text-blue-link">Consolidated results for release</h3>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-lg-12" style="margin-bottom: 1em;">

          <?php Pjax::begin(); ?>
          <?= GridView::widget([
            'dataProvider' => $data_provider,
            'layout' => "{items}{summary}{pager}",
            'columns' => $columns,
          ]); ?>
          <?php Pjax::end(); ?>


        </div>
      </div>
    </div>


    <?php $form = ActiveForm::begin(); ?>
    <div class="card-footer">
      <div class="row">

        <div class="col-lg-6">
          <div class="form-group">
            <label>Page size</label>
            <?= Html::dropDownList(
              '_limit',
              $_limit,
              [
                10 => 10,
                20 => 20,
                50 => 50,
                100 => 100
              ],
              ['id' => 'pagesize']
            ) ?>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="btn-group pull-right">
            <?= Html::a(
              '<i class="fa fa-file-excel-o"></i> Download .csv',
              [
                'export-report',
                'request_id' => Yii::$app->request->get('request_id'),
                'request_code' => Yii::$app->request->get('request_code'),
              ],
              [
                'class' => 'btn btn-default',
                'title'        => 'Generate consolidate results',
              ]
            );
            ?>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
    </div>
    <?php ActiveForm::end(); ?>
  </div>

  <!-- #region BACK -->
  <div class="row ">
    <div class="col-md-12 final-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back', ['/functional/manage-request/index',], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->
</div>
<?php

use yii\helpers\Html;
?>




<div class="consolidation preview">

  <div class="card-body">
    <span><i class="glyphicon glyphicon-info-sign"></i> At least the results of an evaluation are necessary</span>
    <?php foreach ($data as $activity => $agent_doc_array) {
      $activity_id = explode('.', $activity)[0];
      $activity_name = explode('.', $activity)[1];
      $cbx_name = 'cbx_consolidate[' . $activity_id . ']';
      ?>
      <h4><?= Html::encode($activity_name); ?> &nbsp;&nbsp;
        <small>
          <label>
            <?php if ($activity_id == $first_activity && count($agent_doc_array) == $agents) { ?>
              <input title="It is necessary to have results of all the pathogenic agents to consolidate an evaluation" type="checkbox" checked name=<?= $cbx_name ?> onclick="this.checked=!this.checked;">
            <?php } else if (count($agent_doc_array) == $agents) { ?>
              <input title="It is necessary to have results of all the pathogenic agents to consolidate an evaluation" type="checkbox" checked name=<?= $cbx_name ?>>
            <?php } else { ?>
              <input title="It is necessary to have results of all the pathogenic agents to consolidate an evaluation" type="checkbox" disabled name=<?= $cbx_name ?>>
            <?php }  ?>
            <span class="control-label" id="">consolidate</span>
          </label>
        </small>
      </h4>
      <?php foreach ($agent_doc_array as $agent => $doc) { ?>
        <li><?= Html::encode($agent) ?> - <?= Html::encode($doc) ?></li>
      <?php } ?>
    <?php } ?>
  </div>


</div>
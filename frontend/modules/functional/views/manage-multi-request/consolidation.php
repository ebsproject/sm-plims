<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Consolidate Results';
$this->params['breadcrumbs'][] = ['label' => 'Multi Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] =  $this->title . ' (' . $event_code . ')';
?>
<style>
  .val-req-pos-result {
    border: 0.5px solid #a7a7a7;

    width: 16px;
    height: 8px;
    border-radius: 2px;
    text-align: center;
    margin: 1.5px;
  }

  .val-req-pos-empty {
    border: 0.5px solid #a7a7a7;
    width: 16px;
    height: 8px;
    border-radius: 2px;
    text-align: center;
    margin: 1.5px;
    background-color: #f1f1f1;
  }

  .content-link {
    padding: 3px;
  }

  .content-link:hover {
    background-color: #2675ec;
    border-radius: 5px;
  }

  .val-req-pos-result-lg {
    width: 116px;
    height: 50px;
    border-radius: 4px;
    text-align: center;
    border: 0.5px solid #a7a7a7;
  }

  .val-req-pos-empty-lg {
    width: 116px;
    height: 50px;
    border-radius: 4px;
    text-align: center;
    border: 0.5px solid #a7a7a7;
    background-color: #f1f1f1;
  }
</style>






<?php $form = ActiveForm::begin(); ?>
<div class="consolidation">
  <div class="row">
    <div class="col-lg-12">
      <h1><?= Html::encode($this->title) . ' (<span class="text-blue-link">' . $event_code . '</span>)' ?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card card-outline">
        <div class="card-header">
          <h3 class="card-title text-blue-link">Consolidated Absorbance Result</h3>
        </div>




        <div class="card-body">
          <div class="row">

            <div class="col-xl-3" style="overflow: auto;">
              <div style="text-align: center;">
                <h3><?= $event_code ?></h3>
              </div>
              <!-- key1 = agent_id|agent_name -->
              <?php foreach ($rdl_001 as $key1 => $value1) { ?>
                <div style="display: -webkit-inline-box; width: 100%; margin-bottom: 2px">


                  <div style="width: 60px; margin: 2px; text-align: center; padding-top: 45px;">

                    <span><?= explode("|", $key1)[1] ?></span>
                  </div>
                  <!-- key2/value2 = agent_id|plate_name -->
                  <?php foreach ($value1 as $key2 => $value2) {
                    $href = "consolidation?event_id=" . $event_id . "&index_lvl_2=" . $value2; ?>

                    <!-- mini support row -->
                    <div class="content-link">
                      <a href=<?= $href ?> style="color: #000; text-decoration: none;">
                        <div style="width: 122px; border: 2px #bbb solid; border-radius: 5px; box-shadow: -1px 1px 2px rgb(0 0 0 / 20%); background-color: #fff;">
                          <table style="margin: 2px;">
                            <?php for ($row = 0; $row < 8; $row++) {  ?>
                              <tr>
                                <?php for ($col = 0; $col < 6; $col++) {
                                  $rd_index = 16 * $col + $row + 5;
                                  $support_position =  $value2 . "." . $rd_index;
                                  if (isset($reading_data_list[$support_position])) {
                                    $class = "val-req-pos-result";
                                    $style = $reading_data_list[$support_position]["bg_color"];
                                  } else {
                                    $class = "val-req-pos-empty";
                                    $style = "";
                                  }
                                ?>
                                  <td colspan="2">
                                    <div class=<?= $class ?> style=<?= $style ?>>
                                    </div>
                                  </td>
                                <?php } ?>
                              </tr>
                            <?php } ?>
                          </table>
                        </div>
                      </a>
                    </div>

                  <?php } ?>
                </div>
              <?php } ?>
            </div>

            <div class="col-xl-6">
              <div style="text-align: center;">
                <h3><?= explode("|", $index_lvl_2)[1] ?></h3>
              </div>
              <div style="width: 740px; margin: auto; border: 2px #bbb solid; border-radius: 5px; box-shadow: -1px 1px 2px rgb(0 0 0 / 20%); background-color: #fff;">
                <table style="margin: auto;">
                  <?php for ($row = 0; $row < 8; $row++) { ?>
                    <tr>
                      <?php for ($col = 0; $col < 6; $col++) {
                        $rd_index = 16 * $col + $row + 5;
                        $support_position =  $index_lvl_2 . "." . $rd_index;
                        if (isset($reading_data_list[$support_position])) {
                          $class = "val-req-pos-result-lg";
                          $style = $reading_data_list[$support_position]["bg_color"];
                          $sample_name = $sample_name = $reading_data_list[$support_position]["request_code_title"] . '.' . $reading_data_list[$support_position]["sample_name"];
                          $short_sample_type = $reading_data_list[$support_position]["short_sample_type"];
                          $original_result = $reading_data_list[$support_position]["original_result"];
                          $class_original_result = 'class= "label label-' . $reading_data_list[$support_position]["original_result"] . '"';
                          $indexlvl_1  = $reading_data_list[$support_position]["index_lvl_1"];
                          $href = "consolidation?event_id=" . $event_id . "&index_lvl_2=" . $index_lvl_2 . "&index_lvl_1=" . $indexlvl_1;
                        } else {
                          $class = "val-req-pos-empty-lg";
                          $style = "";
                          $sample_name = "";
                          $short_sample_type = "";
                          $original_result = "";
                          $class_original_result = "";
                          $indexlvl_1  = "";
                          $href = "consolidation?event_id=" . $event_id . "&index_lvl_2=" . $index_lvl_2;
                        }
                      ?>
                        <td colspan="2">
                          <div class="content-link">
                            <a href=<?= $href ?> style="color: #000; text-decoration: none;">
                              <div class=<?= $class ?> style=<?= $style ?>>
                                <span><?= $sample_name ?></span><br>
                                <span><?= $short_sample_type ?></span>
                                <span <?= $class_original_result ?>><?= $original_result ?></span>
                              </div>
                            </a>
                          </div>
                        </td>
                      <?php } ?>
                    </tr>
                  <?php } ?>
                </table>
              </div>
            </div>

            <div class="col-xl-3">

              <?php

              if (isset($reading_data_list[$index_lvl_1])) {
                $agent_name = $reading_data_list[$index_lvl_1]["agent_name"];
                $support_name = $reading_data_list[$index_lvl_1]["agent_name"];
                $request_code = $reading_data_list[$index_lvl_1]["request_code_title"];
                $process = $reading_data_list[$index_lvl_1]["process"];
                $long_sample_type = $reading_data_list[$index_lvl_1]["long_sample_type"];
                $sample_name = $reading_data_list[$index_lvl_1]["request_code_title"] . '.' . $reading_data_list[$index_lvl_1]["sample_name"];
                $original_result = $reading_data_list[$index_lvl_1]["original_result"];
                $class_original_result = 'class= "label label-' . $reading_data_list[$index_lvl_1]["original_result"] . '"';
                $auxiliary_result = $reading_data_list[$index_lvl_1]["auxiliary_result"];
                $class_auxiliary_result = 'class= "label label-' . $reading_data_list[$index_lvl_1]["auxiliary_result"] . '"';
                $reading_data_ids = $reading_data_list[$index_lvl_1]["reading_data_ids"];
                $observation =  explode('|', $reading_data_list[$index_lvl_1]["observation"])[0];
              } else {
                $agent_name = "";
                $support_name = "";
                $request_code = "";
                $process = "";
                $long_sample_type = "";
                $sample_name = "";
                $original_result = "";
                $class_original_result = "";
                $auxiliary_result = "";
                $class_auxiliary_result = "";
                $reading_data_ids = "";
                $observation = "";
              }
              ?>
              <h3>Detail information</h3>
              <p>
                <label class="control-label">Agent:</label>
                <span><?= $agent_name ?></span>
              </p>
              <p>
                <label class="control-label">Support:</label>
                <span><?= $support_name ?></span>
              </p>
              <p>
                <label class="control-label">Request:</label>
                <span><?= $request_code ?></span>
              </p>
              <p>
                <label class="control-label">Process:</label>
                <span><?= $process ?></span>
              </p>
              <p>
                <label class="control-label">Sample type:</label>
                <span><?= $long_sample_type ?></span>
              </p>
              <p>
                <label class="control-label">Sample name:</label>
                <span><?= $sample_name ?></span>
              </p>

              <?php if ($is_in_process) { ?>
                <h3>Correction of results</h3>

                <p>
                  <label class="control-label">Last modified Result:</label>
                  <span <?= $class_auxiliary_result ?>><?= $auxiliary_result ?></span>
                </p>

                <div>
                  <label class="control-label">Observation:</label>
                  <span><?= $observation ?></span>
                </div>

                <p>
                  <?= Select2::widget(
                    [
                      'id' => 'result_id',
                      'name' => 'result_name',
                      'data' => [
                        'negative' => 'negative',
                        'positive' => 'positive',
                        'doubt' => 'doubt',
                      ],
                      'value' =>   $auxiliary_result,
                    ]
                  );
                  ?>
                </p>


                <p>
                <div>
                  <label class="control-label">Observation:</label>
                  <?= Html::textArea(
                    'observation',
                    $observation,
                    [
                      'class' => 'form-control',
                      'rows' => 3
                    ]
                  ) ?>
                </div>
                </p>
              <?php } else { ?>
                <p>
                  <label class="control-label">Original Result:</label>
                  <span <?= $class_original_result ?>><?= $original_result ?></span>
                </p>
                <p>
                  <label class="control-label">Last modified Result:</label>
                  <span <?= $class_auxiliary_result ?>><?= $auxiliary_result ?></span>
                </p>

              <?php } ?>

            </div>
          </div>
        </div>

        <?php if ($is_in_process) { ?>
          <div class="card-footer">
            <div class="row">
              <div class="col-lg-12">
                <div class="btn-group pull-right">
                  <?= Html::a(
                    'Finish',
                    [
                      'finish',
                      'event_id' =>  $event_id,
                    ],
                    [
                      'class' => 'btn btn-success',
                      'title'        => 'Finish process',
                      'data-method'  => 'post',
                    ]
                  ); ?>
                  <?= Html::a(
                    'Save',
                    [
                      'save',
                      'event_id' =>  $event_id,
                      'index_lvl_2' => $index_lvl_2,
                      'index_lvl_1' => $index_lvl_1,
                      'reading_data_ids' => $reading_data_ids,
                    ],
                    [
                      'class' => 'btn btn-primary',
                      'title'        => 'Save',
                      'data-method'  => 'post',
                      'disabled' => ($event_id === null || $index_lvl_2 === null || $index_lvl_1 === null || $reading_data_ids === null) ? true : false,
                    ]
                  ); ?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>




      </div>
    </div>
  </div>
</div>
<?php ActiveForm::end() ?>
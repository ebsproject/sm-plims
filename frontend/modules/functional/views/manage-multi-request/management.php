<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Manage Multi Request';
$this->params['breadcrumbs'][] = ['label' => 'Multi Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] =  $this->title . ' (' . $event_code . ')';
$modal_title = 'Consolidate absorbances';

?>
<style>
  .val-req-pos-empty {
    border: 0.5px solid #a7a7a7;
    width: 55px;
    height: 55px;
    border-radius: 2px;
    text-align: center;
    margin: 1.5px auto;
    font-size: 10px;
    background-color: #f1f1f1;
  }

  .val-req-pos-result:hover {
    width: 120px;
    height: 120px;
    border-radius: 4px;
    font-size: 25px;
    position: absolute;
    margin-top: -60px;
    margin-left: -30px;
    z-index: 1;
  }

  .val-req-pos-result {
    border: 0.5px solid #a7a7a7;
    width: 55px;
    height: 55px;
    border-radius: 2px;
    text-align: center;
    margin: 1.5px auto;
  }

  .label-positive {
    background-color: #dd4b39 !important;
  }

  .label-negative {
    background-color: #008d4c !important;
  }

  .label-doubt {
    background-color: #F39C12 !important;
  }

  .label-buffer {
    background-color: #686868 !important;
  }

  .table-left {
    padding-right: 4px;
  }

  .table-right {
    padding-left: 4px;
  }
</style>


<div class="management">

  <!-- MODAL-INI -->
  <?php $form = ActiveForm::begin(['id' => 'form-manage-multi-request-management-modal', 'options' => ['class' => 'class-manage-multi-request-management-modal'],]) ?>

  <div class="modal fade" id="modal-view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?= Html::encode($modal_title) ?></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="modal-view-content"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <?= Html::a(
            'Consolidate absorbances',
            [
              'consolidate-absorbances',
              'event_id' => $event_id,
            ],
            [
              'data-method' => 'post',
              'class' => 'btn btn-success',
            ]
          ); ?>
        </div>
      </div>
    </div>
  </div>

  <?php ActiveForm::end() ?>
  <!-- MODAL-END -->

  <div class="row">
    <div class="col-lg-12">
      <h1><?= Html::encode($this->title) . ' (<span class="text-blue-link">' . $event_code . '</span>)' ?></h1>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="card card-outline card-secondary">
        <div class="card-header">
          <h3 class="card-title text-blue-link">Distribution</h3>
        </div>




        <div class="card-body">
          <!-- ---------------------------------------- -->
          <?php $form = ActiveForm::begin(['id' => 'form-manage-multi-request-management', 'options' => ['class' => 'class-manage-multi-request-management'],]) ?>
          <div class="row" style="margin-bottom: 10px;">
            <div class="col-lg-4">
              <label class="control-label">Agent: </label>
              <?= Select2::widget(
                [
                  'id' => 'agent_id',
                  'name' => 'agent_name',
                  'data' => $agent_list,
                  'value' =>  $agent_id,
                  'options' => [
                    'placeholder' => 'Select agent ...'
                  ],
                ]
              );
              ?>
            </div>
            <div class="col-lg-2">
              <?= Html::a(
                '<i class="fa fa-eye"></i> See Distribution',
                [
                  'management',
                  'event_id' => Yii::$app->request->get('event_id'),
                ],
                [
                  'class' => 'pull-right btn btn-primary',
                  'style' => 'margin-top: 25px',
                  'data-method'  => 'post',
                ]
              ); ?>
            </div>
            <div class="col-lg-6">
            </div>
          </div>
          <?php ActiveForm::end() ?>
          <!-- ---------------------------------------- -->
        </div>
      </div>
    </div>
  </div>

  <?php if ($agent_id) { ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h3 class="card-title text-blue-link">Absorbances Results</h3>
          </div>
          <?php if ($is_pending || $is_in_process) { ?>
            <div class="card-header">
              <?php $form = ActiveForm::begin(['id' => 'form-manage-multi-request-management-upload', 'options' => ['class' => 'class-manage-multi-request-management-upload'],]) ?>


              <div class="row">
                <div class="col-lg-4">
                  <label class="control-label">Support-Evaluation(s): </label>
                  <?= Select2::widget(
                    [
                      'id' => 'support_activity_id',
                      'name' => 'support_activity_name',
                      'data' => $activity_support_list,
                      'value' =>   key($activity_support_list),
                      'options' => [
                        'placeholder' => 'Select one ...'
                      ],
                    ]
                  );
                  ?>
                </div>
                <div class="col-lg-2">
                </div>
                <div class="col-lg-4">
                  <div class="input-group" title="File extensions: xls, xlsx">
                    <?= $form->field($model_upload_absorbance, 'mixFiles[]')->fileInput(['multiple' => true]) ?>
                  </div>
                </div>
                <div class="col-lg-2">
                  <?= Html::a(
                    '<i class="fa fa-upload"></i> Upload Absorbance',
                    [
                      'upload-absorbance',
                      'event_id' => $event_id,
                      'agent_id' => $agent_id,
                    ],
                    [
                      'title' => 'Upload file',
                      'style' => 'margin-top: 20px',
                      'data-method' => 'post',
                      'class' => 'btn-sm btn-default pull-right',
                    ]
                  ); ?>
                </div>
              </div>


              <?php ActiveForm::end() ?>
            </div>
          <?php } ?>
          <div class="card-body">
            <div class="row">
              <?php foreach ($activity_support_cell_position_reading_list as $key => $array_value) { ?>
                <div <?= (substr($key, -1) == '1' ? 'class="col-md-12 col-lg-12 col-xl-6 table-left"' : 'class="col-md-12 col-lg-12 col-xl-6 table-right"') ?>>
                  <div class="row">
                    <div class="col-lg-6">
                      <h4><?= $key ?></h4>
                    </div>
                    <div class="col-lg-6">
                      <?php if (isset($absorbance_file_list[$key])) { ?>
                        <a style="margin: 10px 0px; position: absolute; right: 20px;" title="<?= isset($absorbance_file_list[$key]) ? $absorbance_file_list[$key] : '' ?>" href="<?= Url::base() ?>/<?= isset($absorbance_file_list[$key]) ? $absorbance_file_list[$key] : '' ?>">
                          <i class="fa fa-file-excel-o"></i> Download absorbance
                        </a>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <table style="width: 100%; border: 0.5px solid #a7a7a7;">
                        <tr>
                          <th>R\C</th>
                          <th>1</th>
                          <th>2</th>
                          <th>3</th>
                          <th>4</th>
                          <th>5</th>
                          <th>6</th>
                          <th>7</th>
                          <th>8</th>
                          <th>9</th>
                          <th>10</th>
                          <th>11</th>
                          <th>12</th>
                        </tr>
                        <?php for ($row = 0; $row < 8; $row++) {  ?>
                          <tr>
                            <td style="text-align: center;">
                              <h4><?= $rows[$row] ?></h4>
                            </td>
                            <?php for ($col = 1; $col <= 12; $col++) { ?>
                              <!-- ---------------------------------------- -->
                              <?php
                              $position_text = $rows[$row] . $col;
                              $position = 8 * ($col - 1) + $row + 1;
                              if (isset($array_value[$position])) {
                                $reading_data_id = $array_value[$position];
                                $class =   'val-req-pos-result';
                                $title = $reading_data_id;
                                $request_code = $reading_data_list[$reading_data_id]['request_code'];
                                $sample_name = $reading_data_list[$reading_data_id]['sample_name'];
                                $order =  $reading_data_list[$reading_data_id]['order'];
                                $style = $reading_data_list[$reading_data_id]['bg_color'];
                                $class_result = $reading_data_list[$reading_data_id]['class_result'];
                                $number_result =  $reading_data_list[$reading_data_id]['number_result'];
                              } else {
                                $class = 'val-req-pos-empty';
                                $title = '';
                                $request_code = '';
                                $sample_name = '';
                                $order = '';
                                $style = '';
                                $class_result = '';
                                $number_result = '';
                              } ?>
                              <!-- ---------------------------------------- -->
                              <td>
                                <div class=<?= $class ?> style=<?= $style ?>>
                                  <div style="flex-direction: row; box-sizing: border-box; display: flex; place-content: flex-start space-between; align-items: flex-start;">
                                    <div style="font-size: 0.75em; display: block;"><?= $position_text ?></div>
                                    <div style="font-size: 0.75em; display: block;"><?= $order ?></div>
                                  </div>
                                  <div style="margin-top: -4px;">
                                    <span style="font-size:0.75em; display: block; font-weight: 700;"><?= $request_code ?></span>
                                  </div>
                                  <div style="margin-top: -4px;">
                                    <span style="font-size:0.75em; display: block; font-weight: 700;"><?= $sample_name ?></span>
                                  </div>
                                  <div style="margin-top: -6px;">
                                    <small>
                                      <span class='<?= $class_result ?>'><?= $number_result ?></span>
                                    </small>
                                  </div>
                                </div>
                              </td>
                              <!-- ---------------------------------------- -->
                            <?php } ?>
                          </tr>
                        <?php } ?>
                      </table>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
          <?php if ($is_documents_complete && ($is_pending || $is_in_process)) { ?>
            <div class="card-footer">
              <div class="row">
                <div class="col-lg-12">
                  <div class="btn-group pull-right">

                    <?= Html::a(
                      'Consolidate',
                      '#',
                      [
                        'class' => 'btn btn-primary modal-view-action',
                        'value' => Url::to(
                          [
                            'consolidation-preview',
                            'event_id' => $event_id,
                            'agents' => count($agent_list),
                          ]
                        )
                      ]
                    ); ?>

                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  <?php } ?>

</div>
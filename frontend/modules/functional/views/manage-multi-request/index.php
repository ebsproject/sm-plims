<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Multi Requests';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
  .table-bordered {
    text-align: center;
  }
</style>

<div class="index">

  <div class="row">
    <div class="col-lg-12">
      <h1><?= Html::encode($this->title) ?></h1>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="card card-outline card-secondary">
        <div class="card-body">


          <?php Pjax::begin(); ?>
          <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}{summary}{pager}",
            'columns' => [
              ['class' => 'yii\grid\SerialColumn'],

              'event_code',
              'request_codes',
              'crop_names',
              'distribution_names',

              [
                'class' => 'yii\grid\ActionColumn',
                'header'        => 'Action',
                'template'      => '{link} {consolidation}',
                'headerOptions' => ['width' => '80'],
                'buttons'       => [
                  'link' => function ($url, $model, $key) {
                    return Html::a(
                      ("<i class='fa fa-eye' title='See multi request distribution'></i>"),

                      [
                        'manage-multi-request/management',
                        'event_id' => $key,
                      ],
                      [
                        'class' => 'btn btn-default btn-xs',
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    );
                  },

                  'consolidation' => function ($url, $model, $key) {
                    return Html::a(
                      ("<i class='fa fa-circle-o-notch' title='See consolidation'></i>"),

                      [
                        'manage-multi-request/consolidation',
                        'event_id' => $key,
                      ],
                      [
                        'class' => 'btn btn-primary btn-xs',
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    );
                  },
                ],
              ],
            ],
          ]); ?>
          <?php Pjax::end(); ?>


        </div>
      </div>
    </div>
  </div>

</div>
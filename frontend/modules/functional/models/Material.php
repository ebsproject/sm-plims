<?php

namespace frontend\modules\functional\models;

use Yii;
use common\models\User;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Parameter;
use yii\helpers\ArrayHelper;


class Material extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_material';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['composite_sample_id', 'composite_sample_type_id', 'composite_sample_group_id', 'crop_id', 'material_type_id', 'material_unit_id', 'num_order', 'composite_sample_order', 'material_unit_qty', 'grafting_order', 'registered_by', 'updated_by', 'deleted_by', 'material_group_id', 'material_status_id', 'historical_data_version'], 'default', 'value' => null],
            [['composite_sample_id', 'composite_sample_type_id', 'composite_sample_group_id', 'crop_id', 'material_type_id', 'material_unit_id', 'num_order', 'composite_sample_order', 'material_unit_qty', 'grafting_order', 'registered_by', 'updated_by', 'deleted_by', 'material_group_id', 'material_status_id', 'historical_data_version'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['short_name', 'long_name', 'origin'], 'string', 'max' => 50],
            [['observation', 'details'], 'string', 'max' => 245],
            [['material_code_a', 'material_code_b', 'material_code_c'], 'string', 'max' => 145],
            [['status'], 'string', 'max' => 15],
            [['registered_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['registered_by' => 'user_id']],
            [['composite_sample_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompositeSample::class, 'targetAttribute' => ['composite_sample_id' => 'composite_sample_id']],
            [['composite_sample_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompositeSampleGroup::class, 'targetAttribute' => ['composite_sample_group_id' => 'composite_sample_group_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['material_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialGroup::class, 'targetAttribute' => ['material_group_id' => 'material_group_id']],
            [['composite_sample_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['composite_sample_type_id' => 'parameter_id']],
            [['material_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['material_type_id' => 'parameter_id']],
            [['material_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['material_unit_id' => 'parameter_id']],
            [['material_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['material_status_id' => 'parameter_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'material_id' => 'Material ID',
            'composite_sample_id' => 'Composite Sample ID',
            'composite_sample_type_id' => 'Composite Sample Type ID',
            'composite_sample_group_id' => 'Composite Sample Group ID',
            'crop_id' => 'Crop ID',
            'material_type_id' => 'Material Type ID',
            'material_unit_id' => 'Material Unit ID',
            'num_order' => 'Orden Material',
            'composite_sample_order' => 'Composite Sample Order',
            'material_unit_qty' => 'Material Unit Qty',
            'short_name' => 'Material Name',
            'long_name' => 'Long Name',
            'observation' => 'Observation',
            'details' => 'Details',
            'origin' => 'Origin',
            'grafting_order' => 'Grafting Order',
            'material_code_a' => 'Material Code',
            'material_code_b' => 'Material Code B',
            'material_code_c' => 'Material Code C',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'material_group_id' => 'Material Group ID',
            'material_status_id' => 'Material Status ID',
            'historical_data_version' => 'Historical Data Version',
        ];
    }

    /**
     * Gets query for [[CompositeSample]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSample()
    {
        return $this->hasOne(CompositeSample::class, ['composite_sample_id' => 'composite_sample_id']);
    }

    /**
     * Gets query for [[CompositeSampleGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleGroup()
    {
        return $this->hasOne(CompositeSampleGroup::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    /**
     * Gets query for [[CompositeSampleType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'composite_sample_type_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[MaterialGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialGroup()
    {
        return $this->hasOne(MaterialGroup::class, ['material_group_id' => 'material_group_id']);
    }

    /**
     * Gets query for [[MaterialStatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialStatus()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'material_status_id']);
    }

    /**
     * Gets query for [[MaterialType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'material_type_id']);
    }

    /**
     * Gets query for [[MaterialUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialUnit()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'material_unit_id']);
    }

    /**
     * Gets query for [[RegisteredBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegisteredBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'registered_by']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

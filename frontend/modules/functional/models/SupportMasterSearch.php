<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\SupportMaster;

/**
 * SupportMasterSearch represents the model behind the search form of `frontend\modules\functional\models\SupportMaster`.
 */
class SupportMasterSearch extends SupportMaster
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['support_master_id', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['support_code', 'detail', 'registered_at', 'updated_at', 'deleted_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SupportMaster::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'support_master_id' => $this->support_master_id,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['ilike', 'support_code', $this->support_code])
            ->andFilterWhere(['ilike', 'detail', $this->detail])
            ->andFilterWhere(['ilike', 'status', $this->status]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\functional\models;

use Yii;
use common\models\User;
use frontend\modules\configuration\models\Parameter;

class Status extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'request_status_id'], 'required'],
            [['request_id', 'request_status_id', 'number_index', 'status_by'], 'default', 'value' => null],
            [['request_id', 'request_status_id', 'number_index', 'status_by'], 'integer'],
            [['status_date'], 'safe'],
            [['observation'], 'string', 'max' => 500],
            [['request_id', 'request_status_id'], 'unique', 'targetAttribute' => ['request_id', 'request_status_id']],
            [['status_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['status_by' => 'user_id']],
            [['request_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_status_id' => 'parameter_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'request_status_id' => 'Request Status ID',
            'number_index' => 'Number Index',
            'status_date' => 'Status Date',
            'status_by' => 'Status By',
            'observation' => 'Observation',
        ];
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestStatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestStatus()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_status_id']);
    }

    /**
     * Gets query for [[StatusBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatusBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'status_by']);
    }
}

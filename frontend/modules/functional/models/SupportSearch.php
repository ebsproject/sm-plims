<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\Support;

/**
 * SupportSearch represents the model behind the search form of `frontend\modules\functional\models\Support`.
 */
class SupportSearch extends Support
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['support_order_id', 'request_id', 'num_order_id', 'essay_id', 'activity_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'support_type_id', 'support_columns', 'support_rows', 'total_slots', 'used_slots', 'available_slots', 'ctrl_qty', 'registered_by', 'updated_by', 'deleted_by', 'support_id', 'request_process_essay_activity_id'], 'integer'],
            [['support_code', 'registered_at', 'updated_at', 'deleted_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Support::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'support_order_id' => $this->support_order_id,
            'request_id' => $this->request_id,
            'num_order_id' => $this->num_order_id,
            'essay_id' => $this->essay_id,
            'activity_id' => $this->activity_id,
            'composite_sample_type_id' => $this->composite_sample_type_id,
            'crop_id' => $this->crop_id,
            'workflow_id' => $this->workflow_id,
            'support_type_id' => $this->support_type_id,
            'support_columns' => $this->support_columns,
            'support_rows' => $this->support_rows,
            'total_slots' => $this->total_slots,
            'used_slots' => $this->used_slots,
            'available_slots' => $this->available_slots,
            'ctrl_qty' => $this->ctrl_qty,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'support_id' => $this->support_id,
            'request_process_essay_activity_id' => $this->request_process_essay_activity_id,
        ]);

        $query->andFilterWhere(['ilike', 'support_code', $this->support_code])
            ->andFilterWhere(['ilike', 'status', $this->status]);

        return $dataProvider;
    }
}

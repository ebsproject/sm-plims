<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\CompositeSample;

/**
 * CompositeSampleSearch represents the model behind the search form of `frontend\modules\functional\models\CompositeSample`.
 */
class CompositeSampleSearch extends CompositeSample
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['composite_sample_id', 'composite_sample_type_id', 'composite_sample_group_id', 'crop_id', 'num_order', 'material_qty', 'material_unit_qty', 'registered_by', 'updated_by', 'deleted_by', 'reading_data_type_id', 'material_group_id'], 'integer'],
            [['short_name', 'registered_at', 'updated_at', 'deleted_at', 'status', 'long_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompositeSample::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'composite_sample_id' => $this->composite_sample_id,
            'composite_sample_type_id' => $this->composite_sample_type_id,
            'composite_sample_group_id' => $this->composite_sample_group_id,
            'crop_id' => $this->crop_id,
            'num_order' => $this->num_order,
            'material_qty' => $this->material_qty,
            'material_unit_qty' => $this->material_unit_qty,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'reading_data_type_id' => $this->reading_data_type_id,
            'material_group_id' => $this->material_group_id,
        ]);

        $query->andFilterWhere(['ilike', 'short_name', $this->short_name])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'long_name', $this->long_name]);

        return $dataProvider;
    }
}

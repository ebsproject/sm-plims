<?php

namespace frontend\modules\functional\models;

use Yii;
use yii\helpers\ArrayHelper;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Workflow;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\Agent;
use frontend\modules\configuration\models\AgentByEssay;
use frontend\modules\configuration\models\ActivityByEssay;
use common\models\User;

class RequestProcessEssay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_request_process_essay';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'essay_id', 'composite_sample_type_id'], 'required'],
            [['request_id', 'num_order_id', 'essay_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'agent_qty', 'registered_by', 'deleted_by', 'num_order', 'sample_qty', 'request_process_essay_status_id', 'request_process_essay_type_id', 'activity_qty', 'request_process_essay_root_id', 'support_master_id', 'start_by', 'finish_by', 'check_by'], 'default', 'value' => null],
            [['request_id', 'num_order_id', 'essay_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'agent_qty', 'registered_by', 'deleted_by', 'num_order', 'sample_qty', 'request_process_essay_status_id', 'request_process_essay_type_id', 'activity_qty', 'request_process_essay_root_id', 'support_master_id', 'start_by', 'finish_by', 'check_by'], 'integer'],
            [['start_date', 'finish_date', 'check_date', 'registered_at', 'deleted_at'], 'safe'],
            [['agent_detail', 'sample_detail', 'activity_detail', 'observation'], 'string'],
            [['sub_total_cost_essay'], 'number'],
            [['is_deleted'], 'boolean'],
            [['status'], 'string', 'max' => 15],
            [['start_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['start_by' => 'user_id']],
            [['finish_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['finish_by' => 'user_id']],
            [['check_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['check_by' => 'user_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['composite_sample_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['composite_sample_type_id' => 'parameter_id']],
            [['request_process_essay_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_process_essay_status_id' => 'parameter_id']],
            [['request_process_essay_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_process_essay_type_id' => 'parameter_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
            [['request_id', 'num_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcess::class, 'targetAttribute' => ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']],
            [['support_master_id'], 'exist', 'skipOnError' => true, 'targetClass' => SupportMaster::class, 'targetAttribute' => ['support_master_id' => 'support_master_id']],
            [['workflow_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workflow::class, 'targetAttribute' => ['workflow_id' => 'workflow_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'num_order_id' => 'Num Order ID',
            'essay_id' => 'Essay ID',
            'composite_sample_type_id' => 'Composite Sample Type ID',
            'crop_id' => 'Crop ID',
            'workflow_id' => 'Workflow ID',
            'start_date' => 'Start Date',
            'finish_date' => 'Finish Date',
            'check_date' => 'Check Date',
            'agent_qty' => 'Agent Qty',
            'agent_detail' => 'Agent Detail',
            'sub_total_cost_essay' => 'Sub Total Cost Essay',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'num_order' => 'Num Order',
            'sample_qty' => 'Sample Qty',
            'sample_detail' => 'Sample Detail',
            'request_process_essay_status_id' => 'Request Process Essay Status ID',
            'request_process_essay_type_id' => 'Request Process Essay Type ID',
            'activity_qty' => 'Activity Qty',
            'activity_detail' => 'Activity Detail',
            'observation' => 'Observation',
            'request_process_essay_root_id' => 'Request Process Assay Root ID',
            'support_master_id' => 'Support Master ID',
            'request_process_essay_id' => 'Request Process Essay ID',
            'start_by' => 'Start By',
            'finish_by' => 'Finish By',
            'check_by' => 'Check By',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[CheckBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCheckBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'check_by']);
    }

    /**
     * Gets query for [[CompositeSampleType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'composite_sample_type_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[Essay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    /**
     * Gets query for [[FinishBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFinishBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'finish_by']);
    }

    /**
     * Gets query for [[ReadingDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['request_process_essay_id' => 'request_process_essay_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[Request0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcess()
    {
        return $this->hasOne(RequestProcess::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivities]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivities()
    {
        return $this->hasMany(RequestProcessEssayActivity::class, ['request_process_essay_id' => 'request_process_essay_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayAgents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayAgents()
    {
        return $this->hasMany(RequestProcessEssayAgent::class, ['request_process_essay_id' => 'request_process_essay_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayStatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayStatus()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_process_essay_status_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_process_essay_type_id']);
    }

    /**
     * Gets query for [[StartBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStartBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'start_by']);
    }

    /**
     * Gets query for [[SupportMaster]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupportMaster()
    {
        return $this->hasOne(SupportMaster::class, ['support_master_id' => 'support_master_id']);
    }

    /**
     * Gets query for [[Workflow]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(Workflow::class, ['workflow_id' => 'workflow_id']);
    }

    // public function getReadingDatasReprocess()
    // {
    //     return $this->hasMany(
    //         ReadingData::class,
    //         [
    //             'request_id' => 'request_id',
    //             'num_order_id' => 'num_order_id',
    //             'essay_id' => 'essay_id',
    //             'composite_sample_type_id' => 'composite_sample_type_id'
    //         ]
    //     )->andOnCondition(['observation' => 'R']);;
    // }

    public function getRequestDetail()
    {
        return $this->hasOne(RequestDetail::class, ['request_id' => 'request_id']);
    }

    public function getRequestProcessEssayActivitySamples()
    {
        return $this->hasMany(RequestProcessEssayActivitySample::class, ['request_process_essay_id' => 'request_process_essay_id']);
    }

    public function getEssays()
    {
        return $this->hasMany(ActivityByEssay::class, ['essay_id' => 'essay_id', 'activity_id' => 'activity_id'])->viaTable('{{%request_process_essay_activity}}', ['request_id' => 'request_id', 'num_order_id' => 'num_order_id', 'essay_id' => 'essay_id', 'composite_sample_type_id' => 'composite_sample_type_id']);
    }

    public function getActivities()
    {
        return $this->hasMany(
            Activity::class,
            [
                'activity_id' => 'activity_id'
            ]
        )
            ->viaTable(
                '{{%request_process_essay_activity}}',
                [
                    'request_id' => 'request_id',
                    'num_order_id' => 'num_order_id',
                    'essay_id' => 'essay_id',
                    'composite_sample_type_id' => 'composite_sample_type_id'
                ]
            )
            ->orderBy(['activity_id' => SORT_ASC]);
    }

    public function getAgents()
    {
        return $this->hasMany(Agent::class, ['agent_id' => 'agent_id'])->viaTable('{{%request_process_essay_agent}}', ['request_id' => 'request_id', 'num_order_id' => 'num_order_id', 'essay_id' => 'essay_id', 'composite_sample_type_id' => 'composite_sample_type_id']);
    }

    public function getAgentByEssays()
    {
        return $this->hasMany(AgentByEssay::class, ['essay_id' => 'essay_id', 'agent_id' => 'agent_id'])->viaTable('{{%request_process_essay_agent}}', ['request_id' => 'request_id', 'num_order_id' => 'num_order_id', 'essay_id' => 'essay_id', 'composite_sample_type_id' => 'composite_sample_type_id']);
    }

    public function getSupports()
    {
        return $this->hasMany(Support::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id', 'essay_id' => 'essay_id', 'composite_sample_type_id' => 'composite_sample_type_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = $this->getUserId();
            $this->registered_at = date("Y-m-d H:i:s", time());
            // $this->status = "active";
        }
        return parent::beforeSave($insert);
    }

    private function getUserId()
    {
        $user_id =  null;
        $_token = Yii::$app->request->headers->get('authorization');

        if (!is_null($_token)) {

            $token = str_replace('Bearer ', '', $_token);
            $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
        } else {
            $user_id = \Yii::$app->user->identity->id;
        }
        return $user_id;
    }


    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

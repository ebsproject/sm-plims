<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\MaterialGroup;

/**
 * MaterialGroupSearch represents the model behind the search form of `frontend\modules\functional\models\MaterialGroup`.
 */
class MaterialGroupSearch extends MaterialGroup
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['material_group_id', 'number_index', 'material_qty', 'material_unit_qty', 'num_order_ini', 'num_order_end', 'composite_sample_group_id'], 'integer'],
            [['group_name', 'sample_group_name', 'material_name_ini', 'material_name_end'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MaterialGroup::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'material_group_id' => $this->material_group_id,
            'number_index' => $this->number_index,
            'material_qty' => $this->material_qty,
            'material_unit_qty' => $this->material_unit_qty,
            'num_order_ini' => $this->num_order_ini,
            'num_order_end' => $this->num_order_end,
            'composite_sample_group_id' => $this->composite_sample_group_id,
        ]);

        $query->andFilterWhere(['ilike', 'group_name', $this->group_name])
            ->andFilterWhere(['ilike', 'sample_group_name', $this->sample_group_name])
            ->andFilterWhere(['ilike', 'material_name_ini', $this->material_name_ini])
            ->andFilterWhere(['ilike', 'material_name_end', $this->material_name_end]);

        return $dataProvider;
    }
}

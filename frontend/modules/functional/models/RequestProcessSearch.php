<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\RequestProcess;

/**
 * RequestProcessSearch represents the model behind the search form of `frontend\modules\functional\models\RequestProcess`.
 */
class RequestProcessSearch extends RequestProcess
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'crop_id', 'workflow_id', 'composite_sample_group_id', 'essay_qty', 'agent_qty', 'material_qty', 'sample_qty', 'request_process_status_id', 'request_process_material_id', 'request_process_payment_id', 'registered_by', 'deleted_by', 'material_unit_qty', 'start_by', 'finish_by', 'check_by'], 'integer'],
            [['start_date', 'finish_date', 'check_date', 'essay_detail', 'agent_detail', 'material_detail', 'sample_detail', 'registered_at', 'deleted_at', 'status', 'child_crop', 'child_crop_other', 'request_process_other', 'registration_out_of_the_system'], 'safe'],
            [['payment_percentage', 'sub_total_cost'], 'number'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestProcess::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'request_id' => $this->request_id,
            'num_order_id' => $this->num_order_id,
            'crop_id' => $this->crop_id,
            'workflow_id' => $this->workflow_id,
            'composite_sample_group_id' => $this->composite_sample_group_id,
            'start_date' => $this->start_date,
            'finish_date' => $this->finish_date,
            'check_date' => $this->check_date,
            'essay_qty' => $this->essay_qty,
            'agent_qty' => $this->agent_qty,
            'material_qty' => $this->material_qty,
            'sample_qty' => $this->sample_qty,
            'payment_percentage' => $this->payment_percentage,
            'sub_total_cost' => $this->sub_total_cost,
            'request_process_status_id' => $this->request_process_status_id,
            'request_process_material_id' => $this->request_process_material_id,
            'request_process_payment_id' => $this->request_process_payment_id,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'material_unit_qty' => $this->material_unit_qty,
            'start_by' => $this->start_by,
            'finish_by' => $this->finish_by,
            'check_by' => $this->check_by,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['ilike', 'essay_detail', $this->essay_detail])
            ->andFilterWhere(['ilike', 'agent_detail', $this->agent_detail])
            ->andFilterWhere(['ilike', 'material_detail', $this->material_detail])
            ->andFilterWhere(['ilike', 'sample_detail', $this->sample_detail])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'child_crop', $this->child_crop])
            ->andFilterWhere(['ilike', 'child_crop_other', $this->child_crop_other])
            ->andFilterWhere(['ilike', 'request_process_other', $this->request_process_other])
            ->andFilterWhere(['ilike', 'registration_out_of_the_system', $this->registration_out_of_the_system]);

        return $dataProvider;
    }
}

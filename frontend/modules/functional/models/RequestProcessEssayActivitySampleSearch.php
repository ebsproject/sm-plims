<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\RequestProcessEssayActivitySample;

/**
 * RequestProcessEssayActivitySampleSearch represents the model behind the search form of `frontend\modules\functional\models\RequestProcessEssayActivitySample`.
 */
class RequestProcessEssayActivitySampleSearch extends RequestProcessEssayActivitySample
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_process_essay_activity_sample_id', 'request_id', 'num_order_id', 'essay_id', 'composite_sample_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'num_order', 'request_process_essay_activity_sample_status_id', 'deleted_by', 'number_value', 'request_process_essay_activity_id', 'registered_by', 'activity_id', 'composite_sample_group_id', 'start_by', 'finish_by', 'check_by', 'historical_data_version'], 'integer'],
            [['deleted_at', 'text_value', 'description_value', 'registered_at', 'start_date', 'finish_date', 'check_date'], 'safe'],
            [['available', 'is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestProcessEssayActivitySample::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'request_process_essay_activity_sample_id' => $this->request_process_essay_activity_sample_id,
            'request_id' => $this->request_id,
            'num_order_id' => $this->num_order_id,
            'essay_id' => $this->essay_id,
            'composite_sample_id' => $this->composite_sample_id,
            'composite_sample_type_id' => $this->composite_sample_type_id,
            'crop_id' => $this->crop_id,
            'workflow_id' => $this->workflow_id,
            'num_order' => $this->num_order,
            'request_process_essay_activity_sample_status_id' => $this->request_process_essay_activity_sample_status_id,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'available' => $this->available,
            'number_value' => $this->number_value,
            'request_process_essay_activity_id' => $this->request_process_essay_activity_id,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'activity_id' => $this->activity_id,
            'composite_sample_group_id' => $this->composite_sample_group_id,
            'start_by' => $this->start_by,
            'finish_by' => $this->finish_by,
            'check_by' => $this->check_by,
            'start_date' => $this->start_date,
            'finish_date' => $this->finish_date,
            'check_date' => $this->check_date,
            'historical_data_version' => $this->historical_data_version,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['ilike', 'text_value', $this->text_value])
            ->andFilterWhere(['ilike', 'description_value', $this->description_value]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\functional\models;

use Yii;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Workflow;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Agent;
use frontend\modules\configuration\models\AgentByEssay;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plims.bsns_request_process_essay_agent".
 *
 * @property int $request_id
 * @property int $num_order_id
 * @property int $essay_id
 * @property int $agent_id
 * @property int $composite_sample_type_id
 * @property int|null $crop_id
 * @property int|null $workflow_id
 * @property int|null $num_order
 * @property int|null $init_position
 * @property int|null $end_position
 * @property int|null $init_content
 * @property int|null $end_content
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 * @property int|null $repetition
 * @property int $request_process_essay_agent_id
 * @property int|null $request_process_essay_id
 * @property bool $is_deleted
 *
 * @property Agent $agent
 * @property Parameter $compositeSampleType
 * @property Crop $crop
 * @property Essay $essay
 * @property ReadingData[] $readingDatas
 * @property Request $request
 * @property RequestProcess $request0
 * @property RequestProcessEssay $requestProcessEssay
 * @property Workflow $workflow
 */
class RequestProcessEssayAgent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_request_process_essay_agent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'essay_id', 'agent_id', 'composite_sample_type_id'], 'required'],
            [['request_id', 'num_order_id', 'essay_id', 'agent_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'num_order', 'init_position', 'end_position', 'init_content', 'end_content', 'registered_by', 'deleted_by', 'repetition', 'request_process_essay_id'], 'default', 'value' => null],
            [['request_id', 'num_order_id', 'essay_id', 'agent_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'num_order', 'init_position', 'end_position', 'init_content', 'end_content', 'registered_by', 'deleted_by', 'repetition', 'request_process_essay_id'], 'integer'],
            [['registered_at', 'deleted_at'], 'safe'],
            [['is_deleted'], 'boolean'],
            [['status'], 'string', 'max' => 15],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agent::class, 'targetAttribute' => ['agent_id' => 'agent_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['composite_sample_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['composite_sample_type_id' => 'parameter_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
            [['request_id', 'num_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcess::class, 'targetAttribute' => ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']],
            [['request_process_essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessEssay::class, 'targetAttribute' => ['request_process_essay_id' => 'request_process_essay_id']],
            [['workflow_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workflow::class, 'targetAttribute' => ['workflow_id' => 'workflow_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'num_order_id' => 'Num Order ID',
            'essay_id' => 'Essay ID',
            'agent_id' => 'Agent ID',
            'composite_sample_type_id' => 'Composite Sample Type ID',
            'crop_id' => 'Crop ID',
            'workflow_id' => 'Workflow ID',
            'num_order' => 'Num Order',
            'init_position' => 'Init Position',
            'end_position' => 'End Position',
            'init_content' => 'Init Content',
            'end_content' => 'End Content',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'repetition' => 'Repetition',
            'request_process_essay_agent_id' => 'Request Process Essay Agent ID',
            'request_process_essay_id' => 'Request Process Essay ID',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[Agent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(Agent::class, ['agent_id' => 'agent_id']);
    }

    /**
     * Gets query for [[CompositeSampleType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'composite_sample_type_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[Essay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    /**
     * Gets query for [[ReadingDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['request_process_essay_agent_id' => 'request_process_essay_agent_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[Request0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest0()
    {
        return $this->hasOne(RequestProcess::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessEssay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssay()
    {
        return $this->hasOne(RequestProcessEssay::class, ['request_process_essay_id' => 'request_process_essay_id']);
    }

    /**
     * Gets query for [[Workflow]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(Workflow::class, ['workflow_id' => 'workflow_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = $this->getUserId();
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        }
        return parent::beforeSave($insert);
    }

    private function getUserId()
    {
        $user_id =  null;
        $_token = Yii::$app->request->headers->get('authorization');

        if (!is_null($_token)) {

            $token = str_replace('Bearer ', '', $_token);
            $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
        } else {
            $user_id = \Yii::$app->user->identity->id;
        }
        return $user_id;
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

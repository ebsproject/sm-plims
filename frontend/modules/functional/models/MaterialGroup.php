<?php

namespace frontend\modules\functional\models;

use Yii;

/**
 * This is the model class for table "{{%material_group}}".
 *
 * @property int $material_group_id
 * @property int|null $number_index
 * @property string|null $group_name
 * @property int|null $material_qty
 * @property int|null $material_unit_qty
 * @property string|null $sample_group_name
 * @property int|null $num_order_ini
 * @property string|null $material_name_ini
 * @property int|null $num_order_end
 * @property string|null $material_name_end
 * @property int|null $composite_sample_group_id
 *
 * @property CompositeSampleGroup $compositeSampleGroup
 * @property Material[] $materials
 */
class MaterialGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.bsns_material_group}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number_index', 'material_qty', 'material_unit_qty', 'num_order_ini', 'num_order_end', 'composite_sample_group_id'], 'default', 'value' => null],
            [['number_index', 'material_qty', 'material_unit_qty', 'num_order_ini', 'num_order_end', 'composite_sample_group_id'], 'integer'],
            [['group_name', 'sample_group_name', 'material_name_ini', 'material_name_end'], 'string', 'max' => 50],
            [['composite_sample_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompositeSampleGroup::class, 'targetAttribute' => ['composite_sample_group_id' => 'composite_sample_group_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'material_group_id' => 'Material Group ID',
            'number_index' => 'Number Index',
            'group_name' => 'Group Name',
            'material_qty' => 'Material Qty',
            'material_unit_qty' => 'Material Unit Qty',
            'sample_group_name' => 'Sample Group Name',
            'num_order_ini' => 'Num Order Ini',
            'material_name_ini' => 'Material Name Ini',
            'num_order_end' => 'Num Order End',
            'material_name_end' => 'Material Name End',
            'composite_sample_group_id' => 'Composite Sample Group ID',
        ];
    }

    /**
     * Gets query for [[CompositeSampleGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleGroup()
    {
        return $this->hasOne(CompositeSampleGroup::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    /**
     * Gets query for [[Materials]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(Material::class, ['material_group_id' => 'material_group_id']);
    }
}

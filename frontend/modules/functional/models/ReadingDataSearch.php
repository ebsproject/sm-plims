<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\ReadingData;

/**
 * ReadingDataSearch represents the model behind the search form of `frontend\modules\functional\models\ReadingData`.
 */
class ReadingDataSearch extends ReadingData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'essay_id', 'activity_id', 'agent_id', 'symptom_id', 'support_order_id', 'support_cell_position', 'grafting_number_id', 'historical_data_version', 'composite_sample_group_id', 'composite_sample_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'reading_data_section_id', 'reading_data_type_id', 'primary_order_num', 'secondary_order_num', 'tertiary_order_num', 'registered_by', 'deleted_by', 'repetition', 'number_of_seeds_tested', 'support_master_id', 'reading_data_id', 'request_process_essay_id', 'request_process_essay_agent_id', 'request_process_essay_activity_id', 'request_process_essay_activity_sample_id', 'support_id'], 'integer'],
            [['observation', 'material_evidence', 'auxiliary_result', 'text_result', 'registered_at', 'deleted_at', 'status'], 'safe'],
            [['number_result'], 'number'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReadingData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'request_id' => $this->request_id,
            'num_order_id' => $this->num_order_id,
            'essay_id' => $this->essay_id,
            'activity_id' => $this->activity_id,
            'agent_id' => $this->agent_id,
            'symptom_id' => $this->symptom_id,
            'support_order_id' => $this->support_order_id,
            'support_cell_position' => $this->support_cell_position,
            'grafting_number_id' => $this->grafting_number_id,
            'historical_data_version' => $this->historical_data_version,
            'composite_sample_group_id' => $this->composite_sample_group_id,
            'composite_sample_id' => $this->composite_sample_id,
            'composite_sample_type_id' => $this->composite_sample_type_id,
            'crop_id' => $this->crop_id,
            'workflow_id' => $this->workflow_id,
            'reading_data_section_id' => $this->reading_data_section_id,
            'reading_data_type_id' => $this->reading_data_type_id,
            'number_result' => $this->number_result,
            'primary_order_num' => $this->primary_order_num,
            'secondary_order_num' => $this->secondary_order_num,
            'tertiary_order_num' => $this->tertiary_order_num,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'repetition' => $this->repetition,
            'number_of_seeds_tested' => $this->number_of_seeds_tested,
            'support_master_id' => $this->support_master_id,
            'reading_data_id' => $this->reading_data_id,
            'request_process_essay_id' => $this->request_process_essay_id,
            'request_process_essay_agent_id' => $this->request_process_essay_agent_id,
            'request_process_essay_activity_id' => $this->request_process_essay_activity_id,
            'request_process_essay_activity_sample_id' => $this->request_process_essay_activity_sample_id,
            'support_id' => $this->support_id,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['ilike', 'observation', $this->observation])
            ->andFilterWhere(['ilike', 'material_evidence', $this->material_evidence])
            ->andFilterWhere(['ilike', 'auxiliary_result', $this->auxiliary_result])
            ->andFilterWhere(['ilike', 'text_result', $this->text_result])
            ->andFilterWhere(['ilike', 'status', $this->status]);

        return $dataProvider;
    }
}

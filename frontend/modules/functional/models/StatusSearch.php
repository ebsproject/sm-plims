<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\Status;

/**
 * StatusSearch represents the model behind the search form of `frontend\modules\functional\models\Status`.
 */
class StatusSearch extends Status
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'request_status_id', 'number_index', 'status_by'], 'integer'],
            [['status_date', 'observation'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Status::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'request_id' => $this->request_id,
            'request_status_id' => $this->request_status_id,
            'number_index' => $this->number_index,
            'status_date' => $this->status_date,
            'status_by' => $this->status_by,
        ]);

        $query->andFilterWhere(['ilike', 'observation', $this->observation]);

        return $dataProvider;
    }
}

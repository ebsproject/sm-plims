<?php

namespace frontend\modules\functional\models;

use Yii;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Workflow;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plims.bsns_request_process".
 *
 * @property int $request_id
 * @property int $num_order_id
 * @property int|null $crop_id
 * @property int|null $workflow_id
 * @property int|null $composite_sample_group_id
 * @property string|null $start_date
 * @property string|null $finish_date
 * @property string|null $check_date
 * @property int|null $essay_qty
 * @property int|null $agent_qty
 * @property int|null $material_qty
 * @property int|null $sample_qty
 * @property string|null $essay_detail
 * @property string|null $agent_detail
 * @property string|null $material_detail
 * @property string|null $sample_detail
 * @property float|null $payment_percentage
 * @property float|null $sub_total_cost
 * @property int|null $request_process_status_id
 * @property int|null $request_process_material_id
 * @property int|null $request_process_payment_id
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 * @property string|null $child_crop
 * @property string|null $child_crop_other
 * @property string|null $request_process_other
 * @property string|null $registration_out_of_the_system
 * @property int|null $material_unit_qty
 * @property int|null $start_by
 * @property int|null $finish_by
 * @property int|null $check_by
 * @property bool $is_deleted
 *
 * @property User $checkBy
 * @property CompositeSampleGroup $compositeSampleGroup
 * @property Crop $crop
 * @property User $finishBy
 * @property ReadingData[] $readingDatas
 * @property Request $request
 * @property RequestProcessEssayActivity[] $requestProcessEssayActivities
 * @property RequestProcessEssayActivitySample[] $requestProcessEssayActivitySamples
 * @property RequestProcessEssayAgent[] $requestProcessEssayAgents
 * @property RequestProcessEssay[] $requestProcessEssays
 * @property Parameter $requestProcessMaterial
 * @property Parameter $requestProcessPayment
 * @property Parameter $requestProcessStatus
 * @property User $startBy
 * @property Support[] $supports
 * @property Workflow $workflow
 */
class RequestProcess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_request_process';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id'], 'required'],
            [['request_id', 'num_order_id', 'crop_id', 'workflow_id', 'composite_sample_group_id', 'essay_qty', 'agent_qty', 'material_qty', 'sample_qty', 'request_process_status_id', 'request_process_material_id', 'request_process_payment_id', 'registered_by', 'deleted_by', 'material_unit_qty', 'start_by', 'finish_by', 'check_by'], 'default', 'value' => null],
            [['request_id', 'num_order_id', 'crop_id', 'workflow_id', 'composite_sample_group_id', 'essay_qty', 'agent_qty', 'material_qty', 'sample_qty', 'request_process_status_id', 'request_process_material_id', 'request_process_payment_id', 'registered_by', 'deleted_by', 'material_unit_qty', 'start_by', 'finish_by', 'check_by'], 'integer'],
            [['start_date', 'finish_date', 'check_date', 'registered_at', 'deleted_at'], 'safe'],
            [['essay_detail', 'agent_detail', 'material_detail', 'sample_detail'], 'string'],
            [['payment_percentage', 'sub_total_cost'], 'number'],
            [['is_deleted'], 'boolean'],
            [['status'], 'string', 'max' => 15],
            [['child_crop', 'child_crop_other'], 'string', 'max' => 360],
            [['request_process_other', 'registration_out_of_the_system'], 'string', 'max' => 45],
            [['request_id', 'num_order_id'], 'unique', 'targetAttribute' => ['request_id', 'num_order_id']],
            [['start_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['start_by' => 'user_id']],
            [['finish_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['finish_by' => 'user_id']],
            [['check_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['check_by' => 'user_id']],
            [['composite_sample_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompositeSampleGroup::class, 'targetAttribute' => ['composite_sample_group_id' => 'composite_sample_group_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['request_process_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_process_status_id' => 'parameter_id']],
            [['request_process_material_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_process_material_id' => 'parameter_id']],
            [['request_process_payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_process_payment_id' => 'parameter_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
            [['workflow_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workflow::class, 'targetAttribute' => ['workflow_id' => 'workflow_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'num_order_id' => 'Num Order ID',
            'crop_id' => 'Crop ID',
            'workflow_id' => 'Workflow ID',
            'composite_sample_group_id' => 'Composite Sample Group ID',
            'start_date' => 'Start Date',
            'finish_date' => 'Finish Date',
            'check_date' => 'Check Date',
            'essay_qty' => 'Essay Qty',
            'agent_qty' => 'Agent Qty',
            'material_qty' => 'Material Qty',
            'sample_qty' => 'Sample Qty',
            'essay_detail' => 'Essay Detail',
            'agent_detail' => 'Agent Detail',
            'material_detail' => 'Material Detail',
            'sample_detail' => 'Sample Detail',
            'payment_percentage' => 'Payment Percentage',
            'sub_total_cost' => 'Sub Total Cost',
            'request_process_status_id' => 'Request Process Status ID',
            'request_process_material_id' => 'Request Process Material ID',
            'request_process_payment_id' => 'Request Process Payment ID',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'child_crop' => 'Child Crop',
            'child_crop_other' => 'Child Crop Other',
            'request_process_other' => 'Request Process Other',
            'registration_out_of_the_system' => 'Registration Out Of The System',
            'material_unit_qty' => 'Material Unit Qty',
            'start_by' => 'Start By',
            'finish_by' => 'Finish By',
            'check_by' => 'Check By',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[CheckBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCheckBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'check_by']);
    }

    /**
     * Gets query for [[CompositeSampleGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleGroup()
    {
        return $this->hasOne(CompositeSampleGroup::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[FinishBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFinishBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'finish_by']);
    }

    /**
     * Gets query for [[ReadingDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivities]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivities()
    {
        return $this->hasMany(RequestProcessEssayActivity::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivitySamples]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivitySamples()
    {
        return $this->hasMany(RequestProcessEssayActivitySample::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayAgents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayAgents()
    {
        return $this->hasMany(RequestProcessEssayAgent::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessEssays]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssays()
    {
        return $this->hasMany(RequestProcessEssay::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessMaterial]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessMaterial()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_process_material_id']);
    }

    /**
     * Gets query for [[RequestProcessPayment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessPayment()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_process_payment_id']);
    }

    /**
     * Gets query for [[RequestProcessStatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessStatus()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_process_status_id']);
    }

    /**
     * Gets query for [[StartBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStartBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'start_by']);
    }

    /**
     * Gets query for [[Supports]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[Workflow]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(Workflow::class, ['workflow_id' => 'workflow_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        }
        return parent::beforeSave($insert);
    }


    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }

    public function getCropId()
    {
        $_user_institution = \Yii::$app->user->identity->institution;
        return ArrayHelper::map(
            Crop::find()->where(
                [
                    'deleted_by' => null,
                    'deleted_at' => null,
                    'status' => 'active',
                    'institution_id' => $_user_institution
                ]
            )->all(),
            'crop_id',
            'short_name'
        );
    }

    public function getWorkflowId()
    {
        return ArrayHelper::map(Workflow::find()->where(
            [
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'workflow_id', 'short_name');
    }

    public function getParameterName($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'short_name', 'short_name');
    }
}

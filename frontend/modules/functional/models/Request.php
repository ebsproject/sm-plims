<?php

namespace frontend\modules\functional\models;

use Yii;
use frontend\modules\configuration\models\Parameter;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plims.bsns_request".
 *
 * @property int $request_id
 * @property int|null $year_code
 * @property int|null $sequential_code
 * @property string|null $request_code
 * @property string|null $details
 * @property string|null $diagnostic
 * @property int|null $agent_qty
 * @property int|null $material_qty
 * @property float|null $total_cost
 * @property int|null $request_type_id
 * @property int|null $request_status_id
 * @property int|null $request_payment_id
 * @property string|null $pay_number
 * @property int|null $requested_user_id
 * @property int|null $owner_user_id
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 * @property int|null $sample_qty
 * @property int|null $material_unit_qty
 * @property bool $is_deleted
 *
 * @property User $ownerUser
 * @property ReadingData[] $readingDatas
 * @property RequestDetail $requestDetail
 * @property RequestDocumentation[] $requestDocumentations
 * @property Parameter $requestPayment
 * @property RequestProcessEssayActivity[] $requestProcessEssayActivities
 * @property RequestProcessEssayActivitySample[] $requestProcessEssayActivitySamples
 * @property RequestProcessEssayAgent[] $requestProcessEssayAgents
 * @property RequestProcessEssay[] $requestProcessEssays
 * @property RequestProcess[] $requestProcesses
 * @property Parameter $requestStatus
 * @property Parameter[] $requestStatuses
 * @property Parameter $requestType
 * @property User $requestedUser
 * @property Status[] $statuses
 * @property Support[] $supports
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year_code', 'sequential_code', 'agent_qty', 'material_qty', 'request_type_id', 'request_status_id', 'request_payment_id', 'requested_user_id', 'owner_user_id', 'registered_by', 'deleted_by', 'sample_qty', 'material_unit_qty'], 'default', 'value' => null],
            [['year_code', 'sequential_code', 'agent_qty', 'material_qty', 'request_type_id', 'request_status_id', 'request_payment_id', 'requested_user_id', 'owner_user_id', 'registered_by', 'deleted_by', 'sample_qty', 'material_unit_qty'], 'integer'],
            [['total_cost'], 'number'],
            [['registered_at', 'deleted_at'], 'safe'],
            [['is_deleted'], 'boolean'],
            [['request_code', 'pay_number'], 'string', 'max' => 45],
            [['details', 'diagnostic'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 15],
            [['requested_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['requested_user_id' => 'user_id']],
            [['owner_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['owner_user_id' => 'user_id']],
            [['request_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_type_id' => 'parameter_id']],
            [['request_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_status_id' => 'parameter_id']],
            [['request_payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_payment_id' => 'parameter_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'year_code' => 'Year Code',
            'sequential_code' => 'Sequential Code',
            'request_code' => 'Request Code',
            'details' => 'Details',
            'diagnostic' => 'Diagnostic',
            'agent_qty' => 'Agent Qty',
            'material_qty' => 'Material Qty',
            'total_cost' => 'Total Cost',
            'request_type_id' => 'Request Type ID',
            'request_status_id' => 'Request Status ID',
            'request_payment_id' => 'Request Payment ID',
            'pay_number' => 'Pay Number',
            'requested_user_id' => 'Requested User ID',
            'owner_user_id' => 'Owner User ID',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'sample_qty' => 'Sample Qty',
            'material_unit_qty' => 'Material Unit Qty',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[OwnerUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOwnerUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'owner_user_id']);
    }

    /**
     * Gets query for [[ReadingDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestDetail]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestDetail()
    {
        return $this->hasOne(RequestDetail::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestDocumentations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestDocumentations()
    {
        return $this->hasMany(RequestDocumentation::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestPayment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestPayment()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_payment_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivities]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivities()
    {
        return $this->hasMany(RequestProcessEssayActivity::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivitySamples]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivitySamples()
    {
        return $this->hasMany(RequestProcessEssayActivitySample::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayAgents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayAgents()
    {
        return $this->hasMany(RequestProcessEssayAgent::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestProcessEssays]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssays()
    {
        return $this->hasMany(RequestProcessEssay::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestProcesses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcesses()
    {
        return $this->hasMany(RequestProcess::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestStatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestStatus()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_status_id']);
    }

    /**
     * Gets query for [[RequestStatuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestStatuses()
    {
        return $this->hasMany(Parameter::class, ['parameter_id' => 'request_status_id'])->viaTable('plims.bsns_status', ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[RequestType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_type_id']);
    }

    /**
     * Gets query for [[RequestedUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestedUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'requested_user_id']);
    }

    /**
     * Gets query for [[Statuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatuses()
    {
        return $this->hasMany(Status::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[Supports]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::class, ['request_id' => 'request_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        }
        return parent::beforeSave($insert);
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()
            ->where(
                [
                    'entity' => $entity,
                    'singularity' => $singularity,
                    'deleted_by' => null,
                    'deleted_at' => null,
                    'status' => 'active',
                ]
            )
            ->andWhere(['<>', 'code', 'RT0'])
            ->all(), 'parameter_id', 'short_name');
    }

    public function getRequestStatusValue()
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => 'request',
                'singularity' => 'status',
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'code', 'short_name');
    }

    public function getRequestProcess()
    {
        return RequestProcess::findOne(
            [
                'request_id' => $this->request_id,
                'num_order_id' => 1
            ]
        );
    }
}

<?php

namespace frontend\modules\functional\models;

use Yii;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Essay;
use yii\helpers\ArrayHelper;

class RequestDocumentation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_request_documentation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'file_type_id', 'registered_by', 'updated_by', 'deleted_by', 'num_order_id', 'essay_id'], 'default', 'value' => null],
            [['request_id', 'file_type_id', 'registered_by', 'updated_by', 'deleted_by', 'num_order_id', 'essay_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['path_documentation'], 'string', 'max' => 900],
            [['document_name', 'document_extension'], 'string', 'max' => 450],
            [['status'], 'string', 'max' => 15],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['file_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['file_type_id' => 'parameter_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_documentation_id' => 'Request Documentation ID',
            'path_documentation' => 'Path Documentation',
            'document_name' => 'Document Name',
            'document_extension' => 'Document Extension',
            'request_id' => 'Request ID',
            'file_type_id' => 'File Type ID',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'num_order_id' => 'Num Order ID',
            'essay_id' => 'Essay ID',
        ];
    }

    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    public function getFileType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'file_type_id']);
    }

    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }


    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

<?php

namespace frontend\modules\functional\models;

use Yii;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Workflow;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\Parameter;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plims.bsns_support".
 *
 * @property int $support_order_id
 * @property int $request_id
 * @property int $num_order_id
 * @property int $essay_id
 * @property int $activity_id
 * @property int $composite_sample_type_id
 * @property int|null $crop_id
 * @property int|null $workflow_id
 * @property int|null $support_type_id
 * @property int|null $support_columns
 * @property int|null $support_rows
 * @property string|null $support_code
 * @property int|null $total_slots
 * @property int|null $used_slots
 * @property int|null $available_slots
 * @property int|null $ctrl_qty
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 * @property int $support_id
 * @property int|null $request_process_essay_activity_id
 *
 * @property Activity $activity
 * @property Parameter $compositeSampleType
 * @property Crop $crop
 * @property Essay $essay
 * @property ReadingData[] $readingDatas
 * @property Request $request
 * @property RequestProcess $request0
 * @property RequestProcessEssayActivity $requestProcessEssayActivity
 * @property Parameter $supportType
 * @property Workflow $workflow
 */
class Support extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_support';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['support_order_id', 'request_id', 'num_order_id', 'essay_id', 'activity_id', 'composite_sample_type_id'], 'required'],
            [['support_order_id', 'request_id', 'num_order_id', 'essay_id', 'activity_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'support_type_id', 'support_columns', 'support_rows', 'total_slots', 'used_slots', 'available_slots', 'ctrl_qty', 'registered_by', 'updated_by', 'deleted_by', 'request_process_essay_activity_id'], 'default', 'value' => null],
            [['support_order_id', 'request_id', 'num_order_id', 'essay_id', 'activity_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'support_type_id', 'support_columns', 'support_rows', 'total_slots', 'used_slots', 'available_slots', 'ctrl_qty', 'registered_by', 'updated_by', 'deleted_by', 'request_process_essay_activity_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['support_code'], 'string', 'max' => 45],
            [['status'], 'string', 'max' => 15],
            [['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::class, 'targetAttribute' => ['activity_id' => 'activity_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['composite_sample_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['composite_sample_type_id' => 'parameter_id']],
            [['support_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['support_type_id' => 'parameter_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
            [['request_id', 'num_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcess::class, 'targetAttribute' => ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']],
            [['request_process_essay_activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessEssayActivity::class, 'targetAttribute' => ['request_process_essay_activity_id' => 'request_process_essay_activity_id']],
            [['workflow_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workflow::class, 'targetAttribute' => ['workflow_id' => 'workflow_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'support_order_id' => 'Support Order ID',
            'request_id' => 'Request ID',
            'num_order_id' => 'Num Order ID',
            'essay_id' => 'Essay ID',
            'activity_id' => 'Activity ID',
            'composite_sample_type_id' => 'Composite Sample Type ID',
            'crop_id' => 'Crop ID',
            'workflow_id' => 'Workflow ID',
            'support_type_id' => 'Support Type ID',
            'support_columns' => 'Support Columns',
            'support_rows' => 'Support Rows',
            'support_code' => 'Support Code',
            'total_slots' => 'Total Slots',
            'used_slots' => 'Used Slots',
            'available_slots' => 'Available Slots',
            'ctrl_qty' => 'Ctrl Qty',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'support_id' => 'Support ID',
            'request_process_essay_activity_id' => 'Request Process Essay Activity ID',
        ];
    }

    /**
     * Gets query for [[Activity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::class, ['activity_id' => 'activity_id']);
    }

    /**
     * Gets query for [[CompositeSampleType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'composite_sample_type_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[Essay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    /**
     * Gets query for [[ReadingDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['support_id' => 'support_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[Request0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest0()
    {
        return $this->hasOne(RequestProcess::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivity()
    {
        return $this->hasOne(RequestProcessEssayActivity::class, ['request_process_essay_activity_id' => 'request_process_essay_activity_id']);
    }

    /**
     * Gets query for [[SupportType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupportType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'support_type_id']);
    }

    /**
     * Gets query for [[Workflow]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(Workflow::class, ['workflow_id' => 'workflow_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = $this->getUserId();
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = $this->getUserId();
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    private function getUserId()
    {
        $user_id =  null;
        $_token = Yii::$app->request->headers->get('authorization');

        if (!is_null($_token)) {

            $token = str_replace('Bearer ', '', $_token);
            $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
        } else {
            $user_id = \Yii::$app->user->identity->id;
        }
        return $user_id;
    }


    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

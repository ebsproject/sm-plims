<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\Material;
use common\models\User;

/**
 * MaterialSearch represents the model behind the search form of `frontend\modules\functional\models\Material`.
 */
class MaterialSearch extends Material
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'material_id',
                    'composite_sample_id',
                    'composite_sample_type_id',
                    'composite_sample_group_id',
                    // 'crop_id',
                    // 'material_type_id',
                    // 'material_unit_id',
                    'num_order',
                    'composite_sample_order',
                    'material_unit_qty',
                    'grafting_order',
                    // 'registered_by',
                    'updated_by',
                    'deleted_by',
                    'material_group_id',
                    // 'material_status_id',
                    'historical_data_version',
                ], 'integer'
            ],
            [
                [
                    'short_name',
                    'long_name',
                    'observation',
                    'details',
                    'origin',
                    'material_code_a',
                    'material_code_b',
                    'material_code_c',
                    'registered_at',
                    'updated_at',
                    'deleted_at',
                    'status',
                    'crop_id',
                    'material_type_id',
                    'material_unit_id',
                    'registered_by',
                    'material_status_id',
                ],
                'safe'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params, $composite_sample_group_id = null, $order_list = null, $status = null)
    {
        $query = Material::find()
            ->where(
                [
                    'plims.bsns_material.composite_sample_group_id' => $composite_sample_group_id,
                ]
            );

        if ($status)
            $query->andWhere(
                [
                    'plims.bsns_material.status' => $status,
                ]
            );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10,],
            'sort' => ['defaultOrder' => $order_list],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->joinWith(
            [
                'crop' => function ($query) {
                    $query->from(['sfh1' => 'plims.bsns_crop']);
                }
            ]
        );
        $query->joinWith(
            [
                'registeredBy' => function ($query) {
                    $query->from(['sfh2' => 'plims.auth_user']);
                }
            ]
        );
        $query->joinWith(
            [
                'materialType' => function ($query) {
                    $query->from(['sfh3' => 'plims.bsns_parameter']);
                }
            ]
        );
        $query->joinWith(
            [
                'materialUnit' => function ($query) {
                    $query->from(['sfh4' => 'plims.bsns_parameter']);
                }
            ]
        );
        $query->joinWith(
            [
                'materialStatus' => function ($query) {
                    $query->from(['sfh5' => 'plims.bsns_parameter']);
                }
            ]
        );
        $query
            ->andFilterWhere([
                'material_id' => $this->material_id,
                'composite_sample_id' => $this->composite_sample_id,
                'composite_sample_type_id' => $this->composite_sample_type_id,
                'composite_sample_group_id' => $this->composite_sample_group_id,
                // 'crop_id' => $this->crop_id,
                // 'material_type_id' => $this->material_type_id,
                // 'material_unit_id' => $this->material_unit_id,
                'num_order' => $this->num_order,
                'composite_sample_order' => $this->composite_sample_order,
                'material_unit_qty' => $this->material_unit_qty,
                'grafting_order' => $this->grafting_order,
                // 'registered_by' => $this->registered_by,
                'registered_at' => $this->registered_at,
                'updated_by' => $this->updated_by,
                'updated_at' => $this->updated_at,
                'deleted_by' => $this->deleted_by,
                'deleted_at' => $this->deleted_at,
                'material_group_id' => $this->material_group_id,
                // 'material_status_id' => $this->material_status_id,
                'historical_data_version' => $this->historical_data_version,
            ]);
        $query
            ->andFilterWhere(['ilike', 'plims.bsns_material.short_name', $this->short_name])
            ->andFilterWhere(['ilike', 'plims.bsns_material.long_name', $this->long_name])
            ->andFilterWhere(['ilike', 'plims.bsns_material.observation', $this->observation])
            ->andFilterWhere(['ilike', 'plims.bsns_material.details', $this->details])
            ->andFilterWhere(['ilike', 'plims.bsns_material.origin', $this->origin])
            ->andFilterWhere(['ilike', 'plims.bsns_material.material_code_a', $this->material_code_a])
            ->andFilterWhere(['ilike', 'plims.bsns_material.material_code_b', $this->material_code_b])
            ->andFilterWhere(['ilike', 'plims.bsns_material.material_code_c', $this->material_code_c])
            ->andFilterWhere(['ilike', 'plims.bsns_material.status', $this->status])

            ->andFilterWhere(['like', 'sfh1.short_name', $this->crop_id])
            ->andFilterWhere(['like', 'sfh2.username', $this->registered_by])
            ->andFilterWhere(['like', 'sfh3.short_name', $this->material_type_id])
            ->andFilterWhere(['like', 'sfh4.short_name', $this->material_unit_id])
            ->andFilterWhere(['like', 'sfh5.short_name', $this->material_status_id]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\RequestProcessEssayAgent;

/**
 * RequestProcessEssayAgentSearch represents the model behind the search form of `frontend\modules\functional\models\RequestProcessEssayAgent`.
 */
class RequestProcessEssayAgentSearch extends RequestProcessEssayAgent
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'essay_id', 'agent_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'num_order', 'init_position', 'end_position', 'init_content', 'end_content', 'registered_by', 'deleted_by', 'repetition', 'request_process_essay_agent_id', 'request_process_essay_id'], 'integer'],
            [['registered_at', 'deleted_at', 'status'], 'safe'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestProcessEssayAgent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'request_id' => $this->request_id,
            'num_order_id' => $this->num_order_id,
            'essay_id' => $this->essay_id,
            'agent_id' => $this->agent_id,
            'composite_sample_type_id' => $this->composite_sample_type_id,
            'crop_id' => $this->crop_id,
            'workflow_id' => $this->workflow_id,
            'num_order' => $this->num_order,
            'init_position' => $this->init_position,
            'end_position' => $this->end_position,
            'init_content' => $this->init_content,
            'end_content' => $this->end_content,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'repetition' => $this->repetition,
            'request_process_essay_agent_id' => $this->request_process_essay_agent_id,
            'request_process_essay_id' => $this->request_process_essay_id,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['ilike', 'status', $this->status]);

        return $dataProvider;
    }
}

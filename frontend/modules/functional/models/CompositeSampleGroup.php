<?php

namespace frontend\modules\functional\models;

use Yii;
use frontend\modules\configuration\models\Parameter;
use yii\helpers\ArrayHelper;

class CompositeSampleGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.bsns_composite_sample_group}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['year_code', 'sequential_code', 'registered_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['year_code', 'sequential_code', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['composite_sample_group_code'], 'string', 'max' => 45],
            [['status'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'composite_sample_group_id' => 'Composite Sample Group ID',
            'year_code' => 'Year Code',
            'sequential_code' => 'Sequential Code',
            'composite_sample_group_code' => 'Composite Sample Group Code',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSamples()
    {
        return $this->hasMany(CompositeSample::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(Material::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcesses()
    {
        return $this->hasMany(RequestProcess::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

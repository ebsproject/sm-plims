<?php

namespace frontend\modules\functional\models;

use Yii;

/**
 * This is the model class for table "plims.bsns_support_master".
 *
 * @property int $support_master_id
 * @property string|null $support_code
 * @property string|null $detail
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 *
 * @property ReadingData[] $readingDatas
 * @property RequestProcessEssay[] $requestProcessEssays
 */
class SupportMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_support_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registered_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['support_code'], 'string', 'max' => 150],
            [['detail'], 'string', 'max' => 900],
            [['status'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'support_master_id' => 'Support Master ID',
            'support_code' => 'Support Code',
            'detail' => 'Detail',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[ReadingDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['support_master_id' => 'support_master_id']);
    }

    /**
     * Gets query for [[RequestProcessEssays]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssays()
    {
        return $this->hasMany(RequestProcessEssay::class, ['support_master_id' => 'support_master_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = $this->getUserId();
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = $this->getUserId();
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    private function getUserId()
    {
        $user_id =  null;
        $_token = Yii::$app->request->headers->get('authorization');

        if (!is_null($_token)) {

            $token = str_replace('Bearer ', '', $_token);
            $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
        } else {
            $user_id = \Yii::$app->user->identity->id;
        }
        return $user_id;
    }
}

<?php

namespace frontend\modules\functional\models;

use Yii;
use common\models\User;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Workflow;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\Agent;
use frontend\modules\configuration\models\Symptom;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plims.bsns_reading_data".
 *
 * @property int|null $request_id
 * @property int|null $num_order_id
 * @property int|null $essay_id
 * @property int|null $activity_id
 * @property int|null $agent_id
 * @property int|null $symptom_id
 * @property int|null $support_order_id
 * @property int|null $support_cell_position
 * @property int|null $grafting_number_id
 * @property int|null $historical_data_version
 * @property int|null $composite_sample_group_id
 * @property int|null $composite_sample_id
 * @property int|null $composite_sample_type_id
 * @property int|null $crop_id
 * @property int|null $workflow_id
 * @property int|null $reading_data_section_id
 * @property int|null $reading_data_type_id
 * @property string|null $observation
 * @property string|null $material_evidence
 * @property string|null $auxiliary_result
 * @property float|null $number_result
 * @property string|null $text_result
 * @property int|null $primary_order_num
 * @property int|null $secondary_order_num
 * @property int|null $tertiary_order_num
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 * @property int|null $repetition
 * @property int|null $number_of_seeds_tested
 * @property int|null $support_master_id
 * @property int $reading_data_id
 * @property int|null $request_process_essay_id
 * @property int|null $request_process_essay_agent_id
 * @property int|null $request_process_essay_activity_id
 * @property int|null $request_process_essay_activity_sample_id
 * @property int|null $support_id
 * @property bool $is_deleted
 *
 * @property Activity $activity
 * @property Agent $agent
 * @property CompositeSampleGroup $compositeSampleGroup
 * @property Parameter $compositeSampleType
 * @property Crop $crop
 * @property Essay $essay
 * @property Parameter $readingDataSection
 * @property Parameter $readingDataType
 * @property Request $request
 * @property RequestProcess $request0
 * @property RequestProcessEssay $requestProcessEssay
 * @property RequestProcessEssayActivity $requestProcessEssayActivity
 * @property RequestProcessEssayActivitySample $requestProcessEssayActivitySample
 * @property RequestProcessEssayAgent $requestProcessEssayAgent
 * @property Support $support
 * @property SupportMaster $supportMaster
 * @property Symptom $symptom
 * @property Workflow $workflow
 */
class ReadingData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_reading_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'essay_id', 'activity_id', 'agent_id', 'symptom_id', 'support_order_id', 'support_cell_position', 'grafting_number_id', 'historical_data_version', 'composite_sample_group_id', 'composite_sample_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'reading_data_section_id', 'reading_data_type_id', 'primary_order_num', 'secondary_order_num', 'tertiary_order_num', 'registered_by', 'deleted_by', 'repetition', 'number_of_seeds_tested', 'support_master_id', 'request_process_essay_id', 'request_process_essay_agent_id', 'request_process_essay_activity_id', 'request_process_essay_activity_sample_id', 'support_id'], 'default', 'value' => null],
            [['request_id', 'num_order_id', 'essay_id', 'activity_id', 'agent_id', 'symptom_id', 'support_order_id', 'support_cell_position', 'grafting_number_id', 'historical_data_version', 'composite_sample_group_id', 'composite_sample_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'reading_data_section_id', 'reading_data_type_id', 'primary_order_num', 'secondary_order_num', 'tertiary_order_num', 'registered_by', 'deleted_by', 'repetition', 'number_of_seeds_tested', 'support_master_id', 'request_process_essay_id', 'request_process_essay_agent_id', 'request_process_essay_activity_id', 'request_process_essay_activity_sample_id', 'support_id'], 'integer'],
            [['observation'], 'string'],
            [['number_result'], 'number'],
            [['registered_at', 'deleted_at'], 'safe'],
            [['is_deleted'], 'boolean'],
            [['material_evidence'], 'string', 'max' => 500],
            [['auxiliary_result', 'text_result'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 15],
            [['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::class, 'targetAttribute' => ['activity_id' => 'activity_id']],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agent::class, 'targetAttribute' => ['agent_id' => 'agent_id']],
            [['composite_sample_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompositeSampleGroup::class, 'targetAttribute' => ['composite_sample_group_id' => 'composite_sample_group_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['composite_sample_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['composite_sample_type_id' => 'parameter_id']],
            [['reading_data_section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['reading_data_section_id' => 'parameter_id']],
            [['reading_data_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['reading_data_type_id' => 'parameter_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
            [['request_id', 'num_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcess::class, 'targetAttribute' => ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']],
            [['request_process_essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessEssay::class, 'targetAttribute' => ['request_process_essay_id' => 'request_process_essay_id']],
            [['request_process_essay_activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessEssayActivity::class, 'targetAttribute' => ['request_process_essay_activity_id' => 'request_process_essay_activity_id']],
            [['request_process_essay_activity_sample_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessEssayActivitySample::class, 'targetAttribute' => ['request_process_essay_activity_sample_id' => 'request_process_essay_activity_sample_id']],
            [['request_process_essay_agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessEssayAgent::class, 'targetAttribute' => ['request_process_essay_agent_id' => 'request_process_essay_agent_id']],
            [['support_id'], 'exist', 'skipOnError' => true, 'targetClass' => Support::class, 'targetAttribute' => ['support_id' => 'support_id']],
            [['support_master_id'], 'exist', 'skipOnError' => true, 'targetClass' => SupportMaster::class, 'targetAttribute' => ['support_master_id' => 'support_master_id']],
            [['symptom_id'], 'exist', 'skipOnError' => true, 'targetClass' => Symptom::class, 'targetAttribute' => ['symptom_id' => 'symptom_id']],
            [['workflow_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workflow::class, 'targetAttribute' => ['workflow_id' => 'workflow_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'num_order_id' => 'Num Order ID',
            'essay_id' => 'Essay ID',
            'activity_id' => 'Activity ID',
            'agent_id' => 'Agent ID',
            'symptom_id' => 'Symptom ID',
            'support_order_id' => 'Support Order ID',
            'support_cell_position' => 'Support Cell Position',
            'grafting_number_id' => 'Grafting Number ID',
            'historical_data_version' => 'Historical Data Version',
            'composite_sample_group_id' => 'Composite Sample Group ID',
            'composite_sample_id' => 'Composite Sample ID',
            'composite_sample_type_id' => 'Composite Sample Type ID',
            'crop_id' => 'Crop ID',
            'workflow_id' => 'Workflow ID',
            'reading_data_section_id' => 'Reading Data Section ID',
            'reading_data_type_id' => 'Reading Data Type ID',
            'observation' => 'Observation',
            'material_evidence' => 'Material Evidence',
            'auxiliary_result' => 'Auxiliary Result',
            'number_result' => 'Number Result',
            'text_result' => 'Text Result',
            'primary_order_num' => 'Primary Order Num',
            'secondary_order_num' => 'Secondary Order Num',
            'tertiary_order_num' => 'Tertiary Order Num',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'repetition' => 'Repetition',
            'number_of_seeds_tested' => 'Number Of Seeds Tested',
            'support_master_id' => 'Support Master ID',
            'reading_data_id' => 'Reading Data ID',
            'request_process_essay_id' => 'Request Process Essay ID',
            'request_process_essay_agent_id' => 'Request Process Essay Agent ID',
            'request_process_essay_activity_id' => 'Request Process Essay Activity ID',
            'request_process_essay_activity_sample_id' => 'Request Process Essay Activity Sample ID',
            'support_id' => 'Support ID',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[Activity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::class, ['activity_id' => 'activity_id']);
    }

    /**
     * Gets query for [[Agent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(Agent::class, ['agent_id' => 'agent_id']);
    }

    /**
     * Gets query for [[CompositeSampleGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleGroup()
    {
        return $this->hasOne(CompositeSampleGroup::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    /**
     * Gets query for [[CompositeSampleType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'composite_sample_type_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[Essay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    /**
     * Gets query for [[ReadingDataSection]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDataSection()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'reading_data_section_id']);
    }

    /**
     * Gets query for [[ReadingDataType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDataType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'reading_data_type_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[Request0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest0()
    {
        return $this->hasOne(RequestProcess::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessEssay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssay()
    {
        return $this->hasOne(RequestProcessEssay::class, ['request_process_essay_id' => 'request_process_essay_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivity()
    {
        return $this->hasOne(RequestProcessEssayActivity::class, ['request_process_essay_activity_id' => 'request_process_essay_activity_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivitySample]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivitySample()
    {
        return $this->hasOne(RequestProcessEssayActivitySample::class, ['request_process_essay_activity_sample_id' => 'request_process_essay_activity_sample_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayAgent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayAgent()
    {
        return $this->hasOne(RequestProcessEssayAgent::class, ['request_process_essay_agent_id' => 'request_process_essay_agent_id']);
    }

    /**
     * Gets query for [[Support]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupport()
    {
        return $this->hasOne(Support::class, ['support_id' => 'support_id']);
    }

    /**
     * Gets query for [[SupportMaster]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupportMaster()
    {
        return $this->hasOne(SupportMaster::class, ['support_master_id' => 'support_master_id']);
    }

    /**
     * Gets query for [[Symptom]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSymptom()
    {
        return $this->hasOne(Symptom::class, ['symptom_id' => 'symptom_id']);
    }

    /**
     * Gets query for [[Workflow]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(Workflow::class, ['workflow_id' => 'workflow_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = $this->getUserId();
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        }
        return parent::beforeSave($insert);
    }

    private function getUserId()
    {
        $user_id =  null;
        $_token = Yii::$app->request->headers->get('authorization');

        if (!is_null($_token)) {

            $token = str_replace('Bearer ', '', $_token);
            $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
        } else {
            $user_id = \Yii::$app->user->identity->id;
        }
        return $user_id;
    }


    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

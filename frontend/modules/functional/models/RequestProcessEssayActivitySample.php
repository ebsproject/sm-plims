<?php

namespace frontend\modules\functional\models;

use Yii;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Workflow;
use frontend\modules\configuration\models\Essay;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plims.bsns_request_process_essay_activity_sample".
 *
 * @property int $request_process_essay_activity_sample_id
 * @property int|null $request_id
 * @property int|null $num_order_id
 * @property int|null $essay_id
 * @property int|null $composite_sample_id
 * @property int|null $composite_sample_type_id
 * @property int|null $crop_id
 * @property int|null $workflow_id
 * @property int|null $num_order
 * @property int|null $request_process_essay_activity_sample_status_id
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property bool $available
 * @property string|null $text_value
 * @property int|null $number_value
 * @property string|null $description_value
 * @property int|null $request_process_essay_activity_id
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $activity_id
 * @property int|null $composite_sample_group_id
 * @property int|null $start_by
 * @property int|null $finish_by
 * @property int|null $check_by
 * @property string|null $start_date
 * @property string|null $finish_date
 * @property string|null $check_date
 * @property int|null $historical_data_version
 * @property bool $is_deleted
 *
 * @property Activity $activity
 * @property User $checkBy
 * @property CompositeSample $compositeSample
 * @property CompositeSampleGroup $compositeSampleGroup
 * @property Parameter $compositeSampleType
 * @property Crop $crop
 * @property Essay $essay
 * @property User $finishBy
 * @property ReadingData[] $readingDatas
 * @property Request $request
 * @property RequestProcess $request0
 * @property RequestProcessEssayActivity $requestProcessEssayActivity
 * @property Parameter $requestProcessEssaySampleActivityStatus
 * @property User $startBy
 * @property Workflow $workflow
 */
class RequestProcessEssayActivitySample extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_request_process_essay_activity_sample';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'essay_id', 'composite_sample_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'num_order', 'request_process_essay_activity_sample_status_id', 'deleted_by', 'number_value', 'request_process_essay_activity_id', 'registered_by', 'activity_id', 'composite_sample_group_id', 'start_by', 'finish_by', 'check_by', 'historical_data_version'], 'default', 'value' => null],
            [['request_id', 'num_order_id', 'essay_id', 'composite_sample_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'num_order', 'request_process_essay_activity_sample_status_id', 'deleted_by', 'number_value', 'request_process_essay_activity_id', 'registered_by', 'activity_id', 'composite_sample_group_id', 'start_by', 'finish_by', 'check_by', 'historical_data_version'], 'integer'],
            [['deleted_at', 'registered_at', 'start_date', 'finish_date', 'check_date'], 'safe'],
            [['available', 'is_deleted'], 'boolean'],
            [['text_value'], 'string', 'max' => 300],
            [['description_value'], 'string', 'max' => 100],
            [['start_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['start_by' => 'user_id']],
            [['finish_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['finish_by' => 'user_id']],
            [['check_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['check_by' => 'user_id']],
            [['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::class, 'targetAttribute' => ['activity_id' => 'activity_id']],
            [['composite_sample_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompositeSample::class, 'targetAttribute' => ['composite_sample_id' => 'composite_sample_id']],
            [['composite_sample_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompositeSampleGroup::class, 'targetAttribute' => ['composite_sample_group_id' => 'composite_sample_group_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['composite_sample_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['composite_sample_type_id' => 'parameter_id']],
            [['request_process_essay_activity_sample_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_process_essay_activity_sample_status_id' => 'parameter_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
            [['request_id', 'num_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcess::class, 'targetAttribute' => ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']],
            [['request_process_essay_activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessEssayActivity::class, 'targetAttribute' => ['request_process_essay_activity_id' => 'request_process_essay_activity_id']],
            [['workflow_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workflow::class, 'targetAttribute' => ['workflow_id' => 'workflow_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_process_essay_activity_sample_id' => 'Request Process Essay Activity Sample ID',
            'request_id' => 'Request ID',
            'num_order_id' => 'Num Order ID',
            'essay_id' => 'Essay ID',
            'composite_sample_id' => 'Composite Sample ID',
            'composite_sample_type_id' => 'Composite Sample Type ID',
            'crop_id' => 'Crop ID',
            'workflow_id' => 'Workflow ID',
            'num_order' => 'Num Order',
            'request_process_essay_activity_sample_status_id' => 'Request Process Essay Sample Activity Status ID',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'available' => 'Available',
            'text_value' => 'Text Value',
            'number_value' => 'Number Value',
            'description_value' => 'Description Value',
            'request_process_essay_activity_id' => 'Request Process Essay Activity ID',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'activity_id' => 'Activity ID',
            'composite_sample_group_id' => 'Composite Sample Group ID',
            'start_by' => 'Start By',
            'finish_by' => 'Finish By',
            'check_by' => 'Check By',
            'start_date' => 'Start Date',
            'finish_date' => 'Finish Date',
            'check_date' => 'Check Date',
            'historical_data_version' => 'Historical Data Version',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[Activity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::class, ['activity_id' => 'activity_id']);
    }

    /**
     * Gets query for [[CheckBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCheckBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'check_by']);
    }

    /**
     * Gets query for [[CompositeSample]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSample()
    {
        return $this->hasOne(CompositeSample::class, ['composite_sample_id' => 'composite_sample_id']);
    }

    /**
     * Gets query for [[CompositeSampleGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleGroup()
    {
        return $this->hasOne(CompositeSampleGroup::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    /**
     * Gets query for [[CompositeSampleType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'composite_sample_type_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[Essay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    /**
     * Gets query for [[FinishBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFinishBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'finish_by']);
    }

    /**
     * Gets query for [[ReadingDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['request_process_essay_activity_sample_id' => 'request_process_essay_activity_sample_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[Request0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest0()
    {
        return $this->hasOne(RequestProcess::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivity()
    {
        return $this->hasOne(RequestProcessEssayActivity::class, ['request_process_essay_activity_id' => 'request_process_essay_activity_id']);
    }

    /**
     * Gets query for [[RequestProcessEssaySampleActivityStatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssaySampleActivityStatus()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_process_essay_activity_sample_status_id']);
    }

    /**
     * Gets query for [[StartBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStartBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'start_by']);
    }

    /**
     * Gets query for [[Workflow]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(Workflow::class, ['workflow_id' => 'workflow_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = $this->getUserId();
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->available = true;
        }
        return parent::beforeSave($insert);
    }

    private function getUserId()
    {
        $user_id =  null;
        $_token = Yii::$app->request->headers->get('authorization');

        if (!is_null($_token)) {

            $token = str_replace('Bearer ', '', $_token);
            $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
        } else {
            $user_id = \Yii::$app->user->identity->id;
        }
        return $user_id;
    }


    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

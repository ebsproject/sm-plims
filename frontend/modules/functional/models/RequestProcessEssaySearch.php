<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\RequestProcessEssay;

/**
 * RequestProcessEssaySearch represents the model behind the search form of `frontend\modules\functional\models\RequestProcessEssay`.
 */
class RequestProcessEssaySearch extends RequestProcessEssay
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'essay_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'agent_qty', 'registered_by', 'deleted_by', 'num_order', 'sample_qty', 'request_process_essay_status_id', 'request_process_essay_type_id', 'activity_qty', 'request_process_essay_root_id', 'support_master_id', 'request_process_essay_id', 'start_by', 'finish_by', 'check_by'], 'integer'],
            [['start_date', 'finish_date', 'check_date', 'agent_detail', 'registered_at', 'deleted_at', 'status', 'sample_detail', 'activity_detail', 'observation'], 'safe'],
            [['sub_total_cost_essay'], 'number'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestProcessEssay::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'request_id' => $this->request_id,
            'num_order_id' => $this->num_order_id,
            'essay_id' => $this->essay_id,
            'composite_sample_type_id' => $this->composite_sample_type_id,
            'crop_id' => $this->crop_id,
            'workflow_id' => $this->workflow_id,
            'start_date' => $this->start_date,
            'finish_date' => $this->finish_date,
            'check_date' => $this->check_date,
            'agent_qty' => $this->agent_qty,
            'sub_total_cost_essay' => $this->sub_total_cost_essay,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'num_order' => $this->num_order,
            'sample_qty' => $this->sample_qty,
            'request_process_essay_status_id' => $this->request_process_essay_status_id,
            'request_process_essay_type_id' => $this->request_process_essay_type_id,
            'activity_qty' => $this->activity_qty,
            'request_process_essay_root_id' => $this->request_process_essay_root_id,
            'support_master_id' => $this->support_master_id,
            'request_process_essay_id' => $this->request_process_essay_id,
            'start_by' => $this->start_by,
            'finish_by' => $this->finish_by,
            'check_by' => $this->check_by,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['ilike', 'agent_detail', $this->agent_detail])
            ->andFilterWhere(['ilike', 'status', $this->status])
            ->andFilterWhere(['ilike', 'sample_detail', $this->sample_detail])
            ->andFilterWhere(['ilike', 'activity_detail', $this->activity_detail])
            ->andFilterWhere(['ilike', 'observation', $this->observation]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\Request;

/**
 * RequestSearch represents the model behind the search form of `frontend\modules\functional\models\Request`.
 */
class RequestSearch extends Request
{
    const STATUS_1 = 'active';


    public function rules()
    {
        return [
            [['request_id', 'year_code', 'sequential_code', 'agent_qty', 'material_qty', 'request_type_id', 'request_status_id', 'request_payment_id', 'requested_user_id', 'owner_user_id', 'registered_by', 'deleted_by', 'sample_qty', 'material_unit_qty'], 'integer'],
            [['request_code', 'details', 'diagnostic', 'pay_number', 'registered_at', 'deleted_at', 'status'], 'safe'],
            [['total_cost'], 'number'],
            [['is_deleted'], 'boolean'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }


    public function search($params, $order_list = null)
    {
        $query = Request::find()
            ->where(
                [
                    'plims.bsns_request.status' => self::STATUS_1
                ]
            );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10,],
            'sort' => ['defaultOrder' => $order_list],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }



        $query->joinWith(
            [
                'requestDetail' => function ($query) {
                    $query->from(['r1' => 'plims.bsns_request_detail']);
                }
            ]
        );




        $query->andFilterWhere([
            'request_id' => $this->request_id,
            'year_code' => $this->year_code,
            'sequential_code' => $this->sequential_code,
            'agent_qty' => $this->agent_qty,
            'material_qty' => $this->material_qty,
            'total_cost' => $this->total_cost,
            'request_type_id' => $this->request_type_id,
            'request_status_id' => $this->request_status_id,
            'request_payment_id' => $this->request_payment_id,
            'requested_user_id' => $this->requested_user_id,
            'owner_user_id' => $this->owner_user_id,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'sample_qty' => $this->sample_qty,
            'material_unit_qty' => $this->material_unit_qty,
            'is_deleted' => $this->is_deleted,
        ]);

        $query

            ->andFilterWhere(['like', 'r1.bar_id', $this->request_id])
            ->andFilterWhere(['ilike', 'request_code', $this->request_code])
            ->andFilterWhere(['ilike', 'details', $this->details])
            ->andFilterWhere(['ilike', 'diagnostic', $this->diagnostic])
            ->andFilterWhere(['ilike', 'pay_number', $this->pay_number])
            ->andFilterWhere(['ilike', 'status', $this->status]);

        return $dataProvider;
    }
}

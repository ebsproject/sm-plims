<?php

namespace frontend\modules\functional\models;

use Yii;
use common\models\User;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Workflow;
use frontend\modules\configuration\models\Activity;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Location;
use frontend\modules\configuration\models\ActivityByEssay;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plims.bsns_request_process_essay_activity".
 *
 * @property int $request_id
 * @property int $num_order_id
 * @property int $essay_id
 * @property int $activity_id
 * @property int $composite_sample_type_id
 * @property int|null $crop_id
 * @property int|null $workflow_id
 * @property int|null $request_process_essay_activity_status_id
 * @property int|null $num_order
 * @property string|null $start_date
 * @property string|null $finish_date
 * @property string|null $check_date
 * @property int|null $control_location_id
 * @property int|null $control_user_id
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 * @property int $request_process_essay_activity_id
 * @property int|null $request_process_essay_id
 * @property int|null $start_by
 * @property int|null $finish_by
 * @property int|null $check_by
 * @property bool $is_deleted
 *
 * @property Activity $activity
 * @property User $checkBy
 * @property Parameter $compositeSampleType
 * @property Location $controlLocation
 * @property User $controlUser
 * @property Crop $crop
 * @property ActivityByEssay $essay
 * @property Essay $essay0
 * @property User $finishBy
 * @property ReadingData[] $readingDatas
 * @property Request $request
 * @property RequestProcess $request0
 * @property RequestProcessEssay $requestProcessEssay
 * @property RequestProcessEssayActivitySample[] $requestProcessEssayActivitySamples
 * @property Parameter $requestProcessEssayActivityStatus
 * @property User $startBy
 * @property Support[] $supports
 * @property Workflow $workflow
 */
class RequestProcessEssayActivity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_request_process_essay_activity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'essay_id', 'activity_id', 'composite_sample_type_id'], 'required'],
            [['request_id', 'num_order_id', 'essay_id', 'activity_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'request_process_essay_activity_status_id', 'num_order', 'control_location_id', 'control_user_id', 'registered_by', 'deleted_by', 'request_process_essay_id', 'start_by', 'finish_by', 'check_by'], 'default', 'value' => null],
            [['request_id', 'num_order_id', 'essay_id', 'activity_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'request_process_essay_activity_status_id', 'num_order', 'control_location_id', 'control_user_id', 'registered_by', 'deleted_by', 'request_process_essay_id', 'start_by', 'finish_by', 'check_by'], 'integer'],
            [['start_date', 'finish_date', 'check_date', 'registered_at', 'deleted_at'], 'safe'],
            [['is_deleted'], 'boolean'],
            [['status'], 'string', 'max' => 15],
            [['control_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['control_user_id' => 'user_id']],
            [['start_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['start_by' => 'user_id']],
            [['finish_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['finish_by' => 'user_id']],
            [['check_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['check_by' => 'user_id']],
            [['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::class, 'targetAttribute' => ['activity_id' => 'activity_id']],
            [['essay_id', 'activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => ActivityByEssay::class, 'targetAttribute' => ['essay_id' => 'essay_id', 'activity_id' => 'activity_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['control_location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::class, 'targetAttribute' => ['control_location_id' => 'location_id']],
            [['composite_sample_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['composite_sample_type_id' => 'parameter_id']],
            [['request_process_essay_activity_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_process_essay_activity_status_id' => 'parameter_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
            [['request_id', 'num_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcess::class, 'targetAttribute' => ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']],
            [['request_process_essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => RequestProcessEssay::class, 'targetAttribute' => ['request_process_essay_id' => 'request_process_essay_id']],
            [['workflow_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workflow::class, 'targetAttribute' => ['workflow_id' => 'workflow_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'num_order_id' => 'Num Order ID',
            'essay_id' => 'Essay ID',
            'activity_id' => 'Activity ID',
            'composite_sample_type_id' => 'Composite Sample Type ID',
            'crop_id' => 'Crop ID',
            'workflow_id' => 'Workflow ID',
            'request_process_essay_activity_status_id' => 'Request Process Essay Activity Status ID',
            'num_order' => 'Num Order',
            'start_date' => 'Start Date',
            'finish_date' => 'Finish Date',
            'check_date' => 'Check Date',
            'control_location_id' => 'Control Location ID',
            'control_user_id' => 'Control User ID',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'request_process_essay_activity_id' => 'Request Process Essay Activity ID',
            'request_process_essay_id' => 'Request Process Essay ID',
            'start_by' => 'Start By',
            'finish_by' => 'Finish By',
            'check_by' => 'Check By',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * Gets query for [[Activity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::class, ['activity_id' => 'activity_id']);
    }

    /**
     * Gets query for [[CheckBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCheckBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'check_by']);
    }

    /**
     * Gets query for [[CompositeSampleType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'composite_sample_type_id']);
    }

    /**
     * Gets query for [[ControlLocation]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getControlLocation()
    {
        return $this->hasOne(Location::class, ['location_id' => 'control_location_id']);
    }

    /**
     * Gets query for [[ControlUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getControlUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'control_user_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[Essay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(ActivityByEssay::class, ['essay_id' => 'essay_id', 'activity_id' => 'activity_id']);
    }

    /**
     * Gets query for [[Essay0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssay0()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    /**
     * Gets query for [[FinishBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFinishBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'finish_by']);
    }

    /**
     * Gets query for [[ReadingDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['request_process_essay_activity_id' => 'request_process_essay_activity_id']);
    }

    /**
     * Gets query for [[Request]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    /**
     * Gets query for [[Request0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequest0()
    {
        return $this->hasOne(RequestProcess::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id']);
    }

    /**
     * Gets query for [[RequestProcessEssay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssay()
    {
        return $this->hasOne(RequestProcessEssay::class, ['request_process_essay_id' => 'request_process_essay_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivitySamples]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivitySamples()
    {
        return $this->hasMany(RequestProcessEssayActivitySample::class, ['request_process_essay_activity_id' => 'request_process_essay_activity_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivityStatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivityStatus()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_process_essay_activity_status_id']);
    }

    /**
     * Gets query for [[StartBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStartBy()
    {
        return $this->hasOne(User::class, ['user_id' => 'start_by']);
    }

    /**
     * Gets query for [[Supports]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::class, ['request_process_essay_activity_id' => 'request_process_essay_activity_id']);
    }

    /**
     * Gets query for [[Workflow]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(Workflow::class, ['workflow_id' => 'workflow_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = $this->getUserId();
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        }
        return parent::beforeSave($insert);
    }

    private function getUserId()
    {
        $user_id =  null;
        $_token = Yii::$app->request->headers->get('authorization');

        if (!is_null($_token)) {

            $token = str_replace('Bearer ', '', $_token);
            $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
        } else {
            $user_id = \Yii::$app->user->identity->id;
        }
        return $user_id;
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\RequestDetail;

/**
 * RequestDetailSearch represents the model behind the search form of `frontend\modules\functional\models\RequestDetail`.
 */
class RequestDetailSearch extends RequestDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'registered_by', 'updated_by', 'deleted_by', 'requested_user', 'consignee_user'], 'integer'],
            [['bar_id', 'st_id', 'plims_id', 'request_program', 'request_sub_program', 'identification_material', 'seed_distribution', 'seed_sent_by', 'icc_seed_health_testing', 'certification', 'cost_shipment_paid_by', 'seed_purpose', 'shipping_date_desired', 'consignee_name', 'request_position', 'request_department', 'request_institution', 'request_street', 'request_city', 'request_country', 'request_phone', 'request_email', 'registered_at', 'updated_at', 'deleted_at', 'status', 'recipient_name', 'recipient_institution', 'recipient_address', 'recipient_country', 'recipient_program', 'recipient_account', 'sender_name', 'sender_institution', 'sender_address', 'sender_country', 'sender_program', 'sender_account', 'international_phytosanitary_certificate', 'quarantine_custody', 'requirements_sheet_folio', 'total_shipping_weight'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'request_id' => $this->request_id,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'requested_user' => $this->requested_user,
            'consignee_user' => $this->consignee_user,
        ]);

        $query->andFilterWhere(['like', 'bar_id', $this->bar_id])
            ->andFilterWhere(['like', 'st_id', $this->st_id])
            ->andFilterWhere(['like', 'plims_id', $this->plims_id])
            ->andFilterWhere(['like', 'request_program', $this->request_program])
            ->andFilterWhere(['like', 'request_sub_program', $this->request_sub_program])
            ->andFilterWhere(['like', 'identification_material', $this->identification_material])
            ->andFilterWhere(['like', 'seed_distribution', $this->seed_distribution])
            ->andFilterWhere(['like', 'seed_sent_by', $this->seed_sent_by])
            ->andFilterWhere(['like', 'icc_seed_health_testing', $this->icc_seed_health_testing])
            ->andFilterWhere(['like', 'certification', $this->certification])
            ->andFilterWhere(['like', 'cost_shipment_paid_by', $this->cost_shipment_paid_by])
            ->andFilterWhere(['like', 'seed_purpose', $this->seed_purpose])
            ->andFilterWhere(['like', 'shipping_date_desired', $this->shipping_date_desired])
            ->andFilterWhere(['like', 'consignee_name', $this->consignee_name])
            ->andFilterWhere(['like', 'request_position', $this->request_position])
            ->andFilterWhere(['like', 'request_department', $this->request_department])
            ->andFilterWhere(['like', 'request_institution', $this->request_institution])
            ->andFilterWhere(['like', 'request_street', $this->request_street])
            ->andFilterWhere(['like', 'request_city', $this->request_city])
            ->andFilterWhere(['like', 'request_country', $this->request_country])
            ->andFilterWhere(['like', 'request_phone', $this->request_phone])
            ->andFilterWhere(['like', 'request_email', $this->request_email])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'recipient_name', $this->recipient_name])
            ->andFilterWhere(['like', 'recipient_institution', $this->recipient_institution])
            ->andFilterWhere(['like', 'recipient_address', $this->recipient_address])
            ->andFilterWhere(['like', 'recipient_country', $this->recipient_country])
            ->andFilterWhere(['like', 'recipient_program', $this->recipient_program])
            ->andFilterWhere(['like', 'recipient_account', $this->recipient_account])
            ->andFilterWhere(['like', 'sender_name', $this->sender_name])
            ->andFilterWhere(['like', 'sender_institution', $this->sender_institution])
            ->andFilterWhere(['like', 'sender_address', $this->sender_address])
            ->andFilterWhere(['like', 'sender_country', $this->sender_country])
            ->andFilterWhere(['like', 'sender_program', $this->sender_program])
            ->andFilterWhere(['like', 'sender_account', $this->sender_account])
            ->andFilterWhere(['like', 'international_phytosanitary_certificate', $this->international_phytosanitary_certificate])
            ->andFilterWhere(['like', 'quarantine_custody', $this->quarantine_custody])
            ->andFilterWhere(['like', 'requirements_sheet_folio', $this->requirements_sheet_folio])
            ->andFilterWhere(['like', 'total_shipping_weight', $this->total_shipping_weight]);

        return $dataProvider;
    }
}

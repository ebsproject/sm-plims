<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\RequestDocumentation;

/**
 * RequestDocumentationSearch represents the model behind the search form of `frontend\modules\functional\models\RequestDocumentation`.
 */
class RequestDocumentationSearch extends RequestDocumentation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_documentation_id', 'request_id', 'file_type_id', 'registered_by', 'updated_by', 'deleted_by', 'num_order_id', 'essay_id'], 'integer'],
            [['path_documentation', 'document_name', 'document_extension', 'registered_at', 'updated_at', 'deleted_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestDocumentation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'request_documentation_id' => $this->request_documentation_id,
            'request_id' => $this->request_id,
            'file_type_id' => $this->file_type_id,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'num_order_id' => $this->num_order_id,
            'essay_id' => $this->essay_id,
        ]);

        $query->andFilterWhere(['like', 'path_documentation', $this->path_documentation])
            ->andFilterWhere(['like', 'document_name', $this->document_name])
            ->andFilterWhere(['like', 'document_extension', $this->document_extension])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

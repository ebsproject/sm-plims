<?php

namespace frontend\modules\functional\models;

use Yii;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Parameter;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plims.bsns_composite_sample".
 *
 * @property int $composite_sample_id
 * @property int $composite_sample_type_id
 * @property int|null $composite_sample_group_id
 * @property int|null $crop_id
 * @property int|null $num_order
 * @property string|null $short_name
 * @property int|null $material_qty
 * @property int|null $material_unit_qty
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 * @property string|null $long_name
 * @property int|null $reading_data_type_id
 * @property int|null $material_group_id
 *
 * @property CompositeSampleGroup $compositeSampleGroup
 * @property Parameter $compositeSampleType
 * @property Crop $crop
 * @property MaterialGroup $materialGroup
 * @property Material[] $materials
 * @property Parameter $readingDataType
 * @property RequestProcessEssayActivitySample[] $requestProcessEssayActivitySamples
 */
class CompositeSample extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_composite_sample';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['composite_sample_type_id'], 'required'],
            [['composite_sample_type_id', 'composite_sample_group_id', 'crop_id', 'num_order', 'material_qty', 'material_unit_qty', 'registered_by', 'updated_by', 'deleted_by', 'reading_data_type_id', 'material_group_id'], 'default', 'value' => null],
            [['composite_sample_type_id', 'composite_sample_group_id', 'crop_id', 'num_order', 'material_qty', 'material_unit_qty', 'registered_by', 'updated_by', 'deleted_by', 'reading_data_type_id', 'material_group_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['short_name'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 15],
            [['long_name'], 'string', 'max' => 150],
            [['composite_sample_type_id', 'composite_sample_group_id', 'num_order', 'reading_data_type_id'], 'unique', 'targetAttribute' => ['composite_sample_type_id', 'composite_sample_group_id', 'num_order', 'reading_data_type_id']],
            [['composite_sample_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompositeSampleGroup::class, 'targetAttribute' => ['composite_sample_group_id' => 'composite_sample_group_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['material_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => MaterialGroup::class, 'targetAttribute' => ['material_group_id' => 'material_group_id']],
            [['composite_sample_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['composite_sample_type_id' => 'parameter_id']],
            [['reading_data_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['reading_data_type_id' => 'parameter_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'composite_sample_id' => 'Composite Sample ID',
            'composite_sample_type_id' => 'Composite Sample Type ID',
            'composite_sample_group_id' => 'Composite Sample Group ID',
            'crop_id' => 'Crop ID',
            'num_order' => 'Num Order',
            'short_name' => 'Short Name',
            'material_qty' => 'Material Qty',
            'material_unit_qty' => 'Material Unit Qty',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'long_name' => 'Long Name',
            'reading_data_type_id' => 'Reading Data Type ID',
            'material_group_id' => 'Material Group ID',
        ];
    }

    /**
     * Gets query for [[CompositeSampleGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleGroup()
    {
        return $this->hasOne(CompositeSampleGroup::class, ['composite_sample_group_id' => 'composite_sample_group_id']);
    }

    /**
     * Gets query for [[CompositeSampleType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'composite_sample_type_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[MaterialGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialGroup()
    {
        return $this->hasOne(MaterialGroup::class, ['material_group_id' => 'material_group_id']);
    }

    /**
     * Gets query for [[Materials]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(Material::class, ['composite_sample_id' => 'composite_sample_id']);
    }

    /**
     * Gets query for [[ReadingDataType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDataType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'reading_data_type_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivitySamples]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivitySamples()
    {
        return $this->hasMany(RequestProcessEssayActivitySample::class, ['composite_sample_id' => 'composite_sample_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

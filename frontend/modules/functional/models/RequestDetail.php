<?php

namespace frontend\modules\functional\models;

use Yii;
use common\models\User;
use frontend\modules\configuration\models\Crop;
use yii\helpers\ArrayHelper;
use frontend\modules\configuration\models\Parameter;

class RequestDetail extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'plims.bsns_request_detail';
    }

    public function rules()
    {
        return [
            [['request_id'], 'required'],
            [['request_id', 'registered_by', 'updated_by', 'deleted_by', 'requested_user', 'consignee_user'], 'default', 'value' => null],
            [['request_id', 'registered_by', 'updated_by', 'deleted_by', 'requested_user', 'consignee_user', 'crop_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['bar_id', 'st_id', 'plims_id', 'request_program', 'request_sub_program', 'identification_material', 'seed_distribution', 'seed_sent_by', 'icc_seed_health_testing', 'certification', 'cost_shipment_paid_by', 'seed_purpose', 'shipping_date_desired', 'consignee_name', 'request_position', 'request_department', 'request_institution', 'request_street', 'request_city', 'request_country', 'request_phone', 'request_email', 'recipient_name', 'recipient_institution', 'recipient_address', 'recipient_country', 'recipient_program', 'recipient_account', 'sender_name', 'sender_institution', 'sender_address', 'sender_country', 'sender_program', 'sender_account', 'international_phytosanitary_certificate', 'quarantine_custody', 'requirements_sheet_folio', 'total_shipping_weight'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 15],
            [['request_id'], 'unique'],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['requested_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['requested_user' => 'user_id']],
            [['consignee_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['consignee_user' => 'user_id']],
            [['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => Request::class, 'targetAttribute' => ['request_id' => 'request_id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'request_id' => 'Request ID',
            'bar_id' => 'Bar ID',
            'st_id' => 'St ID',
            'plims_id' => 'Plims ID',
            'request_program' => 'Request Program',
            'request_sub_program' => 'Request Sub Program',
            'identification_material' => 'Identification Material',
            'seed_distribution' => 'Seed Distribution',
            'seed_sent_by' => 'Seed Sent By',
            'icc_seed_health_testing' => 'Icc Seed Health Testing',
            'certification' => 'Certification',
            'cost_shipment_paid_by' => 'Cost Shipment Paid By',
            'seed_purpose' => 'Seed Purpose',
            'shipping_date_desired' => 'Shipping Date Desired',
            'consignee_name' => 'Consignee Name',
            'request_position' => 'Request Position',
            'request_department' => 'Request Department',
            'request_institution' => 'Request Institution',
            'request_street' => 'Request Street',
            'request_city' => 'Request City',
            'request_country' => 'Request Country',
            'request_phone' => 'Request Phone',
            'request_email' => 'Request Email',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'requested_user' => 'Requested User',
            'consignee_user' => 'Consignee User',


            'recipient_name' => 'Name',
            'recipient_institution' => 'Institution',
            'recipient_address' => 'Address',
            'recipient_country' => 'Country',
            'recipient_program' => 'Program',
            'recipient_account' => 'Account number',


            'sender_name' => 'Name',
            'sender_institution' => 'Institution',
            'sender_address' => 'Address',
            'sender_country' => 'Country',
            'sender_program' => 'Program',
            'sender_account' => 'Account number',


            'international_phytosanitary_certificate' => 'International phytosanitary certificate',
            'quarantine_custody' => 'Quarantine custody number',
            'requirements_sheet_folio' => 'Requirements Sheet folio number',
            'total_shipping_weight' => 'Total shipping weight (kg)',
            'crop_id' => 'Crop ID',
        ];
    }

    public function getRequestedUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'requested_user']);
    }

    public function getConsigneeUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'consignee_user']);
    }

    public function getRequest()
    {
        return $this->hasOne(Request::class, ['request_id' => 'request_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

<?php

namespace frontend\modules\functional\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\functional\models\RequestProcessEssayActivity;

/**
 * RequestProcessEssayActivitySearch represents the model behind the search form of `frontend\modules\functional\models\RequestProcessEssayActivity`.
 */
class RequestProcessEssayActivitySearch extends RequestProcessEssayActivity
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['request_id', 'num_order_id', 'essay_id', 'activity_id', 'composite_sample_type_id', 'crop_id', 'workflow_id', 'request_process_essay_activity_status_id', 'num_order', 'control_location_id', 'control_user_id', 'registered_by', 'deleted_by', 'request_process_essay_activity_id', 'request_process_essay_id', 'start_by', 'finish_by', 'check_by'], 'integer'],
            [['start_date', 'finish_date', 'check_date', 'registered_at', 'deleted_at', 'status'], 'safe'],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestProcessEssayActivity::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'request_id' => $this->request_id,
            'num_order_id' => $this->num_order_id,
            'essay_id' => $this->essay_id,
            'activity_id' => $this->activity_id,
            'composite_sample_type_id' => $this->composite_sample_type_id,
            'crop_id' => $this->crop_id,
            'workflow_id' => $this->workflow_id,
            'request_process_essay_activity_status_id' => $this->request_process_essay_activity_status_id,
            'num_order' => $this->num_order,
            'start_date' => $this->start_date,
            'finish_date' => $this->finish_date,
            'check_date' => $this->check_date,
            'control_location_id' => $this->control_location_id,
            'control_user_id' => $this->control_user_id,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'request_process_essay_activity_id' => $this->request_process_essay_activity_id,
            'request_process_essay_id' => $this->request_process_essay_id,
            'start_by' => $this->start_by,
            'finish_by' => $this->finish_by,
            'check_by' => $this->check_by,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['ilike', 'status', $this->status]);

        return $dataProvider;
    }
}

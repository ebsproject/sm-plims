<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use bizley\jwt\JwtHttpBearerAuth;
use frontend\modules\configuration\models\Activity;
use frontend\modules\functional\models\RequestProcessEssay;
use frontend\models\Script;
use frontend\modules\functional\Functional;
use PhpOffice\PhpSpreadsheet\Calculation\Functions;

class ElisaController extends ActiveController
{
  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
      'class' =>  JwtHttpBearerAuth::class,
      'except' => [],

    ];
    $behaviors['JWTBearerAuth'] = [
      'class' =>  JwtHttpBearerAuth::class,
    ];
    return $behaviors;
  }

  public $modelClass = Request::class;

  public function actionEvent(
    $event_id = null
  ) {
    $data = null;
    $message_resp = null;
    $status_resp = 200;
    try {
      $body = Yii::$app->request->post();
      if (Yii::$app->request->isGet) {
        $data =  $this->getEvent($event_id);
      } else if (Yii::$app->request->isPost) {
        $data = $this->setEvent($body);
      }
    } catch (\Throwable $th) {
      $message_resp = $th->getMessage();
      $status_resp = 400;
    }
    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  private function getEvent($event_id)
  {
    $events = Yii::$app->db->createCommand(
      "SELECT t1.request_id                                      AS request,
            t1.agent_id                                        AS agents,
            t1.num_order_id                                    AS process,
            t2.request_code,
            'MR' || RIGHT('0000' || Cast(t1.support_master_id AS TEXT), 5) AS event_code,
            t1.request_process_essay_status_id                 AS status_id,
            CASE
              WHEN t1.request_process_essay_status_id = :_rpa_pending THEN
              'in distribution'
              WHEN t1.request_process_essay_status_id = :_rpa_in_process THEN
              'in absorbance registration'
              ELSE 'multiple-request process finished'
            END                                                AS status_name
      FROM   plims.bsns_multi_request_event t1
            LEFT JOIN plims.bsns_request t2
                  ON t1.request_id = t2.request_id
      WHERE  t1.support_master_id = :_support_master_id; ",
      [
        ':_rpa_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
        ':_rpa_in_process' => Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
        ':_support_master_id' => $event_id
      ]
    )->queryAll();

    $data['event_id'] =  intval($event_id);


    foreach ($events as $key => $value) {
      if (!is_null($value['request'])) {
        $data['event_code'] = $value['event_code'];
        $data['status_id']  = $value['status_id'];
        $data['status_name']  = $value['status_name'];
        $data['request_process'][] = [
          'request_id' => $value['request'],
          'process_id' => $value['process'],
          'request_code' => $value['request_code']
        ];
      } else if (!is_null($value['agents'])) {
        $data['agents'][] = $value['agents'];
      }
    }

    return $data;
  }

  private function setEvent($body)
  {
    $agent_ids = $body['agents'];
    $request_process = $body['request_process'];

    $date = date("Y-m-d H:i:s", time());
    $user =  $this->getUserId();

    $max_id_plus = Yii::$app->db->createCommand(
      "SELECT COALESCE(Max(support_master_id) + 1, 1)
      FROM   plims.bsns_support_master "
    )->queryScalar();

    $support_master_id = Yii::$app->db->createCommand(
      "INSERT INTO plims.bsns_support_master
      (
        support_code,
        registered_by,
        registered_at,
        status
      )
      VALUES
      (
        :_support_code,
        :_registered_by,
        :_registered_at,
        :_status_active
      )
      RETURNING support_master_id;",
      [
        ':_support_code' => 'PC' . (int) date("Y") . '.' . $max_id_plus,
        ':_registered_by' => $user,
        ':_registered_at' => $date,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryScalar();

    foreach ($request_process as $key => $value) {
      Yii::$app->db->createCommand(
        "INSERT INTO plims.bsns_multi_request_event(
          support_master_id,
          request_id,
          num_order_id,
          request_process_essay_status_id
        )VALUES(
          :_support_master_id,
          :_request_id,
          :_process_id,
          :_rpa_pending
        )
        ",
        [
          ':_support_master_id' => $support_master_id,
          ':_request_id' => $value['request_id'],
          ':_process_id' => $value['process_id'],
          ':_rpa_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
        ]
      )->execute();
    }

    foreach ($agent_ids as $key => $agent_id) {
      Yii::$app->db->createCommand(
        "INSERT INTO plims.bsns_multi_request_event(
          support_master_id ,
          agent_id,
          request_process_essay_status_id
        )VALUES(
          :_support_master_id,
          :_agent_id,
          :_rpa_pending
        )
        ",
        [
          ':_support_master_id' => $support_master_id,
          ':_agent_id' => $agent_id,
          ':_rpa_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
        ]
      )->execute();
    }
    return ['event_id' => $support_master_id];
  }

  // ---------------------------------------------

  public function actionDistributionDev(
    $event_id = null,
    $agent_ids = null,
    $support_order_ids = null
  ) {
    $data = null;
    $message_resp = '';
    $status_resp = 200;

    $date = date("Y-m-d H:i:s", time());
    $user =  $this->getUserId();

    try {
      if (Yii::$app->request->isPost) {
        $new_distribution_list = [];
        $distribution_list = Yii::$app->request->post();



        // re-ordenar y armar nueva lista de distribucion
        foreach ($distribution_list as $key1 => $distribution) {
          $to_delete = (bool) $distribution['to_delete'];

          if ($to_delete) {
            $dist_request_process_essay_id = null;
            $dist_composite_sample_id = null;
            $dist_reading_data_type_id = null;
          } else {
            // MEJORAR EL PROCESO DE VALIDACION PARA NO EJECUTAR LA SENTENCIA SI NO ES NECESARIO
            $dist_request_process_essay_id = $distribution['request_process_essay_id'];
            $dist_composite_sample_id = $distribution['composite_sample_id'];
            $dist_reading_data_type_id = $distribution['reading_data_type_id'];
            $dist_agent_id = $distribution['agent_id'];
            // obtener parametros para las consultas 
            $data_params = Yii::$app->db->createCommand(
              "SELECT request_id, num_order_id, essay_id
              FROM   plims.bsns_request_process_essay
              WHERE  request_process_essay_id = :_rpa_id;",
              [
                ':_rpa_id' => $dist_request_process_essay_id
              ]
            )->queryOne();
            $_request_id = $data_params['request_id'];
            $_num_order_id = $data_params['num_order_id'];
            $_essay_id = $data_params['essay_id'];



            Script::instance()->checkRequest($_request_id, $_num_order_id, $_essay_id, $user, $date);
            Script::instance()->checkStatus($_request_id, $user, $date);
            Script::instance()->checkRequestProcess($_request_id, $_num_order_id, $_essay_id, $user, $date);
            // activacion RPA HEADER de fecha, registrador y status en active
            // validacion que se encuentre en pendiente
            Script::instance()->checkRequestProcessAssayHeader($_request_id, $_num_order_id, $_essay_id, $user, $date);
            // activacion RPA de fecha, registrador y status en active
            // validacion que se encuentre en pendiente
            Script::instance()->checkRequestProcessAssay($dist_request_process_essay_id, $user, $date);
            // activación RPA-AGENT status en active
            // validación status disabled
            Script::instance()->checkRequestProcessAssayAgent($dist_request_process_essay_id, $dist_agent_id, $user, $date);
            // activacion RPA-ACTIVITY de fecha, registrador y status en active
            // validacion que se encuentre en pendiente
            Script::instance()->checkRequestProcessAssayActivity($dist_request_process_essay_id, $user, $date);
            // activacion RPA-ACTIVITY-SAMPLE de fecha, registrador y ensable en true
            // validacion que este en pendiente
            Script::instance()->checkRequestProcessAssayActivitySample($dist_request_process_essay_id, 0, $dist_composite_sample_id, $user, $date);
          }

          $dist_event_id = $distribution['event_id'];
          $dist_agent_id = $distribution['agent_id'];
          $dist_support_order_id = $distribution['support_order_id'];
          $dist_cell_position = $distribution['cell_position'];

          // verificar si existe en otros eventos
          $is_new_in_reviewer = (bool) Yii::$app->db->createCommand(
            "SELECT CASE
                      WHEN Count(composite_sample_id) > 0 THEN false
                      ELSE true
                    END AS is_new_in_reviewer
            FROM   plims.bsns_agent_sample_reviewer
            WHERE  agent_id = :_agent_id
                    AND composite_sample_id = :_composite_sample_id
                    AND support_master_id <> :_event_id; ",
            [
              ':_composite_sample_id' => $dist_composite_sample_id,
              ':_agent_id' => $dist_agent_id,
              ':_event_id' => $dist_event_id,
            ]
          )->queryScalar();

          if ($is_new_in_reviewer || $dist_reading_data_type_id != Functional::READING_DATA_ENTRY) {
            // rellenado de las repeticiones o injertos (grafting) y posiciones de estas
            for ($repetition = 0; $repetition < Functional::ELISA_REPETITION; $repetition++) {
              $new_distribution_list[] = [
                'to_delete' => $to_delete,
                'request_process_essay_id' => $dist_request_process_essay_id,
                'event_id' => $dist_event_id,
                'composite_sample_id' => $dist_composite_sample_id,
                'support_order_id' => $dist_support_order_id,
                'agent_id' => $dist_agent_id,
                'cell_position' => $dist_cell_position + $repetition * 8,
                'repetition' => $repetition,
                'reading_data_type_id' => $dist_reading_data_type_id,
              ];
            }
          } else {
            $data =  'There are distributions found in another event, these have not been registered!';
          }
        }

        foreach ($new_distribution_list as $key => $new_distribution) {
          Script::instance()->mobSampleDistributionDev(
            $new_distribution['to_delete'],
            $new_distribution['request_process_essay_id'],
            $new_distribution['event_id'],
            $new_distribution['composite_sample_id'],
            $new_distribution['support_order_id'],
            $new_distribution['agent_id'],
            $new_distribution['cell_position'],
            $new_distribution['repetition'],
            $new_distribution['reading_data_type_id'],
            $user,
            $date
          );
        }
      } else if (Yii::$app->request->isGet) {

        $act_id_first = Yii::$app->db->createCommand(
          "SELECT activity_id AS act_id_first
          FROM   plims.bsns_activity_by_essay
          WHERE  essay_id = :_assay_elisa
                AND status = :_status_active
                AND num_order = :_num_order_ini; ",
          [
            ':_assay_elisa' => Functional::ESSAY_ELISA,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_num_order_ini' => Functional::NUM_ORDER_INI,
          ]
        )->queryScalar();

        $query_1 = "SELECT t1.request_id,
                        t1.support_master_id        AS event_id,
                        t1.request_process_essay_id,
                        t1.composite_sample_type_id AS sample_type_id,
                        t1.primary_order_num        AS composite_sample_order,
                        t2.short_name               AS composite_sample_name,
                        t1.agent_id,
                        t1.composite_sample_id,
                        t1.reading_data_type_id,
                        t1.support_order_id,
                        t1.support_cell_position
                  FROM   plims.bsns_reading_data t1
                          INNER JOIN plims.bsns_composite_sample t2
                                  ON t1.composite_sample_id = t2.composite_sample_id
                  WHERE  t1.support_master_id = :_support_master_id
                          AND t1.status = :_status_active
                          AND t1.activity_id = :_act_id_first";

        $query_2 = ' ';

        if (!is_null($agent_ids) && !empty($agent_ids)) {
          $query_2 = ' AND t1.agent_id in (' . $agent_ids . ') ';
        }
        if (!is_null($support_order_ids) && !empty($support_order_ids)) {
          $query_2 = $query_2 . ' AND t1.support_order_id in (' . $support_order_ids . ') ';
        }

        $query_3 = "ORDER  BY t1.request_process_essay_id,
                        t1.agent_id,
                        t1.support_id,
                        t1.support_cell_position;";

        $query = $query_1 . $query_2 . $query_3;

        $data = Yii::$app->db->createCommand(
          $query,
          [
            ':_support_master_id' => $event_id,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_act_id_first' => $act_id_first,
          ]
        )->queryAll();
      }
    } catch (\Throwable $th) {
      $message_resp = $th->getMessage();
      $status_resp = 400;
    }


    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionMultiRequests(
    $laboratory_id
  ) {
    $data = [];
    $message_resp = null;
    $status_resp = 200;

    if (Yii::$app->request->isGet) {

      $data = Yii::$app->db->createCommand(
        "SELECT t1.support_master_id                               AS event_id,
                String_agg(DISTINCT CASE
                                      WHEN t3.num_order_id > 1 THEN t2.request_code || ' (R)'
                                      ELSE t2.request_code
                                    END, ', ')                     AS request_codes,
                String_agg(DISTINCT c1.short_name, ', ')           AS crop_names,
                String_agg(DISTINCT p1.short_name, ', ')           AS distribution_names,
                'MR' || RIGHT('0000' || Cast(t1.support_master_id AS TEXT), 5) AS event_code,
                t1.request_process_essay_status_id                 AS status_id,
                CASE
                  WHEN t1.request_process_essay_status_id = :_rpa_pending THEN 'in distribution'
                  WHEN t1.request_process_essay_status_id = :_rpa_in_process THEN 'in absorbance registration'
                  ELSE 'multiple-request process finished'
                END                                                AS status_name
        FROM   plims.bsns_multi_request_event t1
                INNER JOIN plims.bsns_request t2
                        ON t1.request_id = t2.request_id
                          AND t2.status = :_status_active
                INNER JOIN plims.bsns_request_process t3
                        ON t2.request_id = t3.request_id
                          AND t3.num_order_id = t1.num_order_id
                LEFT JOIN plims.bsns_parameter p1
                      ON t2.request_type_id = p1.parameter_id
                LEFT JOIN plims.bsns_crop c1
                      ON t3.crop_id = c1.crop_id
        WHERE t2.laboratory_id = :laboratory_id
        GROUP  BY t1.support_master_id, t1.request_process_essay_status_id
        ORDER  BY t1.support_master_id DESC;",
        [
          ':_rpa_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
          ':_rpa_in_process' => Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':laboratory_id' => $laboratory_id
        ]
      )->queryAll();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionRequestsByAgents(
    $laboratory_id
  ) {
    // QUITAR LAS FINALIZADAS
    $data = [];
    $message_resp = null;
    $status_resp = 200;

    try {
      if (Yii::$app->request->isPost) {
        $agente_ids = Yii::$app->request->post();
        // sort($agente_ids);
        $_agents = implode(',', $agente_ids);
        $data =  Yii::$app->db->createCommand(

          "SELECT DISTINCT t1.request_id   AS request_id,
                    t1.request_code AS request_code,
                    t2.num_order_id AS process_id
          FROM   plims.bsns_request t1
          INNER JOIN plims.bsns_request_process t2
                  ON t1.request_id = t2.request_id
          INNER JOIN plims.bsns_request_process_essay t3  
                  ON t3.request_id = t2.request_id
                      AND t3.num_order_id = t2.num_order_id
          WHERE  t1.status = 'active'
          AND t1.laboratory_id = :laboratory_id
          AND t3.request_process_essay_status_id <> :_rpe_finished
          AND t3.composite_sample_type_id <> :_cst_header
          AND t3.essay_id = :_assay_elisa
          AND EXISTS (SELECT 1
                      FROM   plims.bsns_request_process_essay_agent t4
                      WHERE  ',' || :_agents || ',' 
                        LIKE '%,' || Cast(t4.agent_id AS TEXT) || ',%'
                        AND t3.request_process_essay_id = t4.request_process_essay_id)
          GROUP  BY t2.crop_id,
              t1.request_type_id,
              t3.composite_sample_type_id,
              t1.request_id,
              t1.request_code,
              t3.request_process_essay_id,
              t2.num_order_id
          ORDER  BY 1 DESC; ",
          [
            ':_rpe_finished' => Functional::REQUEST_PROCESS_ESSAY_FINISHED,
            ':_cst_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
            ':_assay_elisa' => Functional::ESSAY_ELISA,
            ':_agents' => $_agents,
            ':laboratory_id' => $laboratory_id
          ]
        )->queryAll();;
      }
    } catch (\Throwable $th) {
      $message_resp = $th->getMessage();
      $status_resp = 400;
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionAssayControlMaterial($request_process_essay_id)
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;
    try {
      if (Yii::$app->request->isGet) {
        $data = Yii::$app->db->createCommand(
          "SELECT DISTINCT t1.composite_sample_id,
                    c1.short_name AS composite_sample_name,
                    CASE
                      WHEN c1.reading_data_type_id = :_rd_entry THEN t1.num_order
                      ELSE NULL
                    END           AS num_order,
                    c1.reading_data_type_id
          FROM   plims.bsns_request_process_essay_activity_sample t1
          INNER JOIN plims.bsns_composite_sample c1
                  ON t1.composite_sample_id = c1.composite_sample_id
          LEFT JOIN plims.bsns_reading_data r1
                  ON t1.request_process_essay_activity_sample_id =
                              r1.request_process_essay_activity_sample_id
                    AND r1.status = :_status_active
          WHERE  ( c1.reading_data_type_id <> :_rd_entry )
          AND t1.request_process_essay_id = :_request_process_essay_id;",
          [
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_rd_entry' => Functional::READING_DATA_ENTRY,
            ':_request_process_essay_id' => $request_process_essay_id,
          ]
        )->queryAll();
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  // tipos de muestras disponibles
  public function actionSampleTypeByAgents($request_id, $process_id)
  {
    $data = [];
    $message_resp = null;
    $status_resp = 200;
    try {
      if (Yii::$app->request->isPost) {
        $agente_ids = Yii::$app->request->post();
        sort($agente_ids);
        $_agents = implode(',', $agente_ids);
        $data = Yii::$app->db->createCommand(
          "SELECT DISTINCT
              tab.status_id,
              tab.status_name,
              tab.sample_type_id,
              tab.sample_type_name,
              tab.request_process_essay_id,
              tab.is_active
          FROM  (

                SELECT  t2.crop_id,
                        t1.request_type_id,
                        t3.composite_sample_type_id AS sample_type_id,
                        p1.short_name AS sample_type_name,
                        t1.request_id   AS request_id,
                        t1.request_code AS request_code,
                        p2.short_name as status_name,
                        t3.request_process_essay_status_id as status_id,
                        CASE
                          WHEN t3.status = :_status_active THEN true
                          ELSE false
                        END           AS is_active,
                        t3.request_process_essay_id,
                        String_agg(cast(t4.agent_id as text), ',' order by t4.agent_id) AS agents
                FROM   plims.bsns_request t1
                      INNER JOIN plims.bsns_request_process t2
                              ON t1.request_id = t2.request_id
                                  AND t1.request_id = :_request_id
                                  AND t2.num_order_id = :_process
                      INNER JOIN plims.bsns_request_process_essay t3
                              ON t2.request_id = t3.request_id
                                  AND t2.num_order_id = t3.num_order_id
                        
                      INNER JOIN plims.bsns_request_process_essay_agent t4
                              ON t3.request_process_essay_id = t4.request_process_essay_id
                      INNER JOIN plims.bsns_parameter p1
                              ON t3.composite_sample_type_id = p1.parameter_id
                      INNER JOIN plims.bsns_parameter p2
                              ON t3.request_process_essay_status_id = p2.parameter_id
                WHERE  t1.status = :_status_active
                      AND t3.request_process_essay_status_id <> :_rpe_finished
                      AND t3.essay_id = :_assay_elisa
                      
                GROUP  BY t2.crop_id,
                          t1.request_type_id,
                          t3.composite_sample_type_id,
                          p1.short_name,
                          t1.request_id,
                          t1.request_code,
                          p2.short_name,
                          t3.request_process_essay_status_id,
                          t3.status,
                          t3.request_process_essay_id 

              ) AS tab
          INNER JOIN plims.bsns_request_process_essay_activity_sample t5
                ON tab.request_process_essay_id = t5.request_process_essay_id    
          WHERE  agents = :_agents 
          -- and t5.available
          ORDER  BY 1 desc;",
          [
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_request_id' => $request_id,
            ':_process' => $process_id,
            ':_agents' => $_agents,
            ':_rpe_finished' => Functional::REQUEST_PROCESS_ESSAY_FINISHED,
            ':_assay_elisa' => Functional::ESSAY_ELISA,
          ]
        )->queryAll();
      }
    } catch (\Throwable $th) {
      $message_resp = $th->getMessage();
      $status_resp = 400;
    }
    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionRequestProcessAssayByAgents($event_id)
  {
    $data = [];
    $message_resp = null;
    $status_resp = 200;
    try {
      if (Yii::$app->request->isPost) {
        $agente_ids = Yii::$app->request->post();
        sort($agente_ids);
        $_agents = implode(',', $agente_ids);
        array_walk($agente_ids, function (&$item) {
          $item  =  $item . ' IN (t1.agent_id)';
        });
        $agente_condition =  implode(' OR ', $agente_ids);
        $query_open = "SELECT tab.request_id, 
                              t3.request_code,
                              tab.sample_type_id, 
                              p1.long_name as sample_type_name,
                              tab.request_process_essay_id,
                              tab.process_id,
                              t4.code, 
                              tab.is_active,
                              tab.agents
                      FROM   (
                          SELECT DISTINCT t1.request_id,
                                          t1.composite_sample_type_id AS sample_type_id,
                                          t1.request_process_essay_id,
                                          t1.num_order_id AS process_id ,
                                          CASE
                                                          WHEN t1.status = :_status_active THEN true
                                                          ELSE false
                                          END                                                             AS is_active,
                                          string_agg(Cast(t1.agent_id AS TEXT), ',' order BY t1.agent_id) AS agents
                          FROM            plims.bsns_request_process_essay_agent t1
                          INNER JOIN      plims.bsns_multi_request_event t2
                          ON              t1.request_id = t2.request_id
                          AND             t2.support_master_id = :_event_id
                          WHERE           t1.composite_sample_type_id <> :_cst_header
                          AND             (";

        $query_close = ")
                        GROUP BY    t1.request_id,
                                    t1.composite_sample_type_id,
                                    t1.request_process_essay_id ,
                                    t1.num_order_id,
                                    t1.status) tab 
                        INNER JOIN plims.bsns_request t3
                            ON tab.request_id = t3.request_id
                        INNER JOIN plims.bsns_parameter p1
                            ON tab.sample_type_id = p1.parameter_id
                        INNER JOIN plims.bsns_request_process_essay t4
                            ON tab.request_process_essay_id = t4.request_process_essay_id
                              AND t4.request_process_essay_status_id <> :_rpa_finished
                        WHERE  tab.agents = :_agents;";
        $query = $query_open . $agente_condition . $query_close;
        $data = Yii::$app->db->createCommand(
          $query,
          [
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_event_id' => $event_id,
            ':_cst_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
            ':_agents' => $_agents,
            ':_rpa_finished' => Functional::REQUEST_PROCESS_ESSAY_STATUS_FINISHED,
          ]
        )->queryAll();
      }
    } catch (\Throwable $th) {
      $message_resp = $th->getMessage();
      $status_resp = 400;
    }
    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  // sample material disponibles
  public function actionAssaySampleMaterial(
    $request_process_essay_id
  ) {

    $data = null;
    $message_resp = null;
    $status_resp = 200;
    try {
      if (Yii::$app->request->isGet) {
        $data = Yii::$app->db->createCommand(
          "SELECT DISTINCT t1.composite_sample_id,
                    c1.short_name AS composite_sample_name,
                    CASE
                      WHEN c1.reading_data_type_id = :_rd_entry THEN t1.num_order
                      ELSE NULL
                    END           AS num_order
          FROM   plims.bsns_request_process_essay_activity_sample t1
          LEFT JOIN plims.bsns_composite_sample c1
                  ON t1.composite_sample_id = c1.composite_sample_id
          WHERE  t1.request_process_essay_id = :_request_process_essay_id
          -- AND t1.available = FALSE
          AND c1.reading_data_type_id = :_rd_entry
          ORDER  BY num_order;",
          [
            ':_rd_entry' => Functional::READING_DATA_ENTRY,
            ':_request_process_essay_id' => $request_process_essay_id,
          ]
        )->queryAll();

        // $data = Yii::$app->db->createCommand(
        //   "SELECT DISTINCT t1.composite_sample_id,
        //             c1.short_name AS composite_sample_name,
        //             CASE
        //               WHEN c1.reading_data_type_id = :_rd_entry THEN t1.num_order
        //               ELSE NULL
        //             END           AS num_order
        //   FROM   plims.bsns_request_process_essay_activity_sample t1
        //   LEFT JOIN plims.bsns_composite_sample c1
        //           ON t1.composite_sample_id = c1.composite_sample_id
        //   WHERE  t1.request_process_essay_id = :_request_process_essay_id
        //   AND c1.reading_data_type_id = :_rd_entry
        //   ORDER  BY num_order;",
        //   [
        //     ':_rd_entry' => Functional::READING_DATA_ENTRY,
        //     ':_request_process_essay_id' => $request_process_essay_id,
        //   ]
        // )->queryAll();
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionAssaySampleMaterialDev(
    $request_process_essay_id,
    $agent_id
  ) {

    $data = null;
    $message_resp = null;
    $status_resp = 200;
    try {
      if (Yii::$app->request->isGet) {
        $data = Yii::$app->db->createCommand(
          "SELECT DISTINCT t1.composite_sample_id,
                    c1.short_name AS composite_sample_name,
                    CASE
                      WHEN c1.reading_data_type_id = :_rd_entry THEN t1.num_order
                      ELSE NULL
                    end           AS num_order
          FROM   plims.bsns_request_process_essay_activity_sample t1
          LEFT JOIN plims.bsns_composite_sample c1
                  ON t1.composite_sample_id = c1.composite_sample_id
          WHERE  t1.request_process_essay_id = :_rpa_id
          AND c1.reading_data_type_id = :_rd_entry
          AND t1.composite_sample_id NOT IN (SELECT t2.composite_sample_id
                                              FROM   plims.bsns_agent_sample_reviewer t2
                                              WHERE  t2.request_process_essay_id = :_rpa_id
                                                    AND t2.agent_id = :_agent_id
                                                    AND t2.is_distributed)
          ORDER  BY num_order;",
          [
            ':_rd_entry' => Functional::READING_DATA_ENTRY,
            ':_rpa_id' => $request_process_essay_id,
            ':_agent_id' => $agent_id,
          ]
        )->queryAll();
      } elseif (Yii::$app->request->isPost) {

        $agent_list = Yii::$app->request->post();

        $query1 = "SELECT DISTINCT t1.composite_sample_id,
                          c1.short_name AS composite_sample_name,
                          CASE
                            WHEN c1.reading_data_type_id = :_rd_entry THEN t1.num_order
                            ELSE NULL
                          end           AS num_order
                  FROM   plims.bsns_request_process_essay_activity_sample t1
                  LEFT JOIN plims.bsns_composite_sample c1
                        ON t1.composite_sample_id = c1.composite_sample_id
                  WHERE  t1.request_process_essay_id = :_rpa_id
                  AND c1.reading_data_type_id = :_rd_entry
                  AND t1.composite_sample_id NOT IN
                    (SELECT DISTINCT t2.composite_sample_id
                      FROM   plims.bsns_agent_sample_reviewer t2
                      WHERE  t2.request_process_essay_id = :_rpa_id
                            AND t2.agent_id IN ( ";

        $query2 = implode(',', $agent_list);

        $query3 = " )
        AND t2.is_distributed)
        ORDER  BY num_order; ";

        $query = $query1 . $query2 . $query3;

        $data = Yii::$app->db->createCommand(
          $query,
          [
            ':_rd_entry' => Functional::READING_DATA_ENTRY,
            ':_rpa_id' => $request_process_essay_id,
          ]
        )->queryAll();
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionAssaySample()
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;
    try {
      if (Yii::$app->request->isPost) {

        $request_list = Yii::$app->request->post();

        foreach ($request_list as $key1 => $request_id) {

          $request_process_essay_list = Yii::$app->db->createCommand(
            "SELECT request_process_essay_id 
            FROM   plims.bsns_request_process_essay
            WHERE  request_id = :_request_id
                   AND composite_sample_type_id <> :_composite_sample_type_header
                   AND status = :_status_disabled
                   AND essay_id = :_assay_elisa; ",
            [
              ':_request_id' => $request_id,
              ':_composite_sample_type_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
              ':_status_disabled' => Functional::STATUS_DISABLED,
              ':_assay_elisa' => Functional::ESSAY_ELISA
            ]
          )->queryColumn();

          foreach ($request_process_essay_list as $key2 => $request_process_essay_id) {

            $model1 = RequestProcessEssay::findOne($request_process_essay_id);
            if ($model1->request_process_essay_status_id == Functional::REQUEST_PROCESS_ESSAY_PENDING) {

              $this->setActiveStatus($request_process_essay_id);
              $message_resp = $message_resp . 'active and ';
              $activity_qty = 2; // En el caso de ELISA
              if ($activity_qty > 0) {
                $model1->activity_qty = $activity_qty;
                if ($model1->save()) {
                  $message_resp = $message_resp . 'update activity qty. in RPA' . "\r\n";
                  $this->setRegulateActivities($request_process_essay_id, $activity_qty);
                  $data[] = [
                    'request_process_essay_id' => $request_process_essay_id,
                    'activity_qty' => $model1->activity_qty,
                    'type_id' =>  $model1->requestProcessEssayType->parameter_id,
                    'type_name' => $model1->requestProcessEssayType->short_name,
                    'status_id' => $model1->requestProcessEssayStatus->parameter_id,
                    'status' => $model1->requestProcessEssayStatus->short_name,
                    'start_date' => $model1->start_date,
                    'end_date' => $model1->finish_date,
                    'is_active' => $model1->status === Functional::STATUS_ACTIVE ? true : false,
                    'sample_type_name' => $model1->compositeSampleType->short_name,
                  ];
                }
              }
            }
          }
          //
        }
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  // ---------------------------------------------

  private function getUserId()
  {
    $user_id =  null;
    $_token = Yii::$app->request->headers->get('authorization');
    if (!is_null($_token)) {
      $token = str_replace('Bearer ', '', $_token);
      $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
    } else {
      $user_id = \Yii::$app->user->identity->id;
    }
    return $user_id;
  }

  public function actionAgents()
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    if (Yii::$app->request->isGet) {
      $data = Yii::$app->db->createCommand(
        "SELECT DISTINCT agent_id,
              agent_name
        FROM   (SELECT a1.agent_id,
            a2.short_name AS agent_name
        FROM   plims.bsns_agent_condition a1
            INNER JOIN plims.bsns_agent a2
                    ON a1.agent_id = a2.agent_id
                        AND a1.essay_id = :_assay_elisa
        ORDER  BY a2.short_name) tab; ",
        [
          ':_assay_elisa' => Functional::ESSAY_ELISA
        ]
      )->queryAll();

      // $data_provider1 = AgentCondition::find()
      //   ->where(
      //     [
      //       'essay_id' => Functional::ESSAY_ELISA,
      //     ]
      //   )
      //   ->orderBy(['num_order' => SORT_ASC,])
      //   ->all();

      // foreach ($data_provider1 as $key => $value) {
      //   $data_provider12[] = [
      //     'agent_id' => $value->agent_id,
      //     'agent_name' => $value->agent->short_name,
      //   ];
      // }
    }

    // $data = $this->getUniqueMultiDimArray($data_provider12, 'agent_id');

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }
}

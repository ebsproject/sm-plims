<?php

namespace frontend\modules\api\controllers;

use yii\rest\ActiveController;
use bizley\jwt\JwtHttpBearerAuth;

use Yii;
use frontend\models\Script;
use frontend\modules\functional\Functional;


class PrintoutController extends ActiveController
{
  // public function behaviors()
  // {
  //   $behaviors = parent::behaviors();
  //   $behaviors['authenticator'] = [
  //     'class' =>  JwtHttpBearerAuth::class,
  //     'except' => [],

  //   ];
  //   $behaviors['JWTBearerAuth'] = [
  //     'class' =>  JwtHttpBearerAuth::class,
  //   ];
  //   return $behaviors;
  // }

  public $modelClass = Request::class;


  //INI CODE



  public function actionRequestProcessAssaySamples($request_process_assay_id)
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {
      if (Yii::$app->request->isGet) {

        // ini code
        $query = "SELECT DISTINCT t1.composite_sample_id as modified_id,
        c1.short_name               AS composite_sample_name,
        t1.num_order                AS num_order,
        c1.composite_sample_type_id AS sample_type_id,
        p1.short_name               AS sample_type_name,
        t1.num_order_id             AS process,
        t1.essay_id                 AS assay_id,
        e1.short_name               AS assay_name,
        CASE
          WHEN t1.num_order_id > 1 THEN t2.request_code || ' (R)'
          ELSE t2.request_code
        END                         AS request_code
        FROM   plims.bsns_request_process_essay_activity_sample t1
        LEFT JOIN plims.bsns_composite_sample c1
        ON t1.composite_sample_id = c1.composite_sample_id
        LEFT JOIN plims.bsns_parameter p1
        ON c1.composite_sample_type_id = p1.parameter_id
        LEFT JOIN plims.bsns_essay e1
        ON e1.essay_id = t1.essay_id
        LEFT JOIN plims.bsns_request t2
        ON t1.request_id = t2.request_id
        WHERE  t1.request_process_essay_id = :_request_process_essay_id
        AND c1.reading_data_type_id = :_rd_entry
        ORDER  BY num_order; ";
        $params = [
          ':_rd_entry' => Functional::READING_DATA_ENTRY,
          ':_request_process_essay_id' => $request_process_assay_id,
        ];
        $data = Yii::$app->db->createCommand($query, $params)->queryAll();




        // $data = Yii::$app->db->createCommand(
        //   "SELECT * FROM TABLE WHERE ID = :id",
        //   [
        //     ':id' => $id
        //   ]
        // )->queryAll();


        // // VARIABLE
        // $data = Yii::$app->db->createCommand(
        //   "SELECT column FROM TABLE WHERE ID = :id",
        //   [
        //     ':id' => $id,
        //   ]
        // )->queryScalar();


        // // ONE ROW
        // $data = Yii::$app->db->createCommand(
        //   "SELECT * FROM TABLE WHERE ID = :id",
        //   [
        //     ':id' => $id
        //   ]
        // )->queryOne();

        // // ONE COLUMNS
        // $data = Yii::$app->db->createCommand(
        //   "SELECT column FROM TABLE WHERE ID = :id",
        //   [
        //     ':id' => $id
        //   ]
        // )->queryColumn();

        // Yii::$app->db->createCommand(
        //   "UPDATE TABLE t
        //   SET    value = :value
        //   WHERE  id = :id;",
        //   [
        //     ':id' => $id,
        //   ]
        // )->execute();







        // end code
      } else if (Yii::$app->request->isPost) {
        // code in POST
      } else if (Yii::$app->request->isPut) {
        // code in PUT
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }





  public function actionReportFolbTest()
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {
      $data = [
        "Semilla con carbon" => 'negativo',
        "Insectos" => 'negativo',
        "Paja" => 'negativo',
        "Semilla de maleza" => 'negativo',
        "Arena" => 'negativo',
        "Claviceptus purpurea" => 'positivo',
      ];
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  public function actionFunct2()
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {
      //code...
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  public function actionFunct3()
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {
      //code...
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }











  // END CODE
}

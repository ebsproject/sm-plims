<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\rest\ActiveController;
use frontend\modules\functional\models\Request;
use frontend\modules\functional\models\RequestProcessEssay;
use bizley\jwt\JwtHttpBearerAuth;
use frontend\models\Script;
use yii\helpers\ArrayHelper;
use frontend\modules\functional\Functional;

class RequestController extends ActiveController
{
  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
      'class' =>  JwtHttpBearerAuth::class,
      'except' => [],

    ];
    $behaviors['JWTBearerAuth'] = [
      'class' =>  JwtHttpBearerAuth::class,
    ];
    return $behaviors;
  }

  public $modelClass = Request::class;

  public function actionRequests(
    $laboratory_id
  ) {

    $data = [];
    $message_resp = null;
    $status_resp = 200;
    try {
      if (Yii::$app->request->isGet) {
        $data = Yii::$app->db->createCommand(
          "SELECT 
            DISTINCT Cast(To_char(( a.registered_at ) :: timestamp, 'yyyymmddhhmiss') AS BIGINT) AS index,
            a.request_id,
            a.request_code,
            b.crop_id,
            d.short_name AS crop_name,
            a.request_type_id AS distribution_id,
            p1.short_name AS distribution_name,
            a.registered_at AS registered_date,
            b.composite_sample_group_id AS sample_group_id,
            a.request_status_id AS status_id,
            p2.short_name AS status_name,
            a.laboratory_id
          FROM   plims.bsns_request a
          inner join plims.bsns_request_process b
              ON a.request_id = b.request_id
                AND a.status = :status_active
          inner join plims.bsns_request_process_essay c
              ON a.request_id = c.request_id 
                AND b.num_order_id = c.num_order_id
          inner join plims.bsns_essay e
              ON e.essay_id = c.essay_id
          inner join plims.bsns_crop d
              ON b.crop_id = d.crop_id
          inner join plims.bsns_parameter p1
              ON a.request_type_id = p1.parameter_id
          inner join plims.bsns_parameter p2
              ON a.request_status_id = p2.parameter_id
          WHERE e.essay_type_id = :essay_type_id
            AND a.laboratory_id = :laboratory_id
          ORDER  BY 1 DESC; ",
          [
            ':status_active' => Functional::STATUS_ACTIVE,
            ':essay_type_id' => Functional::ESSAY_TYPE_MOB_APP,
            ':laboratory_id' => $laboratory_id
          ]
        )->queryAll();
      }
    } catch (\Throwable $th) {
      $message_resp = $th->getMessage();
      $status_resp = 400;
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionRequest(
    $request_id
  ) {

    $data = [];
    $message_resp = null;
    $status_resp = 200;
    try {
      if (Yii::$app->request->isGet) {
        $data = Yii::$app->db->createCommand(
          "SELECT 
            DISTINCT Cast(To_char(( a.registered_at ) :: timestamp, 'yyyymmddhhmiss') AS BIGINT) AS index,
            a.request_id,
            a.request_code,
            b.crop_id,
            d.short_name AS crop_name,
            a.request_type_id AS distribution_id,
            p1.short_name AS distribution_name,
            a.registered_at AS registered_date,
            b.composite_sample_group_id AS sample_group_id,
            a.request_status_id AS status_id,
            p2.short_name AS status_name,
            a.laboratory_id
          FROM   plims.bsns_request a
          inner join plims.bsns_request_process b
              ON a.request_id = b.request_id
                AND a.status = :status_active
          inner join plims.bsns_request_process_essay c
              ON a.request_id = c.request_id 
                AND b.num_order_id = c.num_order_id
          inner join plims.bsns_essay e
              ON e.essay_id = c.essay_id
          inner join plims.bsns_crop d
              ON b.crop_id = d.crop_id
          inner join plims.bsns_parameter p1
              ON a.request_type_id = p1.parameter_id
          inner join plims.bsns_parameter p2
              ON a.request_status_id = p2.parameter_id
          WHERE e.essay_type_id = :essay_type_id
            AND a.request_id = :request_id
          ORDER  BY 1 DESC; ",
          [
            ':status_active' => Functional::STATUS_ACTIVE,
            ':request_id' => $request_id,
            ':essay_type_id' => Functional::ESSAY_TYPE_MOB_APP,
          ]
        )->queryAll();

        $data = $data[0];
      }
    } catch (\Throwable $th) {
      $message_resp = $th->getMessage();
      $status_resp = 400;
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionAssays(
    $request_id,
    $process_id = 0
  ) {

    $data = null;
    $message_resp = null;
    $status_resp = 200;

    if (Yii::$app->request->isGet) {

      $data = Yii::$app->db->createCommand(
        "SELECT a.request_id,
          a.num_order_id                    AS process_id,
          a.code,
          a.essay_id                        AS assay_id,
          b.short_name                      AS assay_name,
          p1.short_name                     AS sample_name,
          c.short_name                      AS crop_name,
          a.request_process_essay_status_id AS status_id,
          p2.short_name                     AS status,
          a.start_date,
          a.finish_date,
          CASE
            WHEN a.status = :status_active THEN true
            ELSE false
          END                               AS is_active
        FROM   plims.bsns_request_process_essay a
          INNER JOIN plims.bsns_essay b
                  ON a.essay_id = b.essay_id
                    AND a.request_id = :request_id
                    AND b.essay_type_id = :essay_type_id
                    AND a.composite_sample_type_id = :composite_sample_type_id
                    AND (a.num_order_id = :process_id OR :process_id = 0)
          INNER JOIN plims.bsns_parameter p1
                  ON a.composite_sample_type_id = p1.parameter_id
          INNER JOIN plims.bsns_crop c
                  ON a.crop_id = c.crop_id
          INNER JOIN plims.bsns_parameter p2
                  ON a.request_process_essay_status_id = p2.parameter_id
          ORDER BY a.registered_at DESC;",
        [
          ':status_active' => Functional::STATUS_ACTIVE,
          ':request_id' => $request_id,
          ':essay_type_id' => Functional::ESSAY_TYPE_MOB_APP,
          ':composite_sample_type_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
          ':process_id' => $process_id,
        ]
      )->queryAll();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  public function actionAssaySampleType(
    $request_id,
    $process_id = 0,
    $assay_id
  ) {

    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {

      if (Yii::$app->request->isGet) {

        $data_provider1 = Yii::$app->db->createCommand(
          "SELECT t1.request_process_essay_status_id AS status_id,
                p1.short_name                      AS status_name,
                t1.composite_sample_type_id        AS sample_type_id,
                p3.short_name                      AS sample_type_name,
                t1.request_process_essay_id, 
                t1.num_order_id as process_id,
                t1.code,
                CASE
                  WHEN t1.status = :status_active THEN true
                  ELSE false
                END                                AS is_active,
                t1.activity_qty,
                NULL                               AS activity_list,
                t1.request_process_essay_type_id   AS type_id,
                p2.short_name                      AS type_name,
                t1.start_date,
                t1.start_by,
                t1.finish_date,
                t1.finish_by
               
        FROM   plims.bsns_request_process_essay t1
                INNER JOIN plims.bsns_parameter p1
                        ON t1.request_process_essay_status_id = p1.parameter_id
                LEFT JOIN plims.bsns_parameter p2
                        ON t1.request_process_essay_type_id = p2.parameter_id
                INNER JOIN plims.bsns_parameter p3
                        ON t1.composite_sample_type_id = p3.parameter_id
        WHERE  t1.request_id = :request_id
                AND (t1.num_order_id = :num_order_id OR :num_order_id = 0)
                AND t1.essay_id = :assay_id
                --AND t1.request_process_essay_status_id <> :REQUEST_PROCESS_ESSAY_FINISHED
                AND t1.composite_sample_type_id <> :composite_sample_type_header
        ORDER  BY p3.code;",
          [
            ':status_active' => Functional::STATUS_ACTIVE,
            ':request_id' => $request_id,
            ':num_order_id' => $process_id,
            ':assay_id' => $assay_id,
            // ':REQUEST_PROCESS_ESSAY_FINISHED' => Functional::REQUEST_PROCESS_ESSAY_FINISHED,
            ':composite_sample_type_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
          ]
        )->queryAll();

        $data = array_map(
          function ($value) {

            $request_process_essay_id = $value['request_process_essay_id'];
            $activity_qty = $value['activity_qty'];
            $activity_list = [];

            if ($activity_qty >= 1) {
              $activity_list = Yii::$app->db->createCommand(
                "SELECT t2.num_order                         AS activity_order,
                        t2.activity_id                       AS activity_id,
                        t3.short_name                        AS activity_name,
                        t2.request_process_essay_activity_id AS request_process_essay_activity_id,
                        CASE
                              WHEN t2.status = :status_active THEN true
                              ELSE false
                        END  as is_active
                FROM   plims.bsns_request_process_essay t1
                        INNER JOIN plims.bsns_request_process_essay_activity t2
                                ON t1.request_process_essay_id = t2.request_process_essay_id
                        INNER JOIN plims.bsns_activity t3
                                ON t3.activity_id = t2.activity_id
                                  AND t3.activity_property_id = :activity_property_set
                WHERE t1.request_process_essay_id = :request_process_essay_id                            
                ORDER  BY t2.num_order
                LIMIT  :activity_qty;",
                [
                  ':status_active' => Functional::STATUS_ACTIVE,
                  ':request_process_essay_id' => $request_process_essay_id,
                  ':activity_property_set' => Functional::ACTIVITY_PROPERTY_SET,
                  'activity_qty' => $activity_qty ? $activity_qty : 0,
                ]
              )->queryAll();
            }
            $value['activity_list'] = $activity_list;
            return $value;
          },
          $data_provider1
        );
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }


    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  public function actionAssaySample(
    $request_process_essay_id
  ) {

    $data = null;
    $message_resp = null;
    $status_resp = 200;

    if (Yii::$app->request->isGet) {

      $data = Yii::$app->db->createCommand(
        "SELECT t1.request_process_essay_status_id AS status_id,
                p1.short_name                      AS status_name,
                t1.composite_sample_type_id        AS sample_type_id,
                p3.short_name                      AS sample_type_name,
                t1.request_process_essay_id        AS request_process_essay_id,
                t1.num_order_id                    AS process_id,
                t1.code,
                CASE
                  WHEN t1.status = :status_active THEN true
                  ELSE false
                END                                AS is_active,
                t1.activity_qty                    AS activity_qty,
                NULL                               AS activity_list,
                t1.request_process_essay_type_id   AS type_id,
                p2.short_name                      AS type_name,
                t1.start_date,
                t1.start_by,
                t1.finish_date,
                t1.finish_by
        FROM   plims.bsns_request_process_essay t1
                INNER JOIN plims.bsns_parameter p1
                        ON t1.request_process_essay_status_id = p1.parameter_id
                LEFT JOIN plims.bsns_parameter p2
                      ON t1.request_process_essay_type_id = p2.parameter_id
                INNER JOIN plims.bsns_parameter p3
                        ON t1.composite_sample_type_id = p3.parameter_id
        WHERE  t1.request_process_essay_id = :request_process_essay_id;",
        [
          ':status_active' => Functional::STATUS_ACTIVE,
          ':request_process_essay_id' => $request_process_essay_id
        ]
      )->queryOne();

      $activity_qty =  $data['activity_qty'];

      $activity_list = Yii::$app->db->createCommand(
        "SELECT t2.num_order                         AS activity_order,
                  t2.activity_id                       AS activity_id,
                  t3.short_name                        AS activity_name,
                  t2.request_process_essay_activity_id AS request_process_essay_activity_id,
                  CASE
                        WHEN t2.status = :status_active THEN true
                        ELSE false
                  END  as is_active
          FROM   plims.bsns_request_process_essay t1
                  INNER JOIN plims.bsns_request_process_essay_activity t2
                          ON t1.request_process_essay_id = t2.request_process_essay_id
                  INNER JOIN plims.bsns_activity t3
                          ON t3.activity_id = t2.activity_id
                            AND t3.activity_property_id = :activity_property_set
          WHERE t1.request_process_essay_id = :request_process_essay_id                            
          ORDER  BY t2.num_order
          LIMIT  :activity_qty;",
        [
          ':status_active' => Functional::STATUS_ACTIVE,
          ':request_process_essay_id' => $request_process_essay_id,
          ':activity_property_set' => Functional::ACTIVITY_PROPERTY_SET,
          'activity_qty' => $activity_qty ? $activity_qty : 0,
        ]
      )->queryAll();

      $data['activity_list'] = $activity_list;
    } else if (Yii::$app->request->isPut) {

      $model1 = RequestProcessEssay::findOne($request_process_essay_id);

      if ($model1->request_process_essay_status_id == Functional::REQUEST_PROCESS_ESSAY_PENDING) {
        $request_process_essay_id = $model1->request_process_essay_id;
        $activity_qty = Yii::$app->request->post('activity_qty');
        $request_process_essay_type_id = Yii::$app->request->post('type_id');
        $assay_id = $model1->essay_id;

        switch ($assay_id) {
            // case Functional::ESSAY_ELISA:

            //   $this->setActiveStatus($request_process_essay_id);
            //   $message_resp = $message_resp . 'active and ';
            //   $activity_qty = 2; // En el caso de ELISA

            //   if ($activity_qty > 0) {
            //     $model1->activity_qty = $activity_qty;
            //     if ($model1->save()) {
            //       $message_resp = $message_resp . 'update activity qty. in RPA';
            //       $this->setRegulateActivities($request_process_essay_id, $activity_qty);
            //       $data = [
            //         'activity_qty' => $model1->activity_qty,
            //         'type_id' =>  $model1->requestProcessEssayType->parameter_id,
            //         'type_name' => $model1->requestProcessEssayType->short_name,
            //         'status_id' => $model1->requestProcessEssayStatus->parameter_id,
            //         'status' => $model1->requestProcessEssayStatus->short_name,
            //         'start_date' => $model1->start_date,
            //         'end_date' => $model1->finish_date,
            //         'is_active' => $model1->status === Functional::STATUS_ACTIVE ? true : false,
            //       ];
            //     }
            //   }

            //   break;
          case Functional::ESSAY_BLOTTER:
            $this->setActiveStatus($request_process_essay_id);
            if ($activity_qty > 0) {
              $model1->activity_qty = $activity_qty;
              if ($model1->save()) {
                $this->setRegulateActivities($request_process_essay_id, $activity_qty);
                $data = [
                  'activity_qty' => $model1->activity_qty,
                  'type_id' =>  $model1->requestProcessEssayType->parameter_id,
                  'type_name' => $model1->requestProcessEssayType->short_name,
                  'status_id' => $model1->requestProcessEssayStatus->parameter_id,
                  'status' => $model1->requestProcessEssayStatus->short_name,
                  'start_date' => $model1->start_date,
                  'end_date' => $model1->finish_date,
                  'is_active' => $model1->status === Functional::STATUS_ACTIVE ? true : false,
                ];
              }
            }
            break;
          case Functional::ESSAY_GERMINATION:
            $this->setActiveStatus($request_process_essay_id);
            $model1->request_process_essay_type_id = ($request_process_essay_type_id ? $request_process_essay_type_id : Functional::PARAMETER_UNDETERMINED);

            if ($activity_qty > 0) {
              $model1->activity_qty = $activity_qty;
              if ($model1->save()) {
                $this->setRegulateActivities($request_process_essay_id, $activity_qty);
                $data = [
                  'activity_qty' => $model1->activity_qty,
                  'type_id' =>  $model1->requestProcessEssayType->parameter_id,
                  'type_name' => $model1->requestProcessEssayType->short_name,
                  'status_id' => $model1->requestProcessEssayStatus->parameter_id,
                  'status' => $model1->requestProcessEssayStatus->short_name,
                  'start_date' => $model1->start_date,
                  'end_date' => $model1->finish_date,
                  'is_active' => $model1->status === Functional::STATUS_ACTIVE ? true : false,
                ];
              }
            }

            break;
          case Functional::ESSAY_PCR:

            $this->setActiveStatus($request_process_essay_id);
            $message_resp = $message_resp . 'active and ';
            $activity_qty = 1; // En el caso de PCR

            if ($activity_qty > 0) {
              $model1->activity_qty = $activity_qty;
              if ($model1->save()) {
                $message_resp = $message_resp . 'update activity qty. in RPA';
                $this->setRegulateActivities($request_process_essay_id, $activity_qty);
                $data = [
                  'activity_qty' => $model1->activity_qty,
                  'type_id' =>  $model1->requestProcessEssayType->parameter_id,
                  'type_name' => $model1->requestProcessEssayType->short_name,
                  'status_id' => $model1->requestProcessEssayStatus->parameter_id,
                  'status' => $model1->requestProcessEssayStatus->short_name,
                  'start_date' => $model1->start_date,
                  'end_date' => $model1->finish_date,
                  'is_active' => $model1->status === Functional::STATUS_ACTIVE ? true : false,
                ];
              }
            }

            break;
          default:
            break;
        }
      } else {
        $message_resp = 'This process has already started';
      }
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  private function getStepDataProviders(
    $request_process_essay_id,
    $assay_id
  ) {

    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {
      $sub_process_status =  $this->getSubProcessStatus($request_process_essay_id);
      //  AVANCE DE MUESTRAS PREPARACION Y EVALUACION
      $data_provider_preparation = Yii::$app->db->createCommand(
        "SELECT     t2.num_order,
                Count(1) ,
                Count(t4.num_order) filter (WHERE t4.start_by IS NOT NULL) AS init_rpeas,
                min (t4.start_date )                                       AS start_date,
                CASE
                          WHEN max (t4.finish_date) IS NULL THEN max (t4.registered_at )
                          ELSE max (t4.finish_date)
                END AS finish_date
        FROM       plims.bsns_request_process_essay_activity t2
        INNER JOIN plims.bsns_activity t3
        ON         t2.activity_id = t3.activity_id
        AND        t3.activity_property_id = :_act_prop_set
        INNER JOIN plims.bsns_request_process_essay_activity_sample t4
        ON         t2.request_process_essay_id = :request_process_essay_id
        AND        t2.request_process_essay_activity_id = t4.request_process_essay_activity_id
        WHERE      t2.status = :status_active
        GROUP BY   t2.num_order
        ORDER BY   t2.num_order;",
        [
          ':_act_prop_set' => Functional::ACTIVITY_PROPERTY_SET,
          ':request_process_essay_id' => $request_process_essay_id,
          ':status_active' =>  Functional::STATUS_ACTIVE
        ]
      )->queryAll();

      switch (true) {
        case ($assay_id == Functional::ESSAY_BLOTTER):
          // consult data provider
          $data_provider_evaluation = Yii::$app->db->createCommand(
            'SELECT tab.num_order,
                    Count(1),
                    Min(tab.start_date)  AS start_date,
                    Max(tab.finish_date) AS finish_date,
                    Sum(tab.readings)    AS finish
            FROM   (SELECT t2.num_order,
                            t3.composite_sample_id,
                            Min(r1.registered_at) AS start_date,
                            Max(r1.registered_at) AS finish_date,
                            CASE
                              WHEN Count(r1) > 0 THEN 1
                              ELSE 0
                            END                   AS "readings"
                    FROM   plims.bsns_request_process_essay t1
                            INNER JOIN plims.bsns_request_process_essay_activity t2
                                    ON t1.request_process_essay_id =
                                      t2.request_process_essay_id
                            INNER JOIN plims.bsns_request_process_essay_activity_sample t3
                                    ON t2.request_process_essay_activity_id =
                                      t3.request_process_essay_activity_id
                                      AND t3.available
                            INNER JOIN plims.bsns_activity t5
                                    ON t5.activity_id = t2.activity_id
                                      AND t5.activity_property_id = :activity_property_set
                            INNER JOIN plims.bsns_parameter p1
                                    ON p1.parameter_id = t1.request_process_essay_status_id
                            INNER JOIN plims.bsns_parameter p2
                                    ON p2.parameter_id =
                                      t2.request_process_essay_activity_status_id
                            INNER JOIN plims.bsns_parameter p3
                                    ON p3.parameter_id =
                                      t3.request_process_essay_activity_sample_status_id
                            LEFT JOIN plims.bsns_reading_data r1
                                  ON r1.request_process_essay_activity_sample_id =
                                      t3.request_process_essay_activity_sample_id
                            INNER JOIN plims.auth_user u
                                    ON u.user_id = t3.registered_by
                    WHERE  t1.request_process_essay_id = :request_process_essay_id
                            AND t2.status = :status_active
                    GROUP  BY t2.num_order,
                              t3.composite_sample_id) tab
            GROUP  BY tab.num_order
            ORDER  BY tab.num_order; ',
            [
              ':activity_property_set' => Functional::ACTIVITY_PROPERTY_SET,
              ':request_process_essay_id' => $request_process_essay_id,
              ':status_active' => Functional::STATUS_ACTIVE
            ]
          )->queryAll();
          $data_provider_users = Yii::$app->db->createCommand(
            "SELECT DISTINCT u2.username AS in_preparation,
                        u4.username AS in_evaluation
            FROM   plims.bsns_request_process_essay_activity t2
              INNER JOIN plims.bsns_request_process_essay_activity_sample t4
                      ON t2.request_process_essay_activity_id =
                          t4.request_process_essay_activity_id
                          -- AND t4.available
                          AND t2.request_process_essay_id = :request_process_essay_id
                          AND t2.status = :status_active
              LEFT JOIN plims.bsns_reading_data r1
                      ON r1.request_process_essay_id = t2.request_process_essay_id
              LEFT JOIN plims.auth_user u2
                      ON u2.user_id = t4.registered_by
              LEFT JOIN plims.auth_user u4
                      ON u4.user_id = r1.registered_by;",
            [
              ':request_process_essay_id' => $request_process_essay_id,
              ':status_active' => Functional::STATUS_ACTIVE
            ]
          )->queryAll();

          // prepare data
          $data = [
            'preparation' => [
              'acum' => 0,
              'total_act_spl' => null,
              'total_acum' => 0,
            ],
            'evaluation' => [
              'acum' => 0,
              'total_act_spl' => null,
              'total_acum' => 0,
            ]
          ];

          // provider preparation
          $data['preparation']['progress'] = [];
          foreach ($data_provider_preparation as $key => $value) {
            $data['preparation']['acum'] = $data['preparation']['acum'] + $value['init_rpeas'];
            $data['preparation']['progress'][] = $value['init_rpeas'] . '/' . $value['count'];
            $data['preparation']['total_act_spl'] =   $value['count'];
            $data['preparation']['status'] = $sub_process_status['preparation'];
            $data['preparation']['total_acum'] =  $data['preparation']['total_acum'] + $value['count'];
            $data['preparation']['start_date'][] = $value['start_date'];
            $data['preparation']['finish_date'][] = $value['finish_date'];
          }

          // provider evaluation
          $data['evaluation']['progress'] = [];
          foreach ($data_provider_evaluation as $key => $value) {
            $data['evaluation']['acum']  = $data['evaluation']['acum'] + $value['finish'];
            $data['evaluation']['progress'][] = $value['finish'] . '/' . $value['count'];
            $data['evaluation']['total_act_spl'] =   $value['count'];
            $data['evaluation']['status'] = $sub_process_status['evaluation'];
            $data['evaluation']['total_acum'] =  $data['evaluation']['total_acum'] + $value['count'];
            $data['evaluation']['start_date'][] = $value['start_date'];
            $data['evaluation']['finish_date'][] = $value['finish_date'];
          }

          // provider users
          $preparation_total_acum = $data['preparation']['total_acum'] > 0 ?   $data['preparation']['total_acum'] : 1;
          $evaluation_total_acum =  $data['evaluation']['total_acum'] > 0 ?   $data['evaluation']['total_acum'] : 1;
          $in_preparation_users = [];
          $in_evaluation_users = [];
          foreach ($data_provider_users as $key => $value) {
            $in_preparation_users[] =   $value['in_preparation'];
            $in_evaluation_users[] =   $value['in_evaluation'];
          }
          $data['preparation']['users'] = implode(', ', array_unique($in_preparation_users));
          $data['evaluation']['users'] = implode(', ', array_unique($in_evaluation_users));
          $data['preparation']['percent'] = round(100.00 * (float) ($data['preparation']['acum'] / $preparation_total_acum), 3);
          $data['evaluation']['percent'] = round(100.00 * (float) ($data['evaluation']['acum'] / $evaluation_total_acum), 3);

          break;

        case ($assay_id == Functional::ESSAY_GERMINATION):
          // consult data provider
          $data_provider_evaluation_all = Yii::$app->db->createCommand(
            'SELECT tab2.activity,
                    Count(tab2.sample),
                    Min(tab2.start_date_1)  AS start_date_1,
                    Max(tab2.finish_date_1) AS finish_date_1,
                    Sum(tab2.readings_1)    AS finish_1,
                    Min(tab2.start_date_2)  AS start_date_2,
                    Max(tab2.finish_date_2) AS finish_date_2,
                    Sum(tab2.readings_2)    AS finish_2
              FROM   (SELECT tab1.activity,
                              tab1.sample,
                              Min(tab1.start_date_1)  AS start_date_1,
                              Max(tab1.finish_date_1) AS finish_date_1,
                              Sum(tab1.readings_1)    AS readings_1,
                              Min(tab1.start_date_2)  AS start_date_2,
                              Max(tab1.finish_date_2) AS finish_date_2,
                              Sum(tab1.readings_2)    AS readings_2
                      FROM   (SELECT t2.num_order AS activity,
                                      t3.num_order AS sample,
                                      CASE
                                        WHEN ( Count(r1) > 0
                                              AND a1.agent_group_id = 147 ) THEN
                                        Min(r1.registered_at)
                                        ELSE NULL
                                      END          AS start_date_1,
                                      CASE
                                        WHEN ( Count(r1) > 0
                                              AND a1.agent_group_id = 147 ) THEN
                                        Max(r1.registered_at)
                                        ELSE NULL
                                      END          AS finish_date_1,
                                      CASE
                                        WHEN ( Count(r1) > 0
                                              AND a1.agent_group_id = 147 ) THEN 1
                                        ELSE 0
                                      END          AS readings_1,
                                      CASE
                                        WHEN ( Count(r1) > 0
                                              AND a1.agent_group_id = 148 ) THEN
                                        Min(r1.registered_at)
                                        ELSE NULL
                                      END          AS start_date_2,
                                      CASE
                                        WHEN ( Count(r1) > 0
                                              AND a1.agent_group_id = 148 ) THEN
                                        Max(r1.registered_at)
                                        ELSE NULL
                                      END          AS finish_date_2,
                                      CASE
                                        WHEN ( Count(r1) > 0
                                              AND a1.agent_group_id = 148 ) THEN 1
                                        ELSE 0
                                      END          AS readings_2
                              FROM   plims.bsns_request_process_essay t1
                                      INNER JOIN plims.bsns_request_process_essay_activity t2
                                              ON t1.request_process_essay_id =
                                                t2.request_process_essay_id
                                      INNER JOIN plims.bsns_request_process_essay_activity_sample t3
                                              ON t2.request_process_essay_activity_id =
                                                t3.request_process_essay_activity_id
                                                AND t3.available = true
                                      INNER JOIN plims.bsns_activity t5
                                              ON t5.activity_id = t2.activity_id
                                                AND t5.activity_property_id =
                                                    :activity_property_set
                                      INNER JOIN plims.bsns_parameter p1
                                              ON p1.parameter_id =
                                                t1.request_process_essay_status_id
                                      INNER JOIN plims.bsns_parameter p2
                                              ON p2.parameter_id =
                                                t2.request_process_essay_activity_status_id
                                      INNER JOIN plims.bsns_parameter p3
                                              ON p3.parameter_id =
                                                t3.request_process_essay_activity_sample_status_id
                                      LEFT JOIN plims.bsns_reading_data r1
                                            ON r1.request_process_essay_activity_sample_id =
                                                t3.request_process_essay_activity_sample_id
                                      LEFT JOIN plims.auth_user u
                                            ON u.user_id = t3.registered_by
                                      LEFT JOIN plims.bsns_agent a1
                                            ON r1.agent_id = a1.agent_id
                              WHERE  t1.request_process_essay_id = :request_process_essay_id
                                      AND t2.status = :status_active
                              GROUP  BY t2.num_order,
                                        t3.num_order,
                                        a1.agent_group_id
                              ORDER  BY t2.num_order,
                                        t3.num_order) tab1
                      GROUP  BY tab1.activity,
                                tab1.sample
                      ORDER  BY tab1.activity,
                                tab1.sample) tab2
              GROUP  BY tab2.activity;',
            [
              ':activity_property_set' => Functional::ACTIVITY_PROPERTY_SET,
              ':request_process_essay_id' => $request_process_essay_id,
              ':status_active' => Functional::STATUS_ACTIVE
            ]
          )->queryAll();

          $data_provider_users_all = Yii::$app->db->createCommand(
            "SELECT String_agg(tab.in_preparation, ',')        AS in_preparation,
                    String_agg(tab.in_evaluation, ',')         AS in_evaluation,
                    String_agg(tab.in_evaluation_symptom, ',') AS in_evaluation_symptom
            FROM   (SELECT DISTINCT u2.username AS in_preparation,
                                    CASE
                                      WHEN r1.agent_id = 147 THEN u4.username
                                      ELSE u4.username
                                    END         AS in_evaluation,
                                    CASE
                                      WHEN r1.agent_id = 148 THEN u4.username
                                      ELSE u4.username
                                    END         AS in_evaluation_symptom
                    FROM   plims.bsns_request_process_essay_activity t2
                            INNER JOIN plims.bsns_request_process_essay_activity_sample t4
                                    ON t2.request_process_essay_activity_id =
                                      t4.request_process_essay_activity_id
                                      AND t4.available = true
                            LEFT JOIN plims.bsns_reading_data r1
                                  ON r1.request_process_essay_id =
                                      t2.request_process_essay_id
                            LEFT JOIN plims.auth_user u2
                                  ON u2.user_id = t4.registered_by
                            LEFT JOIN plims.auth_user u4
                                  ON u4.user_id = r1.registered_by
                    WHERE  t2.request_process_essay_id = :request_process_essay_id
                            AND t2.status = :status_active) tab; ",
            [
              // ':activity_property_set' => Functional::ACTIVITY_PROPERTY_SET,
              ':request_process_essay_id' => $request_process_essay_id,
              ':status_active' => Functional::STATUS_ACTIVE
            ]
          )->queryOne();

          // generate data provider
          $data = [
            'preparation' => [
              'acum' => 0,
              'total_act_spl' => null,
              'total_acum' => 0,
            ],
            'evaluation' => [
              'acum' => 0,
              'total_act_spl' => null,
              'total_acum' => 0,
            ],
            'evaluation_symptom' => [
              'acum' => 0,
              'total_act_spl' => null,
              'total_acum' => 0,
            ]
          ];

          // provider preparation
          $data['preparation']['progress'] = [];
          foreach ($data_provider_preparation as $key => $value) {
            $data['preparation']['acum'] = $data['preparation']['acum'] + $value['init_rpeas'];
            $data['preparation']['progress'][] = $value['init_rpeas'] . '/' . $value['count'];
            $data['preparation']['total_act_spl'] =   $value['count'];
            $data['preparation']['status'] = $sub_process_status['preparation'];
            $data['preparation']['total_acum'] =  $data['preparation']['total_acum'] + $value['count'];
            $data['preparation']['start_date'][] = $value['start_date'];
            $data['preparation']['finish_date'][] = $value['finish_date'];
          }

          // provider evaluation 1
          $data['evaluation']['progress'] = [];
          foreach ($data_provider_evaluation_all as $key => $value) {
            $data['evaluation']['acum']  = $data['evaluation']['acum'] + $value['finish_1'];
            $data['evaluation']['progress'][] = $value['finish_1'] . '/' . $value['count'];
            $data['evaluation']['total_act_spl'] =   $value['count'];
            $data['evaluation']['status'] = $sub_process_status['evaluation'];
            $data['evaluation']['total_acum'] =  $data['evaluation']['total_acum'] + $value['count'];
            $data['evaluation']['start_date_1'][] = $value['start_date_1'];
            $data['evaluation']['finish_date_1'][] = $value['finish_date_1'];

            $data['evaluation_symptom']['acum']  = $data['evaluation_symptom']['acum'] + $value['finish_2'];
            $data['evaluation_symptom']['progress'][] = $value['finish_2'] . '/' . $value['count'];
            $data['evaluation_symptom']['total_act_spl'] =   $value['count'];
            $data['evaluation_symptom']['status'] = $sub_process_status['evaluation'];
            $data['evaluation_symptom']['total_acum'] =  $data['evaluation_symptom']['total_acum'] + $value['count'];
            $data['evaluation_symptom']['start_date_2'][] = $value['start_date_2'];
            $data['evaluation_symptom']['finish_date_2'][] = $value['finish_date_2'];
          }

          // provider users
          $preparation_total_acum = $data['preparation']['total_acum'] > 0 ?   $data['preparation']['total_acum'] : 1;
          $evaluation_total_acum =  $data['evaluation']['total_acum'] > 0 ?   $data['evaluation']['total_acum'] : 1;
          $evaluation_symtom_total_acum =  $data['evaluation_symptom']['total_acum'] > 0 ?   $data['evaluation_symptom']['total_acum'] : 1;

          $data['preparation']['users'] = $data_provider_users_all['in_preparation'];
          $data['evaluation']['users'] = $data_provider_users_all['in_evaluation'];
          $data['evaluation_symptom']['users'] = $data_provider_users_all['in_evaluation_symptom'];

          $data['preparation']['percent'] = round(100.00 * (float) ($data['preparation']['acum'] / $preparation_total_acum), 3);
          $data['evaluation']['percent'] = round(100.00 * (float) ($data['evaluation']['acum'] / $evaluation_total_acum), 3);
          $data['evaluation_symptom']['percent'] = round(100.00 * (float) ($data['evaluation_symptom']['acum'] / $evaluation_symtom_total_acum), 3);
          break;

        default:
          break;
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }
    return [
      'data' => $data,
      'message_resp' => $message_resp,
      'status_resp' => $status_resp
    ];
  }

  public function actionAssaySampleStep(
    $request_process_essay_id = null,
    $assay_id = null,
    $step = null,
    $status  = null
  ) {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    $user = Yii::$app->user->identity->id;
    $date = date("Y-m-d H:i:s", time());
    try {
      if (Yii::$app->request->isGet) {
        $get_step_data_providers = $this->getStepDataProviders($request_process_essay_id, $assay_id);
        $data = $get_step_data_providers['data'];
        $message_resp = $get_step_data_providers['message_resp'];
        $status_resp = $get_step_data_providers['status_resp'];
      } else if (Yii::$app->request->isPut) {

        $ss = $step . $status;
        switch ($ss) {
          case '0init':
            $rpe_status = Yii::$app->db->createCommand(
              "SELECT '('
                      || t1.request_process_essay_status_id
                      || ') '
                      || p1.short_name AS status_name, 
                      t1.request_process_essay_status_id as status_id
              FROM   plims.bsns_request_process_essay t1
                      INNER JOIN plims.bsns_parameter p1
                              ON t1.request_process_essay_status_id = p1.parameter_id
              WHERE  t1.request_process_essay_id = :request_process_essay_id;",
              [':request_process_essay_id' => $request_process_essay_id]
            )->queryOne();

            $message_resp = 'status is alredy in process';

            if ($rpe_status['status_id'] == Functional::REQUEST_PROCESS_ESSAY_PENDING) {
              Script::instance()->checkStatusInProgressSubProcess($request_process_essay_id, $user, $date);
              $message_resp = 'status is in process just now';
            }

            break;
          case '1init':

            $message_resp = 'Set reading data';

            break;
          case '0finish':

            Yii::$app->db->createCommand(
              "UPDATE plims.bsns_request_process_essay_activity_sample t
              SET    start_by = :start_by,
                     start_date = :start_date
              FROM   plims.bsns_request_process_essay t1
                     INNER JOIN plims.bsns_request_process_essay_activity t2
                             ON t1.request_process_essay_id = t2.request_process_essay_id
                                AND t1.request_process_essay_id = :request_process_essay_id
              WHERE  t.request_process_essay_activity_id =
                     t2.request_process_essay_activity_id
                     AND t.start_by IS NULL
                     AND t.start_date IS NULL;",
              [
                ':start_by' => $user,
                ':start_date' => $date,
                ':request_process_essay_id' => $request_process_essay_id,
              ]
            )->execute();

            Yii::$app->db->createCommand(
              "UPDATE plims.bsns_request_process_essay_activity_sample t
              SET    request_process_essay_activity_sample_status_id =
                            :request_process_essay_activity_sample_finished,
                     finish_by = :finish_by,
                     finish_date = :finish_date
              FROM   plims.bsns_request_process_essay t1
                     INNER JOIN plims.bsns_request_process_essay_activity t2
                             ON t1.request_process_essay_id = t2.request_process_essay_id
                                AND t1.request_process_essay_id = :request_process_essay_id
              WHERE  t.request_process_essay_activity_id =
                     t2.request_process_essay_activity_id
                     AND t.request_process_essay_activity_sample_status_id <>
                         :request_process_essay_activity_sample_finished;",
              [
                ':request_process_essay_activity_sample_finished' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_FINISHED,
                ':finish_by' => $user,
                ':finish_date' => $date,
                ':request_process_essay_id' => $request_process_essay_id,
              ]
            )->execute();
            $message_resp = 'Activity-samples finished';

            break;
          case '1finish':

            Yii::$app->db->createCommand(
              "UPDATE plims.bsns_request_process_essay_activity
              SET    start_by = :start_by, 
                     start_date = :start_date
              WHERE  request_process_essay_id = :request_process_essay_id
                     AND start_by IS NULL
                     AND start_date IS NULL
                     AND status = :status_active;",
              [
                ':start_by' => $user,
                ':start_date' => $date,
                ':request_process_essay_id' => $request_process_essay_id,
                ':status_active' => Functional::STATUS_ACTIVE
              ]
            )->execute();

            Yii::$app->db->createCommand(
              "UPDATE plims.bsns_request_process_essay_activity
                SET    request_process_essay_activity_status_id = :request_process_essay_activity_finished, 
                      finish_by = :finish_by,
                      finish_date = :finish_date
                WHERE  request_process_essay_id = :request_process_essay_id
                       AND request_process_essay_activity_status_id <>
                           :request_process_essay_activity_finished
                       AND status = :status_active;",
              [
                ':request_process_essay_activity_finished' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_FINISHED,
                ':finish_by' => $user,
                ':finish_date' => $date,
                ':request_process_essay_id' => $request_process_essay_id,
                ':status_active' => Functional::STATUS_ACTIVE
              ]
            )->execute();

            Yii::$app->db->createCommand(
              "UPDATE plims.bsns_request_process_essay
                  SET    request_process_essay_status_id = :request_process_essay_finished, 
                  finish_by = :finish_by, 
                  finish_date = :finish_date
                  WHERE  request_process_essay_id = :request_process_essay_id; ",
              [
                ':request_process_essay_finished' => Functional::REQUEST_PROCESS_ESSAY_FINISHED,
                ':finish_by' => $user,
                ':finish_date' => $date,
                ':request_process_essay_id' => $request_process_essay_id,
              ]
            )->execute();

            $data = Yii::$app->db->createCommand(
              "SELECT t1.request_id,
                      t1.num_order_id,
                      t1.essay_id,
                      t1.composite_sample_type_id,
                      t1.request_process_essay_activity_id AS rpa_activity_id_get,
                      t1.activity_id                       AS activity_id_get
              FROM   plims.bsns_request_process_essay_activity t1
                      INNER JOIN plims.bsns_activity t2
                              ON t1.activity_id = t2.activity_id
              WHERE  t1.request_process_essay_id = :_rpa_id
                      AND t2.activity_property_id = :_ap_get;",
              [
                ':_rpa_id' => $request_process_essay_id,
                ':_ap_get' => Functional::ACTIVITY_PROPERTY_GET,
              ]
            )->queryOne();

            $request_id = $data['request_id'];
            $num_order_id = $data['num_order_id'];
            $essay_id = $data['essay_id'];
            $composite_sample_type_id = $data['composite_sample_type_id'];
            $rpa_activity_id_get = $data['rpa_activity_id_get'];
            $activity_id_get = $data['activity_id_get'];

            Script::instance()->standardizeResults($request_process_essay_id);

            Script::instance()->consolidateResults(
              $user,
              $date,
              $request_process_essay_id,
              $essay_id,
              $rpa_activity_id_get,
              $activity_id_get
            );

            if ($composite_sample_type_id != Functional::COMPOSITE_SAMPLE_INDIVIDUAL) {

              $activity_id_get =  Yii::$app->db->createCommand(
                "SELECT t1.activity_id AS activity_id_get
                FROM   plims.bsns_activity_by_essay t1
                       INNER JOIN plims.bsns_activity t2
                               ON t1.activity_id = t2.activity_id
                WHERE  t1.essay_id = :_assay_id::INTEGER
                       AND t2.activity_property_id = :_ap_get::INTEGER; ",
                [
                  ':_assay_id' => $essay_id,
                  ':_ap_get' => Functional::ACTIVITY_PROPERTY_GET
                ]
              )->queryScalar();
              $rdt_result = Functional::READING_DATA_ENTRY;

              $this->initReprocessNormalProcess(
                $request_id,
                $num_order_id,
                $request_process_essay_id,
                $essay_id,
                $composite_sample_type_id,
                $activity_id_get,
                $rdt_result,
                $user,
                $date
              );
            }

            $message_resp = 'Set reading data finished';
            $data = null;
            break;

          default:
            $message_resp = 'Default';
            break;
        }
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }
    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionAssaySampleMaterial(
    $request_process_essay_id
  ) {

    $data = null;
    $message_resp = null;
    $status_resp = 200;
    try {
      if (Yii::$app->request->isGet) {
        $data = Yii::$app->db->createCommand(
          "SELECT DISTINCT t1.composite_sample_id,
                    c1.short_name AS composite_sample_name,
                    CASE
                      WHEN c1.reading_data_type_id = :_rd_entry THEN t1.num_order
                      ELSE NULL
                    END           AS num_order
          FROM   plims.bsns_request_process_essay_activity_sample t1
          LEFT JOIN plims.bsns_composite_sample c1
                  ON t1.composite_sample_id = c1.composite_sample_id
          WHERE  t1.request_process_essay_id = :_request_process_essay_id
          AND c1.reading_data_type_id = :_rd_entry
          ORDER  BY num_order;",
          [
            ':_rd_entry' => Functional::READING_DATA_ENTRY,
            ':_request_process_essay_id' => $request_process_essay_id,
          ]
        )->queryAll();
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionAssayActivitySampleMaterial(
    $request_process_essay_id,
    $activity_id = null,
    $composite_sample_id = null,
    $sample_offset = null,
    $sample_limit = null,
    $last_sync_date = null
  ) {

    $data = null;
    $message_resp = null;
    $status_resp = 200;

    if (Yii::$app->request->isGet) {

      $ini_order =  $sample_offset ? $sample_offset + 1 : null;
      $end_order = $sample_limit ? $sample_offset + $sample_limit : null;

      $data = Yii::$app->db->createCommand(
        "SELECT tab.composite_sample_id,
                tab.activity_order,
                tab.activity_id,
                tab.status_id,
                tab.status_name,
                tab.version,
                tab.composite_sample_name,
                tab.activity_sample_order,
                tab.number_of_seeds,
                tab.start_date,
                tab.start_by,
                tab.finish_date,
                tab.finish_by
        FROM   (SELECT t2.composite_sample_id,
                        t1.num_order                                       AS
                        activity_order,
                        t1.activity_id,
                        t2.request_process_essay_activity_sample_status_id AS status_id,
                        p1.short_name                                      AS status_name
                        ,
                        t2.historical_data_version
                        AS version,
                        t3.short_name                                      AS
                              composite_sample_name,
                        t2.num_order                                       AS
                              activity_sample_order,
                        t2.number_of_seeds,
                        t2.start_date,
                        t2.start_by,
                        t2.finish_date,
                        t2.finish_by
                FROM   plims.bsns_request_process_essay_activity t1
                        --                INNER JOIN plims.bsns_activity t4
                        --                        ON t4.activity_id = t1.activity_id
                        --                          AND t4.activity_property_id = :activity_property_id
                        INNER JOIN plims.bsns_request_process_essay_activity_sample t2
                                ON t1.request_process_essay_activity_id =
                                  t2.request_process_essay_activity_id
                                  AND t2.available
                                  AND t1.request_process_essay_id =
                                      :request_process_essay_id
                                  AND ( ( t1.activity_id = :activity_id )
                                          OR ( :activity_id IS NULL ) )
                                  AND ( ( Greatest (t2.check_date, t2.registered_at,
                                          t2.start_date,
                                          t2.finish_date,
                                                  t2.deleted_at) > :last_sync_date )
                                          OR :last_sync_date IS NULL )
                        INNER JOIN plims.bsns_composite_sample t3
                                ON t2.composite_sample_id = t3.composite_sample_id
                                  AND ( t3.composite_sample_id = :composite_sample_id
                                          OR :composite_sample_id IS NULL )
                                  AND ( t3.reading_data_type_id = :reading_data_type_id
                                          OR :reading_data_type_id IS NULL )
                        LEFT JOIN plims.bsns_parameter p1
                              ON p1.parameter_id =
                                  t2.request_process_essay_activity_sample_status_id
                WHERE  ( ( t2.num_order BETWEEN :ini_order AND :end_order )
                          OR ( :ini_order IS NULL
                                OR :end_order IS NULL ) )
                ORDER  BY t1.num_order,
                          t2.num_order) tab
        WHERE  activity_id <> 2; ",
        [
          ':request_process_essay_id' => $request_process_essay_id,
          ':activity_id' => $activity_id,
          ':composite_sample_id' => $composite_sample_id,
          ':reading_data_type_id' => Functional::READING_DATA_ENTRY,
          ':ini_order' => $ini_order,
          ':end_order' => $end_order,
          ':last_sync_date' => $last_sync_date
        ]
      )->queryAll();
    } else if (Yii::$app->request->isPut) {

      $data_provider1 = Yii::$app->request->post();
      $user = $this->getUserId();
      $date = date("Y-m-d H:i:s", time());

      try {
        $data = array_map(
          function ($value) use ($user, $date, $request_process_essay_id) {
            $activity_id =  $value['activity_id'];
            $composite_sample_id =  $value['composite_sample_id'];
            $number_of_seeds =  $value['number_of_seeds'];
            // Script::instance()->checkStatusInProgress_($request_process_essay_id, $activity_id, $composite_sample_id, $user, $date);
            Script::instance()->checkRequestProcessAssayActivity($request_process_essay_id, $user, $date);
            Script::instance()->checkRequestProcessAssayActivitySample($request_process_essay_id, $activity_id, $composite_sample_id, $user, $date);
            // CAMBIAR POSISIONES SI ESQUE EL PROCESO CORRE CORRECTAMENTE
            $request_process_essay_activity_sample_id_new = Script::instance()->setActivitySampleMaterial($user, $date, $request_process_essay_id, $number_of_seeds, $activity_id, $composite_sample_id);
            return  is_null($request_process_essay_activity_sample_id_new) ? 'Same number of seeds!' : $request_process_essay_activity_sample_id_new;
          },
          $data_provider1
        );
      } catch (\Throwable $th) {
        $message_resp = $th->getMessage();
        $status_resp = 400;
      }
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }

  public function actionAgents(
    $request_id,
    $process_id = 0,
    $assay_id,
    $sample_type_id
  ) {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    if (Yii::$app->request->isGet) {

      if ($process_id <= 1) {
        $data = Yii::$app->db->createCommand(
          "SELECT c.num_order  AS agent_order,
                  c.agent_id   AS agent_id,
                  d.short_name AS agent_name
          FROM   plims.bsns_request a
                  INNER JOIN plims.bsns_request_process b
                          ON a.request_id = b.request_id
                            AND a.request_id = :_request_id
                            AND (b.num_order_id = :_num_order_id OR :_num_order_id = 0)
                  INNER JOIN plims.bsns_agent_condition c
                          ON b.crop_id = c.crop_id
                            AND c.essay_id = :_assay_id
                            AND a.request_type_id = c.request_type_id
                            AND c.composite_sample_type_id = :_sample_type_id
                            AND c.status = :_status_active
                  INNER JOIN plims.bsns_agent d
                          ON c.agent_id = d.agent_id
          ORDER  BY c.num_order;",
          [
            ':_request_id' => $request_id,
            ':_num_order_id' => $process_id,
            ':_assay_id' => $assay_id,
            ':_sample_type_id' => $sample_type_id,
            ':_status_active' => Functional::STATUS_ACTIVE,
          ]
        )->queryAll();
      } else {
        $data = Yii::$app->db->createCommand(
          "SELECT t1.num_order  AS agent_order,
                  t1.agent_id,
                  t2.short_name AS agent_name
          FROM   plims.bsns_request_process_essay_agent t1
                  INNER JOIN plims.bsns_agent t2
                          ON t1.agent_id = t2.agent_id
          WHERE  t1.request_id = :_request_id
                  AND t1.num_order_id = :_num_order_id
                  AND t1.essay_id = :_assay_id
          ORDER  BY t1.num_order;",
          [
            ':_request_id' => $request_id,
            ':_num_order_id' => $process_id,
            ':_assay_id' => $assay_id,
          ]
        )->queryAll();
      }
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  public function actionResult(
    $request_process_assay_id,
    $activity_id = null,
    $composite_sample_id = null,
    $last_sync_date = null
  ) {

    $data = null;
    $message_resp = null;
    $status_resp = 200;

    $user = Yii::$app->user->identity->id;
    $date = date("Y-m-d H:i:s", time());

    if (Yii::$app->request->isGet) {
      $data = Yii::$app->db->createCommand(
        "SELECT t2.number_of_seeds,
                  t2.activity_id,
                  t2.composite_sample_id,
                  t3.agent_id,
                  t3.short_name                  AS agent_name,
                  t1.text_result,
                  Cast (t1.number_result AS INT) AS number_result,
                  t1.auxiliary_result,
                  t2.num_order                   AS activity_sample_order_num,
                  t1.reprocess
          FROM   plims.bsns_reading_data t1
                  INNER JOIN plims.bsns_request_process_essay_activity_sample t2
                          ON t1.request_process_essay_activity_sample_id =
                            t2.request_process_essay_activity_sample_id
                  INNER JOIN plims.bsns_agent t3
                          ON t1.agent_id = t3.agent_id
          WHERE  t1.request_process_essay_id = :request_process_assay_id
                  AND ( t1.activity_id = :activity_id
                        OR :activity_id IS NULL )
                  AND ( t1.composite_sample_id = :composite_sample_id
                        OR :composite_sample_id IS NULL )
                  AND ( t1.registered_at > :last_sync_date
                        OR :last_sync_date IS NULL )
                  AND t1.status = :status_active;",
        [
          ':request_process_assay_id' => $request_process_assay_id,
          ':activity_id' => $activity_id,
          ':composite_sample_id' => $composite_sample_id,
          ':status_active' =>  Functional::STATUS_ACTIVE,
          ':last_sync_date' => $last_sync_date
        ]
      )->queryAll();
      if (count($data) == 0) $message_resp = 'No data recorded';
    } else if (Yii::$app->request->isPost) {
      $data_provider1 = Yii::$app->request->post();
      $new_reading_data_id = 0;
      try {
        $reprocess = isset($data_provider1['reprocess']) ? $data_provider1['reprocess'] : false;
        $number_result =  $data_provider1['number_result'];
        $agent_id =  $data_provider1['agent_id'];
        $text_result =  $data_provider1['text_result'];
        $activity_id = $data_provider1['activity_id'];
        $composite_sample_id = $data_provider1['composite_sample_id'];

        $new_reading_data_id = Script::instance()->mobSampleResultsAddResult(
          $number_result,
          $reprocess,
          $request_process_assay_id,
          $activity_id,
          $composite_sample_id,
          $agent_id,
          $text_result,
          $user,
          $date
        );
      } catch (\Throwable $th) {
        $message_resp = $th->getMessage();
        $status_resp = 400;
      }
      $data = [
        'new_reading_data_id' => $new_reading_data_id
      ];
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  public function actionRequestProcessAssay(
    $request_process_assay_id
  ) {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {
      if (Yii::$app->request->isGet) {
      } else if (Yii::$app->request->isPut) {
        $model1 = RequestProcessEssay::findOne($request_process_assay_id);
        $new_status = Yii::$app->request->post('status');
        $message_resp = 'Put ';
        if ($model1->request_process_essay_status_id == Functional::REQUEST_PROCESS_ESSAY_PENDING) {

          $this->setActiveStatus($request_process_assay_id);
          $message_resp = $message_resp . 'active and ';
          $activity_qty = 1; // En el caso de PCR
          if ($activity_qty > 0) {
            $model1->activity_qty = $activity_qty;
            if ($model1->save()) {
              $message_resp = $message_resp . 'update activity qty. in RPA';
              $this->setRegulateActivities($request_process_assay_id, $activity_qty);
              $data = [
                'activity_qty' => $model1->activity_qty,
                'type_id' =>  $model1->requestProcessEssayType->parameter_id,
                'type_name' => $model1->requestProcessEssayType->short_name,
                'status_id' => $model1->requestProcessEssayStatus->parameter_id,
                'status' => $model1->requestProcessEssayStatus->short_name,
                'start_date' => $model1->start_date,
                'end_date' => $model1->finish_date,
                'is_active' => $model1->status === Functional::STATUS_ACTIVE ? true : false,
              ];
            }
          }
        }
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }
    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  public function actionRequestProcessAssayAgent()
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    if (Yii::$app->request->isGet) {
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  // ================================================================================ SYNC ================================================================================

  public function actionCurrentDate()
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {
      if (Yii::$app->request->isGet) {
        $current_date = date("Y-m-d H:i:s", time());
        $data = [
          'current_date' => $current_date
        ];
      } else if (Yii::$app->request->isPut) {
      } else if (Yii::$app->request->isPost) {
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  public function actionLastModificationDates(
    $request_process_assay_id
  ) {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {
      if (Yii::$app->request->isGet) {

        $data['rpa_activity_sample_date']  = Yii::$app->db->createCommand(
          "SELECT Greatest(
              Max(t3.check_date), 
              Max(t3.registered_at), 
              Max(t3.start_date),
              Max(t3.finish_date), 
              Max(t3.deleted_at)) 
            AS rpa_activity_sample_date 
          FROM   plims.bsns_request_process_essay t1
                 INNER JOIN plims.bsns_request_process_essay_activity t2
                         ON t2.request_process_essay_id = t1.request_process_essay_id
                 INNER JOIN plims.bsns_request_process_essay_activity_sample t3
                         ON t2.request_process_essay_activity_id =
                            t3.request_process_essay_activity_id
                            AND t3.available
                 INNER JOIN plims.bsns_essay e1
                         ON t1.essay_id = e1.essay_id
          WHERE  t1.request_process_essay_id = :request_process_essay_id; ",
          [
            ':request_process_essay_id' => $request_process_assay_id
          ]
        )->queryScalar();

        $data['result_date'] = Yii::$app->db->createCommand(
          "SELECT Max(registered_at)AS result_date
          FROM   plims.bsns_reading_data
          WHERE  request_process_essay_id = :request_process_essay_id
                 AND status = 'active'; ",
          [
            ':request_process_essay_id' => $request_process_assay_id
          ]
        )->queryScalar();
      } else if (Yii::$app->request->isPut) {
      } else if (Yii::$app->request->isPost) {
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  // ================================================================================ TEMPLATE ================================================================================

  public function actionTemplate()
  {
    $data = null;
    $message_resp = null;
    $status_resp = 200;

    try {
      if (Yii::$app->request->isGet) {
      } else if (Yii::$app->request->isPut) {
      } else if (Yii::$app->request->isPost) {
      }
    } catch (\Throwable $th) {
      $status_resp = 400;
      $message_resp = $th->getMessage();
    }

    return $this->asJson([
      'code' => 0,
      'data' => $data,
      'message' => $message_resp,
      'status' => $status_resp,
    ]);
  }


  // ================================================================================ PRIVATE FUNCTIONS ================================================================================


  private function getSubProcessStatus($request_process_essay_id)
  {
    $sub_process_status = [
      'preparation' => 'pending',
      'evaluation' => 'pending'
    ];

    $data = Yii::$app->db->createCommand(
      "SELECT distinct t4.start_by,
        t4.start_date,
        t4.finish_by,
        t4.finish_date,
        t4.request_process_essay_activity_sample_status_id AS rpeas_status_id,
        p3.short_name                                      AS rpeas_status                  
    FROM   plims.bsns_request_process_essay t1
        INNER JOIN plims.bsns_parameter p1
                ON p1.parameter_id = t1.request_process_essay_status_id
        INNER JOIN plims.bsns_request_process_essay_activity t2
                ON t1.request_process_essay_id = t2.request_process_essay_id
        INNER JOIN plims.bsns_activity t3
                ON t3.activity_id = t2.activity_id
                  AND t3.activity_property_id = :activity_property_set
        INNER JOIN plims.bsns_parameter p2
                ON p2.parameter_id = t2.request_process_essay_activity_status_id
        INNER JOIN plims.bsns_request_process_essay_activity_sample t4
                ON t2.request_process_essay_activity_id =
                  t4.request_process_essay_activity_id
                  AND t4.available = true
        INNER JOIN plims.bsns_parameter p3
                ON p3.parameter_id =
                  t4.request_process_essay_activity_sample_status_id
    WHERE  t1.request_process_essay_id = :request_process_essay_id
        AND t2.status = :status_active;",
      [
        ':activity_property_set' => Functional::ACTIVITY_PROPERTY_SET,
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' =>  Functional::STATUS_ACTIVE,
      ]
    )->queryAll();

    // $rows_count =  count($data);
    $sub_process_status_list =  array_unique(array_column($data, 'rpeas_status'));
    $rows_count =  count($sub_process_status_list);

    if (in_array('in process', $sub_process_status_list)) {
      $sub_process_status['preparation'] = 'in process';
    }

    if (in_array('finished', $sub_process_status_list)) {
      $rpe_status = Yii::$app->db->createCommand(
        "SELECT CASE
                WHEN request_process_essay_status_id = :_rpa_pending THEN 'pending'
                WHEN request_process_essay_status_id = :_rpa_in_process THEN
                'in process'
                ELSE 'finished'
              END AS rpe_status
        FROM   plims.bsns_request_process_essay
        WHERE  request_process_essay_id = :_request_process_essay_id;",
        [
          ':_rpa_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
          ':_rpa_in_process' => Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
          ':_request_process_essay_id' => $request_process_essay_id,
        ]
      )->queryScalar();

      if ($rpe_status == 'finished') {
        $sub_process_status['evaluation'] = $rpe_status;
      } else {
        $sub_process_status['evaluation'] = 'in process';
      }

      if ($rows_count == 1) {
        $sub_process_status['preparation'] = 'finished';
      }
    }

    return  $sub_process_status;
  }


  private function setActiveStatus($request_process_essay_id)
  {
    Yii::$app->db->createCommand(
      "UPDATE plims.bsns_request_process_essay
        SET status = :status_active
        WHERE request_process_essay_id = :request_process_essay_id;",
      [
        ':request_process_essay_id' => $request_process_essay_id,
        ':status_active' => Functional::STATUS_ACTIVE
      ]
    )->execute();
  }


  private function getUserId()
  {
    $user_id =  null;
    $_token = Yii::$app->request->headers->get('authorization');

    if (!is_null($_token)) {

      $token = str_replace('Bearer ', '', $_token);
      $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
    } else {
      $user_id = \Yii::$app->user->identity->id;
    }
    return $user_id;
  }


  private function setRegulateActivities(
    $request_process_essay_id,
    $activity_qty
  ) {

    $registered_by = $this->getUserId();
    $registered_at = date("Y-m-d H:i:s", time());
    $status_disabled = Functional::STATUS_DISABLED;
    $status_active = Functional::STATUS_ACTIVE;



    Yii::$app->db->createCommand(
      "UPDATE plims.bsns_request_process_essay_activity t
      SET    registered_by = :registered_by,
           registered_at = :registered_at,
             status = :status_disabled
      WHERE  t.request_process_essay_id = :request_process_essay_id;",
      [
        ':registered_by' => $registered_by,
        ':registered_at' => $registered_at,
        ':status_disabled' => $status_disabled,
        ':request_process_essay_id' => $request_process_essay_id,
      ]
    )->execute();

    Yii::$app->db->createCommand(
      "UPDATE plims.bsns_request_process_essay_activity t
      SET    registered_by = :registered_by,
           registered_at = :registered_at,
             status = :status_active
      WHERE  t.request_process_essay_activity_id IN (
              SELECT c.request_process_essay_activity_id
              FROM   plims.bsns_activity a
                    LEFT JOIN plims.bsns_parameter b
                            ON a.activity_property_id = parameter_id
                    INNER JOIN plims.bsns_request_process_essay_activity c
                            ON a.activity_id = c.activity_id
              WHERE c.request_process_essay_id = :request_process_essay_id
                    AND ( ( b.code = 'set' AND c.num_order <= :num_order ) OR b.code = 'get' ) 
              ); ",
      [
        ':registered_by' => $registered_by,
        ':registered_at' => $registered_at,
        ':status_active' => $status_active,
        ':request_process_essay_id' => $request_process_essay_id,
        ':num_order' => $activity_qty,
      ]
    )->execute();

    return true;
  }


  public function initReprocessNormalProcess(
    $request_id,
    $num_order_id,
    $request_process_essay_id,
    $essay_id,
    $composite_sample_type_id,
    $activity_id_get,
    $rdt_result,
    $user,
    $date
  ) {

    $composite_sample_type_id_new = ($composite_sample_type_id == Functional::COMPOSITE_SAMPLE_GENERAL ?  Functional::COMPOSITE_SAMPLE_SIMPLE : Functional::COMPOSITE_SAMPLE_INDIVIDUAL);

    $num_order_id_new =  Functional::NUM_ORDER_ID_REPROCESS;
    // obtain first activitity ID
    $activity_id_set_first = Yii::$app->db->createCommand(
      'SELECT t1.activity_id as activity_id_set_first
        FROM   plims.bsns_activity_by_essay t1
              INNER JOIN plims.bsns_activity t2
                        ON t1.activity_id = t2.activity_id
        WHERE  t1.essay_id = :_assay_id::INTEGER
                AND t1.num_order = :_init_num_order::INTEGER
                AND t2.activity_property_id = :_ap_set::INTEGER
                AND t1.status = :_status_active::TEXT;',
      [
        ':_assay_id' => $essay_id,
        ':_init_num_order' => Functional::NUM_ORDER_INI,
        ':_ap_set' => Functional::ACTIVITY_PROPERTY_SET,
        ':_status_active' => Functional::STATUS_ACTIVE,
      ]
    )->queryScalar();

    Script::instance()->addRequestProcess(
      $user,
      $date,
      $request_id,
      $num_order_id,
      $num_order_id_new
    );

    $request_process_essay_id_new = Script::instance()->addRequestProcessAssay(
      $user,
      $date,
      $request_id,
      $essay_id,
      $num_order_id_new,
      $request_process_essay_id,
      $composite_sample_type_id_new
    );

    Script::instance()->addRequestProcessAssayAgentActive(
      $user,
      $date,
      $num_order_id_new,
      $request_process_essay_id,
      $request_process_essay_id_new,
      $composite_sample_type_id_new,
      $activity_id_get
    );

    // retorno de rpa_activity_id de SET y GET si este ultimo no es null
    $data = Script::instance()->addRequestProcessAssayActivity(
      $user,
      $date,
      $num_order_id_new,
      $request_process_essay_id,
      $request_process_essay_id_new,
      $composite_sample_type_id_new,
      $activity_id_set_first,
      $activity_id_get
    );
    // retorna el primero rpa-activity-id de tipo set
    $rpa_activity_id_get_new = $data['rpa_activity_id_get_new'];
    // null si no tiene actividad get
    $rpa_activity_id_set_new = $data['rpa_activity_id_set_new'];

    Script::instance()->addRequestProcessAssayActivitySample(
      $user,
      $date,
      $num_order_id_new,
      $request_process_essay_id,
      $request_process_essay_id_new,
      $composite_sample_type_id_new,
      $activity_id_set_first,
      $activity_id_get,
      $rpa_activity_id_set_new,
      $rpa_activity_id_get_new,
      $rdt_result
    );
  }
}

<?php

namespace frontend\modules\api\controllers;

use Yii;
use yii\rest\ActiveController;
use common\models\User;
use bizley\jwt\JwtHttpBearerAuth;

class UserController extends ActiveController
{
	public function behaviors()
	{
		$behaviors = parent::behaviors();
		$behaviors['authenticator'] = [
				'class' =>  JwtHttpBearerAuth::class,
				'except' => [],
		];
		$behaviors['JWTBearerAuth'] = [
			'class' =>  JwtHttpBearerAuth::class,
		 ];
		return $behaviors;
	}

	public $modelClass = User::class;

	public function actionMe()
	{
		$data = null;
		$message_resp = null;
		$status_resp = 200;

		try {
			$user_id =  $this->getUserId();

			$data_row = Yii::$app->db->createCommand(
            "SELECT t2.user_id,
						  t2.username,
						  t2.firstname,
						  t2.lastname,
						  t3.role_id,
						  CASE
                        WHEN t3.long_name IS NULL THEN ' - '
                        ELSE t3.long_name
                    end AS role_name,
						  t4.institution_id,
                    CASE
                        WHEN t4.short_name IS NULL THEN ' - '
                        ELSE t4.short_name
                    end AS institution_name,
						  t5.laboratory_id,
                    CASE
                        WHEN t5.long_name IS NULL THEN ' - '
                        ELSE t5.long_name
                    end AS laboratory_name
            FROM   plims.auth_user_by_role_institution_laboratory t1
                    INNER JOIN plims.auth_user t2
                            ON t1.user_id = t2.user_id
                                AND t1.role_id = t2.role
                    INNER JOIN plims.auth_role t3
                            ON t1.role_id = t3.role_id
                    LEFT JOIN plims.bsns_institution t4
                            ON t1.institution_id = t4.institution_id
                    LEFT JOIN plims.bsns_laboratory t5
                            ON t1.laboratory_id = t5.laboratory_id
            WHERE  t1.user_id = :_user_id
            LIMIT  1; ",
            [
               ':_user_id' => $user_id
            ]
        )->queryOne();

			$data = $data_row;
		} catch (\Throwable $th) {
			$status_resp = 400;
			$message_resp = $th->getMessage();
		}

		return $this->asJson([
			'code' => 0,
			'data' => $data,
			'message' => $message_resp,
			'status' => $status_resp,
		]);
	}

	private function getUserId()
	{
		$user_id =  null;
		$_token = Yii::$app->request->headers->get('authorization');
		if (!is_null($_token)) {
			$token = str_replace('Bearer ', '', $_token);
			$user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
		} else {
			$user_id = \Yii::$app->user->identity->id;
		}
		return $user_id;
	}

	// public function actionUpdateRequest(
	// 	$request_id,
	// 	$laboratory_id
	// )
	// {
	// 	$data = null;
	// 	$message_resp = null;
	// 	$status_resp = 200;

	// 	try {
	// 		$user_id =  $this->getUserId();

	// 		Yii::$app->db->createCommand(
	// 			"UPDATE plims.bsns_request
	// 			SET laboratory_id = :_laboratory_id
	// 			WHERE request_id = :_request_id
	// 			",
	// 			[
	// 			  ':_request_id' => $request_id,
	// 			  ':_laboratory_id' => $laboratory_id,
	// 			]
	// 		 )->execute();

			
	// 	} catch (\Throwable $th) {
	// 		$status_resp = 400;
	// 		$message_resp = $th->getMessage();
	// 	}

	// 	return $this->asJson([
	// 		'code' => 0,
	// 		'data' => $data,
	// 		'message' => $message_resp,
	// 		'status' => $status_resp,
	// 	]);
	// }
}
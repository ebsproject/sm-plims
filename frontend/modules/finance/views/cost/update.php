<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\finance\models\Cost */

$this->title = 'Update Cost: ' . $model->cost_id;
$this->params['breadcrumbs'][] = ['label' => 'Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cost_id, 'url' => ['view', 'id' => $model->cost_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cost-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

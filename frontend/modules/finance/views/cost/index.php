<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\finance\models\CostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Costs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cost-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Cost', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cost_id',
            'code',
            'description',
            'cost_value',
            'finance_period',
            //'status',
            //'registered_by',
            //'registered_at',
            //'updated_by',
            //'updated_at',
            //'deleted_by',
            //'deleted_at',
            //'cost_currency_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

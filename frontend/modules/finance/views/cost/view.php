<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\finance\models\Cost */

$this->title = $model->cost_id;
$this->params['breadcrumbs'][] = ['label' => 'Costs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cost-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->cost_id], ['class' => 'btn btn-primary']) ?>
        <?php
        //  Html::a('Delete', ['delete', 'id' => $model->cost_id], [
        //     'class' => 'btn btn-danger',
        //     'data' => [
        //         'confirm' => 'Are you sure you want to delete this item?',
        //         'method' => 'post',
        //     ],
        // ]) 
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cost_id',
            'code',
            'description',
            'cost_value',
            'finance_period',
            'status',
            'registered_by',
            'registered_at',
            'updated_by',
            'updated_at',
            'deleted_by',
            'deleted_at',
            'cost_currency_id',
        ],
    ]) ?>

</div>
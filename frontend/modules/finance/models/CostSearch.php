<?php

namespace frontend\modules\finance\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\finance\models\Cost;

/**
 * CostSearch represents the model behind the search form of `frontend\modules\finance\models\Cost`.
 */
class CostSearch extends Cost
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cost_id', 'finance_period', 'registered_by', 'updated_by', 'deleted_by', 'cost_currency_id'], 'integer'],
            [['code', 'description', 'status', 'registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['cost_value'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cost::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cost_id' => $this->cost_id,
            'cost_value' => $this->cost_value,
            'finance_period' => $this->finance_period,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'cost_currency_id' => $this->cost_currency_id,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

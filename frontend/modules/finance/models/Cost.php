<?php

namespace frontend\modules\finance\models;

use Yii;
use frontend\modules\configuration\models\Parameter;

/**
 * This is the model class for table "{{%cost}}".
 *
 * @property int $cost_id
 * @property string $code
 * @property string $description
 * @property double $cost_value
 * @property int $finance_period
 * @property string $status
 * @property int $registered_by
 * @property string $registered_at
 * @property int $updated_by
 * @property string $updated_at
 * @property int $deleted_by
 * @property string $deleted_at
 * @property int $cost_currency_id
 *
 * @property Parameter $costCurrency
 * @property Essay[] $essays
 */
class Cost extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{plims.bsns_cost}}';
    }

    public function rules()
    {
        return [
            [['cost_value'], 'number'],
            [['finance_period', 'registered_by', 'updated_by', 'deleted_by', 'cost_currency_id'], 'default', 'value' => null],
            [['finance_period', 'registered_by', 'updated_by', 'deleted_by', 'cost_currency_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['code'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 15],
            [['cost_currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['cost_currency_id' => 'parameter_id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'cost_id' => 'Cost ID',
            'code' => 'Code',
            'description' => 'Description',
            'cost_value' => 'Cost Value',
            'finance_period' => 'Finance Period',
            'status' => 'Status',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'cost_currency_id' => 'Cost Currency ID',
        ];
    }

    public function getCostCurrency()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'cost_currency_id']);
    }

    public function getEssays()
    {
        return $this->hasMany(Essay::class, ['essay_cost_id' => 'cost_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}

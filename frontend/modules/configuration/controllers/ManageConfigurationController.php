<?php

namespace frontend\modules\configuration\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use frontend\modules\functional\Functional;

class ManageConfigurationController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  public function actionIndex()
  {
    $is_disabled = true;
    $_user_role = Yii::$app->user->identity->role;
    if ($_user_role === Functional::SUPER_ADMIN_ROLE) {
      $is_disabled = false;
      $value_institution_id = Yii::$app->request->post('institution', 1);
    } else {
      $_user_institution = Yii::$app->user->Identity->institution;
      $value_institution_id = $_user_institution;
    }

    $data_institution = Yii::$app->db->createCommand(
      "SELECT institution_id,
              short_name
        FROM   plims.bsns_institution
        WHERE  is_deleted = false
              AND institution_id <> 1
        ORDER  BY short_name;"
    )->queryAll();
    $data_institution = ArrayHelper::map($data_institution, 'institution_id', 'short_name');
    $newItem = [1 => 'All'];
    $data_institution = $newItem + $data_institution;


    $_query = "SELECT 
                    t2.institution_id,
                    t2.short_name as institution,
                    t1.entity,
                    t1.singularity,
                String_agg(DISTINCT t1.code  || '_' || t1.short_name, ', ') AS code,
                String_agg(DISTINCT t1.status, ', ') AS status
              FROM plims.bsns_parameter t1 inner join plims.bsns_institution t2 
                        on t1.institution_id = t2.institution_id
              WHERE t1.status = :_status_active
              AND t1.institution_id is not null ";
    $_params = [];
    $_params[':_status_active'] =  Functional::STATUS_ACTIVE;

    if ($value_institution_id != null && $value_institution_id != 1) {
      $_query =  $_query . "AND  t1.institution_id = :_institution ";
      $_params[':_institution'] = $value_institution_id;
    }

    $_query =  $_query . "GROUP  BY t1.entity, t1.singularity, t2.short_name, t2.institution_id ORDER  BY 2, 3, 4;";

    $data = Yii::$app->db->createCommand($_query, $_params)->queryAll();

    $dataProvider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => [
          'institution_id',
          'institution',
          'entity',
          'singularity',
        ],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
      'data_institution' => $data_institution,
      'value_institution_id' => $value_institution_id,
      'is_disabled' => $is_disabled,
    ]);
  }


  public function actionEditConfiguration($entity, $singularity, $institution, $institution_id, $parameter_id = null)
  {
    if ($parameter_id == null) {
      $code = null;
      $short_name =  '';
      $long_name =  '';
      $text_value =  '';
      $number_value = '';
      $description = '';
      $class_details = '';
      $status = Functional::STATUS_ACTIVE;
    } else {
      $data_parameter = Yii::$app->db->createCommand(
        "SELECT code,
                short_name,
                long_name,
                text_value,
                number_value,
                description,
                class_details,
                status
        FROM   plims.bsns_parameter
        WHERE  parameter_id = :_parameter_id;",
        [
          ':_parameter_id' => $parameter_id
        ]
      )->queryOne();
      $code =  $data_parameter['code'];
      $short_name =  $data_parameter['short_name'];
      $long_name =  $data_parameter['long_name'];
      $text_value =  $data_parameter['text_value'];
      $number_value =  $data_parameter['number_value'];
      $description = $data_parameter['description'];
      $class_details = $data_parameter['class_details'];
      $status = $data_parameter['status'];
    }

    return $this->redirect(
      [
        'management',

        'entity' => $entity,
        'singularity' => $singularity,
        'institution' =>  $institution,
        'institution_id' =>  $institution_id,

        'parameter_id' => $parameter_id,
        'code' => $code,
        'short_name' => $short_name,
        'long_name' => $long_name,

        'text_value' =>  $text_value,
        'number_value' =>   $number_value,

        'class_details' => $class_details,
        'description' => $description,
        'status' => $status,
      ]
    );
  }


  public function actionManagement(
    $entity = null,
    $singularity = null,
    $institution_id = null,
    $institution = null,

    $parameter_id = null,
    $code = null,
    $short_name = null,
    $long_name = null,

    $text_value = null,
    $number_value = null,

    $description = null,
    $class_details = null,
    $status = null
  ) {

    $data_entity = Yii::$app->db->createCommand(
      "SELECT t1.short_name as entity
      FROM   plims.bsns_parameter t1
      WHERE entity = 'parameter' and singularity = 'entity' and description ='editable'
      AND status = :_status_active
      ORDER BY entity;",
      [
        ':_status_active' => Functional::STATUS_ACTIVE
      ]
    )->queryAll();
    $data_entity =  ArrayHelper::map($data_entity, 'entity', 'entity');

    $data_singularity = Yii::$app->db->createCommand(
      "SELECT DISTINCT t1.short_name as singularity
      FROM   plims.bsns_parameter t1
      WHERE entity = 'parameter' and singularity = 'singularity' and description ='editable'
      AND status = :_status_active
      ORDER BY singularity;",
      [
        ':_status_active' => Functional::STATUS_ACTIVE
      ]
    )->queryAll();
    $data_singularity =  ArrayHelper::map($data_singularity, 'singularity', 'singularity');

    $_query = "SELECT 
        t1.institution_id,
        t2.short_name as institution,
        t1.parameter_id,
        t1.code,
        t1.entity,
        t1.singularity,
        t1.short_name,
        t1.long_name,
        t1.number_value,
        t1.text_value,
        t1.description,
        t1.class_details,
        t1.status
      FROM   plims.bsns_parameter t1 
        inner join plims.bsns_institution t2 
        on t1.institution_id = t2.institution_id
      WHERE t1.entity = :_entity
      AND    t1.singularity = :_singularity 
      AND    t1.institution_id = :_institution_id
      ORDER BY t1.code;";

    $_parameters = [
      ':_entity' => $entity,
      ':_singularity' => $singularity,
      ':_institution_id' => $institution_id,
    ];

    $data_configuration = Yii::$app->db->createCommand(
      $_query,
      $_parameters
    )->queryAll();

    $data_provider_configuration = new ArrayDataProvider([
      'allModels' => $data_configuration,
      'sort' => [
        'attributes' => [
          'parameter_id',
          'code',
          'entity',
          'singularity',
          'short_name',
          'long_name',
          'number_value',
          'text_value',
          'description',
          'class_details',
          'status'
        ],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    $status = $status ? $status : 'active';

    return $this->render('management', [
      'code' => $code,
      'data_entity' => $data_entity,
      'data_singularity' => $data_singularity,
      'data_provider_configuration' => $data_provider_configuration,
    ]);
  }


  public function actionNewConfiguration($institution_id, $institution)
  {
    $redirect = [
      'management',
      'institution_id' => $institution_id,
      'institution' => $institution,
    ];

    if ($institution_id == 1) {
      Yii::$app->session->setFlash('info', \Yii::t('app', 'First you must choose an institution.'));
      $redirect = ['index'];
    }

    return $this->redirect(
      $redirect,
    );
  }


  public function actionSaveConfiguration(
    $entity = null,
    $singularity = null,
    $institution_id = null,
    $institution = null,
    $parameter_id = null
  ) {
    try {
      $date = date("Y-m-d H:i:s", time());
      $user = Yii::$app->user->identity->id;

      $code =  empty(Yii::$app->request->post('code')) ? null : Yii::$app->request->post('code');
      $short_name = empty(Yii::$app->request->post('short_name')) ? null : Yii::$app->request->post('short_name');
      $long_name = empty(Yii::$app->request->post('long_name')) ? null : Yii::$app->request->post('long_name');

      $text_value = empty(Yii::$app->request->post('text_value')) ? null : Yii::$app->request->post('text_value');
      $number_value = empty(Yii::$app->request->post('number_value')) ? null : Yii::$app->request->post('number_value');

      $description = empty(Yii::$app->request->post('description')) ? null : Yii::$app->request->post('description');
      $class_details =  empty(Yii::$app->request->post('class_details')) ? null : Yii::$app->request->post('class_details');
      $status =  empty(Yii::$app->request->post('status')) ? null : Yii::$app->request->post('status');

      if ($parameter_id == null) {
        $entity =  empty(Yii::$app->request->post('entity')) ? null : Yii::$app->request->post('entity');
        $singularity =  empty(Yii::$app->request->post('singularity')) ? null : Yii::$app->request->post('singularity');
        $parameter_id =  Yii::$app->db->createCommand(
          "INSERT INTO plims.bsns_parameter
            (
              entity,
              singularity,
              code,
              short_name,
              long_name,
              text_value,
              number_value,
              description,
              class_details,
              status,
              registered_by,
              registered_at,
              institution_id
            )
            VALUES
            (
              :_entity,
              :_singularity,
              :_code,
              :_short_name,
              :_long_name,
              :_text_value,
              :_number_value,
              :_description,
              :_class_detail,
              :_status_active,
              :_registered_by,
              :_registered_at,
              :_institution_id
            )
            returning parameter_id;",
          [
            ':_entity' => $entity,
            ':_singularity' => $singularity,
            ':_code' => $code,
            ':_short_name' => $short_name,
            ':_long_name' => $long_name,
            ':_text_value' => $text_value,
            ':_number_value' => $number_value,
            ':_description' => $description,
            ':_class_detail' => $class_details,
            ':_status_active' => $status,
            ':_registered_by' =>  $user,
            ':_registered_at' => $date,
            ':_institution_id' => $institution_id
          ]
        )->queryScalar();
        Yii::$app->session->setFlash('success', \Yii::t('app', 'New parameter has been created.'));
      } else {
        Yii::$app->db->createCommand(
          "UPDATE plims.bsns_parameter t
          SET   
            code = :_code,
            status = :_status_active,
            short_name = :_short_name,
            long_name = :_long_name,
            text_value = :_text_value,
            number_value = :_number_value,
            description = :_description,
            class_details = :_class_detail,
            institution_id = :_institution_id
          WHERE parameter_id = :_parameter_id;",
          [
            ':_code' => $code,
            ':_status_active' => $status,
            ':_short_name' => $short_name,
            ':_long_name' => $long_name,
            ':_text_value' => $text_value,
            ':_number_value' => $number_value,
            ':_description' => $description,
            ':_class_detail' => $class_details,
            ':_parameter_id' => $parameter_id,
            ':_institution_id' => $institution_id
          ]
        )->execute();
        Yii::$app->session->setFlash('success', \Yii::t('app', 'Parameter has been updated.'));
      }
    } catch (\Throwable $th) {
      $_error_code = $th->getCode();
      if ($_error_code == 23505) {
        Yii::$app->session->setFlash('error', \Yii::t('app', 'The parameter code must be unique, Set another code.'));
      } else {
        Yii::$app->session->setFlash('error', \Yii::t('app', 'Error code [' . $_error_code . ']: ' . $th->getMessage()));
      }
      $code = null;
    }

    $status = $status ? $status : 'active';

    return $this->redirect(
      [
        'management',
        'entity' => $entity,
        'singularity' => $singularity,

        'institution_id' => $institution_id,
        'institution' => $institution,

        'parameter_id' => $parameter_id,

        'code' => $code,
        'short_name' => $short_name,
        'long_name' => $long_name,

        'text_value' =>  $text_value,
        'number_value' =>   $number_value,

        'class_details' => $class_details,
        'description' => $description,
        'status' => $status,
      ]
    );
  }


  public function actionRemoveConfiguration($entity, $singularity, $parameter_id = null)
  {
    Yii::$app->db->createCommand(
      "UPDATE plims.bsns_parameter
      SET    status = :_status_disabled
      WHERE  entity = :_entity
      AND    singularity = :_singularity
      AND (  parameter_id = :_parameter_id OR :_parameter_id IS NULL);",
      [
        ':_status_disabled' => Functional::STATUS_DISABLED,
        ':_entity' => $entity,
        ':_singularity' => $singularity,
        ':_parameter_id' => $parameter_id,
      ]
    )->execute();

    if ($parameter_id == null) {
      $redirect = ['index'];
    } else {
      $redirect = [
        'management',
        'entity' => $entity,
        'singularity' => $singularity,
      ];
    }

    return $this->redirect(
      $redirect
    );
  }
}

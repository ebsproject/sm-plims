<?php

namespace frontend\modules\configuration\controllers;

use Yii;
use frontend\modules\configuration\models\AgentByEssay;
use frontend\modules\configuration\models\AgentByEssaySearch;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Agent;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;


class AgentByEssayController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new AgentByEssaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($agent_id, $essay_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($agent_id, $essay_id),
        ]);
    }

    public function actionCreate()
    {
        $model = new AgentByEssay();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'agent_id' => $model->agent_id, 'essay_id' => $model->essay_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($agent_id, $essay_id)
    {
        $model = $this->findModel($agent_id, $essay_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'agent_id' => $model->agent_id, 'essay_id' => $model->essay_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($agent_id, $essay_id)
    {
        $this->findModel($agent_id, $essay_id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($agent_id, $essay_id)
    {
        if (($model = AgentByEssay::findOne(['agent_id' => $agent_id, 'essay_id' => $essay_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // -------------------------------------------------------------------------------------------------------------------------------------- //

    public function actionIndexManage()
    {
        $searchModel = new AgentByEssaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $select_data_1 = null;
        $select_value_1 = null;
        $select_data_2 = null;
        $select_value_2 = null;

        $select_data_1 = ArrayHelper::map(Essay::find()->where(
            [
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'essay_id', 'short_name');

        $select_data_2 = ArrayHelper::map(Agent::find()->where(
            [
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'agent_id', 'short_name');

        if (!is_null(Yii::$app->request->post('select_name_1'))) {
            $select_value_1 = Yii::$app->request->post('select_name_1');
            $agent_by_essay_model = AgentByEssay::find()->where(
                [
                    'essay_id' => $select_value_1,
                    'status' => 'active',
                ]
            )->orderBy(
                [
                    'num_order' => SORT_ASC
                ]
            )->all();
            $select_value_2 = ArrayHelper::map($agent_by_essay_model, 'agent_id', 'agent_id');
        }

        return $this->render('index-manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'select_data_1' => $select_data_1,
            'select_value_1' => $select_value_1,
            'select_data_2' => $select_data_2,
            'select_value_2' => $select_value_2,
        ]);
    }

    public function actionEnable($agent_id, $essay_id, $order)
    {
        $model = $this->findModel($agent_id, $essay_id);
        $model->status = 'active';
        $model->num_order = $order;
        $model->deleted_at = null;
        $model->deleted_by = null;
        $model->save();
    }

    public function actionDisabled($agent_id, $essay_id)
    {
        $model = $this->findModel($agent_id, $essay_id);
        $model->status = 'disabled';
        $model->deleted_at = date("Y-m-d H:i:s", time());
        $model->deleted_by = Yii::$app->user->identity->id;
        $model->save();
    }

    public function actionAddRemoveAgentByEssay()
    {
        $postArray = Yii::$app->request->post();
        if (!is_null(Yii::$app->request->post('select_name_1')) and !is_null(Yii::$app->request->post('select_name_2'))) {
            $select_value_1 = Yii::$app->request->post('select_name_1');
            $select_value_2 = Yii::$app->request->post('select_name_2');
            $select_value_2_old =
                ArrayHelper::map(
                    AgentByEssay::find()->where([
                        'essay_id' => $select_value_1
                    ])->all(),
                    'agent_id',
                    'agent_id'
                );
            foreach ($select_value_2_old as $key => $value) {
                $this->actionDisabled($value, $select_value_1);
            }
            $order = 1;
            foreach ($select_value_2 as $key => $value) {
                if (in_array($value, $select_value_2_old)) {
                    $this->actionEnable($value, $select_value_1, $order);
                } else {
                    $model = new AgentByEssay();
                    $model->essay_id = $select_value_1;
                    $model->agent_id = $value;
                    $model->num_order = $order;
                    $model->save();
                }
                $order =  $order + 1;
            }
        }
        return $this->redirect(['index-manage']);
    }
}

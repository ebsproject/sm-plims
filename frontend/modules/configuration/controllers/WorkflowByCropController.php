<?php

namespace frontend\modules\configuration\controllers;

use Yii;
use frontend\modules\configuration\models\WorkflowByCrop;
use frontend\modules\configuration\models\WorkflowByCropSearch;
use frontend\modules\configuration\models\Workflow;
use frontend\modules\configuration\models\Crop;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class WorkflowByCropController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new WorkflowByCropSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($workflow_id, $crop_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($workflow_id, $crop_id),
        ]);
    }

    public function actionCreate()
    {
        $model = new WorkflowByCrop();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'workflow_id' => $model->workflow_id, 'crop_id' => $model->crop_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($workflow_id, $crop_id)
    {
        $model = $this->findModel($workflow_id, $crop_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'workflow_id' => $model->workflow_id, 'crop_id' => $model->crop_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($workflow_id, $crop_id)
    {
        $this->findModel($workflow_id, $crop_id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($workflow_id, $crop_id)
    {
        if (($model = WorkflowByCrop::findOne(['workflow_id' => $workflow_id, 'crop_id' => $crop_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // -------------------------------------------------------------------------------------------------------------------------------------- //

    public function actionIndexManage()
    {
        $searchModel = new WorkflowByCropSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $select_data_1 = null;
        $select_value_1 = null;
        $select_data_2 = null;
        $select_value_2 = null;

        $select_data_1 = ArrayHelper::map(Crop::find()->where(
            [
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'crop_id', 'short_name');

        $select_data_2 = ArrayHelper::map(Workflow::find()->where(
            [
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'workflow_id', 'short_name');

        if (!is_null(Yii::$app->request->post('select_name_1'))) {
            $select_value_1 = Yii::$app->request->post('select_name_1');
            $select_value_2 = ArrayHelper::map(Crop::findOne($select_value_1)->workflows, 'workflow_id', 'workflow_id');
        }

        return $this->render('index-manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'select_data_1' => $select_data_1,
            'select_value_1' => $select_value_1,
            'select_data_2' => $select_data_2,
            'select_value_2' => $select_value_2
        ]);
    }

    public function actionEnable($workflow_id, $crop_id, $order)
    {
        $model = $this->findModel($workflow_id, $crop_id);
        $model->status = 'active';
        $model->num_order = $order;
        $model->deleted_at = null;
        $model->deleted_by = null;
        $model->save();
    }

    public function actionDisabled($workflow_id, $crop_id)
    {
        $model = $this->findModel($workflow_id, $crop_id);
        $model->status = 'disabled';
        $model->deleted_at = date("Y-m-d H:i:s", time());
        $model->deleted_by = Yii::$app->user->identity->id;
        $model->save();
    }

    public function actionAddRemoveWorkflowByCrop()
    {
        $postArray = Yii::$app->request->post();
        if (!is_null(Yii::$app->request->post('select_name_1')) and !is_null(Yii::$app->request->post('select_name_2'))) {
            $select_value_1 = Yii::$app->request->post('select_name_1');
            $select_value_2 = Yii::$app->request->post('select_name_2');
            $select_value_2_old =
                ArrayHelper::map(
                    WorkflowByCrop::find()->where([
                        'crop_id' => $select_value_1
                    ])->all(),
                    'workflow_id',
                    'workflow_id'
                );
            foreach ($select_value_2_old as $key => $value) {
                $this->actionDisabled($value, $select_value_1);
            }
            $order = 1;
            foreach ($select_value_2 as $key => $value) {
                if (in_array($value, $select_value_2_old)) {
                    $this->actionEnable($value, $select_value_1, $order);
                } else {
                    $model = new WorkflowByCrop();
                    $model->crop_id = $select_value_1;
                    $model->workflow_id = $value;
                    $model->num_order = $order;
                    $model->save();
                }
                $order =  $order + 1;
            }
        }
        return $this->redirect(['index-manage']);
    }
}

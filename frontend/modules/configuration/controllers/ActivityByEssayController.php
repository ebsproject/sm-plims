<?php

namespace frontend\modules\configuration\controllers;


use Yii;
use frontend\modules\configuration\models\ActivityByEssay;
use frontend\modules\configuration\models\ActivityByEssaySearch;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Activity;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ActivityByEssayController implements the CRUD actions for ActivityByEssay model.
 */
class ActivityByEssayController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ActivityByEssay models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ActivityByEssaySearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ActivityByEssay model.
     * @param integer $activity_id
     * @param integer $essay_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($activity_id, $essay_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($activity_id, $essay_id),
        ]);
    }

    /**
     * Creates a new ActivityByEssay model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ActivityByEssay();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'activity_id' => $model->activity_id, 'essay_id' => $model->essay_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ActivityByEssay model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $activity_id
     * @param integer $essay_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($activity_id, $essay_id)
    {
        $model = $this->findModel($activity_id, $essay_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'activity_id' => $model->activity_id, 'essay_id' => $model->essay_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ActivityByEssay model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $activity_id
     * @param integer $essay_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($activity_id, $essay_id)
    {
        $this->findModel($activity_id, $essay_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ActivityByEssay model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $activity_id
     * @param integer $essay_id
     * @return ActivityByEssay the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($activity_id, $essay_id)
    {
        if (($model = ActivityByEssay::findOne(['activity_id' => $activity_id, 'essay_id' => $essay_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function actionIndexManage()
    {
        $searchModel = new ActivityByEssaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $select_data_1 = null;
        $select_value_1 = null;
        $select_data_2 = null;
        $select_value_2 = null;

        $select_data_1 = ArrayHelper::map(Essay::find()->where(
            [
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'essay_id', 'short_name');

        $select_data_2 = ArrayHelper::map(
            Activity::find()
                ->select(
                    [
                        'activity_id', "concat(short_name,' [',long_name,']') as short_name"
                    ]
                )
                ->where(
                    [
                        'deleted_by' => null,
                        'deleted_at' => null,
                        'status' => 'active',
                    ]
                )
                ->orderBy(
                    [
                        'short_name'  => SORT_ASC
                    ]
                )->all(),
            'activity_id',
            'short_name'
        );

        if (!is_null(Yii::$app->request->post('select_name_1'))) {
            $select_value_1 = Yii::$app->request->post('select_name_1');
            $activity_by_essay_model = ActivityByEssay::find()->where(
                [
                    'essay_id' => $select_value_1,
                    'status' => 'active',
                ]
            )->orderBy(
                [
                    'num_order' => SORT_ASC
                ]
            )->all();
            $select_value_2 = ArrayHelper::map($activity_by_essay_model, 'activity_id', 'activity_id');
        }

        return $this->render('index-manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'select_data_1' => $select_data_1,
            'select_value_1' => $select_value_1,
            'select_data_2' => $select_data_2,
            'select_value_2' => $select_value_2
        ]);
    }

    public function actionEnable($activity_id, $essay_id, $order)
    {
        $model = $this->findModel($activity_id, $essay_id);
        $model->status = 'active';
        $model->num_order = $order;
        $model->deleted_at = null;
        $model->deleted_by = null;
        $model->save();
    }

    public function actionDisabled($activity_id, $essay_id)
    {
        $model = $this->findModel($activity_id, $essay_id);
        $model->status = 'disabled';
        $model->deleted_at = date("Y-m-d H:i:s", time());
        $model->deleted_by = Yii::$app->user->identity->id;
        $model->save();
    }

    public function actionAddRemoveActivityByEssay()
    {
        $postArray = Yii::$app->request->post();
        if (!is_null(Yii::$app->request->post('select_name_1')) and !is_null(Yii::$app->request->post('select_name_2'))) {
            $select_value_1 = Yii::$app->request->post('select_name_1');
            $select_value_2 = Yii::$app->request->post('select_name_2');
            $select_value_2_old =
                ArrayHelper::map(
                    ActivityByEssay::find()->where([
                        'essay_id' => $select_value_1
                    ])->all(),
                    'activity_id',
                    'activity_id'
                );
            foreach ($select_value_2_old as $key => $value) {
                $this->actionDisabled($value, $select_value_1);
            }
            $order = 1;
            foreach ($select_value_2 as $key => $value) {
                if (in_array($value, $select_value_2_old)) {
                    $this->actionEnable($value, $select_value_1, $order);
                } else {
                    $model = new ActivityByEssay();
                    $model->essay_id = $select_value_1;
                    $model->activity_id = $value;
                    $model->num_order = $order;
                    $model->save();
                }
                $order =  $order + 1;
            }
        }
        return $this->redirect(['index-manage']);
    }
}

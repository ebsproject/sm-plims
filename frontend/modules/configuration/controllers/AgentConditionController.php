<?php

namespace frontend\modules\configuration\controllers;

use Yii;
use frontend\modules\configuration\models\AgentCondition;
use frontend\modules\configuration\models\AgentConditionSearch;

use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Parameter;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * AgentConditionController implements the CRUD actions for AgentCondition model.
 */
class AgentConditionController extends Controller
{
    // parameter undeterminated
    const PARAMETER_UNDETERMINED = 1;

    // request types
    const REQUEST_TYPE_SHIPMENT_DISTRIBUTION = 11; //shipment distribution
    const REQUEST_TYPE_INTRODUCTION = 40; //introduction

    // parameter composite sample type
    const COMPOSITE_SAMPLE_TYPE_HEADER = 32;
    const COMPOSITE_SAMPLE_GENERAL = 30;
    const COMPOSITE_SAMPLE_SIMPLE = 31;
    const COMPOSITE_SAMPLE_INDIVIDUAL = 29;


    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new AgentConditionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($num_order, $essay_id, $crop_id, $request_type_id, $composite_sample_type_id, $request_process_essay_type_id, $agent_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($num_order, $essay_id, $crop_id, $request_type_id, $composite_sample_type_id, $request_process_essay_type_id, $agent_id),
        ]);
    }


    public function actionCreate()
    {
        $model = new AgentCondition();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'num_order' => $model->num_order, 'essay_id' => $model->essay_id, 'crop_id' => $model->crop_id, 'request_type_id' => $model->request_type_id, 'composite_sample_type_id' => $model->composite_sample_type_id, 'request_process_essay_type_id' => $model->request_process_essay_type_id, 'agent_id' => $model->agent_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($num_order, $essay_id, $crop_id, $request_type_id, $composite_sample_type_id, $request_process_essay_type_id, $agent_id)
    {
        $model = $this->findModel($num_order, $essay_id, $crop_id, $request_type_id, $composite_sample_type_id, $request_process_essay_type_id, $agent_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'num_order' => $model->num_order, 'essay_id' => $model->essay_id, 'crop_id' => $model->crop_id, 'request_type_id' => $model->request_type_id, 'composite_sample_type_id' => $model->composite_sample_type_id, 'request_process_essay_type_id' => $model->request_process_essay_type_id, 'agent_id' => $model->agent_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionDelete($num_order, $essay_id, $crop_id, $request_type_id, $composite_sample_type_id, $request_process_essay_type_id, $agent_id)
    {
        $this->findModel($num_order, $essay_id, $crop_id, $request_type_id, $composite_sample_type_id, $request_process_essay_type_id, $agent_id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($num_order, $essay_id, $crop_id, $request_type_id, $composite_sample_type_id, $request_process_essay_type_id, $agent_id)
    {
        if (($model = AgentCondition::findOne(['num_order' => $num_order, 'essay_id' => $essay_id, 'crop_id' => $crop_id, 'request_type_id' => $request_type_id, 'composite_sample_type_id' => $composite_sample_type_id, 'request_process_essay_type_id' => $request_process_essay_type_id, 'agent_id' => $agent_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionIndexManage()
    {
        $searchModel = new AgentConditionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $data = Yii::$app->request->post('consolidated_agent_condition');

        if (!empty($data)) {
            $data_rows = preg_split('/\r\n?/', $data, -1, PREG_SPLIT_NO_EMPTY);

            $first_essay_id = preg_split('/[\s,]+/', $data_rows[0], -1, PREG_SPLIT_NO_EMPTY)[0];

            Yii::$app
                ->db
                ->createCommand()
                ->delete('plims.bsns_agent_condition', [
                    'status' => 'active',
                    'essay_id' => $first_essay_id
                ])
                ->execute();


            foreach ($data_rows as $key_row => $value_row) {

                $data_columns = preg_split('/[\s,]+/', $value_row, -1, PREG_SPLIT_NO_EMPTY);

                //-----------------------------------------------------------------------------------

                $essay_id = $data_columns[0];
                //---------

                //---------
                $crop_id = $data_columns[1];
                $request_type_id = $data_columns[2];
                $composite_sample_type_id = $data_columns[3];
                //---------

                //---------
                $request_process_essay_type_id = $data_columns[4] == '*' ? self::PARAMETER_UNDETERMINED : $data_columns[4];
                //---------

                //---------
                $agent_id = $data_columns[5];
                $reprocess_flag =   $data_columns[6];
                $num_order = $data_columns[7];
                $repetition = $data_columns[8];
                //-----------------------------------------------------------------------------------
                if ($first_essay_id  == $essay_id) {

                    if ($crop_id == '*') {
                        $crop_list = ArrayHelper::map(Crop::find()->where(['status' => 'active'])->all(), 'crop_id', 'crop_id');
                        foreach ($crop_list as $key_crop => $value_crop) {
                            if ($request_type_id == '*') {
                                $request_type_list = ArrayHelper::map(
                                    Parameter::find()
                                        ->where(
                                            [
                                                'status' => 'active',
                                                'entity' => 'request',
                                                'singularity' => 'type',
                                            ]
                                        )
                                        ->all(),
                                    'parameter_id',
                                    'parameter_id'
                                );
                                foreach ($request_type_list as $key_request_type => $value_request_type) {
                                    if ($composite_sample_type_id == '*') {
                                        $composite_sample_type_list = ArrayHelper::map(Parameter::find()->where(['status' => 'active', 'entity' => 'composite_sample', 'singularity' => 'type',])->andWhere(['<>', 'parameter_id', self::COMPOSITE_SAMPLE_TYPE_HEADER])->all(), 'parameter_id', 'parameter_id');
                                        foreach ($composite_sample_type_list as $key_composite_sample_type => $value_composite_sample_type) {
                                            $agent_condition_model = new AgentCondition();
                                            $agent_condition_model->essay_id = $essay_id;

                                            $agent_condition_model->crop_id = $value_crop;
                                            $agent_condition_model->request_type_id =  $value_request_type;
                                            $agent_condition_model->composite_sample_type_id = $value_composite_sample_type;

                                            $agent_condition_model->request_process_essay_type_id = $request_process_essay_type_id;
                                            $agent_condition_model->agent_id = $agent_id;;
                                            $agent_condition_model->reprocess_flag = $reprocess_flag;
                                            $agent_condition_model->num_order = $num_order;
                                            $agent_condition_model->repetition = $repetition;
                                            $agent_condition_model->save();
                                        }
                                    } else {
                                        $agent_condition_model = new AgentCondition();
                                        $agent_condition_model->essay_id = $essay_id;

                                        $agent_condition_model->crop_id = $value_crop;
                                        $agent_condition_model->request_type_id =  $value_request_type;
                                        $agent_condition_model->composite_sample_type_id = $composite_sample_type_id;

                                        $agent_condition_model->request_process_essay_type_id = $request_process_essay_type_id;
                                        $agent_condition_model->agent_id = $agent_id;;
                                        $agent_condition_model->reprocess_flag = $reprocess_flag;
                                        $agent_condition_model->num_order = $num_order;
                                        $agent_condition_model->repetition = $repetition;
                                        $agent_condition_model->save();
                                    }
                                }
                            } else {
                                if ($composite_sample_type_id == '*') {
                                    $composite_sample_type_list = ArrayHelper::map(Parameter::find()->where(['status' => 'active', 'entity' => 'composite_sample', 'singularity' => 'type',])->andWhere(['<>', 'parameter_id', self::COMPOSITE_SAMPLE_TYPE_HEADER])->all(), 'parameter_id', 'parameter_id');
                                    foreach ($composite_sample_type_list as $key_composite_sample_type => $value_composite_sample_type) {
                                        $agent_condition_model = new AgentCondition();
                                        $agent_condition_model->essay_id = $essay_id;

                                        $agent_condition_model->crop_id = $value_crop;
                                        $agent_condition_model->request_type_id =  $request_type_id;
                                        $agent_condition_model->composite_sample_type_id = $value_composite_sample_type;

                                        $agent_condition_model->request_process_essay_type_id = $request_process_essay_type_id;
                                        $agent_condition_model->agent_id = $agent_id;;
                                        $agent_condition_model->reprocess_flag = $reprocess_flag;
                                        $agent_condition_model->num_order = $num_order;
                                        $agent_condition_model->repetition = $repetition;
                                        $agent_condition_model->save();
                                    }
                                } else {
                                    $agent_condition_model = new AgentCondition();
                                    $agent_condition_model->essay_id = $essay_id;

                                    $agent_condition_model->crop_id = $value_crop;
                                    $agent_condition_model->request_type_id =  $request_type_id;
                                    $agent_condition_model->composite_sample_type_id = $composite_sample_type_id;

                                    $agent_condition_model->request_process_essay_type_id = $request_process_essay_type_id;
                                    $agent_condition_model->agent_id = $agent_id;;
                                    $agent_condition_model->reprocess_flag = $reprocess_flag;
                                    $agent_condition_model->num_order = $num_order;
                                    $agent_condition_model->repetition = $repetition;
                                    $agent_condition_model->save();
                                }
                            }
                        }
                    } else {
                        if ($request_type_id == '*') {
                            $request_type_list = ArrayHelper::map(Parameter::find()
                                ->where(
                                    [
                                        'status' => 'active',
                                        'entity' => 'request',
                                        'singularity' => 'type',
                                    ]
                                )
                                ->all(), 'parameter_id', 'parameter_id');
                            foreach ($request_type_list as $key_request_type => $value_request_type) {
                                if ($composite_sample_type_id == '*') {
                                    $composite_sample_type_list = ArrayHelper::map(Parameter::find()->where(['status' => 'active', 'entity' => 'composite_sample', 'singularity' => 'type',])->andWhere(['<>', 'parameter_id', self::COMPOSITE_SAMPLE_TYPE_HEADER])->all(), 'parameter_id', 'parameter_id');
                                    foreach ($composite_sample_type_list as $key_composite_sample_type => $value_composite_sample_type) {
                                        $agent_condition_model = new AgentCondition();
                                        $agent_condition_model->essay_id = $essay_id;

                                        $agent_condition_model->crop_id = $crop_id;
                                        $agent_condition_model->request_type_id =  $value_request_type;
                                        $agent_condition_model->composite_sample_type_id = $value_composite_sample_type;

                                        $agent_condition_model->request_process_essay_type_id = $request_process_essay_type_id;
                                        $agent_condition_model->agent_id = $agent_id;;
                                        $agent_condition_model->reprocess_flag = $reprocess_flag;
                                        $agent_condition_model->num_order = $num_order;
                                        $agent_condition_model->repetition = $repetition;
                                        $agent_condition_model->save();
                                    }
                                } else {
                                    $agent_condition_model = new AgentCondition();
                                    $agent_condition_model->essay_id = $essay_id;

                                    $agent_condition_model->crop_id = $crop_id;
                                    $agent_condition_model->request_type_id =  $value_request_type;
                                    $agent_condition_model->composite_sample_type_id = $composite_sample_type_id;

                                    $agent_condition_model->request_process_essay_type_id = $request_process_essay_type_id;
                                    $agent_condition_model->agent_id = $agent_id;;
                                    $agent_condition_model->reprocess_flag = $reprocess_flag;
                                    $agent_condition_model->num_order = $num_order;
                                    $agent_condition_model->repetition = $repetition;
                                    $agent_condition_model->save();
                                }
                            }
                        } else {
                            if ($composite_sample_type_id == '*') {
                                $composite_sample_type_list = ArrayHelper::map(Parameter::find()->where(['status' => 'active', 'entity' => 'composite_sample', 'singularity' => 'type',])->andWhere(['<>', 'parameter_id', self::COMPOSITE_SAMPLE_TYPE_HEADER])->all(), 'parameter_id', 'parameter_id');
                                foreach ($composite_sample_type_list as $key_composite_sample_type => $value_composite_sample_type) {
                                    $agent_condition_model = new AgentCondition();
                                    $agent_condition_model->essay_id = $essay_id;

                                    $agent_condition_model->crop_id = $crop_id;
                                    $agent_condition_model->request_type_id =  $request_type_id;
                                    $agent_condition_model->composite_sample_type_id = $value_composite_sample_type;

                                    $agent_condition_model->request_process_essay_type_id = $request_process_essay_type_id;
                                    $agent_condition_model->agent_id = $agent_id;;
                                    $agent_condition_model->reprocess_flag = $reprocess_flag;
                                    $agent_condition_model->num_order = $num_order;
                                    $agent_condition_model->repetition = $repetition;
                                    $agent_condition_model->save();
                                }
                            } else {
                                $agent_condition_model = new AgentCondition();
                                $agent_condition_model->essay_id = $essay_id;

                                $agent_condition_model->crop_id = $crop_id;
                                $agent_condition_model->request_type_id =  $request_type_id;
                                $agent_condition_model->composite_sample_type_id = $composite_sample_type_id;

                                $agent_condition_model->request_process_essay_type_id = $request_process_essay_type_id;
                                $agent_condition_model->agent_id = $agent_id;;
                                $agent_condition_model->reprocess_flag = $reprocess_flag;
                                $agent_condition_model->num_order = $num_order;
                                $agent_condition_model->repetition = $repetition;
                                $agent_condition_model->save();
                            }
                        }
                    }
                }
                //-----------------------------------------------------------------------------------

            }

            return $this->redirect(['index-manage']);
        }

        return $this->render('index-manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}

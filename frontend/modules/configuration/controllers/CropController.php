<?php

namespace frontend\modules\configuration\controllers;

use Yii;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\CropSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use frontend\modules\functional\Functional;

/**
 * CropController implements the CRUD actions for Crop model.
 */
class CropController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Crop models.
     * @return mixed
     */
    public function actionIndex()
    {

        $is_disabled = true;
        $_user_role = Yii::$app->user->identity->role;
        if ($_user_role === Functional::SUPER_ADMIN_ROLE) {
            $is_disabled = false;
            $value_institution_id = Yii::$app->request->post('institution', 1);
        } else {
            $_user_institution = Yii::$app->user->Identity->institution;
            $value_institution_id = $_user_institution;
        }

        // about institution
        $data_institution = Yii::$app->db->createCommand(
            "SELECT institution_id,
                  short_name
            FROM   plims.bsns_institution
            WHERE  is_deleted = false
                  AND institution_id <> 1
            ORDER  BY short_name;"
        )->queryAll();
        $data_institution = ArrayHelper::map($data_institution, 'institution_id', 'short_name');
        $newItem = [1 => 'All'];
        $data_institution = $newItem + $data_institution;

        // about crop
        $_query = "SELECT t2.institution_id,
                        t2.short_name AS institution,
                        t1.crop_id,
                        t1.short_name,
                        t1.long_name,
                        t1.description
                    FROM   plims.bsns_crop t1
                        INNER JOIN plims.bsns_institution t2
                                ON t1.institution_id = t2.institution_id
                    WHERE  t1.status = :_status_active
                        AND t1.institution_id IS NOT NULL ";
        $_params = [];
        $_params[':_status_active'] =  Functional::STATUS_ACTIVE;

        if ($value_institution_id != null && $value_institution_id != 1) {
            $_query =  $_query . "AND  t1.institution_id = :_institution ";
            $_params[':_institution'] = $value_institution_id;
        }

        $_query =  $_query . "ORDER  BY 2, 4;";

        $data = Yii::$app->db->createCommand($_query, $_params)->queryAll();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'sort' => [
                'attributes' => [
                    'institution_id',
                    'institution',
                    'crop_id',
                    'short_name',
                    'long_name',
                    'description'
                ],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [

            'dataProvider' => $dataProvider,
            'data_institution' => $data_institution,
            'value_institution_id' => $value_institution_id,
            'is_disabled' => $is_disabled,
        ]);
    }

    /**
     * Displays a single Crop model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionNewCrop($institution_id, $institution)
    {
        $redirect = [
            'create',
            'institution_id' => $institution_id,
            'institution' => $institution,
        ];

        if ($institution_id == 1) {
            Yii::$app->session->setFlash('info', \Yii::t('app', 'First you must choose an institution.'));
            $redirect = ['index'];
        }

        return $this->redirect(
            $redirect,
        );
    }


    /**
     * Creates a new Crop model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($institution_id, $institution)
    {
        $model = new Crop();

        $data_form = [
            'short_name' => null,
            'long_name' => null,
            'description' => null,
            'request_prefix' => null,
            'institution_id' => $institution_id,
            'is_deleted' => 0
        ];

        // INSTITUTION
        $data_institution =  Yii::$app->db->createCommand(
            "SELECT institution_id,
              short_name AS institution_name
        FROM   plims.bsns_institution
        WHERE is_deleted = false
        AND institution_id <> 1
        ORDER  BY short_name;",
            []
        )->queryAll();
        $data_institution = ArrayHelper::map($data_institution, 'institution_id', 'institution_name');

        $model->institution_id =  $institution_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->crop_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'data_form' => $data_form,
            'data_institution' => $data_institution,
        ]);
    }

    /**
     * Updates an existing Crop model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->crop_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Crop model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Crop model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Crop the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Crop::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

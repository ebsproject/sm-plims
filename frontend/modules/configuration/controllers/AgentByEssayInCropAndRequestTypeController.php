<?php

namespace frontend\modules\configuration\controllers;

use Yii;
use frontend\modules\configuration\models\AgentByEssayInCropAndRequestType;
use frontend\modules\configuration\models\AgentByEssayInCropAndRequestTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use frontend\modules\configuration\models\Agent;
use frontend\modules\configuration\models\Essay;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Parameter;

class AgentByEssayInCropAndRequestTypeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new AgentByEssayInCropAndRequestTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($agent_id, $essay_id, $crop_id, $request_type_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($agent_id, $essay_id, $crop_id, $request_type_id),
        ]);
    }


    public function actionCreate()
    {
        $model = new AgentByEssayInCropAndRequestType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'agent_id' => $model->agent_id, 'essay_id' => $model->essay_id, 'crop_id' => $model->crop_id, 'request_type_id' => $model->request_type_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($agent_id, $essay_id, $crop_id, $request_type_id)
    {
        $model = $this->findModel($agent_id, $essay_id, $crop_id, $request_type_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'agent_id' => $model->agent_id, 'essay_id' => $model->essay_id, 'crop_id' => $model->crop_id, 'request_type_id' => $model->request_type_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionDelete($agent_id, $essay_id, $crop_id, $request_type_id)
    {
        $this->findModel($agent_id, $essay_id, $crop_id, $request_type_id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($agent_id, $essay_id, $crop_id, $request_type_id)
    {
        if (($model = AgentByEssayInCropAndRequestType::findOne(['agent_id' => $agent_id, 'essay_id' => $essay_id, 'crop_id' => $crop_id, 'request_type_id' => $request_type_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // -------------------------------------------------------------------------------------------------------------------------------------- //

    public function actionIndexManage()
    {

        $searchModel = new AgentByEssayInCropAndRequestTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $select_data_0 = null;
        $select_value_0 = null;
        $select_data_1 = null;
        $select_value_1 = null;
        $select_data_2 = null;
        $select_value_2 = null;
        $select_data_3 = null;
        $select_value_3 = null;


        $model =  new AgentByEssayInCropAndRequestType();
        $select_data_0 = $model->getParameter('request', 'type');


        $select_data_1 = ArrayHelper::map(Crop::find()->where(
            [
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'crop_id', 'short_name');


        $select_data_2 = ArrayHelper::map(Essay::find()->where(
            [
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'essay_id', 'short_name');


        $select_data_3 = ArrayHelper::map(Agent::find()->where(
            [
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'agent_id', 'short_name');


        $select_name_0 = Yii::$app->request->post('select_name_0');
        $select_name_1 = Yii::$app->request->post('select_name_1');
        $select_name_2 = Yii::$app->request->post('select_name_2');


        if (!empty($select_name_0) and !empty($select_name_1) and !empty($select_name_2)) {
            $select_value_0 = Yii::$app->request->post('select_name_0');
            $select_value_1 = Yii::$app->request->post('select_name_1');
            $select_value_2 =  Yii::$app->request->post('select_name_2');
            $select_value_3 = ArrayHelper::map(AgentByEssayInCropAndRequestType::find()->where([
                'request_type_id' =>  $select_value_0,
                'crop_id' =>  $select_value_1,
                'essay_id' =>  $select_name_2,
            ])->asArray()->all(), 'agent_id', 'agent_id');
        } else if (!empty($select_name_0) and !empty($select_name_1)) {
            $select_value_0 = Yii::$app->request->post('select_name_0');
            $select_value_1 = Yii::$app->request->post('select_name_1');
        } else if (!empty($select_name_0)) {
            $select_value_0 = Yii::$app->request->post('select_name_0');
        }


        return $this->render('index-manage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

            'select_data_0' => $select_data_0,
            'select_value_0' => $select_value_0,
            'select_data_1' => $select_data_1,
            'select_value_1' => $select_value_1,
            'select_data_2' => $select_data_2,
            'select_value_2' => $select_value_2,
            'select_data_3' => $select_data_3,
            'select_value_3' => $select_value_3
        ]);
    }

    public function actionEnable($agent_id, $essay_id, $crop_id, $request_type_id, $order)
    {
        $model = $this->findModel($agent_id, $essay_id, $crop_id, $request_type_id);
        $model->status = 'active';
        $model->num_order = $order;
        $model->deleted_at = null;
        $model->deleted_by = null;
        $model->save();
    }

    public function actionDisabled($agent_id, $essay_id, $crop_id, $request_type_id)
    {
        $model = $this->findModel($agent_id, $essay_id, $crop_id, $request_type_id);
        $model->status = 'disabled';
        $model->deleted_at = date("Y-m-d H:i:s", time());
        $model->deleted_by = Yii::$app->user->identity->id;
        $model->save();
    }

    public function actionAddRemoveAgentByEssayInCrop()
    {
        $select_name_0 = Yii::$app->request->post('select_name_0');
        $select_name_1 = Yii::$app->request->post('select_name_1');
        $select_name_2 = Yii::$app->request->post('select_name_2');
        $select_name_3 = Yii::$app->request->post('select_name_3');
        if (!empty($select_name_0) and !empty($select_name_1) and !empty($select_name_2) and !empty($select_name_3)) {
            $select_value_0 = Yii::$app->request->post('select_name_0');
            $select_value_1 = Yii::$app->request->post('select_name_1');
            $select_value_2 = Yii::$app->request->post('select_name_2');
            $select_value_3 = Yii::$app->request->post('select_name_3');

            $select_value_3_old = ArrayHelper::map(
                AgentByEssayInCropAndRequestType::find()->where([
                    'request_type_id' => $select_value_0,
                    'crop_id' => $select_value_1,
                    'essay_id' => $select_value_2,
                ])->all(),
                'agent_id',
                'agent_id'
            );
            foreach ($select_value_3_old as $key => $value) {
                $this->actionDisabled($value, $select_value_2, $select_value_1, $select_value_0);
            }
            $order = 1;
            foreach ($select_value_3 as $key => $value) {
                if (in_array($value, $select_value_3_old)) {
                    $this->actionEnable($value, $select_value_2, $select_value_1, $select_value_0, $order);
                } else {
                    $model = new AgentByEssayInCropAndRequestType();
                    $model->request_type_id = $select_value_0;
                    $model->crop_id = $select_value_1;
                    $model->essay_id = $select_value_2;
                    $model->agent_id = $value;
                    $model->num_order = $order;
                    $model->save();
                }
                $order =  $order + 1;
            }
        }
        return $this->redirect(['index-manage']);
    }

    // -------------------------------------------------------------------------------------------------------------------------------------- //
}

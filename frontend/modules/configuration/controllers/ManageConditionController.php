<?php

namespace frontend\modules\configuration\controllers;


use Yii;
use yii\web\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use frontend\modules\functional\Functional;

class ManageConditionController extends Controller
{

  const STATUS_ACTIVE = 'active';
  const STATUS_DISABLED = 'disabled';
  const COMPOSITE_SAMPLE_TYPE_HEADER = 32;
  const COMPOSITE_SAMPLE_GENERAL = 30;
  const COMPOSITE_SAMPLE_SIMPLE = 31;
  const COMPOSITE_SAMPLE_INDIVIDUAL = 29;

  const INSTITUTION_TOKEN = 1;
  const LABORATORY_TOKEN = 1;

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  // INDEX
  public function actionIndex()
  {
    $_user_institution = Yii::$app->user->identity->institution;
    $_user_laboratory = Yii::$app->user->Identity->laboratory;

    $data = Yii::$app->db->createCommand(
      "SELECT     
        c1.short_name                                               AS crop_name,
        e1.short_name                                               AS essay,
        p1.short_name                                               AS request_type,
        '<i class='''||p2.class_details||'''></i> '|| p2.short_name AS composite_sample_type,
        p2.class_details                                            AS composite_sample_type_class,
        p3.short_name                                               AS essay_type,
        CASE
          WHEN t1.is_reprocessed THEN '<i class=''fa fa-check-square-o''></i>'
          ELSE '<i class=''fa fa-square-o''></i>'
        END                                                         AS is_reprocessed,
        t1.repetition,
        STRING_AGG(t1.num_order|| ': '|| a1.short_name, '\r\n' 
          ORDER BY t1.num_order)                                    AS agent_names,
        COUNT(a1.agent_id)                                          AS agent_count,
        CASE 
          WHEN t1.code IS NULL THEN '---' 
          ELSE t1.code 
        END                                                         AS code,
        a2.username                                                 AS user
      FROM       plims.bsns_agent_condition t1
        INNER JOIN plims.bsns_crop c1
        ON         t1.crop_id = c1.crop_id
        INNER JOIN plims.bsns_essay e1
        ON         t1.essay_id = e1.essay_id
        INNER JOIN plims.bsns_parameter p1
        ON         t1.request_type_id = p1.parameter_id
        INNER JOIN plims.bsns_parameter p2
        ON         t1.composite_sample_type_id = p2.parameter_id
        INNER JOIN plims.bsns_parameter p3
        ON         t1.request_process_essay_type_id = p3.parameter_id
        INNER JOIN plims.bsns_agent a1
        ON         t1.agent_id = a1.agent_id
        INNER JOIN plims.auth_user a2 
        ON t1.registered_by = a2.user_id
      WHERE      t1.status = :_status_active
        AND        (t1.essay_id = :essay_id OR :essay_id IS NULL)
        AND        t1.laboratory_id = :_laboratory_id
      GROUP BY   c1.short_name,
            e1.short_name,
            p1.short_name,
            p2.short_name,
            p2.code,
            p2.class_details,
            p3.short_name,
            t1.is_reprocessed,
            t1.repetition,
            t1.code,
            a2.username 
      ORDER BY  
            e1.short_name, --ASSAY
            c1.short_name, --CROP
            p1.short_name, --REQUEST TYPE
            
            p2.code, --COMPOSITE SAMPLE
            p3.short_name; --SPECIAL PROPERTY",
      [
        ':_status_active' => self::STATUS_ACTIVE,
        ':essay_id' => null,
        ':_laboratory_id' => $_user_laboratory,
      ]
    )->queryAll();

    $conditionDataProvider = new ArrayDataProvider([
      'allModels' => $data,
      'sort' => [
        'attributes' => [
          'code',
          'crop_name',
          'essay',
          'request_type',
          'composite_sample_type',
          'essay_type',
          'repetition',
          'is_reprocessed',
          'agent_count',
          'user'
        ],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    $dataNew = [];



    foreach ($data as $key => $value) {
      $dataNew[$value['essay']][$value['essay']][$value['crop_name']][$value['request_type']][$value['composite_sample_type']][$value['code']] =
        [
          'crop_name' => $value['crop_name'],
          'essay' => $value['essay'],
          'request_type' => $value['request_type'],
          'composite_sample_type' => $value['composite_sample_type'],
          'composite_sample_type_class' => $value['composite_sample_type_class'],
          'essay_type' => $value['essay_type'],
          'is_reprocessed' => $value['is_reprocessed'],
          'repetition' => $value['repetition'],
          'agent_names' => $value['agent_names'],
          'agent_count' => $value['agent_count'],
          'code' => $value['code'],
          'user' => $value['user']
        ];
    }

    $conditionDataProviderNew = new ArrayDataProvider([
      'allModels' => $dataNew,
      'pagination' => [
        'pageSize' => 20,
      ],
    ]);

    // crop_selected
    $crop_selected = Yii::$app->db->createCommand(
      "SELECT crop_id    AS id,
            short_name AS name
      FROM   plims.bsns_crop
      WHERE  institution_id = :_institution_id ",
      [
        ':_institution_id' => $_user_institution,
      ]
    )->queryAll();
    $crop_selected =  ArrayHelper::map($crop_selected, 'id', 'name');

    // assay_crop_selected
    $assay_crop_selected = Yii::$app->db->createCommand(
      "SELECT t1.essay_id||'_'||t1.crop_id as id,
              t2.short_name || ' - [' || t3.short_name || ']' as name
        FROM   (SELECT tab1.crop_id,
                      tab1.essay_id
              FROM   (SELECT DISTINCT crop_id,
                                      essay_id,
                                      laboratory_id
                      FROM   plims.bsns_agent_condition
                      WHERE  laboratory_id = :_laboratory_id_token) tab1
                      LEFT JOIN (SELECT DISTINCT crop_id,
                                                essay_id,
                                                laboratory_id
                                FROM   plims.bsns_agent_condition
                                WHERE  laboratory_id = :_laboratory_id) tab2
                            ON tab1.essay_id = tab2.essay_id
                                AND tab1.crop_id = tab2.crop_id
              WHERE  tab2.essay_id IS NULL
                      AND tab2.crop_id IS NULL) t1
              LEFT JOIN plims.bsns_essay t2
                    ON t1.essay_id = t2.essay_id
              LEFT JOIN plims.bsns_crop t3
                    ON t1.crop_id = t3.crop_id
        ORDER  BY name;",
      [
        ':_laboratory_id_token' => self::LABORATORY_TOKEN,
        ':_laboratory_id' => $_user_laboratory,
      ]
    )->queryAll();
    $assay_crop_selected =  ArrayHelper::map($assay_crop_selected, 'id', 'name');


    return $this->render('index', [
      'conditionDataProvider' => $conditionDataProvider,
      'conditionDataProviderNew' => $conditionDataProviderNew,
      'assay_crop_selected' => $assay_crop_selected,
      'crop_selected' => $crop_selected
    ]);
  }

  public function actionCloneCondition()
  {
    if (empty($this->request->post('assay_crop_selected'))) {
      # code...
    } else {

      if (empty($this->request->post('crop_selected'))) {
        # code...
      } else {
        $_crop_selected_new = $this->request->post('crop_selected');
        $_assay_crop_selected = $this->request->post('assay_crop_selected');
        $_crop_id = explode('_', $_assay_crop_selected)[1];
        $_assay_id = explode('_', $_assay_crop_selected)[0];
        $_user_laboratory = Yii::$app->user->Identity->laboratory;
        try {
          $db = Yii::$app->db;
          $transaction = $db->beginTransaction();
          $db->createCommand(
            "INSERT INTO plims.bsns_agent_condition (num_order, essay_id, crop_id, request_type_id, composite_sample_type_id, request_process_essay_type_id, agent_id, reprocess_flag, registered_by, registered_at, updated_by, updated_at, deleted_by, deleted_at, status, repetition, code, serial, is_reprocessed, laboratory_id)
            SELECT num_order, essay_id, :_crop_selected_new as crop_id, request_type_id, composite_sample_type_id, request_process_essay_type_id, agent_id, reprocess_flag, registered_by, registered_at, updated_by, updated_at, deleted_by, deleted_at, status, repetition, 'L'|| (:_laboratory_id::TEXT) || code as code, serial, is_reprocessed, :_laboratory_id::INTEGER AS laboratory_id
            FROM plims.bsns_agent_condition
            WHERE laboratory_id = :_laboratory_id_token AND crop_id = :_crop_id AND essay_id = :_assay_id; ",
            [
              ':_laboratory_id' => $_user_laboratory,
              ':_laboratory_id_token' => self::LABORATORY_TOKEN,
              ':_crop_id' => $_crop_id,
              ':_assay_id' => $_assay_id,
              ':_crop_selected_new' => $_crop_selected_new,
            ]
          )->execute();
          $transaction->commit();
        } catch (\Throwable $e) {
          $transaction->rollBack();
          if ($e instanceof \yii\db\IntegrityException) {
            Yii::$app->session->setFlash('error', 'You cannot create or clone this set of conditions, as a set with the same properties already exists or uses the same crop and scientific assay.');
          } else {
            throw $e;
          }
        }
      }
    }

    return $this->redirect(
      [
        'index'
      ]
    );
  }

  public function actionRemoveCondition($code)
  {
    Yii::$app->db->createCommand()
      ->delete(
        'plims.bsns_agent_condition',
        'code = :code',
        [
          ':code' => $code
        ]
      )->execute();
    return $this->redirect(
      [
        'index'
      ]
    );
  }

  public function actionEditCondition($code)
  {
    $session = Yii::$app->session;
    unset($session['_agent_selected']);

    $data_row = Yii::$app->db->createCommand(
      "SELECT crop_id                       AS crop,
            essay_id                      AS assay,
            request_type_id               AS request_type,
            composite_sample_type_id      AS sample_type,
            request_process_essay_type_id AS special_property,
            repetition                    AS repetitions,
            is_reprocessed                AS is_reprocessed
      FROM   plims.bsns_agent_condition
      WHERE  code = :code
      LIMIT  1; ",
      [
        ':code' => $code
      ]
    )->queryOne();

    $crop = $data_row['crop'];
    $assay = $data_row['assay'];
    $request_type = $data_row['request_type'];
    $sample_type = $data_row['sample_type'];
    $special_property = $data_row['special_property'];
    $repetitions = $data_row['repetitions'];
    $is_reprocessed = $data_row['is_reprocessed'] ? 'on' : null;

    return $this->redirect(
      [
        'management',
        'code' =>  $code,
        'crop' =>  $crop,
        'assay' =>  $assay,
        'request_type' =>  $request_type,
        'sample_type' =>  $sample_type,
        'special_property' => $special_property,
        'repetitions' => $repetitions,
        'is_reprocessed' =>  $is_reprocessed,
      ]
    );
  }

  public function actionNewCondition()
  {
    $session = Yii::$app->session;
    unset($session['_agent_selected']);
    return $this->redirect(
      [
        'management',
      ]
    );
  }
  // MANAGEMENT
  public function actionManagement(
    $code = null,
    $crop = null,
    $assay = null,
    $request_type = null,
    $sample_type = null,
    $special_property = null,
    $repetitions = null,
    $is_reprocessed = null
  ) {
    if (is_null($code)) {
      $agent_selected_aux = [];
    } else {
      $agent_selected_aux = Yii::$app->db->createCommand(
        "SELECT t1.agent_id || '|' || a1.short_name AS agent
        FROM   plims.bsns_agent_condition t1
               INNER JOIN plims.bsns_agent a1
                       ON t1.agent_id = a1.agent_id
        WHERE  t1.code = :code
               AND t1.status = :_status_active
        ORDER  BY t1.num_order;",
        [
          ':code' => $code,
          ':_status_active' => self::STATUS_ACTIVE
        ]
      )->queryColumn();
    }

    $session = Yii::$app->session;
    $agent_selected = empty($session['_agent_selected']) ?  $agent_selected_aux : $session['_agent_selected'];
    $agent_selected_list = [];

    foreach ($agent_selected as $key => $value) {
      $agent_selected_list[] =
        [
          'id' => explode('|', $value)[0],
          'order' => $key + 1,
          'name' => explode('|', $value)[1],
        ];
    }

    $rows = count($agent_selected) - 1;
    $data_provider = new ArrayDataProvider([
      'allModels' => $agent_selected_list,
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    $dataSelect = $this->getDataSelect();
    $data_crop = $dataSelect['data_crop'];
    $data_assay = $dataSelect['data_assay'];
    $data_request_type = $dataSelect['data_request_type'];
    $data_sample_type = $dataSelect['data_sample_type'];
    $data_special_property = $dataSelect['data_special_property'];
    $data_agent = $dataSelect['data_agent'];

    $disabled_assays_array = [
      Functional::ESSAY_GERMINATION,
      Functional::ESSAY_BLOTTER,
      Functional::ESSAY_ELISA
    ];

    $disabled_assays = json_encode($disabled_assays_array);

    return $this->render('management', [
      'data_crop' => $data_crop,
      'data_assay' => $data_assay,
      'data_request_type' => $data_request_type,
      'data_sample_type' => $data_sample_type,
      'data_special_property' => $data_special_property,
      'data_agent' => $data_agent,
      'agent_selected' => $agent_selected,
      'rows' => $rows,
      'dataProvider' => $data_provider,
      'code' => $code,
      'disabled_assays' => $disabled_assays,
      'disabled_assays_array' => $disabled_assays_array
    ]);
  }


  public function actionAddAgent(
    $code = null,
    $crop = null,
    $assay = null,
    $request_type = null,
    $sample_type = null,
    $special_property = null
  ) {
    $agent_selected =   $this->request->post('agent_selected');
    $session = Yii::$app->session;
    $session['_agent_selected'] = $agent_selected;

    if (is_null($code)) {
      $crop = $this->request->post('crop');
      $assay =  $this->request->post('assay');
      $request_type =  $this->request->post('request_type');
      $sample_type =  $this->request->post('sample_type');
      $special_property = (empty($this->request->post('special_property'))) ? 1 : $this->request->post('special_property');
    } else {
      $special_property = is_null($special_property) ? 1 : $special_property;
    }

    return $this->redirect(
      [
        'management',
        'code' =>  $code,
        'crop' =>  $crop,
        'assay' =>  $assay,
        'request_type' =>  $request_type,
        'sample_type' =>  $sample_type,
        'special_property' => $special_property,
        'repetitions' => $this->request->post('repetitions'),
        'is_reprocessed' => $this->request->post('is_reprocessed'),
      ]
    );
  }
  public function actionRemoveAgent(
    $key,
    $code,

    $crop = null,
    $assay = null,
    $request_type = null,
    $sample_type = null,
    $special_property = null
  ) {
    $agent_selected =   $this->request->post('agent_selected');
    $this->removeElement($agent_selected, $key);
    $session = Yii::$app->session;
    $session['_agent_selected'] = $agent_selected;

    if (is_null($code)) {
      $crop = $this->request->post('crop');
      $assay =  $this->request->post('assay');
      $request_type =  $this->request->post('request_type');
      $sample_type =  $this->request->post('sample_type');
      $special_property = (empty($this->request->post('special_property'))) ? 1 : $this->request->post('special_property');
    } else {
      $special_property = is_null($special_property) ? 1 : $special_property;
    }

    return $this->redirect(
      [
        'management',
        'code' =>  $code,
        'crop' =>  $crop,
        'assay' =>  $assay,
        'request_type' =>   $request_type,
        'sample_type' =>  $sample_type,
        'special_property' =>  $special_property,
        'repetitions' => $this->request->post('repetitions'),
        'is_reprocessed' =>  $this->request->post('is_reprocessed'),
      ]
    );
  }
  public function actionUp(
    $key,
    $code = null,

    $crop = null,
    $assay = null,
    $request_type = null,
    $sample_type = null,
    $special_property = null
  ) {
    $agent_selected =   $this->request->post('agent_selected');
    $this->moveElement($agent_selected, $key, $key - 1);
    $session = Yii::$app->session;
    $session['_agent_selected'] = $agent_selected;

    if (is_null($code)) {
      $crop = $this->request->post('crop');
      $assay =  $this->request->post('assay');
      $request_type =  $this->request->post('request_type');
      $sample_type =  $this->request->post('sample_type');
      $special_property = (empty($this->request->post('special_property'))) ? 1 : $this->request->post('special_property');
    } else {
      $special_property = is_null($special_property) ? 1 : $special_property;
    }

    return $this->redirect(
      [
        'management',
        'code' =>  $code,
        'crop' =>  $crop,
        'assay' =>  $assay,
        'request_type' =>   $request_type,
        'sample_type' =>  $sample_type,
        'special_property' =>  $special_property,
        'repetitions' => $this->request->post('repetitions'),
        'is_reprocessed' =>  $this->request->post('is_reprocessed'),
      ]
    );
  }
  public function actionDown(
    $key,
    $code = null,

    $crop = null,
    $assay = null,
    $request_type = null,
    $sample_type = null,
    $special_property = null
  ) {
    $agent_selected =   $this->request->post('agent_selected');
    $this->moveElement($agent_selected, $key, $key + 1);
    $session = Yii::$app->session;
    $session['_agent_selected'] = $agent_selected;

    if (is_null($code)) {
      $crop = $this->request->post('crop');
      $assay =  $this->request->post('assay');
      $request_type =  $this->request->post('request_type');
      $sample_type =  $this->request->post('sample_type');
      $special_property = (empty($this->request->post('special_property'))) ? 1 : $this->request->post('special_property');
    } else {
      $special_property = is_null($special_property) ? 1 : $special_property;
    }

    return $this->redirect(
      [
        'management',
        'code' =>  $code,
        'crop' =>  $crop,
        'assay' =>  $assay,
        'request_type' =>   $request_type,
        'sample_type' =>  $sample_type,
        'special_property' =>  $special_property,
        'repetitions' => $this->request->post('repetitions'),
        'is_reprocessed' =>  $this->request->post('is_reprocessed'),
      ]
    );
  }


  public function actionCancel($code = null)
  {
    $session = Yii::$app->session;
    unset($session['_agent_selected']);

    return $this->redirect(
      [
        is_null($code) ? 'management' : 'edit-condition',
        'code' =>  $code,
      ]
    );
  }
  public function actionSave(
    $code = null,
    $crop = null,
    $assay = null,
    $request_type = null,
    $sample_type = null,
    $special_property = null
  ) {
    $user = Yii::$app->user->identity->id;
    $_user_laboratory = Yii::$app->user->Identity->laboratory;
    $date = date("Y-m-d H:i:s", time());
    $session = Yii::$app->session;
    $action = '';

    $redirect = [
      'index',
    ];

    try {
      if (
        (empty($session['_agent_selected']) ||
          is_null($crop) ||
          is_null($assay) ||
          is_null($request_type) ||
          is_null($sample_type)
        )
        &&
        is_null($code)
      ) {
        $redirect = [
          'management',
          'code' =>  $this->request->post('code'),
          'crop' =>  $this->request->post('crop'),
          'assay' =>  $this->request->post('assay'),
          'request_type' =>  $this->request->post('request_type'),
          'sample_type' =>  $this->request->post('sample_type'),
          'special_property' => $this->request->post('special_property'),
          'repetitions' => $this->request->post('repetitions'),
          'is_reprocessed' =>  $this->request->post('is_reprocessed'),
        ];
      } else {
        // existing conditions are eliminated with the same code.
        Yii::$app->db->createCommand()->delete(
          'plims.bsns_agent_condition',
          'code = :code',
          [':code' => $code]
        )->execute();

        if (is_null($code)) {
          // IS NEW CONDITION
          $crop = $this->request->post('crop');
          $assay =  $this->request->post('assay');
          $request_type =  $this->request->post('request_type');
          $sample_type =  $this->request->post('sample_type');
          $special_property =   $this->request->post('special_property', Functional::PARAMETER_UNDETERMINED);

          $base_code =  'L' . $_user_laboratory . 'ACC' . $crop . $assay . $request_type . $sample_type . $special_property . '-';

          $serial = Yii::$app->db->createCommand(
            "SELECT COALESCE(Max(serial), 0) + 1  AS serial
          FROM   plims.bsns_agent_condition
          WHERE  code LIKE :base_code || '%';",
            [
              ':base_code' => $base_code
            ]
          )->queryScalar();
          $code = $base_code . $serial;
          $action = 'create a new condition: <b>' . $code . '</b>';
        } else {
          // IS UPDATED
          $special_property = is_null($special_property) ? Functional::PARAMETER_UNDETERMINED : $special_property;
          $serial = explode('-', $code)[1];
          $action = 'update: <b>' . $code . '</b>';
        }

        // secondary values ​​are defined
        $repetitions = $this->request->post('repetitions');
        $is_reprocessed =  empty($this->request->post('is_reprocessed')) ? false : true;

        if (empty($session['_agent_selected'])) {
          $agent_selected =  $this->request->post('agent_selected');
        } else {
          $agent_selected =  $session['_agent_selected'];
        }

        // our list of conditions is created and saved
        $agent_condition_list = [];
        foreach ($agent_selected as $key => $value) {
          $agent_condition_list[] = [
            $key + 1,
            $assay,
            $crop,
            $request_type,
            $sample_type,
            $special_property,
            explode('|', $value)[0],
            $is_reprocessed,
            $user,
            $date,
            self::STATUS_ACTIVE,
            $repetitions,
            $code,
            $serial,
            $_user_laboratory
          ];
        }

        Yii::$app->db->createCommand()->batchInsert(
          'plims.bsns_agent_condition',
          [
            'num_order',
            'essay_id',
            'crop_id',
            'request_type_id',
            'composite_sample_type_id',
            'request_process_essay_type_id',
            'agent_id',
            'is_reprocessed',
            'registered_by',
            'registered_at',
            'status',
            'repetition',
            'code',
            'serial',
            'laboratory_id'
          ],
          $agent_condition_list
        )->execute();
        // the session is cleaned and we register the redaction path
        unset($session['_agent_selected']);

        Yii::$app->session->setFlash('info', \Yii::t('app', 'You have successfully ' . $action));
      }
    } catch (\Throwable $th) {
      if ($th instanceof \yii\db\IntegrityException) {
        Yii::$app->session->setFlash('error', 'You must enter a value in all fields to create a new condition.');
      } else {
        throw $th;
      }
    }
    return $this->redirect(
      $redirect
    );
  }

  // FUNCTIONS
  private function getDataSelect()
  {
    $_user_institution = Yii::$app->user->Identity->institution;

    $data_crop = Yii::$app->db->createCommand(
      "SELECT crop_id, short_name  
      FROM plims.bsns_crop 
      WHERE status = :_status_active
      AND institution_id = :_institution_id
      ORDER BY short_name;",
      [
        ':_status_active' => self::STATUS_ACTIVE,
        ':_institution_id' => $_user_institution
      ]
    )->queryAll();
    $data_crop =  ArrayHelper::map($data_crop, 'crop_id', 'short_name');

    // todos los ensayos
    $data_assay = Yii::$app->db->createCommand(
      "SELECT essay_id, short_name
      FROM plims.bsns_essay
      WHERE status = :_status_active
      ORDER BY short_name;",
      [
        ':_status_active' => self::STATUS_ACTIVE
      ]
    )->queryAll();
    $data_assay =  ArrayHelper::map($data_assay, 'essay_id', 'short_name');

    // todos los tipos de solicitudes // REQUEST TYPE ===> INSTITUCION
    $data_request_type = Yii::$app->db->createCommand(
      "SELECT parameter_id,
            short_name
      FROM   plims.bsns_parameter
      WHERE  entity = 'request'
            AND singularity = 'type'
            AND status = :_status_active
            -- AND institution_id = :_institution_id
      ORDER  BY short_name;",
      [
        ':_status_active' => self::STATUS_ACTIVE,
        // ':_institution_id' => $_institution_id
      ]
    )->queryAll();
    $data_request_type =  ArrayHelper::map($data_request_type, 'parameter_id', 'short_name');

    // todos los tipos de muestras
    $data_sample_type = Yii::$app->db->createCommand(
      "SELECT parameter_id,
            short_name
      FROM   plims.bsns_parameter
      WHERE  entity = 'composite_sample'
            AND singularity = 'type'
            AND status = :_status_active
            AND parameter_id <> :composite_sample_header
      ORDER  BY code;",
      [
        ':_status_active' => self::STATUS_ACTIVE,
        ':composite_sample_header' => self::COMPOSITE_SAMPLE_TYPE_HEADER,
      ]
    )->queryAll();
    $data_sample_type =  ArrayHelper::map($data_sample_type, 'parameter_id', 'short_name');

    // solo las propiedades especiales de la institucion // REQUEST PROCESS ASSAY TYPE ===> INSTITUCION
    $data_special_property = Yii::$app->db->createCommand(
      "SELECT parameter_id,
            short_name
      FROM   plims.bsns_parameter
      WHERE  entity = 'request_process_essay'
            AND singularity = 'type'
            AND status = :_status_active
      ORDER  BY code;",
      [
        ':_status_active' => self::STATUS_ACTIVE,
      ]
    )->queryAll();
    $data_special_property =  ArrayHelper::map($data_special_property, 'parameter_id', 'short_name');
    $item_special_property = [Functional::PARAMETER_UNDETERMINED => 'Undetermined'];
    $data_special_property = $item_special_property + $data_special_property;

    // todos los agentes
    $data_agent = Yii::$app->db->createCommand(
      "SELECT agent_id || '|' || short_name as agent_id, short_name  
      FROM plims.bsns_agent
      WHERE status = :_status_active
      ORDER BY short_name;",
      [':_status_active' => self::STATUS_ACTIVE]
    )->queryAll();
    $data_agent =  ArrayHelper::map($data_agent, 'agent_id', 'short_name');

    return [
      'data_crop' => $data_crop,
      'data_assay' => $data_assay,
      'data_request_type' => $data_request_type,
      'data_sample_type' => $data_sample_type,
      'data_special_property' => $data_special_property,
      'data_agent' => $data_agent
    ];
  }
  private function moveElement(&$array, $a, $b)
  {
    $out = array_splice($array, $a, 1);
    array_splice($array, $b, 0, $out);
  }
  private function removeElement(&$array, $a)
  {
    array_splice($array, $a, 1);
  }
}

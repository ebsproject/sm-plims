<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\AgentByEssayInCrop;

/**
 * AgentByEssayInCropSearch represents the model behind the search form of `frontend\modules\configuration\models\AgentByEssayInCrop`.
 */
class AgentByEssayInCropSearch extends AgentByEssayInCrop
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_order', 'agent_id', 'essay_id', 'crop_id', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgentByEssayInCrop::find();
        $query->andWhere(
            [
                'bsns_agent_by_essay_in_crop.deleted_by' => null,
                'bsns_agent_by_essay_in_crop.deleted_at' => null,
            ]
        );

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>
                [
                    'crop_id' => SORT_ASC,
                    'essay_id' => SORT_ASC,
                    'num_order' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }


        $query->joinWith('agent');
        $query->joinWith('essay');
        $query->joinWith('crop');

        $query->andFilterWhere([
            'agent_id' => $this->agent_id,
            'essay_id' => $this->essay_id,
            'crop_id' => $this->crop_id,
            'num_order' => $this->num_order,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

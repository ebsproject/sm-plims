<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

class AgentByEssayInCropAndRequestType extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'bsns_agent_by_essay_in_crop_and_request_type';
    }


    public function rules()
    {
        return [
            [['agent_id', 'essay_id', 'crop_id', 'request_type_id'], 'required'],
            [['agent_id', 'essay_id', 'crop_id', 'num_order', 'registered_by', 'updated_by', 'deleted_by', 'request_type_id'], 'default', 'value' => null],
            [['agent_id', 'essay_id', 'crop_id', 'num_order', 'registered_by', 'updated_by', 'deleted_by', 'request_type_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['status'], 'string', 'max' => 15],
            [['agent_id', 'essay_id', 'crop_id', 'request_type_id'], 'unique', 'targetAttribute' => ['agent_id', 'essay_id', 'crop_id', 'request_type_id']],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agent::class, 'targetAttribute' => ['agent_id' => 'agent_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['request_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_type_id' => 'parameter_id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'agent_id' => 'Agent ID',
            'essay_id' => 'Essay ID',
            'crop_id' => 'Crop ID',
            'num_order' => 'Num Order',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'request_type_id' => 'Request Type ID',
        ];
    }


    public function getAgent()
    {
        return $this->hasOne(Agent::class, ['agent_id' => 'agent_id']);
    }


    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }


    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }


    public function getRequestType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_type_id']);
    }


    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }


    public function getParameter($entity, $singularity)
    {

        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

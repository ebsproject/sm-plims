<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%activity}}".
 *
 * @property int $activity_id
 * @property string $short_name
 * @property string $long_name
 * @property string $description
 * @property string $details
 * @property int $registered_by
 * @property string $registered_at
 * @property int $updated_by
 * @property string $updated_at
 * @property int $deleted_by
 * @property string $deleted_at
 * @property string $status
 * @property int $activity_type_id
 * @property int $activity_datasource_id
 * @property int $activity_property_id
 *
 * @property Parameter $activityType
 * @property Parameter $activityDatasource
 * @property Parameter $activityProperty
 * @property ActivityByEssay[] $activityByEssays
 * @property Essay[] $essays
 * @property ReadingData[] $readingDatas
 * @property RequestProcessEssayActivity[] $requestProcessEssayActivities
 * @property RequestProcessEssay[] $requests
 * @property Support[] $supports
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.bsns_activity}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registered_by', 'updated_by', 'deleted_by', 'activity_type_id', 'activity_datasource_id', 'activity_property_id', 'activity_group_id'], 'default', 'value' => null],
            [['registered_by', 'updated_by', 'deleted_by', 'activity_type_id', 'activity_datasource_id', 'activity_property_id', 'activity_group_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['short_name'], 'string', 'max' => 45],
            [['long_name'], 'string', 'max' => 145],
            [['description', 'details'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 15],
            [['activity_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['activity_type_id' => 'parameter_id']],
            [['activity_datasource_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['activity_datasource_id' => 'parameter_id']],
            [['activity_property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['activity_property_id' => 'parameter_id']],
            [['activity_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['activity_group_id' => 'parameter_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'activity_id' => 'Activity ID',
            'short_name' => 'Short Name',
            'long_name' => 'Long Name',
            'description' => 'Description',
            'details' => 'Details',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'activity_type_id' => 'Activity Type',
            'activity_datasource_id' => 'Activity Datasource',
            'activity_property_id' => 'Activity Type Result',
            'activity_group_id' => 'Activity Group',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'activity_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityDatasource()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'activity_datasource_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityProperty()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'activity_property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityGroup()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'activity_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivityByEssays()
    {
        return $this->hasMany(ActivityByEssay::class, ['activity_id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(Essay::class, ['essay_id' => 'essay_id'])->viaTable('{{%activity_by_essay}}', ['activity_id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['activity_id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivities()
    {
        return $this->hasMany(RequestProcessEssayActivity::class, ['activity_id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(RequestProcessEssay::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id', 'essay_id' => 'essay_id'])->viaTable('{{%request_process_essay_activity}}', ['activity_id' => 'activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::class, ['activity_id' => 'activity_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Parameter;

class ParameterSearch extends Parameter
{
    public function rules()
    {
        return [
            [['parameter_id', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['code', 'singularity', 'entity', 'short_name', 'long_name', 'description', 'class_details', 'registered_at', 'updated_at', 'deleted_at', 'status'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Parameter::find();
        $query->andWhere(
            [
                'plims.bsns_parameter.deleted_by' => null,
                'plims.bsns_parameter.deleted_at' => null
            ]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>
                [
                    'entity' => SORT_ASC,
                    'singularity' => SORT_ASC,
                    'code' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'parameter_id' => $this->parameter_id,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'singularity', $this->singularity])
            ->andFilterWhere(['like', 'entity', $this->entity])
            ->andFilterWhere(['like', 'short_name', $this->short_name])
            ->andFilterWhere(['like', 'long_name', $this->long_name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'class_details', $this->class_details])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

class Workflow extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.bsns_workflow}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registered_by', 'updated_by', 'deleted_by', 'repetition'], 'default', 'value' => null],
            [['registered_by', 'updated_by', 'deleted_by', 'repetition'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['short_name'], 'string', 'max' => 45],
            [['long_name'], 'string', 'max' => 145],
            [['description', 'details'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'workflow_id' => 'Workflow ID',
            'short_name' => 'Short Name',
            'long_name' => 'Long Name',
            'description' => 'Description',
            'details' => 'Details',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'repetition' => 'Repetition',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssayByWorkflows()
    {
        return $this->hasMany(EssayByWorkflow::class, ['workflow_id' => 'workflow_id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(Essay::class, ['essay_id' => 'essay_id'])->viaTable('{{%essay_by_workflow}}', ['workflow_id' => 'workflow_id', 'status' => 'status'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['workflow_id' => 'workflow_id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcesses()
    {
        return $this->hasMany(RequestProcess::class, ['workflow_id' => 'workflow_id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssays()
    {
        return $this->hasMany(RequestProcessEssay::class, ['workflow_id' => 'workflow_id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivities()
    {
        return $this->hasMany(RequestProcessEssayActivity::class, ['workflow_id' => 'workflow_id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayAgents()
    {
        return $this->hasMany(RequestProcessEssayAgent::class, ['workflow_id' => 'workflow_id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::class, ['workflow_id' => 'workflow_id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflowByCrops()
    {
        return $this->hasMany(WorkflowByCrop::class, ['workflow_id' => 'workflow_id'])->andWhere(['status' => 'active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrops()
    {
        return $this->hasMany(Crop::class, ['crop_id' => 'crop_id'])->viaTable('{{%workflow_by_crop}}', ['workflow_id' => 'workflow_id', 'status' => 'status'])->andWhere(['status' => 'active']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}

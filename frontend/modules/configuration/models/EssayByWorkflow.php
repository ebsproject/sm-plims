<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

class EssayByWorkflow extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{plims.bsns_essay_by_workflow}}';
    }

    public function rules()
    {
        return [
            [['essay_id', 'workflow_id'], 'required'],
            [['essay_id', 'workflow_id', 'num_order', 'registered_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['essay_id', 'workflow_id', 'num_order', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['status'], 'string', 'max' => 15],
            [['essay_id', 'workflow_id'], 'unique', 'targetAttribute' => ['essay_id', 'workflow_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['workflow_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workflow::class, 'targetAttribute' => ['workflow_id' => 'workflow_id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'essay_id' => 'Essay name',
            'workflow_id' => 'Workflow name',
            'num_order' => 'Num Order',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    public function getWorkflow()
    {
        return $this->hasOne(Workflow::class, ['workflow_id' => 'workflow_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}

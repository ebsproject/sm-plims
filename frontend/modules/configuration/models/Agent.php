<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

class Agent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.bsns_agent}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registered_by', 'updated_by', 'deleted_by', 'agent_type_id', 'agent_group_id'], 'default', 'value' => null],
            [['registered_by', 'updated_by', 'deleted_by', 'agent_type_id', 'agent_group_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['short_name', 'long_name'], 'string', 'max' => 145],
            [['description'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 15],
            [['agent_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['agent_type_id' => 'parameter_id']],
            [['agent_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['agent_group_id' => 'parameter_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'agent_id' => 'Agent ID',
            'short_name' => 'Short Name',
            'long_name' => 'Long Name',
            'description' => 'Description',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'agent_type_id' => 'Agent Type ID',
            'agent_group_id' => 'Agent Group ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'agent_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentGroup()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'agent_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentByEssays()
    {
        return $this->hasMany(AgentByEssay::class, ['agent_id' => 'agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(Essay::class, ['essay_id' => 'essay_id'])->viaTable('{{%agent_by_essay}}', ['agent_id' => 'agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['agent_id' => 'agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayAgents()
    {
        return $this->hasMany(RequestProcessEssayAgent::class, ['agent_id' => 'agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(RequestProcessEssay::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id', 'essay_id' => 'essay_id'])->viaTable('{{%request_process_essay_agent}}', ['agent_id' => 'agent_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->orderBy(['code' => SORT_ASC])->all(), 'parameter_id', 'short_name');
    }
}

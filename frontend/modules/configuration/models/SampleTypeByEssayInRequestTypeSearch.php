<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\SampleTypeByEssayInRequestType;

/**
 * SampleTypeByEssayInRequestTypeSearch represents the model behind the search form of `frontend\modules\configuration\models\SampleTypeByEssayInRequestType`.
 */
class SampleTypeByEssayInRequestTypeSearch extends SampleTypeByEssayInRequestType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_order', 'composite_sample_type_id', 'essay_id', 'request_type_id', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SampleTypeByEssayInRequestType::find();
        $query->andWhere(
            [
                'bsns_sample_type_by_essay_in_request_type.deleted_by' => null,
                'bsns_sample_type_by_essay_in_request_type.deleted_at' => null,
            ]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>
                [
                    'num_order' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }


        // $query->joinWith('parameter');
        // $query->joinWith('essay');
        // $query->joinWith('parameter');

        // grid filtering conditions
        $query->andFilterWhere([
            'composite_sample_type_id' => $this->composite_sample_type_id,
            'essay_id' => $this->essay_id,
            'request_type_id' => $this->request_type_id,
            'num_order' => $this->num_order,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

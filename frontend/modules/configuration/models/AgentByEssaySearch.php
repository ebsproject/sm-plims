<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\AgentByEssay;

class AgentByEssaySearch extends AgentByEssay
{
    public function rules()
    {
        return [
            [['num_order', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at', 'status', 'agent_id', 'essay_id'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = AgentByEssay::find();
        $query->andWhere(
            [
                'bsns_agent_by_essay.deleted_by' => null,
                'bsns_agent_by_essay.deleted_at' => null,
            ]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>
                [
                    'num_order' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->joinWith('essay');
        $query->joinWith('agent');

        $query->andFilterWhere([
            'num_order' => $this->num_order,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'plims.bsns_essay.short_name', $this->essay_id])
            ->andFilterWhere(['like', 'plims.bsns_agent.short_name', $this->agent_id]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\configuration\models;

use Yii;
use frontend\modules\configuration\models\Parameter;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plims.bsns_agent_condition".
 *
 * @property int $num_order
 * @property int $essay_id
 * @property int $crop_id
 * @property int $request_type_id
 * @property int $composite_sample_type_id
 * @property int $request_process_essay_type_id
 * @property int $agent_id
 * @property int|null $reprocess_flag
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 *
 * @property Agent $agent
 * @property Crop $crop
 * @property Essay $essay
 * @property Parameter $requestType
 * @property Parameter $compositeSampleType
 * @property Parameter $requestProcessEssayType
 */
class AgentCondition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_agent_condition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_order', 'essay_id', 'crop_id', 'request_type_id', 'composite_sample_type_id', 'request_process_essay_type_id', 'agent_id'], 'required'],
            [['num_order', 'essay_id', 'crop_id', 'request_type_id', 'composite_sample_type_id', 'request_process_essay_type_id', 'agent_id', 'reprocess_flag', 'registered_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['num_order', 'essay_id', 'crop_id', 'request_type_id', 'composite_sample_type_id', 'request_process_essay_type_id', 'agent_id', 'reprocess_flag', 'registered_by', 'updated_by', 'deleted_by', 'repetition'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['status'], 'string', 'max' => 15],
            [['num_order', 'essay_id', 'crop_id', 'request_type_id', 'composite_sample_type_id', 'request_process_essay_type_id', 'agent_id'], 'unique', 'targetAttribute' => ['num_order', 'essay_id', 'crop_id', 'request_type_id', 'composite_sample_type_id', 'request_process_essay_type_id', 'agent_id']],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agent::class, 'targetAttribute' => ['agent_id' => 'agent_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
            [['request_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_type_id' => 'parameter_id']],
            [['composite_sample_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['composite_sample_type_id' => 'parameter_id']],
            [['request_process_essay_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['request_process_essay_type_id' => 'parameter_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'num_order' => 'Num Order',
            'essay_id' => 'Assay ID',
            'crop_id' => 'Crop ID',
            'request_type_id' => 'Request Type ID',
            'composite_sample_type_id' => 'Composite Sample Type ID',
            'request_process_essay_type_id' => 'Request Process Assay Type ID',
            'agent_id' => 'Agent ID',
            'reprocess_flag' => 'Reprocess',
            'repetition' => 'Repetitions',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[Agent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(Agent::class, ['agent_id' => 'agent_id']);
    }

    /**
     * Gets query for [[Crop]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * Gets query for [[Essay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    /**
     * Gets query for [[RequestType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_type_id']);
    }

    /**
     * Gets query for [[CompositeSampleType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSampleType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'composite_sample_type_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'request_process_essay_type_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

class Crop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.bsns_crop}}';
    }

    public function rules()
    {
        return [
            [['registered_by', 'updated_by', 'deleted_by', 'institution_id'], 'default', 'value' => null],
            [['registered_by', 'updated_by', 'deleted_by', 'institution_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['short_name', 'long_name'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 15],
        ];
    }

    public function attributeLabels()
    {
        return [
            'crop_id' => 'Crop ID',
            'short_name' => 'Crop',
            'long_name' => 'Long Name',
            'description' => 'Description',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'institution_id' => 'Institution ID'
        ];
    }


    public function getMaterials()
    {
        return $this->hasMany(Material::class, ['crop_id' => 'crop_id']);
    }


    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['crop_id' => 'crop_id']);
    }


    public function getRequestProcesses()
    {
        return $this->hasMany(RequestProcess::class, ['crop_id' => 'crop_id']);
    }


    public function getRequestProcessEssays()
    {
        return $this->hasMany(RequestProcessEssay::class, ['crop_id' => 'crop_id']);
    }


    public function getRequestProcessEssayActivities()
    {
        return $this->hasMany(RequestProcessEssayActivity::class, ['crop_id' => 'crop_id']);
    }


    public function getRequestProcessEssayAgents()
    {
        return $this->hasMany(RequestProcessEssayAgent::class, ['crop_id' => 'crop_id']);
    }


    public function getSamples()
    {
        return $this->hasMany(Sample::class, ['crop_id' => 'crop_id']);
    }


    public function getSupports()
    {
        return $this->hasMany(Support::class, ['crop_id' => 'crop_id']);
    }


    public function getWorkflowByCrops()
    {
        return $this->hasMany(WorkflowByCrop::class, ['crop_id' => 'crop_id']);
    }


    public function getWorkflows()
    {
        return $this->hasMany(Workflow::class, ['workflow_id' => 'workflow_id'])->viaTable('{{%workflow_by_crop}}', ['crop_id' => 'crop_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}

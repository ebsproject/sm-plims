<?php

namespace frontend\modules\configuration\models;

use frontend\modules\finance\models\Cost;
use yii\helpers\ArrayHelper;

class Essay extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{plims.bsns_essay}}';
    }

    public function rules()
    {
        return [
            [['registered_by', 'updated_by', 'deleted_by', 'essay_cost_id', 'essay_type_id', 'essay_location_id', 'abbreviation', 'style_class'], 'default', 'value' => null],
            [['registered_by', 'updated_by', 'deleted_by', 'essay_cost_id', 'essay_type_id', 'essay_location_id'], 'integer'],
            [['style_class'], 'string'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['short_name'], 'string', 'max' => 45],
            [['abbreviation'], 'string', 'max' => 10],
            [['long_name'], 'string', 'max' => 145],
            [['description', 'details'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 15],
            [['essay_cost_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cost::class, 'targetAttribute' => ['essay_cost_id' => 'cost_id']],
            [['essay_location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Location::class, 'targetAttribute' => ['essay_location_id' => 'location_id']],
            [['essay_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Parameter::class, 'targetAttribute' => ['essay_type_id' => 'parameter_id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'essay_id' => 'Assay ID',
            'short_name' => 'Short Name',
            'long_name' => 'Long Name',
            'description' => 'Description',
            'details' => 'Details',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'essay_cost_id' => 'Assay Cost ID',
            'essay_type_id' => 'Assay Data Source',
            'essay_location_id' => 'Assay Location ID',
            'abbreviation' => 'Abbreviation',
            'style_class' => 'Style Class',
        ];
    }

    public function getActivityByEssays()
    {
        return $this->hasMany(ActivityByEssay::class, ['essay_id' => 'essay_id']);
    }

    public function getActivities()
    {
        return $this->hasMany(
            Activity::class,
            [
                'activity_id' => 'activity_id'
            ]
        )
            ->viaTable(
                '{{%activity_by_essay}}',
                [
                    'essay_id' => 'essay_id',
                ]
            );
    }

    public function getAgentByEssays()
    {
        return $this->hasMany(AgentByEssay::class, ['essay_id' => 'essay_id']);
    }

    public function getAgents()
    {
        return $this->hasMany(
            Agent::class,
            [
                'agent_id' => 'agent_id'
            ]
        )
            ->viaTable(
                '{{%agent_by_essay}}',
                [
                    'essay_id' => 'essay_id',
                ]
            );
    }

    public function getEssayCost()
    {
        return $this->hasOne(Cost::class, ['cost_id' => 'essay_cost_id']);
    }

    public function getEssayLocation()
    {
        return $this->hasOne(Location::class, ['location_id' => 'essay_location_id']);
    }

    public function getEssayType()
    {
        return $this->hasOne(Parameter::class, ['parameter_id' => 'essay_type_id']);
    }

    public function getEssayByWorkflows()
    {
        return $this->hasMany(EssayByWorkflow::class, ['essay_id' => 'essay_id']);
    }

    public function getWorkflows()
    {
        return $this->hasMany(Workflow::class, ['workflow_id' => 'workflow_id'])->viaTable('{{%essay_by_workflow}}', ['essay_id' => 'essay_id']);
    }

    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::class, ['essay_id' => 'essay_id']);
    }

    public function getRequestProcessEssays()
    {
        return $this->hasMany(RequestProcessEssay::class, ['essay_id' => 'essay_id']);
    }

    public function getRequests()
    {
        return $this->hasMany(RequestProcess::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id'])->viaTable('{{%request_process_essay}}', ['essay_id' => 'essay_id']);
    }

    public function getRequestProcessEssayActivities()
    {
        return $this->hasMany(RequestProcessEssayActivity::class, ['essay_id' => 'essay_id']);
    }

    public function getRequestProcessEssayAgents()
    {
        return $this->hasMany(RequestProcessEssayAgent::class, ['essay_id' => 'essay_id']);
    }

    public function getSupports()
    {
        return $this->hasMany(Support::class, ['essay_id' => 'essay_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParameter($entity, $singularity)
    {
        return ArrayHelper::map(Parameter::find()->where(
            [
                'entity' => $entity,
                'singularity' => $singularity,
                'deleted_by' => null,
                'deleted_at' => null,
                'status' => 'active',
            ]
        )->all(), 'parameter_id', 'short_name');
    }
}

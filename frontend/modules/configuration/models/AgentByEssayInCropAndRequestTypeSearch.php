<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\AgentByEssayInCropAndRequestType;

/**
 * AgentByEssayInCropAndRequestTypeSearch represents the model behind the search form of `frontend\modules\configuration\models\AgentByEssayInCropAndRequestType`.
 */
class AgentByEssayInCropAndRequestTypeSearch extends AgentByEssayInCropAndRequestType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agent_id', 'essay_id', 'crop_id', 'num_order', 'registered_by', 'updated_by', 'deleted_by', 'request_type_id'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgentByEssayInCropAndRequestType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'agent_id' => $this->agent_id,
            'essay_id' => $this->essay_id,
            'crop_id' => $this->crop_id,
            'num_order' => $this->num_order,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'request_type_id' => $this->request_type_id,
        ]);

        $query->andFilterWhere(['ilike', 'status', $this->status]);

        return $dataProvider;
    }
}

<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%activity_by_essay}}".
 *
 * @property int $activity_id
 * @property int $essay_id
 * @property int|null $num_order
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 * @property bool $is_template
 *
 * @property Activity $activity
 * @property Essay $essay
 * @property RequestProcessEssayActivity[] $requestProcessEssayActivities
 * @property RequestProcessEssay[] $requests
 */
class ActivityByEssay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.bsns_activity_by_essay}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activity_id', 'essay_id'], 'required'],
            [['activity_id', 'essay_id', 'num_order', 'registered_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['activity_id', 'essay_id', 'num_order', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['is_template'], 'boolean'],
            [['status'], 'string', 'max' => 15],
            [['activity_id', 'essay_id'], 'unique', 'targetAttribute' => ['activity_id', 'essay_id']],
            [['activity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activity::class, 'targetAttribute' => ['activity_id' => 'activity_id']],
            [['essay_id'], 'exist', 'skipOnError' => true, 'targetClass' => Essay::class, 'targetAttribute' => ['essay_id' => 'essay_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'activity_id' => 'Activity ID',
            'essay_id' => 'Assay ID',
            'num_order' => 'Num Order',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
            'is_template' => 'Is Template',
        ];
    }

    /**
     * Gets query for [[Activity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(Activity::class, ['activity_id' => 'activity_id']);
    }

    /**
     * Gets query for [[Essay]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssay()
    {
        return $this->hasOne(Essay::class, ['essay_id' => 'essay_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivities]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivities()
    {
        return $this->hasMany(RequestProcessEssayActivity::class, ['essay_id' => 'essay_id', 'activity_id' => 'activity_id']);
    }

    /**
     * Gets query for [[Requests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(RequestProcessEssay::class, ['request_id' => 'request_id', 'num_order_id' => 'num_order_id', 'essay_id' => 'essay_id', 'composite_sample_type_id' => 'composite_sample_type_id'])->viaTable('{{%request_process_essay_activity}}', ['essay_id' => 'essay_id', 'activity_id' => 'activity_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}

<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\EssayByWorkflow;

class EssayByWorkflowSearch extends EssayByWorkflow
{
    public function rules()
    {
        return [
            [['num_order', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at', 'status', 'essay_id', 'workflow_id'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = EssayByWorkflow::find();
        $query->andWhere(
            [
                'plims.bsns_essay_by_workflow.deleted_by' => null,
                'plims.bsns_essay_by_workflow.deleted_at' => null,
            ]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' =>
                [
                    'num_order' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->joinWith('workflow');
        $query->joinWith('essay');

        $query->andFilterWhere([
            'num_order' => $this->num_order,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'plims.bsns_essay_by_workflow.status', $this->status])
            ->andFilterWhere(['like', 'plims.bsns_workflow.short_name', $this->workflow_id])
            ->andFilterWhere(['like', 'plims.bsns_essay.short_name', $this->essay_id]);

        return $dataProvider;
    }
}

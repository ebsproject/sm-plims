<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\AgentCondition;

/**
 * AgentConditionSearch represents the model behind the search form of `frontend\modules\configuration\models\AgentCondition`.
 */
class AgentConditionSearch extends AgentCondition
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_order', 'essay_id', 'crop_id', 'request_type_id', 'composite_sample_type_id', 'request_process_essay_type_id', 'agent_id', 'reprocess_flag', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AgentCondition::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'num_order' => $this->num_order,
            'essay_id' => $this->essay_id,
            'crop_id' => $this->crop_id,
            'request_type_id' => $this->request_type_id,
            'composite_sample_type_id' => $this->composite_sample_type_id,
            'request_process_essay_type_id' => $this->request_process_essay_type_id,
            'agent_id' => $this->agent_id,
            'reprocess_flag' => $this->reprocess_flag,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

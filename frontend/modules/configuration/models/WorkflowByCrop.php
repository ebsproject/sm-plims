<?php

namespace frontend\modules\configuration\models;

use Yii;
use yii\helpers\ArrayHelper;

class WorkflowByCrop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.bsns_workflow_by_crop}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['workflow_id', 'crop_id'], 'required'],
            [['workflow_id', 'crop_id', 'num_order', 'registered_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['workflow_id', 'crop_id', 'num_order', 'registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['status'], 'string', 'max' => 15],
            [['workflow_id', 'crop_id'], 'unique', 'targetAttribute' => ['workflow_id', 'crop_id']],
            [['crop_id'], 'exist', 'skipOnError' => true, 'targetClass' => Crop::class, 'targetAttribute' => ['crop_id' => 'crop_id']],
            [['workflow_id'], 'exist', 'skipOnError' => true, 'targetClass' => Workflow::class, 'targetAttribute' => ['workflow_id' => 'workflow_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'workflow_id' => 'Workflow Name',
            'crop_id' => 'Crop Name',
            'num_order' => 'Num Order',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrop()
    {
        return $this->hasOne(Crop::class, ['crop_id' => 'crop_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkflow()
    {
        return $this->hasOne(Workflow::class, ['workflow_id' => 'workflow_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->registered_by = \Yii::$app->user->identity->id;
            $this->registered_at = date("Y-m-d H:i:s", time());
            $this->status = "active";
        } else {
            $this->updated_by = \Yii::$app->user->identity->id;
            $this->updated_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }
}

<?php

namespace frontend\modules\configuration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\configuration\models\Activity;

/**
 * ActivitySearch represents the model behind the search form of `frontend\modules\configuration\models\Activity`.
 */
class ActivitySearch extends Activity
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activity_id', 'registered_by', 'updated_by', 'deleted_by', 'activity_type_id', 'activity_datasource_id', 'activity_property_id', 'activity_group_id'], 'integer'],
            [['short_name', 'long_name', 'description', 'details', 'registered_at', 'updated_at', 'deleted_at', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Activity::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'activity_id' => $this->activity_id,
            'registered_by' => $this->registered_by,
            'registered_at' => $this->registered_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'deleted_by' => $this->deleted_by,
            'deleted_at' => $this->deleted_at,
            'activity_type_id' => $this->activity_type_id,
            'activity_datasource_id' => $this->activity_datasource_id,
            'activity_property_id' => $this->activity_property_id,
            'activity_group_id' => $this->activity_group_id,
        ]);

        $query->andFilterWhere(['like', 'short_name', $this->short_name])
            ->andFilterWhere(['like', 'long_name', $this->long_name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'details', $this->details])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

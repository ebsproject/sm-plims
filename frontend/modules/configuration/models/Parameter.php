<?php

namespace frontend\modules\configuration\models;

use Yii;
use frontend\modules\functional\models\CompositeSample;
use frontend\modules\functional\models\Material;
use frontend\modules\functional\models\ReadingData;
use frontend\modules\functional\models\RequestDocumentation;
use frontend\modules\functional\models\RequestProcessEssayActivity;
use frontend\modules\functional\models\RequestProcessEssayActivitySample;
use frontend\modules\functional\models\Support;
use frontend\modules\functional\models\RequestProcess;
use frontend\modules\functional\models\RequestProcessEssay;
use frontend\modules\functional\models\RequestProcessEssayAgent;
use frontend\modules\functional\models\Request;
use frontend\modules\functional\models\Status;

use frontend\modules\configuration\models\Essay;
use frontend\modules\finance\models\Cost;

/**
 * This is the model class for table "plims.bsns_parameter".
 *
 * @property int $parameter_id
 * @property string|null $code
 * @property string|null $singularity
 * @property string|null $entity
 * @property string|null $short_name
 * @property string|null $long_name
 * @property string|null $description
 * @property string|null $class_details
 * @property int|null $registered_by
 * @property string|null $registered_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 * @property int|null $deleted_by
 * @property string|null $deleted_at
 * @property string|null $status
 *
 * @property Activity[] $activities
 * @property Activity[] $activities0
 * @property Activity[] $activities1
 * @property Activity[] $activities2
 * @property AgentCondition[] $agentConditions
 * @property AgentCondition[] $agentConditions0
 * @property AgentCondition[] $agentConditions1
 * @property Agent[] $agents
 * @property Agent[] $agents0
 * @property CompositeSample[] $compositeSamples
 * @property CompositeSample[] $compositeSamples0
 * @property Cost[] $costs
 * @property Essay[] $essays
 * @property Material[] $materials
 * @property Material[] $materials0
 * @property Material[] $materials1
 * @property Material[] $materials2
 * @property ReadingData[] $readingDatas
 * @property ReadingData[] $readingDatas0
 * @property ReadingData[] $readingDatas1
 * @property RequestDocumentation[] $requestDocumentations
 * @property RequestProcessEssayActivity[] $requestProcessEssayActivities
 * @property RequestProcessEssayActivity[] $requestProcessEssayActivities0
 * @property RequestProcessEssayActivitySample[] $requestProcessEssayActivitySamples
 * @property RequestProcessEssayActivitySample[] $requestProcessEssayActivitySamples0
 * @property RequestProcessEssayAgent[] $requestProcessEssayAgents
 * @property RequestProcessEssay[] $requestProcessEssays
 * @property RequestProcessEssay[] $requestProcessEssays0
 * @property RequestProcessEssay[] $requestProcessEssays1
 * @property RequestProcessEssay[] $requestProcessEssays2
 * @property RequestProcess[] $requestProcesses
 * @property RequestProcess[] $requestProcesses0
 * @property RequestProcess[] $requestProcesses1
 * @property Request[] $requests
 * @property Request[] $requests0
 * @property Request[] $requests1
 * @property Request[] $requests2
 * @property Status[] $statuses
 * @property Support[] $supports
 * @property Support[] $supports0
 */
class Parameter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'plims.bsns_parameter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['registered_by', 'updated_by', 'deleted_by'], 'default', 'value' => null],
            [['registered_by', 'updated_by', 'deleted_by'], 'integer'],
            [['registered_at', 'updated_at', 'deleted_at'], 'safe'],
            [['code', 'singularity', 'entity', 'short_name'], 'string', 'max' => 45],
            [['long_name'], 'string', 'max' => 145],
            [['description', 'class_details'], 'string', 'max' => 245],
            [['status'], 'string', 'max' => 15],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'parameter_id' => 'Parameter ID',
            'code' => 'Code',
            'singularity' => 'Singularity',
            'entity' => 'Entity',
            'short_name' => 'Short Name',
            'long_name' => 'Long Name',
            'description' => 'Description',
            'class_details' => 'Class Details',
            'registered_by' => 'Registered By',
            'registered_at' => 'Registered At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted_by' => 'Deleted By',
            'deleted_at' => 'Deleted At',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[Activities]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['activity_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Activities0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivities0()
    {
        return $this->hasMany(Activity::className(), ['activity_datasource_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Activities1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivities1()
    {
        return $this->hasMany(Activity::className(), ['activity_property_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Activities2]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActivities2()
    {
        return $this->hasMany(Activity::className(), ['activity_group_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[AgentConditions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgentConditions()
    {
        return $this->hasMany(AgentCondition::className(), ['request_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[AgentConditions0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgentConditions0()
    {
        return $this->hasMany(AgentCondition::className(), ['composite_sample_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[AgentConditions1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgentConditions1()
    {
        return $this->hasMany(AgentCondition::className(), ['request_process_essay_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Agents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgents()
    {
        return $this->hasMany(Agent::className(), ['agent_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Agents0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgents0()
    {
        return $this->hasMany(Agent::className(), ['agent_group_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[CompositeSamples]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSamples()
    {
        return $this->hasMany(CompositeSample::className(), ['composite_sample_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[CompositeSamples0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositeSamples0()
    {
        return $this->hasMany(CompositeSample::className(), ['reading_data_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Costs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCosts()
    {
        return $this->hasMany(Cost::className(), ['cost_currency_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Essays]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEssays()
    {
        return $this->hasMany(Essay::className(), ['essay_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Materials]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials()
    {
        return $this->hasMany(Material::className(), ['composite_sample_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Materials0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials0()
    {
        return $this->hasMany(Material::className(), ['material_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Materials1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials1()
    {
        return $this->hasMany(Material::className(), ['material_unit_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Materials2]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterials2()
    {
        return $this->hasMany(Material::className(), ['material_status_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[ReadingDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas()
    {
        return $this->hasMany(ReadingData::className(), ['composite_sample_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[ReadingDatas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas0()
    {
        return $this->hasMany(ReadingData::className(), ['reading_data_section_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[ReadingDatas1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReadingDatas1()
    {
        return $this->hasMany(ReadingData::className(), ['reading_data_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestDocumentations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestDocumentations()
    {
        return $this->hasMany(RequestDocumentation::className(), ['file_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivities]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivities()
    {
        return $this->hasMany(RequestProcessEssayActivity::className(), ['composite_sample_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivities0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivities0()
    {
        return $this->hasMany(RequestProcessEssayActivity::className(), ['request_process_essay_activity_status_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivitySamples]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivitySamples()
    {
        return $this->hasMany(RequestProcessEssayActivitySample::className(), ['composite_sample_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayActivitySamples0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayActivitySamples0()
    {
        return $this->hasMany(RequestProcessEssayActivitySample::className(), ['request_process_essay_activity_sample_status_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcessEssayAgents]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssayAgents()
    {
        return $this->hasMany(RequestProcessEssayAgent::className(), ['composite_sample_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcessEssays]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssays()
    {
        return $this->hasMany(RequestProcessEssay::className(), ['composite_sample_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcessEssays0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssays0()
    {
        return $this->hasMany(RequestProcessEssay::className(), ['request_process_essay_status_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcessEssays1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcessEssays1()
    {
        return $this->hasMany(RequestProcessEssay::className(), ['request_process_essay_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcesses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcesses()
    {
        return $this->hasMany(RequestProcess::className(), ['request_process_status_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcesses0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcesses0()
    {
        return $this->hasMany(RequestProcess::className(), ['request_process_material_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[RequestProcesses1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequestProcesses1()
    {
        return $this->hasMany(RequestProcess::className(), ['request_process_payment_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Requests]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['request_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Requests0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequests0()
    {
        return $this->hasMany(Request::className(), ['request_status_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Requests1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequests1()
    {
        return $this->hasMany(Request::className(), ['request_payment_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Requests2]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRequests2()
    {
        return $this->hasMany(Request::className(), ['request_id' => 'request_id'])->viaTable('plims.bsns_status', ['request_status_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Statuses]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatuses()
    {
        return $this->hasMany(Status::className(), ['request_status_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Supports]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['composite_sample_type_id' => 'parameter_id']);
    }

    /**
     * Gets query for [[Supports0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSupports0()
    {
        return $this->hasMany(Support::className(), ['support_type_id' => 'parameter_id']);
    }
}

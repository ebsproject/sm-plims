<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;
use Yii;

$this->title = 'Configurations';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/admin/admin-configuration/management']];
$this->params['breadcrumbs'][] = ['label' => 'Manage Parameters', 'url' => ['/configuration/manage-configuration/index']];
$this->params['breadcrumbs'][] = 'Manage Parameter';
?>


<div class="management">
  <!-- #region BACK -->
  <div class="row">
    <div class="col-md-8">
      <h1><?= is_null($code) ? Html::encode($this->title) : Html::encode($this->title) . ' (<span class="text-blue-link">' . $code . '</span>)' ?></h1>
    </div>
    <div class="col-md-4 init-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back',  ['manage-configuration/index'], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->


  <div class="row">
    <div class="col-lg-12 col-md-12">
      <?= $this->render('_view-a-set-params', [
        'data_entity' => $data_entity,
        'data_singularity' => $data_singularity,
      ])  ?>
    </div>
  </div>


  <div class="row">
    <div class="col-lg-12 col-md-12">
      <?= $this->render('_view-b-configuration-list', [
        'data_provider_configuration' => $data_provider_configuration,
      ])  ?>
    </div>
  </div>


  <!-- #region BACK -->
  <div class="row ">
    <div class="col-md-12 final-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back',  ['manage-configuration/index'], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->
</div>
<?php

use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;

$this->title = 'Set configuration';

$institution_id = Yii::$app->request->get('institution_id');
$institution = Yii::$app->request->get('institution');

$entity = Yii::$app->request->get('entity');
$singularity = Yii::$app->request->get('singularity');
$parameter_id = Yii::$app->request->get('parameter_id');

$code = Yii::$app->request->get('code');
$short_name  = Yii::$app->request->get('short_name');
$long_name = Yii::$app->request->get('long_name');

$text_value = Yii::$app->request->get('text_value');
$number_value = Yii::$app->request->get('number_value');

$class_details  = Yii::$app->request->get('class_details');
$description = Yii::$app->request->get('description');
$status = Yii::$app->request->get('status');

$disabled = (empty($code) || is_null($code)) ? false : true;
?>
<script>
  $(".js-example-tags").select2({
    tags: true
  });
</script>

<?php $form = ActiveForm::begin(); ?>

<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link"><?= Html::encode($this->title) ?></h3>
  </div>

  <div class="card-body">
    <!--  -->
    <div class="row">
      <div class="col-lg-12">
        <label class="control-label">Institution: <?= $institution ?></label>
      </div>
    </div>
    <!--  -->
    <div class="row">
      <div class="col-lg-4">


        <div>
          <label class="control-label">Entity </label>
          <?= Select2::widget(
            [
              'id' => 'entity_id',
              'name' => 'entity',
              'data' => $data_entity,
              'value' => $entity,
              'options' => [
                'placeholder' => 'Select entity ...',
                'disabled' => $disabled,
              ],
            ]
          );
          ?>
        </div>
        <br>
        <div>
          <label class="control-label">Singularity </label>
          <?= Select2::widget(
            [
              'id' => 'singularity_id',
              'name' => 'singularity',
              'data' => $data_singularity,
              'value' => $singularity,
              'options' => [
                'placeholder' => 'Select singularity ...',
                'disabled' => $disabled
              ],
            ]
          );
          ?>
        </div>
      </div>

      <div class="col-lg-4">

        <div>
          <label class="control-label">Code</label>
          <?= Html::input('text', 'code', $code, $options = ['class' => 'form-control']) ?>
        </div>
        <br>

        <div>
          <label class="control-label">Short name</label>
          <?= Html::input('text', 'short_name', $short_name, $options = ['class' => 'form-control']) ?>
        </div>
        <br>

        <div>
          <label class="control-label">Long name</label>
          <?= Html::input('text', 'long_name', $long_name, $options = ['class' => 'form-control']) ?>
        </div>
        <br>

        <!-- <div class="row">
          <div class="col-lg-6">
            <div>
              <label class="control-label">Text value</label>
              Html::input('text', 'text_value', $text_value, $options = ['class' => 'form-control']) 
            </div>
          </div>

          <div class="col-lg-6">
            <div>
              <label class="control-label">Number value</label>
               Html::input('text', 'number_value', $number_value, $options = ['class' => 'form-control']) 
            </div>
          </div>
        </div> -->

      </div>

      <div class="col-lg-4">
        <!-- <div>
          <label class="control-label">Class detail</label>
          Html::input('text', 'class_details', $class_details, $options = ['class' => 'form-control']) 
        </div> -->
        <br>
        <div>
          <label class="control-label">Description</label>
          <?= Html::textArea(
            'description',
            $description,
            [
              'class' => 'form-control',
              'maxlength' => 245,
              'rows' => 2
            ]
          ) ?>
        </div>
        <br>
        <div>


          <p>
            <?= Html::radioList('status', $status, ['active' => 'active', 'disable' => 'disable']) ?>
          </p>


        </div>
      </div>
    </div>
    <!--  -->
  </div>
  <!--  -->
  <div class="card-footer">
    <div class="row">
      <div class="col-lg-12">
        <div class="btn-group pull-right">


          <?= Html::a(
            '<i class="fa fa-refresh" aria-hidden="true"></i> Reset to create a new configuration',
            [
              'management',
              'entity' => $entity,
              'singularity' => $singularity,
              'institution_id' =>  $institution_id,
              'institution' =>  $institution,
            ],
            ['class' => 'btn btn-default']
          ) ?>
          <?= Html::a(
            is_null($code) ? 'Save' : 'Update',
            [
              'save-configuration',
              'entity' =>  $entity,
              'singularity' =>  $singularity,
              'institution_id' =>  $institution_id,
              'institution' =>  $institution,
              'parameter_id' =>  $parameter_id,
            ],
            [
              'class' => 'btn btn-primary',
              'title'        => 'Save parameters in new condition',
              'data-method'  => 'post',
            ]
          ); ?>



        </div>
      </div>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>
<?php

use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;

$this->title = 'Configuration list';


$entity = Yii::$app->request->get('entity');
$singularity = Yii::$app->request->get('singularity');



$code = Yii::$app->request->get('code');


$disabled = (empty($code) || is_null($code)) ? false : true;
?>

<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link"><?= Html::encode($this->title) ?></h3>
  </div>

  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">

        <?= GridView::widget([
          'dataProvider' => $data_provider_configuration,
          'layout' => "{items}{summary}{pager}",
          'columns' =>
          [
            ['class' => 'yii\grid\SerialColumn'],
            [
              'label' => 'ID',
              'attribute' => 'parameter_id',
              'format' => 'raw',
            ],
            [
              'attribute' => 'code',
              'value' => 'code',
            ],
            [
              'attribute' => 'entity',
              'value' => 'entity',
            ],
            [
              'attribute' => 'singularity',
              'value' => 'singularity',
            ],
            [
              'attribute' => 'short_name',
              'value' => 'short_name',
            ],
            [
              'attribute' => 'long_name',
              'value' => 'long_name',
            ],
            [
              'attribute' => 'text_value',
              'value' => 'text_value',
            ],
            [
              'attribute' => 'number_value',
              'value' => 'number_value',
            ],
            [
              'attribute' => 'class_details',
              'value' => 'class_details',
            ],
            [
              'attribute' => 'description',
              'value' => 'description',
            ],
            [
              'attribute' => 'status',
              'value' => 'status',
            ],
            [
              'class' => 'yii\grid\ActionColumn',
              'header'        => 'Action',
              'template'      => '{edit} {remove}',
              'headerOptions' => ['width' => '80'],
              'buttons'       => [
                'edit' => function ($url, $model, $key) {
                  return Html::a(
                    '<span class="fa fa-edit" title="Edit"></span>',
                    [
                      'edit-configuration',
                      'entity' =>  $model['entity'],
                      'singularity' =>  $model['singularity'],
                      'institution_id' =>  $model['institution_id'],
                      'institution' =>  $model['institution'],
                      'parameter_id' =>  $model['parameter_id'],
                    ],
                    [
                      'class' => 'btn btn-primary btn-xs',
                      'data' => [
                        'method' => 'post',
                      ],
                    ]
                  );
                },
                'remove' => function ($url, $model, $key) {
                  return Html::a(
                    '<span class="glyphicon glyphicon-trash" title="Edit"></span>',
                    [
                      'remove-configuration',
                      'entity' => $model['entity'],
                      'singularity' => $model['singularity'],
                      'institution_id' => $model['institution_id'],
                      'institution' =>  $model['institution'],
                      'parameter_id' => $model['parameter_id'],
                    ],
                    [
                      'class' => 'btn btn-danger btn-xs',
                      'data' => [
                        'confirm' => 'ARE YOU SURE YOU WANT TO DELETE THIS CONFIGURATION: ' . $model['entity'] . '/' . $model['singularity'] . '?',
                        'method' => 'post',
                      ],
                    ]
                  );
                },

              ],
            ],
          ],
        ]); ?>


      </div>
    </div>
  </div>
</div>
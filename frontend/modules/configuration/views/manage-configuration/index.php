<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['/admin/admin-configuration/management']];
$this->params['breadcrumbs'][] = 'Manage Parameters';
?>

<?php $form = ActiveForm::begin(['id' => 'form-1']); ?>
<div class="index">
  <div class="row">
    <div class="col-md-8">
      <h1>Manage Parameters</h1>
      <p>Parameter list and manage functions to create or update a parameter:</p>
    </div>

    <div class="col-md-2" style=" display: flex; justify-content: center; align-items: center;">
      <?= Select2::widget(
        [
          'id' => 'institution_id',
          'name' => 'institution',
          'data' =>  $data_institution,
          'value' => $value_institution_id,
          'options' => [
            'placeholder' => 'Select institution ...',
            'class' => 'form-control',
            'disabled' => $is_disabled,
          ],
          'pluginEvents' => [
            'select2:select' => 'function() {  
                  document.getElementById("form-1").submit();
             }',
          ]
        ]
      ); ?>
    </div>

    <div class="col-md-2" style=" display: flex; justify-content: center; align-items: center;">
      <?= Html::a(
        '<i class="fa fa-plus"></i> New Configurations',
        [
          'new-configuration',
          'institution_id' => $value_institution_id,
          'institution' => $data_institution[$value_institution_id],
        ],
        [
          'class' => 'btn btn-default pull-right',
          'disabled' => $value_institution_id == 1,
        ]
      ) ?>
    </div>



  </div>
  <div class=" row">
    <div class="col-md-12">
      <div class="card card-outline card-secondary">
        <div class="card-body">
          <label class="control-label">Agent(s) selected: </label>
          <?php Pjax::begin(); ?>
          <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}{summary}{pager}",
            'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              [
                'label' => 'Institution',
                'attribute' => 'institution',
                'format' => 'raw',
              ],
              [
                'label' => 'Entity',
                'attribute' => 'entity',
                'format' => 'raw',
              ],
              [
                'label' => 'Singularity',
                'attribute' => 'singularity',
                'format' => 'raw',
              ],
              [
                'label' => 'Code(s)',
                'attribute' => 'code',
                'format' => 'raw',
                'value' => function ($model) {
                  $code = $model['code'];
                  if (strlen($code) > 50) {
                    $code = substr($code, 0, 50) . '...';
                  }
                  return $code;
                },
                'contentOptions' => function ($model) {
                  return [
                    'title' => $model['code'],
                  ];
                },
              ],
              [
                'attribute' => 'status',
                'value' => 'status',
              ],
              [
                'class' => 'yii\grid\ActionColumn',
                'header'        => 'Action',
                'template'      => '{edit} {remove}',
                'headerOptions' => ['width' => '80'],
                'buttons'       => [
                  'edit' => function ($url, $model, $key) {
                    return Html::a(
                      '<span class="fa fa-edit" title="Edit"></span>',
                      [
                        'edit-configuration',
                        'entity' =>  $model['entity'],
                        'singularity' =>  $model['singularity'],
                        'institution_id' =>  $model['institution_id'],
                        'institution' =>  $model['institution'],
                      ],
                      [
                        'class' => 'btn btn-primary btn-xs',
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    );
                  },
                ],
              ],
            ],
          ]); ?>
          <?php Pjax::end(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\configuration\models\AgentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['/admin/admin-configuration/management']];
$this->params['breadcrumbs'][] = 'Agents';

?>
<div class="agent-index">

    <h1>Manage Agents</h1>

    <p>
        <?= Html::a('Create Agent', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'agent_id',
            'short_name',
            'long_name',
            'description',
            'registered_by',
            [
                'attribute' => 'agent_type_id',
                'value' => 'agentType.short_name'
            ],
            [
                'attribute' => 'agent_group_id',
                'value' => 'agentGroup.short_name'
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
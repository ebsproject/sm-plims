<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="agent-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'long_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <label class="control-label">Description</label>
            <?= Html::textArea(
                'description',
                $model->description,
                [
                    'class' => 'form-control',
                    'maxlength' => 245,
                    'rows' => 4
                ]
            ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form
                ->field($model, 'agent_group_id')
                ->dropDownList(
                    $model->getParameter('agent', 'group'),
                    ['prompt' => 'Select...']
                ) ?>
            <h4><i class="fa fa-info"></i> AGENT-GROUP</h4>
            <p>
                <b>pathogens and others: </b>The results of these agents belong to the group of pathogens and their value is measurable.
                (ANILINE BLUE TEST, BLOTTER AND FREEZE PAPER TEST, ENDOSPERM HYDROLYSIS, FILTRATION AND CENTRIFUGATION, GLOEOTINIA TEMULENTA, PCR, STAINING TEST, VISUAL INSPECTION, WASHING AND FILTRATION, WASH TEST FOR BACTERIA)
            </p>
            <p>
                <b>measurable values of symptoms: </b>The results of these agents belong to the group of symptoms and their value is measurable.
                (GERMINATION TEST)
            </p>
            <p>
                <b>description of symptoms: </b>The results of these agents belong to the group of symptoms and their value is measurable or descriptive.
                (GERMINATION TEST)
            </p>
        </div>
        <div class="col-lg-6">
            <?= $form
                ->field($model, 'agent_type_id')
                ->dropDownList(
                    $model->getParameter('agent', 'type'),
                    ['prompt' => 'Select...']
                ) ?>
            <h4><i class="fa fa-info"></i> AGENT-TYPE</h4>
            <p>
                <b>single text result: </b>
                Single textual value result (Positive, Negative).
            </p>
            <p>
                <b>single text information: </b>
                Descriptive text value result.
            </p>
            <p>
                <b>single numeric result: </b>
                Single numeric value result (Absorbances, Number of seeds).
            </p>
            <p>
                <b>single numeric information: </b>
                Descriptive numeric value result.
            </p>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
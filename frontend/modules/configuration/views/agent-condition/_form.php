<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="agent-condition-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'num_order')->textInput() ?>

    <?= $form->field($model, 'essay_id')->textInput() ?>

    <?= $form->field($model, 'crop_id')->textInput() ?>

    <?= $form->field($model, 'request_type_id')->textInput() ?>

    <?= $form->field($model, 'composite_sample_type_id')->textInput() ?>

    <?= $form->field($model, 'request_process_essay_type_id')->textInput() ?>

    <?= $form->field($model, 'agent_id')->textInput() ?>

    <?= $form->field($model, 'reprocess_flag')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
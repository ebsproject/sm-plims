<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="agent-condition-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'num_order') ?>

    <?= $form->field($model, 'essay_id') ?>

    <?= $form->field($model, 'crop_id') ?>

    <?= $form->field($model, 'request_type_id') ?>

    <?= $form->field($model, 'composite_sample_type_id') ?>

    <?= $form->field($model, 'request_process_essay_type_id') ?>

    <?= $form->field($model, 'agent_id') ?>

    <?= $form->field($model, 'reprocess_flag') ?>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
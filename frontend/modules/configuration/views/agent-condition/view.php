<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\AgentCondition */

$this->title = $model->num_order;
$this->params['breadcrumbs'][] = ['label' => 'Agent Conditions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="agent-condition-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'num_order' => $model->num_order, 'essay_id' => $model->essay_id, 'crop_id' => $model->crop_id, 'request_type_id' => $model->request_type_id, 'composite_sample_type_id' => $model->composite_sample_type_id, 'request_process_essay_type_id' => $model->request_process_essay_type_id, 'agent_id' => $model->agent_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'num_order' => $model->num_order, 'essay_id' => $model->essay_id, 'crop_id' => $model->crop_id, 'request_type_id' => $model->request_type_id, 'composite_sample_type_id' => $model->composite_sample_type_id, 'request_process_essay_type_id' => $model->request_process_essay_type_id, 'agent_id' => $model->agent_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'num_order',
            'essay_id',
            'crop_id',
            'request_type_id',
            'composite_sample_type_id',
            'request_process_essay_type_id',
            'agent_id',
            'reprocess_flag',
            'registered_by',
            'registered_at',
            'updated_by',
            'updated_at',
            'deleted_by',
            'deleted_at',
            'status',
        ],
    ]) ?>

</div>

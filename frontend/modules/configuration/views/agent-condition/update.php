<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\AgentCondition */

$this->title = 'Update Agent Condition: ' . $model->num_order;
$this->params['breadcrumbs'][] = ['label' => 'Agent Conditions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->num_order, 'url' => ['view', 'num_order' => $model->num_order, 'essay_id' => $model->essay_id, 'crop_id' => $model->crop_id, 'request_type_id' => $model->request_type_id, 'composite_sample_type_id' => $model->composite_sample_type_id, 'request_process_essay_type_id' => $model->request_process_essay_type_id, 'agent_id' => $model->agent_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agent-condition-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

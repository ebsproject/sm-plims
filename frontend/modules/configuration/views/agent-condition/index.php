<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\configuration\models\AgentConditionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agent Conditions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-condition-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Create Agent Condition', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'num_order',
            'essay_id',
            'crop_id',
            'request_type_id',
            'composite_sample_type_id',
            'request_process_essay_type_id',
            'agent_id',
            'reprocess_flag',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
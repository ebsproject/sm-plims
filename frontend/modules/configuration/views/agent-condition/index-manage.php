<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

$this->title = 'Agent Conditions Manage';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="agent-condition-index">
  <h1><?= Html::encode($this->title) ?></h1>
  <div class="row">
    <div class="col-md-12">







      <div class="card card-outline card-secondary">
        <div class="card-header">
          <?= Html::a('Create Agent Condition', ['create'], ['class' => 'btn btn-success pull-right']) ?>
        </div>
        <div class="card-body">
          <?php Pjax::begin(); ?>
          <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            // 'layout'       => "{items}",
            'columns' => [
              // ['class' => 'yii\grid\SerialColumn'],            
              [
                'attribute' => 'essay_id',
                'enableSorting' => false,
              ],
              [
                'attribute' => 'crop_id',
                'enableSorting' => false,
              ],
              [
                'attribute' => 'request_type_id',
                'enableSorting' => false,
              ],
              [
                'attribute' => 'composite_sample_type_id',
                'enableSorting' => false,
              ],
              [
                'attribute' => 'request_process_essay_type_id',
                'enableSorting' => false,
              ],
              [
                'attribute' => 'agent_id',
                'enableSorting' => false,
              ],
              [
                'attribute' => 'reprocess_flag',
                'enableSorting' => false,
              ],
              [
                'attribute' => 'num_order',
                'enableSorting' => false,
              ],
              [
                'attribute' => 'repetition',
                'enableSorting' => false,
              ],
            ],
          ]); ?>
          <?php Pjax::end(); ?>
        </div>
      </div>




      <?php $form = ActiveForm::begin(); ?>

      <div class="card card-outline card-secondary">
        <div class="card-body">

          <div class="row">
            <div class="col-lg-12">
              <div class="form-group"><i class="icon glyphicon glyphicon-info-sign"></i>
                The update of agents is for a single essay, do not enter an agent list of more than one essay.
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-12">


              <label class="control-label">Agent Condition</label>
              <?= Html::textArea(
                'consolidated_agent_condition',
                null,
                [
                  'class' => 'form-control',
                  'rows' => 20
                ]
              ) ?>


            </div>
          </div>

        </div>
        <div class="card-footer">
          <?= Html::submitButton('Save', ['class' => 'btn btn-primary pull-right']) ?>
        </div>
      </div>

      <?php ActiveForm::end(); ?>









    </div>
  </div>
</div>
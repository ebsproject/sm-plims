<?php

use yii\helpers\Html;

$this->title = 'Create Agent Condition';
$this->params['breadcrumbs'][] = ['label' => 'Agent Conditions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-condition-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\SampleTypeByEssayInRequestType */

$this->title = $model->composite_sample_type_id;
$this->params['breadcrumbs'][] = ['label' => 'Sample Type By Essay In Request Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="sample-type-by-essay-in-request-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'composite_sample_type_id' => $model->composite_sample_type_id, 'essay_id' => $model->essay_id, 'request_type_id' => $model->request_type_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'composite_sample_type_id' => $model->composite_sample_type_id, 'essay_id' => $model->essay_id, 'request_type_id' => $model->request_type_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'composite_sample_type_id',
            'essay_id',
            'request_type_id',
            'num_order',
            // 'registered_by',
            // 'registered_at',
            // 'updated_by',
            // 'updated_at',
            // 'deleted_by',
            // 'deleted_at',
            // 'status',
        ],
    ]) ?>

</div>
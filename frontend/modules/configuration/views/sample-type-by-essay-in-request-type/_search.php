<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\SampleTypeByEssayInRequestTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sample-type-by-essay-in-request-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'composite_sample_type_id') ?>

    <?= $form->field($model, 'essay_id') ?>

    <?= $form->field($model, 'request_type_id') ?>

    <?= $form->field($model, 'num_order') ?>

    <?php // echo $form->field($model, 'registered_by') 
    ?>

    <?php // echo $form->field($model, 'registered_at') 
    ?>

    <?php // echo $form->field($model, 'updated_by') 
    ?>

    <?php // echo $form->field($model, 'updated_at') 
    ?>

    <?php // echo $form->field($model, 'deleted_by') 
    ?>

    <?php // echo $form->field($model, 'deleted_at') 
    ?>

    <?php // echo $form->field($model, 'status') 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\SampleTypeByEssayInRequestType */

$this->title = 'Update Sample Type By Essay In Request Type: ' . $model->composite_sample_type_id;
$this->params['breadcrumbs'][] = ['label' => 'Sample Type By Essay In Request Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->composite_sample_type_id, 'url' => ['view', 'composite_sample_type_id' => $model->composite_sample_type_id, 'essay_id' => $model->essay_id, 'request_type_id' => $model->request_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sample-type-by-essay-in-request-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

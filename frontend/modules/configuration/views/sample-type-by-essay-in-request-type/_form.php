<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\SampleTypeByEssayInRequestType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sample-type-by-essay-in-request-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'composite_sample_type_id')->textInput() ?>

    <?= $form->field($model, 'essay_id')->textInput() ?>

    <?= $form->field($model, 'request_type_id')->textInput() ?>

    <?= $form->field($model, 'num_order') ?>

    <?php // echo $form->field($model, 'registered_by')->textInput() 
    ?>

    <?php // echo $form->field($model, 'registered_at')->textInput() 
    ?>

    <?php // echo $form->field($model, 'updated_by')->textInput() 
    ?>

    <?php // echo $form->field($model, 'updated_at')->textInput() 
    ?>

    <?php // echo $form->field($model, 'deleted_by')->textInput() 
    ?>

    <?php // echo $form->field($model, 'deleted_at')->textInput() 
    ?>

    <?php // echo $form->field($model, 'status')->textInput(['maxlength' => true]) 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
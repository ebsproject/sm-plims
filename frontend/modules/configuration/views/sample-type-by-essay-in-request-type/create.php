<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\SampleTypeByEssayInRequestType */

$this->title = 'Create Sample Type By Essay In Request Type';
$this->params['breadcrumbs'][] = ['label' => 'Sample Type By Essay In Request Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sample-type-by-essay-in-request-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

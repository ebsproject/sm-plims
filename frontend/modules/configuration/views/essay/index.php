<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\configuration\models\EssaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Assays';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="essay-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p> -->
    <?php // Html::a('Create Assay', ['create'], ['class' => 'btn btn-success']) 
    ?>
    <!-- </p> -->

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'essay_id',
            'short_name',
            'long_name',
            'description',
            'details',
            'abbreviation',
            'style_class',
            //'registered_by',
            //'registered_at',
            //'updated_by',
            //'updated_at',
            //'deleted_by',
            //'deleted_at',
            //'status',
            //'essay_cost_id',
            [
                'attribute' => 'essay_type_id',
                'value' => 'essayType.short_name'
            ],
            //'essay_location_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Assay */

$this->title = 'Create Assay';
$this->params['breadcrumbs'][] = ['label' => 'Assays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="essay-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
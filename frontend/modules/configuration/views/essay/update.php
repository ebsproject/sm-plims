<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Assay */

$this->title = 'Update Assay: ' . $model->essay_id;
$this->params['breadcrumbs'][] = ['label' => 'Assays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->essay_id, 'url' => ['view', 'id' => $model->essay_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="essay-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Assay */

$this->title = $model->essay_id;
$this->params['breadcrumbs'][] = ['label' => 'Assays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="essay-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->essay_id], ['class' => 'btn btn-primary']) ?>
        <?php
        //  Html::a('Delete', ['delete', 'id' => $model->essay_id], [
        //     'class' => 'btn btn-danger',
        //     'data' => [
        //         'confirm' => 'Are you sure you want to delete this item?',
        //         'method' => 'post',
        //     ],
        // ]) 
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'essay_id',
            'short_name',
            'long_name',
            'description',
            'details',
            'registered_by',
            'registered_at',
            'updated_by',
            'updated_at',
            'deleted_by',
            'deleted_at',
            'status',
            'essay_cost_id',
            'essay_type_id',
            'essay_location_id',
        ],
    ]) ?>

</div>
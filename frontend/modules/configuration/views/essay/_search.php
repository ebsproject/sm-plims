<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\EssaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="essay-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'essay_id') ?>

    <?= $form->field($model, 'short_name') ?>

    <?= $form->field($model, 'long_name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'details') ?>

    <?php // echo $form->field($model, 'registered_by') ?>

    <?php // echo $form->field($model, 'registered_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'essay_cost_id') ?>

    <?php // echo $form->field($model, 'essay_type_id') ?>

    <?php // echo $form->field($model, 'essay_location_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

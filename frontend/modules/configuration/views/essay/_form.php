<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Assay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="essay-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'long_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'style_class')->textInput() ?>

    <?= $form->field($model, 'abbreviation')->textInput() ?>

    <?= $form->field($model, 'essay_type_id')->dropDownList($model->getParameter('essay', 'type'), ['prompt' => 'Select...']) ?>

    <?php //= $form->field($model, 'essay_location_id')->textInput() 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
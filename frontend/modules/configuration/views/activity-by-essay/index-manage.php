<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Activity By Assays';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-by-essay-index-manage">

  <h1><?= Html::encode($this->title) ?></h1>

  <hr>
  <div class="activity-by-essay-form">
    <h4><?= strtoupper(Html::encode($this->title)) ?> CONFIGURATION</h4>
    <?php Pjax::begin(['enablePushState' => true]); ?>
    <?php $form = ActiveForm::begin(
      [
        'options' => ['data-pjax' => true, 'id' => 'dynamic-activity-by-essay-form']
      ]
    ); ?>

    <p>
      <label class="control-label">Assay</label>
      <?= Select2::widget([
        'id' => 'select_id_1',
        'name' => 'select_name_1',
        'data' => $select_data_1,
        'value' => $select_value_1,
        'options' => [
          'placeholder' => 'Select a assay ...',
        ],
        'pluginEvents' => [
          'select2:select' => 'function() {  
                  document.getElementById("dynamic-activity-by-essay-form").submit();
                  }',
        ]
      ]);
      ?>
    </p>

    <p>
      <label class="control-label">Activity(s)</label>
      <?= Select2::widget([
        'id' => 'select_id_2',
        'name' => 'select_name_2',
        'data' => $select_data_2,
        'value' => $select_value_2,
        'maintainOrder' => true,
        'options' => [
          'placeholder' => '  Select one o more activities ...',
          'multiple' => true
        ],
      ]);
      ?>
    </p>

    <p>
      <?= Html::a(
        'Save',
        [
          'add-remove-activity-by-essay',
        ],
        [
          'class' => 'btn btn-primary',
          'title'        => 'Add/Remove Activity(s) By Assay',
          'data-method'  => 'post',
        ]
      ); ?>
    </p>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

  </div>

  <hr>
  <div class="activity-by-essay-form">
    <h4><?= strtoupper(Html::encode($this->title)) ?> REPORT LIST</h4>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
        'num_order',
        [
          'attribute' => 'essay_id',
          'value' => function ($model) {
            return $model->essay_id . " | " . $model->essay->short_name;
          },
        ],
        [
          'attribute' =>   'activity_id',
          'value' => function ($model) {
            return $model->activity_id  . " | " . $model->activity->short_name  . " [" . $model->activity->long_name . "]";
          },
        ],
        'registered_at',
        'updated_at',
        'status',
      ],
    ]); ?>
    <?php Pjax::end(); ?>

  </div>
</div>
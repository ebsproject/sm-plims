<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\ActivityByAssay */

$this->title = 'Update Activity By Assay: ' . $model->activity_id;
$this->params['breadcrumbs'][] = ['label' => 'Activity By Assays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->activity_id, 'url' => ['view', 'activity_id' => $model->activity_id, 'essay_id' => $model->essay_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="activity-by-essay-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
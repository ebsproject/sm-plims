<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\ActivityByAssay */

$this->title = 'Create Activity By Assay';
$this->params['breadcrumbs'][] = ['label' => 'Activity By Assays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-by-essay-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
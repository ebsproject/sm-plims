<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Crop */

// $this->title = $model->crop_id;
// $this->params['breadcrumbs'][] = ['label' => 'Crops', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;


$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['/admin/admin-configuration/management']];
$this->params['breadcrumbs'][] =  ['label' => 'Crops', 'url' => ['/configuration/crop/index']];
$this->params['breadcrumbs'][] = 'View';

// \yii\web\YiiAsset::register($this);

?>
<div class="crop-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->crop_id], ['class' => 'btn btn-primary']) ?>
        <?php
        //  Html::a('Delete', ['delete', 'id' => $model->crop_id], [
        //     'class' => 'btn btn-danger',
        //     'data' => [
        //         'confirm' => 'Are you sure you want to delete this item?',
        //         'method' => 'post',
        //     ],
        // ])
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'crop_id',
            'short_name',
            'long_name',
            'description',
            'registered_by',
            'registered_at',
            'updated_by',
            'updated_at',
            'deleted_by',
            'deleted_at',
            'status',
        ],
    ]) ?>

</div>
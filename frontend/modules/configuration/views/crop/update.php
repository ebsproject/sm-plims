<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Crop */

$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['/admin/admin-configuration/management']];
$this->params['breadcrumbs'][] =  ['label' => 'Crops', 'url' => ['/configuration/crop/index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="crop-update">

    <h1><?= 'Update Crop: ' . $model->crop_id ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
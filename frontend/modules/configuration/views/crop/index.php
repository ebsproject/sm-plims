<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;


$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['/admin/admin-configuration/management']];
$this->params['breadcrumbs'][] = 'Crops';

?>

<?php $form = ActiveForm::begin(['id' => 'form-1']); ?>

<div class="crop-index">
    <div class="row">
        <div class="col-md-8">
            <h1>Manage Crops</h1>
            <p>Parameter list and manage functions to create or update a parameter:</p>
        </div>

        <div class="col-md-2" style=" display: flex; justify-content: center; align-items: center;">
            <?= Select2::widget(
                [
                    'id' => 'institution_id',
                    'name' => 'institution',
                    'data' =>  $data_institution,
                    'value' => $value_institution_id,
                    'options' => [
                        'placeholder' => 'Select institution ...',
                        'class' => 'form-control',
                        'disabled' => $is_disabled,
                    ],
                    'pluginEvents' => [
                        'select2:select' => 'function() {  
                  document.getElementById("form-1").submit();
             }',
                    ]
                ]
            ); ?>
        </div>

        <div class="col-md-2" style=" display: flex; justify-content: center; align-items: center;">
            <?= Html::a(
                '<i class="fa fa-plus"></i> New Crop',
                [
                    'new-crop',
                    'institution_id' => $value_institution_id,
                    'institution' => $data_institution[$value_institution_id],
                ],
                [
                    'class' => 'btn btn-default pull-right',
                    'disabled' => $value_institution_id == 1,
                ]
            ) ?>
        </div>
    </div>

    <div class=" row">
        <div class="col-md-12">
            <div class="card card-outline card-secondary">
                <div class="card-body">
                    <label class="control-label">Agent(s) selected: </label>
                    <?php Pjax::begin(); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'institution',
                            'crop_id',
                            'short_name',
                            'long_name',
                            'description'
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
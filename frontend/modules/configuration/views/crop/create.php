<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\Crop */

$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['/admin/admin-configuration/management']];
$this->params['breadcrumbs'][] =  ['label' => 'Crops', 'url' => ['/configuration/crop/index']];
$this->params['breadcrumbs'][] = 'Create Crop';

?>
<div class="crop-create">

    <h1>Create Crop</h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data_form' => $data_form,
        'data_institution' => $data_institution,
    ]) ?>

</div>
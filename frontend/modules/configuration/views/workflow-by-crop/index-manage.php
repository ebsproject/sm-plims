<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Workflow By Crops';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="workflow-by-crop-index-manage">

  <h1><?= Html::encode($this->title) ?></h1>

  <hr>
  <div class="workflow-by-crop-form">
    <h4><?= strtoupper(Html::encode($this->title)) ?> CONFIGURATION</h4>
    <?php Pjax::begin(['enablePushState' => true]); ?>
    <?php $form = ActiveForm::begin(
      [
        'options' => ['data-pjax' => true, 'id' => 'dynamic-workflow-by-crop-form']
      ]
    ); ?>

    <p>
      <label class="control-label">Crop</label>
      <?= Select2::widget([
        'id' => 'select_id_1',
        'name' => 'select_name_1',
        'data' => $select_data_1,
        'value' => $select_value_1,
        'options' => [
          'placeholder' => 'Select a crop ...',
        ],
        'pluginEvents' => [
          'select2:select' => 'function() {  
                  document.getElementById("dynamic-workflow-by-crop-form").submit();
                  }',
        ]
      ]);
      ?>
    </p>

    <p>
      <label class="control-label">Workflow(s)</label>
      <?= Select2::widget([
        'id' => 'select_id_2',
        'name' => 'select_name_2',
        'data' => $select_data_2,
        'value' => $select_value_2,
        'maintainOrder' => true,
        'options' => [
          'placeholder' => 'Select one o more workflows ...',
          'multiple' => true
        ],
      ]);
      ?>
    </p>

    <p>
      <?= Html::a(
        'Save',
        [
          'add-remove-workflow-by-crop',
        ],
        [
          'class' => 'btn btn-primary',
          'title'        => 'Add/Remove Workflow(s) By Crop',
          'data-method'  => 'post',
        ]
      ); ?>
    </p>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

  </div>

  <hr>
  <div class="workflow-by-crop-form">
    <h4><?= strtoupper(Html::encode($this->title)) ?> REPORT LIST</h4>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
        'num_order',
        [
          'attribute' => 'crop_id',
          'value' => 'crop.short_name'
        ],
        [
          'attribute' =>   'workflow_id',
          'value' =>   'workflow.short_name'
        ],
        'registered_at',
        'updated_at',
        'status',
      ],
    ]); ?>
    <?php Pjax::end(); ?>

  </div>
</div>
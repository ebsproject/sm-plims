<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\WorkflowByCrop */

$this->title = 'Update Workflow By Crop: ' . $model->workflow_id;
$this->params['breadcrumbs'][] = ['label' => 'Workflow By Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->workflow_id, 'url' => ['view', 'workflow_id' => $model->workflow_id, 'crop_id' => $model->crop_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="workflow-by-crop-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\WorkflowByCrop */

$this->title = $model->workflow_id;
$this->params['breadcrumbs'][] = ['label' => 'Workflow By Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="workflow-by-crop-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'workflow_id' => $model->workflow_id, 'crop_id' => $model->crop_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'workflow_id' => $model->workflow_id, 'crop_id' => $model->crop_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'workflow_id',
            'crop_id',
            'num_order',
            'registered_by',
            'registered_at',
            'updated_by',
            'updated_at',
            'deleted_by',
            'deleted_at',
            'status',
        ],
    ]) ?>

</div>

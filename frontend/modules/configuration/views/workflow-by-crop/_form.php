<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\WorkflowByCrop */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="workflow-by-crop-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'workflow_id')->textInput() ?>

    <?= $form->field($model, 'crop_id')->textInput() ?>

    <?= $form->field($model, 'num_order')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
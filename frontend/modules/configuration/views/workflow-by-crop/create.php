<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\WorkflowByCrop */

$this->title = 'Create Workflow By Crop';
$this->params['breadcrumbs'][] = ['label' => 'Workflow By Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="workflow-by-crop-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

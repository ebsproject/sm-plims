<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Agent By Essay In Crop And Request Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-by-essay-in-crop-and-request-type-index">

  <h1><?= Html::encode($this->title) ?></h1>





  <hr>
  <div class="agent-by-essay-in-crop-form">
    <h4><?= strtoupper(Html::encode($this->title)) ?> CONFIGURATION</h4>
    <?php Pjax::begin(['enablePushState' => true]); ?>
    <?php $form = ActiveForm::begin(
      [
        'options' => ['data-pjax' => true, 'id' => 'dynamic-agent-by-essay-in-crop-form']
      ]
    ); ?>

    <p>
      <label class="control-label">Request Type</label>
      <?= Select2::widget([
        'id' => 'select_id_0',
        'name' => 'select_name_0',
        'data' => $select_data_0,
        'value' => $select_value_0,
        'options' => [
          'placeholder' => 'Select a request type ...',
        ],
        'pluginEvents' => [
          'select2:select' => 'function() {  
                  document.getElementById("dynamic-agent-by-essay-in-crop-form").submit();
                  }',
        ]
      ]);
      ?>
    </p>

    <p>
      <label class="control-label">Crop</label>
      <?= Select2::widget([
        'id' => 'select_id_1',
        'name' => 'select_name_1',
        'data' => $select_data_1,
        'value' => $select_value_1,
        'options' => [
          'placeholder' => 'Select a crop ...',
        ],
        'pluginEvents' => [
          'select2:select' => 'function() {  
                  document.getElementById("dynamic-agent-by-essay-in-crop-form").submit();
                  }',
        ]
      ]);
      ?>
    </p>

    <p>
      <label class="control-label">Essay</label>
      <?= Select2::widget([
        'id' => 'select_id_2',
        'name' => 'select_name_2',
        'data' => $select_data_2,
        'value' => $select_value_2,
        'options' => [
          'placeholder' => 'Select a essay ...',
        ],
        'pluginEvents' => [
          'select2:select' => 'function() {  
                  document.getElementById("dynamic-agent-by-essay-in-crop-form").submit();
                  }',
        ]
      ]);
      ?>
    </p>

    <p>
      <label class="control-label">Agent(s)</label>
      <?= Select2::widget([
        'id' => 'select_id_3',
        'name' => 'select_name_3',
        'data' => $select_data_3,
        'value' => $select_value_3,
        'maintainOrder' => true,
        'options' => [
          'placeholder' => 'Select one o more agents ...',
          'multiple' => true
        ],
      ]);
      ?>
    </p>

    <p>
      <?= Html::a(
        'Save',
        [
          'add-remove-agent-by-essay-in-crop',
        ],
        [
          'class' => 'btn btn-primary',
          'title'        => 'Add/Remove Agent(s) By Essay in Crop',
          'data-method'  => 'post',
        ]
      ); ?>
    </p>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
  </div>



  <hr>

  <div class="agent-by-essay-in-crop-form">
    <h4><?= strtoupper(Html::encode($this->title)) ?> REPORT LIST</h4>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'columns' => [
        'num_order',
        [
          'attribute' => 'crop_id',
          'value' => 'crop.short_name'
        ],
        [
          'attribute' =>   'essay_id',
          'value' =>   'essay.short_name'
        ],
        [
          'attribute' =>   'agent_id',
          'value' =>   'agent.short_name'
        ],
      ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>

</div>
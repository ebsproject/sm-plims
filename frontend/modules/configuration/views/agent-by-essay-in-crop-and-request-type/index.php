<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\configuration\models\AgentByEssayInCropAndRequestTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agent By Essay In Crop And Request Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-by-essay-in-crop-and-request-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Agent By Essay In Crop And Request Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'agent_id',
            'essay_id',
            'crop_id',
            'num_order',
            'registered_by',
            //'registered_at',
            //'updated_by',
            //'updated_at',
            //'deleted_by',
            //'deleted_at',
            //'status',
            //'request_type_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

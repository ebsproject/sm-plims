<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\AgentByEssayInCropAndRequestType */

$this->title = 'Create Agent By Essay In Crop And Request Type';
$this->params['breadcrumbs'][] = ['label' => 'Agent By Essay In Crop And Request Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-by-essay-in-crop-and-request-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\AgentByEssayInCropAndRequestType */

$this->title = 'Update Agent By Essay In Crop And Request Type: ' . $model->agent_id;
$this->params['breadcrumbs'][] = ['label' => 'Agent By Essay In Crop And Request Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->agent_id, 'url' => ['view', 'agent_id' => $model->agent_id, 'essay_id' => $model->essay_id, 'crop_id' => $model->crop_id, 'request_type_id' => $model->request_type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agent-by-essay-in-crop-and-request-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

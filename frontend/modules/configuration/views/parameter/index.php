<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use frontend\modules\configuration\models\Parameter;
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\configuration\models\ParameterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Parameters';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parameter-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Parameter', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'parameter_id',
            // 'code',
            // 'singularity',
            // 'entity',
            [
                'attribute' => 'short_name',
                'format' => 'raw'
            ],
            //'long_name',
            //'description',
            //'class_details',
            //'registered_by',
            //'registered_at',
            //'updated_by',
            //'updated_at',
            //'deleted_by',
            //'deleted_at',
            //'status',
            // [
            //     'class' => ActionColumn::class,
            //     'urlCreator' => function ($action, Parameter $model, $key, $index, $column) {
            //         return Url::toRoute([$action, 'parameter_id' => $model->parameter_id]);
            //     }
            // ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
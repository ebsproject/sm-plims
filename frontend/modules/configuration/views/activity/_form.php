<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'long_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>



    <!--  $form->field($model, 'activity_type_id')->dropDownList($model->getParameter('activity', 'type'), ['prompt' => 'Select...'])  -->



    <!-- $form->field($model, 'activity_datasource_id')->textInput()   -->



    <?= $form
        ->field($model, 'activity_property_id')
        ->dropDownList(
            $model->getParameter('activity', 'property'),
            ['prompt' => 'Select...']
        ) ?>



    <!-- $form->field($model, 'activity_group_id')->textInput()  -->



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
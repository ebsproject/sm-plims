<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->activity_id;
$this->params['breadcrumbs'][] = ['label' => 'Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="activity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->activity_id], ['class' => 'btn btn-primary']) ?>
        <?php
        //  Html::a('Delete', ['delete', 'id' => $model->activity_id], [
        //     'class' => 'btn btn-danger',
        //     'data' => [
        //         'confirm' => 'Are you sure you want to delete this item?',
        //         'method' => 'post',
        //     ],
        // ]) 
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'activity_id',
            'short_name',
            'long_name',
            'description',
            'details',

            [
                'label' => 'Type',
                'value' => $model->activityType->short_name
            ],
            // 'activity_datasource_id',
            [
                'label' => 'Property',
                'value' =>  $model->activityProperty->short_name
            ],
            // 'activity_group_id',

            'registered_by',
            'registered_at',
            'updated_by',
            'updated_at',
            'deleted_by',
            'deleted_at',
            'status',

        ],
    ]) ?>

</div>
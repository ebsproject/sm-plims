<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\ActivitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'activity_id') ?>

    <?= $form->field($model, 'short_name') ?>

    <?= $form->field($model, 'long_name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'details') ?>

    <?php // echo $form->field($model, 'registered_by') 
    ?>

    <?php // echo $form->field($model, 'registered_at') 
    ?>

    <?php // echo $form->field($model, 'updated_by') 
    ?>

    <?php // echo $form->field($model, 'updated_at') 
    ?>

    <?php // echo $form->field($model, 'deleted_by') 
    ?>

    <?php // echo $form->field($model, 'deleted_at') 
    ?>

    <?php // echo $form->field($model, 'status') 
    ?>

    <?php // echo $form->field($model, 'activity_type_id') 
    ?>

    <?php // echo $form->field($model, 'activity_datasource_id') 
    ?>

    <?php // echo $form->field($model, 'activity_property_id') 
    ?>

    <?php // echo $form->field($model, 'activity_group_id') 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\configuration\models\ActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Activity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'activity_id',
            'short_name',
            'long_name',
            // 'description',
            // 'details',
            [
                'attribute' => 'activity_type_id',
                'value' => 'activityType.short_name'
            ],
            // [
            //     'attribute' => 'activity_group_id',
            //     'value' => 'activityGroup.short_name'
            // ],
            [
                'attribute' => 'activity_property_id',
                'value' => 'activityProperty.short_name'
            ],
            // [
            //     'attribute' => 'activity_datasource_id',
            //     'value' => 'activityDatasource.short_name'
            // ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
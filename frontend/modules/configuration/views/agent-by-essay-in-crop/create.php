<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\AgentByEssayInCrop */

$this->title = 'Create Agent By Essay In Crop';
$this->params['breadcrumbs'][] = ['label' => 'Agent By Essay In Crops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-by-essay-in-crop-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Agent By Essays';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-by-essay-index-manage">

  <h1><?= Html::encode($this->title) ?></h1>

  <hr>
  <div class="agent-by-essay-form">
    <h4><?= strtoupper(Html::encode($this->title)) ?> CONFIGURATION</h4>
    <?php Pjax::begin(['enablePushState' => true]); ?>
    <?php $form = ActiveForm::begin(
      [
        'options' => ['data-pjax' => true, 'id' => 'dynamic-agent-by-essay-form']
      ]
    ); ?>

    <p>
      <label class="control-label">Essay</label>
      <?= Select2::widget([
        'id' => 'select_id_1',
        'name' => 'select_name_1',
        'data' => $select_data_1,
        'value' => $select_value_1,
        'options' => [
          'placeholder' => 'Select a essay ...',
        ],
        'pluginEvents' => [
          'select2:select' => 'function() {  
                  document.getElementById("dynamic-agent-by-essay-form").submit();
                  }',
        ]
      ]);
      ?>
    </p>

    <p>
      <label class="control-label">Agent(s)</label>
      <?= Select2::widget([
        'id' => 'select_id_2',
        'name' => 'select_name_2',
        'data' => $select_data_2,
        'value' => $select_value_2,
        'maintainOrder' => true,
        'options' => [
          'placeholder' => 'Select one o more agents ...',
          'multiple' => true
        ],
      ]);
      ?>
    </p>

    <p>
      <?= Html::a(
        'Save',
        [
          'add-remove-agent-by-essay',
        ],
        [
          'class' => 'btn btn-primary',
          'title'        => 'Add/Remove Agent(s) By Essay',
          'data-method'  => 'post',
        ]
      ); ?>
    </p>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>

  </div>

  <hr>
  <div class="agent-by-essay-form">
    <h4><?= strtoupper(Html::encode($this->title)) ?> REPORT LIST</h4>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'columns' => [
        'num_order',
        [
          'attribute' => 'essay_id',
          'value' => 'essay.short_name'
        ],
        [
          'attribute' =>   'agent_id',
          'value' =>   'agent.short_name'
        ],
        'registered_at',
        'updated_at',
        'status',
      ],
    ]); ?>
    <?php Pjax::end(); ?>

  </div>

  <?php


  // echo "<pre>";
  // print_r($select_data_2);
  // echo "</pre>";




  // echo "<pre>";
  // print_r($select_value_2);
  // echo "</pre>";



  // echo "<pre>";
  // print_r($agent_by_essay_model);
  // echo "</pre>";




  ?>
</div>
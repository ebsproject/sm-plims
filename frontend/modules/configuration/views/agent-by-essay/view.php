<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\AgentByEssay */

$this->title = $model->agent_id;
$this->params['breadcrumbs'][] = ['label' => 'Agent By Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="agent-by-essay-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'agent_id' => $model->agent_id, 'essay_id' => $model->essay_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'agent_id' => $model->agent_id, 'essay_id' => $model->essay_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'agent_id',
            'essay_id',
            'num_order',
            'registered_by',
            'registered_at',
            'updated_by',
            'updated_at',
            'deleted_by',
            'deleted_at',
            'status',
        ],
    ]) ?>

</div>

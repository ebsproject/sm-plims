<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\AgentByEssay */

$this->title = 'Create Agent By Essay';
$this->params['breadcrumbs'][] = ['label' => 'Agent By Essays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-by-essay-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use frontend\modules\functional\Functional;

$this->title = 'Set params';
$code = Yii::$app->request->get('code');
$crop = Yii::$app->request->get('crop');
$assay = Yii::$app->request->get('assay');
$request_type = Yii::$app->request->get('request_type');
$sample_type = Yii::$app->request->get('sample_type');

$special_property = Yii::$app->request->get('special_property', Functional::PARAMETER_UNDETERMINED);



$disabled = (empty($code) || is_null($code)) ? false : true;
$disabled_assay = (in_array($assay, $disabled_assays_array)) ? true : false;
?>
<style>
  .table-bordered>tbody {
    text-align: center;
  }
</style>
<?php $form = ActiveForm::begin(); ?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link"><?= Html::encode($this->title) ?></h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-4">
        <div style="display:initial">
          <label class="control-label">Crop </label>
          <?= Select2::widget(
            [
              'id' => 'crop_id',
              'name' => 'crop',
              'data' => $data_crop,
              'value' => $crop,
              'options' => [
                'placeholder' => 'Select crop ...',
                'disabled' => $disabled
              ],
            ]
          );
          ?>
        </div>
        <br>
        <div style="display:initial">
          <label class="control-label">Assay </label>
          <?= Select2::widget(
            [
              'id' => 'assay_id',
              'name' => 'assay',
              'data' => $data_assay,
              'value' => $assay,
              'options' => [
                'placeholder' => 'Select assay ...',
                'disabled' => $disabled
              ],
              'pluginEvents' => [
                'select2:opening' => "function() {
                    var disabledOptions = " . $disabled_assays . "; 
                    var select = $('#assay_id');

                    $.each(disabledOptions, function(index, value) {
                        var option = select.find('option[value=' + value + ']');
                        option.prop('disabled', true);
                    });
                }"
              ]
            ]
          );
          ?>
        </div>
      </div>
      <div class="col-lg-4">
        <div style="display:initial">
          <label class="control-label">Request type</label>
          <?= Select2::widget(
            [
              'id' => 'request_type_id',
              'name' => 'request_type',
              'data' => $data_request_type,
              'value' => $request_type,
              'options' => [
                'placeholder' => 'Select request type ...',
                'disabled' => $disabled
              ],
            ]
          );
          ?>
        </div>
        <br>
        <div style="display:initial">
          <label class="control-label">Sample type </label>
          <?= Select2::widget(
            [
              'id' => 'sample_type_id',
              'name' => 'sample_type',
              'data' => $data_sample_type,
              'value' => $sample_type,
              'options' => [
                'placeholder' => 'Select sample type ...',
                'disabled' => $disabled
              ],
            ]
          );
          ?>
        </div>
      </div>
      <div class="col-lg-4">
        <div style="display:initial">
          <label class="control-label">Special property </label>
          <?= Select2::widget(
            [
              'id' => 'special_property_id',
              'name' => 'special_property',
              'data' => $data_special_property,
              'value' => $special_property,
              'options' => [
                'placeholder' => 'Select special property ...',
                'disabled' => $disabled
              ],
            ]
          );
          ?>
        </div>
        <br>
        <div class="row">
          <div class="col-lg-6">
            <label class="control-label" title="Condition process type">Number of repetitions</label>
            <?= Select2::widget(
              [
                'id' => 'repetitions_id',
                'name' => 'repetitions',
                'data' => ['1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5],
                'value' => Yii::$app->request->get('repetitions', '1'),
                'options' => [
                  'placeholder' => 'Select number of repetitions ...',
                  'disabled' =>  $disabled ? $disabled : $disabled_assay
                ],
              ]
            );
            ?>
          </div>
          <div class="col-lg-6">
            <div style="display:initial">
              <label>
                <span class="control-label" id="">Trigger a reprocess</span>


                <input type="checkbox" name="is_reprocessed" <?= $disabled_assay ? 'disabled' : '' ?> <?= Yii::$app->request->get('is_reprocessed') == 'on' ? 'checked' : '' ?>>
              </label>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--  -->
    <br>
    <div class="row">
      <div class="col-lg-6">
        <label class="control-label">Select agent(s): </label>
        <div class="input-group">
          <?= Select2::widget(
            [
              'id' => 'agent_selected_id',
              'name' => 'agent_selected',
              'data' => $data_agent,
              'value' => $agent_selected,
              'maintainOrder' => true,
              'options' => [
                'placeholder' => 'Select one o more agent(s)...',
                'multiple' => true,
                'allowClear' => true,
                'disabled' => $disabled_assay
              ]
            ]
          );  ?>
          <span class="input-group-btn">
            <?= $disabled_assay ?: Html::a(
              'Add Agent(s)',
              [
                'add-agent',
                'code' =>  $code,
                'crop' =>  $crop,
                'assay' =>  $assay,
                'request_type' =>  $request_type,
                'sample_type' =>  $sample_type,
                'special_property' =>  $special_property
              ],
              [
                'class' => 'btn btn-default',
                'data-method' => 'post',
              ]
            ) ?>
          </span>
        </div>
      </div>
      <div class="col-lg-6">
        <label class="control-label">Agent(s) selected: </label>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'layout' => "{items}{summary}{pager}",
          'columns' =>
          [
            [
              'attribute' => '#',
              'value' => 'order',
            ],
            [
              'attribute' => 'name',
              'value' => 'name',
            ],
            [
              'class' => 'yii\grid\ActionColumn',
              'header'        => 'Up/Down',
              'template'      => '{up} {down}',
              'headerOptions' => ['width' => '80'],
              'buttons'       => [
                'up' => function ($url, $model, $key)  use ($code, $crop, $assay, $request_type, $sample_type, $special_property, $disabled_assay) {
                  if (!$disabled_assay) {
                    return   $key > 0 ? Html::a(
                      '<span class="fa fa-chevron-up" title="Up"></span>',
                      [
                        'up',
                        'key' => $key,
                        'code' => $code,
                        'crop' =>  $crop,
                        'assay' =>  $assay,
                        'request_type' =>   $request_type,
                        'sample_type' =>  $sample_type,
                        'special_property' =>  $special_property,
                      ],
                      [
                        'class' => 'btn btn-primary btn-xs',
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    ) : null;
                  } else {
                    return '';
                  }
                },
                'down' => function ($url, $model, $key) use ($rows, $code, $crop, $assay, $request_type, $sample_type, $special_property, $disabled_assay) {

                  if (!$disabled_assay) {
                    return $key <  $rows ? Html::a(
                      '<span class="fa fa-chevron-down" title="Down"></span>',
                      [
                        'down',
                        'key' => $key,
                        'code' => $code,
                        'crop' =>  $crop,
                        'assay' =>  $assay,
                        'request_type' =>   $request_type,
                        'sample_type' =>  $sample_type,
                        'special_property' =>  $special_property,
                      ],
                      [
                        'class' => 'btn btn-primary btn-xs',
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    ) : null;
                  } else {
                    return '';
                  }
                },
              ],
            ],
            [
              'class' => 'yii\grid\ActionColumn',
              'header'        => 'Remove',
              'template'      => '{remove}',
              'headerOptions' => ['width' => '80'],
              'buttons'       => [
                'remove' => function ($url, $model, $key) use ($code, $crop, $assay, $request_type, $sample_type, $special_property, $disabled_assay) {
                  if (!$disabled_assay) {
                    return   Html::a(
                      '<span class="fa fa-remove" title="Remove"></span>',
                      [
                        'remove-agent',
                        'key' => $key,
                        'code' => $code,
                        'crop' =>  $crop,
                        'assay' =>  $assay,
                        'request_type' =>   $request_type,
                        'sample_type' =>  $sample_type,
                        'special_property' =>  $special_property,
                      ],
                      [
                        'class' => 'btn btn-danger btn-xs',
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    );
                  } else {
                    return '';
                  }
                },
              ],
            ],
          ],
        ]); ?>
        <?php Pjax::end(); ?>
      </div>
    </div>
  </div>
  <?php if (!$disabled_assay) { ?>
    <div class="card-footer">
      <div class="row">
        <div class="col-lg-12">
          <div class="btn-group pull-right">
            <?= Html::a(
              is_null($code) ? 'Cancel' : 'Reset',
              [
                'cancel',
                'code' =>  $code
              ],
              [
                'class' => 'btn btn-default',
                'title'        => 'Cancel and clear all parameters',
              ]
            );
            ?>
            <?= Html::a(
              is_null($code) ? 'Save' : 'Update',
              [
                'save',
                'code' =>  $code,
                'crop' =>  $crop,
                'assay' =>  $assay,
                'request_type' =>  $request_type,
                'sample_type' =>  $sample_type,
                'special_property' =>  $special_property
              ],
              [
                'class' => 'btn btn-primary',
                'title'        => 'Save parameters in new condition',
                'data-method'  => 'post',
              ]
            );
            ?>
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
</div>
<?php ActiveForm::end(); ?>
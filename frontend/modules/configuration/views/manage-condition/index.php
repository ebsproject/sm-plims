<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\widgets\ListView;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;


$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['/admin/admin-configuration/management']];
$this->params['breadcrumbs'][] = 'Manage Conditions';

?>
<div class="index">
  <div class="row">
    <div class="col-md-8">
      <h1>Manage Conditions</h1>
      <p>Condition' list and manage functions to create or update a condition:</p>
    </div>
    <div class="col-md-4 init-row-margin">
      <?= Html::a('<i class="fa fa-plus"></i> New Condition', ['new-condition'], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>

  <?php $form = ActiveForm::begin(); ?>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-secondary">
        <div class="card-header">
          <h3 class="card-title text-blue-link">Clone condition </h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-4 init-row-margin">
              <?= Select2::widget(
                [
                  'id' => 'crop_selected_id',
                  'name' => 'crop_selected',
                  'data' => $crop_selected,
                  'value' => null,
                  'options' => [
                    'placeholder' => 'Select own crop...',
                  ]
                ]
              );  ?>
            </div>
            <div class="col-md-8 init-row-margin">
              <div class="input-group">
                <?= Select2::widget(
                  [
                    'id' => 'assay_crop_selected_id',
                    'name' => 'assay_crop_selected',
                    'data' => $assay_crop_selected,
                    'value' => null,
                    'options' => [
                      'placeholder' => 'Select a "Assay - [crop]" condition to clone...',
                    ]
                  ]
                );  ?>
                <span class="input-group-btn">
                  <?= Html::a(
                    'Clone condition',
                    [
                      'clone-condition',
                    ],
                    [
                      'class' => 'btn btn-default',
                      'data-method' => 'post',
                    ]
                  ) ?>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php ActiveForm::end(); ?>

  <div class="row">
    <div class="col-md-12">
      <div class="card card-outline card-secondary">
        <div class="card-body">

          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="false">Condition Tree View</a></li>
              <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Condition List</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_2-2">
                <?php Pjax::begin(); ?>
                <br>
                <?= GridView::widget([
                  'dataProvider' => $conditionDataProvider,
                  'layout' => "{items}{summary}{pager}",
                  'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                      'label' => 'Code',
                      'attribute' => 'code',
                      'format' => 'raw',
                    ],

                    [
                      'label' => '[Crop]',
                      'attribute' => 'crop_name',
                      'format' => 'raw',
                    ],
                    [
                      'label' => '[Assay]',
                      'attribute' => 'essay',
                      'format' => 'raw',
                    ],
                    [
                      'label' => '[Request type]',
                      'attribute' => 'request_type',
                      'format' => 'raw',
                    ],
                    [
                      'label' => '[Sample type]',
                      'attribute' => 'composite_sample_type',
                      'format' => 'raw',
                    ],
                    [
                      'label' => '[Special property]',
                      'attribute' => 'essay_type',
                      'format' => 'raw',
                    ],
                    [
                      'label' => 'It is reprocessable',
                      'attribute' => 'is_reprocessed',
                      'format' => 'raw',
                    ],
                    [
                      'label' => 'Agent(s) selected',
                      'attribute' => 'agent_count',
                      'value' => function ($model) {
                        return "<span   title='" . $model['agent_names'] . "'>" .  $model['agent_count']   . "</span>";
                      },
                      'format' => 'raw',
                    ],
                    [
                      'label' => 'user',
                      'attribute' => 'user',
                      'format' => 'raw',
                    ],
                    [
                      'class' => 'yii\grid\ActionColumn',
                      'header'        => 'Action',
                      'template'      => '{edit} {remove}',
                      'headerOptions' => ['width' => '80'],
                      'buttons'       => [
                        'edit' => function ($url, $model, $key) {
                          return Html::a(
                            '<span class="fa fa-edit" title="Edit"></span>',
                            [
                              'edit-condition',
                              'code' =>  $model['code'],
                            ],
                            [
                              'class' => 'btn btn-primary btn-xs',
                              'data' => [
                                'method' => 'post',
                              ],
                            ]
                          );
                        },
                        'remove' => function ($url, $model, $key) {
                          return Html::a(
                            '<span class="glyphicon glyphicon-trash" title="Edit"></span>',
                            [
                              'remove-condition',
                              'code' =>  $model['code'],
                            ],
                            [
                              'class' => 'btn btn-danger btn-xs',
                              'data' => [
                                'confirm' => 'ARE YOU SURE YOU WANT TO DELETE THIS CONDITION: ' . $model['code'] . '?',
                                'method' => 'post',
                              ],
                            ]
                          );
                        },

                      ],
                    ],
                  ],
                ]); ?>
                <?php Pjax::end(); ?>
              </div>
              <div class="tab-pane active" id="tab_1-1">
                <br>
                <?= ListView::widget([
                  'dataProvider' => $conditionDataProviderNew,
                  'itemView' => '_item_view_condition',
                  'layout' => '{items}{pager}{summary}',
                  'pager' => [
                    'firstPageLabel' => 'first',
                    'lastPageLabel' => 'last',
                    'prevPageLabel' => 'previous',
                    'nextPageLabel' => 'next',
                  ],
                  'viewParams' => [
                    'fullView' => true,
                    'context' => 'main-page',
                  ],
                ]); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;
use Yii;

$this->title = 'Configurations';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/admin/admin-configuration/management']];
$this->params['breadcrumbs'][] = ['label' => 'Manage Conditions', 'url' => ['/configuration/manage-condition/index']];
$this->params['breadcrumbs'][] = 'Condition setting';

?>


<div class="management">
  <!-- #region BACK -->
  <div class="row">
    <div class="col-md-8">
      <h1><?= is_null($code) ? 'Condition setting' : 'Condition setting (<span class="text-blue-link">' . $code . '</span>)' ?></h1>
    </div>
    <div class="col-md-4 init-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back',  ['manage-condition/index'], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->


  <div class="row">
    <div class="col-lg-12 col-md-12">
      <?= $this->render('_view-a-set-params', [
        'data_crop' => $data_crop,
        'data_assay' => $data_assay,
        'data_request_type' => $data_request_type,
        'data_sample_type' => $data_sample_type,
        'data_special_property' => $data_special_property,
        'data_agent' => $data_agent,
        'agent_selected' => $agent_selected,
        'rows' => $rows,
        'dataProvider' => $dataProvider,
        'disabled_assays' => $disabled_assays,
        'disabled_assays_array' => $disabled_assays_array
      ])  ?>
    </div>
  </div>


  <!-- #region BACK -->
  <div class="row ">
    <div class="col-md-12 final-row-margin">
      <?= Html::a('<i class="fa fa-reply"></i> Back',  ['manage-condition/index'], ['class' => 'btn btn-default pull-right']) ?>
    </div>
  </div>
  <!-- end region -->
</div>
<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<style>
  .condition {
    background-color: #f5f5f5;
    margin-bottom: 10px;
    padding: 15px;
    border-radius: 5px;
    box-shadow: 0 1px 1px rgb(0 0 0 / 10%);
  }
</style>

<div class="condition">
  <div class="row">
    <?php foreach ($model as $assay => $value1) { ?>
      <div class="col-lg-12">
        <h3 style="font-weight: 700;"> <?= $assay ?> </h3>
      </div>
      <?php foreach ($value1 as $crop => $value2) { ?>
        <div class="col-lg-6">
          <h4 style="font-style: italic; color: #4c4c4c;"> <?= $crop ?> </h4>
          <ul style="padding-left: 20px;">
            <?php foreach ($value2 as $request_type => $value3) { ?>
              <li style="border: solid 2px #4c4c4c; border-radius: 3px; padding-left: 10px; color: #f5f5f5; background-color: #4c4c4c;"> <?= $request_type ?> </li>
              <ul style="padding-left: 20px;">
                <?php foreach ($value3 as $sample_type => $value4) { ?>
                  <li> <?= $sample_type ?> </li>

                  <?php foreach ($value4 as $code => $value5) { ?>
                    <ul class="list-group" style="padding-left: 20px;">

                      <li class="list-group-item active"><span><?= $code ?></span></li>
                      <?php if ($value5['essay_type'] != 'undetermianted') { ?> <li class="list-group-item"><span style="font-weight: 700;"><?= $value5['essay_type'] ?></span></li><?php } ?>
                      <li class="list-group-item"><span>It is reprocessable: <?= $value5['is_reprocessed'] ?></span></li>
                      <li class="list-group-item"><span title="<?= $value5['agent_names'] ?>">Nro. agents: <?= $value5['agent_count'] ?></span></li>
                      <li class="list-group-item">
                        <?= Html::a(
                          '<i class="fa fa-edit"></i> Edit',
                          [
                            'edit-condition',
                            'code' => $code,
                          ],
                          [
                            'class' => 'form pull-right',
                            'style' => 'padding: 0px 5px',
                            'data' => [
                              'method' => 'post',
                            ],
                          ]
                        ); ?>

                        <?= Html::a(
                          '<i class="fa fa-trash"></i> Remove ',
                          [
                            'remove-condition',
                            'code' =>  $code,
                          ],
                          [
                            'class' => 'form pull-right text-red',
                            'style' => 'padding: 0px 5px',
                            'data' => [
                              'confirm' => 'ARE YOU SURE YOU WANT TO DELETE THIS CONDITION: ' .  $code . '?',
                              'method' => 'post',
                            ],
                          ]
                        ); ?>
                      </li>
                    </ul>

                  <?php } ?>

                <?php } ?>
              </ul>
            <?php } ?>
          </ul>
        </div>
      <?php } ?>
    <?php } ?>
  </div>
</div>
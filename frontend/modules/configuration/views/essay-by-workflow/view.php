<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\EssayByWorkflow */

$this->title = $model->essay_id;
$this->params['breadcrumbs'][] = ['label' => 'Essay By Workflows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="essay-by-workflow-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'essay_id' => $model->essay_id, 'workflow_id' => $model->workflow_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'essay_id' => $model->essay_id, 'workflow_id' => $model->workflow_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'essay_id',
            'workflow_id',
            'num_order',
            'registered_by',
            'registered_at',
            'updated_by',
            'updated_at',
            'deleted_by',
            'deleted_at',
            'status',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="essay-by-workflow-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'workflow_id')->textInput() ?>

    <?= $form->field($model, 'essay_id')->textInput() ?>

    <?= $form->field($model, 'num_order')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
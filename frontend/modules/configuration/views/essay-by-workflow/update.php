<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\configuration\models\EssayByWorkflow */

$this->title = 'Update Essay By Workflow: ' . $model->essay_id;
$this->params['breadcrumbs'][] = ['label' => 'Essay By Workflows', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->essay_id, 'url' => ['view', 'essay_id' => $model->essay_id, 'workflow_id' => $model->workflow_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="essay-by-workflow-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

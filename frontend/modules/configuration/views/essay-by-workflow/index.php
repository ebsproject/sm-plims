<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Essay By Workflows';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="essay-by-workflow-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Essay By Workflow', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'num_order',
            [
                'attribute' => 'workflow_id',
                'value' => 'workflow.short_name'
            ],
            [
                'attribute' =>   'essay_id',
                'value' =>   'essay.short_name'
            ],
            'registered_at',
            'updated_at',
            'status',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['management']];
$this->params['breadcrumbs'][] = 'Change pass';



?>
<div class="site-signup">
  <h1><?= Html::encode($this->title) ?></h1>
  <p>Please fill out the following fields to update pass:</p>
  <div class="row">
    <div class="col-lg-5">
      <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

      <p>
        <label class="control-label">Old password</label>
        <?= Html::input('password', 'old_password',  $old_password, ['id' => 'old_password_id', 'class' => 'form-control']) ?>
      </p>

      <p>
        <label class="control-label">New password</label>
        <?= Html::input('password', 'new_password',  $new_password, ['id' => 'new_password_id', 'class' => 'form-control']) ?>
      </p>

      <div class="form-group">


        <?= Html::a(
          'Update password',
          [
            'update-pass',
            '_task_id' => $_task_id,
            '_submit' => true
          ],
          [
            'class' => 'btn btn-primary',
            'name' => 'signup-button',
            'data-method'  => 'post',
          ]
        ); ?>


      </div>

      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
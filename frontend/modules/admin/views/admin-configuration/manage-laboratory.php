<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['management']];
$this->params['breadcrumbs'][] = 'Manage laboratory';
?>
<h1>Manage laboratory</h1>
<p>Please fill out the following fields create or update a laboratory:</p>
<?php $form = ActiveForm::begin(['id' => 'form-laboratory']); ?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link"><?= is_null($_id) ? 'New laboratory' : 'Update laboratory' ?></h3>
  </div>
  <div class="card-body">
    <div class="col-lg-6 col-md-6">
      <label class="control-label">Institution</label>
      <?= Select2::widget(
        [
          'id' => 'institution_id',
          'name' => 'institution',
          'data' => $data_institution,
          'value' =>  $data_form['institution_id'],
          'options' => [
            'placeholder' => 'Select institution ...',
            'class' => 'form-control',
          ],
          'pluginEvents' => [
            'select2:select' => 'function() {  
              document.getElementById("form-laboratory").submit();
            }',
          ]
        ]
      );
      ?>
      <br />
      <p>
        <label class="control-label">Laboratory Name</label>
        <?= Html::input('text', 'long_name',  $data_form['long_name'], ['class' => 'form-control',  'maxlength' => 245]) ?>
      </p>
      <p>
        <label class="control-label">Laboratory Abbreviation</label>
        <?= Html::input('text', 'short_name',  $data_form['short_name'], ['class' => 'form-control',  'maxlength' => 45]) ?>
      </p>
      <p>
        <label class="control-label">Request Prefix</label>
        <?= Html::input('text', 'request_prefix',  $data_form['request_prefix'], ['class' => 'form-control',  'maxlength' => 2]) ?>
      </p>
    </div>
    <div class="col-lg-6 col-md-6">
      <p>
        <label class="control-label">Description</label>
        <?= Html::textArea('description', $data_form['description'], ['class' => 'form-control', 'maxlength' => 300, 'rows' => 4]) ?>
      </p>
      <p>
        <?= Html::radioList('is_deleted', $data_form['is_deleted'], [true => 'deleted', false => 'active']) ?>
      </p>
    </div>
  </div>
  <div class="card-footer">
    <div class="btn-group pull-right">
      <?= Html::a(
        'Cancel',
        [
          'manage-laboratory',
          '_task_id' =>  $_task_id
        ],
        [
          'class' => 'btn btn-default',
        ]
      ); ?>
      <?= Html::a(
        is_null($_id) ? 'Save' : 'Update',
        [
          'save-laboratory',
          '_task_id' =>  $_task_id,
          '_id' =>  $_id,
        ],
        [
          'class' => 'btn btn-primary',
          'title'        => 'Save parameters in new condition',
          'data-method'  => 'post',
          'disabled' => $data_form['institution_id'] == 1 ? true : false
        ]
      ); ?>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Laboratory list </h3>
  </div>
  <div class="card-body">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
      'dataProvider' => $data_provider,
      'layout' => "{items}{summary}{pager}",
      'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'laboratory_id',
        'laboratory_abreviation',
        'laboratory_name',
        'description',
        // 'institution_id',
        'institution_name',
        'request_prefix',
        'registered_by',
        'registered_at',
        'updated_by',
        'updated_at',
        // [
        //   'attribute' => 'is_deleted',
        //   'format' => 'raw',
        // ],
        [
          'class' => 'yii\grid\ActionColumn',
          'header'        => 'Action',
          'template'      => '{edit}',
          'headerOptions' => ['width' => '80'],
          'buttons'       => [
            'edit' => function ($url, $model, $key) use ($_task_id) {
              return Html::a(
                '<span class="fa fa-pencil" title="go to do"></span>',
                [
                  'manage-laboratory',
                  '_task_id' => $_task_id,
                  '_submit' => true,
                  '_id' => $key,
                ],
                [
                  'class' => 'btn btn-primary btn-xs',
                  'data' => [
                    'method' => 'post',
                  ],
                ]
              );
            },
          ],
        ],
      ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
</div>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

$this->title = 'Configurations';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="modal-title text-white">Task Unavailable</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>The execution of this task is currently unavailable. Completion of previous tasks is required before proceeding with this task. There are currently dependent tasks that have not been completed yet.</p>
        <p>Please make sure all required tasks are completed before attempting this task again.</p>
      </div>
    </div>
  </div>
</div>


<h1>Configurations</h1>
<p>List of tasks belonging to an administrator role:</p>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Specific configurations </h3>
  </div>
  <div class="card-body">
    <p>Your role, institution and laboratory is:</p>
    <ul>
      <li><strong>Role:</strong> <?= $_role_name ?></li>
      <li><strong>Institution:</strong> <?= $_institution_name ?></li>
      <li><strong>Laboratory:</strong> <?= $_laboratory_name ?></li>
    </ul>
  </div>
  <div class="card-body">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'layout' => "{items}{summary}{pager}",
      'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'task_id',
        'short_name',
        [
          'attribute' => 'description',
          'label' => '#. Description',
        ],
        [
          'attribute' => 'task_status',
          'format' => 'raw',
          'value' => function ($model) {
            if ($model['task_status'] == 'PENDING TASK') {
              return '<span class="label label-red">' . $model['task_status'] . '</span>';
            } else {
              return '<span class="label label-green">' . $model['task_status'] . '</span>';
            }
          },
        ],
        [
          'label' => 'Blocked',
          'format' => 'raw',
          'value' => function ($model) {
            if ($model['is_blocked']) {
              return '<i class="fa fa-lock" aria-hidden="true"></i>';
            } else {
              return '<i class="fa fa-unlock" aria-hidden="true"></i>';
            }
          },
        ],
        [
          'class' => 'yii\grid\ActionColumn',
          'header' => 'Action',
          'template' => '{edit}',
          'headerOptions' => ['width' => '80'],
          'buttons' => [
            'edit' => function ($url, $model, $key) {
              $_attributes = [
                'class' => 'btn btn-primary btn-xs',
              ];
              if ($model['is_blocked']) {
                $_link =  ['my-model'];
                $_attributes['data-toggle'] = 'modal';
                $_attributes['data-target'] = '#myModal';
              } else {
                $_link =  [
                  'complete',
                  '_task_id' => $key,
                  '_task_link' => $model['task_link'],
                ];
                $_attributes['data'] = ['method' => 'post'];
              }
              return Html::a(
                '<span class="fa fa-play-circle" title="go to do"></span>',
                $_link,
                $_attributes
              );
            },
          ],
        ],
      ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
</div>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['management']];
$this->params['breadcrumbs'][] = 'Manage user';
?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="modal-title text-white">More information</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>This module allows for the management of roles within the system. There are up to 4 available roles:</p>
        <ul>
          <!-- <li><strong>Super Admin:</strong> Has access to all administrative settings and functions.</li> -->
          <li><strong>Institution Admin:</strong> Can manage specific institution-related settings.</li>
          <li><strong>Laboratory Admin:</strong> Can manage specific laboratory-related settings.</li>
          <li><strong>Normal User:</strong> Performs scientific evaluations and can create requests for scientific evaluations.</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<h1>Manage user</h1>
<p>Please fill out the following fields create or update a user:</p>

<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">User list </h3>
  </div>
  <div class="card-body">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
      'dataProvider' => $data_provider_user,
      'layout' => "{items}{summary}{pager}",
      'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'user_id',
        'username',
        'firstname',
        'lastname',
        'email',
        'status',
        [
          'attribute' => 'role',
          'label' => 'Role - Institution - Laboratory',
        ],
        'verification_token',
        [
          'attribute' => 'is_deleted',
          'format' => 'raw',
        ],
        [
          'class' => 'yii\grid\ActionColumn',
          'header'        => 'Action',
          'template'      => '{edit}',
          'headerOptions' => ['width' => '80'],
          'buttons'       => [
            'edit' => function ($url, $model, $key) use ($_task_id) {
              return Html::a(
                '<span class="fa fa-pencil" title="go to do"></span>',
                [
                  'manage-user',
                  '_task_id' => $_task_id,
                  '_submit' => true,
                  '_id' => $key,
                ],
                [
                  'class' => 'btn btn-primary btn-xs',
                  'data' => [
                    'method' => 'post',
                  ],
                ]
              );
            },
          ],
        ],
      ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
</div>
<?php $form = ActiveForm::begin(['id' => 'form-1']); ?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link"><?= is_null($_id) ? 'New user' : 'Update user' ?></h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12 col-md-12" style="text-align: right;">
        <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-info-circle"></i> More info</a>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <p>
          <label class="control-label">Username</label>
          <?= Html::input('text', 'username',  $data_form['username'], ['class' => 'form-control', 'disabled' => 'true']) ?>
        </p>
        <p>
          <label class="control-label">Email</label>
          <?= Html::input('email', 'email',  $data_form['email'], ['class' => 'form-control', 'disabled' => 'true']) ?>
        </p>
      </div>
      <div class="col-lg-6 col-md-6">
        <p>
          <label class="control-label">First name</label>
          <?= Html::input('text', 'firstname',  $data_form['firstname'], ['class' => 'form-control', 'disabled' => 'true']) ?>
        </p>
        <p>
          <label class="control-label">Last name</label>
          <?= Html::input('text', 'lastname',  $data_form['lastname'], ['class' => 'form-control', 'disabled' => 'true']) ?>
        </p>
      </div>
    </div>
    <div class="row">
      <?php if ($_id) { ?>

        <div class="col-lg-4 col-md-4">
          <label class="control-label">Role</label>
          <?= Select2::widget(
            [
              'id' => 'role_id',
              'name' => 'role',
              'data' => $data_role,
              'value' => $selected_role,
              'options' => [
                'placeholder' => 'Select role ...', 'class' => 'form-control',
                'disabled' => $_role_is_disabled,
              ],
              'pluginEvents' => [
                'select2:select' => 'function() {  
                  document.getElementById("form-1").submit();
                }',
              ]
            ]
          );
          ?>
          <br />
          <label class="control-label">Institution</label>
          <?= Select2::widget(
            [
              'id' => 'institution_id',
              'name' => 'institution',
              'data' => $data_institution,
              'value' => $selected_institution,
              'options' => [
                'placeholder' => 'Select institution ...', 'class' => 'form-control',
                'disabled' => $_institution_is_disabled,
              ],
              'pluginEvents' => [
                'select2:select' => 'function() {  
                  document.getElementById("form-1").submit();
                }',
              ]
            ]
          );
          ?>
          <br />
          <label class="control-label">Laboratory</label>
          <?= Select2::widget(
            [
              'id' => 'laboratory_id',
              'name' => 'laboratory',
              'data' => $data_laboratory,
              'value' => $selected_laboratory,
              'options' => [
                'placeholder' => 'Select laboratory ...', 'class' => 'form-control',
                'disabled' => $_laboratory_is_disabled,
              ],
            ]
          );
          ?>
        </div>
        <div class="col-lg-8 col-md-8">
          <?php Pjax::begin(); ?>
          <?= GridView::widget([
            'dataProvider' => $data_provider_role_institution_laboratory,
            'layout' => "{items}{summary}{pager}",
            'columns' => [
              ['class' => 'yii\grid\SerialColumn'],
              'username',
              'role_name',
              'institution_name',
              'laboratory_name',
              [
                'class' => 'yii\grid\ActionColumn',
                'header'        => 'Action',
                'template'      => '{remove}',
                'headerOptions' => ['width' => '80'],
                'buttons'       => [
                  'remove' => function ($url, $model, $key) use ($_task_id, $_id) {
                    return Html::a(
                      '<span class="fa fa-trash" title="remove register"></span>',
                      [
                        'remove-role',
                        '_task_id' => $_task_id,

                        '_user_id' => $model['user_id'],
                        '_role_id' => $model['role_id'],
                        '_institution_id' => $model['institution_id'],
                        '_laboratory_id' => $model['laboratory_id'],
                      ],
                      [
                        'class' => 'btn btn-danger btn-xs',
                        'data' => [
                          'method' => 'post',
                        ],
                      ]
                    );
                  },
                ],
              ],
            ],
          ]); ?>
          <?php Pjax::end(); ?>
        </div>
      <?php } ?>
    </div>
  </div>
  <div class="card-footer">
    <div class="btn-group pull-right">
      <?= Html::a(
        'Cancel',
        [
          'manage-user',
          '_task_id' =>  $_task_id
        ],
        [
          'class' => 'btn btn-default',
        ]
      ); ?>
      <?= Html::a(
        'Save',
        [
          'add-role',
          '_task_id' =>  $_task_id,
          '_id' =>  $_id,
        ],
        [
          'class' => 'btn btn-primary',
          'title'        => 'Save a new conditions to a role',
          'data-method'  => 'post',
        ]
      ); ?>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>
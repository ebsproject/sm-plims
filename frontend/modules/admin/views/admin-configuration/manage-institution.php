<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap4\ActiveForm;

$this->title = 'Configurations';
$this->params['breadcrumbs'][] =  ['label' => $this->title, 'url' => ['management']];
$this->params['breadcrumbs'][] = 'Manage institution';
?>

<h1>Manage institution</h1>
<p>Please fill out the following fields create or update a institution:</p>
<?php $form = ActiveForm::begin(['id' => 'form-institution']); ?>
<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link"><?= is_null($_id) ? 'New institution' : 'Update institution' ?></h3>
  </div>
  <div class="card-body">
    <div class="col-lg-6 col-md-6">
      <p>
        <label class="control-label">short_name</label>
        <?= Html::input('text', 'short_name',  $data_form['short_name'], ['class' => 'form-control']) ?>
      </p>
      <p>
        <label class="control-label">long_name</label>
        <?= Html::input('text', 'long_name',  $data_form['long_name'], ['class' => 'form-control']) ?>
      </p>
    </div>
    <div class="col-lg-6 col-md-6">
      <p>
        <label class="control-label">description</label>
        <?= Html::textArea('description', $data_form['description'], ['class' => 'form-control', 'maxlength' => 300, 'rows' => 2]) ?>
      </p>
      <p>
        <?= Html::radioList('is_deleted', $data_form['is_deleted'], [true => 'deleted', false => 'active']) ?>
      </p>
    </div>
  </div>
  <div class="card-footer">
    <div class="btn-group pull-right">
      <?= Html::a(
        'Cancel',
        [
          'manage-institution',
          '_task_id' =>  $_task_id
        ],
        [
          'class' => 'btn btn-default',
        ]
      ); ?>
      <?= Html::a(
        is_null($_id) ? 'Save' : 'Update',
        [
          'save-institution',
          '_task_id' =>  $_task_id,
          '_id' =>  $_id,
        ],
        [
          'class' => 'btn btn-primary',
          'title'        => 'Save parameters in new condition',
          'data-method'  => 'post',
        ]
      ); ?>
    </div>
  </div>
</div>
<?php ActiveForm::end(); ?>

<div class="card card-outline card-secondary">
  <div class="card-header">
    <h3 class="card-title text-blue-link">Institution list </h3>
  </div>
  <div class="card-body">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
      'dataProvider' => $data_provider,
      'layout' => "{items}{summary}{pager}",
      'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'institution_id',
        'short_name',
        'long_name',
        'description',
        'registered_by',
        'registered_at',
        'updated_by',
        'updated_at',
        // [
        //   'attribute' => 'is_deleted',
        //   'format' => 'raw',
        // ],
        [
          'class' => 'yii\grid\ActionColumn',
          'header'        => 'Action',
          'template'      => '{edit}',
          'headerOptions' => ['width' => '80'],
          'buttons'       => [
            'edit' => function ($url, $model, $key) use ($_task_id) {
              return Html::a(
                '<span class="fa fa-pencil" title="go to do"></span>',
                [
                  'manage-institution',
                  '_task_id' => $_task_id,
                  '_submit' => true,
                  '_id' => $key,
                ],
                [
                  'class' => 'btn btn-primary btn-xs',
                  'data' => [
                    'method' => 'post',
                  ],
                ]
              );
            },
          ],
        ],
      ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
</div>
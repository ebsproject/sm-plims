<?php

namespace frontend\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use common\models\User;
use frontend\modules\functional\Functional;

class AdminConfigurationController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::class,
        'denyCallback' => function ($rule, $action) {
          Yii::$app->session->setFlash('danger', \Yii::t('app', 'You do not have sufficient permissions to access this website'));
          return $this->goHome();
        },
        'rules' => [
          [
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [],
      ],
    ];
  }

  // Management
  public function actionManagement($_task_id = null)
  {
    $this->reviewStatus();

    try {
      $_user_role = Yii::$app->user->identity->role;
      $_user_institution = Yii::$app->user->identity->institution;
      $_user_laboratory = Yii::$app->user->identity->laboratory;

      $db = Yii::$app->db;
      $transaction = $db->beginTransaction();
      $data_row = Yii::$app->user->Identity->profile;
      $_role_name = $data_row['role_name'];
      $_institution_name = $data_row['institution_name'];
      $_laboratory_name = $data_row['laboratory_name'];

      $data_array = $db->createCommand(
        "SELECT  task_id,
                short_name,
                long_name,
                description,
                CASE
                  WHEN is_finished THEN 'FINISHED TASK'
                  ELSE 'PENDING TASK'
                END AS task_status,
                task_link,
                is_blocked,
                depends_on 
        FROM  plims.auth_task
        WHERE  role_id = :_role_id
                AND institution_id = :_institution_id
                AND laboratory_id = :_laboratory_id
                AND is_deleted = false
        ORDER  BY long_name; ",
        [
          ':_role_id' => $_user_role,
          ':_institution_id' => $_user_institution,
          ':_laboratory_id' => $_user_laboratory,
        ]
      )->queryAll();
      $transaction->commit();

      $data_provider = new ArrayDataProvider([
        'allModels' => $data_array,
        'sort' => [
          'attributes' => [
            'task_id',
            'short_name',
            'long_name',
            'description',
            'task_status'
          ],
        ],
        'key' => 'task_id',
        'pagination' => [
          'pageSize' => 10,
        ],
      ]);
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }


    return $this->render(
      'management',
      [
        'dataProvider' => $data_provider,
        '_role_name' => $_role_name,
        '_institution_name' => $_institution_name,
        '_laboratory_name' => $_laboratory_name,

        '_user_role' => $_user_role,
        '_user_institution' => $_user_institution,
        '_user_laboratory' => $_user_laboratory,
      ]
    );
  }

  public function actionComplete($_task_id, $_task_link)
  {
    return $this->redirect(
      [
        $_task_link,
        '_task_id' => $_task_id,
      ]
    );
  }

  public function actionUpdatePass($_task_id, $_submit = false)
  {
    $old_password = Yii::$app->request->post('old_password');
    $new_password = Yii::$app->request->post('new_password');

    $user_id = Yii::$app->user->identity->id;
    $user_model = User::findOne($user_id);

    if (!empty($new_password) && !empty($new_password)) {
      if ($user_model->validatePassword($old_password)) {
        try {
          $user_model->setPassword($new_password);
          $user_model->removePasswordResetToken();
          $user_model->generateAuthKey();
          if ($user_model->save()) {

            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();
            $db->createCommand(
              "UPDATE  plims.auth_task
            SET    is_finished = true
            WHERE  task_id = :_task_id
                  AND is_finished IS FALSE;",
              [
                ':_task_id' => $_task_id
              ]
            )->execute();

            $db->createCommand(
              "UPDATE  plims.auth_task
                SET is_blocked = false
                WHERE depends_on = (
                  SELECT short_name
                  FROM   plims.auth_task
                  WHERE  task_id = :_task_id); ",
              [
                ':_task_id' => $_task_id
              ]
            )->execute();

            $transaction->commit();
          }
        } catch (\Throwable $e) {
          $transaction->rollBack();
          throw $e;
        }
        Yii::$app->session->setFlash('success', \Yii::t('app', 'New password saved.'));

        return $this->goHome();
      } else {
        Yii::$app->session->setFlash('danger', \Yii::t('app', 'The entered password is incorrect!'));
      }
    } else if ($_submit) {
      Yii::$app->session->setFlash('danger', \Yii::t('app', 'Both password fields must be filled!'));
    }

    $old_password = '';
    $new_password = '';

    return $this->render(
      'update-pass',
      [
        'old_password' => $old_password,
        'new_password' => $new_password,
        '_task_id' => $_task_id,
      ]
    );
  }

  // Institution
  public function actionManageInstitution($_task_id, $_submit = false, $_id = null)
  {
    $data_form = [
      'short_name' => null,
      'long_name' => null,
      'description' => null,
      'is_deleted' => 0
    ];

    if ($_id) {
      $data_form = Yii::$app->db->createCommand(
        "SELECT short_name,
                long_name,
                description,
                case 
                  when is_deleted then 1 else 0 
                end as is_deleted
        FROM   plims.bsns_institution
        WHERE institution_id = :_institution_id
        ORDER  BY institution_id;",
        [
          ':_institution_id' => $_id
        ]
      )->queryOne();
    }

    $data_array = Yii::$app->db->createCommand(
      "SELECT institution_id,
              short_name,
              long_name,
              description,
              registered_by,
              registered_at,
              updated_by,
              updated_at,
              CASE 
                WHEN is_deleted THEN '<span class=''label label-red''>deleted</span>'
                ELSE '<span class=''label label-green''>active</span>'
              END AS is_deleted
      FROM   plims.bsns_institution
      WHERE is_deleted = false
      and institution_id <> 1
      ORDER  BY short_name;",
      []
    )->queryAll();

    $data_provider = new ArrayDataProvider([
      'allModels' => $data_array,
      'sort' => [
        'attributes' => [
          'institution_id',
          'short_name',
          'long_name',
          'description',
          'registered_by',
          'registered_at',
          'updated_by',
          'updated_at',
          'is_deleted'
        ],
      ],
      'key' => 'institution_id',
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return $this->render(
      'manage-institution',
      [
        '_task_id' => $_task_id,
        'data_provider' => $data_provider,
        'data_form' => $data_form,
        '_id' => $_id,
      ]
    );
  }

  public function actionSaveInstitution($_task_id, $_submit = false, $_id = null)
  {
    $user =  Yii::$app->user->identity->id;
    $date =  date("Y-m-d H:i:s", time());

    $_short_name = Yii::$app->request->post('short_name');
    $_long_name = Yii::$app->request->post('long_name');
    $_description = Yii::$app->request->post('description');
    $_is_deleted = Yii::$app->request->post('is_deleted');

    $redirect = [
      'management',
      '_task_id' => $_task_id,
    ];

    if ($_id) {
      $redirect = [
        'manage-institution',
        '_task_id' => $_task_id,
      ];
      // update institution
      Yii::$app->db->createCommand(
        "UPDATE plims.bsns_institution
        SET short_name = :_short_name,
            long_name = :_long_name,
            description = :_description,
            updated_by = :_updated_by,
            updated_at = :_updated_at,
            is_deleted = :_is_deleted
        WHERE institution_id = :_id;",
        [
          ':_short_name' => $_short_name,
          ':_long_name' => $_long_name,
          ':_description' => $_description,
          ':_updated_by' => $user,
          ':_updated_at' => $date,
          ':_is_deleted' => $_is_deleted,
          ':_id' => $_id
        ]
      )->execute();
    } else {
      // create a new institution
      $db = Yii::$app->db;
      $transaction = $db->beginTransaction();
      try {
        $db->createCommand(
          "INSERT INTO plims.bsns_institution
              (short_name, 
              long_name, 
              description, 
              registered_by, 
              registered_at, 
              is_deleted)
        VALUES (:_short_name, 
                :_long_name, 
                :_description, 
                :_registered_by, 
                :_registered_at, 
                :_is_deleted);",
          [
            ':_short_name' => $_short_name,
            ':_long_name' => $_long_name,
            ':_description' => $_description,
            ':_registered_by' => $user,
            ':_registered_at' => $date,
            ':_is_deleted' => $_is_deleted,
          ]
        )->execute();
        $_new_institution_id = $db->getLastInsertID();

        // and create a new task from institution
        $db->createCommand(
          "INSERT INTO plims.auth_task
                    (short_name,
                    long_name,
                    description,
                    task_link,
                    is_finished,
                    role_id,
                    registered_by,
                    registered_at,
                    institution_id,
                    laboratory_id,
                    is_blocked,
                    depends_on)
          SELECT short_name,
              long_name,
              description,
              task_link,
              :_is_finished,
              role_id,
              :_registered_by,
              :_registered_at,
              :_new_institution_id,
              laboratory_id,
              true,
              ''
          FROM plims.auth_task
          WHERE  is_deleted = false 
          AND institution_id = :_institution_token
          AND laboratory_id = :_laboratory_token
          AND role_id = :_institution_admin_role;",
          [
            ':_is_finished' => false,
            ':_registered_by' => $user,
            ':_registered_at' => $date,
            ':_new_institution_id' => $_new_institution_id,
            ':_institution_token' => Functional::INSTITUTION_TOKEN,
            ':_laboratory_token' => Functional::LABORATORY_TOKEN,
            ':_institution_admin_role' => Functional::INSTITUTION_ADMIN_ROLE,
          ]
        )->execute();

        // update task status 
        $db->createCommand(
          "UPDATE  plims.auth_task
            SET is_finished = true,
            updated_by = :_updated_by,
            updated_at = :_updated_at
            WHERE  task_id = :_task_id;",
          [
            ':_updated_by' => $user,
            ':_updated_at' => $date,
            ':_task_id' => $_task_id
          ]
        )->execute();

        // update blocked to next task
        $db->createCommand(
          "UPDATE plims.auth_task
          SET is_blocked = false,
          updated_by = :_updated_by,
          updated_at = :_updated_at
          WHERE  is_deleted = false
                 AND institution_id = :_institution_token
                 AND laboratory_id = :_laboratory_token
                 AND role_id = :_super_admin_role
                 AND depends_on = (SELECT short_name
                                   FROM   plims.auth_task
                                   WHERE  task_id = :_task_id);",
          [
            ':_updated_by' => $user,
            ':_updated_at' => $date,
            ':_institution_token' => Functional::INSTITUTION_TOKEN,
            ':_laboratory_token' => Functional::LABORATORY_TOKEN,
            ':_super_admin_role' => Functional::SUPER_ADMIN_ROLE,
            ':_task_id' => $_task_id
          ]
        )->execute();
        $transaction->commit();
      } catch (\Exception $e) {
        $transaction->rollBack();
        throw $e;
      }
    }

    return $this->redirect(
      $redirect
    );
  }

  // Laboratory
  public function actionManageLaboratory($_task_id, $_submit = false, $_id = null)
  {
    $_user_institution = Yii::$app->user->Identity->institution;
    $_user_role = Yii::$app->user->identity->role;
    $selected_institution = Yii::$app->request->post('institution', $_user_institution);
    $is_disabled = true;

    $data_form = [
      'short_name' => null,
      'long_name' => null,
      'description' => null,
      'request_prefix' => null,
      'institution_id' => $selected_institution,
      'is_deleted' => 0
    ];

    $query = "SELECT institution_id,
              short_name AS institution_name
              FROM   plims.bsns_institution
              WHERE is_deleted = false";
    $params = [];
    if ($_user_role === Functional::SUPER_ADMIN_ROLE) {
      $query =  $query .         " AND institution_id <> :_institution_token ";
      $params[':_institution_token'] =  Functional::INSTITUTION_TOKEN;
      $is_disabled = false;
    } else {
      $query =  $query . " AND institution_id = :_user_institution ";
      $params[':_user_institution'] =  $selected_institution;
    }
    $query = $query . "ORDER  BY short_name;";

    $data_institution = Yii::$app->db->createCommand($query, $params)->queryAll();
    $data_institution = ArrayHelper::map($data_institution, 'institution_id', 'institution_name');

    if ($_id) {
      $data_form = Yii::$app->db->createCommand(
        "SELECT short_name,
                long_name,
                description,
                request_prefix,
                institution_id,
                case 
                  when is_deleted then 1 else 0 
                end as is_deleted
        FROM   plims.bsns_laboratory
        WHERE laboratory_id = :_laboratory_id
        ORDER  BY laboratory_id;",
        [
          ':_laboratory_id' => $_id
        ]
      )->queryOne();
    }

    $data_array = Yii::$app->db->createCommand(
      "SELECT t1.laboratory_id,
            t1.short_name AS laboratory_abreviation,
            t1.long_name  AS laboratory_name,
            t1.description,
            t1.request_prefix,
            t2.institution_id,
            t2.long_name  AS institution_name,
            t1.registered_by,
            t1.registered_at,
            t1.updated_by,
            t1.updated_at,
            CASE
              WHEN t1.is_deleted THEN
              '<span class=''label label-red''>deleted</span>'
              ELSE '<span class=''label label-green''>active</span>'
            END           AS is_deleted
      FROM   plims.bsns_laboratory t1
            LEFT JOIN plims.bsns_institution t2
                  ON t1.institution_id = t2.institution_id
      WHERE (t1.institution_id = :_institution_id OR :_institution_id  IS NULL)
      AND t1.is_deleted = false
      AND t1.laboratory_id <> 1
      ORDER  BY t1.laboratory_id;",
      [
        ':_institution_id' => $selected_institution,
      ]
    )->queryAll();

    $data_provider = new ArrayDataProvider([
      'allModels' => $data_array,
      'sort' => [
        'attributes' => [
          'laboratory_id',
          'laboratory_abreviation',
          'laboratory_name',
          'description',
          'request_prefix',
          'institution_id',
          'institution_name',
          'registered_by',
          'registered_at',
          'updated_by',
          'updated_at',
          'is_deleted'
        ],
      ],
      'key' => 'laboratory_id',
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return $this->render(
      'manage-laboratory',
      [
        '_task_id' => $_task_id,
        'data_provider' => $data_provider,
        'data_form' => $data_form,
        'data_institution' => $data_institution,
        'is_disabled' => $is_disabled,
        '_id' => $_id,
      ]
    );
  }

  public function actionSaveLaboratory($_task_id, $_submit = false, $_id = null)
  {
    $user =  Yii::$app->user->identity->id;
    $date =  date("Y-m-d H:i:s", time());

    $_short_name = Yii::$app->request->post('short_name');
    $_long_name = Yii::$app->request->post('long_name');
    $_description = Yii::$app->request->post('description');
    $_is_deleted = Yii::$app->request->post('is_deleted');
    $_user_institution = Yii::$app->user->Identity->institution;
    $_selected_institution = Yii::$app->request->post('institution');
    $_request_prefix =  Yii::$app->request->post('request_prefix');

    if ($_id) {
      // update laboratory
      Yii::$app->db->createCommand(
        "UPDATE plims.bsns_laboratory
        SET short_name = :_short_name,
            long_name = :_long_name,
            description = :_description,
            request_prefix = :_request_prefix,
            institution_id = :_institution_id,
            updated_by = :_updated_by,
            updated_at = :_updated_at,
            is_deleted = :_is_deleted
        WHERE laboratory_id = :_id;",
        [
          ':_short_name' => $_short_name,
          ':_long_name' => $_long_name,
          ':_description' => $_description,
          ':_request_prefix' => $_request_prefix,
          ':_institution_id' => $_selected_institution,
          ':_updated_by' => $user,
          ':_updated_at' => $date,
          ':_is_deleted' => $_is_deleted,
          ':_id' => $_id
        ]
      )->execute();
    } else {
      // create new laboratory
      $db = Yii::$app->db;
      $transaction = $db->beginTransaction();
      try {
        $db->createCommand(
          "INSERT INTO plims.bsns_laboratory
                (short_name,
                long_name,
                description,
                request_prefix,
                institution_id,
                registered_by,
                registered_at,
                is_deleted)
        VALUES (:_short_name,
                :_long_name,
                :_description,
                :_request_prefix,
                :_institution_id,
                :_registered_by,
                :_registered_at,
                :_is_deleted);",
          [
            ':_short_name' => $_short_name,
            ':_long_name' => $_long_name,
            ':_description' => $_description,
            ':_request_prefix' => $_request_prefix,
            ':_institution_id' => $_selected_institution,
            ':_registered_by' => $user,
            ':_registered_at' => $date,
            ':_is_deleted' => $_is_deleted,
          ]
        )->execute();
        $_new_laboratory_id = $db->getLastInsertID();

        // and create a new task from laboratory
        $db->createCommand(
          "INSERT INTO plims.auth_task
                    (short_name,
                    long_name,
                    description,
                    task_link,
                    is_finished,
                    role_id,
                    registered_by,
                    registered_at,
                    institution_id,
                    laboratory_id)
          SELECT short_name,
              long_name,
              description,
              task_link,
              :_is_finished,
              role_id,
              :_registered_by,
              :_registered_at,
              :_institution_id,
              :_new_laboratory_id
          FROM plims.auth_task
          WHERE  is_deleted = false 
          AND institution_id = :_institution_token
          AND laboratory_id = :_laboratory_token
          AND role_id = :_laboratory_admin_role",
          [
            ':_is_finished' => false,
            ':_registered_by' => $user,
            ':_registered_at' => $date,
            ':_new_laboratory_id' => $_new_laboratory_id,
            ':_institution_id' => $_selected_institution,
            ':_institution_token' => Functional::INSTITUTION_TOKEN,
            ':_laboratory_token' => Functional::LABORATORY_TOKEN,
            ':_laboratory_admin_role' => Functional::LABORATORY_ADMIN_ROLE,
          ]
        )->execute();
        $transaction->commit();
      } catch (\Exception $e) {
        $transaction->rollBack();
        throw $e;
      }
    }
    return $this->redirect(
      [
        'manage-laboratory',
        '_task_id' => $_task_id,
      ]
    );
  }

  // User
  public function actionManageUser($_task_id, $_submit = false, $_id = null)
  {
    $_user_role = Yii::$app->user->identity->role;
    $_user = Yii::$app->user->identity->id;
    $_user_institution = Yii::$app->user->Identity->institution;
    $_user_laboratory = Yii::$app->user->Identity->laboratory;
    $selected_institution = Yii::$app->request->post('institution');
    $selected_laboratory = Yii::$app->request->post('laboratory');
    $selected_role = Yii::$app->request->post('role');

    $data_form = [
      'username' => null,
      'firstname' => null,
      'lastname' => null,
      'email' => null,
    ];
    $data_role_institution_laboratory = [];
    $_role_is_disabled = false;
    $_institution_is_disabled = false;
    $_laboratory_is_disabled = false;

    // ROLE
    $data_role =  Yii::$app->db->createCommand(
      "SELECT role_id,
                long_name AS role_name
          FROM   plims.auth_role 
          WHERE role_id > :_role
          ORDER  BY role_id;",
      [
        ':_role' => $_user_role
      ]
    )->queryAll();
    $data_role = ArrayHelper::map($data_role, 'role_id', 'role_name');

    // INSTITUTION
    $query_institution =
      "SELECT institution_id,
          short_name AS institution_name
      FROM   plims.bsns_institution
      WHERE  is_deleted = false
          AND institution_id <> 1 ";
    $params_institution = [];

    // LABORATORY
    $query_laboratory =
      "SELECT laboratory_id, 
      '[' ||  short_name || '] ' || long_name AS laboratory_name
      FROM   plims.bsns_laboratory
      WHERE  is_deleted = false
      AND laboratory_id <> 1 ";
    $params_laboratory = [];



    $query_laboratory = $query_laboratory . " AND institution_id = :_institution_id ";
    $params_laboratory[':_institution_id'] = $selected_institution;
    if ($_user_role === Functional::INSTITUTION_ADMIN_ROLE) {
      $query_institution = $query_institution . " AND institution_id = :_institution_id ";
      $params_institution[':_institution_id'] = $_user_institution;
    } else if ($_user_role === Functional::LABORATORY_ADMIN_ROLE) {
      $query_laboratory = $query_laboratory . " AND laboratory_id = :_laboratory_id ";
      $params_laboratory[':_laboratory_id'] = $_user_laboratory;

      $query_institution = $query_institution . " AND institution_id = :_institution_id ";
      $params_institution[':_institution_id'] = $_user_institution;
    }

    $query_institution = $query_institution . " ORDER  BY short_name;";
    $data_institution =  Yii::$app->db->createCommand($query_institution, $params_institution)->queryAll();
    $data_institution = ArrayHelper::map($data_institution, 'institution_id', 'institution_name');

    $query_laboratory = $query_laboratory . " ORDER  BY short_name;";
    $data_laboratory =  Yii::$app->db->createCommand($query_laboratory, $params_laboratory)->queryAll();
    $data_laboratory = ArrayHelper::map($data_laboratory, 'laboratory_id', 'laboratory_name');

    // update role in tabla user
    Yii::$app->db->createCommand(
      "UPDATE plims.auth_user AS tab_1
          SET    role = tab_2.min_role
          FROM   (
                  SELECT tb1.user_id,
                        COALESCE(Min(tb2.role_id), :_user_role) AS min_role
                  FROM   plims.auth_user tb1
                        LEFT JOIN plims.auth_user_by_role_institution_laboratory tb2
                                ON tb1.user_id = tb2.user_id AND tb2.is_deleted = false
                  GROUP  BY tb1.user_id
                  ) AS tab_2
          WHERE  tab_1.user_id = tab_2.user_id;",
      [':_user_role' => Functional::NORMAL_USER_ROLE,]
    )->execute();

    $data_user = Yii::$app->db->createCommand(
      "SELECT t1.user_id,
            t1.username,
            t1.firstname,
            t1.lastname,
            t1.auth_key,
            t1.password_hash,
            t1.password_reset_token,
            t1.email,
            t1.status,
            t2.info AS role,
            t1.verification_token
      FROM   plims.auth_user t1
            LEFT JOIN (SELECT t3.user_id,
                              t6.long_name
                              || ' - '
                              || CASE
                                  WHEN t4.institution_id = :_institution_token THEN
                                  '...'
                                  ELSE t4.short_name
                                END
                              || ' - '
                              || CASE
                                  WHEN t5.laboratory_id = :_laboratory_token THEN
                                  '...'
                                  ELSE t5.short_name
                                END AS info
                      FROM   plims.auth_user_by_role_institution_laboratory t3
                              LEFT JOIN plims.bsns_institution t4
                                    ON t3.institution_id = t4.institution_id
                              LEFT JOIN plims.bsns_laboratory t5
                                    ON t3.laboratory_id = t5.laboratory_id
                              LEFT JOIN plims.auth_role t6
                                    ON t3.role_id = t6.role_id
                      WHERE  t3.is_deleted = false
                              AND t4.is_deleted = false
                              AND t5.is_deleted = false
                              AND t5.is_deleted = false) t2
                  ON t1.user_id = t2.user_id
      WHERE  t1.role > :_role
      ORDER  BY t1.username,
              t1.email; ",
      [
        ':_role' => $_user_role,
        ':_institution_token' => Functional::INSTITUTION_TOKEN,
        ':_laboratory_token' => Functional::LABORATORY_TOKEN,
      ]
    )->queryAll();

    $data_provider_user = new ArrayDataProvider([
      'allModels' => $data_user,
      'sort' => [
        'attributes' => [
          'user_id',
          'username',
          'firstname',
          'lastname',
          'auth_key',
          'password_hash',
          'password_reset_token',
          'email',
          'status',
          'role',
          'verification_token',
        ],
      ],
      'key' => 'user_id',
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    // IF USER SELECTED
    if ($_id) {
      $data_form = Yii::$app->db->createCommand(
        "SELECT user_id,             
            username,            
            firstname,           
            lastname,            
            auth_key,            
            password_hash,       
            password_reset_token,
            email,               
            status,              
            role,                
            verification_token
        FROM   plims.auth_user
        WHERE user_id = :_user_id;",
        [
          ':_user_id' => $_id
        ]
      )->queryOne();

      // ==================================================== //

      switch ($selected_role) {
        case '2': // INSTITUTION_ADMIN_ROLE
          $_role_is_disabled = false;
          $_institution_is_disabled = false;
          $_laboratory_is_disabled = true;
          break;
        case '3': // LABORATORY_ADMIN_ROLE
          $_role_is_disabled = false;
          $_institution_is_disabled = false;
          $_laboratory_is_disabled = false;
          break;
        case '4': // NORMAL_USER_ROLE
          $_role_is_disabled = false;
          $_institution_is_disabled = false;
          $_laboratory_is_disabled = false;
          break;
        default: // SIN SELECCIONAR NINGUN ROLE
          $_role_is_disabled = false;
          $_institution_is_disabled = true;
          $_laboratory_is_disabled = true;
          break;
      }
      // ==================================================== //
      $data_role_institution_laboratory = Yii::$app->db->createCommand(
        "SELECT t1.user_id,
                t1.username,
                t2.role_id,
                t2.long_name as role_name,
                CASE
                  WHEN t2.is_deleted THEN 'rol activo'
                  ELSE 'rol en baja'
                END AS status_role,
                t3.institution_id,
                t3.long_name as institution_name,
                CASE
                  WHEN t3.is_deleted THEN 'institucion activa'
                  ELSE 'institucion en baja'
                END AS status_institution,
                t4.laboratory_id,
                t4.long_name as laboratory_name,
                CASE
                  WHEN t4.is_deleted THEN 'laboratorio activo'
                  ELSE 'laboratorio en baja'
                END AS status_laboratory
          FROM   plims.auth_user_by_role_institution_laboratory t0
                  RIGHT JOIN plims.auth_user t1
                          ON t0.user_id = t1.user_id
                  LEFT JOIN plims.auth_role t2
                        ON t0.role_id = t2.role_id
                  LEFT JOIN plims.bsns_institution t3
                        ON t0.institution_id = t3.institution_id
                  LEFT JOIN plims.bsns_laboratory t4
                        ON t0.laboratory_id = t4.laboratory_id
          WHERE  t0.user_id = :_user_id
          ORDER  BY t1.username;",
        [
          ':_user_id' => $_id
        ]
      )->queryAll();
    }

    $data_provider_role_institution_laboratory = new ArrayDataProvider([
      'allModels' => $data_role_institution_laboratory,
      'sort' => [
        'attributes' => [
          'user_id',
          'username',
          'role_id',
          'role_name',
          'status_role',
          'institution_id',
          'institution_name',
          'status_institution',
          'laboratory_id',
          'laboratory_name',
          'status_laboratory',
        ],
      ],
      'pagination' => [
        'pageSize' => 10,
      ],
    ]);

    return $this->render(
      'manage-user',
      [
        '_task_id' => $_task_id,
        'data_provider_user' => $data_provider_user,
        'data_provider_role_institution_laboratory' => $data_provider_role_institution_laboratory,
        'data_form' => $data_form,

        'data_institution' => $data_institution,
        'data_laboratory' => $data_laboratory,
        'data_role' => $data_role,

        'selected_institution' => $selected_institution,
        'selected_laboratory' => $selected_laboratory,
        'selected_role' => $selected_role,

        '_id' => $_id,

        '_role_is_disabled' => $_role_is_disabled,
        '_institution_is_disabled' => $_institution_is_disabled,
        '_laboratory_is_disabled' => $_laboratory_is_disabled,
      ]
    );
  }

  public function actionAddRole($_task_id, $_id)
  {
    $_user = Yii::$app->user->identity->id;
    $_date = date("Y-m-d H:i:s", time());
    $_user_id = $_id;
    $_role_id = Yii::$app->request->post('role');
    $_institution_id = Yii::$app->request->post('institution');
    $_laboratory_id = Yii::$app->request->post('laboratory');


    $exists_user = Yii::$app->db->createCommand(
      "SELECT CASE
            WHEN Count(1) > 0 THEN true
            ELSE false
          END AS exists_user
      FROM   plims.auth_user_by_role_institution_laboratory
      WHERE  user_id = :_user_id; ",
      [
        ':_user_id' => $_user_id,
      ]
    )->queryScalar();

    $params = [];
    if ($exists_user) {
      $query = "UPDATE plims.auth_user_by_role_institution_laboratory SET ";

      if ($_role_id) {
        $query = $query . "role_id = :_role_id, ";
        $params[':_role_id'] = $_role_id;
      } else {
        $query = $query . "role_id = :_role_id, ";
        $params[':_role_id'] = Functional::NORMAL_USER_ROLE;
      }

      if ($_institution_id) {
        $query = $query . "institution_id = :_institution_id, ";
        $params[':_institution_id'] = $_institution_id;
      } else {
        $query = $query . "institution_id = :_institution_id, ";
        $params[':_institution_id'] = Functional::INSTITUTION_TOKEN;
      }

      if ($_laboratory_id) {
        $query = $query . "laboratory_id = :_laboratory_id, ";
        $params[':_laboratory_id'] = $_laboratory_id;
      } else {
        $query = $query . "laboratory_id = :_laboratory_id, ";
        $params[':_laboratory_id'] = Functional::LABORATORY_TOKEN;
      }

      $query = $query . "updated_by = :_updated_by, ";
      $params[':_updated_by'] = $_user;

      $query = $query . "updated_at = :_updated_at ";
      $params[':_updated_at'] = $_date;

      $query = $query . "WHERE user_id = :_user_id; ";
      $params[':_user_id'] = $_user_id;
    } else {
      $query = "INSERT INTO plims.auth_user_by_role_institution_laboratory
      ( user_id, role_id, institution_id, laboratory_id, registered_by, registered_at )
      VALUES ( :_user_id, ";

      $params[':_user_id'] = $_user_id;

      if ($_role_id) {
        $query = $query . ":_role_id, ";
        $params[':_role_id'] = $_role_id;
      } else {
        $query = $query . ":_role_id, ";
        $params[':_role_id'] = Functional::NORMAL_USER_ROLE;
      }

      if ($_institution_id) {
        $query = $query . ":_institution_id, ";
        $params[':_institution_id'] = $_institution_id;
      } else {
        $query = $query . ":_institution_id, ";
        $params[':_institution_id'] = Functional::INSTITUTION_TOKEN;
      }

      if ($_laboratory_id) {
        $query = $query . ":_laboratory_id, ";
        $params[':_laboratory_id'] = $_laboratory_id;
      } else {
        $query = $query . ":_laboratory_id, ";
        $params[':_laboratory_id'] = Functional::LABORATORY_TOKEN;
      }

      $query = $query . ":_registered_by, ";
      $params[':_registered_by'] = $_user;

      $query = $query . ":_registered_at) ";
      $params[':_registered_at'] = $_date;
    }

    Yii::$app->db->createCommand($query, $params)->execute();

    Yii::$app->db->createCommand(
      "UPDATE plims.auth_user AS tab_1
          SET    role = tab_2.min_role
          FROM   (
                  SELECT tb1.user_id,
                        COALESCE(Min(tb2.role_id), :_user_role) AS min_role
                  FROM   plims.auth_user tb1
                        LEFT JOIN plims.auth_user_by_role_institution_laboratory tb2
                                ON tb1.user_id = tb2.user_id AND tb2.is_deleted = false
                  GROUP  BY tb1.user_id
                  ) AS tab_2
          WHERE  tab_1.user_id = tab_2.user_id
          AND tab_1.user_id = :_user_id;",
      [
        ':_user_role' => Functional::NORMAL_USER_ROLE,
        ':_user_id' => $_user_id,
      ]
    )->execute();

    return $this->redirect(
      [
        'manage-user',
        '_task_id' => $_task_id,
        '_submit' => true,
        '_id' => $_id,
      ]
    );
  }

  public function actionRemoveRole($_task_id, $_user_id, $_role_id, $_institution_id = null, $_laboratory_id = null)
  {
    $_user = Yii::$app->user->identity->id;
    $_date = date("Y-m-d H:i:s", time());

    $query = "DELETE FROM plims.auth_user_by_role_institution_laboratory
    WHERE user_id = :_user_id 
      AND role_id = :_role_id";
    $params = [
      ':_user_id' => $_user_id,
      ':_role_id' => $_role_id
    ];

    if ($_institution_id) {
      $query = $query . " AND institution_id = :_institution_id";
      $params[':_institution_id'] = $_institution_id;
    } else {
      $query = $query . " AND institution_id IS NULL";
    }

    if ($_laboratory_id) {
      $query = $query . " AND laboratory_id = :_laboratory_id";
      $params[':_laboratory_id'] = $_laboratory_id;
    } else {
      $query = $query . " AND laboratory_id IS NULL";
    }
    $query = $query . ";";

    Yii::$app->db->createCommand($query, $params)->execute();

    // Yii::$app->db->createCommand(
    //   "UPDATE plims.auth_user AS tab_1
    //     SET    role = tab_2.role
    //     FROM   (SELECT CASE
    //                     WHEN Min(role_id) IS NULL THEN :_min_role
    //                     ELSE Min(role_id)
    //                   END AS role
    //             FROM   plims.auth_user_by_role_institution_laboratory
    //             WHERE  is_deleted = false
    //                   AND user_id = :_user_id) AS tab_2
    //     WHERE  tab_1.user_id = :_user_id;",
    //   [
    //     ':_min_role' => Functional::NORMAL_USER_ROLE,
    //     ':_user_id' => $_user_id,
    //   ]
    // )->execute();

    Yii::$app->db->createCommand(
      "UPDATE plims.auth_user AS tab_1
          SET    role = tab_2.min_role
          FROM   (
                  SELECT tb1.user_id,
                        COALESCE(Min(tb2.role_id), :_user_role) AS min_role
                  FROM   plims.auth_user tb1
                        LEFT JOIN plims.auth_user_by_role_institution_laboratory tb2
                                ON tb1.user_id = tb2.user_id AND tb2.is_deleted = false
                  GROUP  BY tb1.user_id
                  ) AS tab_2
          WHERE  tab_1.user_id = tab_2.user_id
          AND tab_1.user_id = :_user_id;",
      [
        ':_user_role' => Functional::NORMAL_USER_ROLE,
        ':_user_id' => $_user_id,
      ]
    )->execute();

    Yii::$app->db->createCommand(
      "INSERT INTO  plims.auth_user_by_role_institution_laboratory (user_id, role_id, institution_id, laboratory_id, registered_by, registered_at)
       VALUES(:_user_id, :_min_role, :_institution_id, :_laboratory_id, :_registered_by, :_registered_at);",
      [
        ':_min_role' => Functional::NORMAL_USER_ROLE,
        ':_institution_id' => Functional::INSTITUTION_TOKEN,
        ':_laboratory_id' => Functional::LABORATORY_TOKEN,
        ':_user_id' => $_user_id,
        ':_registered_by' => $_user,
        ':_registered_at' => $_date
      ]
    )->execute();


    return $this->redirect(
      [
        'manage-user',
        '_task_id' => $_task_id,
        '_submit' => true,
        '_id' => $_user_id,
      ]
    );
  }

  public function actionSaveUser($_task_id, $_submit = false, $_id = null)
  {
  }

  private function reviewStatus()
  {
    $user =  Yii::$app->user->identity->id;
    $date =  date("Y-m-d H:i:s", time());
    $_user_role = Yii::$app->user->identity->role;
    $_user_institution = Yii::$app->user->identity->institution;
    $_user_laboratory = Yii::$app->user->identity->laboratory;

    $_result = [
      'CHANGE_PASS' => false,
      'CHANGE_PASS_DETAIL' => null,

      'MANAGE_INSTITUTION' => false,
      'MANAGE_INSTITUTION_DETAIL' => null,

      'MANAGE_USER' => false,
      'MANAGE_USER_DETAIL' => null,

      'MANAGE_CROP' => false,
      'MANAGE_CROP_DETAIL' => null,

      'MANAGE_PARAMETER' => false,
      'MANAGE_PARAMETER_DETAIL' => null,

      'MANAGE_LABORATORY' => false,
      'MANAGE_LABORATORY_DETAIL' => null,

      'MANAGE_AGENT' => false,
      'MANAGE_AGENT_DETAIL' => null,

      'MANAGE_CONDITION' => false,
      'MANAGE_CONDITION_DETAIL' => null,
    ];

    $_user_role = Yii::$app->user->identity->role;
    switch ($_user_role) {
      case Functional::SUPER_ADMIN_ROLE: {
          // 1. Check if changed password.

          // 2. Check if there is at least one active institution.
          $_task_name = 'MANAGE_INSTITUTION';
          $has_institutions = Yii::$app->db->createCommand(
            "SELECT count(institution_id) as institutions
          FROM   plims.bsns_institution
          WHERE  is_deleted = false
                 AND institution_id <> :_institution_token;",
            [':_institution_token' => Functional::INSTITUTION_TOKEN]
          )->queryScalar();

          if ($has_institutions > 0) {
            $_result['MANAGE_INSTITUTION'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_INSTITUTION'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          // 3 Check if there is at least one user associated with the created institution.
          $_task_name = 'MANAGE_USER';
          $_institution_ids = Yii::$app->db->createCommand(
            "SELECT DISTINCT institution_id
          FROM   plims.bsns_institution
          WHERE  is_deleted = false 
          AND institution_id <> :_institution_token;",
            [':_institution_token' => Functional::INSTITUTION_TOKEN]
          )->queryColumn();

          $_institution_created = Yii::$app->db->createCommand(
            "SELECT DISTINCT institution_id
          FROM   plims.auth_user_by_role_institution_laboratory
          WHERE  is_deleted = false 
          AND institution_id <> :_institution_token
          ORDER  BY 1;",
            [':_institution_token' => Functional::INSTITUTION_TOKEN]
          )->queryColumn();

          if (count($_institution_ids) ==  count($_institution_created)) {
            $_result['MANAGE_USER'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_USER'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          // 4. Ensure that at least one set of parameters exists for each institution.

          $_task_name = 'MANAGE_PARAMETER';
          $_parameters_per_institution = Yii::$app->db->createCommand(
            "SELECT institution,
                    Count(parameters) as parameters
            FROM   (SELECT DISTINCT i1.short_name as institution,
                                    p1.singularity
                                    || '-'
                                    || p1.entity AS parameters
                    FROM   plims.bsns_institution i1
                            LEFT JOIN plims.bsns_parameter p1
                                  ON p1.institution_id = i1.institution_id
                                      AND p1.status = :_status_active
                    WHERE  i1.is_deleted = false
                    ORDER  BY 1) AS tab
            GROUP  BY institution
            ORDER  BY 1;",
            [
              ':_status_active' => Functional::STATUS_ACTIVE,
            ]
          )->queryAll();

          $_parameters_per_institution = ArrayHelper::map($_parameters_per_institution, 'institution', 'parameters');

          if (count(array_unique($_parameters_per_institution)) === 1) {
            $_result['MANAGE_PARAMETER'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_PARAMETER'] = false;

            $maxValue = max($_parameters_per_institution);
            $_institution_with_error  = array_keys(array_filter($_parameters_per_institution, function ($value) use ($maxValue) {
              return $value < $maxValue;
            }));
            $_result['MANAGE_PARAMETER_DETAIL']  = implode(', ', $_institution_with_error);
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          // 5. Validate the existence of at least one crop for each institution.
          $_task_name = 'MANAGE_CROP';
          $_has_crops = Yii::$app->db->createCommand(
            "SELECT DISTINCT i1.short_name     AS institution,
                        Count(c1.crop_id) AS crops
            FROM   plims.bsns_institution i1
              LEFT JOIN plims.bsns_crop c1
                      ON c1.institution_id = i1.institution_id
                        AND c1.status = :_status_active
            WHERE  i1.is_deleted = false
              AND i1.institution_id <> :_institution_token
            GROUP  BY institution
            ORDER  BY 1 ",
            [
              ':_institution_token' => Functional::INSTITUTION_TOKEN,
              ':_status_active' => Functional::STATUS_ACTIVE,
            ]
          )->queryAll();
          $_crops_per_institution = ArrayHelper::map($_has_crops, 'institution', 'crops');
          $all_institutions_has_crop = true;


          foreach ($_crops_per_institution as $val) {
            if ($val <= 0) {
              $all_institutions_has_crop = false;
              break;
            }
          }

          if ($all_institutions_has_crop) {

            $_result['MANAGE_CROP'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {

            $_result['MANAGE_CROP'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          // 6. Guarantee that there is at least one laboratory associated with each institution.
          $_task_name = 'MANAGE_LABORATORY';
          $_has_laboratories = Yii::$app->db->createCommand(
            "SELECT DISTINCT i1.short_name           AS institution,
                          Count(l1.laboratory_id) AS laboratories
              FROM   plims.bsns_institution i1
                LEFT JOIN plims.bsns_laboratory l1
                        ON l1.institution_id = i1.institution_id
                          AND l1.is_deleted = false
              WHERE  i1.is_deleted = false
                AND i1.institution_id <> :_institution_token
              GROUP  BY institution
              ORDER  BY 1;",
            [
              ':_institution_token' => Functional::INSTITUTION_TOKEN,
            ]
          )->queryAll();
          $_laboratories_per_institution = ArrayHelper::map($_has_laboratories, 'institution', 'laboratories');
          $all_institutions_has_laboratories = true;


          foreach ($_laboratories_per_institution as $val) {
            if ($val <= 0) {
              $all_institutions_has_laboratories = false;
              break;
            }
          }

          if ($all_institutions_has_laboratories) {

            $_result['MANAGE_LABORATORY'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {

            $_result['MANAGE_LABORATORY'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          break;
        }
      case Functional::INSTITUTION_ADMIN_ROLE: {
          // 1. Make sure at least one set of parameters exists for my institution.
          $_task_name = 'MANAGE_PARAMETER';
          $_parameters_by_institution = Yii::$app->db->createCommand(
            "SELECT DISTINCT singularity
                        || '-'
                        || entity AS parameters
            FROM   plims.bsns_parameter
            WHERE  institution_id = :_institution_id
              AND status = :_status_active;",
            [
              ':_institution_id' => $_user_institution,
              ':_status_active' => Functional::STATUS_ACTIVE,
            ]
          )->queryColumn();

          $_parameters_by_default = Yii::$app->db->createCommand(
            "SELECT DISTINCT singularity
                        || '-'
                        || entity AS parameters
            FROM   plims.bsns_parameter
            WHERE  institution_id = :_institution_token
              AND status = :_status_active; ",
            [
              ':_institution_token' => Functional::INSTITUTION_TOKEN,
              ':_status_active' => Functional::STATUS_ACTIVE,
            ]
          )->queryColumn();

          if (count($_parameters_by_institution) ==  count($_parameters_by_default)) {
            $_result['MANAGE_PARAMETER'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_PARAMETER'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          // 2. Validate the existence of at least one crop associated with my institution.
          $_task_name = 'MANAGE_CROP';
          $_has_crops = Yii::$app->db->createCommand(
            "SELECT count(crop_id) as crops
            FROM   plims.bsns_crop
            WHERE  institution_id = :_institution_id
                   AND status = :_status_active;",
            [
              ':_institution_id' => $_user_institution,
              ':_status_active' => Functional::STATUS_ACTIVE,
            ]
          )->queryScalar();

          if ($_has_crops > 0) {
            $_result['MANAGE_CROP'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_CROP'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          // 3. Guarantee that there is at least one laboratory associated with my institution.
          $_task_name = 'MANAGE_LABORATORY';
          $_has_laboratories = Yii::$app->db->createCommand(
            "SELECT Count(laboratory_id) AS crops
            FROM   plims.bsns_laboratory
            WHERE  institution_id = :_institution_id
                   AND is_deleted = false; ",
            [
              ':_institution_id' => $_user_institution,
            ]
          )->queryScalar();

          if ($_has_laboratories > 0) {
            $_result['MANAGE_LABORATORY'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_LABORATORY'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          // 4. Check if there is at least one user associated with my institution.
          $_task_name = 'MANAGE_USER';
          $_laboratory_ids = Yii::$app->db->createCommand(
            "SELECT DISTINCT laboratory_id
          FROM   plims.bsns_laboratory
          WHERE  is_deleted = false 
          AND institution_id <> :_laboratory_token
          AND institution_id = :_institution_id;",
            [
              ':_laboratory_token' => Functional::LABORATORY_TOKEN,
              ':_institution_id' => $_user_institution,
            ]
          )->queryColumn();

          $_labs_with_admins = Yii::$app->db->createCommand(
            "SELECT DISTINCT laboratory_id
          FROM   plims.auth_user_by_role_institution_laboratory
          WHERE  is_deleted = false 
          AND institution_id = :_institution_id
          AND role_id = :_laboratory_admin_role;",
            [
              ':_institution_id' => $_user_institution,
              ':_laboratory_admin_role' => Functional::LABORATORY_ADMIN_ROLE,
            ]
          )->queryColumn();

          if (count($_laboratory_ids) ==  count($_labs_with_admins)) {
            $_result['MANAGE_USER'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_USER'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          break;
        }
      case Functional::LABORATORY_ADMIN_ROLE: {

          // 1.  Validate the existence of at least one agent created.
          $_task_name = 'MANAGE_AGENT';
          $_has_agents = Yii::$app->db->createCommand(
            "SELECT Count(agent_id) AS agents
            FROM   plims.bsns_agent
            WHERE  status = :_status_active;",
            [
              ':_status_active' => Functional::STATUS_ACTIVE,
            ]
          )->queryScalar();

          if ($_has_agents > 0) {
            $_result['MANAGE_AGENT'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_AGENT'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          // 2. Guarantee that there is at least one condition in the institution.
          $_task_name = 'MANAGE_CONDITION';
          $_has_agents = Yii::$app->db->createCommand(
            "SELECT Count(1) AS conditions
            FROM   plims.bsns_agent_condition
            WHERE  status = :_status_active
                   AND laboratory_id = :_laboratory_id ;",
            [
              ':_status_active' => Functional::STATUS_ACTIVE,
              ':_laboratory_id' => $_user_laboratory,
            ]
          )->queryScalar();

          if ($_has_agents > 0) {
            $_result['MANAGE_CONDITION'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_CONDITION'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          // 3. Check if there is at least one user associated with my laboratory.
          $_task_name = 'MANAGE_USER';

          $_has_users = Yii::$app->db->createCommand(
            "SELECT Count(laboratory_id)
            FROM   plims.auth_user_by_role_institution_laboratory
            WHERE  is_deleted = false
                   AND laboratory_id = :_laboratory_id
                   AND role_id = :_normal_user_role; ",
            [
              ':_laboratory_id' => $_user_laboratory,
              ':_normal_user_role' => Functional::NORMAL_USER_ROLE,
            ]
          )->queryScalar();

          if ($_has_users > 0) {
            $_result['MANAGE_USER'] = true;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, true, false);
          } else {
            $_result['MANAGE_USER'] = false;
            $this->updateValues($_user_institution, $_user_laboratory, $_user_role, $user, $date, $_task_name, false, true);
          }

          break;
        }
      default:
        break;
    }
  }

  private function updateValues($_institution_id, $_laboratory_id, $_role_id, $user, $date, $_task_name, $_is_finished, $_is_blocked)
  {
    Yii::$app->db->createCommand(
      "UPDATE  plims.auth_task
              SET is_finished = :_is_finished,
              updated_by = :_updated_by,
              updated_at = :_updated_at
              WHERE is_deleted = false
                  AND institution_id = :_institution_id
                  AND laboratory_id = :_laboratory_id
                  AND role_id = :_role_id
                  AND short_name = :_task_name;",
      [
        ':_is_finished' => $_is_finished,
        ':_institution_id' => $_institution_id,
        ':_laboratory_id' => $_laboratory_id,
        ':_role_id' => $_role_id,
        ':_updated_by' => $user,
        ':_updated_at' => $date,
        ':_task_name' => $_task_name
      ]
    )->execute();
    // update blocked to next task
    Yii::$app->db->createCommand(
      "UPDATE plims.auth_task
              SET is_blocked = :_is_blocked,
              updated_by = :_updated_by,
              updated_at = :_updated_at
              WHERE is_deleted = false
                    AND institution_id = :_institution_id
                    AND laboratory_id = :_laboratory_id
                    AND role_id = :_role_id
                    AND depends_on =  :_task_name",
      [
        ':_is_blocked' => $_is_blocked,
        ':_updated_by' => $user,
        ':_updated_at' => $date,
        ':_institution_id' => $_institution_id,
        ':_laboratory_id' => $_laboratory_id,
        ':_role_id' => $_role_id,
        ':_task_name' => $_task_name
      ]
    )->execute();
  }
}

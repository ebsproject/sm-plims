<?php

namespace frontend\modules\auth\controllers;

// use sizeg\jwt\Jwt;
// use sizeg\jwt\JwtHttpBearerAuth;
use Yii;
use yii\rest\Controller;
// use yii\rest\ActiveController;
// use common\models\LoginForm;
use common\models\User;
use frontend\models\UserRefreshTokens;
use app\modules\auth\models\APIAuthManager;
// use app\components\B4RController;
// use yii\helpers\Url;

// use bizley\jwt\Jwt;


// class AuthController extends B4RController
class AuthController extends Controller
{
  const STATUS_DELETED = 0;
  const STATUS_INACTIVE = 9;
  const STATUS_ACTIVE = 10;

  const SUPER_ADMIN_USER_ID = 1;
  const NORMAL_USER = 4;

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    // $behaviors['authenticator'] = [
    //   'class' =>  \bizley\jwt\JwtHttpBearerAuth::class,
    //   'except' => [
    //     'login',
    //     'login-mobile',
    //     'verify',
    //     'verify-mobile',
    //     'refresh-token'
    //   ],
    // ];

    $behaviors['JWTBearerAuth'] = [
      'class' => \bizley\jwt\JwtHttpBearerAuth::class,
      'except' => [
        'login',
        'login-offline',
        'login-mobile',
        'verify',
        'verify-mobile',
        'refresh-token',
        'generate',
      ]
    ];


    return $behaviors;
  }


  public function actionLogin()
  {
    // // usr: admin@ebsproject.org
    // // pwd: 12345678

    // // Get variables
    $authorizationUrl = getenv('CB_SG_AUTH_URL');
    $clientId = getenv('CB_SG_CLIENT_ID');
    $clientSecret = getenv('CB_SG_CLIENT_SECRET');
    $redirectUri = getenv('CB_SG_REDIRECT_URI_WEB');

    // // Build URL
    $url = $authorizationUrl
      . '?scope=openid&response_type=code'
      . '&client_id=' . $clientId
      . '&client_secret=' . $clientSecret
      . '&redirect_uri=' . $redirectUri;

    $this->redirect($url, 301);

    // try {
    //   $user =  User::findOne(2);
    //   $duration = 60 * 60 * 24 * 30;
    //   if (Yii::$app->user->login($user, $duration)) {
    //     Yii::$app->session->setFlash('success', \Yii::t('app', 'You have successfully logged in!'));
    //   }
    //   return $this->goHome();
    // } catch (\Throwable $e) {
    //   Yii::$app->session->setFlash('error', \Yii::t('app', 'Unsuccessful login. Kindly contact your system administrator.'));
    //   return $this->goHome();
    // }
  }

  public function actionLoginOffline()
  {
    try {
      $user =  User::findOne(self::SUPER_ADMIN_USER_ID);
      $duration = 60 * 60 * 24 * 30;
      if (Yii::$app->user->login($user, $duration)) {
        Yii::$app->session->setFlash('success', \Yii::t('app', 'You have successfully logged in!'));
      }
      return $this->goHome();
    } catch (\Throwable $e) {
      Yii::$app->session->setFlash('error', \Yii::t('app', 'Unsuccessful logout. Kindly contact your system administrator.'));
      return $this->goHome();
    }
  }


  public function actionVerify()
  {
    try {

      if (isset($_GET['code'])) {
        $code = trim($_GET['code']);
        // Request token
        $apiAuthManager = new  APIAuthManager();
        $generatedToken = $apiAuthManager->requestToken($code);

        if ($generatedToken != NULL) {
          $tokenParts = explode('.', $generatedToken);
          $payload = json_decode(base64_decode($tokenParts[1]), true);
          $user = $this->getValidateUserEmail($payload);
          if (is_null($user)) {
            // Provide an error message to the user
            Yii::$app->session->setFlash('error', \Yii::t('app', 'We were unable to verify your account within the system. Please contact the system administrator.'));
          } else {

            $duration = 60 * 60 * 24 * 30;
            if (Yii::$app->user->login($user, $duration)) {
              Yii::$app->session->setFlash('success', \Yii::t('app', 'You have successfully logged in!'));
            }
          }
          // Go back to the homepage
          return $this->goHome();
        } else {
          // Provide an error message to the user
          Yii::$app->session->setFlash('error', \Yii::t('app', 'We were not able to verify your account. Kindly contact your system administrator.'));
          // Go back to the homepage
          return $this->goHome();
        }
      } else {
        // Provide an error message to the user
        Yii::$app->session->setFlash('error', \Yii::t('app', 'We were not able to verify your account. Kindly contact your system administrator.'));
        // Go back to the homepage
        return $this->goHome();
      }
    } catch (\Throwable $e) {
      Yii::$app->session->setFlash('error', \Yii::t('app', 'Unsuccessful verification. Kindly contact your system administrator.'));
      // Go back to the homepage
      return $this->goHome();
    }
  }

  public function actionLoginMobile()
  {
    // Get variables
    $authorizationUrl = getenv('CB_SG_AUTH_URL');
    $clientId = getenv('CB_SG_CLIENT_ID');
    $clientSecret = getenv('CB_SG_CLIENT_SECRET');
    $redirectUri = getenv('CB_SG_REDIRECT_URI_MOB');

    // Build URL
    $url = $authorizationUrl
      . '?scope=openid&response_type=code'
      . '&client_id=' . $clientId
      . '&prompt=login'
      . '&client_secret=' . $clientSecret
      . '&redirect_uri=' . $redirectUri;
    $this->redirect($url, 301);
  }

  public function actionVerifyMobile()
  {
    try {
      $code = trim($_GET['code']);
      if ($code == '' || $code == NULL) {
        //  Build URL
        $url = 'fieldbook://?status=err&message=We were not able to verify your account. Kindly contact your system administrator.';
        $this->redirect($url, 301);
      }
      // Request token
      $apiAuthManager = new  APIAuthManager();
      $generatedToken = $apiAuthManager->requestTokenMobile($code);

      if ($generatedToken != NULL) {
        $tokenParts = explode('.', $generatedToken);
        $payload = json_decode(base64_decode($tokenParts[1]), true);
        $user = $this->getValidateUserEmail($payload);
        if (is_null($user)) {
          // Provide an error message to the user
          Yii::$app->session->setFlash('error', \Yii::t('app', 'We were unable to verify your account within the system. Please contact the system administrator.'));
          // Go back to the homepage
          return $this->goHome();
        } else {
          $token = $this->generateJwt($user);
          $this->generateRefreshToken($user);
          $username_resp = $user->username;
          $token_resp = $token->toString();
          $expires_at = $token->claims()->get('exp')->getTimeStamp();
          //  Build URL
          $url = 'fieldbook://?' . 'username=' . $username_resp . '&token=' . $token_resp . '&expires_at=' . $expires_at;
          $this->redirect($url, 301);
        }
      } else {
        $url = 'fieldbook://?status=err&message=We were not able to verify your account. Kindly contact your system administrator.';
        $this->redirect($url, 301);
      }
    } catch (\Throwable $e) {
      $url = 'fieldbook://?status=err&message=Your session has expired. Please log in.';
      $this->redirect($url, 301);
    }
  }


  public function actionLogout()
  {
    Yii::$app->user->logout();
    $session = Yii::$app->session;
    $session->destroy(); // Destroy all application session data
    Yii::$app->session->setFlash('success', \Yii::t('app', 'You have successfully logged out!'));
    // Go back to the homepage
    return $this->goHome();
  }

  public function actionData()
  {
    return $this->asJson([
      'success' => true,
    ]);
  }

  public function actionRefreshToken()
  {
    $refreshToken = Yii::$app->request->cookies->getValue('refresh-token', false);
    if (!$refreshToken) {
      return new \yii\web\UnauthorizedHttpException('No refresh token found.');
    }

    $userRefreshToken = UserRefreshTokens::findOne(['urf_token' => $refreshToken]);

    if (Yii::$app->request->getMethod() == 'POST') {
      // Getting new JWT after it has expired
      if (!$userRefreshToken) {
        return new \yii\web\UnauthorizedHttpException('The refresh token no longer exists.');
      }

      $user = User::find()  //adapt this to your needs
        ->where(
          [
            'user_id' => $userRefreshToken->urf_userID,
            'status' => self::STATUS_ACTIVE
          ]
        )
        ->one();
      if (!$user) {
        $userRefreshToken->delete();
        return new \yii\web\UnauthorizedHttpException('The user is inactive.');
      }

      $token = $this->generateJwt($user);

      return [
        'status' => 'ok',
        'token' => (string) $token,
      ];
    } elseif (Yii::$app->request->getMethod() == 'DELETE') {
      // Logging out
      if ($userRefreshToken && !$userRefreshToken->delete()) {
        return new \yii\web\ServerErrorHttpException('Failed to delete the refresh token.');
      }

      return ['status' => 'ok'];
    } else {
      return new \yii\web\UnauthorizedHttpException('The user is inactive.');
    }
  }

  public function actionGenerate($user_id, $pass_code)
  {
    $result = '';
    if ($pass_code == 'f204d55f-3bd3-4972-a3c0-ab4256f0da1d') {
      $user = User::findOne($user_id);
      $expire = '+1 year';
      $token =  $this->generateJwt($user, $expire);
      $result = $token->toString();
    }
    return $result;
  }

  private function generateJwt(User $user, $_expire = null)
  {
    $jwtParams = Yii::$app->params['jwt'];
    $expire = is_null($_expire) ? $jwtParams['expire'] : $_expire;
    $now = new \DateTimeImmutable();
    $token = Yii::$app->jwt->getBuilder()
      ->issuedBy($jwtParams['issuer'])
      ->permittedFor($jwtParams['audience'])
      ->identifiedBy($jwtParams['id'], true)
      ->issuedAt($now)
      ->expiresAt($now->modify($expire))
      ->canOnlyBeUsedAfter($now)
      ->withClaim('uid', $user->user_id) // NO TE OLVIDES DE CAMBIAR POR FAVOR PORQUE SINO EL IVAN SE MOLESTA Y LA KATE NO NOS PAGA
      ->getToken(
        Yii::$app->jwt->getConfiguration()->signer(),
        Yii::$app->jwt->getConfiguration()->signingKey()
      );
    return $token;
  }

  private function generateRefreshToken(User $user, User $impersonator = null): UserRefreshTokens
  {
    $refreshToken = Yii::$app->security->generateRandomString(200);

    // TODO: Don't always regenerate - you could reuse existing one if user already has one with same IP and user agent
    $userRefreshToken = new UserRefreshTokens([
      'urf_userid' => $user->user_id,
      'urf_token' => $refreshToken,
      'urf_ip' => Yii::$app->request->userIP,
      'urf_user_agent' => Yii::$app->request->userAgent,
      'urf_created' => gmdate('Y-m-d H:i:s'),
    ]);
    if (!$userRefreshToken->save()) {
      throw new \yii\web\ServerErrorHttpException('Failed to save the refresh token: ' . $userRefreshToken->getErrorSummary(true));
    }

    // Send the refresh-token to the user in a HttpOnly cookie that Javascript can never read and that's limited by path
    Yii::$app->response->cookies->add(new \yii\web\Cookie([
      'name' => 'refresh-token',
      'value' => $refreshToken,
      'httpOnly' => true,
      'sameSite' => 'none',
      'secure' => true,
      'path' => '/v1/auth/refresh-token',  //endpoint URI for renewing the JWT token using this refresh-token, or deleting refresh-token
    ]));

    return $userRefreshToken;
  }

  // return string $email|null
  private function getValidateUserEmail($payload)
  {
    // Get info from claims
    $emailaddress = isset($payload['http://wso2.org/claims/emailaddress']) ? $payload['http://wso2.org/claims/emailaddress'] : (isset($payload['emailaddress']) ? $payload['emailaddress'] : (isset($payload['email']) ? $payload['email'] : ''));
    $userid = isset($payload['http://wso2.org/claims/userid']) ? $payload['http://wso2.org/claims/userid'] : (isset($payload['userid']) ? $payload['userid'] : '');
    $created = isset($payload['http://wso2.org/claims/created']) ? $payload['http://wso2.org/claims/created'] : (isset($payload['created']) ? $payload['created'] : '');
    $modified = isset($payload['http://wso2.org/claims/modified']) ? $payload['http://wso2.org/claims/modified'] : (isset($payload['modified']) ? $payload['modified'] : '');
    $firstname = isset($payload['http://wso2.org/claims/firstname']) ? $payload['http://wso2.org/claims/firstname'] : (isset($payload['http://wso2.org/claims/givenname']) ? $payload['http://wso2.org/claims/givenname'] : '');
    $lastname = isset($payload['http://wso2.org/claims/lastname']) ? $payload['http://wso2.org/claims/lastname'] : (isset($payload['lastname']) ? $payload['lastname'] : '');
    $role = isset($payload['http://wso2.org/claims/role']) ? $payload['http://wso2.org/claims/role'] : (isset($payload['role']) ? $payload['role'] : '');
    $userProfile = isset($payload['http://wso2.org/claims/photourl']) ? $payload['http://wso2.org/claims/photourl'] : (isset($payload['photourl']) ? $payload['photourl'] : '');

    $email = strtolower($emailaddress);

    // validacion por email
    $user = User::findByEmailAll($email);
    $pos = strpos($email, "@");
    $username =   substr($email, 0,  $pos);


    if (is_null($user)) {
      return null;
    } else {
      if (is_null($user->updated_at)) {
        $user->username = $username;
        $user->firstname = $firstname;
        $user->lastname = $lastname;
        $user->status = self::STATUS_ACTIVE;
        $user->userid = $userid;
        $user->role_1 = isset($role[0]) ? $role[0] : '';
        $user->role_2 = isset($role[1]) ? $role[1] : '';
        $user->role = self::NORMAL_USER;
        $user->photo = $userProfile;
        $user->save();

        $user->id;
      }

      return $user;
    }
  }
}

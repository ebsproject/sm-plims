<?php

namespace frontend\modules\auth\controllers;

use yii\web\Controller;


use app\components\B4RController;
use app\modules\auth\models\UserValidator;
use app\modules\auth\models\APIAuthManager;
use app\models\Person;
use app\models\User;
use Yii;
use yii\helpers\Url;

$this->enableCsrfValidation = false;
/**
 * Default controller for the `auth` module
 */
class DefaultController extends Controller
{
    private $_user = false;

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}

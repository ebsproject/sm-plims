<?php

namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;


use frontend\modules\functional\models\Request;
use frontend\modules\configuration\models\Crop;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\functional\models\RequestDetail;
use frontend\modules\functional\models\RequestProcessEssay;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use frontend\models\UserDashboardConfig;
use frontend\models\Script;
use frontend\modules\functional\Functional;
use yii\data\SqlDataProvider;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        // if (!Yii::$app->user->isGuest) {
        //     return $this->goHome();
        // }

        // $model = new LoginForm();
        // if ($model->load(Yii::$app->request->post()) && $model->login()) {
        //     return $this->goBack();
        // }

        // $model->password = '';

        // return $this->render('login', [
        //     'model' => $model,
        // ]);
        return $this->goHome();
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $session = Yii::$app->session;
        $session->destroy();
        Yii::$app->session->setFlash('info', \Yii::t('app', 'You have successfully logged out!'));
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        }

        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }
        return $this->render('signup', [
            'model' => $model,
        ]);

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            }

            Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if (($user = $model->verifyEmail()) && Yii::$app->user->login($user)) {
            Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
            return $this->goHome();
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    public function getLayout()
    {
        $showSideNav = false;
        $themeColor = 'teal';
        $fontSize = 'normal';
        $language = 'en';
        $darken = '';
        $sideNavSkin = 'light';

        $layout = UserDashboardConfig::getLayoutPreferences();
        if (!empty($layout)) {
            // extract($layout);
        }

        $themeColor = (isset($themeColor) && !empty($themeColor)) ? $themeColor : 'teal';
        $darken = (isset($darken) && !empty($darken)) ? 'darken-' . $darken : '';
        $bodyClass = ((!empty($showSideNav) && $showSideNav == 'true') || !isset($showSideNav)) ? ' fixed-side-nav show-side-nav' : '';

        $showSideNav = (!empty($showSideNav) && isset($showSideNav)) ? $showSideNav : 'true';
        $sideNavClass = ((!empty($showSideNav) && $showSideNav == 'true') || !isset($showSideNav)) ? ' fixed ' : '';
        $sideNavSkin = (!empty($sideNavSkin) && isset($sideNavSkin)) ? $sideNavSkin : 'light';
        Yii::$app->session->set('sideNavSkin', $sideNavSkin);

        $sideNavSkin = (isset($sideNavSkin) && !empty($sideNavSkin) && $sideNavSkin == 'dark') ? ' dark-side-nav ' : '';
        $fontSize = (isset($fontSize) && !empty($fontSize)) ? $fontSize : 'normal';
        $language = (isset($language) && !empty($language)) ? $language : 'en';

        Yii::$app->session->set('showSideNav', $showSideNav);
        Yii::$app->session->set('fontSize', $fontSize);
        Yii::$app->session->set('language', $language);

        return compact('themeColor', 'darken', 'bodyClass', 'sideNavClass', 'sideNavSkin', 'fontSize', 'language');
    }

    public function actionAdmin()
    {
        $model = new LoginForm();

        if (
            isset(Yii::$app->request->post('LoginForm')['username'])
            &&
            Yii::$app->request->post('LoginForm')['username'] == 'admin'
        ) {
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                Yii::$app->session->setFlash('success', \Yii::t('app', 'You have successfully logged in!'));
                return $this->redirect(
                    [
                        'admin/admin-configuration/management',
                    ]
                );
            }
        } else {
            Yii::$app->session->setFlash('info', \Yii::t('app', 'Only admin login!'));
        }

        $model->password = '';

        return $this->render('admin', [
            'model' => $model,
        ]);
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            return $this->render('error', ['exception' => $exception]);
        }
    }
}

<?php
/* 
* This file is part of Breeding4Results.
* Breeding4Results is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Breeding4Results is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Breeding4Results.  If not, see <http://www.gnu.org/licenses/>
*/

/**
 * Contains methods for authentication using 
 * encrypted access token from Yii1 application
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;

class CheckAccessToken extends Controller
{
    /**
     * Authenticate using access token
     * @return $isTokenExpired boolean whether token is expired or not
     */
    public function authenticate()
    {
        try {
            $token = Yii::$app->session->get('user.b4r_api_v3_token');
            $userId = Yii::$app->session->get('user.id');
            $refreshToken = Yii::$app->session->get('user.refresh_token');

            //check if user id set in parameter
            if (!empty($_GET['userId']) && isset($_GET['userId'])) {
                $userId = $_GET['userId'];
                Yii::$app->session->set('user.id', $userId);
            }

            $isTokenExpired = false;

            //if access token and user id is set
            if (!empty($token) && !empty($userId)) {

                $tokenParts = explode('.', $token);
                $payload = json_decode(base64_decode($tokenParts[1]), true);

                $expiration = $payload['exp'];
                $now = microtime(true);

                // check how many minutes the token has before it expires
                $duration = $expiration - $now;
                $hours = (int) ($duration / 60 / 60);
                $minutes = (int) ($duration / 60) - $hours * 60;

                // if a new request is made <= 10 mins before expiration, request new token
                if ($minutes <= 10 && $minutes > 0) {

                    $clientId = getenv('CB_SG_CLIENT_ID');
                    $clientSecret = getenv('CB_SG_CLIENT_SECRET');

                    $params = [
                        'clientId' => $clientId,
                        'clientSecret' => $clientSecret,
                        'grantType' => 'refresh_token',
                        'refreshToken' => $refreshToken
                    ];

                    $response = Yii::$app->api->getParsedResponse('POST', 'auth/login', json_encode($params));
                    if ($response['status'] == 200) {
                        if (isset($response['data'][0])) {
                            $newToken = $response['data'][0]['token'];
                            $newRefreshToken = $response['data'][0]['refreshToken'];

                            $session = Yii::$app->session;

                            // Set session variable for access token
                            $session->set('user.b4r_api_v3_token', $newToken);
                            $session->set('user.refresh_token', $newRefreshToken);
                            $session->set('user.is_logged_in', true);

                            $tokenParts = explode('.', $newToken);
                            $payload = json_decode(base64_decode($tokenParts[1]), true);

                            $expiration = $payload['exp'];
                        }
                    }
                }

                if ($expiration < $now) {
                    $isTokenExpired = true;
                }
            }

            return $isTokenExpired;
        } catch (\Throwable $th) {
            Yii::$app->session->setFlash('error', \Yii::t('app', 'Your session has expired. Please log in.'));

            // Go back to the homepage
            return $this->goHome();
        }
    }
}

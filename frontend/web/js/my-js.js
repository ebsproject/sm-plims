$('.modal-update-action').click(function () {
	try {
		$('#modal-update')
			.modal('show')
			.find('.modal-update-content')
			.load($(this).attr('value'));
	} catch (error) {
		console.log($(this));
	}
	return false;
});

$('.modal-view-action').click(function () {
	try {
		$('#modal-view')
			.modal('show')
			.find('.modal-view-content')
			.load($(this).attr('value'));
	} catch (error) {
		console.log($(this));
	}
	return false;
});

$(function () {
	$('.tree li:has(ul)')
		.addClass('parent_li')
		.find(' > span')
		.attr('title', 'Collapse this branch');
	$('.tree li.parent_li > span').on('click', function (e) {
		var children = $(this).parent('li.parent_li').find(' > ul > li');
		if (children.is(':visible')) {
			children.hide('fast');
			$(this)
				.attr('title', 'Expand this branch')
				.find(' > i')
				.addClass('icon-plus-sign')
				.removeClass('icon-minus-sign');
		} else {
			children.show('fast');
			$(this)
				.attr('title', 'Collapse this branch')
				.find(' > i')
				.addClass('icon-minus-sign')
				.removeClass('icon-plus-sign');
		}
		e.stopPropagation();
	});
});

<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'bower_components/font-awesome/css/font-awesome.min.css',
        'admin-lte@3.2/css/adminlte.min.css',
        'css/bootstrap-3.4.1/bootstrap.min.css',
        'css/site.css',
        'css/my-style.css',
        'css/my-new-style.css',
        'css/toggle-switch-three-state-radio-button.css',
        'css/range-slider.css',
    ];
    public $js = [
        'js/bootstrap-3.4.1/bootstrap.min.js',
        // 'plugins/adminlte-2.4.18/jQueryUI/jquery-ui.min.js',
        // 'plugins/adminlte-2.4.18/jQueryUI/jquery-ui.js',
        // 'js/adminlte-2.4.18/adminlte.min.js',
        'admin-lte@3.2/js/adminlte.min.js',
        'js/my-js.js',
        'js/bootstrap-toggle.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}

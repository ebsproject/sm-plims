<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\modules\functional\Functional;

class Script extends Model
{

  // CHECK STATUS
  public function checkRequest($_request_id)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "UPDATE plims.bsns_request
        SET    request_status_id = :_rs_request_in_process
        WHERE  request_id = :_request_id
               AND status = :_status_active
               AND request_status_id = :_rs_assay_generated; ",
        [
          ':_request_id' => $_request_id,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_rs_request_in_process' => Functional::REQUEST_IN_PROCESS,
          ':_rs_assay_generated' => Functional::ASSAY_GENERATED,
        ]
      )->execute();
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }
  public function checkStatus($_request_id, $user, $date)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "INSERT INTO plims.bsns_status
              (request_id,
              request_status_id,
              number_index,
              status_date,
              status_by,
              observation)
        SELECT :_request_id                         AS request_id,
          :_r_in_progress                       AS request_status_id,
          number_index + 1                     AS number_index,
          :_start_date                          AS status_date,
          :_start_by                            AS status_by,
          'Started the data insertion process' AS observation
        FROM   plims.bsns_status
        WHERE  request_id = :_request_id
        AND request_status_id = :_r_assays_generated
        ON CONFLICT (request_id, request_status_id) DO NOTHING;",
        [
          ':_request_id' => $_request_id,
          ':_r_in_progress' => Functional::REQUEST_IN_PROCESS,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_r_assays_generated' => Functional::ASSAY_GENERATED,
        ]
      )->execute();
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }
  public function checkRequestProcess($_request_id, $_num_order_id, $_essay_id, $user, $date)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "UPDATE plims.bsns_request_process
        SET    request_process_status_id = :_rp_in_progress,
              start_by = :_start_by,
              start_date  = :_start_date
        WHERE  request_id = :_request_id
              AND num_order_id = :_num_order_id
              AND request_process_status_id = :_rp_pending;",
        [
          ':_rp_in_progress' => Functional::REQUEST_PROCESS_IN_PROCESS,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_request_id' => $_request_id,
          ':_num_order_id' => $_num_order_id,
          ':_rp_pending' => Functional::REQUEST_PROCESS_PENDING
        ]
      )->execute();
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }
  public function checkRequestProcessAssayHeader($_request_id, $_num_order_id, $_essay_id, $user, $date)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay
        SET    request_process_essay_status_id = :_rpa_in_progress,
              start_by = :_start_by,
              start_date  = :_start_date,
              status = case when essay_id = :_assay_elisa then :_status_active else status end
        WHERE  request_id = :_request_id
              AND num_order_id = :_num_order_id
              AND essay_id = :_assay_id
              AND request_process_essay_status_id = :_rpa_pending
              AND composite_sample_type_id = :_cst_id;",
        [
          ':_rpa_in_progress' => Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
          ':_request_id' => $_request_id,
          ':_num_order_id' => $_num_order_id,
          ':_assay_id' => $_essay_id,
          ':_rpa_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_assay_elisa' => Functional::ESSAY_ELISA,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_cst_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER
        ]
      )->execute();
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }
  public function checkRequestProcessAssay($_request_process_essay_id, $user, $date)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay
        SET    request_process_essay_status_id = :_rpa_in_progress,
              start_by = :_start_by,
              start_date  = :_start_date,
              status = case when essay_id = :_assay_elisa then :_status_active else status end
        WHERE  request_process_essay_id = :_request_process_essay_id
              AND request_process_essay_status_id = :_rpa_pending;",
        [
          ':_rpa_in_progress' => Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
          ':_rpa_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_assay_elisa' => Functional::ESSAY_ELISA,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_request_process_essay_id' => $_request_process_essay_id,
        ]
      )->execute();
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }
  public function checkRequestProcessAssayAgent($request_process_essay_id, $agent_id, $user, $date)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      $query = "UPDATE plims.bsns_request_process_essay_agent
      SET status = :_status_active
      WHERE request_process_essay_id = :_request_process_essay_id 
      AND agent_id = :_agent_id
      AND status = :_status_disabled;";

      $query_params = [
        ':_status_active' => Functional::STATUS_ACTIVE,
        ':_agent_id' => $agent_id,
        ':_status_disabled' => Functional::STATUS_DISABLED,
        ':_request_process_essay_id' => $request_process_essay_id,
      ];

      $db->createCommand(
        $query,
        $query_params
      )->execute();

      $params = implode("; ", $query_params);
      $db->createCommand(
        "INSERT INTO plims.log_scripts (description, query, params, registered_by, registered_at)
        VALUES (:_description, :_query, :_params, :_registered_by, :_registered_at );
         ",
        [
          ':_description' => 'checkRequestProcessAssayAgent',
          ':_query' =>  $query,
          ':_params' =>  $params,
          ':_registered_by' => $user,
          ':_registered_at' => $date,
        ]
      )->execute();

      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }



  public function checkRequestProcessAssayActivity($_request_process_essay_id, $user, $date)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay_activity
        SET    request_process_essay_activity_status_id = :_rpea_in_progress,
              start_by = :_start_by,
              start_date  = :_start_date,
              status = case when essay_id = :_assay_elisa then :_status_active else status end
        WHERE  request_process_essay_id = :_request_process_essay_id
              AND request_process_essay_activity_status_id = :_rpea_pending
              AND ( activity_id = :_activity_id OR :_activity_id = 0 );",
        [
          ':_rpea_in_progress' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_IN_PROCESS,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_assay_elisa' => Functional::ESSAY_ELISA,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_request_process_essay_id' => $_request_process_essay_id,
          ':_rpea_pending' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_PENDING,
          ':_activity_id' => 0,
        ]
      )->execute();
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }
  public function checkRequestProcessAssayActivitySample($_request_process_essay_id, $_activity_id, $_composite_sample_id, $user, $date)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay_activity_sample 
        SET    request_process_essay_activity_sample_status_id = :_rpeas_in_progress,
              start_by = :_start_by,
              start_date = :_start_date,
              available = TRUE
        WHERE request_process_essay_id = :_request_process_essay_id
              AND ( activity_id = :_activity_id OR :_activity_id = 0 )
              AND ( composite_sample_id = :_composite_sample_id OR :_composite_sample_id = 0 )
              AND request_process_essay_activity_sample_status_id = :_rpeas_pending",
        [
          ':_rpeas_in_progress' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_IN_PROCESS,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_request_process_essay_id' => $_request_process_essay_id,
          ':_activity_id' => $_activity_id,
          ':_composite_sample_id' => $_composite_sample_id,
          ':_rpeas_pending' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_PENDING,
        ]
      )->execute();
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }
  // All check status, used in web assays process
  public function checkStatusInProgressSubProcess($_request_process_essay_id, $user, $date)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay_activity_sample t1
        SET    available = true
        FROM   plims.bsns_request_process_essay_activity t2
        WHERE  t1.request_process_essay_activity_id = t2.request_process_essay_activity_id
              AND t2.request_process_essay_id = :_request_process_essay_id
              AND t2.status = :_status_active;",
        [
          ':_request_process_essay_id'  => $_request_process_essay_id,
          ':_status_active' => Functional::STATUS_ACTIVE,
        ]
      )->execute();

      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay
      SET    request_process_essay_status_id = :_rpe_in_progress,
             start_by = :_start_by,
             start_date  = :_start_date,
             status = :_status_active
      WHERE  request_process_essay_id = :_request_process_essay_id
             AND request_process_essay_status_id = :_rpe_pending",
        [
          ':_rpe_in_progress' => Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_request_process_essay_id' => $_request_process_essay_id,
          ':_rpe_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
        ]
      )->execute();

      $data_array = $db->createCommand(
        "SELECT request_id,
            num_order_id,
            essay_id
      FROM   plims.bsns_request_process_essay
      WHERE  request_process_essay_id = :_request_process_essay_id;",
        [
          ':_request_process_essay_id' => $_request_process_essay_id
        ]
      )->queryOne();

      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay
      SET    request_process_essay_status_id = :_rpe_in_progress,
             start_by = :_start_by,
             start_date  = :_start_date
      WHERE  request_id = :_request_id
             AND num_order_id = :_num_order_id
             AND essay_id = :_essay_id
             AND composite_sample_type_id = :_composite_sample_type_header
             AND request_process_essay_status_id = :_rpe_pending;",
        [
          ':_rpe_in_progress' => Functional::REQUEST_PROCESS_ESSAY_IN_PROCESS,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_request_id' => $data_array['request_id'],
          ':_num_order_id' => $data_array['num_order_id'],
          ':_essay_id' => $data_array['essay_id'],
          ':_composite_sample_type_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
          ':_rpe_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
        ]
      )->execute();

      $db->createCommand(
        "UPDATE plims.bsns_request_process
      SET    request_process_status_id = :_rp_in_progress,
             start_by = :_start_by,
             start_date  = :_start_date
      WHERE  request_id = :_request_id
             AND num_order_id = :_num_order_id
             AND request_process_status_id = :_rp_pending;",
        [
          ':_rp_in_progress' => Functional::REQUEST_PROCESS_IN_PROCESS,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_request_id' => $data_array['request_id'],
          ':_num_order_id' => $data_array['num_order_id'],
          ':_rp_pending' => Functional::REQUEST_PROCESS_PENDING
        ]
      )->execute();

      $db->createCommand(
        "UPDATE plims.bsns_request
      SET    request_status_id = :_r_in_progress
      WHERE  request_id = :_request_id
             AND request_status_id = :_r_assays_generated; ",
        [
          ':_r_in_progress' => Functional::REQUEST_IN_PROCESS,
          ':_request_id' => $data_array['request_id'],
          ':_r_assays_generated' => Functional::ASSAY_GENERATED,
        ]
      )->execute();


      $db->createCommand(
        "INSERT INTO plims.bsns_status
            (request_id,
            request_status_id,
            number_index,
            status_date,
            status_by,
            observation)
      SELECT :_request_id                         AS request_id,
      :_r_in_progress                       AS request_status_id,
      number_index + 1                     AS number_index,
      :_start_date                          AS status_date,
      :_start_by                            AS status_by,
      'Started the data insertion process' AS observation
      FROM   plims.bsns_status
      WHERE  request_id = :_request_id
      AND request_status_id = :_r_assays_generated
      ON CONFLICT (request_id, request_status_id) DO NOTHING;",
        [
          ':_request_id' => $data_array['request_id'],
          ':_r_in_progress' => Functional::REQUEST_IN_PROCESS,
          ':_start_by' => $user,
          ':_start_date' => $date,
          ':_r_assays_generated' => Functional::ASSAY_GENERATED,
        ]
      )->execute();

      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  // ADD RESULTS
  public function mobSampleDistributionDev(
    $to_delete,
    $request_process_essay_id,
    $support_master,
    $composite_sample_id,
    $support_order,
    $agent_id,
    $cell_position,
    $grafting_number,
    $reading_data_type_id,
    $user,
    $date
  ) {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      if ((bool)$to_delete) {

        // obtenemos la lista de muestras
        $composite_sample_id_list = $db->createCommand(
          "SELECT DISTINCT composite_sample_id FROM plims.bsns_reading_data
          WHERE  support_master_id = :_support_master
              AND agent_id = :_agent_id 
              AND support_order_id = :_support_order
              AND support_cell_position = :_cell_position;",
          [
            ':_support_master' => $support_master,
            ':_agent_id' => $agent_id,
            ':_support_order' => $support_order,
            ':_cell_position' => $cell_position,
          ]
        )->queryColumn();

        if (count($composite_sample_id_list) > 0) {
          // formamos un concatenado de los ids
          $composite_sample_id_text = implode(",", $composite_sample_id_list);

          $query_1 = "DELETE FROM plims.bsns_agent_sample_reviewer
          WHERE  support_master_id = :_support_master
              AND agent_id = :_agent_id 
              AND :_reading_data_type_id = :_reading_data_entry
              AND composite_sample_id in ( ";
          $query_2 = " );";

          // armamos el query de ejecucion
          $query = $query_1 . $composite_sample_id_text . $query_2;

          // eliminar del revisor si existe dentro de las muestras 
          $db->createCommand(
            $query,
            [
              ':_support_master' => $support_master,
              ':_agent_id' => $agent_id,
              '_reading_data_type_id' => $reading_data_type_id,
              '_reading_data_entry' => Functional::READING_DATA_ENTRY,
            ]
          )->execute();
        }

        // eliminar distribuciónEstimado  de la lectura si existe
        $db->createCommand(
          "DELETE FROM plims.bsns_reading_data
          WHERE  support_master_id = :_support_master
              AND agent_id = :_agent_id 
              AND support_order_id = :_support_order
              AND support_cell_position = :_cell_position;",
          [
            ':_support_master' => $support_master,
            ':_agent_id' => $agent_id,
            ':_support_order' => $support_order,
            ':_cell_position' => $cell_position,
          ]
        )->execute();
      } else {

        // eliminar distribución de la lectura si existe
        $db->createCommand(
          "DELETE FROM plims.bsns_reading_data
          WHERE  support_master_id = :_support_master
              AND request_process_essay_id = :_request_process_essay_id 
              AND agent_id = :_agent_id 
              AND support_order_id = :_support_order
              AND support_cell_position = :_cell_position;",
          [
            ':_support_master' => $support_master,
            ':_request_process_essay_id' => $request_process_essay_id,
            ':_agent_id' => $agent_id,
            ':_support_order' => $support_order,
            ':_cell_position' => $cell_position,
          ]
        )->execute();

        // eliminar del revisor si existe
        $db->createCommand(
          "DELETE FROM plims.bsns_agent_sample_reviewer
          WHERE  support_master_id = :_support_master
              AND request_process_essay_id = :_request_process_essay_id 
              AND agent_id = :_agent_id 
              AND composite_sample_id = :_cs_id
              AND :_reading_data_type_id = :_reading_data_entry;",
          [
            ':_support_master' => $support_master,
            ':_request_process_essay_id' => $request_process_essay_id,
            ':_agent_id' => $agent_id,
            '_cs_id' => $composite_sample_id,
            '_reading_data_type_id' => $reading_data_type_id,
            '_reading_data_entry' => Functional::READING_DATA_ENTRY,
          ]
        )->execute();

        // se verifica la existencia del support
        $support_exists = $db->createCommand(
          "SELECT CASE
                    WHEN Count(support_id) = 0 THEN false
                    ELSE true
                  END AS support_exists
          FROM   plims.bsns_support
          WHERE  request_process_essay_id = :_request_process_essay_id
                  AND support_order_id = :_support_order;",
          [
            ':_request_process_essay_id' => $request_process_essay_id,
            ':_support_order' => $support_order,
          ]
        )->queryScalar();

        // se obtiene el support_id
        if ($support_exists) {
          $support_id = $db->createCommand(
            "SELECT support_id
            FROM   plims.bsns_support
            WHERE  request_process_essay_id = :_request_process_essay_id
                    AND support_order_id = :_support_order",
            [
              ':_request_process_essay_id' => $request_process_essay_id,
              ':_support_order' => $support_order,
            ]
          )->queryScalar();
        } else {
          // si no existe se crea un support_id
          $support_id = $db->createCommand(
            "INSERT INTO plims.bsns_support
            (request_id,
            essay_id,
            composite_sample_type_id,
            support_order_id,
            registered_by,
            registered_at,
            status,
            num_order_id,
            crop_id,
            workflow_id,
            activity_id,
            support_type_id,
            request_process_essay_id,
            request_process_essay_activity_id)
            SELECT request_id,
                  essay_id,
                  composite_sample_type_id,
                  :_support_order AS support_order_id,
                  :_user          AS registered_by,
                  :_date          AS registered_at,
                  :_status_active AS status,
                  num_order_id,
                  crop_id,
                  workflow_id,
                  activity_id,
                  support_type_id,
                  request_process_essay_id,
                  request_process_essay_activity_id
            FROM plims.bsns_support
            WHERE request_process_essay_id = :_request_process_essay_id
              AND support_order_id = :_support_order_ini
            RETURNING support_id;",
            [
              ':_support_order' => $support_order,
              ':_user' => $user,
              ':_date' => $date,
              ':_status_active' => Functional::STATUS_ACTIVE,
              ':_request_process_essay_id' => $request_process_essay_id,
              ':_support_order_ini' => Functional::READING_DATA_SUPPORT_ORDER_INI,
            ]
          )->queryScalar();
        }

        // obtener lecturas por evento, proceso, agente, muestra y repetición
        $readings = $db->createCommand(
          "SELECT Count(1) 
          FROM   plims.bsns_reading_data
          WHERE  support_master_id = :_support_master
                AND request_process_essay_id = :_request_process_essay_id 
                AND agent_id = :_agent_id
                AND composite_sample_id = :_composite_sample_id
                AND grafting_number_id = :_grafting_number;",
          [
            ':_support_master' => $support_master,
            ':_request_process_essay_id' => $request_process_essay_id,
            ':_agent_id' => $agent_id,
            ':_composite_sample_id' => $composite_sample_id,
            ':_grafting_number' => $grafting_number,
          ]
        )->queryScalar();

        // si existen lecturas entonces actualizar
        if ($readings > 0 && $reading_data_type_id == Functional::READING_DATA_ENTRY) {

          $db->createCommand(
            "UPDATE plims.bsns_reading_data
            SET   support_cell_position = :_cell_position,
                  support_order_id = :_support_order,
                  support_id = :_support_id
            WHERE  support_master_id = :_support_master
                  AND request_process_essay_id = :_request_process_essay_id 
                  AND agent_id = :_agent_id
                  AND composite_sample_id = :_composite_sample_id
                  AND grafting_number_id = :_grafting_number;",
            [
              ':_cell_position' => $cell_position,
              ':_support_order' => $support_order,
              ':_support_id' => $support_id,
              ':_support_master' => $support_master,
              ':_request_process_essay_id' => $request_process_essay_id,
              ':_agent_id' => $agent_id,
              ':_composite_sample_id' => $composite_sample_id,
              ':_grafting_number' => $grafting_number,
            ]
          )->execute();
        } else {
          // si no existen lecturas entonces insertar
          $db->createCommand(
            "INSERT INTO plims.bsns_reading_data
                    (request_id,
                    num_order_id,
                    essay_id,
                    activity_id,
                    agent_id,
                    support_order_id,
                    support_cell_position,
                    grafting_number_id,
                    historical_data_version,
                    composite_sample_group_id,
                    composite_sample_id,
                    composite_sample_type_id,
                    crop_id,
                    workflow_id,
                    reading_data_type_id,
                    primary_order_num,
                    secondary_order_num,
                    tertiary_order_num,
                    registered_by,
                    registered_at,
                    status, 
                    support_master_id,
                    request_process_essay_id,
                    request_process_essay_agent_id,
                    request_process_essay_activity_id,
                    request_process_essay_activity_sample_id,
                    support_id)
            SELECT t1.request_id,
                t1.num_order_id,
                t1.essay_id,
                t1.activity_id,
                t2.agent_id,
                :_support_order::INTEGER        AS support_order_id,
                :_cell_position::INTEGER        AS support_cell_position,
                :_grafting_number::INTEGER      AS grafting_number_id,
                :_order_ini::INTEGER            AS historical_data_version,
                t1.composite_sample_group_id,
                t1.composite_sample_id,
                t1.composite_sample_type_id,
                t1.crop_id,
                t1.workflow_id,
                :_reading_data_type_id::INTEGER AS reading_data_type_id,
                t1.num_order          AS primary_order_num,
                t2.num_order          AS secondary_order_num,
                t0.num_order          AS tertiary_order_num,
                :_user::INTEGER                 AS registered_by,
                :_date::timestamp                AS registered_at,
                :_status_active::text        AS status,
                :_support_master::INTEGER       AS support_master_id,
                t2.request_process_essay_id,
                t2.request_process_essay_agent_id,
                t1.request_process_essay_activity_id,
                t1.request_process_essay_activity_sample_id,
                :_support_id::INTEGER          AS support_id
            FROM   plims.bsns_request_process_essay_activity t0
                INNER JOIN plims.bsns_request_process_essay_activity_sample t1
                        ON t0.request_process_essay_id = :_request_process_essay_id::INTEGER
                            AND t0.request_process_essay_activity_id =
                                t1.request_process_essay_activity_id
                            AND t1.composite_sample_id = :_composite_sample_id::INTEGER
                INNER JOIN plims.bsns_request_process_essay_agent t2
                        ON t0.request_process_essay_id = t2.request_process_essay_id
                            AND t2.status = :_status_active::text
                            AND t2.agent_id = :_agent_id::INTEGER;",
            [
              ':_support_order' => $support_order,
              ':_cell_position' => $cell_position,
              ':_grafting_number' => $grafting_number,
              ':_order_ini' => Functional::READING_DATA_HISTORICAL_DATA_VERSION_INIT - 1,
              ':_reading_data_type_id' => $reading_data_type_id,
              ':_user' => $user,
              ':_date' => $date,
              ':_status_active' => Functional::STATUS_ACTIVE,
              ':_support_master' => $support_master,
              ':_support_id' => $support_id,
              ':_request_process_essay_id' => $request_process_essay_id,
              ':_composite_sample_id' => $composite_sample_id,
              ':_agent_id' => $agent_id,
            ]
          )->execute();

          if ($reading_data_type_id == Functional::READING_DATA_ENTRY) {
            $db->createCommand(
              "INSERT INTO plims.bsns_agent_sample_reviewer
                          (request_process_essay_id,
                          composite_sample_id,
                          agent_id,
                          num_order,
                          support_master_id,
                          registered_by,
                          registered_at)
                          
              VALUES     (:_request_process_essay_id::INTEGER,
                          :_composite_sample_id::INTEGER,
                          :_agent_id::INTEGER,
                          :_num_order::INTEGER,
                          :_support_master_id::INTEGER,
                          :_user::INTEGER,
                          :_date::timestamp);",
              [
                ':_request_process_essay_id' => $request_process_essay_id,
                ':_composite_sample_id' => $composite_sample_id,
                ':_agent_id' => $agent_id,
                ':_num_order' => Functional::NUM_ORDER_INI,
                ':_support_master_id' => $support_master,
                ':_user' => $user,
                ':_date' => $date,
              ]
            )->execute();
          }
        }
      }
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function mobSampleResultsAddResult($number_result, $reprocess, $request_process_essay_id, $activity_id, $composite_sample_id, $agent_id, $text_result, $user, $date)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $data_array = $db->createCommand(
        "INSERT INTO plims.bsns_reading_data
        (
            request_id,
            num_order_id,
            essay_id,
            activity_id,
            agent_id,
            symptom_id,
            support_order_id,
            support_cell_position,
            grafting_number_id,
            historical_data_version,
            composite_sample_group_id,
            composite_sample_id,
            composite_sample_type_id,
            crop_id,
            workflow_id,
            reading_data_section_id,
            reading_data_type_id,
            observation,
            material_evidence,
            number_result,
            text_result,
            primary_order_num,
            secondary_order_num,
            tertiary_order_num,
            registered_by,
            registered_at,
            status,
            number_of_seeds_tested,
            support_master_id,
            request_process_essay_id,
            request_process_essay_agent_id,
            request_process_essay_activity_id,
            request_process_essay_activity_sample_id,
            support_id,
            reprocess
        )                 
        SELECT t0.request_id                               AS request_id,
              t0.num_order_id                             AS num_order_id,
              t0.essay_id                                 AS essay_id,
              t0.activity_id                              AS activity_id,
              t2.agent_id                                 AS agent_id,
              r1.symptom_id                               AS symptom_id,
              r1.support_order_id                         AS support_order_id,
              r1.support_cell_position                    AS support_cell_position,
              r1.grafting_number_id                       AS grafting_number_id,
              COALESCE(r1.historical_data_version + 1, 1) AS historical_data_version,
              t1.composite_sample_group_id                AS composite_sample_group_id,
              t1.composite_sample_id                      AS composite_sample_id,
              t1.composite_sample_type_id                 AS composite_sample_type_id,
              t1.crop_id                                  AS crop_id,
              t1.workflow_id                              AS workflow_id,
              r1.reading_data_section_id                  AS reading_data_section_id,
              :_rdt_entry                                 AS reading_data_type_id,
              r1.observation                              AS observation,
              r1.material_evidence                        AS material_evidence,
              :_number_result                             AS number_result,
              :_text_result                               AS text_result,
              t1.num_order                                AS primary_order_num,
              t2.num_order                                AS secondary_order_num,
              t0.num_order                                AS tertiary_order_num,
              :_user                                      AS registered_by,
              :_date                                      AS registered_at,
              :_status_active                             AS status,
              t1.number_of_seeds                          AS number_of_seeds_tested,
              r1.support_master_id                        AS support_master_id,
              t2.request_process_essay_id                 AS request_process_essay_id,
              t2.request_process_essay_agent_id           AS request_process_essay_agent_id,
              t1.request_process_essay_activity_id        AS request_process_essay_activity_id,
              t1.request_process_essay_activity_sample_id AS request_process_essay_activity_sample_id,
              r1.support_id                               AS support_id,
              :_reprocess                                 AS reprocess
        FROM   plims.bsns_request_process_essay_activity t0 
              INNER JOIN plims.bsns_request_process_essay_activity_sample t1
                      ON t0.request_process_essay_id = :_request_process_essay_id
                          AND t0.request_process_essay_activity_id =
                              t1.request_process_essay_activity_id
                          AND t0.activity_id = :_activity_id
                          AND t1.composite_sample_id = :_composite_sample_id
                          AND t1.available
              INNER JOIN plims.bsns_request_process_essay_agent t2
                      ON t0.request_process_essay_id = t2.request_process_essay_id
                          AND t2.agent_id = :_agent_id
              LEFT JOIN plims.bsns_reading_data r1
                      ON r1.request_process_essay_activity_sample_id =
                                  t1.request_process_essay_activity_sample_id
                        AND r1.request_process_essay_agent_id =
                            t2.request_process_essay_agent_id
                        AND r1.status = :_status_active
        RETURNING reading_data_id, request_process_essay_activity_sample_id;",
        [
          ':_number_result' => $number_result,
          ':_text_result' => $text_result,
          ':_user' => $user,
          ':_date' => $date,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_reprocess' => $reprocess,
          ':_request_process_essay_id' => $request_process_essay_id,
          ':_activity_id' => $activity_id,
          ':_composite_sample_id' => $composite_sample_id,
          ':_agent_id' => $agent_id,
          ':_rdt_entry' => Functional::READING_DATA_ENTRY,
        ]
      )->queryOne();

      $new_reading_data_id = $data_array['reading_data_id'];

      $new_request_process_essay_activity_sample_id =  $data_array['request_process_essay_activity_sample_id'];

      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay_agent
        SET    status = :_status_active
        WHERE  request_process_essay_id = :_request_process_essay_id
              AND agent_id = :_agent_id
              AND status = :_status_disabled;",
        [
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_request_process_essay_id' => $request_process_essay_id,
          ':_agent_id' => $agent_id,
          ':_status_disabled' => Functional::STATUS_DISABLED,
        ]
      )->execute();

      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay_activity_sample
        SET    request_process_essay_activity_sample_status_id = :_request_process_essay_activity_sample_status_finished,
              finish_date = :_date,
              finish_by = :_user
        WHERE  request_process_essay_activity_sample_id = :_request_process_essay_activity_sample_id
              AND request_process_essay_activity_sample_status_id = :_request_process_essay_activity_sample_status_in_process;",
        [
          ':_request_process_essay_activity_sample_status_finished' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_FINISHED,
          ':_date' => $date,
          ':_user' => $user,
          ':_request_process_essay_activity_sample_id' => $new_request_process_essay_activity_sample_id,
          ':_request_process_essay_activity_sample_status_in_process' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_IN_PROCESS,
        ]
      )->execute();

      $db->createCommand(
        "UPDATE plims.bsns_reading_data
        SET    status = :_status_disabled
        WHERE  request_process_essay_id = :_request_process_essay_id
              AND activity_id = :_activity_id
              AND composite_sample_id = :_composite_sample_id
              AND agent_id = :_agent_id
              AND reading_data_id <> :_reading_data_id;",
        [
          ':_status_disabled' => Functional::STATUS_DISABLED,
          ':_request_process_essay_id' => $request_process_essay_id,
          ':_activity_id' => $activity_id,
          ':_composite_sample_id' => $composite_sample_id,
          ':_agent_id' => $agent_id,
          ':_reading_data_id' => $new_reading_data_id,
        ]
      )->execute();


      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
    return $new_reading_data_id;
  }

  public function mobTemplateSettings($user, $date, $request_id, $num_order_id, $assay_id, $num_order)
  {

    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay
        (
            request_id,
            num_order_id,
            crop_id,
            workflow_id,
            composite_sample_type_id,
            sample_detail,
            code,
            essay_id, 
            num_order,
            sample_qty,
            request_process_essay_status_id,
            request_process_essay_type_id,
            registered_by,
            registered_at,
            status
        )
        SELECT t0.request_id,
            t1.num_order_id,
            t1.crop_id,
            t1.workflow_id,
            p1.parameter_id   AS composite_sample_type_id,
            '<i class=''' || p1.class_details || '''></i> ' || p1.short_name  AS sample_detail,
            'SH'
              || Substring(Cast(t0.year_code AS TEXT), 3, 2) 
              || RIGHT('000'
              || Cast(t0.sequential_code AS TEXT), 3) 
              || Cast(t1.num_order_id AS TEXT) 
              || e1.abbreviation 
              || p1.short_name AS code,
            e1.essay_id,
            :_num_order::INTEGER,
            t1.sample_qty,
            p2.parameter_id   AS request_process_essay_status_id,
            a1.request_process_essay_type_id,
            :_user_id          AS registered_by,
            :_current_datetime AS registered_at,
            :_status_disabled as status  
        FROM   plims.bsns_request t0
                INNER JOIN plims.bsns_request_process t1
                        ON t0.request_id = t1.request_id
                            AND t1.num_order_id = :_num_order_id
                LEFT JOIN plims.bsns_agent_condition a1 
                            ON a1.request_type_id = t0.request_type_id
                          AND a1.crop_id = t1.crop_id
                          AND a1.essay_id = :_assay_id
                          AND a1.num_order = :_num_order_ini
                          AND a1.status = :_status_active
                          AND t0.laboratory_id = a1.laboratory_id 
                LEFT JOIN plims.bsns_parameter p1
                        ON p1.entity = 'composite_sample'
                          AND p1.singularity = 'type'
                          AND p1.parameter_id = a1.composite_sample_type_id
                LEFT JOIN plims.bsns_essay e1
                            ON e1.essay_id = :_assay_id
                LEFT JOIN plims.bsns_parameter p2
                            ON p2.entity = 'request_process_essay'
                          AND p2.singularity = 'status'
                          AND p2.code = '3.1'
        WHERE  t0.request_id = :_request_id
        ORDER  BY p1.code;",
        [
          ':_user_id' => $user,
          ':_current_datetime' => $date,
          ':_status_disabled' => Functional::STATUS_DISABLED,
          ':_num_order_id' =>  $num_order_id,
          ':_assay_id' =>  $assay_id,
          ':_num_order_ini' => Functional::NUM_ORDER_INI,
          ':_status_active' =>  Functional::STATUS_ACTIVE,
          ':_request_id' =>  $request_id,
          ':_num_order' =>  $num_order,
        ]
      )->execute();

      $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay_agent
            (
                request_id,
                num_order_id,
                crop_id,
                workflow_id,
                composite_sample_type_id,
                essay_id,
                agent_id,
                repetition,
                request_process_essay_id,
                num_order,
                registered_by,
                registered_at,
                status
            )
        SELECT t2.request_id,
              t2.num_order_id,
              t2.crop_id,
              t2.workflow_id,
              t2.composite_sample_type_id,
              t2.essay_id,
              a1.agent_id,
              a1.repetition,
              t2.request_process_essay_id,
              a1.num_order,
              t2.registered_by,
              t2.registered_at,
              t2.status
        FROM   plims.bsns_request t0
              INNER JOIN plims.bsns_request_process_essay t2
                      ON t0.request_id = t2.request_id
                        AND t2.essay_id = :_assay_id
                        AND t2.num_order_id = :_num_order_id
                        AND t2.composite_sample_type_id <> :_composite_sample_type_header
              LEFT JOIN plims.bsns_agent_condition a1
                      ON t0.request_type_id = a1.request_type_id 
                        AND t2.crop_id = a1.crop_id
                        AND t2.composite_sample_type_id = a1.composite_sample_type_id
                        AND t2.essay_id = a1.essay_id
                        AND t0.laboratory_id = a1.laboratory_id 
                        AND a1.status = :_status_active
        WHERE  t2.request_id = :_request_id
        ORDER  BY a1.composite_sample_type_id, a1.num_order;",
        [
          ':_composite_sample_type_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
          ':_num_order_id' =>  $num_order_id,
          ':_assay_id' =>  $assay_id,
          ':_status_active' =>  Functional::STATUS_ACTIVE,
          ':_request_id' =>  $request_id,
        ]
      )->execute();


      $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay_activity
        (
            request_id,
            num_order_id,
            crop_id,
            workflow_id,
            composite_sample_type_id,
            essay_id,
            activity_id,
            num_order,
            request_process_essay_id,
            request_process_essay_activity_status_id,
            registered_by,
            registered_at,
            status
        )
        SELECT t2.request_id,
               t2.num_order_id,
               t2.crop_id,
               t2.workflow_id,
               t2.composite_sample_type_id,
               t2.essay_id,
               a2.activity_id,
               a2.num_order,
               t2.request_process_essay_id,
               p2.parameter_id    as request_process_essay_activity_status_id,
               t2.registered_by,
               t2.registered_at,
               t2.status
        FROM   plims.bsns_request_process_essay t2
               INNER JOIN plims.bsns_activity_by_essay a2
                      ON t2.essay_id = a2.essay_id
                         AND a2.status = :_status_active
                         AND t2.composite_sample_type_id <> :_composite_sample_type_header
                         AND t2.request_id = :_request_id
                         AND t2.essay_id = :_assay_id
                         AND t2.num_order_id = :_num_order_id
               LEFT JOIN plims.bsns_parameter p2
                      ON p2.entity = 'request_process_essay_activity'
                         AND p2.singularity = 'status'
                         AND p2.code = '4.1';",
        [
          ':_composite_sample_type_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
          ':_num_order_id' =>  $num_order_id,
          ':_assay_id' =>  $assay_id,
          ':_status_active' =>  Functional::STATUS_ACTIVE,
          ':_request_id' =>  $request_id,
        ]
      )->execute();

      if ($assay_id == Functional::ESSAY_ELISA) {
        $db->createCommand(
          "INSERT INTO plims.bsns_composite_sample
            (
            composite_sample_type_id,
            composite_sample_group_id,
            crop_id,
            num_order,
            short_name,
            long_name,
            reading_data_type_id,
            registered_by,
            registered_at,
            status,
            material_group_id
            )
          SELECT c1.composite_sample_type_id,
                c1.composite_sample_group_id,
                c1.crop_id,
                :_order_ini::INTEGER as num_order,
                p4.short_name,
                p4.long_name,
                p4.parameter_id as reading_data_type_id,
                :_user_id          AS registered_by,
                :_current_datetime AS registered_at,
                :_status_active    AS status,
                c1.material_group_id
          FROM   plims.bsns_request_process t1
                INNER JOIN plims.bsns_composite_sample c1
                        ON t1.composite_sample_group_id = c1.composite_sample_group_id
                            AND t1.request_id = :_request_id
                            AND t1.num_order_id = :_num_order_id
                            AND c1.num_order = :_order_ini
                LEFT JOIN plims.bsns_parameter p4
                        ON p4.parameter_id <> c1.reading_data_type_id
                          AND p4.singularity = 'type'
                          AND p4.entity = 'reading_data'
          ON CONFLICT DO NOTHING;",
          [
            ':_order_ini' => Functional::NUM_ORDER_INI,
            ':_user_id' => $user,
            ':_current_datetime' => $date,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_request_id' => $request_id,
            ':_num_order_id' => $num_order_id,
          ]
        )->execute();

        $db->createCommand(
          "INSERT INTO plims.bsns_support
            (
                  request_id,
                  num_order_id,
                  crop_id,
                  workflow_id,
                  composite_sample_type_id,
                  essay_id,
                  activity_id,
                  support_order_id,
                  support_type_id,
                  request_process_essay_id,
                  request_process_essay_activity_id,
                  registered_by,
                  registered_at,
                  status
              )
            SELECT request_id,
                  num_order_id,
                  crop_id,
                  workflow_id,
                  composite_sample_type_id,
                  essay_id,
                  activity_id,
                  :_order_ini      AS support_order_id,
                  :_undetermined AS support_type_id,
                  request_process_essay_id,
                  request_process_essay_activity_id,
                  registered_by,
                  registered_at,
                  status
            FROM   plims.bsns_request_process_essay_activity t3
            WHERE  t3.request_id = :_request_id
                  AND t3.num_order_id = :_num_order_id
                  AND t3.essay_id = :_assay_id;",
          [
            ':_order_ini' => Functional::NUM_ORDER_INI,
            ':_undetermined' => Functional::PARAMETER_UNDETERMINED,
            ':_request_id' => $request_id,
            ':_num_order_id' => $num_order_id,
            ':_assay_id' => $assay_id,
          ]
        )->execute();
      }

      $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay_activity_sample
                (
                  request_id,
                  num_order_id,
                  essay_id,
                  activity_id,
                  composite_sample_id,
                  composite_sample_type_id,
                  composite_sample_group_id,
                  crop_id,
                  workflow_id,
                  num_order,
                  request_process_essay_activity_sample_status_id,
                  registered_by,
                  registered_at,
                  available,
                  historical_data_version,
                  request_process_essay_activity_id,
                  request_process_essay_id
                )
        SELECT t1.request_id,
            t1.num_order_id,
            t3.essay_id,
            t3.activity_id,
            c1.composite_sample_id,
            c1.composite_sample_type_id,
            c1.composite_sample_group_id,
            t1.crop_id,
            t1.workflow_id,
            c1.num_order,
            p2.parameter_id  AS request_process_essay_activity_sample_status_id,
            t3.registered_by AS registered_by,
            t3.registered_at AS registered_at,
            false            AS available,
            0                AS historical_data_version,
            t3.request_process_essay_activity_id AS request_process_essay_activity_id,
            t3.request_process_essay_id
        FROM   plims.bsns_request_process t1
            INNER JOIN plims.bsns_request_process_essay_activity t3
                    ON t1.request_id = t3.request_id
                        AND t1.num_order_id = t3.num_order_id
                        AND t1.request_id = :_request_id
                        AND t3.essay_id = :_assay_id
                        AND t1.num_order_id = :_num_order_id
            LEFT JOIN plims.bsns_composite_sample c1
                    ON t1.composite_sample_group_id = c1.composite_sample_group_id
                      AND t3.composite_sample_type_id = c1.composite_sample_type_id
                      AND c1.reading_data_type_id IN ( CASE
                                                          WHEN t3.essay_id IN ( :_essay_elisa ) THEN c1.reading_data_type_id
                                                          ELSE :_reading_data_type_result
                                                        END )
            LEFT JOIN plims.bsns_parameter p2
                    ON p2.entity = 'request_process_essay_activity_sample'
                      AND p2.singularity = 'status'
                      AND p2.code = '5.1'
        ORDER  BY c1.composite_sample_type_id,
                t3.activity_id,
                c1.num_order;",
        [
          ':_request_id' => $request_id,
          ':_assay_id' => $assay_id,
          ':_num_order_id' => $num_order_id,
          ':_essay_elisa' => Functional::ESSAY_ELISA,
          ':_reading_data_type_result' => Functional::READING_DATA_ENTRY,
        ]
      )->execute();


      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function webTemplateSettings($user, $date, $request_id, $composite_sample_type_id, $assay_id, $num_order_id, $request_process_essay_type_id, $num_order)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      // insert into REQUEST PROCESS ASSAY
      $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay
        (
            request_id,
            num_order_id,
            crop_id,
            workflow_id,
            composite_sample_type_id,
            essay_id,
            sample_detail,
            code,
            num_order,
            sample_qty,
            request_process_essay_status_id,
            request_process_essay_type_id,
            registered_by,
            registered_at,
            status
        )
        SELECT DISTINCT t2.request_id,
               t2.num_order_id,
               t2.crop_id,
               t2.workflow_id,
               a1.composite_sample_type_id AS composite_sample_type_id,
               t2.essay_id,
               '<i class=' || p2.class_details || '></i> ' || p2.short_name  AS sample_detail,
               t2.code || 
               p2.short_name ||
               CASE
                   WHEN p3.parameter_id = 1 THEN ''
                   ELSE p3.code
               END AS code,
               :_num_order::INTEGER                  AS num_order,
               t2.sample_qty,
               :_rpas_pending::INTEGER as request_process_essay_status_id,
               a1.request_process_essay_type_id,
               :_user_id::INTEGER as registered_by,
               :_current_datetime::timestamp as registered_at,
               t2.status
        FROM   plims.bsns_request t0
               INNER JOIN plims.bsns_request_process_essay t2
                       ON t2.request_id = t0.request_id AND t0.request_id = :_request_id
                          AND t2.essay_id = :_assay_id
                          AND t2.num_order_id = :_num_order_id      
                          AND t2.composite_sample_type_id = :_cst_header_id
               LEFT JOIN plims.bsns_parameter p2
                      ON p2.parameter_id = :_composite_sample_type_id
               LEFT JOIN plims.bsns_parameter p3
                                on p3.parameter_id = :_request_process_essay_type_id
               LEFT JOIN plims.bsns_agent_condition a1
                      ON a1.request_type_id = t0.request_type_id
                     AND a1.crop_id = t2.crop_id
                     AND a1.essay_id = t2.essay_id
                     AND a1.composite_sample_type_id = :_composite_sample_type_id
                     AND a1.request_process_essay_type_id = :_request_process_essay_type_id
                     AND t0.laboratory_id = a1.laboratory_id 
                     AND a1.num_order = :_order_ini;",
        [
          ':_composite_sample_type_id' => $composite_sample_type_id,
          ':_num_order' => $num_order,
          ':_user_id' => $user,
          ':_current_datetime' => $date,
          ':_request_id' => $request_id,
          ':_assay_id' => $assay_id,
          ':_num_order_id' => $num_order_id,
          ':_cst_header_id' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
          ':_request_process_essay_type_id' => $request_process_essay_type_id,
          ':_order_ini' => Functional::NUM_ORDER_INI,
          ':_rpas_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
        ]
      )->execute();


      $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay_agent (
                request_id,
                num_order_id,
                crop_id,
                workflow_id,
                composite_sample_type_id,
                essay_id,
                agent_id,
                repetition,
                request_process_essay_id,
                num_order,
                registered_by,
                registered_at,
                status
            ) SELECT t2.request_id,
              t2.num_order_id,
              t2.crop_id,
              t2.workflow_id,
              t2.composite_sample_type_id,
              t2.essay_id,
              a1.agent_id,
              a1.repetition,
              t2.request_process_essay_id,
              a1.num_order as num_order,
              t2.registered_by,
              t2.registered_at,
              t2.status
        FROM   plims.bsns_request t0
              INNER JOIN plims.bsns_request_process_essay t2
                      ON t0.request_id = t2.request_id
                        AND t2.essay_id = :_assay_id
                        AND t2.num_order_id = :_num_order_id
                        AND t2.composite_sample_type_id = :_composite_sample_type_id
                        AND t2.request_process_essay_type_id = :_request_process_essay_type_id
              LEFT JOIN plims.bsns_agent_condition a1
                      ON a1.request_type_id = t0.request_type_id
                    AND a1.crop_id = t2.crop_id
                    AND a1.composite_sample_type_id = t2.composite_sample_type_id
                    AND a1.request_process_essay_type_id = t2.request_process_essay_type_id 
                    AND a1.essay_id = t2.essay_id
                    AND a1.status = :_status_active
                    AND t0.laboratory_id = a1.laboratory_id 
        WHERE  t2.request_id = :_request_id
        ORDER  BY a1.composite_sample_type_id,
                  a1.num_order;",
        [
          ':_assay_id' => $assay_id,
          ':_num_order_id' => $num_order_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
          ':_request_process_essay_type_id' => $request_process_essay_type_id,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_request_id' => $request_id,
        ]
      )->execute();



      $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay_activity
        (
            request_id,
            num_order_id,
            crop_id,
            workflow_id,
            composite_sample_type_id,
            essay_id,
            activity_id,
            num_order,
            request_process_essay_id,
            request_process_essay_activity_status_id,
            registered_by,
            registered_at,
            status
        )
        SELECT t2.request_id,
               t2.num_order_id,
               t2.crop_id,
               t2.workflow_id,
               t2.composite_sample_type_id,
               t2.essay_id,
               a2.activity_id,
               a2.num_order,
               t2.request_process_essay_id,
               p2.parameter_id as request_process_essay_activity_status_id,
               t2.registered_by,
               t2.registered_at,
               t2.status
        FROM   plims.bsns_request_process_essay t2
               INNER JOIN plims.bsns_activity_by_essay a2
                          ON t2.essay_id = a2.essay_id
                         AND t2.composite_sample_type_id = :_composite_sample_type_id
                         AND t2.request_id = :_request_id
                         AND t2.essay_id = :_assay_id
                         AND t2.num_order_id = :_num_order_id
                         and t2.request_process_essay_type_id = :_request_process_essay_type_id
                         AND a2.status = :_status_active
               LEFT JOIN plims.bsns_parameter p2
                      ON p2.entity = 'request_process_essay_activity'
                         AND p2.singularity = 'status'
                         AND p2.code = '4.1';",
        [
          ':_request_id' => $request_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
          ':_assay_id' => $assay_id,
          ':_num_order_id' => $num_order_id,
          ':_request_process_essay_type_id' => $request_process_essay_type_id,
          ':_status_active' => Functional::STATUS_ACTIVE,
        ]
      )->execute();


      $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay_activity_sample
            (
                request_id,
                num_order_id,
                essay_id,
                activity_id,
                composite_sample_id,
                composite_sample_type_id,
                composite_sample_group_id,
                crop_id,
                workflow_id,
                num_order,
                request_process_essay_activity_sample_status_id,
                registered_by,
                registered_at,
                available,
                historical_data_version,
                request_process_essay_activity_id,
                request_process_essay_id
            )
        SELECT t1.request_id,
              t1.num_order_id,
              t3.essay_id,
              t3.activity_id,
              c1.composite_sample_id,
              c1.composite_sample_type_id,
              c1.composite_sample_group_id,
              t1.crop_id,
              t1.workflow_id,
              c1.num_order,
              p2.parameter_id  AS request_process_essay_activity_sample_status_id,
              t3.registered_by AS registered_by,
              t3.registered_at AS registered_at,
              false            AS available,
              :_historical_data_version0                AS historical_data_version,
              t3.request_process_essay_activity_id,
              t2.request_process_essay_id
        FROM   plims.bsns_request_process t1
          INNER JOIN plims.bsns_request_process_essay t2
                          ON t2.request_id = t1.request_id
                          AND t2.num_order_id = t1.num_order_id
                          AND t2.essay_id = :_assay_id
                          AND t2.composite_sample_type_id = :_composite_sample_type_id
                          AND t2.request_process_essay_type_id = :_request_process_essay_type_id
                          and t2.request_id = :_request_id 
          INNER JOIN plims.bsns_request_process_essay_activity t3
                  ON t2.request_process_essay_id = t3.request_process_essay_id 
          LEFT JOIN plims.bsns_composite_sample c1
                      ON t1.composite_sample_group_id = c1.composite_sample_group_id
                      AND t3.composite_sample_type_id = c1.composite_sample_type_id
                      AND c1.reading_data_type_id = :_reading_data_type_id
                      
          LEFT JOIN plims.bsns_parameter p2
                      ON p2.entity = 'request_process_essay_activity_sample'
                      AND p2.singularity = 'status'
                      AND p2.code = '5.1'
        ORDER  BY c1.composite_sample_type_id,
                  t3.activity_id,
                  c1.num_order;",
        [
          ':_historical_data_version0' => Functional::ACTIVITY_SAMPLE_HISTORICAL_DATA_VERSION_INIT,
          ':_assay_id' => $assay_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
          ':_request_process_essay_type_id' => $request_process_essay_type_id,
          ':_request_id' => $request_id,
          ':_reading_data_type_id' => Functional::READING_DATA_ENTRY,
        ]
      )->execute();


      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function webSampleResultsAddResult($user, $date, $reading_data_id, $text_result, $number_result, $request_process_essay_agent_id, $request_process_essay_activity_sample_id)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      if ($reading_data_id > 0) {
        $db->createCommand(
          "UPDATE plims.bsns_reading_data
          SET    status = :_status_disabled
          WHERE  reading_data_id = :_reading_data_id 
          AND text_result <>  :_text_result;",
          [
            ':_status_disabled' => Functional::STATUS_DISABLED,
            ':_reading_data_id' => $reading_data_id,
            ':_text_result' => $text_result,
          ]
        )->execute();

        $db->createCommand(
          "INSERT INTO plims.bsns_reading_data
              (request_id,
              num_order_id,
              essay_id,
              activity_id,
              agent_id,
              symptom_id,
              support_order_id,
              support_cell_position,
              grafting_number_id,
              historical_data_version,
              composite_sample_group_id,
              composite_sample_id,
              composite_sample_type_id,
              crop_id,
              workflow_id,
              reading_data_section_id,
              reading_data_type_id,
              observation,
              material_evidence,
              auxiliary_result,
              number_result,
              text_result,
              primary_order_num,
              secondary_order_num,
              tertiary_order_num,
              registered_by,
              registered_at, 
              status,
              number_of_seeds_tested,
              support_master_id,
              request_process_essay_id,
              request_process_essay_agent_id,
              request_process_essay_activity_id,
              request_process_essay_activity_sample_id,
              support_id)
          SELECT request_id,
              num_order_id,
              essay_id,
              activity_id,
              agent_id,
              symptom_id,
              support_order_id,
              support_cell_position,
              grafting_number_id,
              historical_data_version + 1 AS historical_data_version,
              composite_sample_group_id,
              composite_sample_id,
              composite_sample_type_id,
              crop_id,
              workflow_id,
              reading_data_section_id,
              reading_data_type_id,
              observation,
              material_evidence,
              auxiliary_result,
              :_number_result                 AS number_result,
              :_text_result                   AS text_result,
              primary_order_num,
              secondary_order_num,
              tertiary_order_num,
              :_user_id                    AS registered_by,
              :_current_datetime           AS registered_at, 
              :_status_active              AS status,
              number_of_seeds_tested,
              support_master_id,
              request_process_essay_id,
              request_process_essay_agent_id,
              request_process_essay_activity_id,
              request_process_essay_activity_sample_id,
              support_id
          FROM   plims.bsns_reading_data
          WHERE  reading_data_id = :_reading_data_id
          AND    text_result <> :_text_result_aux;",
          [
            ':_number_result' => $number_result,
            ':_text_result' => $text_result,
            ':_user_id' => $user,
            ':_current_datetime' => $date,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_reading_data_id' => $reading_data_id,
            ':_text_result_aux' => $text_result,
          ]
        )->execute();
      } else {
        $db->createCommand(
          "INSERT INTO plims.bsns_reading_data
              (request_id,
              num_order_id,
              essay_id,
              activity_id,
              agent_id,
              historical_data_version,
              composite_sample_group_id,
              composite_sample_id,
              composite_sample_type_id,
              crop_id,
              workflow_id,
              reading_data_type_id,
              number_result,
              text_result,
              primary_order_num,
              secondary_order_num,
              tertiary_order_num,
              registered_by,
              registered_at,
              status,
              number_of_seeds_tested,
              support_master_id,
              request_process_essay_id,
              request_process_essay_agent_id,
              request_process_essay_activity_id,
              request_process_essay_activity_sample_id,
              support_id)
          SELECT t1.request_id,
              t1.num_order_id,
              t1.essay_id,
              t4.activity_id,
              t2.agent_id,
              :_historical_data_version_init             AS historical_data_version,
              t4.composite_sample_group_id,
              t4.composite_sample_id,
              t4.composite_sample_type_id,
              t1.crop_id,
              t1.workflow_id,
              :_reading_data_type_id                      AS reading_data_type_id,
              :_number_result                             AS number_result,
              :_text_result                               AS text_result,
              t4.num_order                                AS primary_order_num,
              t2.num_order                                AS secondary_order_num,
              t3.num_order                                AS tertiary_order_num,
              :_user_id                                   AS registered_by,
              :_current_datetime                          AS registered_at,
              :_status_active                             AS status,
              t4.number_of_seeds                          AS number_of_seeds_tested,
              NULL                                        AS support_master_id,
              t1.request_process_essay_id,
              t2.request_process_essay_agent_id,
              t3.request_process_essay_activity_id,
              t4.request_process_essay_activity_sample_id,
              NULL                                        AS support_id
          FROM   plims.bsns_request_process_essay t1
              INNER JOIN plims.bsns_request_process_essay_agent t2
                      ON t1.request_process_essay_id = t2.request_process_essay_id
              INNER JOIN plims.bsns_request_process_essay_activity t3
                      ON t1.request_process_essay_id = t3.request_process_essay_id
              INNER JOIN plims.bsns_request_process_essay_activity_sample t4
                      ON t1.request_process_essay_id = t4.request_process_essay_id
                          AND t3.request_process_essay_activity_id =
                              t4.request_process_essay_activity_id
                  AND t4.available                                
          WHERE  t4.request_process_essay_activity_sample_id = :_request_process_essay_activity_sample_id
              AND t2.request_process_essay_agent_id = :_request_process_essay_agent_id;",
          [
            ':_historical_data_version_init' => Functional::READING_DATA_HISTORICAL_DATA_VERSION_INIT,
            ':_reading_data_type_id' => Functional::READING_DATA_ENTRY,
            ':_number_result' => $number_result,
            ':_text_result' => $text_result,
            ':_user_id' => $user,
            ':_current_datetime' => $date,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_request_process_essay_activity_sample_id' => $request_process_essay_activity_sample_id,
            ':_request_process_essay_agent_id' => $request_process_essay_agent_id,
          ]
        )->execute();
      }

      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function webSampleResultsAddResultBulk($user, $date, $slider_init, $slider_end, $request_process_essay_id, $is_overwrite, $text_result, $number_result)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "INSERT INTO plims.bsns_reading_data
        (
            request_id,
            num_order_id,
            essay_id,
            activity_id,
            agent_id,
            historical_data_version,
            composite_sample_group_id,
            composite_sample_id,
            composite_sample_type_id,
            crop_id,
            workflow_id,
            reading_data_type_id,
            number_result,
            text_result, 
            primary_order_num,
            secondary_order_num,
            tertiary_order_num,
            registered_by,
            registered_at,
            status,
            number_of_seeds_tested,
            request_process_essay_id,
            request_process_essay_agent_id,
            request_process_essay_activity_id,
            request_process_essay_activity_sample_id
        )
        SELECT 
            tab.request_id,
            tab.num_order_id,
            tab.essay_id,
            tab.activity_id,
            tab.agent_id,
            tab.historical_data_version,
            tab.composite_sample_group_id,
            tab.composite_sample_id,
            tab.composite_sample_type_id,
            tab.crop_id,
            tab.workflow_id,
            tab.reading_data_type_id,
            tab.number_result,
            tab.text_result, 
            tab.primary_order_num,
            tab.secondary_order_num,
            tab.tertiary_order_num,
            tab.registered_by,
            tab.registered_at,
            tab.status,
            tab.number_of_seeds_tested,
            tab.request_process_essay_id,
            tab.request_process_essay_agent_id,
            tab.request_process_essay_activity_id,
            tab.request_process_essay_activity_sample_id
        FROM   (
          SELECT tab1.request_id,
              tab1.num_order_id,
              tab1.essay_id,
              tab1.activity_id,
              tab1.agent_id,
              CASE 
                WHEN tab2.historical_data_version IS NULL THEN tab1.historical_data_version::INTEGER + 1
                ELSE tab2.historical_data_version::INTEGER + 1
              END AS historical_data_version,
              tab1.composite_sample_group_id,
              tab1.composite_sample_id,
              tab1.composite_sample_type_id,
              tab1.crop_id,
              tab1.workflow_id,
              tab1.reading_data_type_id,
              tab1.number_result,
              CASE
                WHEN tab2.text_result = :_text_empty
                      OR tab2.text_result IS NULL
                      OR :_is_overwrite THEN :_text_result
                ELSE :_text_discard
              END                                       AS text_result,
              tab1.primary_order_num,
              tab1.secondary_order_num,
              tab1.tertiary_order_num,
              tab1.registered_by,
              tab1.registered_at,
              tab1.status,
              tab1.number_of_seeds_tested,
              tab1.request_process_essay_id,
              tab1.request_process_essay_agent_id,
              tab1.request_process_essay_activity_id,
              tab1.request_process_essay_activity_sample_id
          FROM  (
              SELECT t1.request_id,
                  t1.num_order_id,
                  t1.essay_id,
                  t4.activity_id,
                  t2.agent_id,
                  :_init_version         AS historical_data_version,
                  t4.composite_sample_group_id,
                  t4.composite_sample_id,
                  t4.composite_sample_type_id,
                  t1.crop_id,
                  t1.workflow_id,
                  :_reading_data_type_id::INTEGER AS reading_data_type_id,
                  :_number_result::INTEGER        AS number_result,
                  :_text_result          AS text_result,
                  t4.num_order          AS primary_order_num,
                  t2.num_order          AS secondary_order_num,
                  t3.num_order          AS tertiary_order_num,
                  :_user_id::INTEGER              AS registered_by,
                  :_current_datetime::timestamp     AS registered_at,
                  :_status_active        AS status,
                  t4.number_of_seeds    AS number_of_seeds_tested,
                  t1.request_process_essay_id,
                  t2.request_process_essay_agent_id,
                  t3.request_process_essay_activity_id,
                  t4.request_process_essay_activity_sample_id
              FROM   plims.bsns_request_process_essay t1
                  INNER JOIN plims.bsns_request_process_essay_agent t2
                          ON t1.request_process_essay_id = t2.request_process_essay_id
                  INNER JOIN plims.bsns_request_process_essay_activity t3
                          ON t1.request_process_essay_id = t3.request_process_essay_id
                  INNER JOIN plims.bsns_request_process_essay_activity_sample t4
                          ON t1.request_process_essay_id = t4.request_process_essay_id
                            AND t3.request_process_essay_activity_id =
                                t4.request_process_essay_activity_id
                            AND t4.available
              WHERE  t1.request_process_essay_id = :_request_process_essay_id
                  AND t4.num_order >= :_slider_init
                  AND t4.num_order <= :_slider_end
              ) tab1
              LEFT JOIN 
              (
              SELECT r2.request_id,
                  r2.num_order_id,
                  r2.essay_id,
                  r2.activity_id,
                  r2.agent_id,
                  r2.historical_data_version,
                  r2.composite_sample_group_id,
                  r2.composite_sample_id,
                  r2.composite_sample_type_id,
                  r2.crop_id,
                  r2.workflow_id,
                  r2.reading_data_type_id,
                  r2.number_result,
                  r2.text_result,
                  r2.primary_order_num,
                  r2.secondary_order_num,
                  r2.tertiary_order_num,
                  r2.registered_by,
                  r2.registered_at,
                  r2.status,
                  r2.number_of_seeds_tested,
                  r2.request_process_essay_id,
                  r2.request_process_essay_agent_id,
                  r2.request_process_essay_activity_id,
                  r2.request_process_essay_activity_sample_id
              FROM   plims.bsns_reading_data r2
              WHERE  r2.status = :_status_active
                  AND r2.request_process_essay_id = :_request_process_essay_id
                  AND r2.primary_order_num >= :_slider_init
                  AND r2.primary_order_num <= :_slider_end
              ) tab2
              ON tab1.request_process_essay_activity_sample_id = tab2.request_process_essay_activity_sample_id
                  AND tab1.request_process_essay_agent_id = tab2.request_process_essay_agent_id
          ) tab
        WHERE  tab.text_result <> :_text_discard;",
        [
          ':_text_empty' => Functional::TEXT_EMPTY,
          ':_is_overwrite' => $is_overwrite,
          ':_text_result' => $text_result,
          ':_text_discard' => Functional::TEXT_DISCARD,
          ':_init_version' => Functional::READING_DATA_HISTORICAL_DATA_VERSION_INIT,
          ':_reading_data_type_id' => Functional::READING_DATA_ENTRY,
          ':_number_result' => $number_result,
          ':_user_id' => $user,
          ':_current_datetime' => $date,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_request_process_essay_id' => $request_process_essay_id,
          ':_slider_init' => $slider_init,
          ':_slider_end' => $slider_end,
        ]
      )->execute();


      // deshabilitar intervalo de resultados
      $db->createCommand(
        "UPDATE plims.bsns_reading_data
        SET    status = :_status_disabled
        WHERE  request_process_essay_id = :_request_process_essay_id
               AND primary_order_num >= :_slider_init
               AND primary_order_num <= :_slider_end;",
        [
          ':_status_disabled' => Functional::STATUS_DISABLED,
          ':_request_process_essay_id' => $request_process_essay_id,
          ':_slider_init' => $slider_init,
          ':_slider_end' => $slider_end,
        ]
      )->execute();


      // habilitar intervalo de resultados insertados
      $db->createCommand(
        "UPDATE plims.bsns_reading_data r1
        SET    status = :_status_active
        FROM   (SELECT Max(r2.historical_data_version) AS historical_data_version,
                       r2.primary_order_num,
                       r2.secondary_order_num,
                       r2.request_process_essay_id
                FROM   plims.bsns_reading_data r2
                WHERE  r2.request_process_essay_id = :_request_process_essay_id
                       AND r2.primary_order_num >= :_slider_init
                       AND r2.primary_order_num <= :_slider_end
                GROUP  BY r2.primary_order_num,
                          r2.secondary_order_num,
                          r2.request_process_essay_id) tab
        WHERE  r1.request_process_essay_id = tab.request_process_essay_id
               AND r1.historical_data_version = tab.historical_data_version
               AND r1.primary_order_num = tab.primary_order_num
               AND r1.secondary_order_num = tab.secondary_order_num;",
        [
          ':_request_process_essay_id' => $request_process_essay_id,
          ':_slider_init' => $slider_init,
          ':_slider_end' => $slider_end,
          ':_status_active' => Functional::STATUS_ACTIVE,
        ]
      )->execute();


      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  // FINISH RESULT
  public function webFinishRpaas($user, $date, $request_process_essay_id)
  {

    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay_activity_sample
        SET    request_process_essay_activity_sample_status_id = :_rpeas_finished,
              finish_by = :_user,
              finish_date = :_date
        WHERE  request_process_essay_id = :_rpa_id
        and request_process_essay_activity_sample_status_id <> :_rpeas_finished
              AND available;",
        [
          ':_user' => $user,
          ':_date' => $date,
          ':_rpa_id' => $request_process_essay_id,
          ':_rpeas_finished' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_FINISHED,
        ]
      )->execute();

      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay_activity
        SET    request_process_essay_activity_status_id = :_rpea_finished,
              finish_by = :_user,
              finish_date = :_date 
        WHERE  request_process_essay_id = :_rpa_id
        and request_process_essay_activity_status_id <> :_rpea_finished
              AND status = :_status_active;",
        [
          ':_user' => $user,
          ':_date' => $date,
          ':_rpa_id' => $request_process_essay_id,
          ':_rpea_finished' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_FINISHED,
          ':_status_active' => Functional::STATUS_ACTIVE,
        ]
      )->execute();

      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay 
        SET    request_process_essay_status_id = :_rpe_finished,
               finish_by = :_user,
               finish_date = :_date 
        WHERE  request_process_essay_id = :_rpa_id
        and request_process_essay_status_id <> :_rpe_finished
               AND status = :_status_active;",
        [
          ':_user' => $user,
          ':_date' => $date,
          ':_rpa_id' => $request_process_essay_id,
          ':_rpe_finished' => Functional::REQUEST_PROCESS_ESSAY_FINISHED,
          ':_status_active' => Functional::STATUS_ACTIVE,
        ]
      )->execute();

      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function standardizeResults($request_process_essay_id)
  {
    // estandariza los resultados en la columna resultado auxiliar p/n para todos los ensayos
    $db =  Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      $db->createCommand(
        "UPDATE plims.bsns_reading_data
        SET auxiliary_result = 
          CASE
            WHEN auxiliary_result IS NULL
                AND text_result IS NULL
                AND number_result > 0 THEN :_positive_result
            WHEN auxiliary_result IS NULL
                AND text_result IS NULL
                AND ( number_result <= 0 OR number_result IS NULL ) THEN :_negative_result
            WHEN auxiliary_result IS NULL THEN text_result
            ELSE auxiliary_result
          END
        WHERE status = :_status_active
              AND reading_data_type_id = :_rdt_entry
              AND request_process_essay_id = :_rpa_id;",
        [
          ':_positive_result' => Functional::POSITIVE_RESULT,
          ':_negative_result' => Functional::NEGATIVE_RESULT,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_rdt_entry' => Functional::READING_DATA_ENTRY,
          ':_rpa_id' => $request_process_essay_id,
        ]
      )->execute();
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function consolidateResults($user, $date, $request_process_essay_id, $essay_id, $rpa_activity_id_get, $activity_id_get)
  {
    // consolida los resultados en una sola actividad [get] para quienes manejan mas de una actividad [set]
    $db =  Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      $tertiary_order_num = Yii::$app->db->createCommand(
        "SELECT t1.num_order as _tertiary_order_num
        FROM   plims.bsns_activity_by_essay t1
               INNER JOIN plims.bsns_activity t2
                       ON t1.activity_id = t2.activity_id
        WHERE  t1.essay_id = :_assay_id
               AND t1.status = :_status_active
               AND t2.activity_property_id = :_ap_get;",
        [
          ':_assay_id' => $essay_id,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_ap_get' => Functional::ACTIVITY_PROPERTY_GET,
        ]
      )->queryScalar();

      $db->createCommand(
        "INSERT INTO plims.bsns_reading_data
            (
                request_id,
                num_order_id,
                essay_id,
                activity_id,
                agent_id,
                historical_data_version,
                composite_sample_group_id,
                composite_sample_id,
                composite_sample_type_id,
                crop_id,
                workflow_id,
                reading_data_type_id,
                observation,
                auxiliary_result,
                number_result,
                text_result,
                primary_order_num,
                secondary_order_num,
                tertiary_order_num,
                registered_by,
                registered_at,
                status,
                request_process_essay_id,
                request_process_essay_agent_id,
                request_process_essay_activity_id,
                request_process_essay_activity_sample_id,
                is_deleted,
                reprocess
            )    
          SELECT t1.request_id,
                t1.num_order_id,
                t1.essay_id,
                :_activity_id_get::INTEGER                                              AS activity_id,
                t1.agent_id,
                Max(t1.historical_data_version)                                         AS historical_data_version,
                t1.composite_sample_group_id,
                t1.composite_sample_id,
                t1.composite_sample_type_id,
                t1.crop_id,
                t1.workflow_id,
                t1.reading_data_type_id,
                t1.observation,
                CASE
                  WHEN STRING_AGG(DISTINCT Cast(t1.auxiliary_result AS TEXT), '|') LIKE '%'
                        || :_positive_result::text
                        || '%' THEN :_positive_result::text
                  ELSE STRING_AGG(DISTINCT Cast(t1.auxiliary_result AS TEXT), '|')
                END                                                                     AS auxiliary_result,
                Sum(t1.number_result)                                                   AS number_result,
                STRING_AGG(DISTINCT COALESCE(t1.text_result, t1.auxiliary_result), '|') AS text_result,
                tab.num_order                                                           AS primary_order_num,
                t1.secondary_order_num                                                  AS secondary_order_num,
                :_tertiary_order_num::INTEGER                                           AS tertiary_order_num,
                :_user::INTEGER                                                         AS registered_by,
                :_date::timestamp                                                       AS registered_at,
                :_status_active::text                                                   AS status,
                t1.request_process_essay_id,
                t1.request_process_essay_agent_id,
                :_rpa_activity_id_get::INTEGER                                          AS request_process_essay_activity_id,
                tab.request_process_essay_activity_sample_id                            AS request_process_essay_activity_sample_id,
                false                                                                   AS is_deleted,
                t1.reprocess
          FROM   plims.bsns_reading_data t1
                INNER JOIN (SELECT t2.request_process_essay_activity_sample_id,
                                    t2.composite_sample_id,
                                    t2.num_order
                            FROM   plims.bsns_request_process_essay_activity_sample t2
                            WHERE  t2.request_process_essay_id = :_rpa_id::INTEGER
                                    AND t2.activity_id = :_activity_id_get::INTEGER) AS tab
                        ON t1.composite_sample_id = tab.composite_sample_id
          WHERE  t1.request_process_essay_id = :_rpa_id::INTEGER
                AND t1.status = :_status_active::text
                AND t1.is_deleted = false
                AND t1.activity_id IN (SELECT DISTINCT t3.activity_id
                                        FROM   plims.bsns_activity t3
                                        WHERE  t3.activity_property_id = :_ap_set::INTEGER)
          GROUP  BY t1.request_id,
                    t1.num_order_id,
                    t1.essay_id,
                    t1.agent_id,
                    t1.composite_sample_group_id,
                    t1.composite_sample_id,
                    t1.composite_sample_type_id,
                    t1.crop_id,
                    t1.workflow_id,
                    t1.reading_data_type_id,
                    t1.observation,
                    tab.num_order,
                    t1.secondary_order_num,
                    t1.request_process_essay_id,
                    t1.request_process_essay_agent_id,
                    tab.request_process_essay_activity_sample_id,
                    t1.reprocess;",
        [
          ':_activity_id_get' => $activity_id_get,
          ':_tertiary_order_num' => $tertiary_order_num,
          ':_user' => $user,
          ':_date' => $date,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_rpa_activity_id_get' => $rpa_activity_id_get,
          ':_rpa_id' => $request_process_essay_id,
          ':_ap_set' => Functional::ACTIVITY_PROPERTY_SET,
          ':_positive_result' => Functional::POSITIVE_RESULT,
        ]
      )->execute();
      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  // REPROCESS
  public function addRequestProcess(
    $user,
    $date,
    $request_id,
    $num_order_id,
    $num_order_id_new,
  ) {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "INSERT INTO plims.bsns_request_process
        (
            request_id,
            num_order_id,
            crop_id,
            workflow_id,
            composite_sample_group_id,
            request_process_status_id,
            request_process_material_id,
            request_process_payment_id,
            registered_by,
            registered_at,
            status,
            child_crop,
            child_crop_other,
            request_process_other,
            registration_out_of_the_system,
            material_unit_qty,
            is_deleted
        )
      SELECT request_id,
            :_num_order_id_new AS num_order_id,
            crop_id,
            workflow_id,
            composite_sample_group_id,
            :_rp_pending AS request_process_status_id,
            request_process_material_id,
            request_process_payment_id,
            :_user          AS registered_by,
            :_date          AS registered_at,
            :_status_active AS status,
            child_crop,
            child_crop_other,
            request_process_other,
            registration_out_of_the_system,
            material_unit_qty,
            false AS is_deleted
      FROM   plims.bsns_request_process
      WHERE  request_id = :_request_id
      AND    num_order_id = :_num_order_id
      ON CONFLICT (request_id, num_order_id) DO NOTHING;",
        [
          ':_rp_pending' => Functional::REQUEST_PROCESS_PENDING,
          ':_user' => $user,
          ':_date' => $date,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_num_order_id_new' => $num_order_id_new,
        ]
      )->execute();

      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function addRequestProcessAssay(
    $user,
    $date,
    $request_id,
    $assay_id,
    $num_order_id_new,
    $request_process_essay_id,
    $composite_sample_type_id_new
  ) {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      // verfica si existe una cabecera
      $has_not_header = $db->createCommand(
        "SELECT CASE
        WHEN Count(1) > 0 THEN false
        ELSE true
      END has_not_header
      FROM   plims.bsns_request_process_essay
      WHERE  request_id = :_request_id
      AND essay_id = :_assay_id
      AND num_order_id = :_num_order_id_new
      AND composite_sample_type_id = :_cs_header;",
        [
          ':_request_id' => $request_id,
          ':_assay_id' => $assay_id,
          ':_num_order_id_new' => $num_order_id_new,
          ':_cs_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
        ]
      )->queryScalar();
      // insersion de cabecera en caso no exista
      if ($has_not_header) {
        $db->createCommand(
          "INSERT INTO plims.bsns_request_process_essay
                  (request_id,
                  num_order_id,
                  essay_id,
                  composite_sample_type_id,
                  crop_id,
                  workflow_id,
                  agent_qty,
                  agent_detail,
                  sub_total_cost_essay,
                  registered_by,
                  registered_at,
                  deleted_by,
                  deleted_at,
                  status,
                  num_order,
                  sample_qty,
                  sample_detail,
                  code,
                  request_process_essay_status_id,
                  request_process_essay_type_id,
                  is_deleted)
        SELECT t1.request_id,
            :_num_order_id_new::INTEGER,
            t1.essay_id,
            :_cs_header,
            t1.crop_id,
            workflow_id,
            t1.agent_qty,
            t1.agent_detail,
            t1.sub_total_cost_essay,
            :_user                   AS registered_by,
            :_date                   AS registered_at,
            t1.deleted_by,
            t1.deleted_at,
            t1.status,
            t1.num_order::INTEGER,
            t1.sample_qty,
            NULL                     AS sample_detail,
            'SH' || SUBSTRING(CAST(t2.year_code AS TEXT), 3, 2) 
            || RIGHT('000' || CAST(t2.sequential_code AS TEXT), 3)
            || CAST(:_num_order_id_new::INTEGER AS TEXT)
            || e1.abbreviation       AS code,
            :_rpe_pending::INTEGER   AS request_process_essay_status_id,
            t1.request_process_essay_type_id,
            FALSE                    AS is_deleted
        FROM   plims.bsns_request_process_essay t1
            inner join plims.bsns_request t2 ON t1.request_id = t2.request_id
            inner join plims.bsns_essay e1 on t1.essay_id = e1.essay_id 
        WHERE  t1.request_process_essay_id = :_rpa_id
            AND t1.status = :_status_active
            AND t1.is_deleted = FALSE;",
          [
            ':_cs_header' => Functional::COMPOSITE_SAMPLE_TYPE_HEADER,
            ':_user' => $user,
            ':_date' => $date,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_rpe_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
            ':_rpa_id' => $request_process_essay_id,
            ':_num_order_id_new' => $num_order_id_new,
          ]
        )->execute();
      }

      // insersion de request_process_assay
      $request_process_essay_id_new = $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay
              (request_id,
              num_order_id,
              essay_id,
              composite_sample_type_id,
              crop_id,
              workflow_id,
              agent_qty,
              agent_detail,
              sub_total_cost_essay,
              registered_by,
              registered_at,
              deleted_by,
              deleted_at,
              status,
              num_order,
              sample_qty,
              sample_detail,
              code,
              request_process_essay_status_id,
              request_process_essay_type_id,
              activity_qty,
              request_process_essay_root_id,
              is_deleted)
              SELECT     t1.request_id,
                        :_num_order_id_new::INTEGER,
                        t1.essay_id,
                        :_cs_type,
                        t1.crop_id,
                        t1.workflow_id,
                        t1.agent_qty,
                        t1.agent_detail,
                        t1.sub_total_cost_essay,
                        :_user,
                        :_date,
                        t1.deleted_by,
                        t1.deleted_at,
                        t1.status,
                        t1.num_order::INTEGER,
                        t1.sample_qty,
                        '<i class='''|| p2.class_details || '''></i> ' || p2.short_name AS sample_detail,
                        'SH'|| substring(cast(t2.year_code AS text), 3, 2)
                            || RIGHT('000'
                            || cast(t2.sequential_code AS text), 3)
                            || cast(:_num_order_id_new::INTEGER AS text)
                            || e1.abbreviation
                            || p2.short_name AS code,
                        :_rpe_pending::INTEGER      AS request_process_essay_status_id,
                        t1.request_process_essay_type_id,
                        1        AS activity_qty,
                        :_rpa_id AS request_process_essay_root_id,
                        false    AS is_deleted
              FROM       plims.bsns_request_process_essay t1
              INNER JOIN plims.bsns_parameter p2
                ON         p2.parameter_id = :_cs_type
              INNER JOIN plims.bsns_request t2
                ON         t1.request_id = t2.request_id
              INNER JOIN plims.bsns_essay e1
                ON         t1.essay_id = e1.essay_id
              WHERE      t1.request_process_essay_id = :_rpa_id
                AND        t1.status = :_status_active
                AND        t1.is_deleted = false
              RETURNING request_process_essay_id;",
        [
          ':_cs_type' => $composite_sample_type_id_new,
          ':_user' => $user,
          ':_date' => $date,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_rpe_pending' => Functional::REQUEST_PROCESS_ESSAY_PENDING,
          ':_rpa_id' => $request_process_essay_id,
          ':_num_order_id_new' => $num_order_id_new,
        ]
      )->queryScalar();

      $transaction->commit();

      return $request_process_essay_id_new;
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function addRequestProcessAssayAgentActive(
    $user,
    $date,
    $num_order_id_new,
    $request_process_essay_id,
    $request_process_essay_id_new,
    $composite_sample_type_id_new,
    $activity_id_get
  ) {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay_agent
              (request_id,
              num_order_id,
              essay_id,
              agent_id,
              composite_sample_type_id,
              crop_id,
              workflow_id,
              num_order,
              init_position,
              end_position,
              init_content,
              end_content,
              registered_by,
              registered_at,
              status,
              repetition,
              request_process_essay_id,
              is_deleted)
          SELECT DISTINCT tab1.request_id,
              :_num_order_id_new::INTEGER,
              tab1.essay_id,
              tab1.agent_id,
              :_cs_type::INTEGER,
              tab1.crop_id,
              tab1.workflow_id      AS workflow_id,
              tab1.num_order,
              tab1.init_position,
              tab1.end_position,
              tab1.init_content,
              tab1.end_content,
              :_user::INTEGER       AS registered_by,
              :_date::TIMESTAMP     AS registered_at,
              :_status_active::TEXT       AS status,
              tab1.repetition,
              :_rpa_id_new::INTEGER AS request_process_essay_id,
              false                 AS is_deleted
          FROM   plims.bsns_request_process_essay_agent tab1
          INNER JOIN plims.bsns_agent tab3
                  ON tab1.agent_id = tab3.agent_id
          LEFT JOIN plims.bsns_reading_data tab2
                  ON tab1.request_process_essay_agent_id = tab2.request_process_essay_agent_id
                    AND tab2.status = :_status_active
                    AND ( (tab2.activity_id  = :_activity_id_get::INTEGER) OR (:_activity_id_get IS NULL) )
                    AND tab2.reading_data_type_id = :_rdt_entry::INTEGER
                    AND tab2.text_result = 'positive'
          WHERE  tab1.request_process_essay_id = :_rpa_id::INTEGER
          AND (tab2.auxiliary_result = :_positive_result::TEXT OR tab2.reprocess OR tab3.agent_group_id IN ( 147, 148 ))
          ORDER BY tab1.num_order;",
        [
          ':_cs_type' => $composite_sample_type_id_new,
          ':_user' => $user,
          ':_date' => $date,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_rpa_id_new' => $request_process_essay_id_new,
          ':_rpa_id' => $request_process_essay_id,
          ':_positive_result' => Functional::POSITIVE_RESULT,
          ':_num_order_id_new' => $num_order_id_new,
          ':_activity_id_get' => $activity_id_get,
          ':_rdt_entry' => Functional::READING_DATA_ENTRY,
        ]
      )->execute();

      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function addRequestProcessAssayActivity(
    $user,
    $date,
    $num_order_id_new,
    $request_process_essay_id,
    $request_process_essay_id_new,
    $composite_sample_type_id_new,
    $activity_id_set_first,
    $activity_id_get
  ) {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $rpa_activity_id_get_new = null;
      // insert request process essay activity GET
      if (!is_null($activity_id_get)) {
        $rpa_activity_id_get_new = $db->createCommand(
          "INSERT INTO plims.bsns_request_process_essay_activity
                (request_id,
                num_order_id,
                essay_id,
                activity_id,
                composite_sample_type_id,
                crop_id,
                workflow_id,
                request_process_essay_activity_status_id,
                num_order,
                registered_by,
                registered_at,
                status,
                request_process_essay_id)
            SELECT t1.request_id,
                :_num_order_id_new::INTEGER AS num_order_id,
                t1.essay_id,
                t1.activity_id,
                :_cs_type AS composite_sample_type_id,
                t1.crop_id,
                t1.workflow_id,
                :_rpea_pending  AS request_process_essay_activity_status_id,
                t1.num_order,
                :_user          AS registered_by,
                :_date          AS registered_at,
                :_status_active AS status,
                :_rpa_id_new    AS request_process_essay_id
            FROM   plims.bsns_request_process_essay_activity t1
            WHERE  request_process_essay_id = :_rpa_id
                AND activity_id = :_activity_id_get
            RETURNING request_process_essay_activity_id;",
          [
            ':_cs_type' => $composite_sample_type_id_new,
            ':_rpea_pending' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_PENDING,
            ':_user' => $user,
            ':_date' => $date,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_rpa_id_new' => $request_process_essay_id_new,
            ':_rpa_id' => $request_process_essay_id,
            ':_activity_id_get' => $activity_id_get,
            ':_num_order_id_new' => $num_order_id_new,
          ]
        )->queryScalar();
      }

      // insert request process essay activity SET
      $rpa_activity_id_set_new = $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay_activity
                (request_id,
                num_order_id,
                essay_id,
                activity_id,
                composite_sample_type_id,
                crop_id,
                workflow_id,
                request_process_essay_activity_status_id,
                num_order,
                registered_by,
                registered_at,
                status,
                request_process_essay_id)
          SELECT t1.request_id,
                :_num_order_id_new::INTEGER,
                t1.essay_id,
                t1.activity_id,
                :_cs_type,
                t1.crop_id,
                t1.workflow_id,
                :_rpea_pending  AS request_process_essay_activity_status_id,
                t1.num_order,
                :_user          AS registered_by,
                :_date          AS registered_at,
                :_status_active AS status,
                :_rpa_id_new    AS request_process_essay_id
          FROM   plims.bsns_request_process_essay_activity t1
          WHERE  request_process_essay_id = :_rpa_id
            AND activity_id = :_activity_id_set_first
          RETURNING request_process_essay_activity_id;",
        [
          ':_cs_type' => $composite_sample_type_id_new,
          ':_rpea_pending' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_PENDING,
          ':_user' => $user,
          ':_date' => $date,
          ':_status_active' => Functional::STATUS_ACTIVE,
          ':_rpa_id_new' => $request_process_essay_id_new,
          ':_rpa_id' => $request_process_essay_id,
          ':_activity_id_set_first' => $activity_id_set_first,
          ':_num_order_id_new' => $num_order_id_new,
        ]
      )->queryScalar();

      $transaction->commit();

      $data = [
        'rpa_activity_id_get_new' => $rpa_activity_id_get_new,
        'rpa_activity_id_set_new' => $rpa_activity_id_set_new,
      ];

      return $data;
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function addRequestProcessAssayActivitySample(
    $user,
    $date,
    $num_order_id_new,
    $request_process_essay_id,
    $request_process_essay_id_new,
    $composite_sample_type_id_new,
    $activity_id_set_first,
    $activity_id_get,
    $rpa_activity_id_set_new,
    $rpa_activity_id_get_new,
    $rdt_result
  ) {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();

    try {
      $data = '';
      if (!is_null($activity_id_get)) {
        if (is_null($rdt_result)) {
          // $data = $data . '| GET READING DATA TYPE NULO ';
          $db->createCommand(
            "INSERT INTO plims.bsns_request_process_essay_activity_sample
                  (request_id,
                  num_order_id,
                  essay_id,
                  composite_sample_id,
                  composite_sample_type_id,
                  crop_id,
                  workflow_id,
                  num_order,
                  request_process_essay_activity_sample_status_id,
                  available,
                  request_process_essay_activity_id,
                  registered_by,
                  registered_at,
                  activity_id,
                  composite_sample_group_id,
                  historical_data_version,
                  request_process_essay_id,
                  is_deleted)
              SELECT request_id,
                    num_order_id,
                    essay_id,
                    composite_sample_id,
                    composite_sample_type_id,
                    crop_id,
                    workflow_id,
                    Row_number ()
                      over (
                        ORDER BY tab1.num_order) AS num_order,
                    request_process_essay_activity_sample_status_id,
                    available,
                    request_process_essay_activity_id,
                    registered_by,
                    registered_at,
                    activity_id,
                    composite_sample_group_id,
                    historical_data_version,
                    request_process_essay_id,
                    is_deleted
              FROM   (SELECT DISTINCT tab2.request_id                     AS request_id,
                                      :_num_order_id_new::INTEGER         AS num_order_id,
                                      tab2.essay_id                       AS essay_id,
                                      tab3.composite_sample_id            AS composite_sample_id,
                                      :_cs_type::INTEGER                AS composite_sample_type_id,
                                      tab2.crop_id                        AS crop_id,
                                      tab2.workflow_id                    AS workflow_id,
                                      tab3.num_order                      AS num_order,
                                      :_rpeas_pending::INTEGER          AS request_process_essay_activity_sample_status_id,
                                      TRUE                                AS available,
                                      :_rpea_id_new::INTEGER            AS request_process_essay_activity_id,
                                      :_user::INTEGER                   AS registered_by,
                                      :_date::TIMESTAMP                 AS registered_at,
                                      tab2.activity_id                    AS activity_id,
                                      tab2.composite_sample_group_id      AS composite_sample_group_id,
                                      :_historical_version_ini::INTEGER AS historical_data_version,
                                      :_rpa_id_new::INTEGER             AS request_process_essay_id,
                                      FALSE                               AS is_deleted
                      FROM   plims.bsns_reading_data tab2
                            inner join plims.bsns_composite_sample tab3
                                    ON tab2.composite_sample_group_id = tab3.composite_sample_group_id
                                        AND tab3.composite_sample_type_id = :_cs_type::INTEGER
                                        AND tab2.request_process_essay_id = :_rpa_id::INTEGER
                                        AND tab2.activity_id = :_activity_id_get ::INTEGER
                                        AND tab2.status = :_status_active::TEXT
                                        AND tab2.auxiliary_result = :_positive_result::TEXT
                            INNER JOIN plims.bsns_material_group tab4
                                  ON tab4.composite_sample_group_id =
                                    tab2.composite_sample_group_id
                                    AND tab3.material_group_id = tab4.material_group_id
                                    AND ( ( tab2.primary_order_num = tab4.number_index
                                            AND tab2.reading_data_type_id = :_rdt_entry )
                                            OR ( tab3.reading_data_type_id <> :_rdt_entry ) ) ) tab1;",
            [
              ':_cs_type' => $composite_sample_type_id_new,
              ':_rpeas_pending' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_PENDING,
              ':_rpea_id_new' => $rpa_activity_id_get_new,
              ':_user' => $user,
              ':_date' => $date,
              ':_historical_version_ini' => Functional::ACTIVITY_SAMPLE_HISTORICAL_DATA_VERSION_INIT,
              ':_rpa_id_new' => $request_process_essay_id_new,
              ':_rpa_id' => $request_process_essay_id,
              ':_activity_id_get' => $activity_id_get,
              ':_status_active' => Functional::STATUS_ACTIVE,
              ':_positive_result' => Functional::POSITIVE_RESULT,
              ':_num_order_id_new' => $num_order_id_new,
              ':_rdt_entry' => Functional::READING_DATA_ENTRY,
            ]
          )->execute();
        } else {
          // $data = $data . '| GET READING DATA TYPE NO NULO ';
          $db->createCommand(
            "INSERT INTO plims.bsns_request_process_essay_activity_sample
                  (request_id,
                  num_order_id,
                  essay_id,
                  composite_sample_id,
                  composite_sample_type_id,
                  crop_id,
                  workflow_id,
                  num_order,
                  request_process_essay_activity_sample_status_id,
                  available,
                  request_process_essay_activity_id,
                  registered_by,
                  registered_at,
                  activity_id,
                  composite_sample_group_id,
                  historical_data_version,
                  request_process_essay_id,
                  is_deleted)
              SELECT request_id,
                    num_order_id,
                    essay_id,
                    composite_sample_id,
                    composite_sample_type_id,
                    crop_id,
                    workflow_id,
                    Row_number ()
                      over (
                        ORDER BY tab1.num_order) AS num_order,
                    request_process_essay_activity_sample_status_id,
                    available,
                    request_process_essay_activity_id,
                    registered_by,
                    registered_at,
                    activity_id,
                    composite_sample_group_id,
                    historical_data_version,
                    request_process_essay_id,
                    is_deleted
              FROM   (SELECT DISTINCT tab2.request_id                     AS request_id,
                                      :_num_order_id_new::INTEGER         AS num_order_id,
                                      tab2.essay_id                       AS essay_id,
                                      tab3.composite_sample_id            AS composite_sample_id,
                                      :_cs_type::INTEGER                AS composite_sample_type_id,
                                      tab2.crop_id                        AS crop_id,
                                      tab2.workflow_id                    AS workflow_id,
                                      tab3.num_order                      AS num_order,
                                      :_rpeas_pending::INTEGER          AS request_process_essay_activity_sample_status_id,
                                      TRUE                                AS available,
                                      :_rpea_id_new::INTEGER            AS request_process_essay_activity_id,
                                      :_user::INTEGER                   AS registered_by,
                                      :_date::TIMESTAMP                 AS registered_at,
                                      tab2.activity_id                    AS activity_id,
                                      tab2.composite_sample_group_id      AS composite_sample_group_id,
                                      :_historical_version_ini::INTEGER AS historical_data_version,
                                      :_rpa_id_new::INTEGER             AS request_process_essay_id,
                                      FALSE                               AS is_deleted
                      FROM   plims.bsns_reading_data tab2
                            INNER JOIN plims.bsns_composite_sample tab3
                                    ON tab2.composite_sample_group_id = tab3.composite_sample_group_id
                                        AND tab3.composite_sample_type_id = :_cs_type::INTEGER
                                        AND tab2.request_process_essay_id = :_rpa_id::INTEGER
                                        AND tab2.activity_id = :_activity_id_get ::INTEGER
                                        AND tab2.status = :_status_active::TEXT
                                        AND tab2.auxiliary_result = :_positive_result::TEXT
                                        AND tab3.reading_data_type_id = :_rdt_result::INTEGER 
                            INNER JOIN plims.bsns_material_group tab4
                                    ON tab4.composite_sample_group_id =
                                        tab2.composite_sample_group_id
                                        AND tab4.material_group_id = tab3.material_group_id
                                        AND tab4.number_index = tab2.primary_order_num) tab1;",
            [
              ':_cs_type' => $composite_sample_type_id_new,
              ':_rpeas_pending' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_PENDING,
              ':_rpea_id_new' => $rpa_activity_id_get_new,
              ':_user' => $user,
              ':_date' => $date,
              ':_historical_version_ini' => Functional::ACTIVITY_SAMPLE_HISTORICAL_DATA_VERSION_INIT,
              ':_rpa_id_new' => $request_process_essay_id_new,
              ':_rpa_id' => $request_process_essay_id,
              ':_activity_id_get' => $activity_id_get,
              ':_status_active' => Functional::STATUS_ACTIVE,
              ':_positive_result' => Functional::POSITIVE_RESULT,
              ':_rdt_result' => $rdt_result,
              ':_num_order_id_new' => $num_order_id_new,
            ]
          )->execute();
        }
      }

      if (is_null($rdt_result)) {
        // $data = $data . '| SET READING DATA TYPE NULO ';
        $db->createCommand(
          "INSERT INTO plims.bsns_request_process_essay_activity_sample
                (request_id,
                num_order_id,
                essay_id,
                composite_sample_id,
                composite_sample_type_id,
                crop_id,
                workflow_id,
                num_order,
                request_process_essay_activity_sample_status_id,
                available,
                request_process_essay_activity_id,
                registered_by,
                registered_at,
                activity_id,
                composite_sample_group_id,
                historical_data_version,
                request_process_essay_id,
                is_deleted)
            SELECT request_id,
                  num_order_id,
                  essay_id,
                  composite_sample_id,
                  composite_sample_type_id,
                  crop_id,
                  workflow_id,
                  Row_number ()
                    over (
                      ORDER BY tab1.num_order) AS num_order,
                  request_process_essay_activity_sample_status_id,
                  available,
                  request_process_essay_activity_id,
                  registered_by,
                  registered_at,
                  activity_id,
                  composite_sample_group_id,
                  historical_data_version,
                  request_process_essay_id,
                  is_deleted
            FROM   (SELECT DISTINCT tab2.request_id                     AS request_id,
                                    :_num_order_id_new::INTEGER         AS num_order_id,
                                    tab2.essay_id                       AS essay_id,
                                    tab3.composite_sample_id            AS composite_sample_id,
                                    :_cs_type::INTEGER                AS composite_sample_type_id,
                                    tab2.crop_id                        AS crop_id,
                                    tab2.workflow_id                    AS workflow_id,
                                    tab3.num_order                      AS num_order,
                                    :_rpeas_pending::INTEGER          AS request_process_essay_activity_sample_status_id,
                                    TRUE                                AS available,
                                    :_rpea_id_new::INTEGER            AS request_process_essay_activity_id,
                                    :_user::INTEGER                   AS registered_by,
                                    :_date::TIMESTAMP                 AS registered_at,
                                    tab2.activity_id                    AS activity_id,
                                    tab2.composite_sample_group_id      AS composite_sample_group_id,
                                    :_historical_version_ini::INTEGER AS historical_data_version,
                                    :_rpa_id_new::INTEGER             AS request_process_essay_id,
                                    FALSE                               AS is_deleted
                    FROM   plims.bsns_reading_data tab2
                          inner join plims.bsns_composite_sample tab3
                                  ON tab2.composite_sample_group_id = tab3.composite_sample_group_id
                                      AND tab3.composite_sample_type_id = :_cs_type::INTEGER
                                      AND tab2.request_process_essay_id = :_rpa_id::INTEGER
                                      AND tab2.activity_id = :_activity_id_set_first ::INTEGER
                                      AND tab2.status = :_status_active::TEXT
                                      AND tab2.auxiliary_result = :_positive_result::TEXT
                          INNER JOIN plims.bsns_material_group tab4
                                  ON tab4.composite_sample_group_id =
                                    tab2.composite_sample_group_id
                                    AND tab3.material_group_id = tab4.material_group_id
                                    AND ( ( tab2.primary_order_num = tab4.number_index
                                            AND tab2.reading_data_type_id = :_rdt_entry )
                                            OR ( tab3.reading_data_type_id <> :_rdt_entry ) ) ) tab1;",
          [
            ':_cs_type' => $composite_sample_type_id_new,
            ':_rpeas_pending' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_PENDING,
            ':_rpea_id_new' => $rpa_activity_id_set_new,
            ':_user' => $user,
            ':_date' => $date,
            ':_historical_version_ini' => Functional::ACTIVITY_SAMPLE_HISTORICAL_DATA_VERSION_INIT,
            ':_rpa_id_new' => $request_process_essay_id_new,
            ':_rpa_id' => $request_process_essay_id,
            ':_activity_id_set_first' => $activity_id_set_first,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_positive_result' => Functional::POSITIVE_RESULT,
            ':_num_order_id_new' => $num_order_id_new,
            ':_rdt_entry' => Functional::READING_DATA_ENTRY,
          ]
        )->execute();
      } else {
        // $data = $data . '| SET READING DATA TYPE NO NULO ';
        $db->createCommand(
          "INSERT INTO plims.bsns_request_process_essay_activity_sample
                (request_id,
                num_order_id,
                essay_id,
                composite_sample_id,
                composite_sample_type_id,
                crop_id,
                workflow_id,
                num_order,
                request_process_essay_activity_sample_status_id,
                available,
                request_process_essay_activity_id,
                registered_by,
                registered_at,
                activity_id,
                composite_sample_group_id,
                historical_data_version,
                request_process_essay_id,
                is_deleted)
            SELECT request_id,
                  num_order_id,
                  essay_id,
                  composite_sample_id,
                  composite_sample_type_id,
                  crop_id,
                  workflow_id,
                  Row_number ()
                    over (
                      ORDER BY tab1.num_order) AS num_order,
                  request_process_essay_activity_sample_status_id,
                  available,
                  request_process_essay_activity_id,
                  registered_by,
                  registered_at,
                  activity_id,
                  composite_sample_group_id,
                  historical_data_version,
                  request_process_essay_id,
                  is_deleted
            FROM   (SELECT DISTINCT tab2.request_id                     AS request_id,
                                    :_num_order_id_new::INTEGER         AS num_order_id,
                                    tab2.essay_id                       AS essay_id,
                                    tab3.composite_sample_id            AS composite_sample_id,
                                    :_cs_type::INTEGER                AS composite_sample_type_id,
                                    tab2.crop_id                        AS crop_id,
                                    tab2.workflow_id                    AS workflow_id,
                                    tab3.num_order                      AS num_order,
                                    :_rpeas_pending::INTEGER          AS request_process_essay_activity_sample_status_id,
                                    TRUE                                AS available,
                                    :_rpea_id_new::INTEGER            AS request_process_essay_activity_id,
                                    :_user::INTEGER                   AS registered_by,
                                    :_date::TIMESTAMP                 AS registered_at,
                                    tab2.activity_id                    AS activity_id,
                                    tab2.composite_sample_group_id      AS composite_sample_group_id,
                                    :_historical_version_ini::INTEGER AS historical_data_version,
                                    :_rpa_id_new::INTEGER             AS request_process_essay_id,
                                    FALSE                               AS is_deleted
                    FROM   plims.bsns_reading_data tab2
                          inner join plims.bsns_composite_sample tab3
                                  ON tab2.composite_sample_group_id = tab3.composite_sample_group_id
                                      AND tab3.composite_sample_type_id = :_cs_type::INTEGER
                                      AND tab2.request_process_essay_id = :_rpa_id::INTEGER
                                      AND tab2.activity_id = :_activity_id_set_first ::INTEGER
                                      AND tab2.status = :_status_active::TEXT
                                      AND tab2.auxiliary_result = :_positive_result::TEXT
                                      AND tab3.reading_data_type_id = :_rdt_result::INTEGER 
                          inner join plims.bsns_material_group tab4
                                  ON tab4.composite_sample_group_id =
                                      tab2.composite_sample_group_id
                                      AND tab4.material_group_id = tab3.material_group_id
                                      AND tab4.number_index = tab2.primary_order_num) tab1;",
          [
            ':_cs_type' => $composite_sample_type_id_new,
            ':_rpeas_pending' => Functional::REQUEST_PROCESS_ESSAY_ACTIVITY_SAMPLE_PENDING,
            ':_rpea_id_new' => $rpa_activity_id_set_new,
            ':_user' => $user,
            ':_date' => $date,
            ':_historical_version_ini' => Functional::ACTIVITY_SAMPLE_HISTORICAL_DATA_VERSION_INIT,
            ':_rpa_id_new' => $request_process_essay_id_new,
            ':_rpa_id' => $request_process_essay_id,
            ':_activity_id_set_first' => $activity_id_set_first,
            ':_status_active' => Functional::STATUS_ACTIVE,
            ':_positive_result' => Functional::POSITIVE_RESULT,
            ':_rdt_result' => $rdt_result,
            ':_num_order_id_new' => $num_order_id_new,
          ]
        )->execute();
      }


      $transaction->commit();

      return $data;
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function addSupport($request_id, $num_order_id, $assay_id)
  {

    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {
      $db->createCommand(
        "INSERT INTO plims.bsns_support
        (
              request_id,
              num_order_id,
              crop_id,
              workflow_id,
              composite_sample_type_id,
              essay_id,
              activity_id,
              support_order_id,
              support_type_id,
              request_process_essay_id,
              request_process_essay_activity_id,
              registered_by,
              registered_at,
              status
          )
        SELECT request_id,
              num_order_id,
              crop_id,
              workflow_id,
              composite_sample_type_id,
              essay_id,
              activity_id,
              :_order_ini      AS support_order_id,
              :_undetermined AS support_type_id,
              request_process_essay_id,
              request_process_essay_activity_id,
              registered_by,
              registered_at,
              status
        FROM   plims.bsns_request_process_essay_activity t3
        WHERE  t3.request_id = :_request_id
              AND t3.num_order_id = :_num_order_id
              AND t3.essay_id = :_assay_id;",
        [
          ':_order_ini' => Functional::NUM_ORDER_INI,
          ':_undetermined' => Functional::PARAMETER_UNDETERMINED,
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_assay_id' => $assay_id,
        ]
      )->execute();



      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  // REMOVE
  public function webRequestRemove($request_id)
  {
    $db =  Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      $events_in_request = $db->createCommand(
        "SELECT Count(support_master_id) AS events_in_request
        FROM   plims.bsns_multi_request_event
        WHERE  request_id = :_request_id;",
        [
          ':_request_id' => $request_id,
        ]
      )->queryScalar();

      if ($events_in_request == 0) {
        $composite_sample_group_id = $db->createCommand(
          "SELECT composite_sample_group_id FROM   plims.bsns_request_process WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->queryScalar();
        // eliminacion de lecturas
        $db->createCommand(
          "DELETE FROM plims.bsns_reading_data WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_request_detail WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_material WHERE  composite_sample_group_id = :_sample_group;",
          [
            ':_sample_group' => $composite_sample_group_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_request_process_essay_activity_sample WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_composite_sample WHERE  composite_sample_group_id = :_sample_group;",
          [
            ':_sample_group' => $composite_sample_group_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_material_group WHERE  composite_sample_group_id = :_sample_group;",
          [
            ':_sample_group' => $composite_sample_group_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_support WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_request_process_essay_activity WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_request_process_essay_agent WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_request_process_essay WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_request_process WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_composite_sample_group WHERE  composite_sample_group_id = :_sample_group;",
          [
            ':_sample_group' => $composite_sample_group_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_status WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_request_documentation 
            WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $db->createCommand(
          "DELETE FROM plims.bsns_request WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id,
          ]
        )->execute();
        $transaction->commit();
      } else {
        $support_master_id_list = $db->createCommand(
          "SELECT DISTINCT support_master_id
            FROM   plims.bsns_multi_request_event
            WHERE  request_id = :_request_id;",
          [
            ':_request_id' => $request_id
          ]
        )->queryColumn();

        $request_id_list = $db->createCommand(
          "SELECT DISTINCT request_id
              FROM   plims.bsns_multi_request_event
              WHERE  support_master_id IN (SELECT DISTINCT support_master_id
                                           FROM   plims.bsns_multi_request_event
                                           WHERE  request_id = :_request_id)
                     AND request_id IS NOT NULL;",
          [
            ':_request_id' => $request_id
          ]
        )->queryColumn();

        foreach ($support_master_id_list as $key => $support_master_id) {
          $db->createCommand(
            "DELETE FROM plims.bsns_agent_sample_reviewer WHERE  support_master_id = :_support_master_id;",
            [
              ':_support_master_id' => $support_master_id,
            ]
          )->execute();
        }

        foreach ($request_id_list as $key => $request_id) {

          $composite_sample_group_id = $db->createCommand(
            "SELECT composite_sample_group_id FROM   plims.bsns_request_process WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->queryScalar();

          $db->createCommand(
            "DELETE FROM plims.bsns_reading_data WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();

          $db->createCommand(
            "DELETE FROM plims.bsns_request_detail WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_material WHERE  composite_sample_group_id = :_sample_group;",
            [
              ':_sample_group' => $composite_sample_group_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_request_process_essay_activity_sample WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();





          $db->createCommand(
            "DELETE FROM plims.bsns_composite_sample WHERE  composite_sample_group_id = :_sample_group;",
            [
              ':_sample_group' => $composite_sample_group_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_material_group WHERE  composite_sample_group_id = :_sample_group;",
            [
              ':_sample_group' => $composite_sample_group_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_support WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_request_process_essay_activity WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_request_process_essay_agent WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_request_process_essay WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_request_process WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_composite_sample_group WHERE  composite_sample_group_id = :_sample_group;",
            [
              ':_sample_group' => $composite_sample_group_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_status WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_request_documentation 
              WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();

          $db->createCommand(
            "DELETE FROM plims.bsns_multi_request_event 
                WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();
        }

        foreach ($support_master_id_list as $key => $support_master_id) {
          $db->createCommand(
            "DELETE FROM plims.bsns_multi_request_event
                WHERE  support_master_id = :_support_master_id;",
            [
              ':_support_master_id' => $support_master_id,
            ]
          )->execute();

          $db->createCommand(
            "DELETE FROM plims.bsns_request_documentation 
                WHERE  support_master_id = :_support_master_id;",
            [
              ':_support_master_id' => $support_master_id,
            ]
          )->execute();
          $db->createCommand(
            "DELETE FROM plims.bsns_support_master
                WHERE  support_master_id = :_support_master_id;",
            [
              ':_support_master_id' => $support_master_id,
            ]
          )->execute();
        }


        foreach ($request_id_list as $key => $request_id) {
          $db->createCommand(
            "DELETE FROM plims.bsns_request WHERE  request_id = :_request_id;",
            [
              ':_request_id' => $request_id,
            ]
          )->execute();
        }
        $transaction->commit();
      }
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function webSampleRemove($request_id, $num_order_id, $essay_id, $composite_sample_type_id)
  {
    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      $support_master_id = $db->createCommand(
        "SELECT support_master_id 
      FROM   plims.bsns_request_process_essay
      WHERE  request_id = :_request_id
        AND    num_order_id = :_num_order_id
        AND    essay_id = :_essay_id
        AND    (composite_sample_type_id = :_composite_sample_type_id OR :_composite_sample_type_id is null);",
        [
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_essay_id' => $essay_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
        ]
      )->queryScalar();

      $db->createCommand(
        "DELETE FROM plims.bsns_reading_data
      WHERE  request_id = :_request_id
        AND    num_order_id = :_num_order_id
        AND    essay_id = :_essay_id
        AND    (composite_sample_type_id = :_composite_sample_type_id OR :_composite_sample_type_id is null);",
        [
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_essay_id' => $essay_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
        ]
      )->execute();

      $db->createCommand(
        "DELETE FROM plims.bsns_support
      WHERE  request_id = :_request_id
        AND    num_order_id = :_num_order_id
        AND    essay_id = :_essay_id
        AND    (composite_sample_type_id = :_composite_sample_type_id OR :_composite_sample_type_id is null);",
        [
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_essay_id' => $essay_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
        ]
      )->execute();

      $db->createCommand(
        "DELETE FROM plims.bsns_request_process_essay_agent
      WHERE  request_id = :_request_id
        AND    num_order_id = :_num_order_id
        AND    essay_id = :_essay_id
        AND    (composite_sample_type_id = :_composite_sample_type_id OR :_composite_sample_type_id is null);",
        [
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_essay_id' => $essay_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
        ]
      )->execute();

      $db->createCommand(
        "DELETE FROM plims.bsns_request_process_essay_activity_sample
      WHERE  request_id = :_request_id
        AND    num_order_id = :_num_order_id
        AND    essay_id = :_essay_id
        AND    (composite_sample_type_id = :_composite_sample_type_id OR :_composite_sample_type_id is null);",
        [
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_essay_id' => $essay_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
        ]
      )->execute();

      $db->createCommand(
        "DELETE
      FROM   plims.bsns_support
      WHERE  request_id = :_request_id
        AND    num_order_id = :_num_order_id
        AND    essay_id = :_essay_id
        AND    (composite_sample_type_id = :_composite_sample_type_id OR :_composite_sample_type_id is null);",
        [
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_essay_id' => $essay_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
        ]
      )->execute();

      $db->createCommand(
        "DELETE FROM plims.bsns_request_process_essay_activity
      WHERE  request_id = :_request_id
        AND    num_order_id = :_num_order_id
        AND    essay_id = :_essay_id
        AND    (composite_sample_type_id = :_composite_sample_type_id OR :_composite_sample_type_id is null); ",
        [
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_essay_id' => $essay_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
        ]
      )->execute();

      $db->createCommand(
        "DELETE FROM plims.bsns_support_master
      WHERE  support_master_id = :_support_master_id;",
        [
          ':_support_master_id' => $support_master_id,
        ]
      )->execute();


      $db->createCommand(
        "DELETE FROM plims.bsns_request_process_essay
      WHERE  request_id = :_request_id
        AND    num_order_id = :_num_order_id
        AND    essay_id = :_essay_id
        AND    (composite_sample_type_id = :_composite_sample_type_id OR :_composite_sample_type_id is null);",
        [
          ':_request_id' => $request_id,
          ':_num_order_id' => $num_order_id,
          ':_essay_id' => $essay_id,
          ':_composite_sample_type_id' => $composite_sample_type_id,
        ]
      )->execute();

      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }
  }

  public function setActivitySampleMaterial($user, $date, $request_process_essay_id, $number_of_seeds, $activity_id, $composite_sample_id)
  {

    $db = Yii::$app->db;
    $transaction = $db->beginTransaction();
    try {

      $request_process_essay_activity_sample_id = $db->createCommand(
        "INSERT INTO plims.bsns_request_process_essay_activity_sample
        (
            request_id,
            num_order_id,
            essay_id,
            composite_sample_id,
            composite_sample_type_id,
            crop_id,
            workflow_id,
            num_order,
            request_process_essay_activity_sample_status_id,
            available,
            text_value,
            number_of_seeds,
            description_value,
            request_process_essay_activity_id,
            registered_by,
            registered_at,
            activity_id,
            composite_sample_group_id,
            start_by,
            finish_by,
            check_by,
            start_date,
            finish_date,
            check_date,
            historical_data_version,
            request_process_essay_id,
            is_deleted
        )
        SELECT t1.request_id                                      AS request_id,
          t1.num_order_id                                    AS num_order_id,
          t1.essay_id                                        AS essay_id,
          t1.composite_sample_id                             AS composite_sample_id,
          t1.composite_sample_type_id                        AS composite_sample_type_id,
          t1.crop_id                                         AS crop_id,
          t1.workflow_id                                     AS workflow_id,
          t1.num_order                                       AS num_order,
          t1.request_process_essay_activity_sample_status_id AS request_process_essay_activity_sample_status_id,
          t1.available                                       AS available,
          t1.text_value                                      AS text_value,
          :_number_of_seeds                                  AS number_of_seeds,
          t1.description_value                               AS description_value,
          t1.request_process_essay_activity_id,
          :_registered_by              AS registered_by,
          :_registered_at              AS registered_at,
          t1.activity_id               AS activity_id,
          t1.composite_sample_group_id AS composite_sample_group_id,
          CASE
                  WHEN t1.start_by IS NULL THEN :_registered_by
                  ELSE t1.start_by
          END          AS start_by,
          t1.finish_by AS finish_by,
          t1.check_by  AS check_by,
          CASE
                  WHEN t1.start_date IS NULL THEN :_registered_at
                  ELSE t1.start_date
          END                                        AS start_by,
          t1.finish_date                             AS finish_date,
          t1.check_date                              AS check_date,
          t1.historical_data_version + 1             AS historical_data_version,
          :_request_process_essay_id                 AS request_process_essay_id,
          t1.is_deleted                              AS is_deleted
        FROM   plims.bsns_request_process_essay_activity_sample AS t1
        WHERE  t1.activity_id = :_activity_id
          AND    t1.composite_sample_id = :_composite_sample_id
          AND    t1.available = true
          AND    (
                  t1.number_of_seeds <> :_number_of_seeds
          OR     t1.number_of_seeds IS NULL) 
        RETURNING request_process_essay_activity_sample_id;",
        [
          ':_number_of_seeds' => $number_of_seeds,
          ':_registered_by' => $user,
          ':_registered_at' => $date,
          ':_request_process_essay_id' => $request_process_essay_id,
          ':_activity_id' => $activity_id,
          ':_composite_sample_id' => $composite_sample_id,

        ]
      )->queryScalar();

      $db->createCommand(
        "UPDATE plims.bsns_request_process_essay_activity_sample
        SET    available = false
        WHERE  activity_id = :_activity_id
               AND composite_sample_id = :_composite_sample_id
               AND available = true
               AND request_process_essay_activity_sample_id <> :_request_process_essay_activity_sample_id; ",
        [
          ':_activity_id' => $activity_id,
          ':_composite_sample_id' => $composite_sample_id,
          '_request_process_essay_activity_sample_id' => $request_process_essay_activity_sample_id,
        ]
      )->execute();

      $transaction->commit();
    } catch (\Throwable $e) {
      $transaction->rollBack();
      throw $e;
    }

    return $request_process_essay_activity_sample_id;
  }
}

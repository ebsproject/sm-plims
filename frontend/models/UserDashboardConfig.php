<?php
/*
 * This file is part of Breeding4Results.
 *
 * Breeding4Results is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Breeding4Results is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Breeding4Results.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace frontend\models;

use Yii;
use common\models\User;
use app\models\Config;
use app\models\Application;
use app\models\ApplicationAction;

/**
 * This is the model class for table "platform.user_dashboard_config".
 *
 * @property int $id Unique identifier of the record
 * @property int $user_id ID of the user that has the dashboard configuration
 * @property string $data Dashboard configuration data of user saved in json format
 * @property string $description More information about the record
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 *
 * @property MasterUser $user
 * @property MasterUser $creator
 * @property MasterUser $modifier
 */
class UserDashboardConfig extends UserDashboardConfigBase
{
    /**
     * Retrieves data filters saved in user dashboard config
     *
     * @param $userId integer current user identifier
     * @param $attr text attribute that user wants to retrieve (i.e program_id, phase_id, etc.)
     * @return $data mixed saved data filters 
     */
    public function getDashboardFilters($userId, $attr = null)
    {
        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        if (count($result) < 1) { // if no filters saved in user session yet

            //get program where user belongs to
            $data = ['program_id' => Yii::$app->userprogram->get('id')];
        } else { //if there is data saved in user session
            $config = json_decode($result[0]);

            if (isset($config->dashboard_filters)) {

                $data = $config->dashboard_filters;

                $data->program_id = Yii::$app->userprogram->get('id'); //overwrite value saved in db from url

            } else { //if no saved dashboard filter

                $data = ['program_id' => Yii::$app->userprogram->get('id')];
            }
        }

        $data = (object) $data;

        Yii::$app->session->set('dashboardFilters', $data);

        return $data;
    }

    /**
     * Retrieves program saved in user dashboard config
     *
     * @return $programId program identifier 
     */
    public function getUserProgram()
    {
        $userId = User::getUserId();
        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        if (count($result) < 1) { // if no filters saved in user session yet

            //get program where user belongs to
            $programId = User::getUserProgram($userId);
        } else { //if there is data saved in user session
            $config = json_decode($result[0]);

            if (isset($config->dashboard_filters)) {

                $data = $config->dashboard_filters;

                $programId = $data->program_id;
            } else { //if no saved dashboard filter

                $programId = User::getUserProgram($userId);
            }
        }

        return $programId;
    }

    /**
     * Builds favorites data
     *
     * @param $userId integer user identifier
     * @return $data mixed favorites data
     */
    public static function buildFavoritesData($userId)
    {

        $data = []; //initialize

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        if (count($result) > 0) { // if there is saved in user config
            $config = json_decode($result[0]);

            if (isset($config->favorites)) { //if there are saved favorites
                foreach ($config->favorites as $key => $value) {
                    $app = Application::getAppInfoById($value);
                    if (!empty($app) && isset($app)) { //if application exists
                        $data[] = $app;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Builds tools data
     *
     * @param $userId integer user identifier
     * @param $timestamp timestamp identifier of the tool widget
     * @return $data mixed tools data
     */
    public static function buildToolsData($userId, $timestamp)
    {
        $data = []; //initialize

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        $toolsArr = [];
        if (isset($result[0]) && !empty($result[0])) { //if there is saved in user config
            $config = json_decode($result[0]);

            $tools_timestamp = 'tools_widget_' . $timestamp;

            if (isset($config->$tools_timestamp)) { //if there are saved favorites
                foreach ($config->$tools_timestamp as $key => $value) {
                    $app = Application::getAppInfoById($value);
                    if (!empty($app) && isset($app)) { //if application exists
                        $data[] = $app;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Builds recently used tools data
     *
     * @param $userId integer user identifier
     * @return $data mixed recently used tools data
     */
    public static function buildRecentlyUsedData($userId)
    {
        $data = []; //initialize recently used data
        $timestamp = []; //initialize timestamp

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        if (count($result) > 0) { // if there is saved in user config
            $config = json_decode($result[0]);

            if (isset($config->recently_used)) { //if there are saved favorites
                $apps = array_reverse($config->recently_used);
                foreach ($apps as $key => $value) {
                    if ($key < 20) { //display only last 20 items
                        $app = Application::getAppInfoById($value->application_id);
                        //get timestamp info
                        $timestamp = (isset($value->timestamp) && !empty($value->timestamp)) ? ['timestamp' => $value->timestamp] : [];
                        if (!empty($app) && isset($app)) { //if application exists
                            $data[] = array_merge($app, $timestamp);
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Adds widget in platform.user_dashboard_config
     *
     * @param $widgetId integer widget identifier
     * @param $userId integer user identifier
     */
    public static function addWidget($widgetId, $userId)
    {
        $oldArr = []; //current widgets
        $new = false; //if no data in usre dashboard config yet
        $date = date('Y-m-d H:i:s');

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        $newArr = [["widget_id" => (int) $widgetId, 'timestamp' => $date]];
        //if there is item in user dashboard config
        if (isset($result[0]) && !empty($result[0])) {

            $valArr = json_decode($result[0], true);

            //if there are dashboard widgets saved yet in user's dashboard config
            $oldArr = (isset($valArr['dashboard_widgets'])) ? $valArr['dashboard_widgets'] : [];
        }

        //if none is saved in user's config, get default widgets
        if (empty($oldArr)) {
            $oldArr = Config::getConfigByAbbrev('DEFAULT_DASHBOARD_WIDGETS');

            if (!isset($valArr['dashboard_widgets']) && !isset($result[0])) {
                $new = true;
            }
        }

        $oldCol1Arr = (isset($oldArr['col_1'])) ? $oldArr['col_1'] : [];
        $oldCol2Arr = (isset($oldArr['col_2'])) ? $oldArr['col_2'] : [];
        $widgetsArr = array_merge($newArr, $oldCol1Arr);

        $valArr['dashboard_widgets']['col_1'] = $widgetsArr;
        $valArr['dashboard_widgets']['col_2'] = $oldCol2Arr;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, $new);
    }

    /**
     * Saves data to user dashboard config
     * 
     * @param $data array value of dashboard config that will be saved
     * @param $userId integer user identifier
     * @param $new boolean whether new record or not
     */
    public static function saveNewData($data, $userId, $new = false)
    {

        $param = [
            'data' => $data
        ];

        $method = ($new) ? 'POST' : 'PUT';

        $result = Yii::$app->api->getResponse($method, 'persons/' . $userId . '/dashboard-configurations', json_encode($param));

        if ($result['status'] == 404) {
            Yii::$app->api->getResponse('POST', 'persons/' . $userId . '/dashboard-configurations', json_encode($param));
        }
    }

    /**
     * Checks whether to disable a prompt when removing a widget or not
     * @param $userId integer user identofier
     * @param $disable boolean whether to disable prompt or not
     */
    public static function checkIfShowRemoveWidgetPrompt($userId)
    {
        $disable = false;

        $config = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        if (isset($config[0])) {

            $config = json_decode($config[0], true);

            if (isset($config['disable_remove_widget_prompt'])) {
                $disable = true;
            }
        }

        return $disable;
    }

    /**
     * Removes a widget in dashboard
     *
     * @param $userId integer user identifier
     * @param $col text column identifier
     * @param $key integer dashboard identifier
     */
    public static function removeWidget($userId, $col, $key)
    {

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        $oldArr = [];
        $new = false;
        $newData = [];

        if (isset($result[0]) && !empty($result[0])) {
            $valArr = json_decode($result[0], true);
            $oldArr = (isset($valArr['dashboard_widgets'])) ? $valArr['dashboard_widgets'] : [];
        }

        //if none is saved in user's config, get default widgets
        if (empty($oldArr)) {
            $data = Config::getConfigByAbbrev('DEFAULT_DASHBOARD_WIDGETS');

            $oldArr = (isset($data)) ? $data : [];
        }

        if (!empty($oldArr[$col])) {
            foreach ($oldArr[$col] as $k => $value) {
                if ($k !== (int) $key) {
                    $newData[] = $value;
                }
            }
        }

        $otherCol = 'col_1';
        if ($col == 'col_1') {
            $otherCol = 'col_2';
        }

        $valArr['dashboard_widgets'][$col] = $newData;
        $valArr['dashboard_widgets'][$otherCol] = $oldArr[$otherCol];

        $data = json_encode($valArr);
        //save new config
        UserDashboardConfig::saveNewData($data, $userId, false);
    }

    /**
     * Disable remove widget prompt in dashboard
     *
     * @param $userId integer user identifier
     */
    public static function disableRemoveWidgetPrompt($userId)
    {
        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        $new = false;

        if (isset($result[0]) && !empty($result[0])) {
            $valArr = json_decode($result[0], true);
        } else {
            $new = true;
        }

        $valArr['disable_remove_widget_prompt'] = true;
        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, $new);
    }

    /**
     * Reorder widgets
     *
     * @param $userId integer user identifier
     * @param $col1 array list of widgets in first column
     * @param $col2 array list of widgets in second column
     */
    public static function reorderWidget($userId, $col1, $col2)
    {

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        $valArr = json_decode($result[0], true);
        $valArr['dashboard_widgets'] = ['col_1' => $col1, 'col_2' => $col2];

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, false);
    }

    /**
     * Updates label of a widget
     *
     * @param $userId integer user identifier
     * @param $col text column identifier
     * @param $key integer key identifier
     * @param $val new value of the widget label
     */
    public static function updateWidgetLabel($userId, $col, $key, $val)
    {
        $newData = [];

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        $valArr = json_decode($result[0], true);
        $oldArr = (isset($valArr['dashboard_widgets'])) ? $valArr['dashboard_widgets'] : [];

        //if no saved config yet, get default
        if (empty($oldArr)) {
            $widgets = Config::getConfigByAbbrev('DEFAULT_DASHBOARD_WIDGETS');
            $oldArr = (isset($widgets)) ? $widgets : [];
        }

        $otherCol = 'col_1';
        if ($col == 'col_1') {
            $otherCol = 'col_2';
        }

        foreach ($oldArr[$col] as $k => $value) {
            if ($k !== (int) $key) {
                $newData[] = $value;
            } else {
                $id = ['widget_id' => $value['widget_id']];
                $is_minimized = (isset($value['is_minimized'])) ? ['is_minimized' => $value['is_minimized']] : [];
                $custom_label = ['custom_label' => $val];
                $timestamp = (isset($value['timestamp'])) ? ['timestamp' => $value['timestamp']] : [];

                $newArr = array_merge($id, $is_minimized, $custom_label, $timestamp);
                $newData[] = $newArr;
            }
        }

        $valArr['dashboard_widgets'][$col] = $newData;
        $valArr['dashboard_widgets'][$otherCol] = $oldArr[$otherCol];

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, false);
    }

    /**
     * Minimize or maximize a widget
     *
     * @param $userId integer user identifier
     * @param $col text column identifier
     * @param $key integer key identifier
     * @param $collapse boolean whether to minimize a widget or not
     */
    public static function collapseWidget($userId, $col, $key, $collapse)
    {
        $newData = [];
        $new = false; //if no data in usre dashboard config yet

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

        if (isset($result[0]) && !empty($result[0])) {
            $valArr = json_decode($result[0], true);
            $oldArr = (isset($valArr['dashboard_widgets'])) ? $valArr['dashboard_widgets'] : [];
        }

        //if none is saved in user's config, get default widgets
        if (empty($oldArr)) {
            $widgets = Config::getConfigByAbbrev('DEFAULT_DASHBOARD_WIDGETS');
            $oldArr = (isset($widgets)) ? $widgets : [];

            if (!isset($valArr['dashboard_widgets']) && !isset($result[0])) {
                $new = true;
            }
        }

        $otherCol = 'col_1';
        if ($col == 'col_1') {
            $otherCol = 'col_2';
        }

        foreach ($oldArr[$col] as $k => $value) {
            if ($k !== (int) $key) {
                $newData[] = $value;
            } else {
                $is_minimized = ($collapse !== 'false' && $collapse !== false) ? ['is_minimized' => true] : [];
                $id = ['widget_id' => $value['widget_id']];
                $custom_label = (isset($value['custom_label'])) ? ['custom_label' => $value['custom_label']] : [];
                $timestamp = (isset($value['timestamp'])) ? ['timestamp' => $value['timestamp']] : [];

                $newArr = array_merge($id, $is_minimized, $custom_label, $timestamp);
                $newData[] = $newArr;
            }
        }

        $valArr['dashboard_widgets'][$col] = $newData;
        $valArr['dashboard_widgets'][$otherCol] = $oldArr[$otherCol];

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, $new);
    }

    /**
     * Saves data dashboard data filters to user dashboard config
     * @param $userId integer user identifier
     * @param $data array list of dashboard data filters
     */
    public static function saveDashboardFilters($userId, $data)
    {
        $oldArr = []; //current filters
        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId);
        $new = false;

        if (isset($result[0]) && !empty($result[0])) { //with record
            $valArr = json_decode($result[0], true);
            $oldArr = (isset($valArr['dashboard_filters'])) ? $valArr['dashboard_filters'] : [];
        }

        //if no dashboard filters is saved in config
        if (empty($oldArr)) {
            if (!isset($valArr['dashboard_filters']) && !isset($result[0])) {
                $new = true;
            }
        }

        $valArr['dashboard_filters'] = $data;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, $new);
    }

    /**
     * Check if application is in favorites
     * @param $module text module of the app
     * @param $controller text controller of the app
     * @param $action text action of the app
     * @param $params text params of the app
     * @return mixed data if in favorites or not
     */
    public static function checkIfInFavorites($module, $controller, $action, $params)
    {
        $userId = User::getUserId(); //get user id
        $cond = '';

        $applicationId = UserDashboardConfig::getApplicationId($module, $controller, $action, $params);
        if (empty($applicationId)) { //if application does not exist in database
            return 'App not in db';
        }

        //if in db, check whether in favorites
        if (!empty($applicationId)) {
            $appId = $applicationId[0];
            //get data in user dashboard config
            $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId);

            $favoritesArr = [];
            if (isset($data[0]) && !empty($data[0])) {
                $valArr = json_decode($data[0], true);
                $favoritesArr = (isset($valArr['favorites'])) ? $valArr['favorites'] : [];

                if (in_array($appId, $favoritesArr)) { //if in favorites
                    return ['inFavorites' => true, 'appId' => $appId];
                } else {
                    return ['inFavorites' => false, 'appId' => $appId];
                }
            }
        }

        return ['inFavorites' => false, 'appId' => $appId];
    }

    /**
     * Gets application id by module, controller, action and params
     *
     * @param $module text module of the app
     * @param $controller text controller of the app
     * @param $action text action of the app
     * @param $params text params of the app
     * 
     * @return $applicationId integer application identifier
     */
    public static function getApplicationId($module = null, $controller = null, $action = null, $params = null)
    {
        $applicationId = null;
        $applicationAbbrev = null;

        if (!empty($params)) {
            $params = json_encode($params);
        }

        if (!empty($module)) { //add module condition
            $param['module'] = (string) $module;
        }

        if (!empty($controller)) { //add controller condition
            $param['controller'] = (string) $controller;
        }

        if (!empty($action)) { //add action condition
            $param['action'] = (string) $action;
        }

        if (!empty($params)) { //add params condition
            $param['params'] = (string) $params;
        }

        $appObj = ApplicationAction::searchAll($param, 'limit=1', false);

        // if there is no result, check only module and controller
        if (!isset($appObj['data'][0]['applicationDbId'])) {
            unset($param);
            if (!empty($module)) { //add module condition
                $param['module'] = (string) $module;
            }

            if (!empty($controller)) { //add controller condition
                $param['controller'] = (string) $controller;
            }

            $appObj = ApplicationAction::searchAll($param, 'limit=1', false);
        }

        // if there is no result, check only module
        if (!isset($appObj['data'][0]['applicationDbId'])) {
            unset($param);

            if (!empty($module)) { // add module condition
                $param['module'] = (string) $module;
            }

            $appObj = ApplicationAction::searchAll($param, 'limit=1', false);
        }

        if (isset($appObj['data'][0]['applicationDbId']) && isset($appObj['data'][0]['abbrev'])) {
            $applicationId[] = $appObj['data'][0]['applicationDbId'];
            $applicationAbbrev[] = $appObj['data'][0]['abbrev'];

            //save application abbrev in session
            Yii::$app->session->set('applicationAbbrev', $applicationAbbrev);
        }

        return $applicationId;
    }

    /**
     * Mark on un-mark a tool as favorite
     * @param $appId integer application identifier
     * @return $inFavorites boolean state whther in favorites or not
     */
    public static function markUnmarkFavorites($appId)
    {
        $userId = User::getUserId(); //get user id
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
        $new = false;
        $oldArr = [];
        $inFavorites = false;

        if (isset($data[0]) && !empty($data[0])) { //if there is saved data in config
            $valArr = json_decode($data[0], true);
            $oldArr = (isset($valArr['favorites'])) ? $valArr['favorites'] : [];
            if (in_array($appId, $oldArr)) {
                $inFavorites = true;
            }
        }

        if (!isset($data[0]) && empty($data[0])) { //if there is no saved config
            $new = true;
            $sql = "select config_value from platform.config where abbrev = 'DEFAULT_DASHBOARD_WIDGETS'";
            $res = Yii::$app->db->createCommand($sql)->queryColumn();
            $valArr = (isset($res[0])) ? json_decode($res[0], true) : [];
        }

        if ($inFavorites) { //remove item in favorites
            if (($key = array_search($appId, $oldArr)) !== false) {
                unset($oldArr[$key]);
            }
        } else { //add item in favorites
            if (!in_array($appId, $oldArr)) {
                array_push($oldArr, $appId);
            }
        }

        $valArr['favorites'] = $oldArr;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, $new);

        return $inFavorites;
    }

    /**
     * Un-mark a tool as favorite
     * @param $appId integer application identifier
     * @return $inFavorites boolean state whther in favorites or not
     */
    public static function unmarkFavorites($appId)
    {
        $userId = User::getUserId(); //get user id
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
        $new = false;
        $oldArr = [];

        if (isset($data[0]) && !empty($data[0])) { //if there is saved data in config
            $valArr = json_decode($data[0], true);
            $oldArr = (isset($valArr['favorites'])) ? $valArr['favorites'] : [];
        }

        if (!isset($data[0]) && empty($data[0])) { //if there is no saved config
            $new = true;
            $sql = "select config_value from platform.config where abbrev = 'DEFAULT_DASHBOARD_WIDGETS'";
            $res = Yii::$app->db->createCommand($sql)->queryColumn();
            $valArr = (isset($res[0])) ? json_decode($res[0], true) : [];
        }

        if (($key = array_search($appId, $oldArr)) !== false) {
            unset($oldArr[$key]);
        }

        $valArr['favorites'] = $oldArr;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, $new);
    }

    /**
     * Add and application in tools widget
     *
     * @param $userId integer user identifier
     * @param $timestamp timestamp tools widget identifier
     * @param $appId integer application identifier
     */
    public static function addToolsInWidget($userId, $timestamp, $appId)
    {
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
        if (isset($data[0]) && !empty($data[0])) { //if there is saved data in config
            $valArr = json_decode($data[0], true);
            $oldArr = (isset($valArr['tools_widget_' . $timestamp])) ? $valArr['tools_widget_' . $timestamp] : [];
        }

        //add item in tools widget
        if (!in_array($appId, $oldArr)) {
            array_push($oldArr, $appId);
        }

        $valArr['tools_widget_' . $timestamp] = $oldArr;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, false);
    }

    /**
     * Remove a tool in tools widget
     * @param $appId integer application identifier
     * @param $timestamp timestamp identifier of tool widget
     */
    public static function removeToolInWidget($appId, $timestamp)
    {

        $userId = User::getUserId(); //get user id
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
        $oldArr = [];

        if (isset($data[0]) && !empty($data[0])) { //if there is saved data in config
            $valArr = json_decode($data[0], true);
            $oldArr = (isset($valArr['tools_widget_' . $timestamp])) ? $valArr['tools_widget_' . $timestamp] : [];
        }

        if (($key = array_search($appId, $oldArr)) !== false) {
            unset($oldArr[$key]);
        }

        $valArr['tools_widget_' . $timestamp] = $oldArr;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, false);
    }

    /**
     * Reorder favorites
     * @param $userId integer user identifier
     * @param $appIds array list of application ids
     */
    public static function reorderFavorites($userId, $appIds)
    {
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
        $favArr = [];

        if (isset($data[0]) && !empty($data[0])) { //if there is saved data in config
            $valArr = json_decode($data[0], true);
        }

        $valArr['favorites'] = $appIds;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, false);
    }

    /**
     * Reorder tools in tools widget
     * @param $userId integer user identifier
     * @param $appIds array list of application ids
     * @param $timestamp timestamp identifier of the tool widget
     */
    public static function reorderTools($userId, $appIds, $timestamp)
    {
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
        $favArr = [];

        if (isset($data[0]) && !empty($data[0])) { //if there is saved data in config
            $valArr = json_decode($data[0], true);
        }

        $valArr['tools_widget_' . $timestamp] = $appIds;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, false);
    }

    /**
     * Edit dashboard layout
     * @param $userId integer user identifier
     * @param $layout text of application ids
     */
    public static function saveLayout($userId, $layout)
    {
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
        $new = false;

        if (isset($data[0]) && !empty($data[0])) { //if there is saved data in config
            $valArr = json_decode($data[0], true);
        } else {
            $new = true;
        }

        $valArr['dashboard_layout'] = $layout;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, $new);
    }

    /**
     * Get dashboard layout
     * @param $userId integer user identifier
     * @return $layout text of application ids
     */
    public static function getLayout($userId)
    {
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config

        $layout = 'two';
        if (isset($data[0]) && !empty($data[0])) { //if there is saved data in config
            $valArr = json_decode($data[0], true);
            $layout = isset($valArr['dashboard_layout']) ? $valArr['dashboard_layout'] : 'two';
        }

        return $layout;
    }

    /**
     * Get data in user dashboar configuration
     * @param $userId integer user identifier
     * @return $result array list of user dashboard config data
     */
    public static function getDashboardConfigDataByUserId($userId)
    {
        // retrieve dashboard configuration of the user
        $data = Yii::$app->api->getResponse('GET', 'persons/' . $userId . '/dashboard-configurations');

        // check if has result and retrieve menu data of the user
        if (isset($data['body']['result']['data'][0]['data'])) {
            $data = $data['body']['result']['data'][0]['data'];
        }

        $data = json_encode($data);
        $result[] = $data;

        return $result;
    }

    /**
     * Populate recently used tools in user config
     *
     * @param $userId integer user identifier
     * @param $module text module of the app
     * @param $controller text controller of the app
     * @param $action text action of the app
     * @param $params text params of the app
     */
    public static function populateRecentlyUsed($userId, $module, $controller, $action, $params)
    {
        $applicationId = UserDashboardConfig::getApplicationId($module, $controller, $action, $params);
        $appId = !empty($applicationId) ? $applicationId[0] : 0;
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard config
        $config = json_decode($data[0], true);

        $date = date('Y-m-d H:i:s');
        $oldArr = [];
        $new = false;

        if ($appId !== 0) {

            if (isset($data[0]) && !empty($data[0]) && $config['status'] !== 404) { //if there is saved data in config
                $valArr = json_decode($data[0], true);
                $oldArr = (isset($valArr['recently_used'])) ? $valArr['recently_used'] : [];
            }

            if (!isset($data[0]) && empty($data[0]) && $config['status'] == 404) { //if there is no saved config
                $new = true;
            }

            $newData = [];
            foreach ($oldArr as $key => $value) {
                if ($value['application_id'] == $appId) { //remove to previous array
                } else {
                    $newData[] = $value;
                }
            }

            $newAppArr = ['timestamp' => $date, "application_id" => $appId];
            array_push($newData, $newAppArr);

            $valArr['recently_used'] = $newData;
            $newData = json_encode($valArr);

            //save new config
            UserDashboardConfig::saveNewData($newData, $userId, $new);
        }
    }

    /**
     * Returns all added widgets
     *
     * @param $userId integer user identifier
     * @return $widgetsArr array list of widgets
     */
    public static function getAddedWidgets($userId)
    {
        $data = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard
        // $data = json_decode($data,true);
        $widgetsArr = [];
        $w1 = []; //widgets in columns 1
        $w2 = []; //widgets in coiumn 2

        if (isset($data[0]) && !empty($data[0])) { //if there is saved data in config
            $valArr = json_decode($data[0], true);
            $widgetsArr = (isset($valArr['dashboard_widgets'])) ? $valArr['dashboard_widgets'] : [];
        }

        if ((!isset($data[0]) && empty($data[0])) || empty($widgetsArr)) { //if there is no saved config
            $widgetsArr = Config::getConfigByAbbrev('DEFAULT_DASHBOARD_WIDGETS');
        }

        $w1 = (isset($widgetsArr['col_1']) && !empty($widgetsArr['col_1'])) ? $widgetsArr['col_1'] : [];
        $w2 = (isset($widgetsArr['col_2']) && !empty($widgetsArr['col_2'])) ? $widgetsArr['col_2'] : [];

        $widgetsArr = array_merge($w1, $w2);
        $widgetsArr = array_column($widgetsArr, 'widget_id');

        return $widgetsArr;
    }

    /**
     * Saves layout preferences
     * @param $attribute text attibute identifier
     * @param $value text value of the attribute
     */
    public static function saveLayoutPreferences($attribute, $value)
    {
        $userId = User::getUserId(); //get user id
        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard
        $new = false;

        if (isset($result[0]) && !empty($result[0])) { //with record
            $valArr = json_decode($result[0], true);
        } else {
            $new = true;
        }

        $valArr['layout'][$attribute] = $value;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, $new);
    }

    /**
     * Retrieves layout preferences
     * @return $layout array list or layout preferences
     */
    public static function getLayoutPreferences()
    {
        $userId = User::getUserId(); //get user id
        $layout = [];

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard

        if (isset($result[0]) && !empty($result[0])) { //with record
            $valArr = json_decode($result[0], true);
            $layout = (isset($valArr['layout'])) ? $valArr['layout'] : [];
        }

        return $layout;
    }

    /**
     * Saves current filters
     * @param $name text filter name
     * @param $filters array list of current filters
     */
    public static function saveCurrentFilters($name, $filters)
    {
        //check first if name already exists
        $exists = UserDashboardConfig::checkIfFilterNameExists($name);
        $new = false;
        $valArr = [];

        if ($exists) {
            return 'exists';
        }
        $userId = User::getUserId(); //get user id

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard

        if (isset($result[0]) && !empty($result[0])) { //with record
            $valArr = json_decode($result[0], true);
        } else {
            $new = true;
        }

        $valArr['saved_dashboard_filters'][$name] = $filters;

        $data = json_encode($valArr);

        //save new config
        UserDashboardConfig::saveNewData($data, $userId, $new);
        return 'success';
    }

    /**
     * Checks if filter name already exists
     */
    public static function checkIfFilterNameExists($name)
    {
        $userId = User::getUserId(); //get user id

        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard

        if (isset($result[0]) && !empty($result[0])) { //with record
            $valArr = json_decode($result[0], true);
        }

        if (isset($valArr['saved_dashboard_filters'][$name])) {
            return true;
        }

        return false;
    }

    /**
     * Deletes filter
     * @param $filters currently applied data filters
     * @return filter name if exists
     */
    // public function deleteFilter($filterName)
    // {
    //     $userId = User::getUserId();
    //     $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard
    //     $oldArr = [];

    //     if (isset($result[0]) && !empty($result[0])) { //with record
    //         $valArr = json_decode($result[0], true);
    //         $oldArr = (isset($valArr['saved_dashboard_filters'])) ? $valArr['saved_dashboard_filters'] : [];
    //     }

    //     unset($oldArr[$filterName]);

    //     $valArr['saved_dashboard_filters'] = $oldArr;

    //     $data = json_encode($valArr);

    //     UserDashboardConfig::saveNewData($data, $userId, $new);
    // }

    /**
     * Applies selected filter
     * @param $selected selected data filter to be applied
     */
    // public function applySelectedFilter($selected)
    // {
    //     $userId = User::getUserId();
    //     $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard
    //     $filterArr = [];
    //     $program = null;

    //     if (isset($result[0]) && !empty($result[0])) { //with record
    //         $valArr = json_decode($result[0], true);
    //         $filterArr = (isset($valArr['saved_dashboard_filters'][$selected])) ? $valArr['saved_dashboard_filters'][$selected] : [];
    //     }

    //     if (isset($filterArr['program_id'])) {
    //         $program = Program::getProgramAttr('id', $filterArr['program_id'], 'abbrev');
    //     }
    //     $valArr['dashboard_filters'] = $filterArr;

    //     $data = json_encode($valArr);
    //     UserDashboardConfig::saveNewData($data, $userId, $new);

    //     return $program;
    // }

    /**
     * Builds data for saved filter
     * @param $selected selected data filter to be viewed
     */
    public function buildDashboardFilterData($selected)
    {
        $userId = User::getUserId();
        $result = UserDashboardConfig::getDashboardConfigDataByUserId($userId); //get data in user dashboard
        $filterArr = [];

        if (isset($result[0]) && !empty($result[0])) { //with record
            $valArr = json_decode($result[0], true);
            $filterArr = (isset($valArr['saved_dashboard_filters'][$selected])) ? $valArr['saved_dashboard_filters'][$selected] : [];
        }

        return $filterArr;
    }
}

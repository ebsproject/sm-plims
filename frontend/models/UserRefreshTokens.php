<?php

namespace frontend\models;

use Yii;
use common\models\User;

class UserRefreshTokens extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.auth_user_refresh_tokens}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db1');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urf_userid', 'urf_token', 'urf_ip', 'urf_user_agent'], 'required'],
            [['urf_userid'], 'default', 'value' => null],
            [['urf_userid'], 'integer'],
            [['urf_created'], 'safe'],
            [['urf_token', 'urf_user_agent'], 'string', 'max' => 1000],
            [['urf_ip'], 'string', 'max' => 50],
            [['urf_userid'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['urf_userid' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_refresh_tokenid' => 'User Refresh Tokenid',
            'urf_userid' => 'Urf Userid',
            'urf_token' => 'Urf Token',
            'urf_ip' => 'Urf Ip',
            'urf_user_agent' => 'Urf User Agent',
            'urf_created' => 'Urf Created',
        ];
    }

    /**
     * Gets query for [[UrfUser]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUrfUser()
    {
        return $this->hasOne(User::class, ['user_id' => 'urf_userid']);
    }
}

<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "platform.user_dashboard_config".
 *
 * @property int $id Unique identifier of the record
 * @property int $user_id ID of the user that has the dashboard configuration
 * @property string $data Dashboard configuration data of user saved in json format
 * @property string $description More information about the record
 * @property string $remarks Additional details about the record
 * @property string $creation_timestamp Timestamp when the record was added to the table
 * @property int $creator_id ID of the user who added the record to the table
 * @property string $modification_timestamp Timestamp when the record was last modified
 * @property int $modifier_id ID of the user who last modified the record
 * @property string $notes Additional details added by an admin; can be technical or advanced details
 * @property bool $is_void Indicator whether the record is deleted or not
 *
 * @property MasterUser $user
 * @property MasterUser $creator
 * @property MasterUser $modifier
 */
class UserDashboardConfigBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'platform.user_dashboard_config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'data'], 'required'],
            [['user_id', 'creator_id', 'modifier_id'], 'default', 'value' => null],
            [['user_id', 'creator_id', 'modifier_id'], 'integer'],
            [['data', 'description', 'remarks', 'notes'], 'string'],
            [['creation_timestamp', 'modification_timestamp'], 'safe'],
            [['is_void'], 'boolean'],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::class, 'targetAttribute' => ['user_id' => 'id']],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::class, 'targetAttribute' => ['creator_id' => 'id']],
            [['modifier_id'], 'exist', 'skipOnError' => true, 'targetClass' => MasterUser::class, 'targetAttribute' => ['modifier_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'data' => 'Data',
            'description' => 'Description',
            'remarks' => 'Remarks',
            'creation_timestamp' => 'Creation Timestamp',
            'creator_id' => 'Creator ID',
            'modification_timestamp' => 'Modification Timestamp',
            'modifier_id' => 'Modifier ID',
            'notes' => 'Notes',
            'is_void' => 'Is Void',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MasterUser::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(MasterUser::class, ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModifier()
    {
        return $this->hasOne(MasterUser::class, ['id' => 'modifier_id']);
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use frontend\modules\functional\models\RequestDocumentation;
use frontend\modules\configuration\models\Parameter;
use frontend\modules\functional\Functional;

class UploadForm extends Model
{
  /**
   * @var UploadedFile[]
   */
  public $mixFiles;
  public $filename;
  public $path;

  public function rules()
  {
    return [
      [
        [
          'mixFiles'
        ],
        'file',
        'skipOnEmpty' => false,
        'extensions' => 'xls, xlsx, pdf, doc, docx, ppt, pptx, jpg, png, txt, zip',
        'maxSize' => 1024 * 1024 * 2,
        'maxFiles' => 4,
        'tooBig' => 'A resume can\'t be larger than 2 mb.',
        'checkExtensionByMimeType' => false
      ],
    ];
  }

  public function upload($stampText, $request_id, $essay_id = null)
  {
    $this->path = 'documents/documentation/';
    if ($this->validate()) {
      foreach ($this->mixFiles as $file) {
        try {
          $this->filename = $file->baseName . $stampText . '.' . $file->extension;
          $file->saveAs($this->path . $this->filename);
          // ------------------------------------------------------------------------------------------------------------
          $_document_name = $file->baseName . $stampText;
          $_path_documentation = $this->path;
          $_document_extension = $file->extension;

          $no_exits_document = Yii::$app->db->createCommand(
            "SELECT CASE
                      WHEN Count(1) > 0 THEN false
                      ELSE true
                    END AS exits_document
            FROM   plims.bsns_request_documentation
            WHERE  document_name = :_document_name
                    AND path_documentation = :_path_documentation
                    AND document_extension = :_document_extension
                    AND status = :_status_active;",
            [
              ':_document_name' => $_document_name,
              ':_path_documentation' => $_path_documentation,
              ':_document_extension' => $_document_extension,
              ':_status_active' => Functional::STATUS_ACTIVE,
            ]
          )->queryScalar();
          if ($no_exits_document) {
            $request_documentation = new RequestDocumentation();
            $request_documentation->request_id = $request_id;
            $request_documentation->essay_id = $essay_id;
            $request_documentation->document_name =  $_document_name;
            $request_documentation->path_documentation = $_path_documentation;
            $request_documentation->document_extension = $_document_extension;
            $request_documentation->file_type_id = Parameter::find()->where(['code' => $file->extension])->one()->parameter_id;
            $request_documentation->save();
          }
          // ------------------------------------------------------------------------------------------------------------
        } catch (\Throwable $th) {
          echo "<pre>upload";
          print_r($th);
          echo "</pre>";
          die();
        }
      }
      return true;
    } else {
      return false;
    }
  }

  public function uploadMaterial($stampText)
  {
    $this->path = 'documents/templates/';
    if ($this->validate()) {
      foreach ($this->mixFiles as $file) {
        try {
          $this->filename = $file->baseName . $stampText . '.' . $file->extension;
          $file->saveAs($this->path . $this->filename);
        } catch (\Throwable $th) {
          echo "<pre>upload";
          print_r($th);
          echo "</pre>";
          die();
        }
      }
      return true;
    } else {
      return false;
    }
  }

  public function uploadAbsorbances($stampText, $event_id, $support_order_id, $activity_id, $agent_id)
  {
    $date =  date("Y-m-d H:i:s", time());
    $user =  Yii::$app->user->identity->id;

    $this->path = 'documents/absorbances/';
    if ($this->validate()) {
      foreach ($this->mixFiles as $file) {
        try {
          $this->filename = $file->baseName . $stampText . '.' . $file->extension;
          $file->saveAs($this->path . $this->filename);
          // ------------------------------------------------------------------------------------------------------------
          // disabled other documents with the same name, etc.
          Yii::$app->db->createCommand(
            "UPDATE plims.bsns_request_documentation
            SET    status = :_status_disabled,
                   updated_by = :_user,
                   updated_at = :_date
            WHERE  document_name = :_document_name
                   AND document_extension = :_document_extension
                   AND support_master_id = :_support_master_id
                   AND support_order_id = :_support_order_id
                   AND activity_id = :_activity_id
                   AND agent_id = :_agent_id;",
            [
              ':_status_disabled' => Functional::STATUS_DISABLED,
              ':_user' => $user,
              ':_date' => $date,
              ':_document_name' => $file->baseName . $stampText,
              ':_document_extension' => $file->extension,
              ':_support_master_id' => $event_id,
              ':_support_order_id' => $support_order_id,
              ':_activity_id' => $activity_id,
              ':_agent_id' => $agent_id,
            ]
          )->execute();
          // ------------------------------------------------------------------------------------------------------------
          $file_type_id = Parameter::find()->where(['code' => $file->extension])->one()->parameter_id;

          Yii::$app->db->createCommand(
            "INSERT INTO plims.bsns_request_documentation(
              path_documentation,
              document_name,
              document_extension,
              file_type_id,
              registered_by,
              registered_at,
              status,
              support_master_id,
              support_order_id,
              activity_id,
              agent_id
             )
            VALUES (
              :_path_documentation,
              :_document_name,
              :_document_extension,
              :_file_type_id,
              :_registered_by,
              :_registered_at,
              :_status_active,
              :_support_master_id,
              :_support_order_id,
              :_activity_id,
              :_agent_id
            ); ",
            [
              ':_path_documentation' =>  'documents/absorbances/',
              ':_document_name' => $file->baseName . $stampText,
              ':_document_extension' =>  $file->extension,
              ':_file_type_id' =>  $file_type_id,
              ':_registered_by' => $user,
              ':_registered_at' => $date,
              ':_status_active' => Functional::STATUS_ACTIVE,
              ':_support_master_id' => $event_id,
              ':_support_order_id' => $support_order_id,
              ':_activity_id' => $activity_id,
              ':_agent_id' => $agent_id,
            ]
          )->execute();
          // ------------------------------------------------------------------------------------------------------------
        } catch (\Throwable $th) {
          echo "<pre>upload: ";
          print_r($th);
          echo "</pre>";
          die();
        }
      }
      return true;
    } else {
      return false;
    }
  }
}

<?php

namespace app\models;

use yii\base\Model;

class UploadFile extends Model
{
  public $document_file;

  public function rules()
  {
    return [
      [
        [
          'document_file'
        ],
        'file',
        'skipOnEmpty' => false,
        'extensions' => 'xls, xlsx, pdf, doc, docx, ppt, pptx, jpg, png, txt, zip',
        'maxSize' => 1024 * 1024 * 2
      ],
    ];
  }

  public function upload($stampText)
  {
    $result = [];
    if ($this->validate()) {
      $upload_name = $this->document_file->baseName . $stampText . '.' . $this->document_file->extension;
      $this->document_file->saveAs('documents/absorbances/' .  $upload_name);
      $result = [
        'name' => $upload_name,
        'path' => 'documents/absorbances/',
      ];
      return $result;
    } else {
      return false;
    }
  }

  public function uploadDocumentation($stampText)
  {
    $result = [];
    if ($this->validate()) {
      $upload_name = $this->document_file->baseName . $stampText . '.' . $this->document_file->extension;
      $this->document_file->saveAs('documents/documentation/' .  $upload_name);
      $result = [
        'name' => $upload_name,
        'path' => 'documents/documentation/',
        'extension' => $this->document_file->extension,
      ];
      return $result;
    } else {
      return false;
    }
  }
}

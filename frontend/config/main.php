<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

use \yii\web\Request;
use bizley\jwt\Jwt;
use Lcobucci\JWT\Validation\Constraint\PermittedFor;
use Lcobucci\JWT\Validation\Constraint\StrictValidAt;
use Lcobucci\Clock\SystemClock;

$clock = SystemClock::fromUTC();
$jwtParams['audience'] = 'https://frontend.example.com';
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

return [
    'id' => $baseUrl == 'SEEDHEALTHLABDEV_ID',
    'name' => $baseUrl == '/ebs-plims-dev' ? 'PLIMS - DEV' : 'PLIMS',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'auth' => [
            'class' => 'frontend\modules\auth\Auth',
        ],
        'configuration' => [
            'class' => 'frontend\modules\configuration\Configuration',
        ],
        'functional' => [
            'class' => 'frontend\modules\functional\Functional',
        ],
        'finance' => [
            'class' => 'frontend\modules\finance\Finance',
        ],
        'intelligence' => [
            'class' => 'frontend\modules\intelligence\Intelligence',
        ],
        'api' => [
            'class' => 'frontend\modules\api\Api',
        ],
        'admin' => [
            'class' => 'frontend\modules\admin\Admin',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'jwt' => [
            'class' =>  Jwt::class,
            'signer' =>  Jwt::HS256,
            'signingKey' =>  'aBn2j3FuyNx7npfkLwmeWE6tkGrqKaKD',
            'verifyingKey' => [
                'key' =>  'aBn2j3FuyNx7npfkLwmeWE6tkGrqKaKD',
                'passphrase' => '',
                'store' => Jwt::STORE_IN_MEMORY,
                'method' => Jwt::METHOD_PLAIN,
            ],
            'validationConstraints' => [new PermittedFor($jwtParams['audience']), new StrictValidAt($clock)]
        ],
        'session' => [
            'name' => 'seed-health-lab-dev-session',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['mycat'],
                    'logVars' => [],
                    'logFile' => '@app/runtime/logs/mycat.log'
                ]
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'dateFormat' => 'Y-M-d',
            'datetimeFormat' => 'Y-M-d H:i:s',
            'timeFormat' => 'H:i:s',
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],

        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'basePath' => '@webroot',
                    'baseUrl' => '@web',
                    'js' => [
                        '//code.jquery.com/jquery-3.6.0.min.js',
                    ]
                ],
            ],
            'converter' => [
                'class' => 'yii\web\AssetConverter',
            ],
        ],
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                // ['class' => 'yii\rest\UrlRule', 'controller' => 'user'],
            ]
        ],
    ],
    'params' => $params,
];

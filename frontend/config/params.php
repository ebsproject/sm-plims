<?php

use \yii\web\Request;

$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
putenv("CB_SG_TOKEN_URL=" . getenv('cb_sg_token_url'));
putenv("CB_SG_AUTH_URL=" . getenv('cb_sg_auth_url'));
putenv("CB_SG_CLIENT_ID=" . getenv('cb_sg_client_id'));
putenv("CB_SG_CLIENT_SECRET=" . getenv('cb_sg_client_secret'));
putenv("CB_SG_REDIRECT_URI_WEB=" . getenv('cb_sg_redirect_uri_web'));
putenv("CB_SG_REDIRECT_URI_MOB=" . getenv('cb_sg_redirect_uri_mob'));

return [
    'adminEmail' => 'operator.plims@gmail.com',
    'supportEmail' => 'operator.plims@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
    'institution' => "El Centro Internacional de Mejoramiento de Maíz y Trigo",
    'jwt' => [
        'issuer' => 'https://api.example.com',
        'audience' => 'https://frontend.example.com',
        'id' => 'aBn2j3FuyNx7npfkLwmeWE6tkGrqKaKD',
        'expire' => '+24 hour',
    ],
    'graphql' => [
        'endpoint' => getenv('graphql_endpoint'),
        'qry' => '{"query":"
            query {findContactList(page: {number: 1,size: 1000}){totalElements,totalPages,number,content {email person {givenName,familyName,institutions 
{legalName}},addresses {streetAddress,region,country {name}}}}}
            "}',
        'header' => 'Content-Type: application/json',
    ]
];

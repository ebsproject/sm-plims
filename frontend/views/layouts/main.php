<?php

use common\widgets\Alert;
use frontend\assets\AppAsset;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;

AppAsset::register($this);

$php_version = '[PHP version - ' . phpversion() . ']';
$yii2_version = '[Yii2 version - ' . Yii::getVersion() . ']';
$postgresql_version = Yii::$app->db->createCommand(
    "SELECT '['|| Replace(Trim(Substring(Version(), 1, 16)), 'PostgreSQL','PostgreSQL version - ')|| ']';"
)->queryScalar();

$tec_version = (stripos($_SERVER["REQUEST_URI"], "/ebs-plims-dev") !== false) ? $php_version . $yii2_version . $postgresql_version : '';

$_role_name = '';
$_institution_name = '';
$_laboratory_name = '';

if (!Yii::$app->user->isGuest) {
    $data_row = Yii::$app->user->Identity->profile;
    $_role_name =  $data_row['role_name'];
    $_institution_name = $data_row['institution_name'];
    $_laboratory_name = $data_row['laboratory_name'];
}


$composerData = json_decode(file_get_contents(Yii::getAlias('@app/../composer.json')), true);
$version = isset($composerData['extra']['plims-version']) ? $composerData['extra']['plims-version'] : '1.0.0';

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/png" sizes="16x16" href=<?= Yii::$app->homeUrl . "favicon.ico" ?>>


    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        @media (min-width: 768px) {

            .container {
                width: 750px;
            }
        }

        @media (min-width: 992px) {

            .navbar-nav {
                float: right;
                margin: 0;
            }

            .container {
                width: 970px;
            }
        }

        @media (min-width: 1200px) {
            .container {
                width: 1170px;
            }
        }

        @media (min-width: 1450px) {


            .container {
                width: 1440px;
            }
        }

        @media (min-width: 1600px) {
            .container {
                width: 1550px;
            }
        }

        .container {
            max-width: 1550px !important;
        }

        body {
            font-family: 'Flexo-CapsDEMO';
        }

        .breadcrumb {
            padding: 8px 15px;
            margin-bottom: 20px;
            list-style: none;
            background-color: #f5f5f575;
            border-radius: 4px;
        }

        .footer {
            height: 60px;
            background-color: #f5f5f540;
            border-top: 1px solid #ddd;
            padding-top: 20px;
        }
    </style>

</head>
<!-- sidebar-mini layout-fixed control-sidebar-slide-open 
sidebar-mini layout-fixed control-sidebar-slide-open -->

<body onload="$('#loader').hide();" class="d-flex flex-column h-100">

    <div id="loader">
        <div class="loader-message">
            <i>Loading...<i class='fa fa-refresh fa-spin'></i></i>
        </div>
    </div>

    <?php $this->beginBody() ?>

    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="padding-left: 10%; padding-right: 10%">
            <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>"><?= Html::encode(Yii::$app->name) ?></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">

                    <li class="nav-item active">
                        <a class="nav-link" href=<?= Yii::$app->homeUrl ?>><i class="fa fa-home" aria-hidden="true"></i> Home <span class="sr-only">(current)</span></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href=<?= Yii::$app->homeUrl . "site/about" ?>><i class="fa fa-info-circle" aria-hidden="true"></i> About</a>
                    </li>
                    <?php if (Yii::$app->user->isGuest) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href=<?= Yii::$app->homeUrl . "site/admin" ?>> <i class="fa fa-user-secret" aria-hidden="true"></i> Admin</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href=<?= Yii::$app->homeUrl . "auth/auth/login" ?>><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a>
                        </li>

                    <?php } else if (Yii::$app->user->identity->role >= 1) { ?>

                        <li class="nav-item">
                            <a class="nav-link" href=<?= Yii::$app->homeUrl . "functional/manage-request/index" ?>>
                                Requests
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href=<?= Yii::$app->homeUrl . "functional/manage-multi-request/index" ?>>
                                Multi Requests
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href=<?= Yii::$app->homeUrl . "functional/manage-search/management" ?>>
                                <i class="fa fa-search" aria-hidden="true"></i> Search
                            </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-user-circle-o" aria-hidden="true"></i> <?= Yii::$app->user->identity->username ?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

                                <div style="margin: 10px;">
                                    <small title="Role"><i class="fa fa-users" aria-hidden="true"></i> <?= $_role_name ?></small><br>
                                    <small title="Institution"><i class="fa fa-university" aria-hidden="true"></i> <?= $_institution_name ?></small><br>
                                    <small title="Laboratory"><i class="fa fa-flask" aria-hidden="true"></i> <?= $_laboratory_name ?></small><br>
                                </div>

                                <?php if (Yii::$app->user->identity->role <= 3) { ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href=<?= Yii::$app->homeUrl . 'admin/admin-configuration/management' ?>>
                                        <i class="fa fa-cog" aria-hidden="true"></i>
                                        Configurations
                                    </a>
                                <?php  } ?>

                                <a class="dropdown-item" href=<?= Yii::$app->homeUrl . "site/logout" ?> data-method="post">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    <?php } ?>

                </ul>
            </div>
        </nav>
    </header>

    <main role="main" class="flex-shrink-0">
        <div class="container">
            <?php echo Breadcrumbs::widget(
                [
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]
            ) ?>
            <?php echo Alert::widget() ?>
            <?php echo $content ?>
        </div>
    </main>

    <footer class="footer mt-auto py-3 text-muted">
        <div class="container">

            <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?>
                <?= date('Y') . ' - ' ?>
                <b>PLIMS version. <?= $version ?></b>
                <?= $tec_version ?>
            </p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage();

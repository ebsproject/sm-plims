<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;

$php_version = '[PHP version - ' . phpversion() . ']';
$yii2_version = '[Yii2 version - ' . Yii::getVersion() . ']';
$postgresql_version = Yii::$app->db->createCommand("SELECT '['|| Replace(Trim(Substring(Version(), 1, 16)), 'PostgreSQL','PostgreSQL version - ')|| ']';")->queryScalar();
$database_baseline_version = Yii::$app->db->createCommand("SELECT unnest(string_to_array(text_value, '; ')) as data_values FROM plims.bsns_parameter WHERE code = 'sd00';")->queryColumn();
?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>The International Maize and Wheat Improvement Center, <a href="https://www.cimmyt.org/es/acerca-del-cimmyt/">CIMMYT</a>, is a nonprofit research and training organization with more than 400 partners and collaborators in more than 100 countries.</p>
    <p>His vision is for a world with healthier and more prosperous people - free from global food crises - and more resilient agrifood systems.</p>

    <?php if (Yii::$app->user->isGuest == false) { ?>
        <?php if (Yii::$app->user->identity->id === 1) { ?>
            <h3>Technologies Version</h3>
            <ul>
                <li>
                    <?= $php_version ?>
                </li>
                <li>
                    <?= $yii2_version ?>
                </li>
                <li>
                    <?= $postgresql_version ?>
                </li>
                <?php foreach ($database_baseline_version as $key => $value) {  ?>
                    <li>
                        [<?= $value ?>]
                    </li>
                <?php } ?>
            </ul>
        <?php }  ?>
    <?php }  ?>
</div>
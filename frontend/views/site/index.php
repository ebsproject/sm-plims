<?php

use yii\helpers\Html;

$this->title = 'sm-plims';
?>

<div class="site-index">
    <br />
    <br />
    <br />
    <div class="jumbotron">
        <?= Html::a(
            '<h2>PLIMS - PHYTOSANITARY LABORATORY INFORMATION MANAGEMENT SYSTEM</h2>',
            ['index',],
            ['data-method'  => 'post',]
        ); ?>
        <h4 class="lead">Welcome to SEEDHEALTHLAB application, here you can handle requests for laboratory CIMMYT.</h4>
    </div>
    <br>
</div>
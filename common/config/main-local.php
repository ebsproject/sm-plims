<?php

use \yii\web\Request;

$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
return [
    'components' => [

        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=' . getenv("db_host") . ';dbname=' . getenv("db_name") . ';port=' . getenv("db_port"),
            'username' => getenv("db_user"),
            'password' => getenv("db_pass"),
            'enableSchemaCache' => true,
            'charset' => 'utf8',
            'tablePrefix' => 'bsns_',
            'schemaMap' => [
                'pgsql' => [
                    'class' => 'yii\db\pgsql\Schema',
                    'defaultSchema' => 'plims'
                ]
            ],
        ],

        'db1' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=' . getenv("db_host") . ';dbname=' . getenv("db_name") . ';port=' . getenv("db_port"),
            'username' => getenv("db_user"),
            'password' => getenv("db_pass"),
            'enableSchemaCache' => true,
            'charset' => 'utf8',
            'tablePrefix' => 'auth_',
            'schemaMap' => [
                'pgsql' => [
                    'class' => 'yii\db\pgsql\Schema',
                    'defaultSchema' => 'plims'
                ]
            ],
        ],

        'formatter' => [
            'class' => 'yii\i18n\Formatter',
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@frontend/mail',
            'useFileTransport' => false, //set this property to false to send
        ],
    ],
    'TimeZone' => 'America/Lima',
];

<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    const SUPER_ADMIN = 1;
    const INSTITUTION_ADMIN_ROLE = 2;
    const LABORATORY_ADMIN_ROLE = 3;
    const NORMAL_USER = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{plims.auth_user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        // return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
        return static::findOne(['user_id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

        $user_id = Yii::$app->jwt->parse($token)->claims()->get('uid');
        return static::find()
            ->where(['user_id' =>  $user_id])
            ->andWhere(['<>', 'status', self::STATUS_INACTIVE])
            ->one();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmailAll($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token)
    {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Check if user is admin or not
     *
     * @return $userType boolean whether current logged in user is admin or not
     */
    public function isSuperAdmin()
    {
        $personType = (Yii::$app->user->identity->role == self::SUPER_ADMIN) ? true : false;
        return $personType;
    }

    public function isInstitutionAdmin()
    {
        $personType = (Yii::$app->user->identity->role == self::INSTITUTION_ADMIN_ROLE) ? true : false;
        return $personType;
    }

    public function isLaboratoryAdmin()
    {
        $personType = (Yii::$app->user->identity->role == self::LABORATORY_ADMIN_ROLE) ? true : false;
        return $personType;
    }

    public function isNormalUser()
    {
        $personType = (Yii::$app->user->identity->role == self::NORMAL_USER) ? true : false;
        return $personType;
    }

    public function getRole()
    {
        // if user can have only one role
        return current(\Yii::$app->authManager->getRolesByUser($this->id));
    }

    public function getInstitution()
    {
        $_user = $this->id;
        $_role = $this->role;

        $_institution = Yii::$app->db->createCommand(
            "SELECT  institution_id
            FROM   plims.auth_user_by_role_institution_laboratory
            WHERE  is_deleted = false AND user_id = :_user AND role_id = :_role;",
            [
                ':_user' => $_user,
                ':_role' => $_role,
            ]
        )->queryScalar();

        return $_institution;
    }

    public function getLaboratory()
    {
        $_user = $this->id;
        $_role = $this->role;

        $_laboratory = Yii::$app->db->createCommand(
            "SELECT  laboratory_id
            FROM   plims.auth_user_by_role_institution_laboratory
            WHERE  is_deleted = false AND user_id = :_user AND role_id = :_role;",
            [
                ':_user' => $_user,
                ':_role' => $_role,
            ]
        )->queryScalar();

        return $_laboratory;
    }

    public function getProfile()
    {
        $data_row = Yii::$app->db->createCommand(
            "SELECT CASE
                        WHEN t3.long_name IS NULL THEN ' - '
                        ELSE t3.long_name
                    end AS role_name,
                    CASE
                        WHEN t4.short_name IS NULL THEN ' - '
                        ELSE t4.short_name
                    end AS institution_name,
                    CASE
                        WHEN t5.long_name IS NULL THEN ' - '
                        ELSE t5.long_name
                    end AS laboratory_name
            FROM   plims.auth_user_by_role_institution_laboratory t1
                    INNER JOIN plims.auth_user t2
                            ON t1.user_id = t2.user_id
                                AND t1.role_id = t2.role
                    INNER JOIN plims.auth_role t3
                            ON t1.role_id = t3.role_id
                    LEFT JOIN plims.bsns_institution t4
                            ON t1.institution_id = t4.institution_id
                    LEFT JOIN plims.bsns_laboratory t5
                            ON t1.laboratory_id = t5.laboratory_id
            WHERE  t1.user_id = :_user_id
            LIMIT  1; ",
            [
                ':_user_id' => $this->id
            ]
        )->queryOne();

        return $data_row;
    }
}

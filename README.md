<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Advanced Project Template - PLIMS</h1>
    <br>
</p>

Yii 2 Advanced Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
developing complex Web applications with multiple tiers.

The template includes three tiers: front end, back end, and console, each of which
is a separate Yii application.

The template is designed to work in a team development environment. It supports
deploying the application in different environments.

Documentation is at [docs/guide/README.md](docs/guide/README.md).

[![Latest Stable Version](https://img.shields.io/packagist/v/yiisoft/yii2-app-advanced.svg)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Total Downloads](https://img.shields.io/packagist/dt/yiisoft/yii2-app-advanced.svg)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Build Status](https://travis-ci.com/yiisoft/yii2-app-advanced.svg?branch=master)](https://travis-ci.com/yiisoft/yii2-app-advanced)

## Using this container

The following environment variables/parameters are available (shown below with their respective default values) and can be set during `docker run` invocation.

These environment variables can be seen in the `env.list` file in he path public/

```bash
#Set all environment variables needed to initialize the PLIMS Webpage:
#Main-local file in public folder
ENV db_host=localhost
ENV db_name=sm_plims_db
ENV db_port=5432
ENV db_user=ebsuser
ENV db_pass=3nt3rpr1SE!
#Params file in public folder
ENV cb_sg_token_url=token_url
ENV cb_sg_auth_url=auth_url
ENV cb_sg_client_id=client_id
ENV cb_sg_client_secret=client_secret
ENV cb_sg_redirect_uri_web=redirect_uri_web
ENV cb_sg_redirect_uri_mob=redirect_uri_mob
ENV graphql_endpoint=endpoint
```

### Using Docker Run (more complex but more flexibility)

**Steps**

* Make sure your repository is up to date with remote (ie. `git pull --all`)
* Build the image. Make sure you are in the root directory of this repository, then run

```bash  
docker build -t sm-plims .
```

* If the build succeeds, you should now have the docker image locally. You can then start and initialize the container.
```bash
docker run -it -d -p 80:80 --env-file public/env.list sm-plims:latest
```
Or
```bash
docker run -it -d -p 80:80 -e db_host=localhost -e db_name=sm_plims_db -e db_port=5432 -e db_user=ebsuser -e db_pass=3nt3rpr1SE! sm-plims:latest
```

* Wait a minute or two. Feel free to check the status of the schema migration via `docker logs sm-plims`.
* You now have a running YII2 PLIMS project on port 80 with all the latest changes. You can either connect to it to port 80 from outside the container.
